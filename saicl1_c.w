&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI
&ANALYZE-RESUME
/* Connected Databases
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME wsaicl1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wsaicl1
/*-------------------------------------------------------------------------
  File: SAICL1_C
  Description: Saisie clients
  Input Parameters: Type de saisie C:commandes D:Devis F:Factures R:Comptoir DA:Devis atelier
  Author: HL
  Created: 18/02/97

*** HISTORIQUE DES MODIFS DANS SAICL1_C.I ***
$A66728 IRR 07/01/14 Tenir compte des droits sur l'utilisateur qui est dans un groupe
$A69132 IRR 07/01/14 D�sactiver Echap si le bouton Abandonner n'est pas actif
$A69045 IRR 13/01/14 Correction date de relance devis quand on retourne sur un devis
$A67820 ARI 23/01/14 Ajout d'un param�tre pour gendm_a
$A68906 LMI 22/01/14 Export XML : s�lection + pgms avec option disponible
$A67777 ARI 24/01/14 Tableau libre de l'entete client accessible
$A53038 HL 29/01/14 Condition gratuit, type gratuit (gratuit, typ_gra, {lignecl7.i})
$A68016 CSA 05/02/14 Probl�me de stock emplacement quand date de livraison inf�rieure � la date d'arr�t� de situation
$A74851 IRR 20/03/14 Faire le test en historique contrat de location quand le contrat est cl�tur�
$A74887 MT 03/04/14 Tester param�trage "gestion des affr�tements" avant de lancer les options d'affr�tement
$A76223 JL 17/04/14 Si on enl�ve le cl_paye alors v�rifier si cl_fact pour maj code_reg et code_ech
$A76691 JL 24/04/14 Controler l'unicit� de la ref. cde (parcde.typ_fich='C1', gst_zone[1])
$A77503 IRR 09/05/14 Correction case location coch�e
$A79284 vr 10/06/14 Correction sur calcul date mad/liv
$A69165 HL 21/01/14 Code_tva par d�faut dans tva.dft_poste[7]
$A41450 ALE 27/06/14 Remplacement libell� 'S�lection du correspondant' par 'S�lection du correspondant / Fax Express
$A80432 RDE 23/07/14 Ajout d'un bouton dans la combar pour l'envoi d'un sms
$A65359 HL 19/08/14 Changement de client livr� en modif de commande : modif adresse livraison
$A61956 ARI 18/09/14 Gestion de l'affaire � la ligne
$A85716 IRR 30/09/14 Alimentation de l'affaire, si elle est renseign�e � l'�cran pr�c�dent
$A74998 AOR 26/11/14 Distinguer la pr�f�rence 'saisie par d�faut' pour devis et pour commande
$A92960 CGU 06/02/14 Masquer la case "Afficher les devis termin�s" so aucun client s�lectionn�
$A95455 JCA 03/04/15 Cr�ation avoir ou fac � partir de l'historique : mauvais libell� de fen�tre ("facture financi�re" au lieu de "facture") + "avoir" non g�r� + gestion affichage motif avoir
$A99173 JCA 24/04/15 MAJ num d�p�t sur les lignes PHASE
$A99522 DDG 26/03/15 Correction appel WF dans une transaction
$A50782 CFA 28/04/15 Possibilit� de modifier le d�p�t en modification de factures/avoir financier
$A104178 DDG 28/05/15 Bug: Clic droit en cr�ation pi�ce client ne fonctionne plus
$A41444  GVI 10/06/15 Maj de la langue de l'entetcli avant {entetcl3.i}
$A64611  JCA 15/06/15 Droits pour assistants chapitres
$A83361  CSA 02/06/15 Statut produit/d�pot : manque test dans mouvement de stock
$A106276 DDG 23/07/15 Gestion devis/cde : pb msg erreur Affaire inexistante
$A108407 DDG 03/09/15 Reprise devis & affaire = n� de commande
$A116696 DDG 04/12/15 GED: Pouvoir cr�er des documents GED d�s la cr�ation d'une fiche
$A92146  DDG 22/12/15 Saisie vente et choix materiel, passer le client
$A106349 CGU 04/01/16 Ne afficher les pi�ces si aucun client s�lectionn� et parcde.gst_zone[3] = NO (Affichage du dernier client s�lectionn�)
$A119346 HL 12/01/16 Produit li� gratuit �cras� par recalcul des prix (nmc_lie="L" et gratuit=yes)
$A103205 EMO 12/01/16 Contrat client : nombre de commandes d�j� vendues prend les paniers Web
$A120503 CGU 29/01/16 Ajout de l'option "Recalculer gratuits forc�s"
$A118183 CSA 11/03/16 Modification date livraison sur entete BL => r�percuter sur histoemp/histolot/histoimm
$A122103 ACH 15/03/16 A l'ouverture d'une fiche affaire, d�sactiver la combars
$A124140 MFO 18/03/16 Meilleure gestion des dates
$A127383 ASO 29/04/16 Perte de saisie caisse comptoir si modif BL en comptoir
$A128155 LLH 06/05/16 Workflow : Points d'appel lignes vente sur saicl1
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/
/* Create an unnamed pool to store all the widgets created
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.
/* ***************************  Definitions  ************************** */
/* Parameters Definitions ---                                           */

def input parameter pnocl as int no-undo.
def input parameter nopt as int no-undo. /* si = 556 alors saisie avoirs financiers
                                            si = 615 alors saisie factures financi�res */
def input parameter type-saisie as char no-undo.
def input parameter porigine as char no-undo.
    /*
    "1" --> menu_g
    "2" --> sailiv_c (type-saisie="F") ou abonnfac_c (type-saisie="A")
    "3" --> saitel_c,saicom_c pour �cran F1
    "4" --> saitel_c,saicom_c pour �cran FC1
    "5" --> vispop_b ($1753)
    "6a" --> creation �cran fc1
    "6b" --> modification de SUICLI
    "LOC" -> depuis l'�cran de location
    */
def input parameter prowid as rowid no-undo.
/* Shared Local Variable Definitions ---*/
DEF NEW SHARED VAR G-caisse AS INT.
DEF NEW SHARED VAR G-demoz  AS LOG.

DEF SHARED VARIABLE g-ndos AS INT.
DEF SHARED VARIABLE g-nom AS CHAR.
DEF SHARED VARIABLE G-USER AS CHAR.
DEF SHARED VARIABLE G-DFTDEV AS CHAR.
DEF SHARED VAR g-langue AS CHAR NO-UNDO.  /*A35891*/
DEF SHARED VAR g-multisoc AS LOG. /*$A55544*/

/* Local Variable Definitions ---                                       */

{portail/menu-api.i} /* $A40219 */
{Api-UL.i} /* API sur Unit�s Logistiques A55537 */

/* $A41067... */
DEF TEMP-TABLE tt-web NO-UNDO
    FIELD tt-cpt AS INT
    FIELD tt-sit AS CHAR
    INDEX id1 IS PRIMARY tt-cpt.

DEF VAR init-dir AS CHAR NO-UNDO.

DEF NEW SHARED TEMP-TABLE WCODe NO-UNDO
    FIELD wcode AS INT
    INDEX wcode1 IS PRIMARY UNIQUE wcode.

DEFINE VARIABLE HndDrTyp AS HANDLE     NO-UNDO.
DEFINE VARIABLE drtyp    AS LOGICAL    NO-UNDO.
/* ...$A41067 */

DEF VAR g-condech-ngr1 AS INT no-undo.
DEF VAR g-condech-ngr2 AS INT no-undo.
DEF VAR g-condech-ndos AS INT no-undo.
DEF VAR TvaParDefaut AS INT NO-UNDO.
DEF VAR g-tva-ngr1 AS INT no-undo.
DEF VAR g-tva-ngr2 AS INT no-undo.
DEF VAR g-tva-ndos AS INT no-undo.
DEF VAR g-modereg-ngr1 AS INT no-undo.
DEF VAR g-modereg-ngr2 AS INT no-undo.
DEF VAR g-modereg-ndos AS INT no-undo.
{hGco-Api.DEF}
ASSIGN
    g-condech-ngr1 = GetSessionInt("PocwinShared", "g-condech-ngr1", 0)
    g-condech-ngr2 = GetSessionInt("PocwinShared", "g-condech-ngr2", 0)
    g-condech-ndos = GetSessionInt("PocwinShared", "g-condech-ndos", 0)
    g-modereg-ngr1 = GetSessionInt("PocwinShared", "g-modereg-ngr1", 0)
    g-modereg-ngr2 = GetSessionInt("PocwinShared", "g-modereg-ngr2", 0)
    g-modereg-ndos = GetSessionInt("PocwinShared", "g-modereg-ndos", 0)
    g-tva-ngr1  = GetSessionInt("PocwinShared", "g-tva-ngr1", 0)
    g-tva-ngr2  = GetSessionInt("PocwinShared", "g-tva-ngr2", 0)
    g-tva-ndos  = GetSessionInt("PocwinShared", "g-tva-ndos", 0).
DEF VAR pgmSpec AS CHARACTER NO-UNDO.
DEF BUFFER RTF2 FOR RTF.
DEF VAR valopx AS CHAR no-undo.
DEF VAR codePostal AS CHARACTER  NO-UNDO.
DEF VAR codePostalLiv AS CHARACTER  NO-UNDO. /*$A35969*/
def buffer clientr for client.
def buffer cptclir for cptcli.
def buffer entetcli2 for entetcli.
def buffer lignecli2 for lignecli.
def buffer produitr for produit.
def buffer parcde2 for parcde.
DEF VAR MAJPREST AS LOG no-undo. /* $TX */
DEF VAR NOMPGM  AS CHAR INIT "saicl1_c" no-undo. /* $A38645 */
DEF VAR NOMPGM1 AS CHAR INIT "edtdev" no-undo. /* devis */
DEF VAR NOMPGM2 AS CHAR INIT "edtfac" no-undo. /* facture */
DEF VAR NOMPGM3 AS CHAR INIT "edtarc" no-undo. /* accus� de r�ception */
DEF VAR NOMPGM4 AS CHAR INIT "edtbp" no-undo.  /* Bon de pr�paration */
DEF VAR NOMPGM5 AS CHAR INIT "edtarc" no-undo. /* Proforma */
DEF VAR NOMPGM6 AS CHAR INIT "edtboi" no-undo. /* Bon interne */
DEF VAR NOMPGM7 AS CHAR INIT "edtbl" no-undo.  /* Bon de livraison */
DEF VAR NOMPGM8 AS CHAR INIT "edtspe_bo" no-undo.  /* sp�cification */ /*$BOIS*/
DEF VAR NOMPGM9 AS CHAR INIT "edtctrvt_gp" NO-UNDO.  /*$2629*/
DEF VAR quiauto AS CHAR no-undo. /*$913*/
def var v-rapide as char no-undo. /*$872*/
DEF VAR debut-prog AS LOG no-undo.
DEF VAR BhCpy AS HANDLE no-undo.
DEF var depot-f1 as CHAR no-undo.
def var depot-fc1 as CHAR no-undo.
DEF VAR type-plan AS CHAR no-undo.
DEF VAR interv AS CHAR no-undo.
DEF VAR qprepare AS CHAR no-undo.
DEF VAR qcondition AS CHAR no-undo.
DEF VAR qliste-champ AS CHAR no-undo.
DEF VAR qchar AS CHAR no-undo.
DEF VAR qliste-label AS CHAR no-undo.
DEF VAR CHQRYBY  AS C no-undo.
DEF VAR COLORD   AS C no-undo.
DEF VAR notel AS CHAR no-undo.
DEF VAR nofax AS CHAR no-undo.
DEF VAR ori_mat AS CHAR no-undo.
DEF VAR ori_parc AS CHAR no-undo.
DEF VAR ori_depot AS INT no-undo. /* $a36019 */
def var type-cours as char no-undo.
def var parcod as char no-undo.
def var parlib as char no-undo.
def var vcli as char no-undo.
def var vqui as char no-undo.
def var txtmsg as char no-undo.
def var ltype-saisie as char no-undo.
def var vchar as char no-undo.
def var val-ini as char no-undo.
def var msg as char no-undo.
def var vapp1 as char no-undo.
def var vapp2 as char no-undo.
def var vapp3 as char no-undo.
def var cc1 as char no-undo.
DEF VAR p%val8 AS DEC DECIMALS 8 NO-UNDO.
def var wa as char extent 5 no-undo.
def var wt as char extent 5 no-undo.
def var vld as char extent 5 no-undo.
def var vln as char extent 5 no-undo.
def var vla as char extent 5 no-undo.
def var vlt as char extent 5 no-undo.
def var vll as char extent 5 no-undo.
DEFINE VARIABLE txtmes AS CHARACTER  NO-UNDO. /*$A64016*/


DEFINE TEMP-TABLE TT-opt-ligne NO-UNDO /* $A41467 */
    FIELD nmc_lie   LIKE lignecli.nmc
    FIELD lien_nmc  LIKE lignecli.lien_nmc
    FIELD lig_rowid AS ROWID
    INDEX i1 nmc_lie lien_nmc.

DEFINE VARIABLE crtHorsSaicl1 AS LOGICAL    NO-UNDO. /* $A56248 */

DEFINE VARIABLE CtrlFrame   AS HANDLE     NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COM-HANDLE NO-UNDO.

DEFINE VARIABLE CtrlFrame-2   AS HANDLE     NO-UNDO.
DEFINE VARIABLE chCtrlFrame-2 AS COM-HANDLE NO-UNDO.

DEFINE VARIABLE initmarges AS LOGICAL    NO-UNDO.

/* $1493 ... */
DEF VAR buf-ty_mini AS CHAR NO-UNDO.
DEF VAR buf-bl_mini AS LOG NO-UNDO.
DEF VAR buf-mt_mini AS INT NO-UNDO.
DEF VAR buf-pmini AS LOG NO-UNDO.
/* ... $1493 */

DEFINE VARIABLE passe AS LOGICAL    NO-UNDO.
DEFINE VARIABLE pascharger AS LOGICAL    NO-UNDO. /* $A38251 */
DEFINE VARIABLE etudrapid AS LOGICAL    NO-UNDO.         /* $A40589 */
DEFINE VARIABLE vrai-v-rapide AS CHARACTER  NO-UNDO. /* $A40589 */

{aff-api.i}

DEF VAR g-user-droit AS CHAR no-undo.
DEF VAR g-user-perso AS CHAR no-undo.
DEF VAR ProLink AS CHAR NO-UNDO.
def var aff-dev as c no-undo.
DEF VAR TOTAL-HEURE AS CHAR no-undo.
def var numaff as char no-undo. /*$14*/
DEF VAR dat_mad-ini AS DATE no-undo.
def var wdat_mad as date no-undo.
DEF VAR wforcerdat_mad AS LOG NO-UNDO. /*$A79284*/
DEF VAR tmpdat AS DATE no-undo.
DEF VAR tmpdat2 AS DATE no-undo.
DEF VAR dat-liv-ini AS DATE no-undo.
def var wd as date extent 5 no-undo.
/* Extension dates */
def var wdat_valid as DATE no-undo.
def var wdat_rec as DATE no-undo.
def var wdat_livd as date no-undo.
def var wdat_acc as date no-undo.
def var wdat_rll as date no-undo.
DEF VAR whr_rec AS i no-undo.
DEF VAR wdelai_trs AS I no-undo.
DEF VAR wno_adr AS I no-undo.
DEF VAR tx_atelier AS DEC no-undo.
def var cum-marge as dec no-undo.
/* $2059 ... */
DEFINE VARIABLE on-gere-les-frais AS LOG NO-UNDO.
DEFINE VARIABLE frais1-prc1 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais1-prc2 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais1-prc3 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais1-prc4 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais2-prc1 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais2-prc2 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais2-prc3 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais2-prc4 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais3-prc1 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais3-prc2 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais3-prc3 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais3-prc4 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais4-prc1 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais4-prc2 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais4-prc3 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais4-prc4 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais5-prc1 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais5-prc2 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais5-prc3 AS DECIMAL NO-UNDO.
DEFINE VARIABLE frais5-prc4 AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-1      AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-2      AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-3      AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-4      AS DECIMAL NO-UNDO.
/* $2128... */
DEFINE VARIABLE base-1sec     AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-2sec     AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-3sec     AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-4sec     AS DECIMAL NO-UNDO.
/* ...$2128 */
DEFINE VARIABLE base-total  AS DECIMAL NO-UNDO.
DEFINE VARIABLE base-totalsec AS DECIMAL NO-UNDO. /* $2128 */
/* ... $2059 */
def var cm1 as dec no-undo.
def var cm2 as dec no-undo.
def var cm3 as dec no-undo.
def var cm4 as dec no-undo.
def var cm5 as dec no-undo.
def var kqt1 as dec no-undo.
def var wn as dec extent 5 no-undo.
DEF VAR depass AS DEC no-undo.
DEF VAR encdb AS CHAR no-undo.
DEF VAR TOTAL-POID AS DEC DECIMALS 4 no-undo.
DEF VAR TOTAL-VOLUME AS DEC DECIMALS 4 no-undo.
DEF VAR TOTAL-HT AS DEC DECIMALS 2 no-undo.
DEF VAR TOTAL-Ha AS DEC DECIMALS 2 no-undo.
DEF VAR TOTAL-TTC AS DEC DECIMALS 2 no-undo.
def var tot-dev as dec decimals 2 no-undo.
def var total-uni as DEC no-undo.
DEF VAR TOTAL-ML AS DEC no-undo. /*$A57952*/
DEF VAR wqv AS DEC DECIMALS 5 no-undo.
def var wqp as dec decimals 5 no-undo.
def var px-int as dec decimals 6 no-undo.
DEF VAR HCol AS HANDLE NO-UNDO.
DEF VAR HBrowse AS HANDLE NO-UNDO.
/* DEF VAR HQuery AS HANDLE NO-UNDO.  $2240 */
def var child-info as handle no-undo.
def var wid as handle no-undo.
def var zah as handle extent 5 no-undo.
def var znh as handle extent 5 no-undo.
def var zdh as handle extent 5 no-undo.
def var zlh as handle extent 5 no-undo.
def var zth as handle extent 5 no-undo.
def var xah as handle extent 5 no-undo.
def var xnh as handle extent 5 no-undo.
def var xdh as handle extent 5 no-undo.
def var xlh as handle extent 5 no-undo.
def var xth as handle extent 5 no-undo.
def var first-wid as handle no-undo.
def var first-val as handle no-undo.
def var first-fc1 as handle no-undo.
def var ecr-cours as i no-undo.
def var x as i no-undo.
def var w as i no-undo.
def var y as i no-undo.
def var typ-cde as i no-undo.
def var parnum as i no-undo.
def var pcores as i no-undo.
def var chap-cours as i NO-UNDO INIT 1.
DEFINE VARIABLE nb-chap AS INTEGER    NO-UNDO.
def var chap1 as i no-undo.
def var cle-cours as i no-undo.
DEF VAR local_no_info AS INT no-undo.
DEF VAR sem-liv-ini AS INT no-undo.
DEF VAR qcolonne-lock AS i no-undo.
DEF VAR pcol AS INT no-undo.
def var nocde as i no-undo.
DEF VAR ecart_liv_mad AS INT INIT -1 no-undo.
DEF VAR TOTAL-COL AS I no-undo.
DEF VAR TOTAL-PAL AS I no-undo.
DEF VAR no-semaine AS I no-undo.
DEF VAR Fax   AS LOG no-undo.
def var maj-sto as log no-undo.
def var codebarre as log no-undo.
def var note-modifiee as log no-undo.
DEF VAR droit-modifier AS LOG EXTENT 4 INIT YES no-undo.
def var aucun-depot as LOG no-undo.
DEF VAR ListeDepots AS CHAR no-undo.
DEF VAR COLDESC  AS LOG no-undo.
DEF VAR wori_ate AS LOG INIT NO no-undo. /* type atelier */
DEF VAR wori_saibp1 AS LOG INIT NO no-undo. /* origine saibp1 en cr�ation */ /* $a36019 */
DEF VAR LOGBID AS LOG no-undo.
def var creation as log no-undo.
def var plusieurs-types as log no-undo.
def var modif-entete as log no-undo.
def var toutes-saisies as log no-undo.
def var retour as LOG no-undo.
def var modif-val-libre as log no-undo.
def var kop1 as log no-undo.
def var kva1 as log no-undo.
def var kpm1 as log no-undo. /* $1630 */
def var wl as LOG extent 5 no-undo.
def var val-traite as log no-undo. /* Chapitre valeur libre trait� */
def var au-moins-une as log no-undo. /* Au moins une valeur libre d�plac�e */
DEF VAR gere_ate AS LOG INIT NO no-undo. /* $879 */
DEF VAR gere_aff AS LOG INIT NO no-undo. /* $1932 */
def var droit-marge as log no-undo.
DEF VAR pas-droit-crt AS LOG no-undo.
DEF VAR pas-droit-vpr AS LOG no-undo. /*$2629*/
def var pmodpha as log no-undo.
def var pdetail as log no-undo.
def var pphatot as log no-undo.
def var pphatxt as log no-undo.
def var pqte    as log no-undo.
def var ppu     as log no-undo.
DEFINE VARIABLE lgTextile AS LOGICAL NO-UNDO.
DEF VAR rowid-planav AS ROWID no-undo.
def var svrowid as rowid no-undo.
def var svrowid-lig as rowid no-undo.
def var derlig as i no-undo.
DEF VAR rentetcli AS ROWID      NO-UNDO. /* $2240 */

DEF VAR pref_parcde_prod AS LOG no-undo.        pref_parcde_prod = CAN-FIND (FIRST parcde WHERE parcde.typ_fich = "D" AND parcde.gst_zone [3] NO-LOCK). /* $1090 & $1091 */
DEF VAR pref_parcde_famcpt AS LOG no-undo.      pref_parcde_famcpt = CAN-FIND (FIRST parcde WHERE parcde.typ_fich = "D" AND parcde.gst_zone [2] NO-LOCK).
DEF VAR pref_parcde_commerc AS LOG no-undo.     pref_parcde_commerc = CAN-FIND (FIRST parcde WHERE parcde.typ_fich = "I" AND parcde.gst_zone [1] NO-LOCK).
DEF VAR pref_parcde_groupe AS LOG NO-UNDO.      pref_parcde_groupe = CAN-FIND (FIRST parcde WHERE parcde.typ_fich = "I" AND parcde.gst_zone [3] NO-LOCK).
DEF VAR pref_parcde_transpor AS LOG no-undo.    pref_parcde_transpor = CAN-FIND (FIRST parcde WHERE parcde.typ_fich = "I" AND parcde.gst_zone [2] NO-LOCK).

{fabdep.i} /* $1091 */
{decia.i} /* $A59286 */
{deciv.i}
{decim.i}
{tmp-fac.i}
{tmp-bl.i}
{tmp-arc.i} /* $690 */
{calcul_dat.i}
{radical.i "5"}
{tmp-bp.i}
{tmpfor.i 1}
{bois-api.i}
{gco-majstk.i} /*$STKJR*/
{api-piece-cl.i} /* $2178 */
DEF buffer produit3 for produit. /* $1090 */
DEF VAR CorFax   AS LOG NO-UNDO. /*$1022*/
DEF VAR Proforma-fax AS LOG    NO-UNDO. /*$A20038*/
DEF VAR Proforma-mail AS LOG    NO-UNDO. /*$A20038*/

DEF VAR CorMail  AS LOG NO-UNDO.
DEF VAR Wmaildev AS L NO-UNDO.
DEFINE VARIABLE Wmailoffre AS LOGICAL    NO-UNDO.
DEFINE VARIABLE Devniv1 AS LOGICAL    NO-UNDO.
DEFINE VARIABLE Devniv2 AS LOGICAL    NO-UNDO.
DEFINE VARIABLE Devniv3 AS LOGICAL    NO-UNDO.
/*$1105 : Workflow */
DEF VAR gest-wkf AS LOG  NO-UNDO.
DEF VAR pok-wkf AS LOG  NO-UNDO.
DEF VAR paction-wkf AS LOG  NO-UNDO.
DEF VAR pretwf AS CHAR no-undo.
DEF TEMP-TABLE wecli NO-UNDO LIKE entetcli.
DEF TEMP-TABLE wlcli NO-UNDO LIKE lignecli /*$A128155*/
    FIELD rid AS ROWID.		               /*$A128155*/
DEF VAR wlist-init AS CHAR NO-UNDO.
{workflow/api_wf.i}
/* ...$1105*/
DEF VAR depotcsg AS INT no-undo. /*$1130*/

DEFINE VARIABLE rccola AS INT NO-UNDO.
DEFINE VARIABLE rccolb AS INT NO-UNDO.
DEFINE VARIABLE ColBlanc AS CHARACTER NO-UNDO. ColBlanc = "16777215".
DEFINE VARIABLE Ftitre-e AS CHARACTER  NO-UNDO.
DEF VAR myonglet     AS COM-HANDLE no-undo.
DEF VAR xitem          AS COM-HANDLE NO-UNDO.
DEF VAR xitem-edtDocs AS COM-HANDLE NO-UNDO. /*$A36586*/
/*$A49610...*/
DEF VAR xitem-edtArc AS COM-HANDLE NO-UNDO.
DEF VAR xitem-edtFac AS COM-HANDLE NO-UNDO.
DEF VAR xitem-edtDev AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE CorDocsComp AS LOGICAL    NO-UNDO.
DEFINE VARIABLE CorAvcConf AS LOGICAL    NO-UNDO.
DEFINE VARIABLE ARCDocsComp AS LOGICAL    NO-UNDO.
DEFINE VARIABLE ARCAvcConf AS LOGICAL    NO-UNDO.
DEFINE VARIABLE DEVDocsComp AS LOGICAL    NO-UNDO.
DEFINE VARIABLE DEVAvcConf AS LOGICAL    NO-UNDO.
DEFINE VARIABLE FACDocsComp AS LOGICAL    NO-UNDO.
DEFINE VARIABLE FACAvcConf AS LOGICAL    NO-UNDO.
DEF VAR Docmaildev AS LOG    NO-UNDO.
DEF VAR Docmailarc AS LOG    NO-UNDO.
DEF VAR Docmailbl AS LOG    NO-UNDO.
DEF VAR Docmailfac AS LOG    NO-UNDO.

DEF VAR g-hdocs AS HANDLE NO-UNDO.
/*...$A49610*/

DEF VAR xitem2         AS COM-HANDLE NO-UNDO. /* $A38645 */
DEFINE VARIABLE Pload AS LOGICAL    NO-UNDO.
/*$1383...*/
DEF VAR parm-edt AS CHAR NO-UNDO.
DEF VAR modedit AS INT INIT 1 NO-UNDO.
DEF VAR wrecap AS CHAR NO-UNDO.
DEF VAR wdetail AS LOG NO-UNDO.
DEF VAR wdetail-texte AS LOG NO-UNDO.
DEF VAR wdescente AS LOG NO-UNDO.
DEF VAR wnote AS LOG NO-UNDO.
DEF VAR wentete AS LOG NO-UNDO.
DEF VAR wpied AS LOG NO-UNDO.
DEF VAR wvalo AS CHAR NO-UNDO.
DEF VAR wheure AS CHAR NO-UNDO.
DEF VAR wrecmo AS LOG NO-UNDO.
DEF VAR wnivo AS INT NO-UNDO.
DEF VAR wref AS CHAR NO-UNDO.
DEF VAR wvar AS LOG NO-UNDO.
DEF VAR wopt AS LOG NO-UNDO.
DEF VAR wpm AS LOG NO-UNDO. /* $1630 */
DEF VAR adr_fac2cde AS LOG    NO-UNDO. /*$A60003*/

DEF VAR atelier_sur_cde AS LOG INIT FALSE no-undo.
DEF VAR chemin_calpx_t AS CHAR NO-UNDO. chemin_calpx_t = "calpx_t".

/*$A38645...*/
DEF VAR bcataloguePressed    AS LOGICAL NO-UNDO INIT NO. /* bouton catalogue electronique press� ?*/
DEFINE TEMP-TABLE tt-catalog NO-UNDO
    FIELD tt-id         AS CHAR
    FIELD tt-option     AS INT      /* reperer l'option choisie en fonction du catalogue */
    FIELD tt-libelle    AS CHAR
    FIELD tt-appelant   AS CHAR     /* Programme progress appelant */
    INDEX id1 IS PRIMARY tt-option.
/*...$A38645*/


DEFINE VARIABLE intiTab AS CHARACTER  NO-UNDO. /*A35891*/
DEFINE VARIABLE Pcaisse AS INTEGER    NO-UNDO. /*$2129*/
/* $2240... */
DEF VAR hparent AS HANDLE     NO-UNDO.
hparent = SOURCE-PROCEDURE.
/* ...$2240 */
/*...$1383*/
{Hgcoergo.i}
{hcombars.i}
{G-hprowinBase.i}

/*$2104...*/
{VERSION_tps.i htps-api.i}
{VERSION_tps.i prop_tps.i}
DEFINE VARIABLE buf-id  AS ROWID  NO-UNDO.
/*...$2104*/

{recpgminit.i} /*$A61779*/

DEF VAR droits-modif-aff AS LOG    NO-UNDO. /*$A32485*/

DEF VAR FrmComBars-f1  AS HANDLE NO-UNDO.
DEF VAR hComBars-f1    AS INT NO-UNDO.
&SCOPED-DEFINE bquitter         101
&SCOPED-DEFINE bcreer           102
&SCOPED-DEFINE bmodifier        103
&SCOPED-DEFINE bsupprimer       104
&SCOPED-DEFINE bpopup           105
&SCOPED-DEFINE bediter-devis    106
&SCOPED-DEFINE bediter-bp       107
&SCOPED-DEFINE bannuler-bp      108
&SCOPED-DEFINE bediter-interne  109
&SCOPED-DEFINE bediter-arc      110
&SCOPED-DEFINE bediter-proforma 111
&SCOPED-DEFINE bvalider-bl      112
&SCOPED-DEFINE breediter-bl     113
&SCOPED-DEFINE bsaisie-liv      114
&SCOPED-DEFINE bediter-facture  115
&SCOPED-DEFINE bdc              116
&SCOPED-DEFINE bcd              117
&SCOPED-DEFINE bcbl             118
&SCOPED-DEFINE Bediter          119
&SCOPED-DEFINE Bajouter-colonnes  120
&SCOPED-DEFINE Bajouter-colonnes2 121
&SCOPED-DEFINE Bvoir            122
&SCOPED-DEFINE Bdevis           123
&SCOPED-DEFINE Boption          124
&SCOPED-DEFINE bavan            125
&SCOPED-DEFINE Baffcde          126
&SCOPED-DEFINE Bwrkf            127
&SCOPED-DEFINE Baffenc          128
&SCOPED-DEFINE Bwrkf2           129
&SCOPED-DEFINE Bwrkf3           130
&SCOPED-DEFINE Bfax             131
&SCOPED-DEFINE B-mail           132
&SCOPED-DEFINE bsms             171 /* $A80432 */
&SCOPED-DEFINE Bgstenvoi        133
&SCOPED-DEFINE Bcorres-f        134
&SCOPED-DEFINE Bcorres-m        135
&SCOPED-DEFINE Bmaildev         136
&SCOPED-DEFINE Bmaildev1        137
&SCOPED-DEFINE Bmaildev12       138
&SCOPED-DEFINE Bmaildevt        139
&SCOPED-DEFINE bediter-spe      140 /*$BOIS*/
&SCOPED-DEFINE brelance         141 /* $1855 */
&SCOPED-DEFINE Bmailoffre       142
&SCOPED-DEFINE Bverifpx         143 /*$2026*/
&SCOPED-DEFINE Bcomptoir        144 /*$2176*/
&SCOPED-DEFINE Bgendm           145
&SCOPED-DEFINE Breaffect        146
&SCOPED-DEFINE Bacompte         147  /*$2624*/
&SCOPED-DEFINE bediter-ctrvt    148 /*$2629*/
&SCOPED-DEFINE bproforma-f      149  /*$A20038*/
&SCOPED-DEFINE bproforma-m      150 /*$A20038*/
&SCOPED-DEFINE bedtDocComp      151 /*$A36586*/
&SCOPED-DEFINE bedtAvcConf      152 /*$A36586*/
&SCOPED-DEFINE Bdetailtemps     153 /*$A39538*/
/* $A41067... */
&SCOPED-DEFINE bfiche           154
&SCOPED-DEFINE bweb             155
&SCOPED-DEFINE binfor           156
&SCOPED-DEFINE bintegrer        157 /*$A41829*/
&SCOPED-DEFINE bparedtg         158
/*$A49610...*/
&SCOPED-DEFINE bedtDocARC       159
&SCOPED-DEFINE bedtAvcARC       160
&SCOPED-DEFINE bedtDocDEV       161
&SCOPED-DEFINE bedtAvcDEV       162
&SCOPED-DEFINE bedtDocFAC       163
&SCOPED-DEFINE bedtAvcFAC       164
&SCOPED-DEFINE docmailDEV       165
&SCOPED-DEFINE docmailBL        166
&SCOPED-DEFINE docmailARC       167
&SCOPED-DEFINE docmailFAC       168
/*...$A49610*/
&SCOPED-DEFINE btn-infoentete   169 /*$A54437*/
&SCOPED-DEFINE bXML             170 /*$A68906*/

DEF VAR FrmComBars-fentete  AS HANDLE NO-UNDO.
DEF VAR hComBars-fentete    AS INT NO-UNDO.
&SCOPED-DEFINE bannuler-p   201
&SCOPED-DEFINE bvalider-p   202
&SCOPED-DEFINE bsupprimer-p 203
&SCOPED-DEFINE brecalcul    204
&SCOPED-DEFINE bnote       205
&SCOPED-DEFINE bcolonnes    206
&SCOPED-DEFINE Bpopup2      207
&SCOPED-DEFINE Bdocinfo     208
&SCOPED-DEFINE Bchgadr      209
&SCOPED-DEFINE btexte       210
&SCOPED-DEFINE bversdev     211
&SCOPED-DEFINE Badrliv      212 /*$A35969*/
&SCOPED-DEFINE Btablibre    213 /*$A67777*/
&SCOPED-DEFINE bcatelec     270  /* $A38645 270 -> 300 r�serv� pour l'acc�s aux catalogues �lectroniques */

/* de 350 � 400 r�serv� sites web */
/* ...$A41067 */

/*$A35969...*/
DEF VAR FrmComBars-adrliv  AS HANDLE NO-UNDO.
DEF VAR hComBars-adrliv    AS INT NO-UNDO.
&SCOPED-DEFINE babandonner-adrliv   301
&SCOPED-DEFINE bvalider-adrliv   302

DEF VAR adrliv-modifie AS LOG NO-UNDO.
DEF VAR adrliv-zon_geo AS INT NO-UNDO.
DEF VAR adrliv-typ_veh AS LOG NO-UNDO.
DEF VAR adrliv-typ_con AS CHAR NO-UNDO.
DEF VAR adrliv-civ_liv AS CHAR NO-UNDO.
DEF VAR adrliv-adr_liv AS CHAR EXTENT 3 NO-UNDO.
DEF VAR adrliv-adrliv4 AS CHAR NO-UNDO.
DEF VAR adrliv-k_post2l AS CHAR NO-UNDO.
DEF VAR adrliv-villel AS CHAR NO-UNDO.
DEF VAR adrliv-paysl AS CHAR NO-UNDO.

DEF VAR adrliv-transpor AS INT NO-UNDO.
DEF VAR adrliv-mod_liv AS CHAR NO-UNDO.
DEF VAR adrliv-comment AS CHAR NO-UNDO.
DEF VAR adrliv-com_liv AS CHAR EXTENT 4 NO-UNDO.
DEF VAR adrliv-delai_trs AS INT NO-UNDO.
DEF VAR adrliv-port AS INT NO-UNDO.
DEF VAR adrliv-no_adr AS INT NO-UNDO.

DEF VAR recalculer-prix-franco AS LOG NO-UNDO.
/*...$A35969*/


DEF VAR hpostit AS HANDLE NO-UNDO.

DEFINE VARIABLE vContrat AS CHARACTER NO-UNDO. /* $1727 */
DEFINE VARIABLE vIndice AS INTEGER NO-UNDO. /* $1727 */
DEFINE VARIABLE vChargerClient AS LOGICAL NO-UNDO. /* $1753 */
{trpaff-api.i} /*$1728*/

{ctr-libs.i}
{artphase.i}
{delcal_gp.i}

/* $2215 ... */
DEF VAR lstparc AS CHAR NO-UNDO.
DEF BUFFER materielr FOR materiel.
DEF VAR vTypFich AS CHAR NO-UNDO.
DEF VAR vMateriel AS CHAR NO-UNDO.
DEF VAR vNoOrdre AS INT NO-UNDO.
/* ... $2215 */

DEF VAR vTypeContrat AS CHAR NO-UNDO. /* $A33727 "LOC" -> Location, "" -> Maintenance */

DEF VAR val-chap1 AS LOG    NO-UNDO.
DEF VAR val-chap2 AS LOG    NO-UNDO.

DEF VAR pmsg AS CHAR  NO-UNDO. /*$A33800*/

{gco-mvtstk.i} /* $A40207 */

DEF VAR afficherinfo AS LOG    NO-UNDO. /*$A54437*/

DEF VAR vNeSertPas AS CHAR NO-UNDO. /*$A57952*/

/* Verrouillages logiques : SetLock, FreeLock, IsLockedEx  $A42431 */
DEF VAR pQuiLck AS CHAR NO-UNDO.
DEF VAR pQuiWLck AS CHAR NO-UNDO.
DEF VAR pPCLck AS CHAR NO-UNDO.
DEF VAR pDateLck AS DATE NO-UNDO.
DEF VAR pHeureLck AS CHAR NO-UNDO.
DEF VAR pComment AS CHAR NO-UNDO.

DEF VAR ChangementClientLivre AS LOG NO-UNDO. /*$A65359*/

DEFINE VARIABLE pvalaffaire AS CHARACTER  NO-UNDO. /*$A85716*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f1

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES entetcli

/* Definitions for FRAME fonglet                                        */
&Scoped-define FIELDS-IN-QUERY-fonglet entetcli.cod_cli entetcli.no_cde ~
entetcli.no_bl
&Scoped-define ENABLED-FIELDS-IN-QUERY-fonglet entetcli.cod_cli ~
entetcli.no_cde entetcli.no_bl
&Scoped-define ENABLED-TABLES-IN-QUERY-fonglet entetcli
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-fonglet entetcli
&Scoped-define QUERY-STRING-fonglet FOR EACH entetcli SHARE-LOCK
&Scoped-define OPEN-QUERY-fonglet OPEN QUERY fonglet FOR EACH entetcli SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-fonglet entetcli
&Scoped-define FIRST-TABLE-IN-QUERY-fonglet entetcli


/* Definitions for FRAME FRAME-adrliv                                   */
&Scoped-define FIELDS-IN-QUERY-FRAME-adrliv entetcli.adr_liv[1] ~
entetcli.adrliv4 entetcli.k_post2l entetcli.zon_geo entetcli.typ_con ~
entetcli.typ_veh
&Scoped-define ENABLED-FIELDS-IN-QUERY-FRAME-adrliv entetcli.adr_liv[1] ~
entetcli.civ_liv entetcli.adr_liv[2] entetcli.adr_liv[3] entetcli.adrliv4 ~
entetcli.k_post2l entetcli.villel entetcli.paysl entetcli.zon_geo ~
entetcli.typ_con entetcli.typ_veh
&Scoped-define ENABLED-TABLES-IN-QUERY-FRAME-adrliv entetcli
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-FRAME-adrliv entetcli
&Scoped-define QUERY-STRING-FRAME-adrliv FOR EACH entetcli SHARE-LOCK
&Scoped-define OPEN-QUERY-FRAME-adrliv OPEN QUERY FRAME-adrliv FOR EACH entetcli SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-FRAME-adrliv entetcli
&Scoped-define FIRST-TABLE-IN-QUERY-FRAME-adrliv entetcli


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Bfidelite RECT-25 Bcrtcli RECT-29 RECT-10 ~
brc-cli BR-nc$ cli$ nat$ tvoir cdepot BR-mg qui$ dat-cde$ typ-cde$ dat-liv$ ~
sem$ ref-cde$ not-ref$ Tbdevis Ft$selection-1
&Scoped-Define DISPLAYED-OBJECTS cli$ lib-cli nat$ tvoir cdepot qui$ LIB-mg ~
dat-cde$ typ-cde$ dat-liv$ sem$ ref-cde$ not-ref$ Tbdevis Ft$selection-1

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wsaicl1 AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Bcrtcli  NO-FOCUS FLAT-BUTTON
     LABEL "Button 1"
     SIZE 3.14 BY .92 TOOLTIP "Cr�ation rapide d'un client (CTRL-F6)".

DEFINE BUTTON Bfidelite  NO-FOCUS FLAT-BUTTON
     LABEL ""
     SIZE 3.14 BY .92 TOOLTIP "S�lection carte de fid�lit�".

DEFINE BUTTON BR-mg
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-nc$
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brc-cli
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE VARIABLE cdepot AS CHARACTER INITIAL "0"
     LABEL "D�p�t"
     VIEW-AS COMBO-BOX INNER-LINES 30
     LIST-ITEM-PAIRS "1234567890123456789012345","1"
     DROP-DOWN AUTO-COMPLETION UNIQUE-MATCH
     SIZE 35 BY 1 TOOLTIP "D�p�t (CTRL-F4)"
     BGCOLOR 16 FGCOLOR 23 FONT 49 NO-UNDO.

DEFINE VARIABLE typ-cde$ AS CHARACTER FORMAT "X(30)":U
     LABEL "Type"
     VIEW-AS COMBO-BOX INNER-LINES 4
     DROP-DOWN-LIST
     SIZE 32.57 BY 1
     BGCOLOR 16 FGCOLOR 23 FONT 2 NO-UNDO.

DEFINE VARIABLE cli$ AS CHARACTER FORMAT "X(30)":U
     LABEL "Client"
     VIEW-AS FILL-IN
     SIZE 12.57 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE dat-cde$ AS DATE FORMAT "99/99/99":U
     LABEL "&Saisie le"
     VIEW-AS FILL-IN
     SIZE 10.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE dat-liv$ AS DATE FORMAT "99/99/99":U
     LABEL "Date livraison"
     VIEW-AS FILL-IN
     SIZE 7.86 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE Ft$selection-1 AS CHARACTER FORMAT "X(255)":U INITIAL "S�lection"
      VIEW-AS TEXT
     SIZE 15.29 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE lib-cli AS CHARACTER FORMAT "X(35)":U
     VIEW-AS FILL-IN
     SIZE 30 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-mg AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 30 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE nat$ AS CHARACTER FORMAT "X(3)"
     LABEL "Nature"
     VIEW-AS FILL-IN
     SIZE 5.14 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE not-ref$ AS CHARACTER FORMAT "X(15)":U
     LABEL "Notre r�f."
     VIEW-AS FILL-IN
     SIZE 16 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE qui$ AS CHARACTER FORMAT "X(10)"
     LABEL "Saisi par"
     VIEW-AS FILL-IN
     SIZE 12.57 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE ref-cde$ AS CHARACTER FORMAT "X(30)":U
     LABEL "R�f. client"
     VIEW-AS FILL-IN
     SIZE 16 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE sem$ AS INTEGER FORMAT ">>":U INITIAL 52
     VIEW-AS FILL-IN
     SIZE 2.86 BY .79 TOOLTIP "ou semaine de livraison"
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 27.14 BY 1.08.

DEFINE RECTANGLE RECT-25
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 113.14 BY 2.63.

DEFINE RECTANGLE RECT-29
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 113.14 BY 2.58.

DEFINE VARIABLE Tbdevis AS LOGICAL INITIAL no
     LABEL "&Afficher les devis termin�s"
     VIEW-AS TOGGLE-BOX
     SIZE 21.72 BY .63
     FONT 49 NO-UNDO.

DEFINE VARIABLE tvoir AS LOGICAL INITIAL no
     LABEL "Voir infos"
     VIEW-AS TOGGLE-BOX
     SIZE 10.43 BY .63 NO-UNDO.

DEFINE BUTTON bappdev  NO-FOCUS FLAT-BUTTON
     LABEL "Bajouter colonnes 2"
     SIZE 3.14 BY .92 TOOLTIP "Appliquer cette devise � toutes les lignes de la pi�ce".

DEFINE BUTTON bcreer-aff  NO-FOCUS FLAT-BUTTON
     LABEL "&Cr�er"
     SIZE 2.86 BY .88 TOOLTIP "Cr�er affaire"
     FONT 9.

DEFINE BUTTON bdate
     LABEL "&Autres dates (F7)"
     SIZE 13.29 BY .83 TOOLTIP "Dates de r�ception, de livraison demand�e et accord�e, de mise � dispo, ...".

DEFINE BUTTON BMODIF-aff  NO-FOCUS FLAT-BUTTON
     LABEL "&Modifier"
     SIZE 2.86 BY .88 TOOLTIP "Modifier affaire"
     FONT 9.

DEFINE BUTTON bpardev
     LABEL "&Changer param�tres"
     SIZE 16.57 BY .83.

DEFINE BUTTON bplanech
     LABEL "&Plan �ch."
     SIZE 10 BY .79.

DEFINE BUTTON br-aa
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-aff
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-ci
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-DE
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-ECH
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-fc
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-gc
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-kp
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-kv
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-LG
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-nc
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-oc
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-PA
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-reg
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-rg
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-rp1
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-rv
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-sc
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-sd
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-se
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-tar
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-tf
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-xt1
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-xt2
     LABEL "Brt2":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-xt3
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-xt4
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-xt5
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON bt$adresse-aff  NO-FOCUS FLAT-BUTTON
     LABEL ""
     SIZE 2.86 BY .88 TOOLTIP "Choisir une adresse affaire"
     FONT 9.

DEFINE BUTTON btn-materiel
     LABEL "btn-materiel"
     SIZE 2.86 BY .88.

DEFINE VARIABLE edepot AS CHARACTER INITIAL "0"
     LABEL "D�p�t"
     VIEW-AS COMBO-BOX INNER-LINES 30
     LIST-ITEM-PAIRS "1234567890123456789012345","1"
     DROP-DOWN AUTO-COMPLETION UNIQUE-MATCH
     SIZE 24.29 BY 1 TOOLTIP "D�p�t (CTRL-F4)"
     BGCOLOR 16 FGCOLOR 23 FONT 49 NO-UNDO.

DEFINE VARIABLE Fhie1 AS CHARACTER FORMAT "X(255)":U INITIAL "�"
      VIEW-AS TEXT
     SIZE 3 BY .75
     FONT 41 NO-UNDO.

DEFINE VARIABLE FHie2 AS CHARACTER FORMAT "X(255)":U INITIAL "�"
      VIEW-AS TEXT
     SIZE 3 BY .75
     FONT 41 NO-UNDO.

DEFINE VARIABLE FHie3 AS CHARACTER FORMAT "X(255)":U INITIAL "�"
      VIEW-AS TEXT
     SIZE 3 BY .75
     FONT 41 NO-UNDO.

DEFINE VARIABLE fill-materiel AS CHARACTER FORMAT "X(17)":U
     LABEL "Mat�riel"
     VIEW-AS FILL-IN
     SIZE 18 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE Ft$selection-c1 AS CHARACTER FORMAT "X(255)":U INITIAL "Adresse client"
      VIEW-AS TEXT
     SIZE 22.14 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE Ft$selection-c2 AS CHARACTER FORMAT "X(255)":U INITIAL "Informations diverses"
      VIEW-AS TEXT
     SIZE 25.43 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE lib-aa AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 27 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-aff AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 20.43 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-CDE-CI AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-CDE-PA AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-CI AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-ECH AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-fc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-gc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-kv AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 27 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-LG AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-materiel AS CHARACTER FORMAT "X(35)":U
     VIEW-AS FILL-IN
     SIZE 21 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-nc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 27 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-oc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 27 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-PA AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-REG AS CHARACTER FORMAT "X(35)":U
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-rg AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-rp1 AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 27 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-rv AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-sc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-sd AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-se AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-tf AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-xt1 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-xt2 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-xt3 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-xt4 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-xt5 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE xa1 AS CHARACTER FORMAT "X(35)" INITIAL "qpj45678901234567890123456789012345"
     LABEL "za1"
     VIEW-AS FILL-IN
     SIZE 31 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xa2 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za2"
     VIEW-AS FILL-IN
     SIZE 31 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xa3 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za3"
     VIEW-AS FILL-IN
     SIZE 31 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xa4 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za4"
     VIEW-AS FILL-IN
     SIZE 31 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xa5 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za5"
     VIEW-AS FILL-IN
     SIZE 31 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xd1 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xd2 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xd3 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xd4 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xd5 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xn1 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xn2 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn2"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xn3 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn3"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xn4 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn4"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xn5 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn5"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xt1 AS CHARACTER FORMAT "X(5)"
     LABEL "zt1"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xt2 AS CHARACTER FORMAT "X(5)"
     LABEL "zt2"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xt3 AS CHARACTER FORMAT "X(5)"
     LABEL "zt3"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xt4 AS CHARACTER FORMAT "X(5)"
     LABEL "zt4"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE xt5 AS CHARACTER FORMAT "X(5)"
     LABEL "zt5"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE radio-adresse AS LOGICAL
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS
          "Commande", yes,
"Facturation", no
     SIZE 25 BY .54 NO-UNDO.

DEFINE RECTANGLE RECT-242
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 59.43 BY .08.

DEFINE RECTANGLE RECT-243
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 59.43 BY .08.

DEFINE RECTANGLE RECT-244
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 49.43 BY .08.

DEFINE RECTANGLE RECT-245
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 59.43 BY .08.

DEFINE RECTANGLE RECT-246
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 59.43 BY .08.

DEFINE RECTANGLE RECT-256
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 111.86 BY 1.08.

DEFINE RECTANGLE RECT-257
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 51 BY 17.21.

DEFINE RECTANGLE RECT-258
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 60.72 BY 17.21.

DEFINE VARIABLE tConsign AS LOGICAL INITIAL no
     LABEL "Stock de consignation"
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .58 NO-UNDO.

DEFINE VARIABLE TMultiech AS LOGICAL INITIAL no
     LABEL "Ech mult."
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .58 NO-UNDO.

DEFINE VARIABLE xl1 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 18.57 BY .63 NO-UNDO.

DEFINE VARIABLE xl2 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 18.57 BY .63 NO-UNDO.

DEFINE VARIABLE xl3 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 18.57 BY .63 NO-UNDO.

DEFINE VARIABLE xl4 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 18.57 BY .63 NO-UNDO.

DEFINE VARIABLE xl5 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 18.57 BY .63 NO-UNDO.

DEFINE BUTTON br-ct
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-gpres
     LABEL "br ss 2"
     SIZE 2.86 BY .88.

DEFINE BUTTON br-rp2
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-ss
     LABEL "Button 1"
     SIZE 2.86 BY .88.

DEFINE BUTTON brcchq
     LABEL "Button 5"
     SIZE 2.86 BY .88.

DEFINE BUTTON brcfact
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcfactor
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcgrp
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brclivre
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcpaye
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcpln
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcplr
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON brcstat
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE BUTTON btn-contratloc
     LABEL "Btn 1"
     SIZE 2.86 BY .88.

DEFINE BUTTON btn-lancement
     LABEL "":L
     SIZE 2.86 BY .88.

DEFINE VARIABLE Fborne AS CHARACTER FORMAT "X(255)":U INITIAL "Borne minimale de remise / quantit�:"
      VIEW-AS TEXT
     SIZE 25 BY .63 NO-UNDO.

DEFINE VARIABLE fill-contratloc AS CHARACTER FORMAT "X(17)":U
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE fill-lancement AS CHARACTER FORMAT "X(12)":U
     LABEL "Lancement"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE Ft$selection-c3 AS CHARACTER FORMAT "X(255)":U INITIAL "Informations diverses"
      VIEW-AS TEXT
     SIZE 24.43 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE Ft$selection-c4 AS CHARACTER FORMAT "X(255)":U INITIAL "Fili�re client"
      VIEW-AS TEXT
     SIZE 17 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE lib-cchq AS CHARACTER FORMAT "X(256)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-contratloc AS CHARACTER FORMAT "X(35)":U
     VIEW-AS FILL-IN
     SIZE 30 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-ct AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-fact AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-factor AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-gpres AS CHARACTER FORMAT "X(256)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-grp AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-livre AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-paye AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-pln AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-plr AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-rp2 AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-ss AS CHARACTER FORMAT "X(256)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-stat AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE vcl_fact AS CHARACTER FORMAT "X(30)":U
     LABEL "Factur� �"
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_grp AS CHARACTER FORMAT "X(30)":U
     LABEL "Groupement"
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_livre AS CHARACTER FORMAT "X(30)":U
     LABEL "Livr� �"
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_paye AS CHARACTER FORMAT "X(30)":U
     LABEL "Pay� par"
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_pln AS CHARACTER FORMAT "X(30)":U
     LABEL "Plateforme nat."
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_plr AS CHARACTER FORMAT "X(30)":U
     LABEL "Plateforme r�g."
     VIEW-AS FILL-IN
     SIZE 6.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vcl_stat AS CHARACTER FORMAT "X(30)":U
     LABEL "Client statistique"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE vc_factor AS CHARACTER FORMAT "X(30)":U
     LABEL "Client affacturage"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE radio-contrat AS CHARACTER
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS
          "de maintenance", "M",
"de location", "L"
     SIZE 13.29 BY 1.5 NO-UNDO.

DEFINE RECTANGLE RECT-260
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 76 BY 6.75.

DEFINE RECTANGLE RECT-261
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 35.86 BY 18.29.

DEFINE RECTANGLE RECT-262
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 76 BY 11.46.

DEFINE VARIABLE toggle-contrat AS LOGICAL INITIAL no
     LABEL "Associer contrat"
     VIEW-AS TOGGLE-BOX
     SIZE 14.57 BY .79.

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 27.14 BY 1.08.

DEFINE VARIABLE FILL-libmat AS CHARACTER FORMAT "X(255)":U
      VIEW-AS TEXT
     SIZE 13.43 BY .63
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE BUTTON badresse  NO-FOCUS FLAT-BUTTON
     LABEL "&Autres adresses"
     SIZE 2.72 BY .75 TOOLTIP "Choisir une autre adresse"
     FONT 49.

DEFINE BUTTON br-ciliv
     LABEL "br-ciliv":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-kpliv
     LABEL "BR-kpliv":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-paliv
     LABEL "br-paliv":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-tc
     LABEL "br ml 2":L
     SIZE 2.86 BY .88.

DEFINE BUTTON br-zg
     LABEL "Button 3":L
     SIZE 2.86 BY .88.

DEFINE VARIABLE ft$titre1 AS CHARACTER FORMAT "X(255)":U INITIAL "Adresse"
      VIEW-AS TEXT
     SIZE 12.29 BY .67
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE ft$titre2 AS CHARACTER FORMAT "X(255)":U INITIAL "Livraison"
      VIEW-AS TEXT
     SIZE 13.29 BY .67
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE lib-ge AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE lib-tc AS CHARACTER FORMAT "X(25)":U
     VIEW-AS FILL-IN
     SIZE 25 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 27.14 BY 1.08.

DEFINE RECTANGLE RECT-263
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 70.86 BY 3.79.

DEFINE RECTANGLE RECT-264
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL
     SIZE 70.86 BY 8.63.

DEFINE BUTTON BR-zt1
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-zt2
     LABEL "Brt2":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-zt3
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-zt4
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE BUTTON BR-zt5
     LABEL "Brt1":L
     SIZE 2.86 BY .88.

DEFINE VARIABLE LIB-zt1 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-zt2 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-zt3 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-zt4 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE LIB-zt5 AS CHARACTER FORMAT "X(25)":U INITIAL "1234567890123456789012345"
     VIEW-AS FILL-IN
     SIZE 26.29 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE za1 AS CHARACTER FORMAT "X(35)" INITIAL "qpj45678901234567890123456789012345"
     LABEL "za1"
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE za2 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za2"
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE za3 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za3"
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE za4 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za4"
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE za5 AS CHARACTER FORMAT "X(35)" INITIAL "12345678901234567890123456789012345"
     LABEL "za5"
     VIEW-AS FILL-IN
     SIZE 26 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zd1 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zd2 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zd3 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zd4 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zd5 AS DATE FORMAT "99/99/99"
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 7.72 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zn1 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn1"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zn2 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn2"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zn3 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn3"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zn4 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn4"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zn5 AS DECIMAL FORMAT "->>>>>>>>9.9999" INITIAL 0
     LABEL "zn5"
     VIEW-AS FILL-IN
     SIZE 11 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zt1 AS CHARACTER FORMAT "X(5)"
     LABEL "zt1"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zt2 AS CHARACTER FORMAT "X(5)"
     LABEL "zt2"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zt3 AS CHARACTER FORMAT "X(5)"
     LABEL "zt3"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zt4 AS CHARACTER FORMAT "X(5)"
     LABEL "zt4"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zt5 AS CHARACTER FORMAT "X(5)"
     LABEL "zt5"
     VIEW-AS FILL-IN
     SIZE 6 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE zl1 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 24.29 BY .63 NO-UNDO.

DEFINE VARIABLE zl2 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 24.29 BY .63 NO-UNDO.

DEFINE VARIABLE zl3 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 24.29 BY .63 NO-UNDO.

DEFINE VARIABLE zl4 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 24.29 BY .63 NO-UNDO.

DEFINE VARIABLE zl5 AS LOGICAL INITIAL no
     LABEL "123456789012345"
     VIEW-AS TOGGLE-BOX
     SIZE 24.29 BY .63 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY fonglet FOR
      entetcli SCROLLING.

DEFINE QUERY FRAME-adrliv FOR
      entetcli SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME FRAME-adrliv
     badresse AT ROW 3.5 COL 21 WIDGET-ID 46
     entetcli.adr_liv[1] AT ROW 4.29 COL 19.14 COLON-ALIGNED WIDGET-ID 58
          LABEL "Nom"
          VIEW-AS FILL-IN
          SIZE 40 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-ciliv AT ROW 5.13 COL 28.14 WIDGET-ID 48
     entetcli.civ_liv AT ROW 5.17 COL 19.14 COLON-ALIGNED WIDGET-ID 66
          LABEL "Civilit�"
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-CI AT ROW 5.17 COL 29 COLON-ALIGNED NO-LABEL WIDGET-ID 54 FORMAT "X(25)":U
          VIEW-AS FILL-IN
          SIZE 30.14 BY .79
          BGCOLOR 17 FGCOLOR 15
     entetcli.adr_liv[2] AT ROW 6 COL 19.14 COLON-ALIGNED WIDGET-ID 60
          LABEL "Adresse"
          VIEW-AS FILL-IN
          SIZE 40 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     entetcli.adr_liv[3] AT ROW 6.83 COL 19.14 COLON-ALIGNED NO-LABEL WIDGET-ID 62
          VIEW-AS FILL-IN
          SIZE 40 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     entetcli.adrliv4 AT ROW 7.67 COL 19.14 COLON-ALIGNED NO-LABEL WIDGET-ID 64
          VIEW-AS FILL-IN
          SIZE 40 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     BR-kpliv AT ROW 8.46 COL 28.29 WIDGET-ID 50
     entetcli.k_post2l AT ROW 8.5 COL 19.14 COLON-ALIGNED WIDGET-ID 68
          LABEL "C.P/Ville"
          VIEW-AS FILL-IN
          SIZE 7 BY .79 TOOLTIP "Vous devez saisir ici le d�but du Code Postal ou de la Ville pour rechercher"
          BGCOLOR 16 FGCOLOR 23
     entetcli.villel AT ROW 8.5 COL 29.14 COLON-ALIGNED NO-LABEL WIDGET-ID 72
          VIEW-AS FILL-IN
          SIZE 30 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-paliv AT ROW 9.29 COL 28.29 WIDGET-ID 52
     entetcli.paysl AT ROW 9.33 COL 19.14 COLON-ALIGNED WIDGET-ID 70
          LABEL "Pays"
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-PA AT ROW 9.33 COL 29.14 COLON-ALIGNED NO-LABEL WIDGET-ID 56 FORMAT "X(25)":U
          VIEW-AS FILL-IN
          SIZE 30 BY .79
          BGCOLOR 17 FGCOLOR 15
     entetcli.zon_geo AT ROW 10.25 COL 19.14 COLON-ALIGNED WIDGET-ID 78
          LABEL "Zone g�o."
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-zg AT ROW 10.25 COL 29.43 WIDGET-ID 74
     lib-ge AT ROW 10.25 COL 30.29 COLON-ALIGNED NO-LABEL WIDGET-ID 76
     br-tc AT ROW 13 COL 31.14 WIDGET-ID 42
     entetcli.typ_con AT ROW 13.04 COL 21 COLON-ALIGNED WIDGET-ID 44
          LABEL "Type conditionnement" FORMAT "X(4)"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-tc AT ROW 13.04 COL 32.29 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     entetcli.typ_veh AT ROW 13.92 COL 23.14 WIDGET-ID 12
          LABEL "Cat�gorie du v�hicule"
          VIEW-AS TOGGLE-BOX
          SIZE 18 BY .79
     ft$titre1 AT ROW 2.5 COL 1.72 NO-LABEL WIDGET-ID 16
     ft$titre2 AT ROW 11.63 COL 1.72 NO-LABEL WIDGET-ID 8
     RECT-12 AT ROW 1.04 COL 1.14 WIDGET-ID 4
     RECT-263 AT ROW 11.96 COL 1.14 WIDGET-ID 6
     RECT-264 AT ROW 2.88 COL 1.14 WIDGET-ID 14
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 21.43 ROW 3.71
         SIZE 74.43 BY 16.38
         FONT 49
         TITLE "Adresse Livraison" WIDGET-ID 100.

DEFINE FRAME Fval
     za1 AT ROW 1.54 COL 16.86 COLON-ALIGNED
     zd1 AT ROW 1.54 COL 68.72 COLON-ALIGNED
     za2 AT ROW 2.42 COL 16.86 COLON-ALIGNED
     zd2 AT ROW 2.42 COL 68.72 COLON-ALIGNED
     za3 AT ROW 3.29 COL 16.86 COLON-ALIGNED
     zd3 AT ROW 3.29 COL 68.72 COLON-ALIGNED
     za4 AT ROW 4.17 COL 16.86 COLON-ALIGNED
     zd4 AT ROW 4.17 COL 68.72 COLON-ALIGNED
     za5 AT ROW 5.04 COL 16.86 COLON-ALIGNED
     zd5 AT ROW 5.04 COL 68.72 COLON-ALIGNED
     zn1 AT ROW 5.96 COL 16.86 COLON-ALIGNED
     zl1 AT ROW 6.33 COL 70.86
     zn2 AT ROW 6.83 COL 16.86 COLON-ALIGNED
     zl2 AT ROW 7 COL 70.86
     zl3 AT ROW 7.67 COL 70.86
     zn3 AT ROW 7.71 COL 16.86 COLON-ALIGNED
     zl4 AT ROW 8.33 COL 70.86
     zn4 AT ROW 8.58 COL 16.86 COLON-ALIGNED
     zl5 AT ROW 9 COL 70.86
     zn5 AT ROW 9.46 COL 16.86 COLON-ALIGNED
     zt1 AT ROW 10.33 COL 16.86 COLON-ALIGNED
     BR-zt1 AT ROW 10.33 COL 25.14
     LIB-zt1 AT ROW 10.33 COL 26.14 COLON-ALIGNED NO-LABEL
     zt2 AT ROW 11.33 COL 16.86 COLON-ALIGNED
     BR-zt2 AT ROW 11.33 COL 25.14
     LIB-zt2 AT ROW 11.33 COL 26.14 COLON-ALIGNED NO-LABEL
     zt3 AT ROW 12.29 COL 16.86 COLON-ALIGNED
     BR-zt3 AT ROW 12.29 COL 25.14
     LIB-zt3 AT ROW 12.33 COL 26.14 COLON-ALIGNED NO-LABEL
     zt4 AT ROW 13.25 COL 16.86 COLON-ALIGNED
     BR-zt4 AT ROW 13.25 COL 25.14
     LIB-zt4 AT ROW 13.29 COL 26.14 COLON-ALIGNED NO-LABEL
     BR-zt5 AT ROW 14.17 COL 25.14
     zt5 AT ROW 14.21 COL 16.86 COLON-ALIGNED
     LIB-zt5 AT ROW 14.21 COL 26.14 COLON-ALIGNED NO-LABEL
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

DEFINE FRAME Fentete
     RECT-11 AT ROW 1.04 COL 1.14 WIDGET-ID 4
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1 ROW 1
         SIZE 113.43 BY 1.21
         FONT 49.

DEFINE FRAME Fc1
     bt$adresse-aff AT ROW 7.83 COL 58.14 WIDGET-ID 48
     bcreer-aff AT ROW 7.83 COL 51.29
     BMODIF-aff AT ROW 7.83 COL 54.72
     bappdev AT ROW 9.88 COL 30.72
     entetcli.dat_cde AT ROW 1.13 COL 2.57 COLON-ALIGNED
          LABEL "Le"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.dat_liv AT ROW 1.13 COL 22.57 COLON-ALIGNED
          LABEL "Date livraison"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.semaine AT ROW 1.13 COL 30.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 2.86 BY .79 TOOLTIP "ou semaine de livraison"
          BGCOLOR 16 FGCOLOR 23
     bdate AT ROW 1.13 COL 35.57
     entetcli.dat_px AT ROW 1.13 COL 52 COLON-ALIGNED
          LABEL "Prix"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.ref_cde AT ROW 1.13 COL 68.72 COLON-ALIGNED
          LABEL "R�f. client"
          VIEW-AS FILL-IN
          SIZE 17 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.not_ref AT ROW 1.13 COL 93.43 COLON-ALIGNED
          LABEL "Notre r�f."
          VIEW-AS FILL-IN
          SIZE 17 BY .79
          BGCOLOR 16 FGCOLOR 23
     radio-adresse AT ROW 2.17 COL 85 NO-LABEL
     edepot AT ROW 2.75 COL 13.72 COLON-ALIGNED
     br-ci AT ROW 2.75 COL 82.29
     entetcli.civ_fac AT ROW 2.79 COL 73.14 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.civ_cde AT ROW 2.79 COL 73.14 COLON-ALIGNED
          LABEL "Civilit�"
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 43 FGCOLOR 23
     LIB-CI AT ROW 2.79 COL 83.29 COLON-ALIGNED NO-LABEL
     LIB-CDE-CI AT ROW 2.79 COL 83.29 COLON-ALIGNED NO-LABEL
     tConsign AT ROW 2.96 COL 40.43
     entetcli.adr_cde[1] AT ROW 3.63 COL 73.14 COLON-ALIGNED
          LABEL "Nom"
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 43 FGCOLOR 23
     entetcli.adr_fac[1] AT ROW 3.63 COL 73.14 COLON-ALIGNED
          LABEL "Nom"
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-oc AT ROW 3.67 COL 19.57
     entetcli.ori_cde AT ROW 3.71 COL 13.72 COLON-ALIGNED
          LABEL "Origine"
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-oc AT ROW 3.71 COL 20.57 COLON-ALIGNED NO-LABEL
     entetcli.adr_cde[2] AT ROW 4.46 COL 73.14 COLON-ALIGNED
          LABEL "Adresse"
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 43 FGCOLOR 23 FONT 49
     entetcli.adr_fac[2] AT ROW 4.46 COL 73.14 COLON-ALIGNED
          LABEL "Adresse"
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     br-nc AT ROW 4.5 COL 19.57
     entetcli.nat_cde AT ROW 4.54 COL 13.72 COLON-ALIGNED
          LABEL "Nature"
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-nc AT ROW 4.54 COL 20.57 COLON-ALIGNED NO-LABEL
     entetcli.adr_cde[3] AT ROW 5.29 COL 73.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 43 FGCOLOR 23 FONT 49
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc1
     entetcli.adr_fac[3] AT ROW 5.29 COL 73.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     br-kv AT ROW 5.33 COL 19.57 WIDGET-ID 30
     entetcli.canal AT ROW 5.38 COL 13.72 COLON-ALIGNED WIDGET-ID 28
          LABEL "Canal"
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
     LIB-kv AT ROW 5.38 COL 20.57 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     entetcli.adr_cde[4] AT ROW 6.13 COL 73.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 43 FGCOLOR 23 FONT 49
     entetcli.adrfac4 AT ROW 6.13 COL 73.14 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 36 BY .79
          BGCOLOR 16 FGCOLOR 23 FONT 49
     br-rp1 AT ROW 6.17 COL 27.86
     entetcli.commerc[1] AT ROW 6.21 COL 13.72 COLON-ALIGNED
          LABEL "Commercial n� 1"
          VIEW-AS FILL-IN
          SIZE 12 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-rp1 AT ROW 6.21 COL 28.86 COLON-ALIGNED NO-LABEL
     BR-kp AT ROW 6.92 COL 82.29
     entetcli.k_post2f AT ROW 6.96 COL 73.14 COLON-ALIGNED
          LABEL "C.P/Ville"
          VIEW-AS FILL-IN
          SIZE 7 BY .79 TOOLTIP "Vous devez saisir ici le d�but du Code Postal ou de la Ville pour rechercher"
          BGCOLOR 16 FGCOLOR 23
     entetcli.k_post2c AT ROW 6.96 COL 73.14 COLON-ALIGNED
          LABEL "C.P/Ville"
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 43 FGCOLOR 23
     entetcli.villec AT ROW 6.96 COL 83.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 26 BY .79
          BGCOLOR 43 FGCOLOR 23
     entetcli.villef AT ROW 6.96 COL 83.29 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 26 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-aa AT ROW 7 COL 27.86
     entetcli.app_aff AT ROW 7.04 COL 13.72 COLON-ALIGNED
          LABEL "Apporteur d'affaires"
          VIEW-AS FILL-IN
          SIZE 12 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-aa AT ROW 7.04 COL 28.86 COLON-ALIGNED NO-LABEL
     BR-PA AT ROW 7.75 COL 82.29
     entetcli.paysc AT ROW 7.79 COL 73.14 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 43 FGCOLOR 23
     entetcli.paysf AT ROW 7.79 COL 73.14 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-PA AT ROW 7.79 COL 83.29 COLON-ALIGNED NO-LABEL
     LIB-CDE-PA AT ROW 7.79 COL 83.29 COLON-ALIGNED NO-LABEL
     br-aff AT ROW 7.83 COL 27.86
     entetcli.affaire AT ROW 7.88 COL 13.72 COLON-ALIGNED
          LABEL "Affaire"
          VIEW-AS FILL-IN
          SIZE 12 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-aff AT ROW 7.88 COL 28.86 COLON-ALIGNED NO-LABEL
     br-rg AT ROW 8.63 COL 82.43
     btn-materiel AT ROW 8.67 COL 33.72 WIDGET-ID 304
     entetcli.region AT ROW 8.67 COL 73.29 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-rg AT ROW 8.67 COL 83.43 COLON-ALIGNED NO-LABEL
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc1
     fill-materiel AT ROW 8.71 COL 13.72 COLON-ALIGNED WIDGET-ID 306
     lib-materiel AT ROW 8.71 COL 34.72 COLON-ALIGNED NO-LABEL WIDGET-ID 308
     BR-LG AT ROW 9.5 COL 82.43
     entetcli.langue AT ROW 9.54 COL 73.29 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-LG AT ROW 9.54 COL 83.43 COLON-ALIGNED NO-LABEL
     BR-tar AT ROW 9.88 COL 9.29
     BR-DE AT ROW 9.88 COL 27.57
     entetcli.no_tarif AT ROW 9.92 COL 4.14 COLON-ALIGNED
          LABEL "Tarif"
          VIEW-AS FILL-IN
          SIZE 3 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.devise AT ROW 9.92 COL 21.57 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 4 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.tx_ech_d AT ROW 9.92 COL 37.57 COLON-ALIGNED
          LABEL "Taux"
          VIEW-AS FILL-IN
          SIZE 8.72 BY .79 TOOLTIP "Taux de change (F4:Conversion invers�e)"
          BGCOLOR 16 FGCOLOR 23
     entetcli.fac_dvs AT ROW 10.04 COL 12.43
          LABEL "Factur� en"
          VIEW-AS TOGGLE-BOX
          SIZE 11 BY .63
     entetcli.prix_franco AT ROW 10.04 COL 49.72 WIDGET-ID 50
          LABEL "Prix franco"
          VIEW-AS TOGGLE-BOX
          SIZE 11 BY .54
     br-gc AT ROW 10.54 COL 82.43 WIDGET-ID 310
     entetcli.groupe AT ROW 10.58 COL 73.29 COLON-ALIGNED WIDGET-ID 314
          VIEW-AS FILL-IN
          SIZE 7 BY .79
     lib-gc AT ROW 10.58 COL 83.43 COLON-ALIGNED NO-LABEL WIDGET-ID 312
     entetcli.deb_loc AT ROW 11.13 COL 25.43 COLON-ALIGNED WIDGET-ID 294
          LABEL "du"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
     entetcli.fin_loc AT ROW 11.13 COL 36.57 COLON-ALIGNED WIDGET-ID 298
          LABEL "au"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
     entetcli.dur_loc AT ROW 11.13 COL 51.43 COLON-ALIGNED WIDGET-ID 296
          LABEL "Dur�e" FORMAT ">>>>9"
          VIEW-AS FILL-IN
          SIZE 6.57 BY .79
     entetcli.location AT ROW 11.21 COL 15.72 WIDGET-ID 302
          VIEW-AS TOGGLE-BOX
          SIZE 9.29 BY .63
     br-fc AT ROW 11.38 COL 82.43
     entetcli.famille AT ROW 11.42 COL 73.29 COLON-ALIGNED
          LABEL "Hi�rarchie"
          VIEW-AS FILL-IN
          SIZE 7 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-fc AT ROW 11.42 COL 83.43 COLON-ALIGNED NO-LABEL
     entetcli.cal_marg AT ROW 12.21 COL 1.57 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS
                    "Aucun calcul selon marge", " ":U,
"Valeur", "V":U,
"%/Prix vente", "P":U,
"Coeff/pb", "C":U
          SIZE 60 BY .63
     br-sc AT ROW 12.21 COL 82.43 WIDGET-ID 64
     entetcli.s2_famille AT ROW 12.25 COL 73.29 COLON-ALIGNED NO-LABEL WIDGET-ID 52
          VIEW-AS FILL-IN
          SIZE 7 BY .79
     lib-sc AT ROW 12.25 COL 83.43 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     entetcli.rem_glo[1] AT ROW 12.96 COL 17.57 COLON-ALIGNED
          LABEL "Remises pied"
          VIEW-AS FILL-IN
          SIZE 5.72 BY .79
          BGCOLOR 16 FGCOLOR 23
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc1
     entetcli.rem_glo[2] AT ROW 12.96 COL 23.72 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN
          SIZE 5.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     bpardev AT ROW 12.96 COL 31.72
     br-sd AT ROW 13.04 COL 82.43 WIDGET-ID 68
     entetcli.s3_famille AT ROW 13.08 COL 73.29 COLON-ALIGNED NO-LABEL WIDGET-ID 54
          VIEW-AS FILL-IN
          SIZE 7 BY .79
     lib-sd AT ROW 13.08 COL 83.43 COLON-ALIGNED NO-LABEL WIDGET-ID 70
     br-se AT ROW 13.88 COL 82.43 WIDGET-ID 72
     entetcli.s4_famille AT ROW 13.92 COL 73.29 COLON-ALIGNED NO-LABEL WIDGET-ID 56
          VIEW-AS FILL-IN
          SIZE 7 BY .79
     lib-se AT ROW 13.92 COL 83.43 COLON-ALIGNED NO-LABEL WIDGET-ID 74
     BR-reg AT ROW 14.04 COL 20.57
     entetcli.code_reg AT ROW 14.08 COL 14.72 COLON-ALIGNED
          LABEL "Mode r�glement"
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-REG AT ROW 14.08 COL 21.72 COLON-ALIGNED NO-LABEL
     TMultiech AT ROW 14.21 COL 50.29
     BR-ECH AT ROW 14.88 COL 20.57
     entetcli.code_ech AT ROW 14.92 COL 14.72 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-ECH AT ROW 14.92 COL 21.72 COLON-ALIGNED NO-LABEL
     bplanech AT ROW 14.92 COL 50.29
     xa1 AT ROW 15.67 COL 71 COLON-ALIGNED
     br-rv AT ROW 15.71 COL 20.57
     entetcli.regime AT ROW 15.75 COL 14.72 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-rv AT ROW 15.75 COL 21.57 COLON-ALIGNED NO-LABEL
     xl1 AT ROW 16 COL 64
     xd2 AT ROW 16.25 COL 41 COLON-ALIGNED
     xd3 AT ROW 16.25 COL 54 COLON-ALIGNED
     xn2 AT ROW 16.29 COL 58 COLON-ALIGNED
     xa2 AT ROW 16.42 COL 71 COLON-ALIGNED
     br-tf AT ROW 16.58 COL 20.57
     entetcli.type_fac AT ROW 16.63 COL 14.72 COLON-ALIGNED
          LABEL "Type facturation"
          VIEW-AS FILL-IN
          SIZE 3.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     LIB-tf AT ROW 16.63 COL 21.57 COLON-ALIGNED NO-LABEL
     xl2 AT ROW 16.67 COL 64
     xt3 AT ROW 16.96 COL 14.14 COLON-ALIGNED
     BR-xt3 AT ROW 16.96 COL 22.29
     xd1 AT ROW 17 COL 54 COLON-ALIGNED
     xt2 AT ROW 17 COL 64.29 COLON-ALIGNED
     BR-xt2 AT ROW 17 COL 72.57
     xn3 AT ROW 17.04 COL 22.57 COLON-ALIGNED
     LIB-xt3 AT ROW 17.04 COL 31.86 COLON-ALIGNED NO-LABEL
     LIB-xt2 AT ROW 17.08 COL 73.57 COLON-ALIGNED NO-LABEL
     xa3 AT ROW 17.17 COL 71 COLON-ALIGNED
     xn1 AT ROW 17.29 COL 59.43 COLON-ALIGNED
     xl3 AT ROW 17.29 COL 64
     xd5 AT ROW 17.71 COL 54 COLON-ALIGNED
     xn4 AT ROW 17.83 COL 14 COLON-ALIGNED
     xt4 AT ROW 17.83 COL 22.72 COLON-ALIGNED
     BR-xt4 AT ROW 17.83 COL 30.86
     LIB-xt4 AT ROW 17.88 COL 31.86 COLON-ALIGNED NO-LABEL
     xa4 AT ROW 17.92 COL 71 COLON-ALIGNED
     xl4 AT ROW 17.96 COL 64
     BR-xt1 AT ROW 18.25 COL 72.57
     xt1 AT ROW 18.29 COL 64.29 COLON-ALIGNED
     LIB-xt1 AT ROW 18.29 COL 73.57 COLON-ALIGNED NO-LABEL
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc1
     xd4 AT ROW 18.46 COL 54 COLON-ALIGNED
     xl5 AT ROW 18.58 COL 64
     xn5 AT ROW 18.63 COL 14 COLON-ALIGNED
     BR-xt5 AT ROW 18.67 COL 30.86
     xa5 AT ROW 18.67 COL 71 COLON-ALIGNED
     xt5 AT ROW 18.71 COL 22.72 COLON-ALIGNED
     LIB-xt5 AT ROW 18.71 COL 31.86 COLON-ALIGNED NO-LABEL
     Ft$selection-c2 AT ROW 2.13 COL 1.57 NO-LABEL WIDGET-ID 22
     Ft$selection-c1 AT ROW 2.13 COL 62.86 NO-LABEL WIDGET-ID 20
     Fhie1 AT ROW 12.29 COL 68 NO-LABEL WIDGET-ID 284
     FHie2 AT ROW 13.13 COL 69.72 NO-LABEL WIDGET-ID 286
     FHie3 AT ROW 13.92 COL 72.14 NO-LABEL WIDGET-ID 288
     RECT-242 AT ROW 12.04 COL 2
     RECT-243 AT ROW 9.63 COL 2
     RECT-256 AT ROW 1 COL 1.14 WIDGET-ID 4
     RECT-257 AT ROW 2.46 COL 62.14 WIDGET-ID 6
     RECT-258 AT ROW 2.46 COL 1.14 WIDGET-ID 8
     RECT-244 AT ROW 10.42 COL 62.86 WIDGET-ID 282
     RECT-245 AT ROW 13.88 COL 2 WIDGET-ID 290
     RECT-246 AT ROW 10.92 COL 2 WIDGET-ID 292
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

DEFINE FRAME fonglet
     entetcli.cod_cli AT ROW 1.21 COL 77.57 COLON-ALIGNED WIDGET-ID 2
          LABEL "Client"
           VIEW-AS TEXT
          SIZE 5.72 BY .63
          BGCOLOR 17 FGCOLOR 15
     entetcli.no_cde AT ROW 1.21 COL 87.57 COLON-ALIGNED WIDGET-ID 6
          LABEL "N�"
           VIEW-AS TEXT
          SIZE 9.72 BY .63
          BGCOLOR 17 FGCOLOR 15
     FILL-libmat AT ROW 1.21 COL 97.86 COLON-ALIGNED NO-LABEL WIDGET-ID 4
     entetcli.no_bl AT ROW 1.21 COL 102 COLON-ALIGNED WIDGET-ID 8
          LABEL "B.L"
           VIEW-AS TEXT
          SIZE 9.86 BY .63
          BGCOLOR 17 FGCOLOR 15
    WITH 1 DOWN NO-BOX OVERLAY
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1 ROW 2.21
         SIZE 113.43 BY 19.92
         FONT 49.

DEFINE FRAME f1
     Bfidelite AT ROW 2.88 COL 62.14 WIDGET-ID 22
     Bcrtcli AT ROW 2.88 COL 58.86
     brc-cli AT ROW 2.88 COL 25.72
     BR-nc$ AT ROW 2.88 COL 110
     cli$ AT ROW 2.92 COL 11 COLON-ALIGNED
     lib-cli AT ROW 2.92 COL 26.72 COLON-ALIGNED NO-LABEL
     nat$ AT ROW 2.92 COL 102.86 COLON-ALIGNED
     tvoir AT ROW 3.04 COL 78
     cdepot AT ROW 3.88 COL 76 COLON-ALIGNED
     BR-mg AT ROW 3.92 COL 25.72
     qui$ AT ROW 3.96 COL 11 COLON-ALIGNED
     LIB-mg AT ROW 3.96 COL 26.72 COLON-ALIGNED NO-LABEL
     dat-cde$ AT ROW 5.38 COL 11.29 COLON-ALIGNED
     typ-cde$ AT ROW 5.38 COL 34.43 COLON-ALIGNED
     dat-liv$ AT ROW 6.54 COL 11.29 COLON-ALIGNED
     sem$ AT ROW 6.54 COL 19.14 COLON-ALIGNED NO-LABEL
     ref-cde$ AT ROW 6.54 COL 34.43 COLON-ALIGNED
     not-ref$ AT ROW 6.54 COL 58.86 COLON-ALIGNED
     Tbdevis AT ROW 6.71 COL 77.29
     Ft$selection-1 AT ROW 2.25 COL 1.72 NO-LABEL WIDGET-ID 20
     RECT-25 AT ROW 2.46 COL 1.14
     RECT-29 AT ROW 5.08 COL 1.14
     RECT-10 AT ROW 1.04 COL 1.14 WIDGET-ID 4
    WITH 1 DOWN NO-BOX OVERLAY
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1 ROW 1
         SIZE 113.29 BY 21.17
         FONT 49.

DEFINE FRAME Fc2
     br-ct AT ROW 1.67 COL 57.43
     entetcli.num_tel AT ROW 1.71 COL 15.86 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 18.29 BY .79
          BGCOLOR 16 FGCOLOR 23
     entetcli.cat_tar AT ROW 1.71 COL 48.57 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 6.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-ct AT ROW 1.71 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.cat_cum AT ROW 1.88 COL 96.43 WIDGET-ID 28
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.num_fax AT ROW 2.58 COL 15.86 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 18.29 BY .79
          BGCOLOR 16 FGCOLOR 23
     brcgrp AT ROW 2.67 COL 57.43
     vcl_grp AT ROW 2.71 COL 48.57 COLON-ALIGNED
     lib-grp AT ROW 2.71 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.tar_fil[1] AT ROW 2.79 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
     entetcli.tar_cum[1] AT ROW 2.79 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.liasse AT ROW 3.46 COL 15.86 COLON-ALIGNED
          LABEL "Liasses facture"
          VIEW-AS FILL-IN
          SIZE 3 BY .79
          BGCOLOR 16 FGCOLOR 23
     brclivre AT ROW 3.54 COL 57.43
     vcl_livre AT ROW 3.58 COL 48.57 COLON-ALIGNED
     lib-livre AT ROW 3.58 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.tar_fil[2] AT ROW 3.67 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
     entetcli.tar_cum[2] AT ROW 3.67 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.taux_esc AT ROW 4.33 COL 15.86 COLON-ALIGNED
          VIEW-AS FILL-IN
          SIZE 5.43 BY .79
          BGCOLOR 16 FGCOLOR 23
     brcfact AT ROW 4.42 COL 57.43
     vcl_fact AT ROW 4.46 COL 48.57 COLON-ALIGNED
     lib-fact AT ROW 4.46 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.tar_fil[3] AT ROW 4.54 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
     entetcli.tar_cum[3] AT ROW 4.54 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     brcpaye AT ROW 5.29 COL 57.43
     vcl_paye AT ROW 5.33 COL 48.57 COLON-ALIGNED
     lib-paye AT ROW 5.33 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.gencod_det AT ROW 5.38 COL 2.29
          LABEL "Edition gencod d�tail"
          VIEW-AS TOGGLE-BOX
          SIZE 27.72 BY .67
     entetcli.tar_fil[4] AT ROW 5.42 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc2
     entetcli.tar_cum[4] AT ROW 5.42 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.fac_ttc AT ROW 6 COL 2.29
          LABEL "Facturation T.T.C"
          VIEW-AS TOGGLE-BOX
          SIZE 31.72 BY .63
     brcpln AT ROW 6.17 COL 57.43
     vcl_pln AT ROW 6.21 COL 48.57 COLON-ALIGNED
     lib-pln AT ROW 6.21 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.tar_fil[5] AT ROW 6.29 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
     entetcli.tar_cum[5] AT ROW 6.29 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.fac_pxa AT ROW 6.63 COL 2.29
          LABEL "Facturation prix achat"
          VIEW-AS TOGGLE-BOX
          SIZE 28.72 BY .63
     brcplr AT ROW 7.04 COL 57.43
     vcl_plr AT ROW 7.08 COL 48.57 COLON-ALIGNED
     lib-plr AT ROW 7.08 COL 58.43 COLON-ALIGNED NO-LABEL
     entetcli.tar_fil[6] AT ROW 7.17 COL 86
          LABEL "Client tarif"
          VIEW-AS TOGGLE-BOX
          SIZE 10 BY .63 TOOLTIP "Calcul du prix pour le client en cours et pour ce client tarif"
     entetcli.tar_cum[6] AT ROW 7.17 COL 96.43 WIDGET-ID 26
          LABEL "Cumuler conditions"
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .63 TOOLTIP "Lecture de ses conditions pour application de ses remises en cumul"
     entetcli.maj_ach AT ROW 7.25 COL 15.86 COLON-ALIGNED
          LABEL "% marge"
          VIEW-AS FILL-IN
          SIZE 6.72 BY .79
          BGCOLOR 16 FGCOLOR 23
     brcstat AT ROW 8.63 COL 61.72
     vcl_stat AT ROW 8.67 COL 48.57 COLON-ALIGNED
     lib-stat AT ROW 8.67 COL 62.72 COLON-ALIGNED NO-LABEL
     entetcli.proforma AT ROW 8.79 COL 2.29
          VIEW-AS TOGGLE-BOX
          SIZE 23.72 BY .63
     entetcli.fra_app AT ROW 9.42 COL 2.29
          VIEW-AS TOGGLE-BOX
          SIZE 22.72 BY .63
     brcfactor AT ROW 9.63 COL 61.72
     vc_factor AT ROW 9.67 COL 48.57 COLON-ALIGNED
     lib-factor AT ROW 9.67 COL 62.72 COLON-ALIGNED NO-LABEL
     entetcli.edt_arc AT ROW 10.67 COL 2.29
          VIEW-AS TOGGLE-BOX
          SIZE 18.14 BY .63
     br-rp2 AT ROW 11.13 COL 61.72
     entetcli.commerc[2] AT ROW 11.17 COL 48.57 COLON-ALIGNED
          LABEL "Commercial n� 2"
          VIEW-AS FILL-IN
          SIZE 11 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-rp2 AT ROW 11.17 COL 62.72 COLON-ALIGNED NO-LABEL
     entetcli.prep_isol AT ROW 12.29 COL 2.29 WIDGET-ID 2
          LABEL "Pr�paration isol�e"
          VIEW-AS TOGGLE-BOX
          SIZE 26.72 BY .63
     brcchq AT ROW 12.33 COL 61.72
     entetcli.cpt_bq AT ROW 12.38 COL 48.57 COLON-ALIGNED
          LABEL "Banque"
          VIEW-AS FILL-IN
          SIZE 11 BY .79 TOOLTIP "Compte banque pour les virements �trangers (adresse Swift)"
          BGCOLOR 16 FGCOLOR 23
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Fc2
     lib-cchq AT ROW 12.38 COL 62.72 COLON-ALIGNED NO-LABEL
     entetcli.plus_bpbl AT ROW 12.92 COL 2.29 WIDGET-ID 66
          LABEL "Commandes regroup�es sur B.P"
          VIEW-AS TOGGLE-BOX
          SIZE 32.86 BY .63
     btn-lancement AT ROW 13.46 COL 61.72
     fill-lancement AT ROW 13.54 COL 48.57 COLON-ALIGNED
     entetcli.liv_cpl AT ROW 13.88 COL 2.29 WIDGET-ID 2
          LABEL "Livraison commande compl�te"
          VIEW-AS TOGGLE-BOX
          SIZE 32.72 BY .63
     entetcli.cde_cpl AT ROW 14.5 COL 2.29 WIDGET-ID 2
          LABEL "Facturation commande compl�te"
          VIEW-AS TOGGLE-BOX
          SIZE 32.72 BY .63
     br-ss AT ROW 14.58 COL 56.29
     entetcli.saison AT ROW 14.63 COL 48.57 COLON-ALIGNED HELP
          ""
          LABEL "Saison"
          VIEW-AS FILL-IN
          SIZE 5.43 BY .79
          BGCOLOR 16 FGCOLOR 23
     lib-ss AT ROW 14.67 COL 61.86 COLON-ALIGNED NO-LABEL
     entetcli.bl_regr AT ROW 15.5 COL 2.29 WIDGET-ID 68
          LABEL "Commandes regroup�es sur B.L"
          VIEW-AS TOGGLE-BOX
          SIZE 30.86 BY .63
     entetcli.grp_ps AT ROW 15.63 COL 48.57 COLON-ALIGNED HELP
          ""
          LABEL "Groupe de PS" FORMAT "X(3)"
          VIEW-AS FILL-IN
          SIZE 5.43 BY .79
          BGCOLOR 16 FGCOLOR 23
     br-gpres AT ROW 15.63 COL 56.29
     lib-gpres AT ROW 15.71 COL 61.86 COLON-ALIGNED NO-LABEL
     entetcli.chif_bl AT ROW 16.13 COL 2.29
          VIEW-AS TOGGLE-BOX
          SIZE 31.72 BY .63
     radio-contrat AT ROW 16.75 COL 53.72 NO-LABEL WIDGET-ID 38
     entetcli.lias_bl AT ROW 16.79 COL 15.86 COLON-ALIGNED WIDGET-ID 70
          LABEL "Nombre de B.L"
          VIEW-AS FILL-IN
          SIZE 3 BY .79
     btn-contratloc AT ROW 16.96 COL 79.43 WIDGET-ID 34
     toggle-contrat AT ROW 17 COL 38.43 WIDGET-ID 30
     fill-contratloc AT ROW 17 COL 66.43 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     lib-contratloc AT ROW 17 COL 80.43 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     entetcli.tva_date AT ROW 17.83 COL 15.86 COLON-ALIGNED WIDGET-ID 72
          LABEL "Date TVA"
          VIEW-AS FILL-IN
          SIZE 8 BY .79 TOOLTIP "Date d'application de la TVA"
     entetcli.borne AT ROW 18.54 COL 64.86 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS
                    "1", 1,
"2", 2,
"3", 3,
"4", 4,
"5", 5,
"6", 6,
"7", 7,
"8", 8,
"9", 9,
"Aucune", 0
          SIZE 46.29 BY .63
     entetcli.dat_ech AT ROW 18.67 COL 15.86 COLON-ALIGNED
          LABEL "Date �ch�ance"
          VIEW-AS FILL-IN
          SIZE 8 BY .79
          BGCOLOR 16 FGCOLOR 23
     Ft$selection-c3 AT ROW 1.04 COL 1.57 NO-LABEL WIDGET-ID 22
     Ft$selection-c4 AT ROW 1.04 COL 38 NO-LABEL WIDGET-ID 20
     Fborne AT ROW 18.54 COL 39.29 NO-LABEL
     RECT-260 AT ROW 1.38 COL 37.29 WIDGET-ID 6
     RECT-261 AT ROW 1.38 COL 1.14 WIDGET-ID 8
     RECT-262 AT ROW 8.21 COL 37.29 WIDGET-ID 24
    WITH 1 DOWN NO-BOX
         SIDE-LABELS NO-UNDERLINE THREE-D
         AT COL 1.57 ROW 3.29
         SIZE 112.43 BY 18.71
         FGCOLOR 23 FONT 49.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wsaicl1 ASSIGN
         HIDDEN             = YES
         TITLE              = "Gestion"
         COLUMN             = 1.29
         ROW                = 2.54
         HEIGHT             = 21.17
         WIDTH              = 113.29
         MAX-HEIGHT         = 21.17
         MAX-WIDTH          = 113.43
         VIRTUAL-HEIGHT     = 21.17
         VIRTUAL-WIDTH      = 113.43
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         FONT               = 49
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wsaicl1
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f1
   NOT-VISIBLE FRAME-NAME                                               */
/* SETTINGS FOR BUTTON BR-mg IN FRAME f1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-nc$ IN FRAME f1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brc-cli IN FRAME f1
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN Ft$selection-1 IN FRAME f1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN lib-cli IN FRAME f1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-mg IN FRAME f1
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME Fc1
                                                                        */
/* SETTINGS FOR FILL-IN entetcli.adrfac4 IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_cde[1] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_cde[2] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_cde[4] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_fac[1] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_fac[2] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.affaire IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.app_aff IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR BUTTON br-aa IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-aff IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-ci IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-DE IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-fc IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-gc IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-kp IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-kv IN FRAME Fc1
   NO-DISPLAY                                                           */
ASSIGN
       br-kv:PRIVATE-DATA IN FRAME Fc1     =
                "KV".

/* SETTINGS FOR BUTTON BR-LG IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-nc IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-oc IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-PA IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-rg IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-rp1 IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-rv IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-sc IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-sd IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-se IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-tar IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-tf IN FRAME Fc1
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN entetcli.canal IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.civ_cde IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.code_reg IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.commerc[1] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.dat_cde IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.dat_liv IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.dat_px IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.deb_loc IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.dur_loc IN FRAME Fc1
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX entetcli.fac_dvs IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.famille IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Fhie1 IN FRAME Fc1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN FHie2 IN FRAME Fc1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN FHie3 IN FRAME Fc1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN entetcli.fin_loc IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Ft$selection-c1 IN FRAME Fc1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Ft$selection-c2 IN FRAME Fc1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN entetcli.k_post2c IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.k_post2f IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN lib-aa IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-aff IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-CDE-CI IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-CDE-PA IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-CI IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-fc IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-gc IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-kv IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-LG IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-nc IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-oc IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-PA IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-rg IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-rp1 IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-sc IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-sd IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-se IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-tf IN FRAME Fc1
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN entetcli.nat_cde IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.not_ref IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.no_tarif IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.ori_cde IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.prix_franco IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.ref_cde IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.rem_glo[1] IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.tx_ech_d IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.type_fac IN FRAME Fc1
   EXP-LABEL                                                            */
/* SETTINGS FOR FRAME Fc2
                                                                        */
/* SETTINGS FOR TOGGLE-BOX entetcli.bl_regr IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR BUTTON br-ct IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-gpres IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-rp2 IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON br-ss IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcfact IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcfactor IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcgrp IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brclivre IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcpaye IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcpln IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcplr IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON brcstat IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON btn-lancement IN FRAME Fc2
   NO-DISPLAY                                                           */
/* SETTINGS FOR TOGGLE-BOX entetcli.cat_cum IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.cde_cpl IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.commerc[2] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.cpt_bq IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.dat_ech IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.fac_pxa IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.fac_ttc IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Fborne IN FRAME Fc2
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Ft$selection-c3 IN FRAME Fc2
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Ft$selection-c4 IN FRAME Fc2
   ALIGN-L                                                              */
/* SETTINGS FOR TOGGLE-BOX entetcli.gencod_det IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.grp_ps IN FRAME Fc2
   EXP-LABEL EXP-FORMAT EXP-HELP                                        */
/* SETTINGS FOR FILL-IN entetcli.liasse IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.lias_bl IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN lib-ct IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-fact IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-factor IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-grp IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-livre IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-paye IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-pln IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-plr IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-rp2 IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN lib-stat IN FRAME Fc2
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.liv_cpl IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.maj_ach IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.plus_bpbl IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.prep_isol IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.saison IN FRAME Fc2
   EXP-LABEL EXP-HELP                                                   */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[1] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[2] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[3] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[4] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[5] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_cum[6] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[1] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[2] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[3] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[4] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[5] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX entetcli.tar_fil[6] IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.tva_date IN FRAME Fc2
   EXP-LABEL                                                            */
/* SETTINGS FOR FRAME Fentete
   NOT-VISIBLE                                                          */
/* SETTINGS FOR FRAME fonglet
                                                                        */
/* SETTINGS FOR FILL-IN entetcli.cod_cli IN FRAME fonglet
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.no_bl IN FRAME fonglet
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.no_cde IN FRAME fonglet
   EXP-LABEL                                                            */
/* SETTINGS FOR FRAME FRAME-adrliv
                                                                        */
ASSIGN
       FRAME FRAME-adrliv:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN entetcli.adr_liv[1] IN FRAME FRAME-adrliv
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.adr_liv[2] IN FRAME FRAME-adrliv
   NO-DISPLAY EXP-LABEL                                                 */
/* SETTINGS FOR FILL-IN entetcli.adr_liv[3] IN FRAME FRAME-adrliv
   NO-DISPLAY EXP-LABEL                                                 */
/* SETTINGS FOR BUTTON br-ciliv IN FRAME FRAME-adrliv
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-kpliv IN FRAME FRAME-adrliv
   NO-DISPLAY                                                           */
ASSIGN
       BR-kpliv:PRIVATE-DATA IN FRAME FRAME-adrliv     =
                "PA":U.

/* SETTINGS FOR BUTTON br-paliv IN FRAME FRAME-adrliv
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN entetcli.civ_liv IN FRAME FRAME-adrliv
   NO-DISPLAY EXP-LABEL                                                 */
/* SETTINGS FOR FILL-IN ft$titre1 IN FRAME FRAME-adrliv
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN ft$titre2 IN FRAME FRAME-adrliv
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN entetcli.k_post2l IN FRAME FRAME-adrliv
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN LIB-CI IN FRAME FRAME-adrliv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN LIB-PA IN FRAME FRAME-adrliv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN entetcli.paysl IN FRAME FRAME-adrliv
   NO-DISPLAY EXP-LABEL                                                 */
/* SETTINGS FOR FILL-IN entetcli.typ_con IN FRAME FRAME-adrliv
   EXP-LABEL EXP-FORMAT                                                 */
/* SETTINGS FOR TOGGLE-BOX entetcli.typ_veh IN FRAME FRAME-adrliv
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN entetcli.villel IN FRAME FRAME-adrliv
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN entetcli.zon_geo IN FRAME FRAME-adrliv
   EXP-LABEL                                                            */
/* SETTINGS FOR FRAME Fval
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wsaicl1)
THEN wsaicl1:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f1
/* Query rebuild information for FRAME f1
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME f1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME Fc1
/* Query rebuild information for FRAME Fc1
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME Fc1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME Fc2
/* Query rebuild information for FRAME Fc2
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME Fc2 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME Fentete
/* Query rebuild information for FRAME Fentete
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME Fentete */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fonglet
/* Query rebuild information for FRAME fonglet
     _TblList          = "GCO.entetcli"
     _Query            is NOT OPENED
*/  /* FRAME fonglet */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-adrliv
/* Query rebuild information for FRAME FRAME-adrliv
     _TblList          = "GCO.entetcli"
     _Query            is OPENED
*/  /* FRAME FRAME-adrliv */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME Fval
/* Query rebuild information for FRAME Fval
     _Query            is NOT OPENED
*/  /* FRAME Fval */
&ANALYZE-RESUME





/* ************************  Control Triggers  ************************ */

&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.affaire
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.affaire wsaicl1
ON LEAVE OF entetcli.affaire IN FRAME Fc1 /* Affaire */
OR RETURN OF entetcli.affaire
run return-affaire.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.app_aff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.app_aff wsaicl1
ON LEAVE OF entetcli.app_aff IN FRAME Fc1 /* Apporteur d'affaires */
OR RETURN OF entetcli.app_aff DO:
/*$2104...find tabgco where tabgco.TYPE_TAB = "MG" AND tabgco.fct_aa=YES and tabgco.A_TAB = input entetcli.app_aff no-lock no-error.*/
buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(input entetcli.app_aff) + " AND TPPERSONNE.FCT_AA = YES " ).
IF BUF-ID <> ? THEN assign lib-aa:SCREEN-VALUE = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).   /*...$2104*/
else lib-aa:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
  apply "tab" to br-aa.
  return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME badresse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL badresse wsaicl1
ON CHOOSE OF badresse IN FRAME FRAME-adrliv /* Autres adresses */
DO:
/*$A35969*/
    X = (IF INT(vcl_livre:SCREEN-VAL IN FRAME fc2)<>0 THEN INT(vcl_livre:SCREEN-VAL IN FRAME fc2) ELSE cle-cours).
    run recad_b ("C","L",X,entetcli.adr_liv[1]:screen-val in frame frame-adrliv,yes,output parlib, output parnum,output pcores).
    if parnum <> 0 then do:
        find adresse where adresse.typ_fich="C" and adresse.cod_tiers=X and adresse.affaire = "" and adresse.cod_adr=parnum no-lock no-error.
        find TABCOMP where TABCOMP.TYPE_TAB = "CI" and TABCOMP.A_TAB = adresse.civilite no-lock no-error.
        IF AVAILABLE tabcomp THEN DO:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
            IF intiTab <> "" THEN LIB-CI:screen-value = intiTab.
            ELSE LIB-CI:screen-value = TABCOMP.inti_tab.
            /*A35891*/
        END.
        else LIB-CI:screen-value = Traduction("Inexistant",-2,"").

        find TABCOMP where TABCOMP.TYPE_TAB = "PA" and TABCOMP.A_TAB = adresse.pays no-lock no-error.
        IF AVAILABLE tabcomp THEN DO:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
            IF intiTab <> "" THEN LIB-pa:screen-value = intiTab.
            ELSE LIB-pa:screen-value = TABCOMP.inti_tab.
            /*A35891*/
        END.
        else LIB-pa:screen-value = Traduction("Inexistant",-2,"").

        display
            adresse.civilite @ entetcli.civ_liv lib-ci adresse.nom_adr @ entetcli.adr_liv[1]
            adresse.adresse[1] @ entetcli.adr_liv[2] adresse.adresse[2] @ entetcli.adr_liv[3] adresse.adresse4 @ entetcli.adrliv4
            adresse.k_post2 @ entetcli.k_post2l adresse.ville @ entetcli.villel
            adresse.pays @ entetcli.paysl lib-pa adresse.zon_geo @ entetcli.zon_geo with frame frame-adrliv.
        COdePostalLiv = entetcli.k_post2l:SCREEN-VALUE.
        ASSIGN adrliv-transpor=(IF adresse.transpor=0 THEN client.transpor ELSE adresse.transpor)
               adrliv-MOD_liv=(IF adresse.MOD_liv="" THEN client.MOD_liv ELSE adresse.MOD_liv)
               adrliv-comment=adresse.comment
               adrliv-com_liv[1]=adresse.com_liv[1]
               adrliv-com_liv[2]=adresse.com_liv[2]
               adrliv-com_liv[3]=adresse.com_liv[3]
               adrliv-com_liv[4]=adresse.com_liv[4]
               adrliv-no_adr = adresse.cod_adr
               adrliv-zon_geo = adresse.zon_geo.

        IF adresse.transpor=0 AND pref_parcde_transpor AND AVAIL entetcli THEN DO:
            FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = entetcli.depot NO-LOCK NO-ERROR.
            IF AVAIL depcli AND depcli.transpor > 0 THEN adrliv-transpor = depcli.transpor.
        END.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME bcreer-aff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcreer-aff wsaicl1
ON CHOOSE OF bcreer-aff IN FRAME Fc1 /* Cr�er */
DO:
do with frame Fc1:
  run creer-aff.
  apply "entry" to entetcli.affaire.
  return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME Bfidelite
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Bfidelite wsaicl1
ON CHOOSE OF Bfidelite IN FRAME f1
DO:
    DEFINE VARIABLE Pnocarte AS INTEGER    NO-UNDO.
    DEFINE VARIABLE Pcadeau  AS LOGICAL    NO-UNDO.
    DEFINE VARIABLE Plstrow  AS CHARACTER  NO-UNDO.

    RUN RECFID_Z(OUTPUT parcod, OUTPUT parlib, OUTPUT Pnocarte, OUTPUT logbid).
    IF logbid THEN DO:
        IF Pnocarte > 0 THEN DO:
            /*IF NOT VALID-HANDLE(Hfidelite) THEN RUN infofid_fid PERSISTENT SET Hfidelite (THIS-PROCEDURE).
 *             IF VALID-HANDLE(Hfidelite) THEN RUN Affichage-info IN Hfidelite (Pnocarte, OUTPUT Pcadeau, OUTPUT Plstrow).*/
        END.
        /*IF Pcadeau THEN RUN APPLIQUE-REMISE.*/
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME BMODIF-aff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BMODIF-aff wsaicl1
ON CHOOSE OF BMODIF-aff IN FRAME Fc1 /* Modifier */
DO:
do with frame Fc1:
  run modif-aff.
  apply "entry" to entetcli.affaire.
  return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bpardev
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bpardev wsaicl1
ON CHOOSE OF bpardev IN FRAME Fc1 /* Changer param�tres */
run appel-parval.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bplanech
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bplanech wsaicl1
ON CHOOSE OF bplanech IN FRAME Fc1 /* Plan �ch. */
run echcli_c (input int(entetcli.cod_cli:screen-value in frame fonglet), input type-cours,
              input int(entetcli.no_cde:screen-value in frame fonglet),input int(entetcli.no_bl:screen-value in frame fonglet),
              input int(entetcli.code_ech:screen-value), input int(entetcli.code_reg:screen-value)).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME br-ciliv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-ciliv wsaicl1
ON CHOOSE OF br-ciliv IN FRAME FRAME-adrliv /* br-ciliv */
DO:
    /*$A35969*/
    run rectc_b ("CI", output parlib, output parcod).
    if parcod <> "" then do:
         /*A35891..*/
        IF g-langue <> "" THEN intiTab = getIntituleTableLangue ("CI", parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        assign
            entetcli.civ_liv:screen-value = parcod
            LIB-ci:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME BR-ECH
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-ECH wsaicl1
ON CHOOSE OF BR-ECH IN FRAME Fc1
DO:
    run recce_b (output parnum, output parlib).
    if parnum <> 0 then do:
        assign entetcli.code_ech:screen-value = string(parnum)
               LIB-ech:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME BR-kpliv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-kpliv wsaicl1
ON CHOOSE OF BR-kpliv IN FRAME FRAME-adrliv /* BR-kpliv */
DO:
    /*$A35969*/
    parcod = entetcli.k_post2l:screen-value.
    run reccpv_b(input-o parcod,yes, yes, output parlib).
    if parlib<>"" then do:
        assign
            entetcli.villel:screen-value = parlib
            entetcli.k_post2l:screen-value=parcod
            CodePostalLiv = entetcli.k_post2l:screen-value.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME BR-mg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-mg wsaicl1
ON CHOOSE OF BR-mg IN FRAME f1
DO:
    /*$2104...run recmg_b (5, output parlib, output parcod).*/
    parcod = "".
    RUN VALUE(TPS_CHEMINPGM("recintequ_tps"))("", "I", NO, "tppersonne.vente = YES":U, ?, ?, ?, ?, ?, INPUT-OUTPUT parcod, OUTPUT parlib).
    if parcod <> "" then do:
        assign qui$:screen-value = parcod
               LIB-mg:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BR-nc$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-nc$ wsaicl1
ON CHOOSE OF BR-nc$ IN FRAME f1
DO:
    run rectg_b ("NC", output parlib, output parcod).
    if parcod <> "" then do:
        /*A35891..*/
        IF g-langue <> "" THEN intiTab = getIntituleTableLangue ("NC", parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        nat$:screen-value = parcod.
        run openquery.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME br-paliv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-paliv wsaicl1
ON CHOOSE OF br-paliv IN FRAME FRAME-adrliv /* br-paliv */
DO:
    /*$A35969*/
    run rectc_b ("PA":U, output parlib, output parcod).
    if parcod <> "" then do:
         /*A35891..*/
        IF g-langue <> "" THEN intiTab = getIntituleTableLangue ("PA":U, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        assign
            entetcli.paysl:screen-value = parcod
            LIB-pa:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME BR-reg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-reg wsaicl1
ON CHOOSE OF BR-reg IN FRAME Fc1
DO:
    run recmr_b (true, output parnum, output parlib).
    if parnum <> 0 then do:
        assign entetcli.code_reg:screen-value = string(parnum)
               LIB-reg:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME br-tc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-tc wsaicl1
ON CHOOSE OF br-tc IN FRAME FRAME-adrliv /* br ml 2 */
DO:
    /*$A35969*/
    DEF VAR lstparc AS CHAR NO-UNDO.
    DEF VAR iparc AS INT NO-UNDO.

    IF entetcli.typ_veh:CHECKED THEN DO:
        parcod = "".
        lstparc = "".
        DO iparc = 1 TO 9:
            IF parsoc.nom_mat[iparc] <> "" THEN lstparc = lstparc + (IF lstparc = "" THEN "" ELSE ",") + STRING(iparc).
        END.
        RUN reccat_at (lstparc, OUTPUT parlib, OUTPUT parcod).
        IF parcod <> "" THEN
            ASSIGN
                entetcli.typ_con:screen-value = parcod
                LIB-tc:screen-value = parlib.
    END.
    ELSE DO:
        run rectg_b ("TC", output parlib, output parcod).
        if parcod <> "" then do:
            /*A35891..*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("TC", parcod, g-langue).
            IF intiTab <> "" THEN parlib = intiTab.
            /*...A35891*/
            assign
                entetcli.typ_con:screen-value = parcod
                LIB-tc:screen-value = parlib.
          apply "TAB" TO SELF.
        END.
    END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME br-zg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-zg wsaicl1
ON CHOOSE OF br-zg IN FRAME FRAME-adrliv /* Button 3 */
DO:
run rectg_b ("GE", output parlib, output parcod).
if parcod <> "" then do:
    /*A35891..*/
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GE", parcod, g-langue).
    IF intiTab <> "" THEN parlib = intiTab.
    /*...A35891*/
    assign
        entetcli.zon_geo:screen-value = parcod
        LIB-ge:screen-value = parlib.
    apply "TAB" TO SELF.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME brc-cli
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brc-cli wsaicl1
ON CHOOSE OF brc-cli IN FRAME f1
DO:
parcod=input cli$.
run reccli_b ((if type-saisie="D" then "" else "C"),(if type-saisie="D" then "3,9" else if type-saisie="C" then "1,3,4,9" else "1,2,3,4,9"),input-output parcod, output parlib).
if parcod <> "" then do:
  assign
      cli$:screen-value = parcod
      LIB-cli:screen-value = parlib
      cli$=parcod
      logbid=session:set-wait-state("general").
  RUN CLI-YES.
  session:set-wait-state("").
  apply "entry" to self.
  apply "TAB" TO SELF.
END.
else apply "entry" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME bt$adresse-aff
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt$adresse-aff wsaicl1
ON CHOOSE OF bt$adresse-aff IN FRAME Fc1
DO:
    /* $A33761 */
    DEF VAR parnum-lieu AS INT NO-UNDO.
    DEF VAR nb_adresse AS INT NO-UNDO.

    ASSIGN nb_adresse = 0 parnum-lieu = 0.
    FOR EACH adresse WHERE adresse.typ_fich = "A" AND adresse.affaire = entetcli.affaire:SCREEN-VAL NO-LOCK:
        nb_adresse = nb_adresse + 1.
        IF nb_adresse = 1 THEN parnum-lieu = adresse.no_lieu.
    END.

    IF nb_adresse > 1 THEN DO:
        SESSION:SET-WAIT-STATE("general").
        parnum-lieu = 0.
        RUN reclieu_b ("A",entetcli.affaire:SCREEN-VAL,YES,"no_lieu","C", OUTPUT parnum, OUTPUT parnum-lieu, OUTPUT parlib).
    END.

    IF parnum-lieu <> 0 THEN DO:
        FIND adresse WHERE adresse.no_lieu = parnum-lieu NO-LOCK NO-ERROR.
        FIND TABCOMP WHERE TABCOMP.TYPE_TAB = "CI" AND TABCOMP.A_TAB = adresse.civilite NO-LOCK NO-ERROR.
        IF AVAILABLE tabcomp THEN DO:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
            IF intiTab <> "" THEN LIB-CI = intiTab.
            ELSE LIB-CI = TABCOMP.inti_tab.
            /*A35891*/
        END.
        else LIB-CI = Traduction("Inexistant",-2,"").
        FIND TABCOMP WHERE TABCOMP.TYPE_TAB = "PA" AND TABCOMP.A_TAB = adresse.pays NO-LOCK NO-ERROR.
        IF AVAILABLE tabcomp THEN DO:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
            IF intiTab <> "" THEN LIB-PA:screen-value = intiTab.
            ELSE LIB-PA:screen-value = TABCOMP.inti_tab.
            /*A35891*/
        END.
        else LIB-PA:screen-value = Traduction("Inexistant",-2,"").

        ASSIGN entetcli.num_tel:SCREEN-VAL IN FRAME fc2=adresse.num_tel
               entetcli.num_fax:SCREEN-VAL IN FRAME fc2=adresse.num_fax.
        DISPLAY adresse.civilite @ entetcli.civ_fac lib-ci adresse.nom_adr @ entetcli.adr_fac[1]
                adresse.adresse[1] @ entetcli.adr_fac[2] adresse.adresse[2] @ entetcli.adr_fac[3] adresse.adresse4 @ entetcli.adrfac4
                adresse.k_post2 @ entetcli.k_post2f adresse.ville @ entetcli.villef
                adresse.pays @ entetcli.paysf lib-pa WITH FRAME fc1.
        CodePostal = entetcli.k_post2f:SCREEN-VAL.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME btn-contratloc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-contratloc wsaicl1
ON CHOOSE OF btn-contratloc IN FRAME Fc2 /* Btn 1 */
DO:
    DO WITH FRAME Fc2:
        RUN reccl_lo("", "", "", 0, "N", fill-contratloc:SCREEN-VAL, "1", OUTPUT vContrat, OUTPUT vIndice, OUTPUT logbid, vTypeContrat).
        IF logbid THEN
        DO:
            FIND FIRST savcon WHERE savcon.contrat = vContrat AND savcon.indice = vIndice AND savcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
            IF AVAIL savcon THEN ASSIGN fill-contratloc:SCREEN-VAL = savcon.contrat
                                        fill-contratloc:PRIVATE-DATA = STRING(savcon.indice)
                                        lib-contratloc:SCREEN-VAL = savcon.lib_con.
            ELSE ASSIGN lib-contratloc:SCREEN-VAL = Traduction("Inexistant",-2,"").
            ASSIGN fill-contratloc.
        END.
        APPLY "TAB" TO btn-contratloc.
        RETURN NO-APPLY.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-lancement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-lancement wsaicl1
ON CHOOSE OF btn-lancement IN FRAME Fc2
DO:
    RUN reclan_gp (no,"n","v","",OUTPUT vchar, YES,0 /*$1114*/).
    IF vchar <> "" THEN fill-lancement:SCREEN-VAL = vchar.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME btn-materiel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-materiel wsaicl1
ON CHOOSE OF btn-materiel IN FRAME Fc1 /* btn-materiel */
DO:
    DEF VAR pcode AS CHAR INIT "" NO-UNDO.
    DEF VAR pinti AS CHAR NO-UNDO.
    DEF VAR wparc AS CHAR no-undo.
    wparc = "0".

    pcode = fill-materiel:SCREEN-VALUE.
    RUN recmat_at(INPUT-O wparc, lstparc, "1,9", "", "", entetcli.cod_cli:SCREEN-VALUE IN FRAME fonglet /* $A92146 */ , "", INPUT-OUTPUT pcode, OUTPUT pinti, NO).

    IF pinti <> "" THEN
    DO:
        FIND FIRST materiel WHERE materiel.typ_fich = wparc AND materiel.materiel = pcode NO-LOCK NO-ERROR.
        IF AVAIL materiel THEN ASSIGN fill-materiel:SCREEN-VALUE = pcode
                                      lib-materiel:SCREEN-VALUE = pinti
                                      fill-materiel
                                      vTypFich = materiel.typ_fich
                                      vMateriel = materiel.materiel
                                      vNoOrdre = materiel.no_ordre.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.cal_marg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.cal_marg wsaicl1
ON VALUE-CHANGED OF entetcli.cal_marg IN FRAME Fc1 /* Calcul marge (,P,V) */
run vc-marg.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.canal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.canal wsaicl1
ON LEAVE OF entetcli.canal IN FRAME Fc1 /* Canal */
OR RETURN OF entetcli.canal DO:
    find tabgco where tabgco.TYPE_TAB = "KV" and tabgco.A_TAB = input entetcli.canal no-lock no-error.
    if available tabgco then do:
        /*A35891... lib-kv:screen-value=tabgco.inti_tab. else lib-kv:screen-value=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("KV", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-kv:screen-value = intiTab.
        ELSE lib-kv:screen-value = tabgco.inti_tab.
    END.
    else lib-kv:screen-value = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-kv.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME entetcli.cat_tar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.cat_tar wsaicl1
ON LEAVE OF entetcli.cat_tar IN FRAME Fc2 /* Cat�gorie tarifaire */
OR RETURN OF entetcli.cat_tar DO:
find tabgco where tabgco.TYPE_TAB = "CT" and tabgco.A_TAB = input entetcli.cat_tar no-lock no-error.
if available tabgco then do:
        /*A35891... lib-ct:screen-value=tabgco.inti_tab. else lib-ct:screen-value=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CT", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ct:screen-value = intiTab.
        ELSE lib-ct:screen-value = tabgco.inti_tab.
END.
else lib-ct:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-ct.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.civ_fac
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.civ_fac wsaicl1
ON LEAVE OF entetcli.civ_fac IN FRAME Fc1 /* Civilit� */
OR RETURN OF entetcli.civ_fac DO:
    find TABCOMP where TABCOMP.TYPE_TAB = "CI" and TABCOMP.A_TAB = input entetcli.civ_fac no-lock no-error.
    /*A35891... if available TABCOMP then lib-ci:screen-value = TABCOMP.inti_tab.*/
    IF AVAILABLE tabcomp THEN DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ci:screen-value = intiTab.
        ELSE lib-ci:screen-value = TABCOMP.inti_tab.
        /*...A35891*/
    END.
    else lib-ci:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-ci.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME entetcli.civ_liv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.civ_liv wsaicl1
ON LEAVE OF entetcli.civ_liv IN FRAME FRAME-adrliv /* Civilit� */
OR RETURN OF entetcli.civ_liv DO:
    /*$A35969*/
    find TABCOMP where TABCOMP.TYPE_TAB = "CI" and TABCOMP.A_TAB = input entetcli.civ_liv no-lock no-error.
    /*A35891... if available TABCOMP then lib-ci:screen-value = TABCOMP.inti_tab.*/
    if available TABCOMP then DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ci:screen-value = intiTab.
        /*...A35891*/
        ELSE lib-ci:screen-value = TABCOMP.inti_tab.
    END.
    else lib-ci:screen-value = Traduction("Inexistant",-2,"").

    if last-event:function="return" then do:
        apply "tab" to br-ciliv.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME cli$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cli$ wsaicl1
ON RETURN OF cli$ IN FRAME f1 /* Client */
OR LEAVE OF cli$ DO:
if input cli$<>cli$ or (last-event:function="return" and input cli$="") then do:
    parcod=input cli$.
    run rechcli ((if type-saisie="D" then "" else "C"),(if type-saisie="D" then "3,9" else if type-saisie="C" then "1,3,4,9" else "1,2,3,4,9"),input-output parcod, output parlib).
    if parcod<>"" then do:
        assign
            cli$:screen-value = parcod
            LIB-cli:screen-value = parlib
            cli$=parcod
            logbid=session:set-wait-state("general").
        RUN CLI-YES.
        session:set-wait-state("").
        apply "tab" to brc-cli.
        return no-apply.
    end.
    else do:
        assign
            cli$
            LIB-cli:screen-value=Traduction("Inexistant",-2,"").
        RUN CLI-NO.
    end.
end.
else if last-event:function="return" and input cli$<>"" then do:
    apply "tab" to brc-cli.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.code_ech
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.code_ech wsaicl1
ON LEAVE OF entetcli.code_ech IN FRAME Fc1 /* Code �ch�ance */
OR RETURN OF entetcli.code_ech DO:
find condech where condech.ngr1=g-condech-ngr1 AND condech.ngr2=g-condech-ngr2 AND condech.ndos=g-condech-ndos AND condech.code_ech = input entetcli.code_ech no-lock no-error.
if available condech then LIB-ech:screen-value = condech.inti_ech.
else LIB-ech:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-ech.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.code_reg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.code_reg wsaicl1
ON LEAVE OF entetcli.code_reg IN FRAME Fc1 /* Mode r�glement */
OR RETURN OF entetcli.code_reg DO:
find MODEREG where modereg.ngr1=g-modereg-ngr1 AND modereg.ngr2=g-modereg-ngr2 AND modereg.ndos=g-modereg-ndos AND codet_reg and modereg.code_reg = input entetcli.code_reg no-lock no-error.
if available modereg then LIB-reg:screen-value = modereg.inti_reg.
else LIB-reg:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-reg.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.commerc[1]
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.commerc[1] wsaicl1
ON LEAVE OF entetcli.commerc[1] IN FRAME Fc1 /* Commercial n�1 */
OR RETURN OF entetcli.commerc[1] DO:
/*$2104...find tabgco where tabgco.TYPE_TAB = "MG" AND tabgco.fct_com=YES and tabgco.A_TAB = input entetcli.commerc[1] no-lock no-error.*/
buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(input entetcli.commerc[1]) + " AND TPPERSONNE.FCT_COM = YES ").
IF  buf-id <> ? THEN assign lib-rp1:screen-value = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).   /*...$2104*/
else lib-rp1:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-rp1.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME entetcli.commerc[2]
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.commerc[2] wsaicl1
ON LEAVE OF entetcli.commerc[2] IN FRAME Fc2 /* Commercial n�2 */
OR RETURN OF entetcli.commerc[2] DO:
/*$2104...find tabgco where tabgco.TYPE_TAB = "MG" AND tabgco.fct_com=YES and tabgco.A_TAB = input entetcli.commerc[2] no-lock no-error.*/
buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(input entetcli.commerc[2]) + " AND TPPERSONNE.FCT_COM = YES ").
IF  buf-id <> ? THEN assign lib-rp2:screen-value = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).   /*...$2104*/
else lib-rp2:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-rp2.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.deb_loc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.deb_loc wsaicl1
ON LEAVE OF entetcli.deb_loc IN FRAME Fc1 /* du */
OR LEAVE OF GCO.entetcli.fin_loc
OR LEAVE OF GCO.entetcli.dur_loc DO:
    SELF:BGC = 16.

    IF NOT SELF:MODIFIED THEN RETURN.
    SELF:MODIFIED = NO.
    IF SELF:NAME = "dur_loc" THEN DO:
        IF DATE(entetcli.deb_loc:SCREEN-VAL) <> ? THEN entetcli.fin_loc:SCREEN-VAL = STRING(DATE(entetcli.deb_loc:SCREEN-VAL) + (INT(SELF:SCREEN-VAL) /*- 1*/)).
        ELSE IF DATE(entetcli.fin_loc:SCREEN-VAL) <> ? THEN entetcli.deb_loc:SCREEN-VAL = STRING(DATE(entetcli.fin_loc:SCREEN-VAL) - (INT(SELF:SCREEN-VAL) /*- 1*/)).
    END.
    ELSE entetcli.dur_loc:SCREEN-VAL = (IF DATE(entetcli.fin_loc:SCREEN-VAL) <> ? AND DATE(entetcli.deb_loc:SCREEN-VAL) <> ? THEN STRING(MAX(DATE(entetcli.fin_loc:SCREEN-VAL) /*+ 1*/ - DATE(entetcli.deb_loc:SCREEN-VAL),0)) ELSE "0").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.devise
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.devise wsaicl1
ON LEAVE OF entetcli.devise IN FRAME Fc1 /* Devise */
OR RETURN OF entetcli.devise DO:
find TABCOMP where TABCOMP.type_tab = "DE" and TABCOMP.a_tab = input entetcli.devise no-lock no-error.
entetcli.tx_ech_d:hidden=yes.
if available TABCOMP then do:
    if tabcomp.a_tab<>g-dftdev and (tabcomp.dev_in=? or input entetcli.dat_px<tabcomp.dev_in) then do:
        run rch-tx-devise-vente.
        entetcli.tx_ech_d:hidden=no.
    end.
end.
if last-event:function="return" then do:
    apply "tab" to br-de.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edepot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edepot wsaicl1
ON VALUE-CHANGED OF edepot IN FRAME Fc1 /* D�p�t */
DO:
    IF self:TEXT-SELECTED THEN
    do:
        IF creation AND CAN-FIND (FIRST depsoc NO-LOCK)  /* $1110 */ /*�XREF_NOWHERE�*/
            AND NOT (parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/) THEN
        DO:
            MESSAGE Traduction("Vous avez chang� de d�p�t",-2,"") skip Traduction("Voulez-vous recalculer votre date de livraison ?",-2,"") + " "
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
            IF logbid = YES THEN
            DO:
                FIND FIRST depsoc WHERE depsoc.depot = INT (edepot:SCREEN-VAL) NO-LOCK NO-ERROR.
                dat-liv-ini=TODAY.
                IF CAN-DO("C,D",type-saisie) THEN DO:
                    IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN /*$979*/
                        run trt-calendar (0,year(dat-liv-ini),(IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv),input-output dat-liv-ini).
                    else dat-liv-ini=dat-liv-ini + (IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv).
                END.
                IF parsoc.sem_liv THEN sem-liv-ini=CalculSemaine(dat-liv-ini).
                assign
                    entetcli.dat_liv:SCREEN-VAL = STRING (dat-liv-ini)
                    entetcli.semaine:SCREEN-VAL = STRING (sem-liv-ini).
                IF CAN-DO ("D,C", type-cours) AND parsoc.gpao THEN
                DO:
                    dat_mad-ini = dat-liv-ini.
                    IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN DO: /*$979*/
                        dat_mad-ini = TODAY.
                        run trt-calendar (0,year(dat_mad-ini),(IF AVAIL depsoc AND depsoc.del_mad <> 0 THEN depsoc.del_mad ELSE pargp.DEL_mad),input-output dat_mad-ini).
                    END.
                    ecart_liv_mad = (IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv) - (IF AVAIL depsoc AND depsoc.del_mad <> 0 THEN depsoc.del_mad ELSE pargp.DEL_mad).
                    wdat_mad = dat_mad-ini.
                    wforcerdat_mad = YES. /*$A79284*/
                END.
            END.
        END.

        FIND tabgco WHERE tabgco.TYPE_tab = "DS" AND tabgco.n_tab = INT(edepot:SCREEN-VAL) NO-LOCK NO-ERROR.
        IF parsoc.ges_can AND parsoc.par_can="D":U THEN RUN affichage-canal.

        IF pref_parcde_commerc OR pref_parcde_groupe THEN DO:
            FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT (edepot:SCREEN-VAL) NO-LOCK NO-ERROR.

            IF pref_parcde_groupe THEN DO:
                IF AVAIL depcli AND depcli.groupe <> "" THEN entetcli.groupe:SCREEN-VAL IN FRAME fc1 = depcli.groupe.
                ELSE entetcli.groupe:SCREEN-VAL IN FRAME fc1 = client.groupe.
                find tabgco where tabgco.type_tab="GC":U and tabgco.a_tab=entetcli.groupe:SCREEN-VAL IN FRAME fc1 no-lock no-error.
                if available tabgco then do:
                    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GC":U, tabgco.a_tab, g-langue).
                    IF intiTab <> "" THEN lib-gc:screen-value IN FRAME Fc1 = intiTab.
                    ELSE lib-gc:screen-value IN FRAME Fc1 = tabgco.inti_tab.
                END.
                else lib-gc:screen-value IN FRAME Fc1=Traduction("Inexistant",-2,"").
            END.

            IF pref_parcde_commerc THEN DO:
                IF AVAIL depcli THEN DO:
                    IF depcli.commerc[1] <> "" THEN entetcli.commerc[1]:SCREEN-VAL IN FRAME Fc1 = depcli.commerc[1].
                                               ELSE entetcli.commerc[1]:SCREEN-VAL IN FRAME Fc1 = client.commerc[1].
                    IF depcli.commerc[2] <> "" THEN entetcli.commerc[2]:SCREEN-VAL IN FRAME fc2 = depcli.commerc[2].
                                               ELSE entetcli.commerc[2]:SCREEN-VAL IN FRAME fc2 = client.commerc[2].
                END.
                ELSE ASSIGN entetcli.commerc[1]:SCREEN-VAL IN FRAME fc1 = client.commerc[1]
                            entetcli.commerc[2]:SCREEN-VAL IN FRAME fc2 = client.commerc[2].

                /*$2104...find tabgco where tabgco.type_tab="MG" AND tabgco.fct_com=YES and tabgco.a_tab=entetcli.commerc[1]:screen-value IN FRAME Fc1 no-lock no-error.*/
                buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[1]:screen-value IN FRAME Fc1) + " AND TPPERSONNE.fct_com = YES ").
                IF  buf-id <> ? THEN assign lib-rp1:screen-value IN FRAME Fc1 = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).
                ELSE lib-rp1:screen-value IN FRAME Fc1 = Traduction("Inexistant",-2,"").
                /*find tabgco where tabgco.type_tab="MG" AND tabgco.fct_com=YES and tabgco.a_tab=entetcli.commerc[2]:screen-value IN FRAME Fc2 no-lock no-error.*/
                buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[2]:screen-value IN FRAME Fc2) + " AND TPPERSONNE.fct_com = YES ").
                IF  buf-id <> ? THEN assign lib-rp2:screen-value IN FRAME Fc2 = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).
                ELSE lib-rp2:screen-value IN FRAME Fc2 = Traduction("Inexistant",-2,"").   /*...$2104*/
            END.
        END.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.fac_dvs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.fac_dvs wsaicl1
ON VALUE-CHANGED OF entetcli.fac_dvs IN FRAME Fc1 /* Factur� en */
RUN vc-facdvs.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME entetcli.fac_pxa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.fac_pxa wsaicl1
ON VALUE-CHANGED OF entetcli.fac_pxa IN FRAME Fc2 /* Facturation prix achat */
entetcli.maj_ach:hidden=(input entetcli.fac_pxa="non").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.famille
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.famille wsaicl1
ON LEAVE OF entetcli.famille IN FRAME Fc1 /* Hi�rarchie */
OR RETURN OF entetcli.famille DO:
    find tabgco where tabgco.TYPE_TAB = "FC" and tabgco.A_TAB = input entetcli.famille no-lock no-error.
    if available tabgco then DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("FC", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-fc:screen-value = intiTab. /*...A35891*/
        ELSE lib-fc:screen-value = tabgco.inti_tab.
    END.
    else lib-fc:screen-value = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-fc.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME fill-contratloc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fill-contratloc wsaicl1
ON LEAVE OF fill-contratloc IN FRAME Fc2
OR RETURN OF fill-contratloc
DO:
    IF fill-contratloc:SCREEN-VAL <> fill-contratloc THEN
    DO:
        IF fill-contratloc:BGCOLOR = 31 THEN fill-contratloc:BGCOLOR = 16.
        FIND savcon WHERE savcon.contrat = fill-contratloc:SCREEN-VAL AND savcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
        IF AVAIL savcon THEN ASSIGN lib-contratloc:SCREEN-VAL = savcon.lib_con
                                    fill-contratloc:PRIVATE-DATA = STRING(savcon.indice).
        ELSE IF AMBIGUOUS savcon THEN
        DO:
            RUN reccl_lo("", "", "", 0, "N", fill-contratloc:SCREEN-VAL, "1", OUTPUT vContrat, OUTPUT vIndice, OUTPUT logbid, vTypeContrat).
            IF logbid THEN
            DO:
                FIND FIRST savcon WHERE savcon.contrat = vContrat AND savcon.indice = vIndice AND savcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
                IF AVAIL savcon THEN ASSIGN fill-contratloc:SCREEN-VAL = savcon.contrat
                                            fill-contratloc:PRIVATE-DATA = STRING(savcon.indice)
                                            lib-contratloc:SCREEN-VAL = savcon.lib_con.
                ELSE ASSIGN lib-contratloc:SCREEN-VAL = Traduction("Inexistant",-2,"").
            END.
            ELSE ASSIGN lib-contratloc:SCREEN-VAL = Traduction("Inexistant",-2,"").
        END.
        ELSE ASSIGN lib-contratloc:SCREEN-VAL = Traduction("Inexistant",-2,"").
        ASSIGN fill-contratloc.
        IF LAST-EVENT:FUNCTION = "RETURN" THEN
        DO:
            APPLY "TAB" TO btn-contratloc.
            RETURN NO-APPLY.
        END.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME fill-materiel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fill-materiel wsaicl1
ON LEAVE OF fill-materiel IN FRAME Fc1 /* Mat�riel */
OR RETURN OF fill-materiel IN FRAME fc1
DO:
DEF VAR wparc AS CHAR NO-UNDO. wparc = "0".

    fill-materiel:BGCOLOR = 16.

    IF INPUT fill-materiel <> fill-materiel THEN
    DO:
        ASSIGN fill-materiel = fill-materiel:SCREEN-VALUE
               parcod = INPUT fill-materiel
               vTypFich = ""
               vMateriel = ""
               vNoOrdre = 0.

        RUN rech-mat(INPUT-O wparc, lstparc, "1,9", entetcli.cod_cli:SCREEN-VALUE IN FRAME fonglet /* $A92146 */, "c", INPUT-OUTPUT parcod, OUTPUT parlib, LAST-EVENT:FUNCTION <> "RETURN").
        IF parcod <> "" THEN
        DO:
            FIND FIRST materiel WHERE materiel.typ_fich = wparc AND materiel.materiel = parcod NO-LOCK NO-ERROR.
            IF AVAIL materiel THEN ASSIGN fill-materiel:SCREEN-VALUE = parcod
                                          lib-materiel:SCREEN-VALUE = parlib
                                          fill-materiel = parcod
                                          vTypFich = materiel.typ_fich
                                          vMateriel = materiel.materiel
                                          vNoOrdre = materiel.no_ordre.
        END.
        ELSE
        DO:
            IF INPUT fill-materiel <> "" THEN ASSIGN fill-materiel
                                                     lib-materiel:SCREEN-VALUE = Traduction("Inexistant", -2, "").
            ELSE ASSIGN fill-materiel
                        lib-materiel:SCREEN-VALUE = Traduction("Tous", -2, "").
        END.
        APPLY "TAB" TO SELF.
        RETURN NO-APPLY.
    END.
    ELSE IF LAST-EVENT:FUNCTION = "RETURN" THEN
    DO:
        APPLY "TAB" TO SELF.
        RETURN NO-APPLY.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.groupe
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.groupe wsaicl1
ON LEAVE OF entetcli.groupe IN FRAME Fc1 /* Groupe */
OR RETURN OF entetcli.groupe DO:
    find tabgco where tabgco.TYPE_TAB = "GC":U and tabgco.A_TAB = input entetcli.groupe no-lock no-error.
    if available tabgco then DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GC":U, tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-gc:screen-value = intiTab. /*...A35891*/
        ELSE lib-gc:screen-value = tabgco.inti_tab.
    END.
    else lib-gc:screen-value = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-gc.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME entetcli.k_post2l
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.k_post2l wsaicl1
ON LEAVE OF entetcli.k_post2l IN FRAME FRAME-adrliv /* C.P/Ville */
DO:
  /*$A35969*/
  IF entetcli.k_post2l:SCREEN-VAL<>CodePostalLiv OR entetcli.villel:SCREEN-VAL = "" then do:
        parcod = entetcli.k_post2l:SCREEN-VAL.
        run reccpv_b (input-o parcod, NO, YES, output parlib).
        if parlib <> "" then assign
            entetcli.villel:SCREEN-VAL = parlib
            entetcli.k_post2l:SCREEN-VAL = parcod
            CodePostalLiv = parcod.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.langue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.langue wsaicl1
ON LEAVE OF entetcli.langue IN FRAME Fc1 /* Langue */
or return of entetcli.langue DO:
find TABCOMP where TABCOMP.type_tab = "LG" and TABCOMP.a_tab = input entetcli.LANGUE no-lock no-error.
IF AVAILABLE tabcomp THEN DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("LG", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-lg:screen-value = intiTab.
        ELSE lib-lg:screen-value = TABCOMP.inti_tab.
        /*A35891*/
END.
else lib-lg:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-lg.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.location
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.location wsaicl1
ON VALUE-CHANGED OF entetcli.location IN FRAME Fc1 /* Location */
RUN vc-location.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME nat$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL nat$ wsaicl1
ON RETURN OF nat$ IN FRAME f1 /* Nature */
DO:
   find tabgco where tabgco.type_tab = "NC" and tabgco.a_tab = input nat$ no-lock no-error.
   if available tabgco or nat$:screen-value="" then do:
       IF lgtextile THEN RUN SAISON-NATURE.
       run openquery.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.nat_cde
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.nat_cde wsaicl1
ON LEAVE OF entetcli.nat_cde IN FRAME Fc1 /* Nature */
OR RETURN OF entetcli.nat_cde DO:
find tabgco where tabgco.TYPE_TAB = "NC" and tabgco.A_TAB = input entetcli.nat_cde no-lock no-error.
if available tabgco then do:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("NC", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-nc:screen-value = intiTab.
    ELSE lib-nc:screen-value = tabgco.inti_tab.
    IF parsoc.ges_can AND parsoc.par_can="N":U THEN RUN affichage-canal.
    IF lgTextile THEN do:
        RUN SAISON-NATURE.
        RUN DATPX-SAISON.
    END.
END.
else lib-nc:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-nc.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.ori_cde
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.ori_cde wsaicl1
ON LEAVE OF entetcli.ori_cde IN FRAME Fc1 /* Origine */
OR RETURN OF entetcli.ori_cde DO:
find tabgco where tabgco.TYPE_TAB = "OC" and tabgco.A_TAB = input entetcli.ori_cde no-lock no-error.
if available tabgco then DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("OC", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-oc:screen-value = intiTab. /*A35891*/
    ELSE lib-oc:screen-value = tabgco.inti_tab.

    IF parsoc.ges_can AND parsoc.par_can="O":U THEN RUN affichage-canal.
END.
else lib-oc:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-oc.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.paysf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.paysf wsaicl1
ON LEAVE OF entetcli.paysf IN FRAME Fc1 /* Pays */
or return of entetcli.paysf DO:
find TABCOMP where TABCOMP.type_tab = "PA" and TABCOMP.a_tab = input entetcli.paysf no-lock no-error.
IF AVAILABLE tabcomp THEN DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-pa:screen-value = intiTab.
        ELSE lib-pa:screen-value = TABCOMP.inti_tab.
        /*A35891*/
END.
else lib-pa:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-pa.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME entetcli.paysl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.paysl wsaicl1
ON LEAVE OF entetcli.paysl IN FRAME FRAME-adrliv /* Pays */
OR RETURN OF entetcli.paysl DO:
    /*$A35969*/
    find tabcomp where tabcomp.type_tab = "PA" and tabcomp.a_tab = input entetcli.paysl no-lock no-error.
    IF AVAILABLE tabcomp THEN DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-pa:screen-value = intiTab.
        ELSE lib-pa:screen-value = TABCOMP.inti_tab.
        /*A35891*/
    END.
    else LIB-pa:screen-value = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-paLiv.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME qui$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qui$ wsaicl1
ON LEAVE OF qui$ IN FRAME f1 /* Saisi par */
OR RETURN OF qui$ DO:
    /*$2104...*/
    IF INPUT qui$ <> qui$ THEN DO:
        IF INPUT qui$ <> "" then do:
            ASSIGN parcod = input qui$  qui$.
            RUN rech-int IN G-htps-api("9", NO, "TPPERSONNE.VENTE = YES":U, ?, ?, ?, ?, ?, INPUT-OUTPUT parcod, OUTPUT parlib).
            IF parlib <> "" AND parlib <> ? THEN ASSIGN qui$:SCREEN-VALUE = parcod  qui$  LIB-mg:screen-value = parlib.
            ELSE ASSIGN LIB-mg:screen-value = Traduction("Inexistant",-2,"")  + " (" + Traduction("vente",-2,"") + ")".
        end.
        ELSE ASSIGN qui$ LIB-mg:SCREEN-VALUE = "".
    END.
    /*...$2104*/

    if last-event:function="return" then do:
        apply "tab" to br-mg.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME radio-contrat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL radio-contrat wsaicl1
ON VALUE-CHANGED OF radio-contrat IN FRAME Fc2
DO:
    vTypeContrat = (IF radio-contrat:SCREEN-VAL = "L" THEN "LOC":U ELSE "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.regime
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.regime wsaicl1
ON LEAVE OF entetcli.regime IN FRAME Fc1 /* R�gime TVA */
OR RETURN OF entetcli.regime DO:
find tabgco where tabgco.TYPE_TAB = "RV" and tabgco.N_TAB = input entetcli.regime no-lock no-error.
if available tabgco then do:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("RV", STRING(tabgco.n_tab), g-langue).
    IF intiTab <> "" THEN lib-rv:screen-value = intiTab. /*A35891*/
    ELSE lib-rv:screen-value = tabgco.inti_tab.
END.
else lib-rv:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-rv.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.region
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.region wsaicl1
ON LEAVE OF entetcli.region IN FRAME Fc1 /* R�gion */
OR RETURN OF entetcli.region DO:
find tabgco where tabgco.TYPE_TAB = "RG" and tabgco.N_TAB = input entetcli.region no-lock no-error.
if available tabgco THEN DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("RG", STRING(tabgco.n_tab), g-langue).
    IF intiTab <> "" THEN lib-rg:screen-value = intiTab. /*A35891*/
    ELSE lib-rg:screen-value = tabgco.inti_tab.
END.
else lib-rg:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-rg.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.s2_famille
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.s2_famille wsaicl1
ON LEAVE OF entetcli.s2_famille IN FRAME Fc1 /* Famille (Niveau 2) */
OR RETURN OF entetcli.s2_famille DO:
find tabgco where tabgco.TYPE_TAB = "SC" and tabgco.A_TAB = STRING(entetcli.famille:screen-value,"X(3)") + input entetcli.s2_famille no-lock no-error.
if available tabgco then DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SC", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-sc:screen-value = intiTab. /*...A35891*/
    ELSE lib-sc:screen-value = tabgco.inti_tab.
END.
else lib-sc:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-sc.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.s3_famille
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.s3_famille wsaicl1
ON LEAVE OF entetcli.s3_famille IN FRAME Fc1 /* Famille (Niveau 3) */
OR RETURN OF entetcli.s3_famille DO:
find tabgco where tabgco.TYPE_TAB = "SD" and tabgco.A_TAB = STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:screen-value,"X(3)") + input entetcli.s3_famille no-lock no-error.
if available tabgco then DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SD", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-sd:screen-value = intiTab. /*...A35891*/
    ELSE lib-sd:screen-value = tabgco.inti_tab.
END.
else lib-sd:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-sd.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.s4_famille
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.s4_famille wsaicl1
ON LEAVE OF entetcli.s4_famille IN FRAME Fc1 /* Famille (Niveau 4) */
OR RETURN OF entetcli.s4_famille DO:
find tabgco where tabgco.TYPE_TAB = "SE" and tabgco.A_TAB = STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:screen-value,"X(3)") + STRING(entetcli.s3_famille:screen-value,"X(3)") + input entetcli.s4_famille no-lock no-error.
if available tabgco then DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SE", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-se:screen-value = intiTab. /*...A35891*/
    ELSE lib-se:screen-value = tabgco.inti_tab.
END.
else lib-se:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-se.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME Tbdevis
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tbdevis wsaicl1
ON VALUE-CHANGED OF Tbdevis IN FRAME f1 /* Afficher les devis termin�s */
run openquery.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME TMultiech
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL TMultiech wsaicl1
ON VALUE-CHANGED OF TMultiech IN FRAME Fc1 /* Ech mult. */
bplanech:HIDDEN=(SELF:SCREEN-VAL="no").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc2
&Scoped-define SELF-NAME toggle-contrat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL toggle-contrat wsaicl1
ON VALUE-CHANGED OF toggle-contrat IN FRAME Fc2 /* Associer contrat */
ASSIGN radio-contrat:HIDDEN = (IF parsoc.ges_loc AND parsoc.ges_sav AND toggle-contrat:SCREEN-VAL = "YES" THEN NO ELSE YES)
       fill-contratloc:HIDDEN = NOT toggle-contrat:CHECKED
       btn-contratloc:HIDDEN = NOT toggle-contrat:CHECKED
       lib-contratloc:HIDDEN = NOT toggle-contrat:CHECKED.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&Scoped-define SELF-NAME tvoir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tvoir wsaicl1
ON VALUE-CHANGED OF tvoir IN FRAME f1 /* Voir infos */
run voir-infos.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fc1
&Scoped-define SELF-NAME entetcli.type_fac
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.type_fac wsaicl1
ON LEAVE OF entetcli.type_fac IN FRAME Fc1 /* Type facturation */
OR RETURN OF entetcli.type_fac DO:
find tabgco where tabgco.TYPE_TAB = "TF" and tabgco.A_TAB = input entetcli.type_fac no-lock no-error.
if available tabgco THEN DO:
    IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("TF", tabgco.a_tab, g-langue).
    IF intiTab <> "" THEN lib-tf:screen-value = intiTab. /*A35891*/
    ELSE lib-tf:screen-value = tabgco.inti_tab.
END.
else lib-tf:screen-value = Traduction("Inexistant",-2,"").
if last-event:function="return" then do:
    apply "tab" to br-tf.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-adrliv
&Scoped-define SELF-NAME entetcli.typ_con
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.typ_con wsaicl1
ON LEAVE OF entetcli.typ_con IN FRAME FRAME-adrliv /* Type conditionnement */
OR RETURN OF entetcli.typ_con DO:
    /*$A35969*/

    IF INPUT entetcli.typ_con <> "" THEN /*$A35969*/
    DO:
        IF entetcli.typ_veh:CHECKED THEN
        DO:
            FIND FIRST atecat WHERE atecat.catego=input entetcli.typ_con no-lock no-error.
            if available atecat then do:
                LIB-tc:screen-value = atecat.inti_cat.
            end.
            else LIB-tc:screen-value = Traduction("Inexistante",-2,"").
        END.
        ELSE DO:
            find tabgco where tabgco.type_tab="TC" and tabgco.a_tab=input entetcli.typ_con no-lock no-error.
            if available tabgco  AND AVAIL entetcli then do:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("TC", tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN LIB-tc:screen-value = intiTab. /*A35891*/
                ELSE LIB-tc:screen-value = tabgco.inti_tab.
            end.
            else LIB-tc:screen-value = Traduction("Inexistant",-2,"").
        END.
    END.
    ELSE LIB-tc:screen-value = "". /*$A35969*/
    if last-event:function="return" then do:
        apply "tab" to br-tc.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.typ_veh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.typ_veh wsaicl1
ON VALUE-CHANGED OF entetcli.typ_veh IN FRAME FRAME-adrliv /* Cat�gorie du v�hicule */
DO:
    /*$A35969*/
    IF entetcli.typ_veh:CHECKED THEN entetcli.typ_con:LABEL = Traduction("Cat�gorie du v�hicule",-1,"").
    ELSE entetcli.typ_con:LABEL = Traduction("Type conditionnement",-1,"").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME entetcli.zon_geo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL entetcli.zon_geo wsaicl1
ON LEAVE OF entetcli.zon_geo IN FRAME FRAME-adrliv /* Zone g�o. */
OR RETURN OF entetcli.zon_geo DO:
    /*$A35969*/
    find tabgco where tabgco.type_tab="GE" and tabgco.n_tab=input entetcli.zon_geo no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GE", STRING(tabgco.n_tab), g-langue).
        IF intiTab <> "" THEN LIB-ge:screen-value = intiTab. /*A35891*/
        ELSE LIB-ge:screen-value = tabgco.inti_tab.
    end.
    else LIB-ge:screen-value = Traduction("Inexistante",-2,"").
    if last-event:function ="return" then do:
        apply "tab" to br-zg.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wsaicl1


/* ***************************  Main Block  *************************** */
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN
    CURRENT-WINDOW                = {&WINDOW-NAME}
    THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
    LOGBID = SESSION:SET-WAIT-STATE("GENERAL")
    {&WINDOW-NAME}:PRIVATE-DATA=string(nopt)
    {&WINDOW-NAME}:hidden = YES
    frame f1:hidden = YES.


/* $Trad */	TraitementArbre(YES,FRAME FRAME-adrliv:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME Fval:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME Fentete:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME Fc1:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME fonglet:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME f1:HANDLE,'','').
/* $Trad */	TraitementArbre(YES,FRAME Fc2:HANDLE,'','').
/* $Trad */	TraitementArbre(NO,wsaicl1:HANDLE,'','').

ON Alt-Cursor-Up OF frame F1         ANYWHERE logbid=CBFocusItem(hcombars-f1            , 2, {&Bquitter}).
ON Alt-Cursor-Up OF FRAME Fentete    ANYWHERE logbid=CBFocusItem(hcombars-Fentete       , 2, {&bannuler-p}).
ON Alt-Cursor-Up OF FRAME Fc1        ANYWHERE logbid=CBFocusItem(hcombars-Fentete       , 2, {&bannuler-p}).
ON Alt-Cursor-Up OF FRAME Fc2        ANYWHERE logbid=CBFocusItem(hcombars-Fentete       , 2, {&bannuler-p}).
ON Alt-Cursor-Up OF FRAME Fval       ANYWHERE logbid=CBFocusItem(hcombars-Fentete       , 2, {&bannuler-p}).
ON F1 ANYWHERE DO:
    IF FRAME-NAME="f1" THEN  AidezMoi("pgi_ventes",  nopt, ?, "").
    ELSE  AidezMoi("pgi_ventes",  (if type-saisie="C" then 17011 else if type-saisie="D" then 17008 else if type-saisie="F" then 17016 else 17019), ?, "").
END.
/* Raccourcis onglet suivant/pr�c�dent */
ON F11 OF FRAME fc1 OR F11 OF FRAME fc2 OR F11 OF FRAME fonglet OR F11 OF FRAME fval
      ANYWHERE DO:
    if nb-chap > 1 then do:
        session:set-wait-state("general").
        RUN VALIDER-CHAPITRE.
        IF chap-cours <> nb-chap THEN chap-cours = chap-cours + 1.
        RUN chapitre-suivprec.
    end.
END.
ON F10 OF FRAME fc1 OR F10 OF FRAME fc2 OR F10 OF FRAME fonglet OR F10 OF FRAME fval
    ANYWHERE DO:
    if nb-chap > 1 then do:
        session:set-wait-state("general").
        RUN VALIDER-CHAPITRE.
        IF chap-cours <> 1 THEN chap-cours = chap-cours - 1.
        RUN chapitre-suivprec.
    end.
END.
ON SHIFT-F8 OF FRAME f1 ANYWHERE DO:  /* $2240 */
    IF CBGetItemEnableState (Hcombars-f1, 2, {&Baffcde}) AND CBGetItemShowState (Hcombars-f1, 2, {&Baffcde})
        THEN RUN choose-Baffcde.
END.
ON CHOOSE OF bcrtcli IN FRAME f1 DO:
    RUN mntcli_b (type-saisie,OUTPUT X).
    IF X<>0 THEN DO:
        cli$:SCREEN-VAL=STRING(X).
        APPLY "return" TO cli$.
    END.
END.
ON 'CTRL-F6':U OF FRAME f1 ANYWHERE DO:
    IF NOT pas-droit-crt THEN DO:
        APPLY "entry" TO bcrtcli.
        APPLY "choose" TO bcrtcli.
    END.
END.

ON 'CTRL-F4':U OF FRAME f1 ANYWHERE APPLY "entry" TO cdepot.
ON VALUE-CHANGED OF cdepot IN FRAME f1 DO:
    IF self:TEXT-SELECTED THEN
    do:
        RUN openquery.
        IF CAN-FIND (FIRST depsoc NO-LOCK) THEN /* recalcul des dates */ /*�XREF_NOWHERE�*/
        DO:
            FIND FIRST depsoc WHERE depsoc.depot = INT(SELF:SCREEN-VAL) NO-LOCK NO-ERROR.  /* $1110 */
            dat-liv-ini=TODAY.
            IF CAN-DO("C,D",type-saisie) THEN DO:
                IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN /*$979*/
                    run trt-calendar (0,year(dat-liv-ini),(IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv),input-output dat-liv-ini).
                else dat-liv-ini=dat-liv-ini + (IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv).
            END.
            IF parsoc.sem_liv THEN sem-liv-ini=CalculSemaine(dat-liv-ini).
            ASSIGN
                dat-liv$:SCREEN-VAL IN FRAME f1 = STRING (dat-liv-ini)
                sem$:SCREEN-VAL = STRING (sem-liv-ini).
        END.
    END.
END.
ON LEAVE OF cdepot IN FRAME f1 DO:
    IF self:LOOKUP(SELF:SCREEN-VAL) = 0 THEN SELF:SCREEN-VALUE = ?.
    IF INT(SELF:SCREEN-VAL)=? OR SELF:SCREEN-VAL = CodeDepotCombo(SELF:HANDLE)  /*$ A77918 */ THEN
    DO:
        vchar = CodeDepotCombo(SELF:HANDLE).
        IF vchar<>"" AND LOOKUP(vchar,SELF:LIST-ITEM-PAIRS)>0 THEN SELF:SCREEN-VAL=vchar.
        ELSE SELF:SCREEN-VAL=SELF:ENTRY(1).
        APPLY "value-changed" TO SELF.
    END.
END.
ON 'CTRL-F4':U OF FRAME fc1 ANYWHERE APPLY "entry" TO edepot.
ON LEAVE OF edepot IN FRAME fc1 DO:
    IF self:LOOKUP(SELF:SCREEN-VAL) = 0 THEN SELF:SCREEN-VALUE = ?.
    IF INT(SELF:SCREEN-VAL)=? OR SELF:SCREEN-VAL = CodeDepotCombo(SELF:HANDLE)  /*$ A77918 */ THEN
    DO:
        vchar = CodeDepotCombo(SELF:HANDLE).
        IF vchar<>"" AND LOOKUP(vchar,SELF:LIST-ITEM-PAIRS)>0 THEN SELF:SCREEN-VAL=vchar.
        ELSE SELF:SCREEN-VAL=SELF:ENTRY(1).
        APPLY "value-changed" TO SELF.
    END.
END.

ON VALUE-CHANGED OF radio-adresse ASSIGN
    radio-adresse
    entetcli.adr_fac[1]:HIDDEN = radio-adresse
    entetcli.adr_fac[2]:HIDDEN = radio-adresse
    entetcli.adr_fac[3]:HIDDEN = radio-adresse
    entetcli.adrfac4:HIDDEN = radio-adresse
    entetcli.villef:HIDDEN = radio-adresse
    br-kp:HIDDEN = radio-adresse
    entetcli.k_post2f:HIDDEN = radio-adresse
    entetcli.civ_fac:HIDDEN = radio-adresse
    entetcli.paysf:HIDDEN = radio-adresse
    lib-pa:HIDDEN = radio-adresse
    lib-ci:HIDDEN = radio-adresse
    br-pa:HIDDEN = radio-adresse
    br-ci:HIDDEN = radio-adresse
    entetcli.adr_cde[1]:HIDDEN = NOT radio-adresse
    entetcli.adr_cde[2]:HIDDEN = NOT radio-adresse
    entetcli.adr_cde[3]:HIDDEN = NOT radio-adresse
    entetcli.adr_cde[4]:HIDDEN = NOT radio-adresse
    entetcli.villec:HIDDEN = NOT radio-adresse
    entetcli.k_post2c:HIDDEN = NOT radio-adresse
    entetcli.civ_cde:HIDDEN = NOT radio-adresse
    entetcli.paysc:HIDDEN = NOT radio-adresse
    lib-cde-pa:HIDDEN = NOT radio-adresse
    lib-cde-ci:HIDDEN = NOT radio-adresse.

ON f4 OF entetcli.tx_ech_d IN FRAME fc1 DO:
    p%val8 = entetcli.tx_ech_d:INPUT-VALUE.
    RUN unidev_o (entetcli.devise:INPUT-VALUE,INPUT-O p%val8,OUTPUT logbid).
    IF logbid THEN DISP p%val8 @ entetcli.tx_ech_d WITH FRAME fc1.
END.
ON LEAVE OF entetcli.k_post2f in FRAME fc1
OR RETURN OF entetcli.k_post2f DO:
    IF entetcli.k_post2f:SCREEN-VAL<>CodePostal OR entetcli.villef:SCREEN-VAL = "" then do:
        parcod = entetcli.k_post2f:SCREEN-VAL.
        run reccpv_b (input-o parcod, NO, YES, output parlib).
        if parlib <> "" then assign
            entetcli.villef:SCREEN-VAL = parlib
            entetcli.k_post2f:SCREEN-VAL = parcod
            CodePostal = parcod.
    end.
END.

ON LEAVE OF entetcli.cpt_bq IN FRAME Fc2 /* Banque */
OR RETURN OF entetcli.cpt_bq DO:
    self:SCREEN-VALUE = SUBSTR(trim(self:SCREEN-VALUE) + "000000000", 1, 9).
    FIND PLANCPT where plancpt.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ PLANCPT.cpt = input entetcli.cpt_bq no-lock no-error.
    IF available PLANCPT THEN lib-cchq:SCREEN-VALUE = PLANCPT.cpt_INTi.
    ELSE lib-cchq:SCREEN-VALUE = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to brcchq.
        return no-apply.
    end.
END.
ON LEAVE OF entetcli.saison IN FRAME Fc2 /* Saison */
OR RETURN OF entetcli.saison DO:
    find FIRST tabgco where tabgco.TYPE_TAB = "SS" and tabgco.a_TAB = input entetcli.saison no-lock no-error.
    if available tabgco then do:
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SS", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ss:screen-value = intiTab.
        ELSE lib-ss:screen-value = tabgco.inti_tab.
        /*lib-ss:screen-value = tabgco.inti_tab.*/
        RUN DATPX-SAISON.
    END.
    else lib-ss:screen-value = Traduction("Inexistante",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-ss.
        return no-apply.
    end.
END.
ON LEAVE OF entetcli.grp_ps IN FRAME Fc2 /* Groupe de prestations */
OR RETURN OF entetcli.grp_ps DO:
    find FIRST tabgco where tabgco.TYPE_TAB = "GP" AND tabgco.a_tab = input entetcli.grp_ps no-lock no-error.
    if available tabgco then do:
         /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GP":U, tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-gpres:screen-value = intiTab.
        ELSE lib-gpres:screen-value = tabgco.inti_tab.
        /*lib-gpres:screen-value = tabgco.inti_tab.*/
    END.
    else lib-gpres:screen-value = Traduction("Inexistant",-2,"").
    if last-event:function="return" then do:
        apply "tab" to br-gpres.
        return no-apply.
    end.
END.
ON ENTRY OF cli$ IN FRAME f1 DO:
    if valid-handle(child-info) then RUN ChildTop IN child-info.
END.
/* Triggers Return --> tabulation */
ON RETURN OF WSAICL1 ANYWHERE DO:
if can-do("fill-in,toggle-box,radio-set,combo-box",self:type) then do:
  apply "tab" to self.
  return no-apply.
end.
else if self:type="editor" then self:insert-string("~n").
else if self:type="button" then apply "choose" to self.
else if can-do("browse,selection-list",self:type) then apply "default-action" to self.
END.

/* Bouton droit : Menu contextuel */
ON MOUSE-MENU-CLICK OF wsaicl1 OR ALT-F1 OF {&WINDOW-NAME} ANYWHERE RUN MENUCLICK. /*$A104178*/

ON 'end-resize':U OF FRAME f1 ANYWHERE DO:
    /* ne marche pas pour les champs calcul�s */
    IF SELF:PARENT=hbrowse:HANDLE AND SELF:NAME<>? THEN DO:
        IF LoadSettings("HBSAICL1", OUTPUT hSettingsApi) THEN DO:
            SaveSetting(SELF:NAME, STRING(SELF:WIDTH)).
            UnloadSettings(hSettingsApi).
        end.
    END.
END.
ON 'ctrl-f12':U OF FRAME f1 ANYWHERE RUN choose-bajouter-colonnes.
/*ON START-SEARCH OF FRAME F1 ANYWHERE DO:
 *     IF SELF:TYPE <> "Browse" THEN RETURN NO-APPLY.
 *     HCol = SELF:CURRENT-COLUMN.
 *     IF HCol:NAME = ? THEN RETURN NO-APPLY.
 *     ASSIGN
 *         ColDesc = (IF ColOrd = HCol:NAME THEN NOT ColDesc ELSE NO)
 *         chQryBy = "BY " + HCol:NAME + (IF ColDesc THEN " DESC" ELSE "").
 *     ColOrd = HCol:NAME.
 *     RUN openquery.
 *     APPLY "entry" TO hbrowse.
 * END. $2240 */
ON LEAVE OF dat-liv$ IN FRAME f1
DO:
    IF parsoc.sem_liv AND SELF:MODIFIED THEN do:
        sem$=CalculSemaine(DATE(SELF:SCREEN-VAL)).
        DISPLAY sem$ WITH FRAME f1.
    END.
END.
ON LEAVE OF sem$ IN FRAME f1
DO:
    IF SELF:MODIFIED AND INPUT sem$<54 AND INPUT sem$<>sem$ THEN
        DISPLAY CalculDateFinSem (INT(SELF:SCREEN-VAL)) @ dat-liv$ WITH FRAME f1.
END.
ON LEAVE OF entetcli.dat_liv IN FRAME fc1
DO:
    IF parsoc.sem_liv AND SELF:MODIFIED THEN do:
        parnum=CalculSemaine(DATE(SELF:SCREEN-VAL)).
        DISPLAY parnum @ entetcli.semaine WITH FRAME fc1.
    END.
    IF SELF:MODIFIED THEN DO:
        IF CAN-DO ("D,C", type-cours) AND parsoc.gpao THEN DO:
            tmpdat = INPUT entetcli.dat_liv.
            run trt-calendar (0,year(tmpdat),-1 * ecart_liv_mad,input-output tmpdat).
            wdat_mad = tmpdat.
        END.
        tmpdat = INPUT entetcli.dat_liv.
        IF wdelai_trs>0 THEN
            run trt-calendar (IF AVAIL client THEN client.cal_liv ELSE 0,year(tmpdat),wdelai_trs,input-output tmpdat).
        wdat_acc = tmpdat.
    END.
END.
ON LEAVE OF entetcli.semaine IN FRAME fc1
DO:
    IF SELF:MODIFIED AND INPUT entetcli.semaine<54 AND INPUT entetcli.semaine<>parnum THEN
    DO:
        DISPLAY CalculDateFinSem (INT(SELF:SCREEN-VAL)) @ entetcli.dat_liv WITH FRAME fc1.

        IF CAN-DO ("D,C", type-cours) AND parsoc.gpao THEN DO:
            tmpdat = INPUT entetcli.dat_liv.
            run trt-calendar (0,year(tmpdat),-1 * ecart_liv_mad,input-output tmpdat).
            wdat_mad = tmpdat.
        END.
        tmpdat = INPUT entetcli.dat_liv.
        IF wdelai_trs>0 THEN
            run trt-calendar (IF AVAIL client THEN client.cal_liv ELSE 0,year(tmpdat),wdelai_trs,input-output tmpdat).
        wdat_acc = tmpdat.
    END.

END.
/* Raccourcis */
ON CHOOSE OF bappdev IN FRAME Fc1 run bappdev.
ON F8 OF FRAME F1 ANYWHERE DO:
    IF CBGetItemEnableState (Hcombars-f1, 2, {&bmodifier}) AND CBGetItemShowState (Hcombars-f1, 2, {&bmodifier}) THEN
        RUN CHOOSE-BMODIFIER.
END.
ON CTRL-F7 OF FRAME F1 ANYWHERE DO:
    IF LoadSettings("SAICL", OUTPUT hSettingsApi) THEN DO:
        logbid=SaveSetting("Toutes", STRING(toutes-saisies)).

        UnloadSettings(hSettingsApi).
    end.
END.
ON F6 OF FRAME F1 ANYWHERE DO:
    APPLY "entry" TO FrmComBars-f1.
    IF CBGetItemEnableState (Hcombars-f1, 2, {&bcreer}) AND CBGetItemShowState (Hcombars-f1, 2, {&bcreer}) THEN
        RUN CHOOSE-BCREER.
    RETURN NO-APPLY.
END.
ON F9 OF wsaicl1 ANYWHERE do:
    if frame-name<>"f1" then do:
        APPLY "entry" TO FrmComBars-fentete. /* $A55279 */
        RUN CHOOSE-bvalider-p.
    END.
END.
ON F7 OF wsaicl1 ANYWHERE DO:
    if frame-name<>"f1" then do:
        APPLY "entry" TO bdate IN FRAME fc1.
        APPLY "choose" TO bdate.
    END.
    ELSE IF CBGetItemShowState (Hcombars-f1, 2,{&bsaisie-liv}) THEN RUN choose-bsaisie-liv.
END.

/*$A35969...*/
ON F9 OF FRAME frame-adrliv ANYWHERE DO:
    APPLY "entry" TO FrmComBars-adrliv.
    RUN CHOOSE-bvalider-adrliv.
    RETURN NO-APPLY.
END.
ON END-ERROR OF FRAME frame-adrliv
OR ENDKEY OF FRAME frame-adrliv  ANYWHERE DO:
    APPLY "entry" TO FrmComBars-adrliv.
    RUN CHOOSE-babandonner-adrliv.
    RETURN NO-APPLY.
END.

/*...$A35969*/

/* Recherche Tables GCO */
ON CHOOSE OF br-sc IN FRAME Fc1 run br-sc.
ON CHOOSE OF br-sd IN FRAME Fc1 run br-sd.
ON CHOOSE OF br-se IN FRAME Fc1 run br-se.
ON CHOOSE OF br-aff,br-aa,br-rp1,br-nc,br-fc,br-gc,br-rg,br-rv,br-tf,br-oc,br-tar,br-kv IN FRAME Fc1 run br-fcg1.
ON CHOOSE OF br-ct,br-rp2,br-ss,brcchq,br-gpres IN FRAME Fc2 run br-fcg2.
ON CHOOSE OF br-zt1,br-zt2,br-zt3,br-zt4,br-zt5 IN FRAME FVAL run br-val-fval.
ON CHOOSE OF br-xt1,br-xt2,br-xt3,br-xt4,br-xt5 IN FRAME FC1 run br-val-fc1.
/* Recherche Tables COMPTA */
ON CHOOSE OF br-ci,br-de,br-lg,br-pa IN FRAME Fc1 run br-fcc1.
ON CHOOSE OF br-kp IN FRAME Fc1 RUN br-kp.
ON CHOOSE OF bdate IN FRAME FC1
DO:
    ASSIGN tmpdat  = wdat_mad
           tmpdat2 = INPUT FRAME fc1 entetcli.dat_liv.
    run saicl1d_c (type-cours, (IF INT(vcl_livre:SCREEN-VAL IN FRAME fc2)<>0 THEN INT(vcl_livre:SCREEN-VAL IN FRAME fc2) ELSE cle-cours),
                   input-output wdat_valid,
                   input-output wdat_rec, input-output whr_rec,
                   INPUT-OUTPUT tmpdat2, INPUT-OUTPUT wdat_mad,
                   INPUT-OUTPUT wdelai_trs, INPUT-OUTPUT wno_adr, input-output wdat_livd, input-output wdat_acc, input-output wdat_rll, INT(edepot:SCREEN-VAL)).
    entetcli.dat_liv:SCREEN-VAL IN FRAME fc1 = string(tmpdat2).

    /* $A43168 ... */
    IF parsoc.sem_liv THEN do:
        parnum=CalculSemaine(tmpdat2).
        DISPLAY parnum @ entetcli.semaine WITH FRAME fc1.
    END.
    /* ... $A43168 */

    IF CAN-DO ("D,C", type-cours) AND parsoc.gpao AND tmpdat <> wdat_mad THEN
    DO:
        wforcerdat_mad = YES. /*$A79284*/
        RUN delai-trt-calendar (wdat_mad, INPUT INPUT FRAME fc1 entetcli.dat_liv, OUTPUT ecart_liv_mad).
    END.

    IF parsoc.prix_franco AND entetcli.prix_franco:CHECKED AND NOT creation AND AVAIL entetcli AND entetcli.k_postl <> wno_adr THEN recalculer-prix-franco = YES. /*$A35969*/
END.
ON RETURN, LEAVE OF zt1, zt2, zt3, zt4, zt5 IN FRAME fval RUN val-fval. /*$A40063*/
ON RETURN, LEAVE OF xt1, xt2, xt3, xt4, xt5 IN FRAME fc1 RUN val-fc1. /*$A40063*/
/* Recherches clients */
ON RETURN, LEAVE OF vcl_stat,vcl_livre,vcl_fact,vcl_grp,vcl_paye,vcl_pln,vcl_plr,vc_factor IN FRAME Fc2 DO:
    run ret-vcl.
    if last-event:function="return" then return no-apply.
END.
ON CHOOSE OF brcstat,brclivre,brcgrp,brcfact,brcpaye,brcpln,brcplr,brcfactor IN FRAME Fc2 /*$342*/ run br-vcl.
ON LEAVE OF entetcli.NO_tarif IN FRAME Fc1 OR RETURN OF entetcli.NO_tarif DO:
    RUN trt-tarif.
    if last-event:function="return" then do:
        apply "tab" to br-tar.
        return no-apply.
    end.
END.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON WINDOW-CLOSE, END-ERROR, ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
    if frame-name="f1" then APPLY "CLOSE":U TO THIS-PROCEDURE.
    else if porigine<>"2" OR entetcli.typ_sai="A" then DO :
        IF CBGetItemEnableState (Hcombars-fentete, 2, {&bannuler-p}) THEN RUN choose-bannuler-p. /*$A69132*/
    END.
    RETURN NO-APPLY.
END.
ON CLOSE OF THIS-PROCEDURE DO:
  RUN fin-pgm.
  RUN put-ini.
  IF svrowid <> ? THEN FreeLock ("entetcli", STRING(svrowid)) NO-ERROR. /* $A36207 */
  IF VALID-HANDLE(hapi-piece-cl) THEN APPLY "close" TO hapi-piece-cl. /* $2178 */
  IF VALID-HANDLE(HndGcoErg)  THEN APPLY "CLOSE" TO HndGcoerg.
  IF VALID-HANDLE(hComBars)   THEN APPLY "close" TO hComBars.
  IF VALID-HANDLE(HndDrTyp)   THEN APPLY "close" TO HndDrTyp.   /* $A41067 */
  IF VALID-HANDLE(g-hdocs) THEN APPLY "close" TO g-hdocs. /*$A49610*/
  IF VALID-HANDLE(myonglet) THEN RELEASE OBJECT myonglet NO-ERROR.
  IF VALID-HANDLE(xitem) THEN RELEASE OBJECT xitem NO-ERROR.
  IF VALID-HANDLE(xitem2) THEN RELEASE OBJECT xitem2 NO-ERROR.  /* $A38645*/
  IF VALID-HANDLE(xitem-edtDocs) THEN RELEASE OBJECT xitem-edtDocs NO-ERROR. /*$A36586*/
  IF VALID-HANDLE(xitem-edtARc) THEN RELEASE OBJECT xitem-edtARc NO-ERROR. /*$A49610*/
  IF VALID-HANDLE(xitem-edtFac) THEN RELEASE OBJECT xitem-edtFac NO-ERROR. /*$A49610*/
  IF VALID-HANDLE(xitem-edtDev) THEN RELEASE OBJECT xitem-edtDev NO-ERROR. /*$A49610*/
  IF VALID-HANDLE(chCtrlFrame) THEN RELEASE OBJECT chCtrlFrame NO-ERROR.
  IF VALID-HANDLE(CtrlFrame) THEN DELETE OBJECT CtrlFrame NO-ERROR.
  IF VALID-HANDLE({&WINDOW-NAME}) THEN DELETE WIDGET {&WINDOW-NAME}.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
  ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

    /* $1753 ... */
    IF porigine = "5" THEN ASSIGN vChargerClient = YES
                                  porigine = "1".
    /* ... $1753 */

    /* On vient de t�l�vente, visite */
    IF substr(porigine,1,1)="4" OR substr(porigine,1,1)="3" THEN ASSIGN
        type-plan=substr(porigine,2,1)
        interv=substr(porigine,3,10)
        rowid-planav=TO-ROWID(substr(porigine,13))
        porigine=substr(porigine,1,1).
    RUN proc-init.
    if can-do("1,3",porigine) then do:
        SESSION:SET-WAIT-STATE("").
        view {&WINDOW-NAME}.
        APPLY "entry" TO {&WINDOW-NAME}.
        APPLY "ENTRY" TO cli$.
    end.
    else if porigine="2" OR porigine = "6b" OR porigine = "LOC":U then do:
        RUN RESIZEWINDOW.
        find client where client.cod_cli=pnocl no-lock no-error.
        find cptcli where cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli=pnocl no-lock no-error.
        IF porigine = "6b" THEN PCL_SET_COD_CLI("SAICL1":U, pnocl).
        assign svrowid=prowid creation=false.
        run envoi-entete.
        RUN fin-envoi-entete.
        RUN LockWindowUpdate IN G-HPROWIN (0).
        IF v-rapide <> "oui" OR porigine = "2" /* en saisie rapide avec passage sur l'en-t�te, on passe aussi ici $2013 */ THEN DO:
            IF entetcli.typ_sai<>"A" AND porigine <> "6b" THEN Logbid = CBEnableItem (Hcombars-fentete, 2, {&bannuler-p}, NO).
            Logbid = CBEnableItem (Hcombars-fentete, 2, {&bsupprimer-p}, NO).
            /*RUN LockWindowUpdate IN G-HPROWIN (0).*/
            SESSION:SET-WAIT-STATE("").
            view {&WINDOW-NAME}.
            IF porigine = "6b" THEN DO:
                if valid-handle(first-wid) AND first-wid:VISIBLE then apply "entry" to first-wid.
                else if chap-cours=1 then apply "entry" to entetcli.dat_cde in frame fc1.
            END.
        END.
    end.
    else if porigine="4" OR porigine = "6a" then do:
        RUN RESIZEWINDOW.
        if (parcde.gst_zone[11] and type-saisie="D") or (parcde.gst_zone[8] and type-saisie<>"D") then run anocde_s (output nocde).
        if nocde=0 then nocde=next-value(compteur_commande_client).
        find client where client.cod_cli=pnocl no-lock no-error.
        find cptcli where cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli=pnocl no-lock no-error.
        IF pvalaffaire <> ? AND pvalaffaire <> "" THEN DO : /*$A85716...*/
            FIND FIRST affaire WHERE affaire.affaire = pvalaffaire NO-LOCK NO-ERROR.
            IF AVAIL affaire THEN DO :
                ASSIGN entetcli.affaire:screen-value = affaire.affaire
                       lib-aff:SCREEN-VAL            = affaire.int_aff.
            END.
        END. /*...$A85716*/
        ASSIGN
            dat-cde$=today
            dat-liv$=dat-liv-ini
            sem$=sem-liv-ini
            typ-cde=(if plusieurs-types then int(substr(typ-cde$:screen-value,1,1)) else 1)
            creation=yes.

        /* $2240... */
        IF VALID-HANDLE(hparent) AND hparent:FILE-NAME = "api-piece-cl":U THEN DO:
            DEF VAR temp AS CHAR  NO-UNDO.
            qui$ = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"qui":U).
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"datecde":U).
            IF temp <> "" AND temp <> ? THEN dat-cde$ = DATE(temp).
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"dateliv":U).
            IF temp <> "" AND temp <> ? THEN dat-liv$ = DATE(temp).
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"sem":U).
            IF temp <> "" AND temp <> ? AND temp <> "0" THEN sem$ = INT(temp).
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"typcde":U).
            IF temp <> "" AND temp <> ? AND temp <> "0" THEN typ-cde = INT(temp).
            not-ref$ = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"notref":U).
            ref-cde$ = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"refcde":U).
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"depot":U).
            IF temp <> ? AND temp <> "0" THEN depot-fc1 = temp.
            temp = DYNAMIC-FUNCTION("PCL_GET_VALEUR" IN hparent,"nature":U).
            IF temp <> "" AND temp <> ? THEN ASSIGN nat$:SCREEN-VAL = temp nat$.
        END.
        /* ...$2240*/

        IF parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/ THEN DO:
            dat-liv$=getsessiondate("Tournee", "Dat", TODAY).
            IF parsoc.sem_liv THEN sem$=CalculSemaine(dat-liv$).
            ASSIGN
                ref-cde$=getsessiondataex("Tournee", "Ref","")
                NOT-ref$=getsessiondataex("Tournee", "Not","").
      END.
      SESSION:SET-WAIT-STATE("").
      view {&WINDOW-NAME}.
      IF v-rapide = "oui" THEN debut-prog=YES.
      run envoi-entete.
      debut-prog=NO.
      RUN fin-envoi-entete.
    end.
    IF NOT THIS-PROCEDURE:PERSISTENT THEN WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.
{ini-libs.i}
{rech-mat.i} /* $2215 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aTrad$VariableInitial wsaicl1
PROCEDURE aTrad$VariableInitial :

	ASSIGN
                Ft$selection-1:SCREEN-VAL IN FRAME f1 = Traduction("S�lection",-2,"") Ft$selection-1 /* $tradVar */
                Fhie1:SCREEN-VAL IN FRAME Fc1 = "�" Fhie1 /* $tradVar */
                FHie2:SCREEN-VAL IN FRAME Fc1 = "�" FHie2 /* $tradVar */
                FHie3:SCREEN-VAL IN FRAME Fc1 = "�" FHie3 /* $tradVar */
                Ft$selection-c1:SCREEN-VAL IN FRAME Fc1 = Traduction("Adresse client",-2,"") Ft$selection-c1 /* $tradVar */
                Ft$selection-c2:SCREEN-VAL IN FRAME Fc1 = Traduction("Informations diverses",-2,"") Ft$selection-c2 /* $tradVar */
                Fborne:SCREEN-VAL IN FRAME Fc2 = Traduction("Borne minimale de remise / quantit�",-2,"") + ":" Fborne /* $tradVar */
                Ft$selection-c3:SCREEN-VAL IN FRAME Fc2 = Traduction("Informations diverses",-2,"") Ft$selection-c3 /* $tradVar */
                Ft$selection-c4:SCREEN-VAL IN FRAME Fc2 = Traduction("Fili�re client",-2,"") Ft$selection-c4 /* $tradVar */
                ft$titre1:SCREEN-VAL IN FRAME FRAME-adrliv = Traduction("Adresse",-2,"") ft$titre1 /* $tradVar */
                ft$titre2:SCREEN-VAL IN FRAME FRAME-adrliv = Traduction("Livraison",-2,"") ft$titre2 /* $tradVar */
		.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE active-f1 wsaicl1
PROCEDURE active-f1 :
RUN Ecran1_Ok.
if creation then do:
    qcondition=?.
    run openquery.
    if avail entetcli then do:
        if hbrowse:num-iterations>0 and hbrowse:num-iterations<>? then hbrowse:set-repositioned-row(hbrowse:num-iterations,"always").
        /*HQuery:REPOSITION-TO-ROWID(svrowid) NO-ERROR.*/
        hbrowse:QUERY:REPOSITION-TO-ROWID(svrowid) NO-ERROR.  /* $2240 */
    end.
end.
else if hbrowse:num-iterations>0 then hbrowse:refresh().
if hbrowse:num-iterations>0 and avail entetcli then apply "value-changed" to hbrowse.
session:set-wait-state("").
if creation or hbrowse:visible=no then apply "entry" to cli$ IN FRAME f1.
else apply "entry" to hbrowse.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actuSupp wsaicl1
PROCEDURE actuSupp :
/* $2178... */
    DO WITH FRAME f1 :
        hbrowse:delete-current-row().
        if hbrowse:num-iterations>0 then RUN refresh-hbrowse IN hapi-piece-cl.  /* $2240 */
        else DO:
            RUN cacher-champs.
        END.
    END.
    /* ...$2178 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aff-assist wsaicl1
PROCEDURE aff-assist :
/* Assistant associ� */
    /*$A64611...*/
    FIND FIRST droittyp WHERE droittyp.util = g-user-droit AND droittyp.typ_fich = "AV":U AND droittyp.typ_elem = "" AND droittyp.chapitre = 0 AND droittyp.interdit_aff = YES NO-LOCK NO-ERROR.
    IF NOT AVAILABLE droittyp THEN DO: /*...$A64611*/
        find assistan where assistan.typ_fich="V" and assistan.cod_chap=ecr-cours no-lock no-error.
        if available assistan then DO:
            IF NOT VALID-HANDLE(hpostit) THEN RUN postit PERSISTENT SET hpostit.
            IF VALID-HANDLE(hpostit) then RUN PostitAffichage IN hpostit (FRAME F1:HANDLE, 2, "SV", Traduction("Assistant saisie",-2,""),assistan.texte).
        end.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affichage-canal wsaicl1
PROCEDURE affichage-canal :
do with frame fc1:
    entetcli.canal:SCREEN-VAL=tabgco.canal.
    FIND tabgco WHERE tabgco.TYPE_tab = "KV" AND tabgco.a_tab = entetcli.canal:SCREEN-VAL NO-LOCK NO-ERROR.
    IF AVAIL tabgco THEN DO:
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("KV", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-kv:screen-value = intiTab.
        ELSE lib-kv:screen-value = tabgco.inti_tab.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE annul-entete wsaicl1
PROCEDURE annul-entete :
IF AVAIL entetcli AND entetcli.MOD_liv > "" /*$A74887...*/ AND parsoc.aff_cli /*...$A74887*/ THEN RUN CREER_AFFRETEMENT . /*$1728*/

    IF (porigine="2" AND entetcli.typ_sai="A") THEN apply "close" to this-procedure.
    ELSE if porigine="4" OR (porigine = "6a" AND wori_saibp1 = NO /*$A36019*/ AND crtHorsSaicl1 = NO /* $A56248 */) then do:
        /* $A99522 ... */
        FIND FIRST entetcli WHERE entetcli.cod_cli = INT(cod_cli:SCREEN-VALUE IN FRAME fonglet)
                              AND entetcli.typ_sai = type-saisie
                              AND entetcli.no_cde  = INT(no_cde:SCREEN-VALUE)
                              AND entetcli.no_bl   = INT(no_bl:SCREEN-VALUE) NO-LOCK NO-ERROR.
        IF AVAILABLE entetcli THEN DO:
            PgmSpec = RechPgmSpeGco("s-supcl.r").
            IF pgmSpec <> ? THEN
                RUN VALUE(pgmSpec) (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,output vchar).
            IF CONNECTED ("WORKFLOW") AND SEARCH("workflow\execut_wf.r") <> ? THEN
                RUN VALUE (PropWF + "\execut_wf") (NO, 0, "PROGIWIN", "*SAISIE VENTE", YES, "GCO.entetcli" + CHR(1) + STRING(ROWID(entetcli)),"S","",OUTPUT pretwf).
            DO TRANSACTION:
                find entetcli where entetcli.cod_cli=int(cod_cli:screen-value in frame fonglet) and entetcli.typ_sai=type-saisie and
                        entetcli.no_cde=int(no_cde:screen-value) and entetcli.no_bl=int(no_bl:screen-value) exclusive-lock no-wait no-error.
                IF AVAIL entetcli THEN DO:
                    /* M�morisation infos avant suppression pour trigger delec_db */
                    entetcli.contrat=STRING("SAICL1_C","X(12)") + "PGM$��".
                    delete entetcli.
                END.
            END.
        end.
        /* ... $A99522 */
        apply "close" to this-procedure.
    end.
    else do:
        ASSIGN
            retour=no
            parnum=0.
        if modif-entete then do:
            /* Suppression commande en cours si cr�ation et abandon */
            if creation then do:
                IF AVAIL entetcli AND entetcli.achev THEN logbid=NO. /* Attention, cela veut dire �dition dans saicl3 donc fin valide mais pb sur aper�u avec echap sur saisie */
                ELSE DO:
                    logbid=NO.
                    message SUBSTITUTE(Traduction("Voulez-vous supprimer la pi�ce &1 que vous �tiez en train de cr�er ?",-2,""),entetcli.no_cde:screen-value in frame fonglet)
                        view-as alert-box question buttons yes-no update logbid.
                END.
                IF logbid THEN DO:
                    /* $A99522 ... */
                    FIND FIRST entetcli WHERE entetcli.cod_cli = INT(entetcli.cod_cli:SCREEN-VALUE)
                                          AND entetcli.typ_sai = type-saisie
                                          AND entetcli.no_cde  = INT(entetcli.no_cde:SCREEN-VALUE)
                                          AND entetcli.no_bl   = INT(entetcli.no_bl:SCREEN-VALUE) NO-LOCK NO-ERROR.
                    IF AVAIL entetcli THEN DO:
                        PgmSpec = RechPgmSpeGco("s-supcl.r").
                        IF pgmSpec <> ? THEN
                            RUN VALUE(pgmSpec) (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,output vchar).
                        IF CONNECTED ("WORKFLOW") AND SEARCH("workflow\execut_wf.r") <> ? THEN
                            RUN VALUE (PropWF + "\execut_wf") (NO, 0, "PROGIWIN", "*SAISIE VENTE", YES, "GCO.entetcli" + CHR(1) + STRING(ROWID(entetcli)),"S","",OUTPUT pretwf).
                        DO TRANSACTION:
                            find entetcli where entetcli.cod_cli=int(cod_cli:screen-value in frame fonglet) and entetcli.typ_sai=type-saisie and entetcli.no_cde=int(no_cde:screen-value) and entetcli.no_bl=int(no_bl:screen-value) exclusive-lock no-wait no-error.
                            IF AVAIL entetcli THEN DO:
                                /* M�morisation infos avant suppression pour trigger delec_db */
                                entetcli.contrat = STRING("SAICL1_C","X(12)") + "INT$��".
                                /* $A67777 ... */
                                FOR EACH lisval WHERE lisval.typ_fich = "EC":U AND lisval.cod_tiers = STRING(entetcli.NO_cde) EXCLUSIVE-LOCK:
                                    DELETE lisval.
                                END.
                                /* ... $A67777 */
                                DELETE entetcli.
                            END.
                        END.
                    END.
                    /* ... $A99522 */
                end.
                else do:
                    ASSIGN
                        parnum=9
                        qcondition=?.
                    release entetcli.
                end.
            end.
            /* Modif */
            else do:
                retour=?.
                if entetcli.no_fact<>0 then message Traduction("Vous ne pouvez pas abandonner une facture d�j� �dit�e",-2,"") view-as alert-box info.
                else do:
                    IF (porigine = "6a" AND (wori_saibp1 OR crtHorsSaicl1 /* $A56248 */)) THEN /*$A36019*/
                    DO:
                        do transaction:
                            entetcli.achev=yes.
                            RUN maj-fin1.
                        end.
                    END.
                    ELSE
                    DO:
                        message Traduction("Vous abandonnez une pi�ce en cours.",-2,"") skip
                            Traduction("Si vous y avez apport� des changements, il est recommand� de valider et non d'abandonner.",-2,"") skip
                            Traduction("Voulez-vous la verrouiller pour l'interdire � tout acc�s (autre que par vous) ou tout traitement ?",-2,"") view-as alert-box question buttons yes-no-cancel update retour.
                        if not retour then do transaction:
                            IF entetcli.typ_sai<>"A" THEN entetcli.achev=yes.
                            RUN maj-fin1.
                        end.
                        IF porigine<>"6b" AND porigine <> "LOC":U AND hbrowse:num-iterations>0 then hbrowse:refresh().
                    END.
                END.
            end.
        end.

        else if creation then do transaction:
            /* m�morise quand m�me dans entetsup le n�cde et n�BL pour info */
            CREATE ENTETSUP.
            ASSIGN
                ENTETSUP.cod_cli  = entetcli.cod_cli:INPUT-VALUE
                ENTETSUP.typ_sai  = type-saisie
                ENTETSUP.no_cde   = entetcli.no_cde:INPUT-VALUE
                ENTETSUP.no_bl    = entetcli.no_bl:INPUT-VALUE
                ENTETSUP.USR_SUP  = G-USER
                ENTETSUP.DAT_SUP  = TODAY
                ENTETSUP.HR_SUP   = INT(SUBSTR(STRING(TIME,"hh:mm:ss"),1,2) + SUBSTR(STRING(TIME,"hh:mm:ss"),4,2) + SUBSTR(STRING(TIME,"hh:mm:ss"),7,2))
                ENTETSUP.PGM_SUP  = "SAICL1_C"
                ENTETSUP.TYPE_sup = "INT":U
                entetsup.vente    = YES
                entetsup.dat_cde  = entetcli.dat_cde:INPUT-VALUE IN FRAME fc1
                entetsup.qui      = qui$
                entetsup.ref_cde  = entetcli.ref_cde:INPUT-VALUE
                entetsup.not_ref  = entetcli.NOT_ref:INPUT-VALUE
                entetsup.dat_liv  = entetcli.dat_liv:INPUT-VALUE
                entetsup.depot    = edepot:INPUT-VALUE
                entetsup.civ_fac  = entetcli.civ_fac:INPUT-VALUE
                entetsup.adr_fac[1]  = entetcli.adr_fac[1]:INPUT-VALUE
                entetsup.adr_fac[2]  = entetcli.adr_fac[2]:INPUT-VALUE
                entetsup.adr_fac[3]  = entetcli.adr_fac[3]:INPUT-VALUE
                entetsup.villef   = entetcli.villef:INPUT-VALUE
                entetsup.paysf    = entetcli.paysf:INPUT-VALUE
                entetsup.k_post2f = entetcli.k_post2f:INPUT-VALUE
                entetsup.adrfac4  = entetcli.adrfac4:INPUT-VALUE
                entetsup.ndos     = g-ndos
                ENTETSUP.usr_crt  = G-USER
                ENTETSUP.dat_crt  = TODAY.

            for each echcli where echcli.cod_cli=int(entetcli.cod_cli:screen-value) and echcli.typ_sai=type-saisie and
                echcli.no_cde=int(entetcli.no_cde:screen-value) and echcli.no_bl=int(entetcli.no_bl:screen-value) :
                delete echcli.
            end.
            for each txtentcl where txtentcl.cod_cli=int(entetcli.cod_cli:screen-value) and txtentcl.typ_sai=type-saisie and
                txtentcl.no_cde=int(entetcli.no_cde:screen-value) and txtentcl.no_bl=int(entetcli.no_bl:screen-value) :
                IF txtentcl.lien_rtf > 0 THEN DO:
                    FIND FIRST rtf WHERE rtf.lien_rtf = txtentcl.lien_rtf EXCLUSIVE-LOCK NO-ERROR.
                    IF AVAIL rtf THEN DELETE rtf.
                END.
                delete txtentcl.
            end.
            Release entetsup.

            /*A49793...*/
            IF local_no_info <> 0 THEN DO:
                FOR EACH infoent WHERE infoent.typ_fich = "E" AND infoent.NO_info = local_no_info EXCLUSIVE-LOCK :
                    DELETE infoent.
                END.
                FOR EACH infolig WHERE infolig.typ_fich = "E" AND infolig.NO_info = local_no_info EXCLUSIVE-LOCK :
                    DELETE infolig.
                END.
            END.
            /*...A49793*/
        end.

        if retour<>? then do:
            IF porigine="6b" OR (porigine = "6a" AND (wori_saibp1 OR crtHorsSaicl1 /* $A56248 */)) /*$A36019*/ THEN APPLY "CLOSE":U TO THIS-PROCEDURE.
            ELSE DO:
                IF AVAIL entetcli THEN FIND CURRENT entetcli NO-LOCK.
                RUN Ecran1_Ok.
                IF parnum=9 THEN DO:
                    run openquery.
                    if avail entetcli then do:
                        if hbrowse:num-iterations>0 and hbrowse:num-iterations<>? then hbrowse:set-repositioned-row(hbrowse:num-iterations,"always").
                        /*HQuery:REPOSITION-TO-ROWID(svrowid) NO-ERROR.*/        /* $2240 */
                        hbrowse:QUERY:REPOSITION-TO-ROWID(svrowid) NO-ERROR. /* $2240 */
                        apply "value-changed" to hbrowse.
                    END.
                END.
                IF hbrowse:hidden=NO THEN apply "entry" to hbrowse.
                ELSE APPLY "ENTRY" TO cli$ IN FRAME f1.
            END.
        end.
        else do transaction:
            find entetcli where rowid(entetcli)=svrowid exclusive-lock no-wait no-error.
        end.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE appel-parval wsaicl1
PROCEDURE appel-parval :
DO with frame fc1:
    assign
        cc1=entetcli.cal_marg:SCREEN-VALUE
        pdetail=(if parsoc.ctva[2]=0 then no else yes)
        pphatot=(if ctva[3]=0 then no else yes)
        pphatxt=(if ctva[4]=0 then no else yes)
        pqte=(if ctva[5]=0 or ctva[5]=1 then no else yes)
        ppu=(if ctva[5]=0 or ctva[5]=2 then no else yes).

    valopx=parsoc.valo_px.
    if NOT creation THEN FOR FIRST cleval WHERE cleval.chapitre="PROGIWIN" AND cleval.domaine="MARGE_VTE" AND cleval.cod_user="PROGIWIN" AND cleval.section=STRING(entetcli.NO_cde) AND cleval.cle="valopx" NO-LOCK:
        valopx=cleval.valeur.
    END.

    /* $2059 ... */
    SetSessionData ("MARGE-FRAIS":U,"ON-GERE-LES-FRAIS":U,STRING(on-gere-les-frais)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-1":U,STRING(frais1-prc1)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-2":U,STRING(frais1-prc2)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-3":U,STRING(frais1-prc3)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-4":U,STRING(frais1-prc4)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-1":U,STRING(frais2-prc1)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-2":U,STRING(frais2-prc2)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-3":U,STRING(frais2-prc3)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-4":U,STRING(frais2-prc4)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-1":U,STRING(frais3-prc1)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-2":U,STRING(frais3-prc2)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-3":U,STRING(frais3-prc3)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-4":U,STRING(frais3-prc4)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-1":U,STRING(frais4-prc1)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-2":U,STRING(frais4-prc2)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-3":U,STRING(frais4-prc3)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-4":U,STRING(frais4-prc4)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-1":U,STRING(frais5-prc1)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-2":U,STRING(frais5-prc2)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-3":U,STRING(frais5-prc3)).
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-4":U,STRING(frais5-prc4)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-1":U,STRING(base-1)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-2":U,STRING(base-2)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-3":U,STRING(base-3)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-4":U,STRING(base-4)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-total":U,STRING(base-total)).
    /* ... $2059 */
    /* $2128... */
    SetSessionData ("MARGE-FRAIS":U,"BASE-1sec":U,STRING(base-1sec)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-2sec":U,STRING(base-2sec)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-3sec":U,STRING(base-3sec)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-4sec":U,STRING(base-4sec)).
    SetSessionData ("MARGE-FRAIS":U,"BASE-totalsec":U,STRING(base-totalsec)).
    /* ...$2128 */
    run parval_c ("",(if creation then type-saisie else entetcli.typ_sai) + valopx,
                  0,input-output cc1,input-output cm1,input-output cm2,input-output cm3,input-output cm4,input-output cm5,input-output kqt1,input-output kop1,input-output kva1,
                  input-output kpm1 /* $1630 */,output pmodpha,input-output pdetail,input-output pphatot,input-output pphatxt,input-output pqte,input-output ppu,/*$251*/"",/*$251*/"",output logbid).
    /* $2059 ... */
    IF logbid THEN DO:
        frais1-prc1 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-1":U)).
        frais1-prc2 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-2":U)).
        frais1-prc3 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-3":U)).
        frais1-prc4 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-4":U)).
        frais2-prc1 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-1":U)).
        frais2-prc2 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-2":U)).
        frais2-prc3 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-3":U)).
        frais2-prc4 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-4":U)).
        frais3-prc1 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-1":U)).
        frais3-prc2 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-2":U)).
        frais3-prc3 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-3":U)).
        frais3-prc4 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-4":U)).
        frais4-prc1 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-1":U)).
        frais4-prc2 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-2":U)).
        frais4-prc3 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-3":U)).
        frais4-prc4 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-4":U)).
        frais5-prc1 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-1":U)).
        frais5-prc2 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-2":U)).
        frais5-prc3 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-3":U)).
        frais5-prc4 = DEC (GetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-4":U)).
    END.
    /* ... $2059 */

    /* si commande en modif et changement param�tres �dition niveaux */
    if logbid and pmodpha and not creation then do transaction:
        session:set-wait-state("GENERAL").
        for each lignecli of entetcli where sous_type = "PH" and LIGNECLI.nmc_lie<>"S" :
            assign
                lignecli.zal[1] = (if pdetail then "O" else "")
                lignecli.total = pphatot
                lignecli.tot_txt = pphatxt
                /* $46 */
                lignecli.zal[2]=(if not ppu and not pqte then "0" else if ppu and not pqte then "1"
                    else if pqte and not ppu then "2" else "3").
        end.
        session:set-wait-state("").
    end.
    if logbid then do transaction: /*$498*/
      def var modcalc as log no-undo.
      assign modcalc=((not creation and entetcli.cal_marg:screen-val<>cc1) OR initmarges) initmarges = NO
      /*$400*/entetcli.cal_marg:screen-value=(if cc1="" then parsoc.coeff_pa else cc1) bpardev:hidden=(cc1="").
      if /* $A65059... */ (NOT creation OR modif-entete) AND /* ...$A65059 */ (modcalc or entetcli.coef_mar[1]<>cm1 or entetcli.coef_mar[2]<>cm2 or entetcli.coef_mar[3]<>cm3 or entetcli.coef_mar[4]<>cm4 or entetcli.coef_mar[5]<>cm5) then do:
          /*logbid=yes.
 *         message Traduction("Vous avez chang� le mode de calcul et/ou les coefficients de calcul des prix.",-2,"") skip
 *                 Traduction("Voulez-vous recalculer les prix et le montant de la pi�ce ?",-2,"")
 *                 view-as alert-box question buttons yes-no update logbid.
 *         if logbid then do:*/
          assign entetcli.coef_mar[1]=cm1 entetcli.coef_mar[2]=cm2 entetcli.coef_mar[3]=cm3
                 entetcli.coef_mar[4]=cm4 entetcli.coef_mar[5]=cm5 entetcli.cal_marg=cc1.
          /* $2059 ... */
          entetcli.det_frais = STRING(frais1-prc1) + CHR(2)
                           + STRING(frais1-prc2) + CHR(2)
                           + STRING(frais1-prc3) + CHR(2)
                           + STRING(frais1-prc4)
                           + CHR(1)
                           + STRING(frais2-prc1) + CHR(2)
                           + STRING(frais2-prc2) + CHR(2)
                           + STRING(frais2-prc3) + CHR(2)
                           + STRING(frais2-prc4)
                           + CHR(1)
                           + STRING(frais3-prc1) + CHR(2)
                           + STRING(frais3-prc2) + CHR(2)
                           + STRING(frais3-prc3) + CHR(2)
                           + STRING(frais3-prc4)
                           + CHR(1)
                           + STRING(frais4-prc1) + CHR(2)
                           + STRING(frais4-prc2) + CHR(2)
                           + STRING(frais4-prc3) + CHR(2)
                           + STRING(frais4-prc4)
                           + CHR(1)
                           + STRING(frais5-prc1) + CHR(2)
                           + STRING(frais5-prc2) + CHR(2)
                           + STRING(frais5-prc3) + CHR(2)
                           + STRING(frais5-prc4).
          /* ... $2059 */
          run recalcul-global.
          /* $952 */
          SESSION:SET-WAIT-STATE("general").
          IF entetcli.cal_marg <> "" /*OR (entetcli.fac_pxa AND entetcli.maj_ach<>0)*/ THEN RUN calcmarg (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,"P").
          RUN maj_phase (entetcli.cod_cli,entetcli.NO_cde,entetcli.NO_bl,entetcli.typ_sai).
          SESSION:SET-WAIT-STATE("").
        /*end.*/
      end.
    end.

    /* $2059 ... */
    SetSessionData ("MARGE-FRAIS":U,"BASE-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-4":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-TOTAL":U,"").
    /* $2128... */
    SetSessionData ("MARGE-FRAIS":U,"BASE-1sec":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-2sec":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-3sec":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-4sec":U,"").
    SetSessionData ("MARGE-FRAIS":U,"BASE-TOTALsec":U,"").
    /* ...$2128 */
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS1-PRC-4":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS2-PRC-4":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS3-PRC-4":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS4-PRC-4":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-1":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-2":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-3":U,"").
    SetSessionData ("MARGE-FRAIS":U,"FRAIS5-PRC-4":U,"").
    /* ... $2059 */

END. /*fin $498*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AppelExterieurMarge wsaicl1
PROCEDURE AppelExterieurMarge :
DEF INPUT PARAM pservice AS CHAR no-undo.
    DEF INPUT PARAM pdonnee AS CHAR no-undo.
    IF pservice = "init" AND pdonnee = "" THEN DO:
        initmarges = YES.
        /* Initialisation des coeffs de la pi�ces */
        FOR EACH lignecli OF entetcli WHERE LIGNECLI.nmc_lie<>"S":
            ASSIGN lignecli.cal_marg="" lignecli.coef_mar[1]=0 lignecli.coef_mar[2]=0 lignecli.coef_mar[3]=0 lignecli.coef_mar[4]=0 lignecli.coef_mar[5]=0.
        END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-bXML wsaicl1
PROCEDURE CHOOSE-bXML : /*$A68906*/
DO WITH FRAME fc1:
DEF VAR iLigne AS INT    NO-UNDO.
DEF VAR iCpt   AS INT    NO-UNDO.
DEF VAR iQuery AS HANDLE NO-UNDO.
DEF VAR ListeRowId AS CHAR NO-UNDO INIT "".
   iQuery = HBrowse:QUERY.
   iQuery:GET-LAST(NO-LOCK) NO-ERROR.
   iLigne = iQuery:NUM-RESULTS NO-ERROR.
   IF iLigne = ? THEN iLigne = 0.
   IF iLigne > 0 THEN DO:
      iQuery:GET-FIRST(NO-LOCK) NO-ERROR.
      REPEAT WHILE NOT iQuery:QUERY-OFF-END :
         ListeRowId = ListeRowId + STRING(iQuery:GET-BUFFER-HANDLE(1):ROWID) + "," NO-ERROR.
         iQuery:GET-NEXT(NO-LOCK) NO-ERROR.
      END.
   END.
   IF listeRowid <> "" THEN RUN getXml_GP("entetcli":U,"",RIGHT-TRIM(listeRowid,","),"client":U,INT(cli$:SCREEN-VAL IN FRAME F1)).
   ELSE MESSAGE traduction("Traitement impossible !",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK TITLE Traduction("Gestion des commandes",-2,"").
   HBrowse:SET-REPOSITIONED-ROW(1) NO-ERROR.
   APPLY "entry" TO HBrowse.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE bappdev wsaicl1
PROCEDURE bappdev :
DEF VAR px-obj as dec decimals 4 no-undo.
DO WITH FRAME fc1:
    IF entetcli.devise:SCREEN-VAL <>"" THEN DO:
        SESSION:SET-WAIT("general").
        parnum=g-ndv.
        IF entetcli.devise:SCREEN-VAL<>g-dftdev THEN DO:
          FIND FIRST tabcomp WHERE tabcomp.type_tab="DE" AND tabcomp.a_tab=entetcli.devise:SCREEN-VAL NO-LOCK NO-ERROR.
          IF AVAIL tabcomp THEN parnum=tabcomp.deb_ce.
        END.
        IF entetcli.devise:SCREEN-VAL=g-dftdev OR (entetcli.devise:SCREEN-VAL<>g-dftdev AND AVAIL tabcomp) THEN FOR EACH lignecli OF entetcli SHARE-LOCK:
            IF entetcli.devise:SCREEN-VAL<>lignecli.dev_vte AND CAN-DO("AR,HS,PS",lignecli.sous_type) THEN DO:
                /*$BOIS...*/
                IF isproduitbois(lignecli.typ_elem) THEN DO:
                    IF lignecli.nmc_lie = "" THEN DO:
                        if lignecli.dev_vte=g-dftdev or entetcli.devise:screen-val=g-dftdev then do:
                            if lignecli.dev_vte=g-dftdev then do:
                                run conv-dev ("P","F","V",entetcli.devise:screen-val,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,lignecli.px_refv,output px-obj).
                            END.
                            else do:
                                run conv-dev ("P","D","V",lignecli.dev_vte,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,lignecli.px_refv,output px-obj).
                            END.
                        end.
                        else do:
                            run conv-dev ("P","D","V",lignecli.dev_vte,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,lignecli.px_refv,output px-obj).
                            run conv-dev ("P","F","V",entetcli.devise:screen-val,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,px-obj,output px-obj).
                        end.
                        lignecli.dev_vte=entetcli.devise:SCREEN-VAL.
                        maj-prix-ligne-commande-bois(ROWID(lignecli),px-obj).
                    END.
                END.
                /*...$BOIS*/
                ELSE DO:
                    if lignecli.dev_vte=g-dftdev or entetcli.devise:screen-val=g-dftdev then do:
                        if lignecli.dev_vte=g-dftdev then do:
                            run conv-dev ("P","F","V",entetcli.devise:screen-val,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,lignecli.px_vte,output px-obj).
                            /* $TX ... */
                            FOR EACH preslc WHERE preslc.typ_cf = "C" AND preslc.cod_cf = entetcli.cod_cli AND preslc.typ_sai = entetcli.typ_sai AND preslc.no_cde = entetcli.no_cde AND preslc.no_bl = entetcli.no_bl AND preslc.n_surlig = lignecli.n_surlig EXCLUSIVE-LOCK:
                                RUN conv-dev ("P","F","V",entetcli.devise:SCREEN-VAL,DATE(entetcli.dat_px:SCREEN-VAL),entetcli.tx_ech_d:INPUT-VALUE,preslc.cout,OUTPUT preslc.cout).
                            END.
                            /* ... $TX */
                        END.
                        else do:
                            run conv-dev ("P","D","V",lignecli.dev_vte,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,lignecli.px_vte,output px-obj).
                            /* $TX ... */
                            FOR EACH preslc WHERE preslc.typ_cf = "C" AND preslc.cod_cf = entetcli.cod_cli AND preslc.typ_sai = entetcli.typ_sai AND preslc.no_cde = entetcli.no_cde AND preslc.no_bl = entetcli.no_bl AND preslc.n_surlig = lignecli.n_surlig EXCLUSIVE-LOCK:
                                RUN conv-dev ("P","D","V",lignecli.dev_vte,DATE(entetcli.dat_px:SCREEN-VAL),entetcli.tx_ech_d:INPUT-VALUE,preslc.cout,OUTPUT preslc.cout).
                            END.
                            /* ... $TX */
                        END.
                    end.
                    else do:
                        /* Laissez 0 dans le tx change car dans ce cas-l� la devise est <> devise ent�te et <> devise soci�t� */
                        run conv-dev ("P","D","V",lignecli.dev_vte,date(entetcli.dat_px:screen-val),0,lignecli.px_vte,output px-obj).
                        run conv-dev ("P","F","V",entetcli.devise:screen-val,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,px-obj,output px-obj).
                        /* $TX ... */
                        FOR EACH preslc WHERE preslc.typ_cf = "C" AND preslc.cod_cf = entetcli.cod_cli AND preslc.typ_sai = entetcli.typ_sai AND preslc.no_cde = entetcli.no_cde AND preslc.no_bl = entetcli.no_bl AND preslc.n_surlig = lignecli.n_surlig EXCLUSIVE-LOCK:
                            /* Laissez 0 dans le tx change car dans ce cas-l� la devise est <> devise ent�te et <> devise soci�t� */
                            run conv-dev ("P","D","V",lignecli.dev_vte,date(entetcli.dat_px:screen-val),0,preslc.cout,output preslc.cout).
                            run conv-dev ("P","F","V",entetcli.devise:screen-val,date(entetcli.dat_px:screen-val),entetcli.tx_ech_d:INPUT-VALUE,preslc.cout,output preslc.cout).
                        END.
                        /* ... $TX */
                    end.
                    ASSIGN lignecli.px_vte=px-obj lignecli.dev_vte=entetcli.devise:SCREEN-VAL.
                    {lignecl5.i lignecli entetcli}
                END.
            END.
        END.
        SESSION:SET-WAIT("").
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-fcc1 wsaicl1
PROCEDURE br-fcc1 :
do with frame fc1:
    run rectc_b (self:private-data, output parlib, output parcod).
    if parcod <> "" then do:
         /*A35891..*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (self:PRIVATE-DATA, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        if self:name="br-pa" then assign entetcli.paysf:screen-value = parcod lib-pa:screen-value = parlib.
        else if self:name="br-de" then do:
            assign entetcli.tx_ech_d:hidden=yes entetcli.devise:screen-value = parcod.
            find TABCOMP where TABCOMP.type_tab = "DE" and TABCOMP.a_tab = input entetcli.devise no-lock no-error.
            if available TABCOMP then do:
                if tabcomp.a_tab<>g-dftdev and (tabcomp.dev_in=? or input entetcli.dat_px<tabcomp.dev_in) then do:
                    run rch-tx-devise-vente.
                    entetcli.tx_ech_d:hidden=no.
                end.
            end.
        end.
        else if self:name="br-lg" then assign entetcli.langue:screen-value = parcod lib-lg:screen-value = parlib.
        else if self:name="br-ci" then assign entetcli.civ_fac:screen-value = parcod lib-ci:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-fcg1 wsaicl1
PROCEDURE br-fcg1 :
do with frame fc1:
  if self:name="br-tar" then do:
    IF parsoc.ges_loc /*AND type-saisie = "D" $A50403*/ AND entetcli.location:CHECKED THEN SetSessionData("location":U, "pxrfvt_b%location","yes").
    run rectar_b (0, output parnum, output parlib).
    IF parsoc.ges_loc /*AND type-saisie = "D" $A50403*/ AND entetcli.location:CHECKED THEN SetSessionData("location":U, "pxrfvt_b%location","").
    if parnum <> 0 then do:
      entetcli.no_tarif:screen-val=STRING(parnum).
      RUN trt-tarif.
      apply "TAB" TO SELF.
    end.
  end.
  else do:
    parcod = "".  /*$2104*/
    if self:name="br-aff" then run recaff_b (int(entetcli.cod_cli:screen-value in frame fonglet), output parlib, output parcod).
    ELSE if self:name="br-aa" then /*$2104...run recmg_b (12,output parlib,output parcod).*/ RUN VALUE(TPS_CHEMINPGM("recintequ_tps"))(Traduction("Recherche apporteurs d'affaires",-2,""), "I", NO, "tppersonne.FCT_AA = YES", ?, ?, ?, ?, ?, INPUT-OUTPUT parcod, OUTPUT parlib).  /*$2104*/
    ELSE if self:name="br-rp1" then /*$2104...run recmg_b (11,output parlib,output parcod).*/ RUN VALUE(TPS_CHEMINPGM("recintequ_tps"))(Traduction("Recherche commerciaux",-2,""), "I", NO, "tppersonne.FCT_COM = YES", ?, ?, ?, ?, ?, INPUT-OUTPUT parcod, OUTPUT parlib).  /*$2104*/
    else run rectg_b (self:private-data, output parlib, output parcod).
    if parcod <> "" then do:
         /*A35891..*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (self:PRIVATE-DATA, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        if self:name="br-aa" then assign entetcli.app_aff:screen-value = parcod lib-aa:screen-value = parlib.
        else if self:name="br-rp1" then assign entetcli.commerc[1]:screen-value = parcod lib-rp1:screen-value = parlib.
        else if self:name="br-fc" then assign entetcli.famille:screen-value = parcod lib-fc:screen-value = parlib.
        else if self:name="br-gc" then assign entetcli.groupe:screen-value = parcod lib-gc:screen-value = parlib.
        else if self:name="br-rg" then assign entetcli.region:screen-value = parcod lib-rg:screen-value = parlib.
        else if self:name="br-rv" then assign entetcli.regime:screen-value = parcod lib-rv:screen-value = parlib.
        else if self:name="br-tf" then assign entetcli.type_fac:screen-value = parcod lib-tf:screen-value = parlib.
        else if self:name="br-oc" then DO:
            assign
                entetcli.ori_cde:screen-value = parcod
                lib-oc:screen-value = parlib.
            IF parsoc.ges_can AND parsoc.par_can="O":U THEN DO:
                FIND tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.a_tab = entetcli.ori_cde:SCREEN-VAL NO-LOCK NO-ERROR.
                RUN affichage-canal.
            END.
        END.
        else if self:name="br-nc" then do:
            assign entetcli.nat_cde:screen-value = parcod lib-nc:screen-value = parlib.
            IF parsoc.ges_can AND parsoc.par_can="N":U THEN DO:
                FIND tabgco WHERE tabgco.TYPE_tab = "NC" AND tabgco.a_tab = entetcli.nat_cde:SCREEN-VAL NO-LOCK NO-ERROR.
                RUN affichage-canal.
            END.
            IF lgTextile THEN do:
                RUN SAISON-NATURE.
                RUN DATPX-SAISON.
            END.
        END.
        else if self:name="br-kv" then assign entetcli.canal:screen-value = parcod lib-kv:screen-value = parlib.
        else do:
            assign entetcli.affaire:screen-value = parcod lib-aff:screen-value = parlib bcreer-aff:sensitive = NO.
            APPLY 'LEAVE' TO entetcli.affaire.
        END.
        apply "TAB" TO SELF.
    END.
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-fcg2 wsaicl1
PROCEDURE br-fcg2 :
do with frame fc2:
    if self:name="br-rp2" THEN DO:
        /*$2104...*/
        parcod = "".
       /*run recmg_b (11,output parlib,output parcod).*/ RUN VALUE(TPS_CHEMINPGM("recintequ_tps"))(Traduction("Recherche commerciaux",-2,""), "I", NO, "tppersonne.FCT_COM = YES", ?, ?, ?, ?, ?, INPUT-OUTPUT parcod, OUTPUT parlib).  /*$2104*/
    END.
    ELSE IF SELF:NAME="brcchq" THEN DO:
        parcod = RIGHT-TRIM(radical-5,'*').
        RUN RECCPT_b (input-output parcod, input "", yes, "", output parlib).
    END.
    ELSE run rectg_b (self:private-data, output parlib, output parcod).
    if parcod <> "" then do:
        /*A35891..*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (self:private-data, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        CASE self:name:
            WHEN "br-ct"  then assign entetcli.cat_tar:screen-value = parcod lib-ct:screen-value = parlib.
            WHEN "br-rp2" THEN assign entetcli.commerc[2]:screen-value = parcod lib-rp2:screen-value = parlib.
            WHEN "br-ss"  THEN do:
                ASSIGN entetcli.saison:screen-value = parcod lib-ss:screen-value = parlib.
                RUN DATPX-SAISON.
            END.
            WHEN "br-gpres"  THEN ASSIGN entetcli.grp_ps:screen-value = parcod lib-gpres:screen-value = parlib.
            WHEN "brcchq" THEN ASsign entetcli.cpt_bq:SCREEN-VALUE = parcod lib-cchq:SCREEN-VALUE = parlib.
        END CASE.
        apply "TAB" TO SELF.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-kp wsaicl1
PROCEDURE br-kp :
do with frame fc1:
    parcod = entetcli.k_post2f:screen-value.
    run reccpv_b (input-o parcod,yes, yes, output parlib).
    if parlib<>"" then assign
        entetcli.villef:screen-value = parlib
        entetcli.k_post2f:screen-value=parcod
        CodePostal = parcod.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-sc wsaicl1
PROCEDURE br-sc :
do with frame fc1:
    IF entetcli.famille:screen-value="" THEN MESSAGE Traduction("Vous devez renseigner la hi�rarchie client amont !",-2,"")
        VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        parcod = "".
        run rectg_b ("SC" + entetcli.famille:screen-value, output parlib, output parcod).
        if parcod <> "" then do:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SC", STRING(entetcli.famille:screen-value,"X(3)") + parcod, g-langue).
            IF intiTab <> "" THEN parlib = intiTab.
            assign
                entetcli.s2_famille:screen-value = parcod
                lib-sc:screen-value = parlib.
            apply "TAB" TO SELF.
        END.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-sd wsaicl1
PROCEDURE br-sd :
do with frame fc1:
    IF entetcli.famille:screen-value="" OR entetcli.s2_famille:screen-value="" THEN MESSAGE Traduction("Vous devez renseigner la hi�rarchie client amont !",-2,"")
        VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        parcod = "".
        run rectg_b ("SD" + STRING(entetcli.famille:screen-value,"X(3)") + entetcli.s2_famille:screen-value, output parlib, output parcod).
        if parcod <> "" then do:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SD", STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:screen-value,"X(3)") + parcod, g-langue).
            IF intiTab <> "" THEN parlib = intiTab.
            assign
                entetcli.s3_famille:screen-value = parcod
                lib-sd:screen-value = parlib.
            apply "TAB" TO SELF.
        END.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-se wsaicl1
PROCEDURE br-se :
do with frame fc1:
    IF entetcli.famille:screen-value="" OR entetcli.s2_famille:screen-value="" OR entetcli.s3_famille:screen-value="" THEN MESSAGE Traduction("Vous devez renseigner la hi�rarchie client amont !",-2,"")
        VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        parcod = "".
        run rectg_b ("SE" + STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:screen-value,"X(3)") + entetcli.s3_famille:screen-value, output parlib, output parcod).
        if parcod <> "" then do:
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SE", STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:screen-value,"X(3)") + STRING(entetcli.s3_famille:screen-value,"X(3)") + parcod, g-langue).
            IF intiTab <> "" THEN parlib = intiTab.
            assign
                entetcli.s4_famille:screen-value = parcod
                lib-se:screen-value = parlib.
            apply "TAB" TO SELF.
        END.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-val-fc1 wsaicl1
PROCEDURE br-val-fc1 :
do with frame fc1:
    run rectg_b (self:private-data, output parlib, output parcod).
    if parcod <> "" then do:
        /*A35891..*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (self:PRIVATE-DATA, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        if self:name="br-xt1" then assign xt1:screen-value = parcod lib-xt1:screen-value = parlib.
        else if self:name="br-xt2" then assign xt2:screen-value = parcod lib-xt2:screen-value = parlib.
        else if self:name="br-xt3" then assign xt3:screen-value = parcod lib-xt3:screen-value = parlib.
        else if self:name="br-xt4" then assign xt4:screen-value = parcod lib-xt4:screen-value = parlib.
        else if self:name="br-xt5" then assign xt5:screen-value = parcod lib-xt5:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-val-fval wsaicl1
PROCEDURE br-val-fval :
do with frame fval:
    run rectg_b (self:private-data, output parlib, output parcod).
    if parcod <> "" then do:
        /*A35891..*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (self:PRIVATE-DATA, parcod, g-langue).
        IF intiTab <> "" THEN parlib = intiTab.
        /*...A35891*/
        if self:name="br-zt1" then assign zt1:screen-value = parcod lib-zt1:screen-value = parlib.
        else if self:name="br-zt2" then assign zt2:screen-value = parcod lib-zt2:screen-value = parlib.
        else if self:name="br-zt3" then assign zt3:screen-value = parcod lib-zt3:screen-value = parlib.
        else if self:name="br-zt4" then assign zt4:screen-value = parcod lib-zt4:screen-value = parlib.
        else if self:name="br-zt5" then assign zt5:screen-value = parcod lib-zt5:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE br-vcl wsaicl1
PROCEDURE br-vcl :
do with frame fc2:
    parcod=(if self:name="brcgrp" then input vcl_grp
        else if self:name="brcfact" then input vcl_fact
        else if self:name="brclivre" then input vcl_livre
        else if self:name="brcstat" then input vcl_stat
        else if self:name="brcpln" then input vcl_pln
        else if self:name="brcplr" then input vcl_plr
        else if self:name="brcpaye" then input vcl_paye
        else input vc_factor).
    run reccli_b ("C","3,9",input-output parcod, output parlib).

    /* A48827 */
    if self:name="brcfact" AND int(vcl_fact)<>int(parcod) THEN RUN chg-fact.
    else if self:name="brclivre" AND int(vcl_livre)<>int(parcod) THEN DO:
        IF NOT creation THEN ChangementClientLivre = YES. /*$A65359*/
        RUN chg-livre.
    END.
    else if self:name="brcpaye" AND int(vcl_paye)<>int(parcod) THEN RUN chg-paye.

    if parcod <> "" then do:
        if self:name="brcgrp" then assign vcl_grp:screen-value = parcod LIB-grp:screen-value = parlib.
        else if self:name="brcfact" then assign vcl_fact vcl_fact:screen-value = parcod LIB-fact:screen-value = parlib.
        else if self:name="brclivre" then assign vcl_livre vcl_livre:screen-value = parcod LIB-livre:screen-value = parlib.
        else if self:name="brcstat" then assign vcl_stat:screen-value = parcod LIB-stat:screen-value = parlib.
        else if self:name="brcpln" then assign vcl_pln:screen-value = parcod LIB-pln:screen-value = parlib.
        else if self:name="brcplr" then assign vcl_plr:screen-value = parcod LIB-plr:screen-value = parlib.
        else if self:name="brcpaye" then assign vcl_paye vcl_paye:screen-value = parcod LIB-paye:screen-value = parlib.
        else assign vc_factor:screen-value = parcod LIB-factor:screen-value = parlib.
        apply "TAB" TO SELF.
    END.
    IF ChangementClientLivre THEN RUN Choose-badrliv. /*$A65359*/
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cacher-champs wsaicl1
PROCEDURE cacher-champs :
DO WITH FRAME f1:
    IF valid-handle(hbrowse) THEN HIDE hbrowse.
    ASSIGN
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bmodifier}, NO)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bsupprimer}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-interne}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-arc}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-proforma}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-bp}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bsaisie-liv}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-devis}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-facture}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bvalider-bl}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&breediter-bl}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bannuler-bp}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bdc}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bcd}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bcbl}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bavan}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bfax}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&b-mail}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&bsms}, NO) /* $A80432 */
        Logbid = CBShowItem (Hcombars-f1, 2, {&bverifpx}, NO) /*$2026*/
        Logbid = CBShowItem (Hcombars-f1, 2, {&bdetailtemps}, NO) /*$A39538*/
        Logbid = CBShowItem (Hcombars-f1, 2, {&bproforma-f}, NO) /*$A20038*/
        Logbid = CBShowItem (Hcombars-f1, 2, {&bproforma-m}, NO). /*$A20038*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CacherChildInfo wsaicl1
PROCEDURE CacherChildInfo :
IF VALID-HANDLE(child-info) THEN RUN ChildBottom IN child-info. /* $2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calcul-gar wsaicl1
PROCEDURE calcul-gar :
/* $A52966 */
    DEF INPUT-OUTPUT  PARAMETER wdat AS DATE        NO-UNDO.

    DEF VAR mm   AS INT NO-UNDO.
    DEF VAR yy   AS INT NO-UNDO.
    DEF VAR ff   AS INT EXTENT 12 INIT [31,28,31,30,31,30,31,31,30,31,30,31] NO-UNDO.
    DEF VAR jj   AS INT NO-UNDO.

    lignegar.dat_deb = wdat.

    IF lignegar.unit_gar = "M" OR lignegar.unit_gar = "A" THEN DO:

        ASSIGN
            mm = MONTH(wdat)
            yy = YEAR(wdat).

        CASE lignegar.unit_gar:
            WHEN "M" THEN mm = mm + lignegar.jour_gar.
            WHEN "A" THEN yy = yy + lignegar.jour_gar.
        END CASE.

        DO WHILE mm > 12:
            ASSIGN mm = mm - 12
                   yy = yy + 1.
        END.

        jj = DAY(wdat).
        IF jj > ff[mm] THEN jj = ff[mm].
        wdat = DATE(mm,jj,yy).

    END.
    ELSE IF lignegar.unit_gar = "J" THEN wdat = wdat + lignegar.jour_gar.
    ELSE IF lignegar.unit_gar = "S" THEN wdat = wdat + (lignegar.jour_gar * 7).


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chapitre-suivprec wsaicl1
PROCEDURE chapitre-suivprec :
myonglet:tabs (chap-cours):SELECTED = YES.
    RUN ENVOI-CHAPITRE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chg-fact wsaicl1
PROCEDURE chg-fact :
/* A48827 */
    X = IF INT(parcod)<>0 THEN int(parcod) ELSE cle-cours.
    FIND clientr WHERE clientr.COD_CLI = x NO-LOCK NO-ERROR.
    FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = x NO-LOCK NO-ERROR.
    /* Client factur� */
    IF AVAIL clientr THEN
        /* entetcli.REGIME:SCREEN-VAL IN FRAME fc1 = string(clientr.regime) A48827 */
        entetcli.liasse:SCREEN-VAL IN FRAME fc2 = string(clientr.liasse).
    IF AVAIL cptclir THEN DO:
        /*$A64171*/
        IF int(vcl_paye) = 0
            THEN ASSIGN entetcli.CODE_REG:SCREEN-VAL IN FRAME Fc1 = string(cptclir.code_reg)
                        entetcli.CODE_ECH:SCREEN-VAL   = string(cptclir.code_ech).

        ASSIGN entetcli.civ_fac:SCREEN-VAL    = cptclir.CIVILITE
               entetcli.ADR_FAC[1]:SCREEN-VAL = cptclir.NOMCLI
               entetcli.ADR_FAC[2]:SCREEN-VAL = cptclir.ADRESSE[1]
               entetcli.ADR_FAC[3]:SCREEN-VAL = cptclir.ADRESSE[2]
               entetcli.ADRFAC4:SCREEN-VAL    = cptclir.ADRESSE4
               entetcli.VILLEF:SCREEN-VAL     = cptclir.VILLE
               entetcli.paysf:SCREEN-VAL      = cptclir.PAYS
               entetcli.langue:SCREEN-VAL     = cptclir.langue
               entetcli.k_post2f:SCREEN-VAL   = cptclir.k_post2
               entetcli.num_tel:SCREEN-VAL IN FRAME Fc2 = cptclir.num_tel
               entetcli.num_fax:SCREEN-VAL = cptclir.num_fax
               entetcli.CPT_BQ:SCREEN-VAL  = string(cptclir.cpt_bq).
    END.
    CodePostal = entetcli.k_post2f:SCREEN-VAL.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chg-livre wsaicl1
PROCEDURE chg-livre :
/* A48827 */
    X = IF INT(parcod)<>0 THEN int(parcod) ELSE cle-cours.
    FIND clientr WHERE clientr.COD_CLI = x NO-LOCK NO-ERROR.
    /* Client livr� */
    IF AVAIL clientr THEN
        entetcli.REGIME:SCREEN-VAL IN FRAME fc1 = string(clientr.regime).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE chg-paye wsaicl1
PROCEDURE chg-paye :
/* A48827 */
    X = IF INT(parcod)<>0 THEN int(parcod) ELSE IF int(vcl_fact) <> 0 THEN int(vcl_fact) /*$A76223*/ ELSE cle-cours.
    FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = x NO-LOCK NO-ERROR.
    /* Client payeur */
    /* IF AVAIL clientr THEN DISP clientr.regime @ entetcli.REGIME WITH FRAME fc1. A48827 */
    IF AVAIL cptclir THEN ASSIGN
        entetcli.CODE_REG:SCREEN-VAL IN FRAME fc1 = string(cptclir.code_reg)
        entetcli.CODE_ECH:SCREEN-VAL IN FRAME fc1 = string(cptclir.code_ech)
        entetcli.CPT_BQ:SCREEN-VAL IN FRAME fc2 = string(cptclir.cpt_bq).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-B-mail wsaicl1
PROCEDURE Choose-B-mail :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        IF NOT VALID-HANDLE (g-hdocs) THEN RUN docs-api PERSISTENT SET g-hdocs.
        rentetcli = PCL_GET_ROWID("SAICL1":U).   /* $2240 */
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
        IF Cormail THEN RUN Prefax("C",ROWID(Entetcli),OUTPUT Retour).
                  ELSE REtour = YES.
        IF Retour THEN CASE entetcli.typ_sai:
            WHEN "D" THEN
                IF PCL_CTRL_ACCES_EDT_DEV(ROWID(entetcli)) THEN DO:
                    SetSessionData ("EDTDEV","P%edtdev%Pdocasso":U,STRING (docmaildev)).
                    IF docmaildev THEN SetSessionData ("DOCS":U,"P%EDTDEV_c%HandleApi",STRING (g-hdocs)).
                    /* RUN CacherChildInfo. 4.4 */
                    RUN PCL_ENVOI_DEV_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli),NO,(IF Wmailoffre THEN "O"
                    ELSE IF Wmaildev AND NOT (devniv1 OR devniv2 OR devniv3) THEN "D"
                    ELSE IF NOT Wmaildev AND (devniv1 OR devniv2 OR devniv3) THEN "R"
                    ELSE IF Wmaildev AND (devniv1 OR devniv2 OR devniv3) THEN "T"
                    ELSE "D"),
                   (IF devniv1 THEN 1
                    ELSE IF devniv2 THEN 2
                    ELSE 3)).
                    /* RUN refresh-hbrowse. 4.4 */
                    SetSessionData ("DOCS":U,"P%EDTDEV_c%HandleApi","").
                    SetSessionData ("EDTDEV","P%edtdev%Pdocasso":U,"").
                    IF VALID-HANDLE(g-hdocs) THEN APPLY "close" TO g-hdocs. /*$A49610*/
                END.
            WHEN "C" THEN DO:
                IF CBGetItemShowState (Hcombars-f1, 2, {&bproforma-m}) AND Proforma-mail AND PCL_CTRL_ACCES_EDT_PROFORMA(ROWID(entetcli)) THEN RUN PCL_ENVOI_PROFORMA_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), NO). /*$A20038*/
                ELSE IF PCL_CTRL_ACCES_EDT_ARC(ROWID(entetcli)) THEN DO :
                    SetSessionData ("EDTARC","P%edtarc%Pdocasso":U,STRING (docmailarc)).
                    IF docmailarc THEN SetSessionData ("DOCS":U,"P%EDTarc_c%HandleApi",STRING (g-hdocs)).
                    RUN PCL_ENVOI_ARC_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), NO).
                    SetSessionData ("DOCS":U,"P%EDTarc_c%HandleApi","").
                    SetSessionData ("EDTARC","P%edtarc%Pdocasso":U,"").
                    IF VALID-HANDLE(g-hdocs) THEN APPLY "close" TO g-hdocs. /*$A49610*/
                END.
            END.
            WHEN "F" OR WHEN "R" THEN do:
                IF entetcli.NO_fact = 0 THEN DO:
                    IF PCL_CTRL_ACCES_EDT_BL(ROWID(entetcli)) THEN DO:
                        SetSessionData ("EDTBL","P%edtbl%Pdocasso":U,STRING (docmailBL)).
                        IF docmailBL THEN SetSessionData ("DOCS":U,"P%EDTBL_c%HandleApi",STRING (g-hdocs)).
                        /* RUN CacherChildInfo. 4.4 */
                        RUN PCL_ENVOI_BL_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), NO).
                        /* RUN refresh-hbrowse. 4.4 */
                        SetSessionData ("DOCS":U,"P%EDTBL_c%HandleApi","").
                        SetSessionData ("EDTBL","P%edtBl%Pdocasso":U,"").
                        IF VALID-HANDLE(g-hdocs) THEN APPLY "close" TO g-hdocs. /*$A49610*/
                    END.
                END.
                ELSE DO:
                    IF PCL_CTRL_ACCES_EDT_FAC(ROWID(entetcli)) THEN DO :
                        SetSessionData ("EDTFAC","P%edtfac%Pdocasso":U,STRING (docmailfac)).
                        IF docmailfac THEN SetSessionData ("DOCS":U,"P%EDTFAC_c%HandleApi",STRING (g-hdocs)).
                        RUN PCL_ENVOI_FACTURE_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), NO).
                        SetSessionData ("DOCS":U,"P%EDTfac_c%HandleApi","").
                        SetSessionData ("EDTFAC","P%edtfac%Pdocasso":U,"").
                        IF VALID-HANDLE(g-hdocs) THEN APPLY "close" TO g-hdocs. /*$A49610*/
                    END.
                END.
            END.
        END CASE.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-babandonner-adrliv wsaicl1
PROCEDURE Choose-babandonner-adrliv :
/* $A38935... */
    /*$A35969*/
    ASSIGN
        FrmComBars-fentete:SENSITIVE = YES
        FRAME frame-adrliv:HIDDEN = YES
        CtrlFrame:SENSITIVE = YES.
    CASE chap-cours:
        WHEN 1 THEN FRAME fc1:SENSITIVE = YES.
        WHEN 2 THEN frame fc2:SENSITIVE = YES.
        WHEN 3 THEN FRAME fval:SENSITIVE = YES.
    END CASE.
    /* ...$A38935 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-badrliv wsaicl1
PROCEDURE choose-badrliv :
/*
        ������ NE PAS AFFICHER LA FRAME EN DIALOG-BOX ������
        Voir fiche $A38935
    */

    /* $A38935... */
    ASSIGN
        CtrlFrame:SENSITIVE = NO
        FrmComBars-fentete:SENSITIVE = NO.
    CASE chap-cours:
        WHEN 1 THEN FRAME fc1:SENSITIVE = NO.
        WHEN 2 THEN frame fc2:SENSITIVE = NO.
        WHEN 3 THEN FRAME fval:SENSITIVE = NO.
    END CASE.
    /* ...$A38935 */

/*$A35969*/
    ENABLE badresse entetcli.civ_liv br-ciliv entetcli.adr_liv[1] entetcli.adr_liv[2] entetcli.adr_liv[3] entetcli.adrliv4 entetcli.k_post2l br-kpliv entetcli.villel entetcli.paysl br-paliv entetcli.zon_geo br-zg entetcli.typ_veh entetcli.typ_con br-tc
        WITH FRAME frame-adrliv /*VIEW-AS DIALOG-BOX $A38935 */.
    DISPLAY  ft$titre1 ft$titre2 WITH FRAME frame-adrliv.

    /*{centrer.i "frame frame-adrliv"} $A38935 */

    IF NOT adrliv-modifie /*$A65359*/ OR ChangementClientLivre THEN DO:
        IF creation /*$A65359*/ OR ChangementClientLivre THEN DO:
            ChangementClientLivre = NO. /*$A65359*/
            IF parsoc.mult_soc THEN DO:
                find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE IN FRAME fc2) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes and adresse.adr_def=yes AND adresse.ndos=g-ndos no-lock no-error.
                IF NOT AVAIL adresse THEN
                    /* Recherche adresse livraison (non principale) */
                    find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes AND adresse.ndos=g-ndos no-lock no-error.
                    IF NOT AVAIL adresse THEN
                        find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes and adresse.adr_def=yes AND adresse.ndos=0 no-lock no-error.
                        IF NOT AVAIL adresse THEN
                            /* Recherche adresse livraison (non principale) */
                            find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes AND adresse.ndos=0 no-lock no-error.
            END.
            ELSE DO:
                find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes and adresse.adr_def=yes no-lock no-error.
                IF NOT AVAIL adresse THEN
                    /* Recherche adresse livraison (non principale) */
                    find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if INT(vcl_livre:SCREEN-VALUE) = 0 THEN client.cod_cli ELSE INT(vcl_livre:SCREEN-VALUE)) and adresse.adr_liv=yes no-lock no-error.
            END.
            if AVAIL adresse then do:
                ASSIGN
                    entetcli.zon_geo:SCREEN-VAL = STRING((IF adresse.zon_geo= 0 THEN client.zon_geo ELSE adresse.zon_geo)) /*$1728*/
                    entetcli.civ_liv:SCREEN-VALUE = adresse.civilite
                    entetcli.adr_liv[1]:SCREEN-VALUE = adresse.nom_adr
                    entetcli.adr_liv[2]:SCREEN-VALUE = adresse.adresse[1]
                    entetcli.adr_liv[3]:SCREEN-VALUE = adresse.adresse[2]
                    entetcli.adrliv4:SCREEN-VALUE = adresse.adresse4
                    entetcli.k_post2l:SCREEN-VALUE = adresse.k_post2
                    CodePostalLiv = INPUT entetcli.k_post2l
                    entetcli.villel:SCREEN-VALUE = adresse.ville
                    entetcli.paysl:SCREEN-VALUE = adresse.pays

                    adrliv-transpor=(IF adresse.transpor=0 THEN client.transpor ELSE adresse.transpor)
                    adrliv-MOD_liv=(IF adresse.MOD_liv="" THEN client.MOD_liv ELSE adresse.MOD_liv)
                    adrliv-comment=adresse.comment
                    adrliv-com_liv[1]=adresse.com_liv[1]
                    adrliv-com_liv[2]=adresse.com_liv[2]
                    adrliv-com_liv[3]=adresse.com_liv[3]
                    adrliv-com_liv[4]=adresse.com_liv[4]
                    adrliv-no_adr = adresse.cod_adr.
                RUN trt-deltra.
                adrliv-delai_trs = wdelai_trs.
                find tabgco where tabgco.type_tab="ML" and a_tab=adrliv-MOD_liv no-lock no-error.
                if available tabgco then adrliv-port=tabgco.code_df.

                IF adresse.transpor=0 AND pref_parcde_transpor THEN DO:
                    FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT(edepot:SCREEN-VAL IN FRAME FC1) NO-LOCK NO-ERROR.
                    IF AVAIL depcli AND depcli.transpor > 0 THEN adrliv-transpor = depcli.transpor.
                END.
            END.
            entetcli.typ_veh:CHECKED = client.typ_veh.
            entetcli.typ_con:SCREEN-VALUE = STRING(client.typ_con).
        END.
        ELSE DO:
            DISPLAY entetcli.zon_geo entetcli.typ_veh entetcli.typ_con entetcli.civ_liv entetcli.adr_liv[1] entetcli.adr_liv[2] entetcli.adr_liv[3] entetcli.adrliv4 entetcli.k_post2l entetcli.villel entetcli.paysl
            WITH FRAME frame-adrliv.
            ASSIGN adrliv-transpor=entetcli.transpor
                   adrliv-MOD_liv=entetcli.MOD_liv
                   adrliv-comment=entetcli.comment
                   adrliv-com_liv[1]=entetcli.com_liv[1]
                   adrliv-com_liv[2]=entetcli.com_liv[2]
                   adrliv-com_liv[3]=entetcli.com_liv[3]
                   adrliv-com_liv[4]=entetcli.com_liv[4]
                   adrliv-delai_trs = entetcli.delai_trs
                   adrliv-port=entetcli.port
                   CodePostalLiv=entetcli.k_post2l
                   adrliv-no_adr = entetcli.k_postl.
        END.

    END.
    ELSE DO:
        DISPLAY adrliv-civ_liv @ entetcli.civ_liv adrliv-adr_liv[1] @ entetcli.adr_liv[1] adrliv-adr_liv[2] @ entetcli.adr_liv[2] adrliv-adr_liv[3] @ entetcli.adr_liv[3] adrliv-adrliv4 @ entetcli.adrliv4
                adrliv-k_post2l @ entetcli.k_post2l adrliv-villel @ entetcli.villel adrliv-paysl @ entetcli.paysl adrliv-zon_geo @ entetcli.zon_geo adrliv-typ_con @ entetcli.typ_con WITH FRAME frame-adrliv.
        ASSIGN entetcli.typ_veh:CHECKED = adrliv-typ_veh.
    END.

    LogBid = badresse:Load-Image(GcoMedia + "\d600.bmp").
    LogBid = br-ciliv:Load-Image(StdMedia + "\combo.bmp").
    LogBid = br-kpliv:Load-Image(StdMedia + "\combo.bmp").
    LogBid = br-paliv:Load-Image(StdMedia + "\combo.bmp").
    LogBid = br-zg:Load-Image(StdMedia + "\combo.bmp").
    LogBid = br-tc:Load-Image(StdMedia + "\combo.bmp").
    APPLY "leave" TO entetcli.zon_geo.
    IF entetcli.typ_veh:CHECKED THEN entetcli.typ_con:LABEL = Traduction("Cat�gorie du v�hicule",-1,"").
    ELSE entetcli.typ_con:LABEL = Traduction("Type conditionnement",-1,"").
    APPLY "leave" TO entetcli.typ_con.
    APPLY "leave" TO entetcli.civ_liv.
    APPLY "leave" TO entetcli.k_post2l.
    APPLY "leave" TO entetcli.paysl.

    FRAME frame-adrliv:HIDDEN = NO.

    APPLY "entry" TO entetcli.adr_liv[1].


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-Baffcde wsaicl1
PROCEDURE choose-Baffcde :
session:set-wait ("general").
    rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
    FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
    IF AVAIL entetcli THEN run affcde_c(entetcli.NO_cde).
    session:set-wait ("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bajouter-colonnes wsaicl1
PROCEDURE CHOOSE-Bajouter-colonnes :
RUN reord_brws ("HBSAICL1","C","ENTETCLI",INPUT-O qliste-champ,INPUT-O qliste-label,INPUT-O qcolonne-lock, INPUT-O qcondition, OUTPUT logbid).
    IF logbid=? THEN RUN init-colonnes.
    IF logbid<>NO THEN DO:
        PCL_SET_QLISTE_CHAMP("SAICL1":U,qliste-champ).
        PCL_SET_QLISTE_LABEL("SAICL1":U,qliste-label).
        PCL_SET_QCOLONNE_LOCK("SAICL1":U,qcolonne-lock).
        PCL_SET_QCONDITION("SAICL1":U,qcondition).
        hbrowse = PCL_CREATION_TABLEAU("SAICL1":U).
        ON F11 OF HBrowse PERSISTENT RUN Ctrlf8 IN THIS-PROCEDURE.
        RUN openquery.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bannuler-bp wsaicl1
PROCEDURE choose-bannuler-bp :
DEF VAR ListeSSCC      AS CHAR   NO-UNDO.
    DEF VAR ssccHeterogene  AS LOG    NO-UNDO INITIAL NO.

/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_ANNULATION_BP(rentetcli) THEN DO:
            RUN PCL_ANNULATION_BP IN hapi-piece-cl (rentetcli, OUTPUT logbid).
            /* IF logbid THEN RUN refresh-hbrowse. 4.4 */
        END.
        /* ...$2240 */

        /* $A40207 ... */
        IF logbid AND parsoc.sscc AND CAN-FIND (FIRST lignecli OF entetcli WHERE lignecli.lien_sscc > 0 NO-LOCK) THEN DO:

            FOR EACH SSCCHis WHERE SSCCHis.lien_sscc = lignecli.lien_sscc NO-LOCK, FIRST SSCC WHERE SSCCHis.sscc = SSCC.sscc NO-LOCK:
                IF NOT SSCC.homogene THEN DO:
                    ssccHeterogene = YES.
                    LEAVE.
                END.
            END.

            logbid = YES.
            IF ssccHeterogene THEN
                MESSAGE Traduction("Des unit�s logistiques h�t�rog�nes sont affect�es � ce B.P. Voulez-vous recr�er des unit�s logistiques homog�nes ?",-2,"")
                    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.

            SESSION:SET-WAIT-STATE ("general").
            IF NOT VALID-HANDLE (HndGco-MvtStk) THEN RUN gco-mvtstk PERSISTENT SET HndGco-MvtStk.
            SSCC_AnnulBP (entetcli.cod_cli, entetcli.no_cde, logbid, OUTPUT ListeSSCC).
            APPLY "close" TO HndGco-MvtStk.
            /*IF ListeSSCC <> "" THEN RUN lanetq_et(ListeSSCC,"","","E","D","SUICLI_C",NO,NO,0,0,"","",OUTPUT LOGBID).*/

            SESSION:SET-WAIT-STATE ("").
        END.
        /* ... $A40207 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bannuler-p wsaicl1
PROCEDURE Choose-bannuler-p :
run annul-entete.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bavan wsaicl1
PROCEDURE choose-bavan :
rentetcli = PCL_GET_ROWID("SAICL1":U).
IF PCL_CTRL_ACCES_AVANCEMENT(rentetcli) THEN DO:
    /* RUN CacherChildInfo. 4.4 */
    RUN PCL_AVANCEMENT IN hapi-piece-cl (rentetcli). /* $2178 */
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcbl wsaicl1
PROCEDURE choose-bcbl :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
                /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_TRF_CMD_BL(rentetcli) THEN DO:
            RUN PCL_TRANSFERT_CMD_BL IN hapi-piece-cl (rentetcli /*, OUTPUT logbid 4.4 */ ).
            /* IF logbid THEN RUN reposition-rowid(rentetcli). 4.4 */
        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcd wsaicl1
PROCEDURE choose-bcd :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
                /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_TRF_CMD_DEV(rentetcli) THEN DO: /* test si possibilit� de transformer le devis */
            RUN PCL_TRANSFERT_CMD_DEV IN hapi-piece-cl (rentetcli /* , OUTPUT logbid 4.4 */ ).
            /* IF logbid THEN RUN reposition-rowid(rentetcli). 4.4 */

        END.
                /* ...$2240 */

    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bchgadr wsaicl1
PROCEDURE Choose-bchgadr :
run trt-adresse.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bcolonnes wsaicl1
PROCEDURE Choose-bcolonnes :
run parcol_b (INPUT-O pcol,
                  IF creation AND type-saisie="D" THEN "EDTDEV" ELSE IF creation AND type-saisie="C" THEN "EDTARC" ELSE IF creation THEN "EDTFAC"
                  ELSE IF entetcli.typ_sai="D" THEN "EDTDEV" ELSE IF entetcli.typ_sai="C" THEN "EDTARC" ELSE "EDTFAC",IF creation THEN ? ELSE ROWID(entetcli)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcomptoir wsaicl1
PROCEDURE choose-bcomptoir :
/*$2176*/
        /* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_TRF_FAC_COMPTOIR(rentetcli) THEN DO:
            RUN PCL_TRANSFERT_FACTURE_COMPTOIR IN hapi-piece-cl (rentetcli).
            /* RUN reposition-rowid(rentetcli). 4.4 */
        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bcreer wsaicl1
PROCEDURE Choose-bcreer :
DO WITH FRAME F1:

  /* $A40589... */
  IF etudrapid AND CAN-DO("D,C",type-saisie) THEN v-rapide = "oui":U.
  ELSE v-rapide = vrai-v-rapide.
  /* ...$A40589 */
  assign
      creation=TRUE
      logbid=session:set-wait-state("general")
      retour=yes
      adrliv-modifie = NO.
  run controle-creer.
  IF RETURN-VALUE="9" THEN return no-apply.
  /* Gestion tourn�e avec assistant en cr�ation commande */
  FIND FIRST client WHERE client.cod_cli = INT(cli$:SCREEN-VAL) NO-LOCK NO-ERROR.
  FIND FIRST cptcli WHERE cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli = INT(cli$:SCREEN-VAL) NO-LOCK NO-ERROR.
  if parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/ then
       run saitou_c (client.cod_cli,?,ref-cde$,not-ref$,IF client.depot <> 0 THEN client.depot ELSE INT (cdepot:SCREEN-VAL) /* $1110 */,dat-liv$,output retour).
  if retour then do:
      if (parcde.gst_zone[11] and type-saisie="D") or (parcde.gst_zone[8] and type-saisie<>"D") then run anocde_s (output nocde).
      if ((parcde.gst_zone[11] and type-saisie="D") or (parcde.gst_zone[8] and type-saisie<>"D")) and nocde=0 then return no-apply.
      if (not parcde.gst_zone[8] and type-saisie<>"D") or (not parcde.gst_zone[11] and type-saisie="D") then nocde=next-value(compteur_commande_client).
      run put-ini.
      IF parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/ THEN DO:
          dat-liv$=getsessiondate("Tournee", "Dat", TODAY).
          IF parsoc.sem_liv THEN sem$=CalculSemaine(dat-liv$).
          ASSIGN ref-cde$=getsessiondataex("Tournee", "Ref","")
                 NOT-ref$=getsessiondataex("Tournee", "Not","").
      END.
      run envoi-entete.
      RUN fin-envoi-entete.
  END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bdc wsaicl1
PROCEDURE choose-bdc :
    DEFINE VAR prowent AS ROWID NO-UNDO.
    DEFINE VAR logbid  AS LOG   NO-UNDO.
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        SetSessionData("API-PIECE-CL":U, "logQuestion":U,"N"). /*param�tre permettant de supprimer l'affichage de questions suppl�mentaires*/ /*A67062*/
        IF PCL_CTRL_ACCES_TRF_DEV_CMD(rentetcli) THEN DO:
            /*MESSAGE Traduction("Voulez-vous conserver le devis d'origine ?",-2,"") VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid. /*A67062*/
 *             IF logbid THEN DO:*/                                                                                                              /*A67062*/
              RUN PCL_TRANSFERT_DEVIS_CMD_ORIGINE IN hapi-piece-cl (rentetcli, OUTPUT prowent, OUTPUT logbid).                              /*A67062*/
              IF logbid THEN RUN reposition-rowid(prowent).
            /*END.                                                                                                                            /*A67062*/
 *             ELSE DO:                                                                                                                        /*A67062*/
 *                 RUN PCL_TRANSFERT_DEVIS_CMD IN hapi-piece-cl (rentetcli /* , OUTPUT logbid 4.4 */ ).
 *                 /* IF logbid THEN RUN reposition-rowid(rentetcli). 4.4 */
 *             END.      */                                                                                                                      /*A67062*/
        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").

    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bdetailtemps wsaicl1
PROCEDURE CHOOSE-Bdetailtemps :
rentetcli = PCL_GET_ROWID("SAICL1":U).
FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR.
IF AVAIL entetcli THEN DO:
    FIND FIRST ateent WHERE ateent.no_cde = entetcli.no_cde AND ateent.cod_cli = entetcli.cod_cli AND ateent.no_bl = entetcli.no_bl AND ateent.typ_sai = entetcli.typ_sai NO-LOCK NO-ERROR.
    IF AVAIL ateent THEN RUN dettpor_at(ROWID(ateent)).
    ELSE MESSAGE Traduction("Le d�tail des temps n'est pas disponible pour cette facture",-2,"")
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bdevis wsaicl1
PROCEDURE choose-bdevis :
DO WITH FRAME F1:
    session:set-wait ("general").
    run controle-creer.
    IF RETURN-VALUE="9" THEN return no-apply.
    FIND FIRST client WHERE client.cod_cli = INT(cli$:SCREEN-VAL) NO-LOCK NO-ERROR.
    FIND FIRST cptcli WHERE cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli = INT(cli$:SCREEN-VAL) NO-LOCK NO-ERROR.
    setSessionData("SAICL1_C":U,"AF_FIN":U, (IF nopt = 556 THEN "A" /* Avoir financier */ ELSE IF nopt = 615 THEN "F" /* Facture financi�re */ ELSE "")). /* $A48698 */
    run seldev_c (client.cod_cli,type-saisie,output svrowid,output retour).
    setSessionData("SAICL1_C":U,"AF_FIN":U,""). /* $A48698 */
    if retour then do: /* Reprise d'un devis */
        SetSessionData("CAPACITE%REPRISE":U,"ON_VA_LANCER":U,"YES":U). /*$2639-2*/
        DO TRANSACTION:
            find first entetcli where rowid(entetcli) = svrowid exclusive-lock no-error.
            if avail entetcli then DO:
                IF AVAIL client THEN entetcli.typ_elem = client.typ_elem. /* $A40302 */
                entetcli.qui = /*qui$:SCREEN-VALUE $913...*/ (IF quiauto <> "" THEN quiauto ELSE qui$:SCREEN-VALUE) /*...$913*/.
                IF nopt = 556 THEN entetcli.af_fin = "A". /* Avoir financier */
                ELSE IF nopt = 615 THEN entetcli.af_fin = "F". /* Facture financi�re */
            END.
        END.
        run openquery.
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR.
        if avail entetcli then hbrowse:QUERY:REPOSITION-TO-ROWID(svrowid) NO-ERROR.
        /* ...$2240 */
        creation=false.
        run envoi-entete.
        RUN fin-envoi-entete.
    end.
    else run openquery.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bsms wsaicl1
PROCEDURE Choose-bsms :
    /* $A80432 */
    IF hbrowse:NUM-SELECTED-ROWS = 1 THEN DO:
        RUN smspcli_c (ROWID(entetcli),"entetcli").
    END.
END PROCEDURE.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-btablibre wsaicl1
PROCEDURE Choose-btablibre :
/*$A67777...*/
DEF VAR retour2 AS CHAR NO-UNDO.
RUN saitbl_b ("EC":U,entetcli.NO_cde:SCREEN-VALUE IN FRAME fonglet,"","",?, OUTPUT retour2).
/*...$A67777*/
END PROCEDURE.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bdocinfo wsaicl1
PROCEDURE Choose-bdocinfo :
DEF VAR ptypsai AS CHAR  NO-UNDO. /*$A49610*/
    DEF VAR pchamp AS CHAR  NO-UNDO.  /*$A49610*/
    DEF VAR plibre AS CHAR  NO-UNDO.  /*$A49610*/
IF local_no_info = 0 THEN DO TRANSACTION:
        local_no_info = next-value(compteur_info).
        FIND FIRST infoent WHERE infoent.typ_fich = "E" AND infoent.NO_info = local_no_info NO-LOCK NO-ERROR.
        DO WHILE AVAILABLE infoent :
            local_no_info = next-value(compteur_info).
            FIND FIRST infoent WHERE infoent.typ_fich = "E" AND infoent.NO_info = local_no_info NO-LOCK NO-ERROR.
        END.
        CREATE infoent.
        ASSIGN infoent.typ_fich = "E"
               infoent.qui = G-USER
               infoent.dat_inf = TODAY
               infoent.no_info = local_no_info.
    END.

    /*$A49610...*/
    IF NOT creation THEN DO :
        IF type-saisie = "F" THEN DO :
            ptypsai = (IF entetcli.NO_fact <> 0 THEN "U" ELSE "B").
            pchamp  = (IF entetcli.NO_fact <> 0 THEN STRING(entetcli.NO_cde) ELSE STRING(entetcli.NO_bl)).
            plibre  = "client.cod_cli" + "," + string(entetcli.cod_cli) + CHR(1) + "entetcli.NO_cde," + string(entetcli.NO_cde) + CHR(1) + "entetcli.no_bl" + ","  + string(entetcli.no_bl).
        END.
        ELSE IF type-saisie = "C" THEN DO :
            ptypsai = "R".
            pchamp  = STRING(entetcli.NO_cde).
            plibre  = "client.cod_cli" + "," + string(entetcli.cod_cli) + CHR(1) + "entetcli.NO_cde," + string(entetcli.NO_cde) + CHR(1) + "entetcli.no_bl" + ","  + string(entetcli.no_bl).
        END.
        ELSE IF type-saisie = "D" THEN DO :
            ptypsai = type-saisie.
            pchamp  = STRING(entetcli.NO_cde).
            plibre  = "client.cod_cli" + "," + string(entetcli.cod_cli) + CHR(1) + "entetcli.NO_cde," + STRING(entetcli.NO_cde) + CHR(1) + "entetcli.no_bl" + ","  + string(entetcli.no_bl).
        END.
        ELSE DO :
            ptypsai = "".
            pchamp  = "".
        END.
    END.
    ELSE DO :
        /*$A60486...*/
        IF type-saisie = "F" THEN DO :
            /* $A116696 ... */
            ptypsai = "B".
            pchamp  = entetcli.no_bl:SCREEN-VALUE IN FRAME fonglet.
            plibre  = "client.cod_cli" + "," + entetcli.cod_cli:screen-value in frame fonglet + CHR(1)
                    + "entetcli.no_cde," + entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet + CHR(1)
                    + "entetcli.no_bl" + ","  + entetcli.no_bl:SCREEN-VALUE IN FRAME fonglet.
            /* ... $A116696 */
        END.
        ELSE IF type-saisie = "C" THEN DO :
            /* $A116696 ... */
            ptypsai = "R".
            pchamp  = entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet.
            plibre  = "client.cod_cli" + "," + entetcli.cod_cli:screen-value in frame fonglet + CHR(1)
                    + "entetcli.no_cde," + entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet + CHR(1)
                    + "entetcli.no_bl" + ","  + entetcli.no_bl:SCREEN-VALUE IN FRAME fonglet.
            /* ... $A116696 */
        END.
        ELSE IF type-saisie = "D" THEN DO :
            /* $A116696 ... */
            ptypsai = "D".
            pchamp  = entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet.
            plibre  = "client.cod_cli" + "," + entetcli.cod_cli:screen-value in frame fonglet + CHR(1)
                    + "entetcli.no_cde," + entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet + CHR(1)
                    + "entetcli.no_bl" + ","  + entetcli.no_bl:SCREEN-VALUE IN FRAME fonglet.
            /* ... $A116696 */
        END.
        ELSE ASSIGN plibre = "" ptypsai = "" pchamp  = "".
        /*...$A60486*/
    END.
    /*...$A49610*/


    SetSessionData ("MNTINF_B","TYPE":U,ptypsai). /*$A49610*/
    SetSessionData ("MNTINF_B","CHAMP":U,pchamp). /*$A49610*/
    SetSessionData ("MNTINF_B","PLIBRE":U,plibre). /*$A49610*/
    IF NOT creation THEN DO:
        RUN mntinf_b ("E", local_no_info, Traduction("Documents pour la commande N�",-2,"") + STRING(entetcli.no_cde) , "C" + STRING(entetcli.cod_cli)).
        DO TRANSACTION:
            entetcli.NO_info = local_no_info.
        END.
    END.
    ELSE DO:
        SetSessionData ("VISU_GED":U,"HISTORIQUE":U,"YES"). /* $A116696 */
        RUN mntinf_b ("E", local_no_info, Traduction("Documents pour la commande N�",-2,"") + STRING(nocde) , "C" + STRING(client.cod_cli)).
        SetSessionData ("VISU_GED":U,"HISTORIQUE":U,""). /* $A116696 */
    END.
    SetSessionData ("MNTINF_B","TYPE":U,"").  /*$A49610*/
    SetSessionData ("MNTINF_B","CHAMP":U,""). /*$A49610*/
    SetSessionData ("MNTINF_B","PLIBRE":U,""). /*$A49610*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-arc wsaicl1
PROCEDURE choose-bediter-arc :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_ARC(rentetcli) THEN DO:

            SetSessionData ("EDTARC","P%edtarc%Pdocasso":U,STRING (ARCDocsComp OR ARCAvcConf)). /*$A49610*/
            SetSessionData ("EDTARC","P%edtarc%PdocassoPref":U,STRING (ARCAvcConf)).            /*$A49610*/

            /* RUN CacherChildInfo. 4.4 */
            RUN PCL_EDITION_ARC IN hapi-piece-cl (rentetcli).
            /* RUN refresh-hbrowse. 4.4 */

            SetSessionData ("EDTARC","P%edtarc%Pdocasso":U,"").     /*$A49610*/
            SetSessionData ("EDTARC","P%edtarc%PdocassoPref":U,""). /*$A49610*/

        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-bp wsaicl1
PROCEDURE choose-bediter-bp :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_BP(rentetcli) THEN DO:
            /* RUN CacherChildInfo. 4.4 */
            RUN PCL_EDITION_BP IN hapi-piece-cl (rentetcli).
            /* RUN refresh-hbrowse. 4.4 */
        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BEDITER-ctrvt wsaicl1
PROCEDURE CHOOSE-BEDITER-ctrvt :
/*$2629 Cr�ation de la proc�dure*/

    IF VALID-HANDLE(Hbrowse) AND Hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        FIND CURRENT entetcli NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */

        RUN edtctrvt_gp(entetcli.typ_sai,entetcli.cod_cli, entetcli.NO_cde, entetcli.NO_bl).
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-devis wsaicl1
PROCEDURE choose-bediter-devis :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_DEV(rentetcli) THEN DO:

            SetSessionData ("EDTDEV","P%edtdev%Pdocasso":U,STRING (DEVDocsComp OR DEVAvcConf)). /*$A49610*/
            SetSessionData ("EDTDEV","P%edtdev%PdocassoPref":U,STRING (DEVAvcConf)).            /*$A49610*/

            /* RUN CacherChildInfo. 4.4 */
               RUN PCL_EDITION_DEV IN hapi-piece-cl (rentetcli).
            /* RUN refresh-hbrowse. 4.4 */

            SetSessionData ("EDTDEV","P%edtdev%Pdocasso":U,"").     /*$A49610*/
            SetSessionData ("EDTDEV","P%edtdev%PdocassoPref":U,""). /*$A49610*/

        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-facture wsaicl1
PROCEDURE choose-bediter-facture :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_FAC(rentetcli) THEN DO :

            SetSessionData ("EDTFAC","P%edtfac%Pdocasso":U,STRING (FACDocsComp OR FACAvcConf)). /*$A49610*/
            SetSessionData ("EDTFAC","P%edtfac%PdocassoPref":U,STRING (FACAvcConf)).            /*$A49610*/

            RUN PCL_EDITION_FACTURE IN hapi-piece-cl (rentetcli).
            SetSessionData ("EDTFAC","P%edtfac%Pdocasso":U,"").     /*$A49610*/
            SetSessionData ("EDTFAC","P%edtdfac%PdocassoPref":U,""). /*$A49610*/

        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-interne wsaicl1
PROCEDURE choose-bediter-interne :
do with frame f1 :
        /* Editer bon interne */
        if hbrowse:num-selected-rows > 0 then DO:
            rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
            FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
            if valid-handle(child-info) then RUN ChildBottom IN child-info.
            run edboni_c (int(cli$),entetcli.typ_sai,entetcli.no_cde,entetcli.no_bl).
        END.
    end.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-proforma wsaicl1
PROCEDURE choose-bediter-proforma :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_PROFORMA(rentetcli) THEN DO:
            /* RUN CacherChildInfo. 4.4 */
            RUN PCL_EDITION_PROFORMA IN hapi-piece-cl (rentetcli).
            /* RUN refresh-hbrowse. 4.4 */
        END.
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bediter-spe wsaicl1
PROCEDURE choose-bediter-spe :
/*$BOIS*/
    /* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_EDT_SPE(rentetcli) THEN
            RUN PCL_EDITION_SPECIFICATION_BOIS IN hapi-piece-cl (rentetcli).
        /* ...$2240 */
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-Bfax wsaicl1
PROCEDURE Choose-Bfax :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR.
        /* ...$2240 */
        IF Corfax THEN RUN Prefax("C",ROWID(Entetcli),OUTPUT Retour).
                  ELSE REtour = YES.
        IF Retour THEN CASE entetcli.typ_sai:
            WHEN "D" THEN
                IF PCL_CTRL_ACCES_EDT_DEV(ROWID(Entetcli))    THEN DO:
                    /* RUN CacherChildInfo. 4.4 */
                    RUN PCL_ENVOI_DEV_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli),YES,(IF Wmailoffre THEN "O"
                    ELSE IF Wmaildev AND NOT (devniv1 OR devniv2 OR devniv3) THEN "D"
                    ELSE IF NOT Wmaildev AND (devniv1 OR devniv2 OR devniv3) THEN "R"
                    ELSE IF Wmaildev AND (devniv1 OR devniv2 OR devniv3) THEN "T"
                    ELSE "D"),
                   (IF devniv1 THEN 1
                    ELSE IF devniv2 THEN 2
                    ELSE 3)).
                    /* RUN refresh-hbrowse. 4.4 */
                END.
            WHEN "C" THEN DO:
                IF CBGetItemShowState (Hcombars-f1, 2, {&bproforma-f}) AND Proforma-fax AND PCL_CTRL_ACCES_EDT_PROFORMA(ROWID(entetcli)) THEN RUN PCL_ENVOI_PROFORMA_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), YES). /*$A20038*/
                ELSE IF PCL_CTRL_ACCES_EDT_ARC(ROWID(entetcli)) THEN RUN PCL_ENVOI_ARC_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), YES).
            END.
            WHEN "F" OR WHEN "R" THEN do:
                IF entetcli.NO_fact = 0 THEN DO:
                    IF PCL_CTRL_ACCES_EDT_BL(ROWID(entetcli)) THEN DO:
                        /* RUN CacherChildInfo. 4.4 */
                        RUN PCL_ENVOI_BL_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), YES).
                        /* RUN refresh-hbrowse. 4.4 */
                    END.
                END.
                ELSE DO:
                    IF PCL_CTRL_ACCES_EDT_FAC(ROWID(entetcli)) THEN RUN PCL_ENVOI_FACTURE_MAIL_FAX IN hapi-piece-cl (ROWID(Entetcli), YES).
                END.
            END.
        END CASE.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BGENDM wsaicl1
PROCEDURE CHOOSE-BGENDM :
IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
        RUN gendm_a (rentetcli, YES). /* $2240 */ /*$A67820*/
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BMODIFIER wsaicl1
PROCEDURE CHOOSE-BMODIFIER :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
        /*$A40568...*/
        IF parsoc.affaire THEN DO:
            logbid = TRUE.
            pmsg = "".
            CASE type-saisie :
                WHEN "C" THEN logbid = AFF_TEST_VALIDITE (9,entetcli.qui,entetcli.affaire,entetcli.dat_cde,"M",OUTPUT pmsg).
                WHEN "F" THEN logbid = AFF_TEST_VALIDITE (10,entetcli.qui,entetcli.affaire,entetcli.dat_cde,"M",OUTPUT pmsg).
                WHEN "D" THEN DO:
                    IF parcde.gst_zone[29] THEN logbid = AFF_TEST_VALIDITE (8,entetcli.qui,entetcli.affaire,entetcli.dat_cde,"M",OUTPUT pmsg).
                    ELSE logbid = TRUE.
                END.
            END CASE.
            IF NOT logbid THEN do:
                MESSAGE pmsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                RETURN NO-APPLY.
            END.
            ELSE IF pmsg <> "" THEN MESSAGE pmsg VIEW-AS ALERT-BOX WARNING BUTTONS OK.
        END.
        /*...$A40568*/
        IF PCL_CTRL_ACCES_MODIFICATION(ROWID(entetcli)) THEN DO:
            /* $A40589... */
            IF etudrapid AND CAN-DO("D,C",entetcli.typ_sai) THEN v-rapide = "oui":U.
            ELSE v-rapide = vrai-v-rapide.
                /* ...$A40589 */
            svrowid = ROWID(entetcli).
            IF VALID-HANDLE(child-info) THEN RUN ChildBottom IN child-info.
            creation = FALSE.
            RUN put-ini.
            RUN envoi-entete.
            RUN fin-envoi-entete.
        END.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bnote wsaicl1
PROCEDURE Choose-bnote :
run notcli_c (int(entetcli.cod_cli:screen-value in frame fonglet),if creation then type-saisie else entetcli.typ_sai,int(entetcli.no_cde:screen-value in frame fonglet),int(entetcli.no_bl:screen-value),input-output note-modifiee).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bparedtg wsaicl1
PROCEDURE choose-bparedtg :
rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
    IF rentetcli <> ? THEN RUN paredtph_c("entetcli", rentetcli, ?, OUTPUT logbid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bQuitter wsaicl1
PROCEDURE Choose-bQuitter :
APPLY "close" TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-brecalcul wsaicl1
PROCEDURE Choose-brecalcul :
run recalcul-prix(yes).
/* $A60738... */
SESSION:SET-WAIT-STATE("general").
IF entetcli.cal_marg <> "" /*OR (entetcli.fac_pxa AND entetcli.maj_ach<>0)*/ THEN RUN calcmarg (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,"P").
RUN maj_phase (entetcli.cod_cli,entetcli.NO_cde,entetcli.NO_bl,entetcli.typ_sai).
SESSION:SET-WAIT-STATE("").
/* ...$A60738 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-breediter-bl wsaicl1
PROCEDURE choose-breediter-bl :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
        IF PCL_CTRL_ACCES_EDT_BL(rentetcli) THEN DO:     /* $2240 */
             /*$A36586...*/
            SetSessionData ("EDTBL","P%edtbl%Pdocasso":U,STRING (CorDocsComp OR CorAvcConf)).
            SetSessionData ("EDTBL","P%edtbl%PdocassoPref":U,STRING (CorAvcConf)).
            /*...$A36586*/

            /* RUN CacherChildInfo. 4.4 */
            RUN PCL_EDITION_BL IN hapi-piece-cl (rentetcli).  /* $2240 */
            SetSessionData ("EDTBL","P%edtbl%Pdocasso":U,""). /*$A36586*/
            SetSessionData ("EDTBL","P%edtbl%PdocassoPref":U,""). /*$A36586*/
            /* RUN refresh-hbrowse. 4.4 */
        END.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BRELANCE wsaicl1
PROCEDURE CHOOSE-BRELANCE :
IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        IF PCL_CTRL_ACCES_RELANCE(rentetcli) THEN DO:
            RUN PCL_RELANCE_DEVIS IN hapi-piece-cl (rentetcli /*, OUTPUT logbid 4.4 */ ). /* $2178 */
            /* IF logbid THEN RUN refresh-hbrowse. 4.4 */
        END.
        /* ...$2240 */
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bsaisie-liv wsaicl1
PROCEDURE choose-bsaisie-liv :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
        IF PCL_CTRL_ACCES_SAISIE_LIVRAISON(rentetcli) THEN DO:  /* $2240 */
            FRAME F1:SENSITIVE = NO.
            IF VALID-HANDLE(child-info) THEN RUN ChildBottom IN child-info.
            RUN sailiv_c (rentetcli,OUTPUT retour).      /* $2240 */
            RUN ERGO-PROC-WINDOW-RESIZED2.
            FRAME F1:SENSITIVE=YES.
            IF NOT RETOUR THEN DO:
                RUN openquery.
                CURRENT-WINDOW = wsaicl1.
            END.
        END.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BSITEWEB wsaicl1
PROCEDURE CHOOSE-BSITEWEB :
DEF INPUT  PARAM pcodsite AS INT    NO-UNDO.

    /* $A41067 */

    IF VALID-HANDLE(hbrowse) AND hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
        FIND FIRST client WHERE client.cod_cli = entetcli.cod_cli NO-LOCK NO-ERROR.
        IF AVAIL client THEN DO:
            FIND FIRST tt-web WHERE tt-web.tt-cpt = pcodsite NO-LOCK NO-ERROR.
            IF AVAIL tt-web THEN RUN exeweb_b ("client":U,ROWID(client),tt-web.tt-sit).
            RUN Sleep IN G-hProwin (2000).
        END.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bsupprimer wsaicl1
PROCEDURE Choose-bsupprimer :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */

        /* $A40207 ... */
        /*IF parsoc.sscc THEN DO:
 *             IF NOT VALID-HANDLE (HndGco-MvtStk) THEN RUN gco-mvtstk PERSISTENT SET HndGco-MvtStk.
 *             FOR EACH lignecli OF entetcli WHERE ROWID (entetcli) = rentetcli NO-LOCK:
 *                 IF lignecli.lien_sscc > 0 AND SSCC_ControleSSCCGere (lignecli.depot, lignecli.cod_pro) THEN DO:
 *                     FOR EACH SSCCHis WHERE SSCCHis.lien_sscc = lignecli.lien_sscc EXCLUSIVE-LOCK:
 *                         FIND FIRST SSCC WHERE SSCC.sscc = SSCCHis.sscc EXCLUSIVE-LOCK NO-ERROR.
 *                         IF AVAIL SSCC THEN DO:
 *                             ASSIGN SSCC.NO_cde      = 0
 *                                    SSCC.cod_cli     = 0
 *                                    SSCC.NO_bl       = 0
 *                                    SSCC.statut      = "DIS":U.
 *                         END.
 *                         DELETE SSCCHis.
 *                     END.
 *                 END.
 *             END.
 *             APPLY "close" TO HndGco-MvtStk.
 *         END.*/

        /* C'est fait dans PCL_SUPPRIMER_PIECE de api-piece-cl */

        /* ... $A40207 */

        IF PCL_CTRL_ACCES_SUPPRESSION(rentetcli) THEN DO: /* $2240 */
            RUN PCL_SUPPRIMER_PIECE IN hapi-piece-cl (rentetcli). /* $2240 */
            /* RUN actuSupp. 4.4 */
        END.
    END.

    SESSION:SET-WAIT("").
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bsupprimer-p wsaicl1
PROCEDURE Choose-bsupprimer-p :
run trt-supprimer.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-btexte wsaicl1
PROCEDURE Choose-btexte :
run txtcli_c (int(entetcli.cod_cli:screen-value in frame fonglet),
                  if creation then type-saisie else entetcli.typ_sai,
                  int(entetcli.no_cde:screen-value in frame fonglet),
                  int(entetcli.no_bl:screen-value),
                  true,
                  input-output note-modifiee).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bvalider-adrliv wsaicl1
PROCEDURE choose-bvalider-adrliv :
/*$A35969*/
    DEF VAR tstpost AS i no-undo.
    DEF VAR wid AS HANDLE NO-UNDO.
    DEF VAR txtmsg AS CHAR NO-UNDO.

    DO WITH FRAME frame-adrliv:
        ASSIGN entetcli.civ_liv:BGCOLOR = 16
               entetcli.adr_liv[1]:BGCOLOR = 16
               entetcli.villel:BGCOLOR = 16
               entetcli.paysl:BGCOLOR = 16
               entetcli.k_post2l:BGCOLOR = 16
               entetcli.zon_geo:BGCOLOR = 16
               entetcli.typ_con:BGCOLOR = 16.

        /*V�rifier adresse*/
        if input entetcli.civ_liv<>"" then do:
            find TABCOMP where TABCOMP.TYPE_TAB="CI" and TABCOMP.A_TAB=input entetcli.civ_liv no-lock no-error.
            if not available TABCOMP then
              assign
                wid=entetcli.civ_liv:handle txtmsg="" + "-" + " " + Traduction("Civilit� inexistante",-2,"") + " " + "~n" + ""
                entetcli.civ_liv:bgcolor = 31
                lib-ci:screen-value = Traduction("Inexistant",-2,"").
        end.

        if input entetcli.adr_liv[1]="" then assign
            wid=(if txtmsg="" then entetcli.adr_liv[1]:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Nom obligatoire",-2,"") + " " + "~n" + ""
            entetcli.adr_liv[1]:bgcolor = 31.

        if input entetcli.villel="" then assign
            wid=(if txtmsg="" then entetcli.villel:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Ville obligatoire",-2,"") + " " + "~n" + ""
            entetcli.villel:bgcolor = 31.

        find TABCOMP where TABCOMP.TYPE_TAB="PA" and TABCOMP.A_TAB=INPUT entetcli.paysl no-lock no-error.
        if not available TABCOMP then assign
            wid=(if txtmsg="" then entetcli.paysl:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Pays inexistant",-2,"") + " " + "~n" + ""
            entetcli.paysl:bgcolor=31
            lib-pa:screen-value = Traduction("Inexistant",-2,"").

        IF INPUT entetcli.paysl = "FRA":U THEN DO:
            tstpost = INT(trim(entetcli.k_post2l:SCREEN-VAL)) NO-ERROR.
            IF ERROR-STATUS:ERROR OR length(trim(entetcli.k_post2l:SCREEN-VAL)) > 5 THEN assign
                wid=(if txtmsg="" then entetcli.k_post2l:handle else wid)
                txtmsg=txtmsg + "" + "-" + " " + Traduction("Code postal inexistant",-2,"") + " " + "~n" + ""
            entetcli.k_post2l:bgcolor = 31.
        END.

        /*V�rifier zone g�ographique*/
        if entetcli.zon_geo:visible and input entetcli.zon_geo<>0 then do:
        find TABGCO where TABgco.TYPE_TAB="GE" and TABgco.n_TAB=input entetcli.zon_geo no-lock no-error.
        if not available TABgco then assign
            wid=(if txtmsg="" then entetcli.zon_geo:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Zone G�ographique inexistante",-2,"") + " " + "~n" + ""
            entetcli.zon_geo:bgcolor=31
            lib-ge:screen-value = Traduction("Inexistante",-2,"").
        end.

        /*V�rifier Type de conditionnement ou Cat�gorie de v�hicule*/
        IF INPUT entetcli.typ_con <> "" THEN
        DO:
            IF entetcli.typ_veh:CHECKED THEN
            DO:
                FIND FIRST atecat WHERE atecat.catego=input entetcli.typ_con no-lock no-error.
                IF NOT AVAIL atecat THEN
                    assign wid=(if txtmsg="" then entetcli.typ_con:handle else wid)
                           txtmsg=txtmsg + "" + "-" + " " + Traduction("Cat�gorie inexistante",-2,"") + " " + "~n" + ""
                           entetcli.typ_con:bgcolor=31
                           lib-tc:screen-value = Traduction("Inexistante",-2,"").
            END.
            ELSE DO:
                find tabgco where tabgco.type_tab="TC" and tabgco.a_tab=input entetcli.typ_con no-lock no-error.
                IF NOT AVAIL tabgco THEN
                    assign wid=(if txtmsg="" then entetcli.typ_con:handle else wid)
                           txtmsg=txtmsg + "" + "-" + " " + Traduction("Type de conditionnement inexistant",-2,"") + " " + "~n" + ""
                           entetcli.typ_con:bgcolor=31
                           lib-tc:screen-value = Traduction("Inexistant",-2,"").
            END.
        END.
        {erreur-p.i}

        ASSIGN adrliv-civ_liv = INPUT entetcli.civ_liv
               adrliv-adr_liv[1] = INPUT entetcli.adr_liv[1]
               adrliv-adr_liv[2] = INPUT entetcli.adr_liv[2]
               adrliv-adr_liv[3] = INPUT entetcli.adr_liv[3]
               adrliv-adrliv4 = INPUT entetcli.adrliv4
               adrliv-k_post2l = INPUT entetcli.k_post2l
               adrliv-villel = INPUT entetcli.villel
               adrliv-paysl = INPUT entetcli.paysl
               adrliv-zon_geo = INPUT entetcli.zon_geo
               adrliv-typ_veh = INPUT entetcli.typ_veh
               adrliv-typ_con = INPUT entetcli.typ_con
               adrliv-modifie = YES.
        recalculer-prix-franco = NO.
        IF parsoc.prix_franco AND AVAIL entetcli AND entetcli.prix_franco AND (adrliv-no_adr <> entetcli.k_postl OR adrliv-typ_con <> entetcli.typ_con) THEN recalculer-prix-franco = YES.

    END.

    /* $A38935... */
    ASSIGN
        FRAME frame-adrliv:HIDDEN = YES
        FrmComBars-fentete:SENSITIVE = YES
        CtrlFrame:SENSITIVE = YES.
    CASE chap-cours:
        WHEN 1 THEN FRAME fc1:SENSITIVE = YES.
        WHEN 2 THEN frame fc2:SENSITIVE = YES.
        WHEN 3 THEN FRAME fval:SENSITIVE = YES.
    END CASE.
    /* ...$A38935 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bvalider-bl wsaicl1
PROCEDURE choose-bvalider-bl :
/* $2178... */
    SESSION:SET-WAIT("general").
    IF hbrowse:NUM-SELECTED-ROWS > 0 THEN DO:
        rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
        IF PCL_CTRL_ACCES_VALIDATION_BL(rentetcli) THEN DO:  /* $2240 */
            RUN PCL_VALIDATION_BL IN hapi-piece-cl (rentetcli). /* $2240 */
            /* run openquery. 4.4 */
        END.
    END.
    SESSION:SET-WAIT("").
    /* ...$2178 */

END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bvalider-p wsaicl1
PROCEDURE Choose-bvalider-p :
DEF VAR svchap AS INT    NO-UNDO.
    session:set-wait ("general").
    RUN VALIDER-CHAPITRE.

    /*$872 ...*/
    IF RETURN-VALUE = "error" THEN DO:
      IF v-rapide = "oui" AND porigine <> "2" /*AND porigine <> "6b" $1750 */ THEN DO:
          RUN LockWindowUpdate IN G-HPROWIN (0).
          RUN Ecran2_Ok.
          IF chap-cours = 1 THEN FRAME FC1:MOVE-TO-TOP().
          ELSE IF chap-cours = 2 THEN FRAME FC2:MOVE-TO-TOP().
          ELSE IF chap-cours = 3 THEN FRAME Fval:MOVE-TO-TOP().
      END.
      RETURN NO-APPLY.
    END.
    /*$872 ...*/

    /* on v�rifie si tous les chapitres ont bien �t� valid�s
       afin de ne pas avoir de zones mal renseign�es $A37918 */
    IF val-chap1 = NO OR val-chap2 = NO THEN DO:
        svchap = chap-cours.
        IF val-chap1 = NO THEN chap-cours = 1.
                          ELSE chap-cours = 2.
        /*RUN ENVOI-CHAPITRE. $A38251 */
        RUN VALIDER-CHAPITRE.
        chap-cours = svchap.

        /*$872 ...*/
        IF RETURN-VALUE = "error" THEN DO:
          /* $A38251... */
          RUN ENVOI-CHAPITRE.
          pascharger = YES.
          myonglet:tabs (chap-cours):SELECTED = YES.
          pascharger = NO.
                  /* ...$A38251 */
          IF v-rapide = "oui" AND porigine <> "2" /*AND porigine <> "6b" $1750 */ THEN DO:
              RUN LockWindowUpdate IN G-HPROWIN (0).
              RUN Ecran2_Ok.
              IF chap-cours = 1 THEN FRAME FC1:MOVE-TO-TOP().
              ELSE IF chap-cours = 2 THEN FRAME FC2:MOVE-TO-TOP().
              ELSE IF chap-cours = 3 THEN FRAME Fval:MOVE-TO-TOP().
          END.
          IF VALID-HANDLE(wid) THEN APPLY "entry" TO wid. /* $A38251 */
          RETURN NO-APPLY.
        END.
        /*$872 ...*/
    END.

    /* Passer sur onglet 'valeurs libre' si au moins une valeur libre obligatoire */
    if creation and not val-traite AND v-rapide="non" then do:
        chap-cours=nb-chap.
        RUN ENVOI-CHAPITRE.
    END.
    ELSE RUN MAJ-ENTETE.
    session:set-wait-state("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bverifpx wsaicl1
PROCEDURE choose-bverifpx :
/* $2240... */
rentetcli = PCL_GET_ROWID("SAICL1":U).
IF PCL_CTRL_ACCES_VERIFPX(rentetcli) THEN DO:
    RUN PCL_VERIFPX IN hapi-piece-cl (rentetcli /* , OUTPUT logbid 4.4 */). /* $2178 */
    /* IF logbid THEN RUN refresh-hbrowse. 4.4 */
END.
/* ...$2240 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-bversdev wsaicl1
PROCEDURE CHOOSE-bversdev :
rentetcli = PCL_GET_ROWID("SAICL1":U). /* $2240 */
IF PCL_CTRL_ACCES_VERSDEV(rentetcli) THEN RUN PCL_VERSIONS_DEVIS IN hapi-piece-cl (rentetcli). /* $2178 */ /* $2240 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bvoir wsaicl1
PROCEDURE CHOOSE-Bvoir :
toutes-saisies = (IF toutes-saisies THEN NO ELSE YES).
    run openquery.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-Bwrkf wsaicl1
PROCEDURE choose-Bwrkf :
/*$1105... */
    IF CONNECTED ("WORKFLOW") THEN DO:
                /* $2240... */
        rentetcli = PCL_GET_ROWID("SAICL1":U).
        FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR.
        /* ...$2240 */
        IF AVAIL entetcli AND HBrowse:NUM-SELECTED-ROWS = 1 THEN RUN VALUE (propWF + "\suivi_wf") ("PROGIWIN","*SAISIE VENTE","",STRING(STRING(entetcli.cod_cli) + CHR(1) + STRING(entetcli.typ_sai) + CHR(1) + STRING(entetcli.no_cde) + CHR(1) + STRING(entetcli.no_bl)), OUTPUT paction-wkf).
        ELSE MESSAGE Traduction("Vous devez s�lectionner une commande.",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK.
    END. /* ...$1105*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-INFOENTETE wsaicl1
PROCEDURE CHOOSE-INFOENTETE :
DEF VAR ps AS CHAR no-undo.
    IF VALID-HANDLE (hbrowse) AND hbrowse:NUM-SELECTED-ROWS >= 1 AND AVAIL entetcli THEN
    DO:
        IF type-saisie = "C" THEN RUN exeinfo ("ENTETCLICOM":U,"entetcli|client":U,STRING (BUFFER entetcli:HANDLE) + "|" + STRING (BUFFER client:HANDLE),yes,{&window-name},yes,"OUI":U,OUTPUT ps).
        ELSE IF type-saisie = "D" THEN RUN exeinfo ("ENTETCLIDEV":U,"entetcli|client":U,STRING (BUFFER entetcli:HANDLE) + "|" + STRING (BUFFER client:HANDLE),yes,{&window-name},yes,"OUI":U,OUTPUT ps).
        ELSE IF type-saisie = "F" THEN RUN exeinfo ("ENTETCLIFAC":U,"entetcli|client":U,STRING (BUFFER entetcli:HANDLE) + "|" + STRING (BUFFER client:HANDLE),yes,{&window-name},yes,"OUI":U,OUTPUT ps).
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cli-no wsaicl1
PROCEDURE cli-no :
/* Pas de client */
DO WITH FRAME F1:
    ASSIGN Logbid = CBEnableItem (Hcombars-f1, 2, {&bcreer}, NO)
           Logbid = CBEnableItem (Hcombars-f1, 2, {&bajouter-colonnes}, NO)
           Logbid = CBEnableItem (Hcombars-f1, 2, {&bvoir}, NO)
           Logbid = CBEnableItem (Hcombars-f1, 2, {&bdevis}, NO)
           Logbid = CBEnableItem (Hcombars-f1, 2, {&baffenc}, NO).
    HIDE
        dat-cde$ dat-liv$ sem$ ref-cde$ typ-cde$ not-ref$ Tbdevis /*$A92960*/
        nat$ br-nc$.
    RUN cacher-champs.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cli-yes wsaicl1
PROCEDURE cli-yes :
/* Client OK */
DO WITH FRAME F1:
    find client where client.cod_cli=int(cli$) no-lock no-error.
    run openquery.
    ASSIGN
        dat-cde$:hidden=no
        dat-liv$:hidden=no
        sem$:hidden=NO WHEN parsoc.sem_liv
        ref-cde$:hidden=no when parsoc.ges_ref
        typ-cde$:hidden=no when plusieurs-types
        not-ref$:hidden=no when parsoc.ges_not
        Tbdevis:HIDDEN = NO /*$A92960*/
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bcreer}, YES)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bdevis}, YES)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bajouter-colonnes}, YES)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&bvoir}, YES)
        nat$:hidden=no when parsoc.ges_nat
        br-nc$:hidden=no when parsoc.ges_nat
        Logbid = CBEnableItem (Hcombars-f1, 2, {&baffenc}, YES).
    if tvoir:screen-value="yes" then do:
        if not valid-handle(child-info) then run afincl_f persistent set child-info.
        run refresh-editeur in child-info (int(cli$)).
        current-window=wsaicl1.
    end.
    find assisdat where assisdat.typ_fich="C" and assisdat.cod_cf=client.cod_cli no-lock no-error.
    if available assisdat and assisdat.dat_fin>=today then DO:
        IF NOT VALID-HANDLE(hpostit) THEN RUN postit PERSISTENT SET hpostit.
        IF VALID-HANDLE(hpostit) then RUN PostitAffichage IN hpostit (FRAME F1:HANDLE, 1, "C", Traduction("Assistant",-2,"") + " " + client.nom_cli,assisdat.texte).
    end.
    else IF VALID-HANDLE(hpostit) then RUN PostitHide IN hpostit (1).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation wsaicl1
PROCEDURE CombarsCreation :
DEFINE VARIABLE ii     AS INTEGER    NO-UNDO. /* $A41067 */

    RUN combars PERSISTENT SET hComBars.
    /* Barre pour la frame f1 */
    hComBars-f1 = CBOCXAdd(FRAME F1:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hComBars-f1, 1.04,  28.26, 1.15,FRAME F1:WIDTH - 27.66).   /*taille et place de la barre*/
    CBAddBar(hComBars-f1, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-f1, NO).
    IF rccola <> INT(ColBlanc) OR  rccolb <> int(ColBlanc) THEN DO:
        IF rccola <> 0 AND rccola <> ? THEN CbsetColor(hComBars-f1,65, rccola).
        IF rccolb <> 0 AND rccolb <> ? THEN CbsetColor(hComBars-f1,66,rccolb).
    END.

    CBLoadIcon(hComBars-f1, {&bquitter},            StdMedia + "\StdMedia.icl",94).
    CBLoadIcon(hComBars-f1, {&bcreer},              StdMedia + "\StdMedia.icl",36).
    CBLoadIcon(hComBars-f1, {&bmodifier},           StdMedia + "\StdMedia.icl",78).
    CBLoadIcon(hComBars-f1, {&bsupprimer},          StdMedia + "\StdMedia.icl",112).
    CBLoadIcon(hComBars-f1, {&bpopup},              StdMedia + "\StdMedia.icl",91).
    CBLoadIcon(hcombars-f1, {&bediter-bp},          StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&breediter-bl},        StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-facture},     StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-devis},       StdMedia + "\StdMedia.icl",57).
    IF parsoc.bois_ar THEN
        CBLoadIcon(hcombars-f1, {&bediter-spe},     StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-arc},         StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-proforma},    StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-interne},     StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter},             StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hcombars-f1, {&bediter-ctrvt},       StdMedia + "\StdMedia.icl",57). /*$2629*/
    CBLoadIcon(hComBars-f1, {&bajouter-colonnes},   StdMedia + "\StdMedia.icl",3).
    CBLoadIcon(hComBars-f1, {&bajouter-colonnes2},  StdMedia + "\StdMedia.icl",111).
    CBLoadIcon(hComBars-f1, {&Bvoir},               StdMedia + "\StdMedia.icl",95).
    CBLoadIcon(hComBars-f1, {&Boption},             StdMedia + "\StdMedia.icl",85).
    CBLoadIcon(hComBars-f1, {&Baffcde},             GcoMedia + "\GcoMedia.icl",2).
    CBLoadIcon(hComBars-f1, {&Baffenc},             GcoMedia + "\GcoMedia.icl",21).
    CBLoadIcon(hComBars-f1, {&Bwrkf},               StdMedia + "\StdMedia.icl",120).
    CBLoadIcon(hComBars-f1, {&Bwrkf2},              StdMedia + "\StdMedia.icl",121).
    CBLoadIcon(hComBars-f1, {&Bwrkf3},              StdMedia + "\StdMedia.icl",122).
    CBLoadIcon(hComBars-f1, {&btn-infoentete},      GcoMedia + "\GcoMedia.icl",40).
    CBLoadIcon(hComBars-f1, {&bavan},               GcoMedia + "\GcoMedia.icl",52).
    CBLoadIcon(hComBars-f1, {&Bfax},                StdMedia + "\StdMedia.icl",63).
    CBLoadIcon(hComBars-f1, {&B-mail},              StdMedia + "\StdMedia.icl",77).
    CBLoadIcon(hComBars-f1, {&bsms},                Prolink  + "\Prolink.icl":U,197). /* $A80432 */
    CBLoadIcon(hComBars-f1, {&Bcd},                 GcoMedia + "\GcoMedia.icl",57).
    CBLoadIcon(hComBars-f1, {&Bdc},                 GcoMedia + "\GcoMedia.icl",56).
    CBLoadIcon(hComBars-f1, {&Bcbl},                GcoMedia + "\GcoMedia.icl",58).
    CBLoadIcon(hComBars-f1, {&bversdev},            StdMedia + "\StdMedia.icl",84).
    CBAddImage(hcombars-f1, {&Bdetailtemps},        GCOMedia + "\d455.bmp"). /*$A39538*/ /* �xref_CBADDImage� */
    CBLoadIcon(hComBars-f1, {&bcomptoir},           GcoMedia + "\GCOMedia.icl",58). /*$2176*/
    CBAddImage(hComBars-f1, {&Breaffect}, GcoMedia + "\d188.bmp"). /* �xref_CBADDImage� */
    CBAddImage(hComBars-f1, {&Bacompte}, GcoMedia + "\d188.bmp"). /*$2624*/ /* �xref_CBADDImage� */
    /* $A41067... */
    CBAddImage(hComBars-f1, {&bfiche},              GcoMedia + "\D23.BMP").  /* �xref_CBADDImage� */
    CBLoadIcon(hComBars-f1, {&bweb},                StdMedia + "\StdMedia.icl",82).
    CBAddImage(hComBars-f1, {&binfor},              GcoMedia + "\infogrc.bmp").  /* �xref_CBADDImage� */
    /* ...$A41067 */
    CBAddImage(hComBars-f1, {&bintegrer},           GcoMedia + "\pda_tp.bmp"). /*$A41829*/ /* �xref_CBADDImage� */
    CBAddImage(hComBars-f1, {&bXML},                GcoMedia + "\type_xml.ico"). /* �xref_CBADDImage� */ /*$A68906*/

    RUN CBDebutPersonnalisation IN hcombars("saicl1_c", "hComBars-f1").

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bquitter}, "&" + Traduction("Quitter",-2,"") , yes).
    CBSetItemStyle(hComBars-f1, 2, {&bquitter}, {&xtpButtonIconAndCaption}).
    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlSplitButtonPopup}, {&bcreer},Traduction("Cr�er",-2,"") +  " (F6)", yes).
    CBSetItemStyle(hComBars-f1, 2, {&bcreer}, {&xtpButtonIcon}).
    CBSetItemTooltip(hComBars-f1, 2, {&bcreer}, Traduction("Cr�er",-2,"") +  " (F6)").
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bdevis}, "&" + Traduction("Reprise Devis",-2,""), NO).
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bmodifier},"", NO).
    CBSetItemTooltip(hComBars-f1, 2, {&bmodifier}, Traduction("Modifier",-2,"") +  " (F8)").
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bsupprimer}, "" , NO).
    CBSetItemTooltip(hComBars-f1, 2, {&bsupprimer}, Traduction("Supprimer",-2,"")).

    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlPopup}, {&Bediter}, "&" + Traduction("Editer",-2,""), YES).
    CBSetItemStyle(hComBars-f1, 2, {&Bediter}, {&xtpButtonIconAndCaption}).
    xitem-edtDev = CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-devis}, '&' + Traduction("Editer Devis",-2,"") , YES,nompgm1).
    /*$A49610...*/
    CBAddItem(hComBars-f1, 2, xitem-edtDev, {&xtpControlButton}, {&bedtDocDEV}, '&' + Traduction("Editer les documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&bedtDocDEV}, DEVDocsComp).
    CBAddItem(hComBars-f1, 2, xitem-edtDev, {&xtpControlButton}, {&bedtAvcDEV}, '&' + Traduction("Editer les documents compl�mentaires avec confirmation",-2,""), NO).
    CBCheckItem(hComBars-f1, 2, {&bedtAvcDEV}, DEVAvcConf).
    /*...$A49610*/

    IF parsoc.bois_ar THEN CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-spe}, '&' + Traduction("Editer Sp�cifications",-2,"") , YES,nompgm8). /*$BOIS*/
    CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-bp}, '&' + Traduction("Editer B.P",-2,"") , YES,nompgm4).
    xitem-edtFac = CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-facture}, '&' + Traduction("Editer Facture",-2,"") , YES,nompgm2).
    /*$A49610...*/
    CBAddItem(hComBars-f1, 2, xitem-edtFAC, {&xtpControlButton}, {&bedtDocFAC}, '&' + Traduction("Editer les documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&bedtDocFAC}, FACDocsComp).
    CBAddItem(hComBars-f1, 2, xitem-edtFac, {&xtpControlButton}, {&bedtAvcFAC}, '&' + Traduction("Editer les documents compl�mentaires avec confirmation",-2,""), NO).
    CBCheckItem(hComBars-f1, 2, {&bedtAvcFAC}, FACAvcConf).
    /*...$A49610*/

    xitem-edtarc = CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-arc}, '&' + Traduction("A.R.C",-2,"") , YES,nompgm3).
    /*$A49610...*/
    CBAddItem(hComBars-f1, 2, xitem-edtarc, {&xtpControlButton}, {&bedtDocARC}, '&' + Traduction("Editer les documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&bedtDocARC}, ARCDocsComp).
    CBAddItem(hComBars-f1, 2, xitem-edtarc, {&xtpControlButton}, {&bedtAvcARC}, '&' + Traduction("Editer les documents compl�mentaires avec confirmation",-2,""), NO).
    CBCheckItem(hComBars-f1, 2, {&bedtAvcARC}, ARCAvcConf).
    /*...$A49610*/

    CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-proforma}, '&' + Traduction("Proforma",-2,"") , YES,nompgm5).
    xitem-edtDocs = CBAddItemPrint(hcombars-f1, 2, xitem, {&breediter-bl}, '&' + Traduction("R��diter B.L",-2,"") , YES,nompgm7).
    CBAddItem(hComBars-f1, 2, xitem-edtDocs, {&xtpControlButton}, {&bedtDocComp}, '&' + Traduction("Editer les documents compl�mentaires",-2,""), YES). /*$A36586*/
    CBCheckItem(hComBars-f1, 2, {&bedtDocComp}, CorDocsComp). /*$A36586*/
    CBAddItem(hComBars-f1, 2, xitem-edtDocs, {&xtpControlButton}, {&bedtAvcConf}, '&' + Traduction("Editer les documents compl�mentaires avec confirmation",-2,""), NO). /*$A36586*/
    CBCheckItem(hComBars-f1, 2, {&bedtAvcConf}, CorAvcConf). /*$A36586*/
    CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-interne}, Traduction("Bon Interne",-2,"") , YES,nompgm6).
    CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-ctrvt}, Traduction("R�capitulatif des co�ts de revient",-2,"") , YES,nompgm9). /*$2629*/

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bannuler-bp}, '&' + Traduction("Annuler B.P",-2,"") , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bannuler-bp}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bvalider-bl}, '&' + Traduction("Valider B.L",-2,"") , YES).
    CBSetItemStyle(hcombars-f1, 2, {&bvalider-bl}, {&xtpButtonIconAndCaption}).
    CBSetItemTooltip(hcombars-f1, 2, {&bvalider-bl}, Traduction("Passage du B.L en historique sans �dition de facture",-2,"")).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bdc}, "", YES).
    CBSetItemTooltip(hComBars-f1, 2, {&bdc}, Traduction("Transformer le devis tel quel en commande",-2,"")).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcd}, "" , YES).
    CBSetItemTooltip(hComBars-f1, 2, {&bcd}, Traduction("Transformer la commande en devis",-2,"")).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcbl}, "" , YES).
    CBSetItemTooltip(hComBars-f1, 2, {&bcbl}, Traduction("Transformer la commande en B.L",-2,"")).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bsaisie-liv}, '&' + Traduction("Livraison",-2,"") + "(F7)" + "" , YES).
    CBSetItemStyle(hcombars-f1, 2, {&bsaisie-liv}, {&xtpButtonIconAndCaption}).

    /*$2176...*/
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcomptoir}, "" , YES).
    CBSetItemTooltip(hComBars-f1, 2, {&bcomptoir}, Traduction("Transformer la facture en saisie comptoir",-2,"")).
    /*...$2176*/


    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlPopup}, {&Boption}, "&" + Traduction("Options",-2,""), YES).
    CBSetItemStyle(hComBars-f1, 2, {&Boption}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bversdev},Traduction("Versions du devis",-2,""), NO).

    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Baffenc},"&" + Traduction("Pi�ce",-2,"") + " (F11)", YES).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bavan},Traduction("Etat d'avancement commande",-2,""), NO).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Baffcde},Traduction("D�tail et �volution d'une commande",-2,"") + " (SHIFT-F8)", NO).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bwrkf},Traduction("Lancer Workflow",-2,""), yes).
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&bXML}, '&' + Traduction("G�n�rer fichier",-2,"") + " XML":U , NO). /*$A68906*/
    /* $1855 ... */
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&BRelance}, Traduction("Statut / Relance devis",-2,"") , YES).
    CBSetItemStyle(hcombars-f1, 2, {&BRelance}, {&xtpButtonIconAndCaption}).
    /* ... $1855 */
    CBAddImage(hComBars-f1, {&bgendm}, GCOMedia + "\d165.bmp"). /* �xref_CBADDImage� */
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&bgendm}, Traduction("G�n�rer demandes de prix",-2,"") , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bgendm}, {&xtpButtonIconAndCaption}).
    /*$2026...*/
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&Bverifpx}, Traduction("Contr�le �volution des prix",-2,"") , YES).
    CBSetItemStyle(hcombars-f1, 2, {&Bverifpx}, {&xtpButtonIconAndCaption}).
    /*...$2026*/
    /*$A39538...*/
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&Bdetailtemps}, Traduction("D�tail des Temps",-2,"") , YES).
    CBSetItemStyle(hcombars-f1, 2, {&Bdetailtemps}, {&xtpButtonIconAndCaption}).
    /*...$A39538*/
    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&Breaffect}, Traduction("R�affectation des r�glements",-2,"") , YES).
    CBSetItemStyle(hcombars-f1, 2, {&Breaffect}, {&xtpButtonIconAndCaption}).
    Logbid = CBShowItem (Hcombars-f1, 2, {&Breaffect}, NO).

    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&Bacompte}, Traduction("Saisie d'un acompte",-2,"") , NO). /*$2624*/
    CBSetItemStyle(hcombars-f1, 2, {&Bacompte}, {&xtpButtonIconAndCaption}). /*$2624*/
    Logbid = CBShowItem (Hcombars-f1, 2, {&Bacompte}, NO). /*$2624*/

    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bintegrer}, Traduction("Int�gration des commandes et devis",-2,"") , YES). /*$A41829*/

    /*$A54437...*/
    IF afficherinfo THEN DO :
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&btn-infoentete}, Traduction("Affichage des informations de l'En-t�te",-2,"") , YES).
        Logbid = CBShowItem (Hcombars-f1, 2, {&btn-infoentete}, type-saisie = "C" OR type-saisie = "D" OR type-saisie = "F").
    END.
    /*...$A54437*/

    CBAddItem(hcombars-f1, 2, xitem, {&xtpControlButton}, {&bparedtg}, Traduction("Pr�f�rences d'�dition des phases",-2,"") , YES).

    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlSplitButtonPopup}, {&bfax},Traduction("Envoyer devis",-2,"") + "," + Traduction("A.R.C",-2,"") + "," + Traduction("B.L",-2,"") + "," + Traduction("facture par Fax",-2,""), yes).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bgstenvoi}, "&" + Traduction("Gestion des envois",-2,""), NO).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bcorres-f}, "&" + Traduction("S�lection du Correspondant " + "/" + " " + Traduction("Fax Express",-2,""),-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bproforma-f},Traduction("Proforma",-2,""), YES). /*$A20038*/
    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlSplitButtonPopup}, {&b-mail},Traduction("Envoyer devis",-2,"") + "," + Traduction("A.R.C",-2,"") + "," + Traduction("B.L",-2,"") + "," + Traduction("facture par courriel",-2,""), NO).
    IF type-saisie = "D" THEN DO:
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bmaildev}, Traduction("Devis",-2,""), YES).
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bmailoffre}, Traduction("Offre de prix",-2,""), YES).
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bmaildev1}, Traduction("R�capitulatif (1er niveau)",-2,""), YES).
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bmaildev12}, Traduction("R�capitulatif (1er et 2�me niveau)",-2,""), NO).
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bmaildevt}, Traduction("R�capitulatif (tous 1es niveaux)",-2,""), NO).
    END.
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bcorres-m}, "&" + Traduction("S�lection du Correspondant " + "/" + " " + Traduction("Fax Express",-2,""),-2,""), YES).
    /*$A49610...*/
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Docmaildev}, Traduction("Envoi des documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&Docmaildev}, Docmaildev).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&DocmailBL}, Traduction("Envoi des documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&DocmailBL}, DocmailBL).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&DocmailARC}, Traduction("Envoi des documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&DocmailARC}, DocmailARC).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&DocmailFAC}, Traduction("Envoi des documents compl�mentaires",-2,""), YES).
    CBCheckItem(hComBars-f1, 2, {&DocmailFAC}, DocmailFAC).
    /*...$A49610*/
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bproforma-m},Traduction("Proforma",-2,""), YES). /*$A20038*/

    /* $A41067... */

    /* $A80432... */
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bsms}, "&" + Traduction("Envoyer un SMS",-2,"") , NO ).
    CBSetItemRightAlign (hComBars-f1, 2, {&bsms}, yes).
    /* ...$A80432 */

    IF MNU_DROIT_NOOPTM (g-user-perso, 409) THEN CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&binfor}, "&" + Traduction("Informations relation client",-2,"") , YES).

    IF MNU_DROIT_NOOPTM (g-user-perso, 23) THEN CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bfiche}, "&" + Traduction("Fiche Client",-2,"") , YES).

    IF CAN-FIND (FIRST siteweb WHERE siteweb.typ_site = "client":U NO-LOCK) THEN DO:
        CBAddItem(hComBars-f1, 2, ?, {&xtpControlPopup}, {&bweb}, Traduction("Acc�s � internet",-2,""), NO).
        CBSetItemStyle(hcombars-f1, 2, {&bweb}, {&xtpButtonIcon}).
    END.
        /* ...$A41067 */

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&Bvoir}, Traduction("Voir toutes les pi�ces du client",-2,"") + "(CTRL-F7 " + Traduction("pour privil�gier le mode actuel d'affichage",-2,"") + ")" , NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&bvoir}, YES).
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&Bajouter-colonnes}, Traduction("Ajouter",-2,"") + "," + Traduction("r�organiser",-2,"") + "," + Traduction("changer la condition",-2,"") + "(CTRL-F12)" + "" , NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&Bajouter-colonnes}, YES).
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlLabel}, {&bpopup}, "" , NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&bpopup}, YES).

    RUN CBFinPersonnalisation IN hcombars("saicl1_c", "hComBars-f1", hComBars-f1, 2).

    /* $A41067... */
    xitem = CBGetItemComHandle(hComBars-f1, 2, {&bweb}).

    ii = 350.
    FOR EACH siteweb WHERE siteweb.typ_site = "client":U NO-LOCK:
        ii = ii + 1.
        IF siteweb.lib_site = "Pages jaunes":U     THEN logbid = CBAddImage(hComBars-f1, ii, GcoMedia + "\pj.bmp").  /* �xref_CBADDImage� */
        ELSE IF siteweb.lib_site = "Google maps":U THEN logbid = CBAddImage(hComBars-f1, ii, GcoMedia + "\google.bmp").  /* �xref_CBADDImage� */
        ELSE IF siteweb.lib_site = "Score 3":U     THEN logbid = CBAddImage(hComBars-f1, ii, GcoMedia + "\score3.bmp"). /* �xref_CBADDImage� */
        ELSE IF siteweb.mot-cle <> "" AND siteweb.chemin <> "" THEN DO:
            FIND FIRST media WHERE media.mot-cle = siteweb.mot-cle NO-LOCK NO-ERROR.
            IF AVAIL media THEN DO:
                IF CAN-DO("1,2,3", SUBSTR(media.chemin,1,1)) THEN init-dir = GetSessionData("Config!", liste-unit[INT(SUBSTR(media.chemin, 1, 1))]) + SUBSTR(media.chemin,2).
                ELSE /*$GED...*/ IF SUBSTR(media.chemin, 1, 1) = "4" AND CONNECTED("GED") THEN RUN rechged(SUBSTR(media.chemin, 3), OUTPUT init-dir). /*...$GED*/
                ELSE init-dir = media.chemin.
            END.
            IF init-dir <> "" THEN DO:
                logbid = SEARCH(init-dir + "\" +  siteweb.chemin) <> ?.
                IF logbid THEN CBAddImage(hComBars-f1, ii, init-dir + "\" +  siteweb.chemin). /* �xref_CBADDImage� */
            END.
        END.
        CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, ii, siteweb.lib_site, NO).

        IF NOT CAN-FIND(FIRST tt-web WHERE tt-web.tt-cpt = ii) THEN DO:
            CREATE tt-web.
            ASSIGN
               tt-web.tt-cpt = ii
               tt-web.tt-sit = siteweb.site.
        END.
    END.
        /* ...$A41067 */

    /* Barre pour la frame fentete */
    hComBars-fentete = CBOCXAdd(FRAME fentete:HANDLE, OUTPUT FrmComBars-fentete).
    CBSetSize(hComBars-fentete, 1.04,  28.26, 1.15,FRAME fentete:WIDTH - 27.66).   /*taille et place de la barre*/
    CBAddBar(hComBars-fentete, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-fentete, NO).
    IF rccola <> INT(ColBlanc) OR  rccolb <> int(ColBlanc) THEN DO:
        IF rccola <> 0 AND rccola <> ? THEN CbsetColor(hComBars-fentete,65, rccola).
        IF rccolb <> 0 AND rccolb <> ? THEN CbsetColor(hComBars-fentete,66,rccolb).
    END.

    CBLoadIcon(hComBars-fentete, {&bannuler-p},     StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hComBars-fentete, {&bvalider-p},     StdMedia + "\StdMedia.icl",116).
    CBLoadIcon(hComBars-fentete, {&bsupprimer-p},   StdMedia + "\StdMedia.icl",112).
    CBLoadIcon(hComBars-fentete, {&bpopup2},        StdMedia + "\StdMedia.icl",91).
    CBLoadIcon(hComBars-fentete, {&bcolonnes},      StdMedia + "\StdMedia.icl",31).
    CBLoadIcon(hComBars-fentete, {&bnote},          StdMedia + "\StdMedia.icl",33).
    CBLoadIcon(hComBars-fentete, {&badrliv},        GCOMedia + "\GCOMedia.icl",61). /*$A35969*/
    CBLoadIcon(hComBars-fentete, {&bcatelec},       StdMedia + "\StdMedia.icl",82). /* $A38645 */


    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&bannuler-p}, "&" + Traduction("Abandonner",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&bannuler-p}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&bvalider-p}, "&" + Traduction("Valider",-2,"") + " (F9)" , NO).
    CBSetItemStyle(hComBars-fentete, 2, {&bvalider-p}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&bsupprimer-p}, "&" + Traduction("Supprimer",-2,""), yes).
    CBSetItemStyle(hComBars-fentete, 2, {&bsupprimer-p}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&brecalcul}, "&" + Traduction("Recalculer prix",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&brecalcul}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&btexte}, "&" + Traduction("Texte",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&btexte}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&bcolonnes}, Traduction("Colonnes visibles � l'�dition",-2,"") , yes).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&Bdocinfo}, "&" + Traduction("Documents",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&Bdocinfo}, {&xtpButtonIconAndCaption}).
    /*$A67777...*/
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&Btablibre}, "&" + Traduction("Tableaux libres",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&Btablibre}, {&xtpButtonIconAndCaption}).
    /*....$A67777*/
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&Bchgadr}, "&" + Traduction("Autres adresses",-2,"") , yes).
    CBSetItemStyle(hComBars-fentete, 2, {&Bchgadr}, {&xtpButtonIconAndCaption}).
    /*$A35969...*/
    IF NOT creation OR NOT(parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C") THEN
    DO:
        CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&Badrliv}, "&" + Traduction("Livraison",-2,"") , yes).
        CBSetItemStyle(hComBars-fentete, 2, {&BadrLiv}, {&xtpButtonIconAndCaption}).
    END.
    /*...A35969*/
    /*$A38645... g�n�ration du menu en fonction du param�trage */
    DEF VAR typcat      AS CHAR  NO-UNDO.
    DEF VAR txtbouton   AS CHAR  NO-UNDO.
    DEF VAR cpt         AS INT    NO-UNDO.
    cpt = {&bcatelec}.
    IF parsoc.ges_ce = YES THEN DO :
        typcat = nompgm + "�" + type-saisie.
        FOR EACH catce WHERE catce.actif_ce = YES NO-LOCK : /*�XREF_WHOLE:catce�*/
            /* V2... */
            /* on recherche si on a un acces primaire (ERP) d'activ� pour ce catalogue */
            IF CAN-FIND (FIRST parce WHERE parce.cod_ce = catce.cod_ce AND parce.pere_param = 0 AND parce.cod_param = 3 AND parce.nom_param = "Acces_primaire" AND parce.valeur_param = "YES" NO-LOCK) THEN DO: 
                /* activation, de l'option */
                FIND FIRST parce WHERE parce.cod_ce = catce.cod_ce AND parce.pere_param = 901 AND parce.cod_param = 0 AND parce.nom_param = typcat AND parce.valeur_param = 'yes' NO-LOCK NO-ERROR.
                IF AVAIL parce THEN DO: /* recherche libell� du bouton */
                    FIND FIRST parce WHERE parce.cod_ce = catce.cod_ce AND parce.pere_param = 0 AND parce.cod_param = 15 AND parce.nom_param = 'Libelle_option' NO-LOCK NO-ERROR.
                    IF AVAIL parce THEN txtbouton = parce.valeur_param.
                    ELSE txtbouton = catce.lib_ce.
                
                    IF cpt = {&bcatelec} THEN DO:
                        xitem2 = CBAddItem(hComBars-fentete, 2, ?, {&xtpControlPopup}, {&bcatelec}, Traduction("Catalogue(s)",-2,""), NO).
                        CBSetItemStyle(hComBars-fentete, 2, {&bcatelec}, {&xtpButtonIconAndCaption}).
                    END.
                    cpt = cpt + 1.
                    CBAddItem(hComBars-fentete, 2, xitem2, {&xtpControlButton}, cpt, txtbouton , yes).
                    CBLoadIcon(hComBars-fentete, cpt, StdMedia + "\StdMedia.icl",61).
                    CBSetItemStyle(hComBars-fentete, 2, cpt, {&xtpButtonIconAndCaption}).
                    CREATE tt-catalog.
                    ASSIGN
                        tt-catalog.tt-id         = catce.cod_ce
                        tt-catalog.tt-option     = cpt
                        tt-catalog.tt-libelle    = txtbouton
                        tt-catalog.tt-appelant   = typcat.
                END.
            END.
            /* ...V2 */
        END.
    END.
    /*...$A38645*/
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlButton}, {&bnote}, "&" + Traduction("Note interne pi�ce",-2,"") , YES).
    CBSetItemRightAlign (hComBars-fentete, 2, {&bnote}, yes).
    CBAddItem(hComBars-fentete, 2, ?, {&xtpControlLabel}, {&bpopup2}, "" , NO).
    CBSetItemRightAlign (hComBars-fentete, 2, {&bpopup2}, YES).

    /*$A35969... Barre pour la frame frame-adrliv */
    IF NOT creation OR NOT(parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C") THEN
    DO:
        hComBars-adrliv = CBOCXAdd(FRAME frame-adrliv:HANDLE, OUTPUT FrmComBars-adrliv).
        CBSetSize(hComBars-adrliv, 1.04,  28.26, 1.15,FRAME frame-adrliv:WIDTH - 29.66).   /*taille et place de la barre*/
        CBAddBar(hComBars-adrliv, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
        CBShowMenu(hComBars-adrliv, NO).
        CBLoadIcon(hComBars-adrliv, {&babandonner-adrliv},     StdMedia + "\StdMedia.icl",1).
        CBLoadIcon(hComBars-adrliv, {&bvalider-adrliv},     StdMedia + "\StdMedia.icl",116).

        CBAddItem(hComBars-adrliv, 2, ?, {&xtpControlButton}, {&babandonner-adrliv}, "&" + Traduction("Abandonner",-2,"") , yes).
        CBSetItemStyle(hComBars-adrliv, 2, {&babandonner-adrliv}, {&xtpButtonIconAndCaption}).
        CBAddItem(hComBars-adrliv, 2, ?, {&xtpControlButton}, {&bvalider-adrliv}, "&" + Traduction("Valider",-2,"") + " (F9)" , NO).
        CBSetItemStyle(hComBars-adrliv, 2, {&bvalider-adrliv}, {&xtpButtonIconAndCaption}).
    END.
    /*...$A35969*/

    RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE(FRAME f1:HANDLE, ltype-saisie).
    SET-TITLE(FRAME fentete:HANDLE, Traduction("Modification",-2,"")).
    SET-TITLE(FRAME frame-adrliv:HANDLE, Traduction("Livraison",-2,"")). /*$A35969*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ComBarsExecute wsaicl1
PROCEDURE ComBarsExecute :
DEFINE INPUT PARAMETER pNumComBar AS INT NO-UNDO.
    DEFINE INPUT PARAMETER pButton    AS INT NO-UNDO.
    IF pNumComBar = 1 THEN CASE pButton:
        WHEN {&bQuitter}         THEN RUN Choose-bQuitter.
        WHEN {&bcreer}           THEN RUN Choose-bcreer.
        WHEN {&bmodifier}        THEN RUN Choose-bmodifier.
        WHEN {&bsupprimer}       THEN RUN Choose-bsupprimer.
        WHEN {&bvalider-bl}      THEN RUN choose-bvalider-bl.
        WHEN {&bediter-bp}       THEN RUN choose-bediter-bp.
        WHEN {&bediter-arc}      THEN RUN choose-bediter-arc.
        WHEN {&breediter-bl}     THEN RUN choose-breediter-bl.
        WHEN {&bXML}             THEN RUN CHOOSE-bXML. /*$A68906*/
        /*$A36586...*/
        WHEN {&bedtDocComp} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtDocComp}, NOT CorDocsComp).
            CorDocsComp = NOT CorDocsComp.
            IF CorDocsComp THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtAvcConf}, NOT CorDocsComp).
                CorAvcConf = NOT CorDocsComp.
            END.
        END.
        WHEN {&bedtAvcConf} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtAvcConf}, NOT CorAvcConf).
            CorAvcConf = NOT CorAvcConf.
            IF CorAvcConf THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtDocComp}, NOT CorAvcConf).
                CorDocsComp = NOT CorAvcConf.
            END.
        END.
        /*...$A36586*/
        /*$A49610...*/
        WHEN {&bedtDocARC} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtDocARC}, NOT ARCDocsComp).
            ARCDocsComp = NOT ARCDocsComp.
            IF ARCDocsComp THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtAvcARC}, NOT ARCDocsComp).
                ARCAvcConf = NOT ARCDocsComp.
            END.
        END.
        WHEN {&bedtAvcARC} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtAvcARC}, NOT ARCAvcConf).
            ARCAvcConf = NOT ARCAvcConf.
            IF ARCAvcConf THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtDocARC}, NOT ARCAvcConf).
                ARCDocsComp = NOT ARCAvcConf.
            END.
        END.
        WHEN {&bedtDocDEV} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtDocDEV}, NOT DEVDocsComp).
            DEVDocsComp = NOT DEVDocsComp.
            IF DEVDocsComp THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtAvcDEV}, NOT DEVDocsComp).
                DEVAvcConf = NOT DEVDocsComp.
            END.
        END.
        WHEN {&bedtAvcDEV} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtAvcDEV}, NOT DEVAvcConf).
            DEVAvcConf = NOT DEVAvcConf.
            IF DEVAvcConf THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtDocDEV}, NOT DEVAvcConf).
                DEVDocsComp = NOT DEVAvcConf.
            END.
        END.
        WHEN {&bedtDocFAC} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtDocFAC}, NOT FACDocsComp).
            FACDocsComp = NOT FACDocsComp.
            IF FACDocsComp THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtAvcFAC}, NOT FACDocsComp).
                FACAvcConf = NOT FACDocsComp.
            END.
        END.
        WHEN {&bedtAvcFAC} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&bedtAvcFAC}, NOT FACAvcConf).
            FACAvcConf = NOT FACAvcConf.
            IF FACAvcConf THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bedtDocFAC}, NOT FACAvcConf).
                FACDocsComp = NOT FACAvcConf.
            END.
        END.
        WHEN {&Docmaildev} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Docmaildev}, NOT Docmaildev).
            Docmaildev = NOT Docmaildev.
        END.

        /*...$A49610*/
        WHEN {&bediter-proforma} THEN RUN choose-bediter-proforma.
        WHEN {&bdc}              THEN RUN choose-bdc.
        WHEN {&bcd}              THEN RUN choose-bcd.
        WHEN {&bsaisie-liv}      THEN RUN choose-bsaisie-liv.
        WHEN {&bcbl}             THEN RUN choose-bcbl.
        WHEN {&bcomptoir}        THEN RUN choose-bcomptoir. /*$2176*/
        WHEN {&bediter-interne}  THEN RUN choose-bediter-interne.
        WHEN {&bannuler-bp}      THEN RUN choose-bannuler-bp.
        WHEN {&bediter-facture}  THEN RUN choose-bediter-facture.
        WHEN {&bediter-devis}    THEN RUN choose-bediter-devis.
        WHEN {&bediter-spe  }    THEN RUN choose-bediter-spe. /*$BOIS*/
        WHEN {&bediter-ctrvt}    THEN RUN CHOOSE-BEDITER-ctrvt. /*$2629*/
        WHEN {&Bajouter-colonnes} THEN RUN CHOOSE-Bajouter-colonnes.
        WHEN {&bvoir}            THEN RUN choose-bvoir.
        /* $A41067... */
        WHEN {&bfiche}           THEN RUN CHOOSE-bfiche IN hapi-piece-cl.
        WHEN {&binfor}           THEN RUN choose-binfor IN hapi-piece-cl.
        /* ...$A41067 */
        WHEN {&bdevis}           THEN RUN choose-bdevis.
        WHEN {&Baffcde}          THEN RUN choose-Baffcde.
        WHEN {&Baffenc}          THEN RUN Ctrlf8.
        WHEN {&Bwrkf}            THEN RUN choose-Bwrkf.
        WHEN {&BRelance}         THEN RUN CHOOSE-BRELANCE. /* $1855 */
        WHEN {&bgendm}           THEN RUN CHOOSE-BGENDM.
        WHEN {&bverifpx}         THEN RUN choose-bverifpx.
        WHEN {&bavan}            THEN RUN choose-bavan.
        WHEN {&Bfax}             THEN RUN CHOOSE-Bfax.
        WHEN {&Bdetailtemps}     THEN RUN CHOOSE-Bdetailtemps. /*$A39538*/
        WHEN {&B-mail}           THEN RUN CHOOSE-B-mail.
        WHEN {&bsms}             THEN RUN choose-bsms. /* $A80432 */
        WHEN {&bversdev}         THEN RUN CHOOSE-bversdev.
        WHEN {&btn-infoentete}   THEN RUN CHOOSE-INFOENTETE.
        WHEN {&Breaffect}        THEN DO:
            G-caisse = Pcaisse.
            RUN regmod_z.
        END.
        WHEN {&Bacompte}        THEN DO: /*$2624*/
            DEF VAR Pticket  AS INT    NO-UNDO.
            DEF VAR Pmontant AS DEC    NO-UNDO.
            DEF VAR Puser    AS CHAR  NO-UNDO.

            run controle-creer.
            IF RETURN-VALUE="9" THEN return no-apply.

            G-caisse = Pcaisse.
            RUN saiaco_z(INT(cli$:SCREEN-VAL IN FRAME F1),"",NO,0,OUTPUT Pticket, OUTPUT Pmontant,OUTPUT puser).
            IF Pticket > 0 THEN RUN openquery.
        END.
        WHEN {&Bcorres-f} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bcorres-f}, NOT CorFax).
            CorFax = NOT CorFax.
        END.
        WHEN {&Bcorres-m} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bcorres-m}, NOT Cormail).
            Cormail = NOT Cormail.
        END.
        /*$A49610...*/
        WHEN {&Docmaildev} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Docmaildev}, NOT Docmaildev).
            Docmaildev = NOT Docmaildev.
        END.
        WHEN {&DocmailBL} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&DocmailBL}, NOT DocmailBL).
            DocmailBL = NOT DocmailBL.
        END.
        WHEN {&DocmailARC} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&DocmailARC}, NOT DocmailARC).
            DocmailARC = NOT DocmailARC.
        END.
        WHEN {&DocmailFac} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&DocmailFac}, NOT DocmailFac).
            DocmailFac = NOT DocmailFac.
        END.
        /*...$A49610*/
        WHEN {&bproforma-f} THEN DO: /*$A20038*/
            CBCheckItem(hComBars-f1, 2, {&bproforma-f}, NOT Proforma-fax).
            Proforma-fax = NOT Proforma-fax.
        END.
        WHEN {&bproforma-m} THEN DO: /*$A20038*/
            CBCheckItem(hComBars-f1, 2, {&bproforma-m}, NOT Proforma-mail).
            Proforma-mail = NOT Proforma-mail.
        END.
        WHEN {&Bmaildev} THEN DO:
            ASSIGN Logbid = CBCheckItem(hComBars-f1, 2, {&Bmaildev}, NOT Wmaildev)
                   Wmaildev = NOT Wmaildev
                   Logbid = CBCheckItem(hComBars-f1, 2, {&Bmailoffre}, NO)
                   Wmailoffre = NO.
        END.
        WHEN {&Bmailoffre} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bmailoffre}, NOT Wmailoffre).
            Wmailoffre = NOT Wmailoffre.
            IF Wmailoffre
                THEN ASSIGN Logbid = CBCheckItem(hComBars-f1, 2, {&Bmaildev}, NO)
                            Wmaildev = NO
                            Logbid = CBCheckItem(hComBars-f1, 2, {&Bmaildev1}, NO)
                            Logbid = CBCheckItem(hComBars-f1, 2, {&Bmaildev12}, NO)
                            Logbid = CBCheckItem(hComBars-f1, 2, {&Bmaildevt}, NO)
                            devniv1 = NO devniv2 = NO devniv3 = NO.
        END.
        WHEN {&Bmaildev1} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bmaildev1}, NOT devniv1).
            CBCheckItem(hComBars-f1, 2, {&Bmaildev12}, NO).
            CBCheckItem(hComBars-f1, 2, {&Bmaildevt}, NO).
            ASSIGN devniv1 = NOT devniv1 devniv2 = NO devniv3 = NO.
        END.
        WHEN {&Bmaildev12} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bmaildev1}, NO).
            CBCheckItem(hComBars-f1, 2, {&Bmaildev12}, NOT devniv2).
            CBCheckItem(hComBars-f1, 2, {&Bmaildevt}, NO).
            ASSIGN devniv2 = NOT devniv2 devniv1 = NO devniv3 = NO.
        END.
        WHEN {&Bmaildevt} THEN DO:
            CBCheckItem(hComBars-f1, 2, {&Bmaildev1}, NO).
            CBCheckItem(hComBars-f1, 2, {&Bmaildev12}, NO).
            CBCheckItem(hComBars-f1, 2, {&Bmaildevt}, NOT devniv3).
            ASSIGN devniv3 = NOT devniv3 devniv1 = NO devniv2 = NO.
        END.
        WHEN {&Bgstenvoi} THEN DO:
            session:set-wait ("general").
            rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
            FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
            RUN mntfax("C", IF AVAIL entetcli THEN entetcli.cod_cli ELSE 0).
            session:set-wait ("").
        END.
        WHEN {&bintegrer} THEN do: /*$A41829*/
            DEFINE VARIABLE vclient AS INTEGER  NO-UNDO.
            DEFINE VARIABLE vtype AS CHARACTER  NO-UNDO.
            IF CAN-FIND(FIRST client WHERE client.cod_cli = int(cli$:SCREEN-VAL IN FRAME F1) NO-LOCK) THEN vclient = int(cli$).
            IF type-saisie = "C" OR type-saisie = "D" THEN vtype = type-saisie.
            RUN intcde_c(vtype, vclient).
            RUN openquery.
        END.
        WHEN {&bparedtg} THEN RUN choose-bparedtg.
        OTHERWISE DO:
            IF pButton > 350 AND pButton < 400 THEN RUN CHOOSE-BSITEWEB(pbutton). /* $A41067 */
        END.
    END CASE.
    ELSE IF pNumComBar = 2 THEN DO:
         /*$A38645...*/
        IF pbutton > {&bcatelec} AND pbutton <= 300 THEN DO:
            FIND FIRST tt-catalog WHERE tt-catalog.tt-option = pbutton NO-LOCK NO-ERROR.
            IF AVAIL tt-catalog THEN
                RUN lancerCatalogue (tt-catalog.tt-id,tt-catalog.tt-libelle, tt-catalog.tt-appelant).
        END.
        /*...$A38645*/

        CASE pButton:
            WHEN {&bannuler-p}   THEN RUN Choose-bannuler-p.
            WHEN {&bvalider-p}   THEN RUN Choose-bvalider-p.
            WHEN {&bsupprimer-p} THEN RUN Choose-bsupprimer-p.
            WHEN {&brecalcul}    THEN RUN Choose-brecalcul.
            WHEN {&btexte}       THEN RUN Choose-btexte.
            WHEN {&bnote}       THEN RUN Choose-bnote.
            WHEN {&bcolonnes}    THEN RUN Choose-bcolonnes.
            WHEN {&bdocinfo}     THEN RUN Choose-bdocinfo.
            WHEN {&btablibre}    THEN RUN Choose-btablibre. /*$A67777*/
            WHEN {&bchgadr}      THEN RUN Choose-bchgadr.
            WHEN {&badrliv}      THEN RUN Choose-badrliv. /*$A35969*/
        END CASE.
    END.
    /*$A35969...*/
    ELSE IF pNumComBar = hComBars-adrliv THEN CASE pButton:
        WHEN {&babandonner-adrliv} THEN RUN Choose-babandonner-adrliv.
        WHEN {&bvalider-adrliv}   THEN RUN Choose-bvalider-adrliv.
    END CASE.
    /*...$A25969*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE controle-creer wsaicl1
PROCEDURE controle-creer :
DEFINE VARIABLE aEnc_max AS DECIMAL    NO-UNDO. /*$1983*/
DEF VAR ii AS INT    NO-UNDO.
DO WITH FRAME F1:
    IF VALID-HANDLE(child-info) THEN RUN ChildBottom IN child-info.
    /* Contr�le */
    ASSIGN cli$:BGCOLOR = 16 qui$:BGCOLOR = 16 dat-liv$:BGCOLOR = 16 sem$:BGCOLOR = 16.

    DEF VAR buf-err-objet AS CHAR NO-UNDO.
    IF NOT PCL_CTRL_ACCES_CREATION (type-saisie, INT(cli$:SCREEN-VAL), qui$:SCREEN-VAL, DATE(dat-cde$:SCREEN-VALUE), DATE(dat-liv$:SCREEN-VALUE), INT(sem$:SCREEN-VAL), OUTPUT buf-err-objet, OUTPUT txtmsg) THEN DO:

        DO ii = 1 TO NUM-ENTRIES(buf-err-objet,"|") :
            CASE ENTRY(ii,buf-err-objet,"|") :
                WHEN "CLIENT":U   THEN cli$:BGCOLOR = 31.
                WHEN "PERSONNE":U THEN qui$:BGCOLOR = 31.
                WHEN "DATECDE":U  THEN dat-cde$:BGCOLOR = 31.
                WHEN "DATELIV":U  THEN dat-liv$:BGCOLOR = 31.
                WHEN "SEMLIV":U   THEN sem$:BGCOLOR = 31.
                OTHERWISE RETURN ERROR.
            END CASE.
        END.

        CASE ENTRY(1,buf-err-objet,"|") :
            WHEN "CLIENT":U   THEN wid = cli$:HANDLE.
            WHEN "PERSONNE":U THEN wid = qui$:HANDLE.
            WHEN "DATECDE":U  THEN wid = dat-cde$:HANDLE.
            WHEN "DATELIV":U  THEN wid = dat-liv$:HANDLE.
            WHEN "SEMLIV":U   THEN wid = sem$:HANDLE.
            OTHERWISE RETURN ERROR.
        END CASE.

        {erreur-p.i}

    END.

    ASSIGN qui$ dat-cde$ dat-liv$ sem$ ref-cde$ not-ref$ typ-cde=(if plusieurs-types then int(substr(typ-cde$:screen-value,1,1)) else 1).
    RUN PCL_CTRL_ENCOURS_CLIENT IN hapi-piece-cl (INT(cli$:SCREEN-VAL), type-saisie, OUTPUT logbid).
    IF NOT logbid THEN RETURN "9".
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ControleProduction wsaicl1
PROCEDURE ControleProduction :
DEF OUTPUT PARAM continuer AS LOG INIT NO no-undo.
DO WITH FRAME f1:
    IF CAN-FIND (FIRST prefab where prefab.cod_cli = entetcli.cod_cli
                                and prefab.no_cde = entetcli.NO_cde
                                AND prefab.statut = "G" NO-LOCK) THEN
    DO:
        IF can-find(droigco where droigco.util=g-user-droit and droigco.fonc="GPMODOFS") THEN
        DO:
            MESSAGE "" + "~t" + Traduction("Vous n'�tes pas autoris� � supprimer une ligne de commande li�e � la production (statut sugg�r�) !",-2,"")
                VIEW-AS ALERT-BOX ERROR.
            continuer = NO.
            RETURN.
        END.
    END.
    IF CAN-FIND (FIRST prefab where prefab.cod_cli = entetcli.cod_cli
                                and prefab.no_cde = entetcli.NO_cde
                                AND prefab.statut = "L" NO-LOCK) THEN
    DO:
        IF can-find(droigco where droigco.util=g-user-droit and droigco.fonc="GPMODOFL") THEN
        DO:
            MESSAGE "" + "~t" + Traduction("Vous n'�tes pas autoris� � supprimer une ligne de commande li�e � la production (statut lanc�) !",-2,"")
                VIEW-AS ALERT-BOX ERROR.
            continuer = NO.
            RETURN.
        END.

        continuer = NO.
        MESSAGE Traduction("Attention ! Des OFs attach�s � votre commande sont lanc�s en production.",-2,"") + " " + "~n" + Traduction("Si vous poursuivez, la ligne de commande sera supprim�e et les OFs de statut lanc�s seront d�tach�s de la commande.",-2,"") + "~n" + "" + "~n" + Traduction("Voulez-vous continuer ?",-2,"")
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE continuer.
        IF continuer = YES THEN
        do:
            continuer = NO.
            MESSAGE Traduction("Confirmez-vous la suppression ?",-2,"") + " "
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE continuer.
        END.
    END.
    ELSE
        continuer = YES.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cr-plan wsaicl1
PROCEDURE cr-plan :
retour=NO.
    FIND CURRENT entetcli NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
    IF type-plan="T" AND parsoc.cr_tlv THEN RUN cr_tlv (rowid-planav,entetcli.no_cde,entetcli.mt_ht,OUTPUT retour).
    ELSE IF type-plan="C" AND parsoc.cr_vis THEN RUN cr_com (rowid-planav,entetcli.no_cde,entetcli.mt_ht,OUTPUT retour).
    ELSE DO TRANSACTION:
        FIND planav WHERE ROWID(planav)=rowid-planav EXCLUSIVE-LOCK NO-ERROR.
        ASSIGN
            parnum = truncate(time / 60,0)
            parnum = truncate(parnum / 60,0) * 100 + (parnum mod 60)
            app_trt=YES
            hr_reel=substr(string(parnum, "9999"),1,2) + ":" + substr(string(parnum, "9999"),3,2)
            planav.NO_cde=entetcli.NO_cde
            planav.mt_ht=entetcli.mt_ht.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creation-tournee wsaicl1
PROCEDURE creation-tournee :
DEFINE VARIABLE svno_adr AS INTEGER NO-UNDO.
ASSIGN
    entetcli.civ_liv=getsessiondataex("Tournee", "Civ","")
    entetcli.adr_liv[1]=getsessiondata("Tournee", "A1")
    entetcli.adr_liv[2]=getsessiondataex("Tournee", "A2","")
    entetcli.adr_liv[3]=getsessiondataex("Tournee", "A3","")
    entetcli.adrliv4=getsessiondataex("Tournee", "A4","")
    entetcli.k_post2l=getsessiondataex("Tournee", "Cp","")
    entetcli.villel=getsessiondata("Tournee", "Vil")
    entetcli.paysl=getsessiondata("Tournee", "Pay")
    entetcli.tournee=getsessiondata("Tournee", "Tou")
    entetcli.NO_ordre=getsessionInt("Tournee", "Ord",0)
    svno_adr=wno_adr
    wno_adr=getsessionInt("Tournee", "Adr",0)
    entetcli.hr_dep=getsessionInt("Tournee", "Hr",0)
    /*$A35969...*/
    entetcli.zon_geo = getsessionInt("Tournee":U, "zon_geo",0)
    entetcli.typ_veh = getsessionLog("Tournee":U, "typ_veh",NO)
    entetcli.typ_con = getsessiondataex("Tournee":U, "typ_con","").
    /*...$A35969*/

IF entetcli.tournee=? THEN ASSIGN
    entetcli.tournee=""
    entetcli.NO_ordre=0.
ELSE IF entetcli.tournee="CO" AND entetcli.depot<10 THEN DO:
    entetcli.tournee=entetcli.tournee + STRING(entetcli.depot,"9").
    FIND tabgco WHERE tabgco.type_tab="TO" AND tabgco.a_tab="CO" + STRING(entetcli.depot,"9") NO-LOCK NO-ERROR.
    if not available tabgco then do:
        CREATE tabgco.
        ASSIGN
            tabgco.type_tab="TO"
            tabgco.a_tab="CO" + STRING(entetcli.depot,"9").
        tabgco.inti_tab=Traduction("Comptoir (Pris sur place)",-2,"").
    end.
END.
/* L'adresse livraison par d�faut a peut-�tre �t� chang�e ? */
IF entetcli.k_postl<>wno_adr THEN DO:
    entetcli.k_postl=wno_adr.
    find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if entetcli.cl_livre=0 THEN entetcli.cod_cli ELSE entetcli.cl_livre) and adresse.affaire = "" and adresse.cod_adr=wno_adr no-lock no-error.
    IF AVAIL adresse THEN DO :
        ASSIGN
        /*entetcli.zon_geo =(IF adresse.zon_geo= 0 THEN client.zon_geo  ELSE adresse.zon_geo) /*$1728*/ $A35969*/
        entetcli.transpor=(IF adresse.transpor=0 THEN client.transpor ELSE adresse.transpor) /*$1728*/
        entetcli.MOD_liv =(IF adresse.MOD_liv="" THEN client.MOD_liv  ELSE adresse.MOD_liv) /*$1728*/
        entetcli.comment=adresse.comment
        entetcli.com_liv[1]=adresse.com_liv[1]
        entetcli.com_liv[2]=adresse.com_liv[2]
        entetcli.com_liv[3]=adresse.com_liv[3]
        entetcli.com_liv[4]=adresse.com_liv[4].
       /*$1728...*/
       find tabgco where tabgco.type_tab="ML" and a_tab=entetcli.MOD_liv no-lock no-error.
       if available tabgco then entetcli.port=tabgco.code_df.
       /*...$1728*/

       IF adresse.transpor=0 AND pref_parcde_transpor THEN DO:
           FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = entetcli.depot NO-LOCK NO-ERROR.
           IF AVAIL depcli AND depcli.transpor > 0 THEN entetcli.transpor = depcli.transpor.
       END.
   end.
END.
wno_adr=svno_adr.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creer-aff wsaicl1
PROCEDURE creer-aff :
DO with frame fc1:
    ASSIGN logbid = session:set-wait-state("GENERAL")
         frame Fc1:sensitive = NO
         wsaicl1:SENSITIVE = NO /*$A122103*/
         parnum = int(input entetcli.affaire) no-error.

  if not error-status:error then numaff = string(int(input entetcli.affaire),"ZZZZZZZZZZZ").
                            else numaff = fill(" ",11 - length(input entetcli.affaire)) + input entetcli.affaire.
   /*$A36981...*/
   IF AVAIL parsoc AND parsoc.c_nuit = 1 AND entetcli.nat_cde:SCREEN-VALUE <> "" THEN DO:
       SetSessionData ("SAICL1":U, "NATURE":U,entetcli.nat_cde:SCREEN-VALUE).
   END.
   /*...$A36981*/
  /* $930 ... */
/*RUN mntaff_af(0, INPUT-O svrowid-lig, int(entetcli.cod_cli:screen-value in frame fonglet), date(entetcli.dat_cde:screen-value in frame fc1), INPUT-O numaff, OUTPUT lib-aff, OUTPUT retour, ?, INT(edepot:SCREEN-VAL), "").*/
  RUN mntaff_af(0, INPUT-O svrowid-lig, int(entetcli.cod_cli:screen-value in frame fonglet), date(entetcli.dat_cde:screen-value in frame fc1), INPUT-O numaff, OUTPUT lib-aff, OUTPUT retour, ?, INT(edepot:SCREEN-VAL), "",? /*$1782*/).
  /* ... $930 */

  if retour then do:
    entetcli.affaire:screen-value = numaff.
    display lib-aff.
  end.
  SetSessionData ("SAICL1":U, "NATURE":U,""). /*$A36981*/
  SetSessionData ("AFF-API":U, "AFFAIRE":U,""). /*$A33230*/

  frame Fc1:sensitive = yes.
  wsaicl1:SENSITIVE = YES. /*$A122103*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creer-imglist wsaicl1
PROCEDURE creer-imglist :
CREATE CONTROL-FRAME CtrlFrame-2 ASSIGN
           FRAME           = FRAME fonglet:HANDLE
           ROW             = 7.75
           COLUMN          = 37
           HEIGHT          = 4.17
           WIDTH           = 14.29
           WIDGET-ID       = 12
           HIDDEN          = YES
           SENSITIVE       = YES.

    ASSIGN
       chCtrlFrame-2 = CtrlFrame-2:COM-HANDLE
       logbid = chCtrlFrame-2:LoadControls(ProLink + "\ImageList.wxx", "FrmImageList":U)
       CtrlFrame-2:NAME = "imglist".

    chCtrlFrame-2:ImageList:listImages:ADD(1,,LOAD-PICTURE(GcoMedia + "\onglet.bmp")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creer-tabstrip wsaicl1
PROCEDURE creer-tabstrip :
CREATE CONTROL-FRAME CtrlFrame ASSIGN
           FRAME           = FRAME fonglet:HANDLE
           ROW             = 1.17
           COLUMN          = 1.29
           HEIGHT          = 19.71
           WIDTH           = 113
           HIDDEN          = NO
           SENSITIVE       = YES.

    ASSIGN chCtrlFrame = CtrlFrame:COM-HANDLE
           logbid = chCtrlFrame:LoadControls(ProLink + "\TabStrip.wxx":U, "FrmTabStrp":U)
           CtrlFrame:NAME = "CtrlFrame":U.

    chCtrlFrame:TabStrip:ImageList = chCtrlFrame-2:ImageList.

    chCtrlFrame:TabStrip:Tabs:CLEAR.

    chCtrlFrame:TabStrip:Tabs:ADD(1,"O1":U, Traduction("En-t�te",-2,"")).
    chCtrlFrame:TabStrip:Tabs:ADD(2,"O2":U, Traduction("En-t�te Divers",-2,"")).
    chCtrlFrame:TabStrip:Tabs:ADD(3,"O3":U, Traduction("Valeurs libres",-2,"")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CREER_AFFRETEMENT wsaicl1
PROCEDURE CREER_AFFRETEMENT :
/*$1728 CREATION PROCEDURE*/
DEF VAR ROWID_enttrans    AS ROWID NO-UNDO .
DEF VAR nb_tranhist       AS INTEGER NO-UNDO .
DEF VAR v-trp_poids       AS DEC NO-UNDO .
DEF VAR v-trp_VOLUME      AS DEC NO-UNDO.
DEF VAR v-trp_NBR_COLIS   AS INT NO-UNDO.
DEF VAR v-trp_NBR_PALETTE AS INT NO-UNDO.
DEF VAR v-trp_NBR_ML      AS DEC NO-UNDO. /*$A57952*/
def var v-MT_PORT         as dec no-undo .
def var v-MT_PORT_FAC     as dec no-undo .
def var v-MT_PORT_ACHAT   as dec no-undo .
DEFINE BUFFER trp_produit FOR produit .
DEF VAR cpt_tranhist AS INT NO-UNDO .
DEF BUFFER btranhist FOR tranhist .
DEF VAR cde_ach_vte_interne AS LOG no-undo.
DEF BUFFER tabgco_1728 FOR tabgco .

/*$A74887 IF NOT parsoc.aff_cli  THEN RETURN .*/

/* Gestion Magasin-Entrepot : la commande client cr��e la commande fournisseur
sauf si c'est une commande fournisseur qui a cr��e cette commande client */
cde_ach_vte_interne = NO .
IF entetcli.typ_sai="C" AND SUBSTR(entetcli.caution,1,2)<>"$$" THEN DO:
    FIND tabgco_1728 WHERE tabgco_1728.TYPE_tab="DS" AND tabgco_1728.cod_cli=entetcli.cod_cli NO-LOCK NO-ERROR.
    IF AVAIL tabgco_1728 THEN DO:
        FIND tabgco_1728 WHERE tabgco_1728.TYPE_tab="DS" AND tabgco_1728.n_tab=entetcli.depot NO-LOCK NO-ERROR.
        IF AVAIL tabgco_1728 AND tabgco_1728.cod_fou>0 THEN
            cde_ach_vte_interne = CAN-FIND (fournis where fournis.cod_fou=tabgco_1728.cod_fou no-lock).
    END.
END.
/* Gestion Magasin-Entrepot : la livraison client g�n�re la r�ception fournisseur */
ELSE IF entetcli.typ_sai="F" /*AND SUBSTR(entetcli.caution,1,3)="$$I"*/ THEN DO:
    FIND entetfou WHERE entetfou.regr=entetcli.NO_cde AND entetfou.typ_sai="C" AND entetfou.retr_cess<>"" NO-LOCK NO-ERROR.
    IF AVAIL entetfou THEN DO:
        FIND tabgco_1728 WHERE tabgco_1728.TYPE_tab="DS" AND tabgco_1728.n_tab=entetfou.depot NO-LOCK NO-ERROR.
        IF AVAIL tabgco_1728 THEN cde_ach_vte_interne =  YES .
    END.
END.
IF NOT cde_ach_vte_interne THEN cde_ach_vte_interne = can-FIND(FIRST parms WHERE parms.cod_cl2=entetcli.cod_cli AND parms.cod_fo2>0 NO-LOCK).

FIND FIRST tabgco WHERE tabgco.TYPE_tab = "ML" AND tabgco.a_tab = entetcli.MOD_liv AND tabgco.externe = YES NO-LOCK NO-ERROR .
IF cde_ach_vte_interne = NO AND AVAILABLE tabgco THEN DO :
   IF NOT (entetcli.typ_sai = "A" OR entetcli.typ_sai = "D" OR entetcli.typ_sai = "F" OR (entetcli.typ_sai = "r" AND entetcli.TYPE_fac<> "CO" ) )
      AND NOT CAN-FIND (FIRST tranhist  where tranhist.cod_cli = entetcli.cod_cli and tranhist.typ_sai_c = entetcli.typ_sai
                             AND tranhist.no_cde_c = entetcli.no_cde and tranhist.no_bl_c = entetcli.NO_bl
                             AND tranhist.affret = YES NO-LOCK )
   THEN DO:
        /* Cr�ation des informations de transport sur les commandes CLIENT */
        FOR FIRST trp_produit WHERE trp_produit.cod_pro = entetcli.port NO-LOCK :
            RUN creation_maj_affretement IN G-htrpaff("", "", "", "", 0, trp_produit.nom_pro, ROWID(entetcli), INPUT-OUTPUT vNeSertPas).
        END.

        RUN derlig_c(no,OUTPUT derlig,ROWID (entetcli)).
   END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Ctrlf8 wsaicl1
PROCEDURE Ctrlf8 :
/* RUN CacherChildInfo. 4.4 */
rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
RUN PCL_VISU_PIECE IN hapi-piece-cl(rentetcli). /* $2178 */     /* $2240 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CtrlFrame.TabStrip.Click wsaicl1
PROCEDURE CtrlFrame.TabStrip.Click :
IF pascharger THEN RETURN. /* $A38251 */
    RUN VALIDER-CHAPITRE.
    IF RETURN-VALUE = "error" THEN DO:
                /* $A38251... */
        pascharger = YES.
        myonglet:tabs (chap-cours):SELECTED = YES.
        pascharger = NO.
                /* ...$A38251 */
        RETURN NO-APPLY.
    END.
    chap-cours = chCtrlFrame:TabStrip:SelectedItem:INDEX.
    RUN envoi-chapitre.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DATPX-SAISON wsaicl1
PROCEDURE DATPX-SAISON :
FIND FIRST tabgco WHERE tabgco.TYPE_tab = "ss" AND tabgco.a_tab = entetcli.saison:SCREEN-VALUE IN FRAME fc2 NO-LOCK NO-ERROR.
    IF AVAILABLE tabgco THEN entetcli.dat_px:SCREEN-VALUE IN FRAME fc1 =
        IF DATE(tabgco.to_chauf) >= DATE(entetcli.dat_px:SCREEN-VALUE) THEN (tabgco.to_chauf)
        ELSE IF client.date_px="C" THEN entetcli.dat_cde:SCREEN-VAL
        ELSE IF client.date_px="L" THEN entetcli.dat_liv:SCREEN-VAL
        ELSE IF parsoc.cod_dev>=99 THEN entetcli.dat_cde:SCREEN-VAL
        ELSE entetcli.dat_liv:SCREEN-VAL.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE defaultaction wsaicl1
PROCEDURE defaultaction :
RUN choose-bmodifier.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletecharacter wsaicl1
PROCEDURE deletecharacter :
RUN CHOOSE-BSUPPRIMER.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Ecran1_Ok wsaicl1
PROCEDURE Ecran1_Ok :
IF svrowid <> ? THEN FreeLock ("entetcli", STRING(svrowid)) NO-ERROR. /* $A36207 */

    ASSIGN
        ref-cde$:SCREEN-VAL IN FRAME f1=""
        not-ref$:screen-val="".
    IF NOT parcde.gst_zone[99] THEN ASSIGN
        dat-liv$:SCREEN-VAL=string(dat-liv-ini)
        sem$:SCREEN-VAL=string(sem-liv-ini).
    IF AVAIL parsoc AND (type-saisie = "R" AND AVAIL parsoc AND parsoc.rab_mgv) THEN /* $914 */ ASSIGN
        qui$ = ""
        qui$:SCREEN-VAL = ""
        lib-mg:SCREEN-VAL = "".
    RUN Ecran2_NonOk.
    RUN ERGO-PROC-WINDOW-RESIZED2.
    VIEW FRAME f1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Ecran2_NonOk wsaicl1
PROCEDURE Ecran2_NonOk :
assign
        frame fc1:hidden=YES
        frame fc2:hidden=YES
        frame fval:hidden=YES
        frame fonglet:hidden=YES
        frame fentete:hidden=YES.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Ecran2_Ok wsaicl1
PROCEDURE Ecran2_Ok :
assign
        frame fonglet:hidden=no
        frame fentete:hidden=no.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE edition-bp wsaicl1
PROCEDURE edition-bp :
DO with frame f1:
do transaction:
    EMPTY TEMP-TABLE WEnt NO-ERROR.
    create WEnt.
    assign
       WEnt.wcli = entetcli.cod_cli
       WEnt.wcde = entetcli.no_cde.
end.
if valid-handle(child-info) then RUN ChildBottom IN child-info.
run Edbp1_c (no, YES,"", "I","I", no, no,no,
             "",?,?, /*$1265 */
             output logbid).
RUN refresh-hbrowse IN hapi-piece-cl.   /* $2240 */
session:set-wait-state("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-chapitre wsaicl1
PROCEDURE envoi-chapitre :
session:set-wait-state("").
    IF not(v-rapide = "oui" AND porigine <> "2" AND porigine <> "6b") THEN RUN LockWindowUpdate IN G-HPROWIN (Wsaicl1:HWND).
    DO x = 1 TO nb-chap :
        chCtrlFrame:TabStrip:Tabs(x):IMAGE = 0.
    END.
    chCtrlFrame:TabStrip:Tabs(chap-cours):IMAGE = 1.

    CASE chap-cours:
        when 1 then assign
            frame fc1:hidden=no
            first-wid=first-fc1.
        when 2 then assign
            frame fc2:hidden=no
            first-wid=entetcli.num_tel:HANDLE.
        when 3 then assign
            frame fval:hidden=no
            first-wid=first-val.
    END CASE.
    if valid-handle(first-wid) then apply "entry" to first-wid.
    else if chap-cours=1 then apply "entry" to entetcli.dat_cde in frame fc1.
    IF NOT debut-prog THEN
        VIEW FRAME Fonglet IN WINDOW Wsaicl1.
    ASSIGN Logbid = FRAME fonglet:MOVE-TO-TOP()
           Logbid = IF chap-cours=1 THEN FRAME Fc1:MOVE-TO-TOP() ELSE IF chap-cours=2 THEN FRAME Fc2:MOVE-TO-TOP() ELSE FRAME Fval:MOVE-TO-TOP().
    IF not(v-rapide = "oui" AND porigine <> "2" AND porigine <> "6b") THEN RUN LockWindowUpdate IN G-HPROWIN (0).
    /* $A37703... */
    if valid-handle(first-wid) then apply "entry" to first-wid.
    else if chap-cours=1 then apply "entry" to entetcli.dat_cde in frame fc1.
    /* ...$A37703 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-entete wsaicl1
PROCEDURE envoi-entete :
assign
    frame f1:hidden=YES
    chap1=1
    modif-entete=no
    val-traite=yes  val-chap1 = NO val-chap2 = NO
    adrliv-modifie = NO /*$A60521*/
    note-modifiee=no.
RUN Ecran2_NonOk.
IF v-rapide = "oui" AND porigine <> "2" AND porigine <> "6b" THEN RUN LockWindowUpdate IN G-HPROWIN (Wsaicl1:HWND). /*$872*/
EMPTY TEMP-TABLE wecli NO-ERROR. /* $1105 */

IF NOT (porigine = "6a" AND wori_saibp1) THEN wori_ate = NO. /* si on ne vient pas de saibp1, on remet la variable � no */ /* $a36019 */

/* Cr�ation */
IF CREATION THEN DO:
    DO WITH FRAME fonglet:
        /* N� commande suivante */
        display
            nocde @ entetcli.NO_cde
            client.cod_cli @ entetcli.cod_cli.
        if type-saisie="R" then display /*string( JL*/
            next-value(compteur_bl) /*)*/ @ entetcli.no_bl.
        else display
            "" @ entetcli.no_bl.
        IF type-saisie = "D" AND wori_ate THEN /* $754 si atelier on affiche l'immat */
            fill-libmat:SCREEN-VAL IN FRAME Fonglet = ori_mat.
        ELSE fill-libmat:SCREEN-VAL IN FRAME Fonglet = "".
    END.

    DO WITH FRAME fentete:
        /* Affichage */
        case type-saisie:
            when "C" then ftitre-e = Traduction("Cr�ation commande",-2,"").
            when "F" then DO:
                /*$A95455 IF nopt = 556 THEN ftitre-e = Traduction("Cr�ation avoir",-2,""). /* Avoir financier */
                ELSE ftitre-e = Traduction("Cr�ation facture",-2,"").*/
                /*$A95455...*/
                IF GetSessionData("copylg_c","avoir":U) <> "" THEN DO:
                    IF GetSessionData("copylg_c","avoir":U) = "YES" THEN ftitre-e = Traduction("Cr�ation avoir",-2,""). ELSE ftitre-e = Traduction("Cr�ation facture",-2,"").
                END.
                ELSE DO:
                    IF nopt = 556 THEN ftitre-e = Traduction("Cr�ation avoir",-2,""). /* Avoir financier */
                    ELSE ftitre-e = Traduction("Cr�ation facture",-2,"").
                END.
                /*...$A95455*/
            END.
            when "D" then ftitre-e = Traduction("Cr�ation devis",-2,"").
            when "R" THEN DO:
                if client.type_fac="CO" THEN ftitre-e = Traduction("Cr�ation facture comptoir",-2,"").
                ELSE ftitre-e = Traduction("Cr�ation B.L comptoir",-2,"").
            END.
        end case.
        MAJ-TITLE(FRAME fentete:HANDLE, Ftitre-e).
    END.

    /*$A95455...*/
    DEF VAR cTitleF AS CHAR NO-UNDO.
    IF type-saisie = "F" THEN DO:
        IF GetSessionData("copylg_c","avoir":U) <> "" THEN DO:
            IF nopt = 615 THEN cTitleF = Traduction("Facture financi�re",-2,"").
            ELSE
                IF nopt = 556 THEN cTitleF = Traduction("Avoir financier",-2,"").
            ELSE /*nopt = 53*/
                IF GetSessionData("copylg_c","avoir":U) = "YES" THEN cTitleF = Traduction("Avoir client",-2,""). ELSE cTitleF = Traduction("Facture client",-2,"").
        END.
        ELSE DO:
            IF nopt = 615 THEN cTitleF = Traduction("Facture financi�re",-2,""). ELSE cTitleF = Traduction("Facture client",-2,"").
        END.
    END.
    /*...$A95455*/
    {&WINDOW-NAME}:title=
        (if type-saisie="C" then Traduction("Commande client",-2,"") + " "
            else if type-saisie="D" then Traduction("Devis client",-2,"") + " "
            /*$A95455 else if type-saisie="F" then (IF nopt = 615 THEN Traduction("Facture financi�re",-2,"") ELSE Traduction("Facture client",-2,"")) + " " */
            ELSE IF type-saisie="F" THEN cTitleF + " " /*$A95455*/
            else if type-saisie="A" then Traduction("Abonnement",-2,"") + " "
            else Traduction("Comptoir client",-2,"") + " ") +
        client.nom_cli + " " + Traduction("de",-2,"") + " " + client.ville +
        " " + Traduction("N� T�l�phone",-2,"") + " : " + cptcli.num_tel +
        " " + IF parsoc.FORM_liv=3 THEN Traduction("Date exp�dition",-2,"")
              ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�part",-2,"")
              ELSE IF parsoc.FORM_liv=1 THEN Traduction("Date livraison",-2,"")
              ELSE Traduction("Date livraison",-2,"") + " " + "(" + Traduction("D�part",-2,"") + ")" + " " + STRING(dat-liv$).

    DO WITH FRAME Fc1:
        DISPLAY cptcli.civilite @ entetcli.civ_cde client.nom_cli @ entetcli.adr_cde[1]
                client.adresse[1] @ entetcli.adr_cde[2] client.adresse[2] @ entetcli.adr_cde[3] client.adresse[3] @ entetcli.adr_cde[4]
                client.k_post2 @ entetcli.k_post2c client.ville @ entetcli.villec
                client.pays @ entetcli.paysc.
    END.

    /* Valeurs libres */
    do x=1 to 5:
        ASSIGN wn[x]=client.znu[x] wa[x]=client.zal[x] wt[x]=client.zta[x] wd[x]=client.zda[x] wl[x]=client.zlo[x].
    end.
    assign
        cle-cours=client.cod_cli
        type-cours=type-saisie
        local_no_info=0 /*$975*/
        pcol=?.
END.

/* Modification */
ELSE DO:
    find entetcli where rowid(entetcli)=svrowid exclusive-lock no-wait no-error.
    if locked entetcli then do:
        run annul-entete.
        message Traduction("Enregistrement verrouill� !",-2,"") view-as alert-box error.
        return error.
    end.
    /*$A42431*/
    IF IsLockedEx("entetcli", string(svrowid), OUTPUT pQuiLck, OUTPUT pQuiWLck, OUTPUT pPCLck, OUTPUT pDateLck, OUTPUT pHeureLck, OUTPUT pComment) THEN DO:
        MESSAGE Traduction("Acc�s � l'enregistrement en-t�te commande",-2,"") + " " + ":" + "" + " " pcomment SKIP
                Traduction("interdit pour le moment car utilisation exclusive",-2,"") SKIP
                Traduction("Par",-2,"") pQuiLck "(" pQuiWLck ")" SKIP
                Traduction("sur",-2,"") pPCLck SKIP
                Traduction("depuis le",-2,"") pDateLck Traduction("�",-2,"") pHeureLck VIEW-AS ALERT-BOX WARNING.
        frame f1:HIDDEN = NO.
        RETURN error.
    END.

    SetLock ("entetcli", STRING(ROWID(entetcli))). /* $A36207 */

    display
        entetcli.no_cde entetcli.cod_cli entetcli.no_bl WITH FRAME Fonglet.
    /* Affichage */
    case entetcli.typ_sai:
        when "C" then Ftitre-e = Traduction("Modification commande",-2,"").
        when "F" then do:
            /*$A95455 IF nopt = 556 THEN ftitre-e = Traduction("Modification avoir",-2,""). /* Avoir financier */
            ELSE ftitre-e = Traduction("Modification facture",-2,"").*/
            /*$A95455...*/
            IF GetSessionData("copylg_c","avoir":U) <> "" THEN DO:
                IF GetSessionData("copylg_c","avoir":U) = "YES" THEN ftitre-e = Traduction("Modification avoir",-2,""). ELSE ftitre-e = Traduction("Modification facture",-2,"").
            END.
            ELSE DO:
                IF nopt = 556 THEN ftitre-e = Traduction("Modification avoir",-2,""). /* Avoir financier */
                ELSE ftitre-e = Traduction("Modification facture",-2,"").
            END.
            /*...$A95455*/
        END.
        when "A" then Ftitre-e = Traduction("Modification abonnement",-2,"").
        when "D" then Ftitre-e = Traduction("Modification devis",-2,"").
        when "R" then Ftitre-e = SUBSTITUTE(Traduction("Modification &1 comptoir",-2,""),(if entetcli.type_fac="CO" then Traduction("facture",-2,"") else Traduction("B.L",-2,""))).
    end case.
    MAJ-TITLE(FRAME fentete:HANDLE, Ftitre-e).

    RUN envoi-title.

    DO WITH FRAME Fc1:
        DISPLAY entetcli.civ_cde entetcli.adr_cde[1] entetcli.adr_cde[2] entetcli.adr_cde[3] entetcli.adr_cde[4]
                entetcli.k_post2c entetcli.villec entetcli.paysc.
    END.

    /* Valeurs libes */
    do x=1 to 5:
       ASSIGN wn[x]=entetcli.znu[x] wa[x]=entetcli.zal[x] wt[x]=entetcli.zta[x] wd[x]=entetcli.zda[x] wl[x]=entetcli.zlo[x].
    end.
    assign cle-cours=entetcli.cod_cli type-cours=entetcli.typ_sai local_no_info=entetcli.NO_info pcol=entetcli.COL_vis.
    IF entetcli.typ_sai = "D" THEN
    DO:
        FIND FIRST ateent OF entetcli NO-LOCK NO-ERROR.
        IF AVAIL ateent THEN ASSIGN wori_ate = YES fill-libmat:SCREEN-VAL = ateent.materiel. /* $754 */ /* $a36019 */
        ELSE ASSIGN wori_ate = NO fill-libmat:SCREEN-VAL = "". /* $a36019 */
    END.
    /* $1105 ... */
    IF CONNECTED ("WORKFLOW") THEN DO:
        CREATE wecli.
        BUFFER-COPY entetcli TO wecli.
    END.
    /* ... $1105 */
END.

if int(entetcli.no_bl:screen-value)=0 then hide entetcli.no_bl.
fill-libmat:HIDDEN = (fill-libmat:SCREEN-VALUE = "").

RUN envoi-Fc1.
RUN envoi-Fc2.

/* Valeurs libres --> Chapitre 3 */
if parcde.gst_zone[2] then DO:
  DO WITH FRAME FVAL:
    /* Initialisation zones en cr�ation */
    if creation then do:
      do x=1 to 5:
        if substr(vla[x],1,1)="O" and int(substr(vla[x],2,2))<>0 then do:
            assign parnum=int(substr(vla[x],2,2)) val-ini=wa[x].
            run pgm-libre.
            wa[x]=val-ini.
        end.
        if substr(vln[x],1,1)="O" and int(substr(vln[x],2,2))<>0 then do:
            assign parnum=int(substr(vln[x],2,2)) val-ini=string(wn[x]).
            run pgm-libre.
            wn[x]=decimal(val-ini).
        end.
        if substr(vld[x],1,1)="O" and int(substr(vld[x],2,2))<>0 then do:
            assign parnum=int(substr(vld[x],2,2)) val-ini=string(wd[x]).
            run pgm-libre.
            wd[x]=date(val-ini).
        end.
        if substr(vll[x],1,1)="O" and int(substr(vll[x],2,2))<>0 then do:
            assign parnum=int(substr(vll[x],2,2)) val-ini=string(wl[x]).
            run pgm-libre.
            wl[x]=(val-ini="yes").
        end.
      end.
      if br-zt1:private-data<>"" and wt[1]="" then do:
        find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type=br-zt1:private-data no-lock no-error.
        if available enttabdft and enttabdft.dft_poste<>"" then wt[1]=enttabdft.dft_poste.
      end.
      if br-zt2:private-data<>"" and wt[2]="" then do:
        find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type=br-zt2:private-data no-lock no-error.
        if available enttabdft and enttabdft.dft_poste<>"" then wt[2]=enttabdft.dft_poste.
      end.
      if br-zt3:private-data<>"" and wt[3]="" then do:
        find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type=br-zt3:private-data no-lock no-error.
        if available enttabdft and enttabdft.dft_poste<>"" then wt[3]=enttabdft.dft_poste.
      end.
      if br-zt4:private-data<>"" and wt[4]="" then do:
        find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type=br-zt4:private-data no-lock no-error.
        if available enttabdft and enttabdft.dft_poste<>"" then wt[4]=enttabdft.dft_poste.
      end.
      if br-zt5:private-data<>"" and wt[5]="" then do:
        find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type=br-zt5:private-data no-lock no-error.
        if available enttabdft and enttabdft.dft_poste<>"" then wt[5]=enttabdft.dft_poste.
      end.
      do x=1 to 5:
        if substr(vlt[x],1,1)="O" and int(substr(vlt[x],2,2))<>0 then do:
            assign parnum=int(substr(vlt[x],2,2)) val-ini=wt[x].
            run pgm-libre.
            wt[x]=val-ini.
        end.
      end.
    end.
    DO X=1 TO 5:
        assign
            zah[X]:SCREEN-VAL=wa[x]
            znh[X]:SCREEN-VAL=string(wn[x])
            zdh[X]:SCREEN-VAL=string(wd[x])
            zlh[X]:SCREEN-VAL=string(wl[x])
            zth[X]:SCREEN-VAL=wt[x].
    END.
    assign
        lib-zt1:screen-value=""
        lib-zt2:screen-value=""
        lib-zt3:screen-value=""
        lib-zt4:screen-value=""
        lib-zt5:screen-value="".
    if wt[1]<>"" then do:
        find tabgco where tabgco.type_tab=br-zt1:private-data and tabgco.a_tab=zt1:screen-value no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-zt1:screen-value=tabgco.inti_tab. else lib-zt1=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-zt1:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-zt1:screen-value = intiTab.
            ELSE lib-zt1:screen-value = tabgco.inti_tab.
        END.
        else lib-zt1=Traduction("Inexistant",-2,"").
    end.
    if wt[2]<>"" then do:
        find tabgco where tabgco.type_tab=br-zt2:private-data and tabgco.a_tab=zt2:screen-value no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-zt2:screen-value=tabgco.inti_tab. else lib-zt2=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-zt2:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-zt2:screen-value = intiTab.
            ELSE lib-zt2:screen-value = tabgco.inti_tab.
        END.
        else lib-zt2=Traduction("Inexistant",-2,"").
    end.
    if wt[3]<>"" then do:
        find tabgco where tabgco.type_tab=br-zt3:private-data and tabgco.a_tab=zt3:screen-value no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-zt3:screen-value=tabgco.inti_tab. else lib-zt3=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-zt3:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-zt3:screen-value = intiTab.
            ELSE lib-zt3:screen-value = tabgco.inti_tab.
        END.
        else lib-zt3=Traduction("Inexistant",-2,"").
    end.
    if wt[4]<>"" then do:
        find tabgco where tabgco.type_tab=br-zt4:private-data and tabgco.a_tab=zt4:screen-value no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-zt4:screen-value=tabgco.inti_tab. else lib-zt4=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-zt4:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-zt4:screen-value = intiTab.
            ELSE lib-zt4:screen-value = tabgco.inti_tab.
        END.
        else lib-zt4=Traduction("Inexistant",-2,"").
    end.
    if wt[5]<>"" then do:
        find tabgco where tabgco.type_tab=br-zt5:private-data and tabgco.a_tab=zt5:screen-value no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-zt5:screen-value=tabgco.inti_tab. else lib-zt5=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-zt5:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-zt5:screen-value = intiTab.
            ELSE lib-zt5:screen-value = tabgco.inti_tab.
        END.
        else lib-zt5=Traduction("Inexistant",-2,"").
    end.
    /* Valeurs obligatoires ,pgm � lancer --> Passage sur chapitre obligatoire */
    IF CREATION THEN DO X=1 TO 5:
        if zah[x]:sensitive and ((substr(zah[x]:private-data,1,1)="O" and wa[x]="") or
          (int(substr(zah[x]:private-data,2,4))<>0 and wa[x]<>"")) then val-traite=no.
        if zdh[x]:sensitive and ((substr(zdh[x]:private-data,1,1)="O" and wd[x]=?) or
          (int(substr(zdh[x]:private-data,2,4))<>0 and wd[x]<>?)) then val-traite=no.
        if zlh[x]:sensitive and int(substr(zlh[x]:private-data,2,4))<>0 then val-traite=no.
        if znh[x]:sensitive and ((substr(znh[x]:private-data,1,1)="O" and wn[x]=0) or
          (int(substr(znh[x]:private-data,2,4))<>0 and wn[x]<>0)) then val-traite=no.
        if zth[x]:sensitive and ((substr(zth[x]:private-data,1,1)="O" and wt[x]="") or
          (int(substr(zth[x]:private-data,2,4))<>0 and wt[x]<>"")) then val-traite=no.
    END.
  END.
  if au-moins-une then run val-lib-dep.
END.

/* Chargement des onglets */
RUN ENVOI-CHAPITRE.

DO WITH FRAME fentete:
  if type-cours<>type-saisie or type-saisie="R" then do:
      IF loadsettings("colsai", output hsettingsapi) then do:
          case type-saisie:
            when "A" then ASSIGN rccola  = int(GetSetting("r8",ColBlanc))
                                 rccolb  = int(GetSetting("r8b",ColBlanc)).
            when "D" then ASSIGN rccola  = int(GetSetting("r3",ColBlanc))
                                 rccolb  = int(GetSetting("r3b",ColBlanc)).
            when "C" then ASSIGN rccola  = int(GetSetting("r4",ColBlanc))
                                 rccolb  = int(GetSetting("r4b",ColBlanc)).
            when "F" then ASSIGN rccola  = int(GetSetting("r5",ColBlanc))
                                 rccolb  = int(GetSetting("r5b",ColBlanc)).
            otherwise DO:
                if entetcli.type_fac:SCREEN-VAL IN FRAME fc1="CO" then ASSIGN rccola  = int(GetSetting("r7",ColBlanc))
                                                                              rccolb  = int(GetSetting("r7b",ColBlanc)).
                else ASSIGN rccola  = int(GetSetting("r6",ColBlanc))
                            rccolb  = int(GetSetting("r6b",ColBlanc)).
            END.
          end case.
          UnloadSettings(hSettingsApi).
      END.
      IF rccola <> INT(ColBlanc) OR  rccolb <> int(ColBlanc) THEN DO:
          IF rccola <> 0 AND rccola <> ? THEN CbsetColor(hComBars-fentete,65, rccola).
          IF rccolb <> 0 AND rccolb <> ? THEN CbsetColor(hComBars-fentete,66,rccolb).
      END.
  end.
  if not gst_zone[22] then Logbid = CBEnableItem (Hcombars-fentete, 2, {&bchgadr}, NO).
  if creation then Logbid = CBEnableItem (Hcombars-fentete, 2, {&bsupprimer-p}, NO).
  if creation then Logbid = CBEnableItem (Hcombars-fentete, 2, {&brecalcul}, NO).
  if creation then Logbid = CBEnableItem (hComBars-fentete, 2, {&Btablibre}, NO). /*$A67777*/
END.
session:set-wait-state("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-fc1 wsaicl1
PROCEDURE envoi-fc1 :
DO WITH FRAME FC1:
    /* ent�te : chapitre 1 */
    ASSIGN
        entetcli.dat_liv:LABEL=IF parsoc.FORM_liv=3 THEN Traduction("Date exp�dition",-2,"")
                               ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�part",-2,"")
                               ELSE Traduction("Date livraison",-2,"")
        entetcli.dat_liv:TOOLTIP=Traduction("Date livraison",-2,"") + " " + "(" + Traduction("D�part",-2,"") + ")".

    tmultiech=no.
    if wt[1]<>"" then do:
        find tabgco where tabgco.type_tab=br-xt1:private-data and tabgco.a_tab=wt[1] no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-xt1:screen-value=tabgco.inti_tab. else lib-xt1:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-xt1:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-xt1:screen-value = intiTab.
            ELSE lib-xt1:screen-value = tabgco.inti_tab.
        END.
        else lib-xt1=Traduction("Inexistant",-2,"").
    end.
    ELSE lib-xt1:screen-value="".
    if wt[2]<>"" then do:
        find tabgco where tabgco.type_tab=br-xt2:private-data and tabgco.a_tab=wt[2] no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-xt2:screen-value=tabgco.inti_tab. else lib-xt2:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-xt2:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-xt2:screen-value = intiTab.
            ELSE lib-xt2:screen-value = tabgco.inti_tab.
        END.
        else lib-xt2=Traduction("Inexistant",-2,"").
    end.
    ELSE lib-xt2:screen-value="".
    if wt[3]<>"" then do:
        find tabgco where tabgco.type_tab=br-xt3:private-data and tabgco.a_tab=wt[3] no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-xt3:screen-value=tabgco.inti_tab. else lib-xt3:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-xt3:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-xt3:screen-value = intiTab.
            ELSE lib-xt3:screen-value = tabgco.inti_tab.
        END.
        else lib-xt3=Traduction("Inexistant",-2,"").
    end.
    ELSE lib-xt3:screen-value="".
    if wt[4]<>"" then do:
        find tabgco where tabgco.type_tab=br-xt4:private-data and tabgco.a_tab=wt[4] no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-xt4:screen-value=tabgco.inti_tab. else lib-xt4:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-xt4:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-xt4:screen-value = intiTab.
            ELSE lib-xt4:screen-value = tabgco.inti_tab.
        END.
        else lib-xt4=Traduction("Inexistant",-2,"").
    end.
    ELSE lib-xt4:screen-value="".
    if wt[5]<>"" then do:
        find tabgco where tabgco.type_tab=br-xt5:private-data and tabgco.a_tab=wt[5] no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-xt5:screen-value=tabgco.inti_tab. else lib-xt5:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (br-xt5:private-data, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-xt5:screen-value = intiTab.
            ELSE lib-xt5:screen-value = tabgco.inti_tab.
        END.
        else lib-xt5=Traduction("Inexistant",-2,"").
    end.
    ELSE lib-xt5:screen-value="".

    /* $2059 ... */
    ASSIGN on-gere-les-frais = NO
           frais1-prc1 = 0 frais1-prc2 = 0 frais1-prc3 = 0 frais1-prc4 = 0
           frais2-prc1 = 0 frais2-prc2 = 0 frais2-prc3 = 0 frais2-prc4 = 0
           frais3-prc1 = 0 frais3-prc2 = 0 frais3-prc3 = 0 frais3-prc4 = 0
           frais4-prc1 = 0 frais4-prc2 = 0 frais4-prc3 = 0 frais4-prc4 = 0
           frais5-prc1 = 0 frais5-prc2 = 0 frais5-prc3 = 0 frais5-prc4 = 0.
    RUN mt_cdecl_ha (INT(entetcli.cod_cli:SCREEN-VAL IN FRAME fonglet),type-saisie,INT(entetcli.no_cde:SCREEN-VAL),INT(entetcli.no_bl:SCREEN-VAL),
                     OUTPUT base-1, OUTPUT base-2, OUTPUT base-3, OUTPUT base-4, OUTPUT base-total, /* $2128 */
                     OUTPUT base-1sec, OUTPUT base-2sec, OUTPUT base-3sec, OUTPUT base-4sec, OUTPUT base-totalsec). /* $2128 */
    /* ... $2059 */

    if creation then do:
        RUN envoi-fc1-suite1. /* $pnv_op1 */

        IF porigine = "1" AND INT (cdepot:SCREEN-VAL in frame f1) <> 0 THEN depot-fc1 = cdepot:SCREEN-VAL. /* Si on vient de l'�cran 1 */
        ELSE IF wori_saibp1 THEN
        DO:
            IF INT(STRING(ori_depot)) > 0 THEN depot-fc1 = STRING(ori_depot). /* $a36019 */
        END.
        IF edepot:NUM-ITEMS > 1 AND client.depot > 0 AND LOOKUP(STRING(client.depot), edepot:LIST-ITEM-PAIRS) > 0 THEN edepot:SCREEN-VAL = STRING(client.depot).
        ELSE edepot:SCREEN-VAL = depot-fc1.

        if parsoc.ges_nat and entetcli.nat_cde:screen-value="" then do:
            find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type="NC" no-lock no-error.
            if available enttabdft and enttabdft.dft_poste<>"" then entetcli.nat_cde:screen-value=enttabdft.dft_poste.
        end.

        /* Origine commande */
        ASSIGN
            entetcli.ori_cde:screen-value=""
            lib-oc:screen-value="".
        IF type-plan="C" THEN FIND FIRST tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.statut = "V" NO-LOCK NO-ERROR.
        ELSE IF type-plan="T" THEN DO:
            FIND planav WHERE ROWID(planav)=rowid-planav NO-LOCK NO-ERROR.
            IF AVAIL planav AND planav.appel="E" THEN FIND FIRST tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.statut = "E" NO-LOCK NO-ERROR.
            ELSE FIND FIRST tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.statut = "S" NO-LOCK NO-ERROR.
        END.
        ELSE FIND FIRST tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.statut = type-saisie NO-LOCK NO-ERROR.
        IF NOT AVAIL tabgco THEN DO:
            find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type="OC" no-lock no-error.
            if available enttabdft and enttabdft.dft_poste<>"" then find tabgco where tabgco.type_tab="OC" and tabgco.a_tab=enttabdft.dft_poste no-lock no-error.
        END.
        IF AVAIL tabgco AND tabgco.type_tab="OC" THEN DO:
            assign entetcli.ori_cde:screen-value=tabgco.a_tab. /*lib-oc:screen-value=tabgco.inti_tab.*/
            /*A35891*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("OC", tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-oc:screen-value = intiTab.
            ELSE lib-oc:screen-value = tabgco.inti_tab.
        END.

        /* Canal de vente */
        IF parsoc.ges_can THEN DO:
            IF parsoc.par_can="":U THEN entetcli.canal:SCREEN-VAL=client.canal.
            ELSE DO:
                IF parsoc.par_can="D":U THEN FIND tabgco WHERE tabgco.TYPE_tab = "DS" AND tabgco.n_tab = INT(edepot:SCREEN-VAL) NO-LOCK NO-ERROR.
                ELSE IF parsoc.par_can="O":U THEN FIND tabgco WHERE tabgco.TYPE_tab = "OC" AND tabgco.a_tab = entetcli.ori_cde:SCREEN-VAL NO-LOCK NO-ERROR.
                ELSE FIND tabgco WHERE tabgco.TYPE_tab = "NC" AND tabgco.a_tab = entetcli.nat_cde:SCREEN-VAL NO-LOCK NO-ERROR.
                IF AVAIL tabgco THEN entetcli.canal:SCREEN-VAL=tabgco.canal.
            END.
            IF entetcli.canal:SCREEN-VAL<>"" THEN DO:
                FIND tabgco WHERE tabgco.TYPE_tab = "KV" AND tabgco.a_tab = entetcli.canal:SCREEN-VAL NO-LOCK NO-ERROR.
                IF AVAIL tabgco THEN lib-kv:screen-value=inti_tab.
            END.
            ELSE lib-kv:screen-value="".
        END.

        for each plechcli where plechcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ plechcli.cod_cli = (IF client.CL_PAYE <> 0 THEN client.CL_PAYE
                ELSE IF client.CL_FACT <> 0 THEN client.CL_FACT
                ELSE client.COD_CLI ) NO-LOCK:
            tmultiech=yes.
            create echcli.
            assign echcli.cod_cli  = int(entetcli.cod_cli:SCREEN-VALUE IN FRAME fonglet)
                   echcli.typ_sai  = type-saisie
                   echcli.no_cde   = int(entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet)
                   echcli.no_bl    = int(entetcli.no_bl:SCREEN-VALUE IN FRAME fonglet)
                   echcli.nb_jour  = plechcli.nb_jour
                   echcli.code_reg = plechcli.code_reg
                   echcli.pourc    = plechcli.pourc.
        end.

        IF client.date_px="C" THEN DISP dat-cde$ @ entetcli.dat_px.
        ELSE IF client.date_px="L" THEN DISP dat-liv$ @ entetcli.dat_px.
        ELSE IF parsoc.cod_dev>=99 THEN DISP dat-cde$ @ entetcli.dat_px.
        ELSE DISP dat-liv$ @ entetcli.dat_px.

        DISPLAY
            cptcli.devise @ entetcli.devise cptcli.langue @ entetcli.langue cptcli.civilite @ entetcli.civ_fac
            dat-cde$ @ entetcli.dat_cde dat-liv$ @ entetcli.dat_liv sem$ @ entetcli.semaine
            ref-cde$ @ entetcli.ref_cde not-ref$ @ entetcli.not_ref client.famille @ entetcli.famille
            client.regime @ entetcli.regime client.s2_famille @ entetcli.s2_famille client.s3_famille @ entetcli.s3_famille client.s4_famille @ entetcli.s4_famille
            client.region @ entetcli.region client.app_aff @ entetcli.app_aff
            client.type_fac @ entetcli.type_fac client.tx_ech_d @ entetcli.tx_ech_d
            cptcli.code_reg @ entetcli.code_reg cptcli.code_ech @ entetcli.code_ech
            client.rem_glo[1] @ entetcli.rem_glo[1] client.rem_glo[2] @ entetcli.rem_glo[2] tmultiech
            /*$585*/ client.no_tarif @ entetcli.no_tarif 0 @ entetcli.dur_loc /* $A106276 ... */ "" @ lib-aff. /* ... $A106276 */
        ASSIGN entetcli.deb_loc:SCREEN-VAL = ?
               entetcli.fin_loc:SCREEN-VAL = ?
               entetcli.prix_franco:CHECKED = (parsoc.prix_franco AND client.prix_franco). /*$A35969*/

        IF parsoc.ges_loc /*$A50403 AND type-saisie = "D"*/ AND entetcli.nat_cde:SCREEN-VAL <> "" THEN DO:
            FIND FIRST tabgco WHERE tabgco.TYPE_tab = "NC" AND tabgco.a_tab = entetcli.nat_cde:SCREEN-VAL NO-LOCK NO-ERROR.
            IF AVAIL(tabgco) THEN entetcli.location:CHECKED = tabgco.location.
            IF entetcli.location:CHECKED THEN RUN vc-location.
        END.
        ELSE entetcli.location:CHECKED = NO.

        /* Gestion des autres dates */
        ASSIGN
            wdelai_trs=0
            wno_adr=1.
        IF parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/ THEN DO:
            wno_adr=getsessionInt("Tournee", "Adr",0).
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.affaire = "" and adresse.cod_adr=wno_adr no-lock no-error.
            IF AVAIL adresse THEN DO:
                wdelai_trs = adresse.delai_trs.
                RUN Trt-deltra.
            END.
        END.
        ELSE DO:
            IF parsoc.mult_soc THEN DO:
                find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes and adresse.adr_def=yes AND adresse.ndos=g-ndos no-lock no-error.
                IF NOT AVAIL adresse THEN
                    /* Recherche adresse livraison (non principale) */
                    find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes AND adresse.ndos=g-ndos no-lock no-error.
                    IF NOT AVAIL adresse THEN
                        find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes and adresse.adr_def=yes AND adresse.ndos=0 no-lock no-error.
                        IF NOT AVAIL adresse THEN
                            /* Recherche adresse livraison (non principale) */
                            find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes AND adresse.ndos=0 no-lock no-error.
            END.
            ELSE DO:
                find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes and adresse.adr_def=yes no-lock no-error.
                IF NOT AVAIL adresse THEN
                    /* Recherche adresse livraison (non principale) */
                    find adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if client.cl_livre=0 THEN client.cod_cli ELSE client.cl_livre) and adresse.adr_liv=yes no-lock no-error.
            END.
            if AVAIL adresse then do:
                ASSIGN wdelai_trs = adresse.delai_trs
                       wno_adr    = adresse.cod_adr.

                RUN Trt-deltra.
            END.
        END.
        IF CAN-DO ("D,C", type-cours) THEN DO:
            wdat_livd = dat-liv$.
            IF wdelai_trs>0 THEN
                run trt-calendar (IF AVAIL client THEN client.cal_liv ELSE 0,year(wdat_livd),wdelai_trs,input-output wdat_livd).
        END.
        ELSE wdat_livd = dat-liv$.
        ASSIGN
            wdat_acc = wdat_livd
            wdat_rll = wdat_livd
            wdat_mad = ?
            wdat_rec = ?
            wdat_valid = ?
            whr_rec = 0.
        IF CAN-DO ("D,C", type-cours) AND parsoc.gpao THEN
        DO:
            FIND FIRST depsoc WHERE depsoc.depot = INT (edepot:SCREEN-VAL) NO-LOCK NO-ERROR. /* $1110 */
            dat_mad-ini = dat-liv$.
            ecart_liv_mad = (IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv) - (IF AVAIL depsoc AND depsoc.del_mad <> 0 THEN depsoc.del_mad ELSE pargp.DEL_mad).
            run trt-calendar (0,year(dat_mad-ini),-1 * ecart_liv_mad,input-output dat_mad-ini).
            ASSIGN
                wdat_mad = dat_mad-ini.
        END.
        IF type-cours = "D" AND AVAIL parcde AND parcde.typ_fich = "C" AND parcde.gst_num[6] <> 0 THEN wdat_valid = TODAY + parcde.gst_num[6].

        if can-do("3,4",porigine) AND type-plan="C" THEN DISP interv @ entetcli.commerc[1].
        ELSE DO:
            IF pref_parcde_commerc THEN DO:
                FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT (edepot:SCREEN-VAL) NO-LOCK NO-ERROR.
                IF AVAIL depcli AND depcli.commerc[1] <> "" THEN DISP depcli.commerc[1] @ entetcli.commerc[1].
                ELSE DISP client.commerc[1] @ entetcli.commerc[1].
            END.
            ELSE DISP client.commerc[1] @ entetcli.commerc[1].
        END.

        IF pref_parcde_groupe THEN DO:
            FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT (edepot:SCREEN-VAL) NO-LOCK NO-ERROR.
            IF AVAIL depcli AND depcli.groupe <> "" THEN DISP depcli.groupe @ entetcli.groupe.
            ELSE DISP client.groupe @ entetcli.groupe.
        END.
        ELSE DISP client.groupe @ entetcli.groupe.

        /*$14 & $253*/
        ASSIGN
            notel = ""
            nofax = "".

        /*$A33230...*/
        /*entetcli.affaire:screen-value = if parsoc.aff_cde and ((parcde.gst_zone[29] and type-saisie="D") or type-saisie<>"D") then string(int(entetcli.no_cde:screen-value),"ZZZZZZZZZZ9") else "". */ /*$A33230*/
        IF pvalaffaire = "" OR pvalaffaire = ? THEN entetcli.affaire:screen-value = "". /*$A85716*/
        FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
        if (parsoc.aff_cde and ((parcde.gst_zone[29] and type-saisie="D") or type-saisie<>"D")) OR (AVAIL affpar AND affpar.aff_auto /*$A41246*/ = "C" AND ((parcde.gst_zone[29] and type-saisie="D") or type-saisie<>"D")) THEN DO:
            SetSessionData ("AFF-API":U, "AFFAIRE":U,entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet).
            entetcli.affaire:SCREEN-VALUE = string(int(entetcli.no_cde:screen-value),"ZZZZZZZZZZ9").
        END.
        /*...$A33230*/

        IF parsoc.mult_soc THEN DO:
            /* Recherche adresse facturation principale */
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes and adresse.adr_fdef=yes AND adresse.ndos=g-ndos no-lock no-error.
            IF NOT AVAIL adresse THEN
                /* Recherche adresse facturation (non principale) */
                find adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes AND adresse.ndos=g-ndos no-lock no-error.
                IF NOT AVAIL adresse THEN
                    find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes and adresse.adr_fdef=yes AND adresse.ndos=0 no-lock no-error.
                    IF NOT AVAIL adresse THEN
                    /* Recherche adresse facturation (non principale) */
                    find adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes AND adresse.ndos=0 no-lock no-error.
        END.
        ELSE DO:
            /* Recherche adresse facturation principale */
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes and adresse.adr_fdef=yes no-lock no-error.
            IF NOT AVAIL adresse THEN
                /* Recherche adresse facturation (non principale) */
                find adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.cod_cli and adresse.adr_fac=yes no-lock no-error.
        END.
        if available adresse then do:
            DISPLAY
                adresse.civilite @ entetcli.civ_fac adresse.nom_adr @ entetcli.adr_fac[1] adresse.adresse[1] @ entetcli.adr_fac[2]
                adresse.adresse[2] @ entetcli.adr_fac[3] adresse.adresse4 @ entetcli.adrfac4 adresse.k_post2 @ entetcli.k_post2f adresse.ville @ entetcli.villef
                adresse.pays @ entetcli.paysf.
            ASSIGN
                notel = adresse.num_tel
                nofax = adresse.num_fax.
        END.
        /* Adresse facturation = Adresse client */
        else do:
            DISPLAY
                cptcli.civilite @ entetcli.civ_fac
                client.nom_cli @ entetcli.adr_fac[1]
                client.adresse[1] @ entetcli.adr_fac[2]
                client.adresse[2] @ entetcli.adr_fac[3]
                client.adresse[3] @ entetcli.adrfac4
                client.k_post2 @ entetcli.k_post2f
                client.ville @ entetcli.villef
                client.pays @ entetcli.paysf.
        END.
        IF notel="" THEN notel = cptcli.num_tel.
        IF nofax="" THEN nofax = cptcli.num_fax.

        /* A48827 Client livr� */
        IF client.CL_livre <> 0 AND client.CL_livre<>client.COD_CLI THEN DO:
            FIND clientr WHERE clientr.COD_CLI = client.CL_livre NO-LOCK NO-ERROR.
            IF AVAIL clientr THEN DISP clientr.regime @ entetcli.REGIME.
        END.

        /* Client factur� */
        IF client.CL_FACT <> 0 AND client.CL_FACT<>client.COD_CLI THEN DO:
            /*FIND clientr WHERE clientr.COD_CLI = client.CL_FACT NO-LOCK NO-ERROR. A48827 */
            FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = client.CL_FACT NO-LOCK NO-ERROR.
            /*IF AVAIL clientr THEN DISP clientr.regime @ entetcli.REGIME. A48827 */
            IF AVAIL cptclir THEN DISP
                cptclir.code_reg @ entetcli.CODE_REG cptclir.code_ech @ entetcli.CODE_ECH
                cptclir.CIVILITE @ entetcli.civ_fac cptclir.NOMCLI @ entetcli.ADR_FAC[1]
                cptclir.ADRESSE[1] @ entetcli.ADR_FAC[2] cptclir.ADRESSE[2] @ entetcli.ADR_FAC[3]
                cptclir.ADRESSE4 @ entetcli.ADRFAC4 cptclir.VILLE @ entetcli.VILLEF
                cptclir.PAYS @ entetcli.paysf cptclir.langue @ entetcli.langue
                cptclir.K_POST2 @ entetcli.K_POST2F.
            /* Recherche adresse facturation principale */
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.CL_FACT and adresse.adr_fac=yes and adresse.adr_fdef=yes no-lock no-error.
            IF NOT AVAIL adresse THEN
                /* Recherche adresse facturation (non principale) */
                find adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.CL_FACT and adresse.adr_fac=yes no-lock no-error.
            IF AVAIL adresse THEN DISPLAY
                adresse.civilite @ entetcli.civ_fac adresse.nom_adr @ entetcli.adr_fac[1] adresse.adresse[1] @ entetcli.adr_fac[2]
                adresse.adresse[2] @ entetcli.adr_fac[3] adresse.adresse4 @ entetcli.adrfac4 adresse.k_post2 @ entetcli.k_post2f adresse.ville @ entetcli.villef
                adresse.pays @ entetcli.paysf.
        END.

        /* Client payeur */
        IF client.CL_PAYE <> 0 AND client.CL_PAYE<>client.COD_CLI AND client.CL_PAYE<>client.CL_FACT THEN DO:
            /* FIND clientr WHERE clientr.COD_CLI = client.CL_PAYE NO-LOCK NO-ERROR. A48827 */
            FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = client.CL_PAYE NO-LOCK NO-ERROR.
            /* IF AVAIL clientr THEN DISP clientr.regime @ entetcli.REGIME. A48827 */
            IF AVAIL cptclir THEN DISP
                cptclir.code_reg @ entetcli.CODE_REG cptclir.code_ech @ entetcli.CODE_ECH.
        END.

        /* $1878 ... */
        IF type-saisie = "D" AND wori_ate THEN /* si devis atelier */
        DO:
            FIND FIRST materiel WHERE materiel.typ_fich = ori_parc AND materiel.materiel = ori_mat NO-LOCK NO-ERROR.
            IF AVAIL materiel THEN
            DO:
                IF materiel.cli_div THEN
                DO:
                    IF AVAIL atepar AND atepar.repr_adr <> "P" THEN /* pas de reprise */
                    DO:
                        /* client divers */
                        FIND LAST ateent WHERE ateent.pce_val = NO AND ateent.cod_cli = pnocl AND ateent.typ_sai = type-saisie
                                         AND ateent.typ_fich = materiel.typ_fich AND ateent.materiel = materiel.materiel NO-LOCK NO-ERROR.
                        IF NOT AVAIL ateent THEN
                            FIND LAST ateent WHERE ateent.pce_val = YES AND ateent.cod_cli = pnocl AND ateent.typ_sai = type-saisie
                                             AND ateent.typ_fich = materiel.typ_fich AND ateent.materiel = materiel.materiel NO-LOCK NO-ERROR.

                        IF AVAIL ateent THEN
                        DO:
                            IF atepar.repr_adr = "" OR atepar.repr_adr = "A" THEN ASSIGN logbid = YES. /* reprise automatique */
                            ELSE IF atepar.repr_adr = "D" THEN ASSIGN logbid = NO. /* reprise � la demande */
                            IF logbid = NO THEN MESSAGE Traduction("Voulez-vous reprendre l'adresse du dernier passage ?",-2,"")
                                                    VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
                            IF logbid THEN
                            DO:
                                FIND FIRST entetcli OF ateent NO-LOCK NO-ERROR.
                                IF AVAIL entetcli THEN
                                DO:
                                    DISPLAY
                                        entetcli.k_post2f entetcli.adr_fac[1] entetcli.adr_fac[2] entetcli.adr_fac[3] entetcli.adrfac4
                                        entetcli.villef entetcli.paysf entetcli.famille entetcli.s2_famille entetcli.s3_famille entetcli.s4_famille
                                        entetcli.civ_fac entetcli.region entetcli.groupe.
                                END.
                                /*ELSE
 *                                 DO:
 *                                     FIND FIRST histoent WHERE histoent.typ_mvt = "C" AND histoent.cod_cf = ateent.cod_cli
 *                                                         AND histoent.NO_cde = ateent.NO_cde AND histoent.NO_bl = ateent.NO_bl NO-LOCK NO-ERROR.
 *                                     IF AVAIL histoent THEN
 *                                         DISPLAY histoent.k_post2f @ entetcli.k_post2f histoent.adr_fac[1] @ entetcli.adr_fac[1] histoent.adr_fac[2] @ entetcli.adr_fac[2]
 *                                                 histoent.adr_fac[3] @ entetcli.adr_fac[3] histoent.adrfac4 @ entetcli.adrfac4 histoent.villef @ entetcli.villef
 *                                                 histoent.paysf @ entetcli.paysf histoent.famille @ entetcli.famille histoent.civ_fac @ entetcli.civ_fac
 *                                                 histoent.region @ entetcli.region.
 *                                 END.*/
                            END.
                        END.
                    END.
                END.
            END.
        END.
        /* ... $1878 */
    end.
    else do:

        assign
            cm1=entetcli.coef_mar[1]
            cm2=entetcli.coef_mar[2]
            cm3=entetcli.coef_mar[3]
            cm4=entetcli.coef_mar[4]
            cm5=entetcli.coef_mar[5]
            edepot:SCREEN-VAL=STRING(entetcli.depot)
            wdat_livd = entetcli.dat_livd
            wdat_acc = entetcli.dat_acc
            wdat_rll = entetcli.dat_rll
            wdat_mad = entetcli.dat_mad
            wdelai_trs = entetcli.delai_trs
            wno_adr = entetcli.k_postl
            wdat_rec = entetcli.dat_rec
            wdat_valid = entetcli.dat_vald
            whr_rec = entetcli.hr_rec
            tConsign:CHECKED = (parcde.gst_zone[33] AND entetcli.dep_csg<>0). /*$1130*/

        IF entetcli.det_frais <> ? AND entetcli.det_frais <> "" AND NUM-ENTRIES (entetcli.det_frais,CHR(1)) = 5 THEN DO:
            FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
            IF AVAIL affpar THEN DO:
                RUN envoi-fc1-suite2. /* $pnv_op1 */
            END.
        END.
        /* ... $2059 */

        IF entetcli.dat_mad <> ? THEN
            RUN delai-trt-calendar (entetcli.dat_mad, entetcli.dat_liv, OUTPUT ecart_liv_mad).
        find tabgco where tabgco.type_tab="OC" and tabgco.a_tab=entetcli.ori_cde no-lock no-error.
        if available tabgco THEN DO:
            /*A35891... lib-oc:screen-value=tabgco.inti_tab. else lib-oc:screen-value=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("OC", tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-oc:screen-value = intiTab.
            ELSE lib-oc:screen-value = tabgco.inti_tab.
        END.
        else lib-oc:screen-value=Traduction("Inexistant",-2,"").

    /*$14*/ if entetcli.affaire<>"" then do:
            find affaire where affaire.affaire=entetcli.affaire no-lock no-error.
            if available affaire then lib-aff:screen-value=affaire.int_aff.
                                 else lib-aff:screen-value=Traduction("Inexistant",-2,"").
        end.
        ELSE display "" @ entetcli.affaire "" @ lib-aff.

        /*$A33230...*/
        FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
        if (parsoc.aff_cde and ((parcde.gst_zone[29] and type-saisie="D") or type-saisie<>"D")) OR (AVAIL affpar AND affpar.aff_auto /*$A41246*/ = "C" AND ((parcde.gst_zone[29] and type-saisie="D") or type-saisie<>"D")) THEN DO:
            SetSessionData ("AFF-API":U, "AFFAIRE":U,entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet).
        END.
        /*...$A33230*/


        find first echcli where echcli.cod_cli=entetcli.cod_cli and echcli.typ_sai=entetcli.typ_sai
            and echcli.no_cde=entetcli.no_cde and echcli.no_bl=entetcli.no_bl no-lock no-error.
        if available echcli then tmultiech=yes.

        /* $2215 ... */
        IF entetcli.materiel <> "" THEN DO: /* $A41246 */
            FIND FIRST materiel WHERE materiel.typ_fich = entetcli.typ_fich
                                  AND materiel.materiel = entetcli.materiel
                                  AND materiel.no_ordre = entetcli.no_ordre_m NO-LOCK NO-ERROR.
            IF AVAIL materiel THEN ASSIGN fill-materiel:SCREEN-VAL = materiel.materiel
                                          lib-materiel:SCREEN-VAL = materiel.nom_mat
                                          vTypFich = materiel.typ_fich
                                          vMateriel = materiel.materiel
                                          vNoOrdre = materiel.no_ordre.
        END.
        /* ... $2215 */

        DISPLAY
            entetcli.devise entetcli.paysf entetcli.langue entetcli.civ_fac
            entetcli.dat_cde entetcli.dat_liv entetcli.semaine entetcli.ref_cde entetcli.not_ref entetcli.fac_dvs entetcli.tx_ech_d
            entetcli.dat_px entetcli.groupe entetcli.famille entetcli.s2_famille entetcli.s3_famille entetcli.s4_famille entetcli.region entetcli.commerc[1]
            entetcli.app_aff entetcli.adr_fac[1] entetcli.ori_cde entetcli.nat_cde
            entetcli.k_post2f entetcli.adr_fac[2] entetcli.adr_fac[3] entetcli.adrfac4 entetcli.villef
            entetcli.regime entetcli.type_fac entetcli.code_reg entetcli.code_ech
            entetcli.rem_glo[1] entetcli.rem_glo[2] entetcli.cal_marg tmultiech entetcli.affaire
            /*$585*/ entetcli.no_tarif entetcli.canal /*$A35969...*/ entetcli.prix_franco /*...$A35969*/
            entetcli.location entetcli.deb_loc entetcli.fin_loc entetcli.dur_loc.

        /* $A108407 ... */
        IF entetcli.affaire = "" THEN DO:
            FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
            IF (parsoc.aff_cde AND ((parcde.gst_zone[29] AND type-saisie="D") OR type-saisie<>"D")) OR (AVAIL affpar AND affpar.aff_auto /*$A41246*/ = "C" AND ((parcde.gst_zone[29] AND type-saisie="D") OR type-saisie<>"D")) THEN DO:
                ASSIGN entetcli.affaire:SCREEN-VALUE = STRING(INT(entetcli.no_cde:SCREEN-VALUE),"ZZZZZZZZZZ9")
                       lib-aff:SCREEN-VALUE = "".
            END.
        END.
        /* ... $A108407 */
    end.
    RUN envoi-fc1-suite3. /* $pnv_op1 */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-fc1-suite1 wsaicl1
PROCEDURE envoi-fc1-suite1 :
DO WITH FRAME FC1:
    assign
        entetcli.fac_dvs:screen-value=string(client.fac_dvs,"oui/non")
        entetcli.cal_marg:screen-value=(if type-saisie="D" then parsoc.coeff_pa else "")
        entetcli.nat_cde:screen-value=nat$:screen-value in frame f1
        cm1=parsoc.coef_mar[1]
        cm2=parsoc.coef_mar[2]
        cm3=parsoc.coef_mar[3]
        cm4=parsoc.coef_mar[4]
        cm5=parsoc.coef_mar[5]
        tConsign:CHECKED = parcde.gst_zone[33] AND NOT (type-saisie="F" AND nopt=53). /*$1130*/

    /* $2059 ... Param�tres de marge (frais) */
    FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
    IF AVAIL affpar THEN DO:
        /* frais 1 */
        IF affpar.typ_frais [1] <> "" THEN DO:
            on-gere-les-frais = YES.
            frais1-prc1 = affpar.prc_frais [1].
            frais1-prc2 = affpar.prc_frais [1].
            frais1-prc3 = affpar.prc_frais [1].
            frais1-prc4 = affpar.prc_frais [1].
        END.
        /* frais 2 */
        IF affpar.typ_frais [2] <> "" THEN DO:
            on-gere-les-frais = YES.
            frais2-prc1 = affpar.prc_frais [2].
            frais2-prc2 = affpar.prc_frais [2].
            frais2-prc3 = affpar.prc_frais [2].
            frais2-prc4 = affpar.prc_frais [2].
        END.
        /* frais 3 */
        IF affpar.typ_frais [3] <> "" THEN DO:
            on-gere-les-frais = YES.
            frais3-prc1 = affpar.prc_frais [3].
            frais3-prc2 = affpar.prc_frais [3].
            frais3-prc3 = affpar.prc_frais [3].
            frais3-prc4 = affpar.prc_frais [3].
        END.
        /* frais 4 */
        IF affpar.typ_frais [4] <> "" THEN DO:
            on-gere-les-frais = YES.
            frais4-prc1 = affpar.prc_frais [4].
            frais4-prc2 = affpar.prc_frais [4].
            frais4-prc3 = affpar.prc_frais [4].
            frais4-prc4 = affpar.prc_frais [4].
        END.
        /* frais 5 */
        IF affpar.typ_frais [5] <> "" THEN DO:
            on-gere-les-frais = YES.
            frais5-prc1 = affpar.prc_frais [5].
            frais5-prc2 = affpar.prc_frais [5].
            frais5-prc3 = affpar.prc_frais [5].
            frais5-prc4 = affpar.prc_frais [5].
        END.
    END.
    /* ... $2059 */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-fc1-suite2 wsaicl1
PROCEDURE envoi-fc1-suite2 :
/* frais 1 */
IF affpar.typ_frais [1] <> "" THEN DO:
    ASSIGN on-gere-les-frais = YES
           frais1-prc1 = DEC (ENTRY (1,ENTRY (1,entetcli.det_frais,CHR(1)),CHR(2)))
           frais1-prc2 = DEC (ENTRY (2,ENTRY (1,entetcli.det_frais,CHR(1)),CHR(2)))
           frais1-prc3 = DEC (ENTRY (3,ENTRY (1,entetcli.det_frais,CHR(1)),CHR(2)))
           frais1-prc4 = DEC (ENTRY (4,ENTRY (1,entetcli.det_frais,CHR(1)),CHR(2))).
END.
/* frais 2 */
IF affpar.typ_frais [2] <> "" THEN DO:
    ASSIGN on-gere-les-frais = YES
           frais2-prc1 = DEC (ENTRY (1,ENTRY (2,entetcli.det_frais,CHR(1)),CHR(2)))
           frais2-prc2 = DEC (ENTRY (2,ENTRY (2,entetcli.det_frais,CHR(1)),CHR(2)))
           frais2-prc3 = DEC (ENTRY (3,ENTRY (2,entetcli.det_frais,CHR(1)),CHR(2)))
           frais2-prc4 = DEC (ENTRY (4,ENTRY (2,entetcli.det_frais,CHR(1)),CHR(2))).
END.
/* frais 3 */
IF affpar.typ_frais [3] <> "" THEN DO:
    ASSIGN on-gere-les-frais = YES
           frais3-prc1 = DEC (ENTRY (1,ENTRY (3,entetcli.det_frais,CHR(1)),CHR(2)))
           frais3-prc2 = DEC (ENTRY (2,ENTRY (3,entetcli.det_frais,CHR(1)),CHR(2)))
           frais3-prc3 = DEC (ENTRY (3,ENTRY (3,entetcli.det_frais,CHR(1)),CHR(2)))
           frais3-prc4 = DEC (ENTRY (4,ENTRY (3,entetcli.det_frais,CHR(1)),CHR(2))).
END.
/* frais 4 */
IF affpar.typ_frais [4] <> "" THEN DO:
    ASSIGN on-gere-les-frais = YES
           frais4-prc1 = DEC (ENTRY (1,ENTRY (4,entetcli.det_frais,CHR(1)),CHR(2)))
           frais4-prc2 = DEC (ENTRY (2,ENTRY (4,entetcli.det_frais,CHR(1)),CHR(2)))
           frais4-prc3 = DEC (ENTRY (3,ENTRY (4,entetcli.det_frais,CHR(1)),CHR(2)))
           frais4-prc4 = DEC (ENTRY (4,ENTRY (4,entetcli.det_frais,CHR(1)),CHR(2))).
END.
/* frais 5 */
IF affpar.typ_frais [5] <> "" THEN DO:
    ASSIGN on-gere-les-frais = YES
           frais5-prc1 = DEC (ENTRY (1,ENTRY (5,entetcli.det_frais,CHR(1)),CHR(2)))
           frais5-prc2 = DEC (ENTRY (2,ENTRY (5,entetcli.det_frais,CHR(1)),CHR(2)))
           frais5-prc3 = DEC (ENTRY (3,ENTRY (5,entetcli.det_frais,CHR(1)),CHR(2)))
           frais5-prc4 = DEC (ENTRY (4,ENTRY (5,entetcli.det_frais,CHR(1)),CHR(2))).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-fc1-suite3 wsaicl1
PROCEDURE envoi-fc1-suite3 :
DO WITH FRAME fc1:
    find tabcomp where tabcomp.type_tab="CI" and tabcomp.a_tab=entetcli.civ_cde:screen-value no-lock no-error.
    if available TABCOMP then DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-cde-ci = intiTab.
        ELSE lib-cde-ci = TABCOMP.inti_tab.
    END.
    else lib-cde-ci = Traduction("Inexistant",-2,"").
    find tabcomp where tabcomp.type_tab="PA" and tabcomp.a_tab=entetcli.paysc:screen-value no-lock no-error.
    if available tabcomp THEN DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-cde-pa = intiTab.
        ELSE lib-cde-pa = TABCOMP.inti_tab.
    END.
    else lib-cde-pa=Traduction("Inexistant",-2,"").

    find tabcomp where tabcomp.type_tab="CI" and tabcomp.a_tab=entetcli.civ_fac:screen-value no-lock no-error.
    /*A35891... if available tabcomp then lib-ci=tabcomp.inti_tab. else lib-ci=Traduction("Inexistant",-2,"").*/
    if available TABCOMP then DO:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ci = intiTab.
        ELSE lib-ci = TABCOMP.inti_tab.
    END.
    else lib-ci = Traduction("Inexistant",-2,""). /*...A35891*/
    find tabcomp where tabcomp.type_tab="PA" and tabcomp.a_tab=entetcli.paysf:screen-value no-lock no-error.
    if available tabcomp THEN DO:
        /*A35891... lib-pa=tabcomp.inti_tab. else lib-pa=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-pa = intiTab.
        ELSE lib-pa = TABCOMP.inti_tab.
    END.
    else lib-pa=Traduction("Inexistant",-2,"").

    find tabcomp where tabcomp.type_tab="LG" and tabcomp.a_tab=entetcli.langue:screen-value no-lock no-error.
    if available tabcomp THEN DO:
        /*A35891... lib-lg=tabcomp.inti_tab. else lib-lg=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("LG", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN lib-lg = intiTab.
        ELSE lib-lg = TABCOMP.inti_tab.
    END.
    else lib-lg=Traduction("Inexistant",-2,""). /*...A35891*/

    find tabgco where tabgco.type_tab="GC":U and tabgco.a_tab=entetcli.groupe:screen-value no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("GC":U, tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-gc = intiTab.
        ELSE lib-gc = tabgco.inti_tab.
    END.
    else lib-gc=Traduction("Inexistant",-2,""). /*...A35891*/

    find tabgco where tabgco.type_tab="FC" and tabgco.a_tab=entetcli.famille:screen-value no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("FC", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-fc = intiTab.
        ELSE lib-fc = tabgco.inti_tab.
    END.
    else lib-fc=Traduction("Inexistant",-2,""). /*...A35891*/
    find tabgco where tabgco.type_tab="SC" and tabgco.a_tab=STRING(entetcli.famille:screen-value,"X(3)") + entetcli.s2_famille:screen-value no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SC", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-sc = intiTab.
        ELSE lib-sc = tabgco.inti_tab.
    END.
    else lib-sc=Traduction("Inexistant",-2,"").
    find tabgco where tabgco.type_tab="SD" and tabgco.a_tab=STRING(entetcli.famille:SCREEN-VALUE,"X(3)") + STRING(entetcli.s2_famille:SCREEN-VALUE,"X(3)") + entetcli.s3_famille:screen-value no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SD", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-sd = intiTab.
        ELSE lib-sd = tabgco.inti_tab.
    END.
    else lib-sd=Traduction("Inexistant",-2,"").
    find tabgco where tabgco.type_tab="SE" and tabgco.a_tab=STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:SCREEN-VALUE,"X(3)") + STRING(entetcli.s3_famille:SCREEN-VALUE,"X(3)") + entetcli.s4_famille:screen-value no-lock no-error.
    if available tabgco then do:
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SE", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-se = intiTab.
        ELSE lib-se = tabgco.inti_tab.
    END.
    else lib-se=Traduction("Inexistant",-2,"").

    find tabgco where tabgco.type_tab="RG" and tabgco.n_tab=int(entetcli.region:screen-value) no-lock no-error.
    if available tabgco then do:
        /*A35891... lib-rg=tabgco.inti_tab. else lib-rg=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("RG", STRING(tabgco.n_tab), g-langue).
        IF intiTab <> "" THEN lib-rg = intiTab.
        ELSE lib-rg = tabgco.inti_tab.
    END.
    else lib-rg=Traduction("Inexistant",-2,""). /*...A35891*/

    /*$2104...find tabgco where tabgco.type_tab="MG" AND tabgco.fct_aa=YES and tabgco.a_tab=entetcli.app_aff:screen-value no-lock no-error.*/
    buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.app_aff:screen-value) + " AND TPPERSONNE.FCT_AA = YES ").
    if buf-id <> ? then lib-aa = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0). else lib-aa=Traduction("Inexistant",-2,"").
    /*find tabgco where tabgco.type_tab="MG" AND tabgco.fct_com=YES and tabgco.a_tab=entetcli.commerc[1]:screen-value no-lock no-error.*/
    buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[1]:screen-value) + " AND TPPERSONNE.FCT_COM = YES ").
    if buf-id <> ? then lib-rp1 = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0). else lib-rp1=Traduction("Inexistant",-2,"").  /*...$2104*/
    find tabgco where tabgco.type_tab="RV" and tabgco.n_tab=int(entetcli.regime:screen-value) no-lock no-error.
    if available tabgco then do:
        /*A35891... llib-rv=tabgco.inti_tab. else lib-rv=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("RV", STRING(tabgco.n_tab), g-langue).
        IF intiTab <> "" THEN lib-rv = intiTab. /*...A35891*/
        ELSE lib-rv = tabgco.inti_tab.
    END.
    else lib-rv=Traduction("Inexistant",-2,"").

    find tabgco where tabgco.type_tab="TF" and tabgco.a_tab=entetcli.type_fac:screen-value no-lock no-error.
    if available tabgco then do:
        /*A35891... lib-tf=tabgco.inti_tab. else lib-tf=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("TF", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-tf = intiTab. /*...A35891*/
        ELSE lib-tf = tabgco.inti_tab.
    END.
    else lib-tf=Traduction("Inexistant",-2,"").
    find modereg where modereg.ngr1=g-modereg-ngr1 AND modereg.ngr2=g-modereg-ngr2 AND modereg.ndos=g-modereg-ndos AND codet_reg and modereg.code_reg=int(entetcli.code_reg:screen-value) no-lock no-error.
    if available modereg then lib-reg=inti_reg. else lib-reg=Traduction("Inexistant",-2,"").
    find condech where condech.ngr1=g-condech-ngr1 AND condech.ngr2=g-condech-ngr2 AND condech.ndos=g-condech-ndos AND condech.code_ech=int(entetcli.code_ech:screen-value) no-lock no-error.
    if available condech then lib-ech=inti_ech. else lib-ech=Traduction("Inexistant",-2,"").
    if parsoc.ges_nat then do:
        find tabgco where tabgco.type_tab="NC" and tabgco.a_tab=entetcli.nat_cde:screen-value no-lock no-error.
        if available tabgco then do:
            /*A35891... lib-nc=tabgco.inti_tab. else lib-nc=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("NC", tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-nc = intiTab. /*...A35891*/
            ELSE lib-nc = tabgco.inti_tab.
        END.
        else lib-nc=Traduction("Inexistant",-2,"").
    end.
    if parsoc.ges_can then do:
        find tabgco where tabgco.type_tab="KV" and tabgco.a_tab=entetcli.canal:screen-value no-lock no-error.
        if available tabgco then do:
            /*A35891... lib-kv=tabgco.inti_tab. else lib-kv=Traduction("Inexistant",-2,"").*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("KV", tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-kv = intiTab. /*...A35891*/
            ELSE lib-kv = tabgco.inti_tab.
        END.
        else lib-kv=Traduction("Inexistant",-2,"").
    end.
    display
        lib-ci lib-pa lib-lg lib-fc lib-gc lib-sc lib-sd lib-se lib-aa lib-rp1 lib-rg lib-rv lib-tf lib-reg lib-ech lib-nc lib-kv
        Ft$selection-c1 Ft$selection-c2 Fhie1 Fhie2 Fhie3 lib-cde-ci lib-cde-pa.

    ENABLE
        entetcli.dat_cde entetcli.dat_liv entetcli.semaine bdate entetcli.dat_px entetcli.ref_cde entetcli.not_ref
        edepot entetcli.ori_cde br-oc entetcli.nat_cde br-nc entetcli.canal br-kv tConsign /*$1130*/ entetcli.commerc[1] br-rp1
        entetcli.app_aff br-aa entetcli.affaire br-aff bcreer-aff bmodif-aff bt$adresse-aff fill-materiel btn-materiel
        entetcli.no_tarif br-tar entetcli.fac_dvs entetcli.devise br-de bappdev entetcli.tx_ech_d /*$585*/ entetcli.prix_franco /*$A35969*/
        entetcli.location entetcli.deb_loc entetcli.fin_loc entetcli.dur_loc
        entetcli.cal_marg bpardev entetcli.rem_glo[1] entetcli.rem_glo[2] entetcli.code_reg br-reg entetcli.code_ech br-ech tmultiech bplanech entetcli.regime br-rv entetcli.type_fac br-tf
        radio-adresse entetcli.civ_fac br-ci entetcli.adr_fac[1] entetcli.adr_fac[2] entetcli.adr_fac[3] entetcli.adrfac4
        entetcli.k_post2f br-kp entetcli.villef entetcli.paysf br-pa entetcli.region br-rg entetcli.langue br-lg
        entetcli.groupe br-gc entetcli.famille br-fc entetcli.s2_famille br-sc entetcli.s3_famille br-sd entetcli.s4_famille br-se.

    APPLY "VALUE-CHANGED" TO radio-adresse.

    IF NOT creation AND can-do("F,R",entetcli.typ_sai) AND ((parsoc.vfm_deb <> ? AND entetcli.dat_liv < parsoc.vfm_deb) OR (parsoc.vfm_fin <> ? AND entetcli.dat_liv > parsoc.vfm_fin)) THEN entetcli.dat_liv:SENSITIVE = NO. /*$A26501*/
    IF NOT creation AND parsoc.prix_franco AND entetcli.prix_franco THEN entetcli.prix_franco:SENSITIVE = NO. /*$A35969*/

    CodePostal = entetcli.k_post2f:SCREEN-VAL.
    find tabcomp where tabcomp.type_tab="DE" and tabcomp.a_tab=entetcli.devise:screen-value no-lock no-error.
    if input entetcli.fac_dvs and available tabcomp and tabcomp.a_tab<>g-dftdev and (tabcomp.dev_in=? or date(entetcli.dat_px:screen-value)<tabcomp.dev_in) then do:
        if input entetcli.tx_ech_d=0 then run rch-tx-devise-vente.
    end.
    else entetcli.tx_ech_d:hidden=yes.
    if tmultiech:screen-value="no" then hide bplanech.
    IF edepot:NUM-ITEMS=1 THEN edepot:HIDDEN=YES.
    ELSE if not creation AND (NOT CAN-DO("C,D",entetcli.typ_sai) OR (AVAIL pargp AND pargp.prealable AND entetcli.typ_sai = "C" /* bloqu� uniquement en statut commande */ )) then do:
        find first lignecli of entetcli where lignecli.sous_type="AR" no-lock no-error.
        if available lignecli /*$A50782...*/ AND entetcli.af_fin = "" /*...$A50782*/ THEN disable edepot.
    end.
    /*$1130...*/
    IF parcde.gst_zone[33] AND NOT (type-saisie="F" AND (nopt=53 OR nopt=615 OR nopt=556) /*A133073*/ ) THEN DO:
        FIND FIRST tabgco WHERE (IF parsoc.mult_soc THEN tabgco.ndos = g-ndos ELSE TRUE) AND tabgco.TYPE_tab = "DS"
                            AND tabgco.externe AND tabgco.cli_cai = INT(entetcli.cod_cli:SCREEN-VAL IN FRAME Fonglet) AND tabgco.type_tf[1] = 'C' NO-LOCK NO-ERROR.
        IF AVAIL tabgco THEN depotcsg=tabgco.n_tab.
        ELSE ASSIGN tConsign:CHECKED = NO tConsign:SENSITIVE = NO.
    END.
    ELSE ASSIGN tConsign:CHECKED = NO tConsign:HIDDEN = YES.
    IF NOT creation AND entetcli.no_fact<>0 THEN ASSIGN tConsign:SENSITIVE = NO.
    /*...$1130*/
    /* Param�tres gestion facturation */
    IF parcde.gst_zone[84] THEN ASSIGN
        entetcli.dat_liv:SENSITIVE = NO
        entetcli.semaine:SENSITIVE = NO.
    if not parsoc.ges_nat then hide entetcli.nat_cde br-nc lib-nc.
    if not parsoc.ges_can then hide entetcli.canal br-kv lib-kv.
    if not parsoc.sem_liv then hide entetcli.semaine.
    if not parsoc.ges_ref then hide entetcli.ref_cde.
    if not parsoc.ges_not then hide entetcli.not_ref.
    if not parsoc.affaire then hide entetcli.affaire br-aff lib-aff bcreer-aff bmodif-aff bt$adresse-aff.
    else do:
    /*$14*/
        if not parcde.gst_zone[28] or (not parcde.gst_zone[29] and type-saisie="D") then hide entetcli.affaire br-aff lib-aff bcreer-aff bmodif-aff bt$adresse-aff.
        else if entetcli.affaire:screen-value<>"" then do:
            find affaire where affaire.affaire=entetcli.affaire:screen-value no-lock no-error.
            if available affaire then bcreer-aff:sensitive = no.
                                 else bcreer-aff:sensitive = yes.
        end.
    /*fin $14*/
    end.
    if not droits-modif-aff then hide bcreer-aff bmodif-aff.  /*$A32485*/
    IF /*type-saisie <> "D" OR $A50403*/ NOT parsoc.ges_loc THEN HIDE entetcli.location.

    /* $2215 ... */
    IF AVAIL parsoc THEN
    DO:
        IF parsoc.ges_mat AND wori_ate = NO /* $a36019 */ THEN
        DO:
            IF lstparc = "" THEN
            DO:
                DO X = 1 TO 9:
                    IF parsoc.nom_mat[X] <> "" THEN lstparc = lstparc + STRING(X) + ",".
                END.
                lstparc = RIGHT-TRIM(lstparc, ",").
            END.
            lib-materiel:HIDDEN = NO.
        END.
        ELSE ASSIGN fill-materiel:HIDDEN = YES
                    btn-materiel:HIDDEN = YES
                    lib-materiel:HIDDEN = YES.
    END.
    /* ... $2215 */

    if not gst_zone[12] then hide entetcli.commerc[1] br-rp1 lib-rp1.
    if not gst_zone[13] then hide entetcli.app_aff br-aa lib-aa.
    if not gst_zone[14] then hide entetcli.fac_dvs entetcli.devise br-de entetcli.tx_ech_d bappdev.
    else if entetcli.fac_dvs:screen-value="non" then hide entetcli.devise br-de entetcli.tx_ech_d bappdev.
    if not gst_zone[15] then hide entetcli.code_reg br-reg lib-reg.
    if not gst_zone[16] then hide entetcli.code_ech br-ech lib-ech.
    if not gst_zone[15] or not gst_zone[16] then hide tmultiech bplanech.
    if not gst_zone[17] then hide entetcli.type_fac br-tf lib-tf.
    if not gst_zone[18] then hide entetcli.regime br-rv lib-rv.
    if not gst_zone[19] then hide entetcli.groupe br-gc lib-gc.
    if not gst_zone[20] then hide entetcli.ori_cde br-oc lib-oc.
    if not gst_zone[22] then hide entetcli.civ_fac br-ci lib-ci entetcli.paysf br-pa lib-pa entetcli.adr_fac[1] entetcli.adr_fac[2] entetcli.adr_fac[3] entetcli.adrfac4 entetcli.k_post2f entetcli.villef br-kp.
    if not gst_zone[23] then hide entetcli.region br-rg lib-rg.
    if not gst_zone[24] then hide entetcli.famille br-fc lib-fc entetcli.s2_famille br-sc lib-sc entetcli.s3_famille br-sd lib-sd entetcli.s4_famille br-se lib-se Fhie1 Fhie2 Fhie3.
    if not gst_zone[25] then hide entetcli.langue br-lg lib-lg.
    if not gst_zone[26] then hide entetcli.rem_glo[1] entetcli.rem_glo[2].
    if not gst_zone[27] then hide entetcli.cal_marg.
    if entetcli.cal_marg:screen-value="" then hide bpardev.
    ELSE hide bappdev.
    /*$585*/ if not parsoc.tar_col or not gst_zone[61] then hide entetcli.no_tarif br-tar.
    IF NOT parsoc.prix_franco THEN HIDE entetcli.prix_franco. /*$A35969*/

    ASSIGN entetcli.deb_loc:HIDDEN = /*entetcli.typ_sai <> "d" OR*/ NOT entetcli.location:CHECKED
           entetcli.fin_loc:HIDDEN = /*entetcli.typ_sai <> "d" OR*/ NOT entetcli.location:CHECKED
           entetcli.dur_loc:HIDDEN = /*entetcli.typ_sai <> "d" OR*/ NOT entetcli.location:CHECKED.

    /* Position curseur */
    run posicurs1.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-fc2 wsaicl1
PROCEDURE envoi-fc2 :
if parcde.gst_zone[1] then DO WITH FRAME FC2:
    ASSIGN
        fill-lancement:HIDDEN = TRUE
        btn-lancement:HIDDEN = TRUE.
    if creation then do:
        assign
            vcl_fact = STRING(client.cl_fact)
            vcl_paye = STRING(client.cl_paye)
            vcl_livre = string(client.cl_livre) /* A48827 */
            entetcli.borne:screen-value=string(client.borne)
            entetcli.fac_ttc:screen-value=string(client.fac_ttc,"oui/non")
            entetcli.fac_pxa:screen-value=string(client.fac_pxa,"oui/non")
            entetcli.proforma:screen-value=string(client.proforma,"oui/non")
            entetcli.fra_app:screen-value=string(client.fra_app,"oui/non")
            entetcli.chif_bl:screen-value=string(client.chif_bl,"oui/non")
            entetcli.edt_arc:screen-value=string(client.edt_arc,"oui/non")
            entetcli.liv_cpl:screen-value=string(client.liv_cpl,"yes/no")
            entetcli.cde_cpl:screen-value=string(client.cde_cpl,"yes/no")
            entetcli.prep_isol:screen-value=string(client.prep_isol,"yes/no")
            entetcli.tar_fil[1]:screen-value=string(client.tar_fil[1],"yes/no")
            entetcli.tar_fil[2]:screen-value=string(client.tar_fil[2],"yes/no")
            entetcli.tar_fil[3]:screen-value=string(client.tar_fil[3],"yes/no")
            entetcli.tar_fil[4]:screen-value=string(client.tar_fil[4],"yes/no")
            entetcli.tar_fil[5]:screen-value=string(client.tar_fil[5],"yes/no")
            entetcli.tar_fil[6]:screen-value=string(client.tar_fil[6],"yes/no")
            entetcli.tar_cum[1]:screen-value=string(client.tar_cum[1],"yes/no")
            entetcli.tar_cum[2]:screen-value=string(client.tar_cum[2],"yes/no")
            entetcli.tar_cum[3]:screen-value=string(client.tar_cum[3],"yes/no")
            entetcli.tar_cum[4]:screen-value=string(client.tar_cum[4],"yes/no")
            entetcli.tar_cum[5]:screen-value=string(client.tar_cum[5],"yes/no")
            entetcli.tar_cum[6]:screen-value=string(client.tar_cum[6],"yes/no")
            entetcli.cat_cum:screen-value=string(client.cat_cum,"yes/no")
            entetcli.dat_ech:SCREEN-VAL=""
            entetcli.tva_date:SCREEN-VAL=""
            entetcli.bl_regr:screen-value=string(client.bl_regr,"yes/no")
            entetcli.plus_bpbl:screen-value=string(client.plus_bpbl,"yes/no")
            .
        IF pref_parcde_commerc THEN DO:
            FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT(edepot:SCREEN-VAL IN FRAME fc1) NO-LOCK NO-ERROR.
            IF AVAIL depcli AND depcli.commerc[2] <> "" THEN DISP depcli.commerc[2] @ entetcli.commerc[2].
            ELSE DISP client.commerc[2] @ entetcli.commerc[2].
        END.
        ELSE DISP client.commerc[2] @ entetcli.commerc[2].
        display
            cptcli.taux_esc @ entetcli.taux_esc client.liasse @ entetcli.liasse /* $A59971 ... */ client.lias_bl @ entetcli.lias_bl /* ... $A59971 */
            client.cat_tar @ entetcli.cat_tar notel @ entetcli.num_tel nofax @ entetcli.num_fax cptcli.cpt_bq @ entetcli.cpt_bq
            string(client.cl_stat) @ vcl_stat client.maj_ach @ entetcli.maj_ach
            vcl_livre string(client.cl_grp) @ vcl_grp
            vcl_paye vcl_fact
            string(client.cl_plnat) @ vcl_pln string(client.cl_plreg) @ vcl_plr.

        IF parsoc.mult_soc THEN DO:
            FIND parvs WHERE parvs.ndos = g-ndos NO-LOCK NO-ERROR.
            IF parvs.int_parc <> "" THEN DISPLAY string(client.c_factor) @ vc_factor. /*$A39262*/
        END.
        ELSE DO:
            IF parsoc.int_parc <> "" THEN DISPLAY string(client.c_factor) @ vc_factor. /*$A39262*/
        END.

        IF lgtextile THEN DO:
            RUN SAISON-NATURE.
            ASSIGN entetcli.grp_ps:SCREEN-VAL = client.grp_ps
                   entetcli.gencod_det:SCREEN-VAL = STRING(client.gencod_det).
        END.
        /* Client factur� */
        IF client.CL_FACT <> 0 AND client.CL_FACT<>client.COD_CLI THEN DO:
            FIND clientr WHERE clientr.COD_CLI = client.CL_FACT NO-LOCK NO-ERROR.
            FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = client.CL_FACT NO-LOCK NO-ERROR.
            IF AVAIL clientr THEN DISP clientr.liasse @ entetcli.liasse.
            IF AVAIL cptclir THEN DISP
                cptclir.num_tel @ entetcli.num_tel
                cptclir.num_fax @ entetcli.num_fax
                cptclir.cpt_bq @ entetcli.CPT_BQ.
            /* Recherche adresse facturation principale */
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.CL_FACT and adresse.adr_fac=yes and adresse.adr_fdef=yes no-lock no-error.
            IF NOT AVAIL adresse THEN
                /* Recherche adresse facturation (non principale) */
                find adresse where adresse.typ_fich="C" and adresse.cod_tiers=client.CL_FACT and adresse.adr_fac=yes no-lock no-error.
            IF AVAIL adresse THEN DISPLAY adresse.num_tel @ entetcli.num_tel
                                          adresse.num_fax @ entetcli.num_fax.
        END.

        /* Client payeur */
        IF client.CL_PAYE <> 0 AND client.CL_PAYE<>client.COD_CLI AND client.CL_PAYE<>client.CL_FACT THEN DO:
            FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = client.CL_PAYE NO-LOCK NO-ERROR.
            IF AVAIL cptclir THEN DISP cptclir.cpt_bq @ entetcli.CPT_BQ.
        END.
    end.
    else do:
        ASSIGN
            vcl_paye = string(entetcli.cl_paye)
            vcl_fact = string(entetcli.cl_fact)
            vcl_livre = string(entetcli.cl_livre). /* A48827 */
        display
            entetcli.saison entetcli.grp_ps entetcli.gencod_det entetcli.num_tel entetcli.num_fax entetcli.taux_esc entetcli.liasse
            /* $A59971 ... */ entetcli.lias_bl /* ... $A59971 */
            entetcli.cat_tar entetcli.borne entetcli.fac_ttc entetcli.fac_pxa entetcli.commerc[2] entetcli.cpt_bq
            entetcli.proforma entetcli.fra_app entetcli.chif_bl entetcli.edt_arc entetcli.liv_cpl entetcli.prep_isol
            string(entetcli.cl_stat) @ vcl_stat entetcli.dat_ech entetcli.maj_ach entetcli.plus_bpbl entetcli.bl_regr
            vcl_livre string(entetcli.cl_grp) @ vcl_grp entetcli.cde_cpl
            string(entetcli.cl_plnat) @ vcl_pln string(entetcli.cl_plreg) @ vcl_plr
            vcl_paye vcl_fact entetcli.tar_fil[1] entetcli.tar_fil[2] entetcli.tar_fil[3] entetcli.tar_fil[4] entetcli.tar_fil[5]
            entetcli.tar_fil[6] entetcli.tar_cum[1] entetcli.tar_cum[2] entetcli.tar_cum[3] entetcli.tar_cum[4] entetcli.tar_cum[5]
            entetcli.tar_cum[6] entetcli.cat_cum
            entetcli.tva_date.

        IF parsoc.mult_soc THEN DO:
            FIND parvs WHERE parvs.ndos = g-ndos NO-LOCK NO-ERROR.
            IF parvs.int_parc <> "" THEN DISPLAY string(entetcli.c_factor) @ vc_factor. /*$A39262*/
        END.
        ELSE DO:
            IF parsoc.int_parc <> "" THEN DISPLAY string(entetcli.c_factor) @ vc_factor. /*$A39262*/
        END.
    end.
    IF lgTextile THEN DO:
        FIND FIRST tabgco WHERE tabgco.TYPE_tab = "ss" AND tabgco.a_tab = INPUT entetcli.saison NO-LOCK NO-ERROR.
        if available tabgco then do:
            /*A35891... lib-ss:SCREEN-VALUE = (IF AVAILABLE tabgco THEN tabgco.inti_tab ELSE Traduction("Inexistante",-2,"")).*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SS", tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-ss = intiTab. /*...A35891*/
            ELSE lib-ss = tabgco.inti_tab.
        END.
        else lib-ss=Traduction("Inexistante",-2,"").
        FIND FIRST tabgco WHERE tabgco.TYPE_tab = "gp" AND tabgco.a_tab = INPUT entetcli.grp_ps NO-LOCK NO-ERROR.
        if available tabgco then do:
            /*A35891... lib-gpres:SCREEN-VALUE = (IF AVAILABLE tabgco THEN tabgco.inti_tab ELSE Traduction("Inexistant",-2,"")).*/
            IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("gp":U, tabgco.a_tab, g-langue).
            IF intiTab <> "" THEN lib-gpres = intiTab. /*...A35891*/
            ELSE lib-gpres = tabgco.inti_tab.
        END.
        else lib-gpres=Traduction("Inexistant",-2,"").
    END.

    find tabgco where tabgco.type_tab="CT" and tabgco.a_tab=entetcli.cat_tar:screen-value no-lock no-error.
    if available tabgco then do:
        /*A35891... lib-ct=tabgco.inti_tab. else lib-ct=Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CT", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ct = intiTab. /*...A35891*/
        ELSE lib-ct = tabgco.inti_tab.
    END.
    else lib-ct=Traduction("Inexistant",-2,"").

    /*$2104...find tabgco where tabgco.type_tab="MG" AND tabgco.fct_com=YES and tabgco.a_tab=entetcli.commerc[2]:screen-value no-lock no-error.*/
    buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[2]:screen-value) + " AND TPPERSONNE.FCT_COM = YES ").
    IF  buf-id <> ? THEN ASSIGN lib-rp2 = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0).  else lib-rp2=Traduction("Inexistant",-2,""). /*...$2104*/
    FIND PLANCPT where plancpt.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ PLANCPT.cpt = int(entetcli.cpt_bq:SCREEN-VAL) no-lock no-error.
    IF available PLANCPT THEN lib-cchq = PLANCPT.cpt_INTi. ELSE lib-cchq=Traduction("Inexistant",-2,"").
    assign lib-stat=Traduction("Inexistant",-2,"") lib-livre=Traduction("Inexistant",-2,"") lib-grp=Traduction("Inexistant",-2,"") lib-paye=Traduction("Inexistant",-2,"") lib-fact=Traduction("Inexistant",-2,"") lib-pln=Traduction("Inexistant",-2,"") lib-plr=Traduction("Inexistant",-2,"").
    if int(vcl_stat:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_stat:screen-value) no-lock no-error.
        if available clientr then lib-stat=clientr.nom_cli.
    end.
    if int(vcl_livre:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_livre:screen-value) no-lock no-error.
        if available clientr then lib-livre=clientr.nom_cli.
    end.
    if int(vcl_grp:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_grp:screen-value) no-lock no-error.
        if available clientr then lib-grp=clientr.nom_cli.
    end.
    if int(vcl_paye:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_paye:screen-value) no-lock no-error.
        if available clientr then lib-paye=clientr.nom_cli.
    end.
    if int(vcl_fact:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_fact:screen-value) no-lock no-error.
        if available clientr then lib-fact=clientr.nom_cli.
    end.
    if int(vcl_pln:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_pln:screen-value) no-lock no-error.
        if available clientr then lib-pln=clientr.nom_cli.
    end.
    if int(vcl_plr:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vcl_plr:screen-value) no-lock no-error.
        if available clientr then lib-plr=clientr.nom_cli.
    end.
    if int(vc_factor:screen-value)<>0 then do:
        find clientr where clientr.cod_cli=int(vc_factor:screen-value) no-lock no-error.
        if available clientr then lib-factor=clientr.nom_cli.
    end.
    display
        lib-ct lib-stat lib-livre lib-grp lib-paye lib-fact lib-pln lib-plr lib-rp2 lib-cchq
        Ft$selection-c3 Ft$selection-c4.

    IF parsoc.mult_soc THEN DO:
        FIND parvs WHERE parvs.ndos = g-ndos NO-LOCK NO-ERROR.
        IF parvs.int_parc <> "" THEN DISPLAY lib-factor. /*$A39262*/
    END.
    ELSE DO:
        IF parsoc.int_parc <> "" THEN DISPLAY lib-factor. /*$A39262*/
    END.

    ENABLE
        entetcli.num_tel entetcli.num_fax entetcli.liasse entetcli.taux_esc entetcli.gencod_det entetcli.saison br-ss entetcli.grp_ps br-gpres
        entetcli.fac_ttc entetcli.fac_pxa entetcli.maj_ach entetcli.proforma entetcli.fra_app /*$A59971 ... */ entetcli.lias_bl /* ... $A59971 */
        entetcli.edt_arc entetcli.prep_isol entetcli.plus_bpbl entetcli.liv_cpl entetcli.cde_cpl entetcli.bl_regr entetcli.chif_bl
        entetcli.tva_date entetcli.dat_ech
        entetcli.cat_tar br-ct entetcli.cat_cum
        vcl_grp brcgrp entetcli.tar_fil[1] entetcli.tar_cum[1]
        vcl_livre brclivre entetcli.tar_fil[2] entetcli.tar_cum[2]
        vcl_fact brcfact entetcli.tar_fil[3] entetcli.tar_cum[3]
        vcl_paye brcpaye entetcli.tar_fil[4] entetcli.tar_cum[4]
        vcl_pln brcpln entetcli.tar_fil[5] entetcli.tar_cum[5]
        vcl_plr brcplr entetcli.tar_fil[6] entetcli.tar_cum[6]
        vcl_stat brcstat
        btn-lancement WHEN (type-cours = "C" AND AVAIL pargp AND pargp.prealable AND pargp.par_prea = "C" AND pargp.cde_prea AND creation = no)
        entetcli.commerc[2] br-rp2 entetcli.cpt_bq /*$1727 ...*/ toggle-contrat radio-contrat fill-contratloc btn-contratloc /*... $1727*/ brcchq
        entetcli.borne.

    IF parsoc.mult_soc THEN DO:
        FIND parvs WHERE parvs.ndos = g-ndos NO-LOCK NO-ERROR.
        IF parvs.int_parc <> "" THEN ENABLE vc_factor brcfactor. /*$A39262*/
        ELSE HIDE vc_factor brcfactor lib-factor. /*$A39262*/
    END.
    ELSE DO:
        IF parsoc.int_parc <> "" THEN ENABLE vc_factor brcfactor. /*$A39262*/
        ELSE HIDE vc_factor brcfactor lib-factor. /*$A39262*/
    END.

    /* $A59971 ... */
    /* Param�tres gestion liasses B.L */
    if not gst_zone[35] then hide entetcli.lias_bl.
    /* ... $A59971 */

    /* Param�tres gestion facturation */
    IF NOT lgTextile THEN HIDE entetcli.saison br-ss lib-ss entetcli.grp_ps br-gpres lib-gpres entetcli.gencod_det.
    if not gst_zone[51] then hide entetcli.num_tel.
    if not gst_zone[52] then hide entetcli.num_fax.
    if not gst_zone[53] then hide entetcli.chif_bl.
    if not gst_zone[54] then hide entetcli.fra_app.
    if not gst_zone[55] then hide entetcli.proforma.
    if not gst_zone[56] then hide entetcli.liasse.
    if not gst_zone[57] then hide entetcli.taux_esc.
    if not parsoc.fac_ttc or not gst_zone[58] then hide entetcli.fac_ttc.
    if not gst_zone[59] then hide entetcli.fac_pxa entetcli.maj_ach.
    if not gst_zone[60] then hide entetcli.edt_arc.
    if not parsoc.rem_vte or not gst_zone[61] then hide entetcli.borne fborne. else display fborne.
    if not gst_zone[61] then hide entetcli.cat_tar br-ct lib-ct entetcli.cat_cum.
    if not gst_zone[62] then hide vcl_stat brcstat lib-stat.
    if not gst_zone[64] then hide vcl_fact vcl_grp vcl_paye vcl_livre brcfact brcgrp brcpaye brclivre lib-fact lib-paye lib-grp lib-livre
        entetcli.tar_fil[1] entetcli.tar_fil[2] entetcli.tar_fil[3] entetcli.tar_fil[4]
        entetcli.tar_cum[1] entetcli.tar_cum[2] entetcli.tar_cum[3] entetcli.tar_cum[4].
    if not gst_zone[64] or not parsoc.gst_plat then hide vcl_pln vcl_plr brcpln brcplr lib-pln lib-plr
        entetcli.tar_fil[5] entetcli.tar_fil[6]
        entetcli.tar_cum[5] entetcli.tar_cum[6].
    if not gst_zone[65] then hide entetcli.commerc[2] br-rp2 lib-rp2.
    if entetcli.fac_pxa:SCREEN-VALUE = "non" THEN HIDE entetcli.maj_ach.
    IF NOT parsoc.bp_regr THEN HIDE entetcli.plus_bpbl.
    IF NOT parsoc.bl_regr THEN HIDE entetcli.bl_regr.
    IF NOT parsoc.ges_bpr THEN HIDE entetcli.prep_isol.
    /* $1727 ... */
    IF NOT parsoc.ges_loc AND NOT parsoc.ges_sav THEN HIDE toggle-contrat fill-contratloc btn-contratloc lib-contratloc.
    ELSE IF parsoc.ges_loc AND NOT parsoc.ges_sav THEN ASSIGN toggle-contrat:LABEL = Traduction("Associer contrat de location",-1,"")
                                                              toggle-contrat:WIDTH = 25.57
                                                              radio-contrat:HIDDEN = YES
                                                              vTypeContrat = "LOC":U.
    ELSE IF NOT parsoc.ges_loc AND parsoc.ges_sav THEN ASSIGN toggle-contrat:LABEL = Traduction("Associer contrat de maintenance",-1,"")
                                                              toggle-contrat:WIDTH = 25.57
                                                              radio-contrat:HIDDEN = YES
                                                              vTypeContrat = "".
    IF creation = NO THEN DO:
        IF entetcli.contrat = "" THEN ASSIGN toggle-contrat:SCREEN-VAL = "NO"
                                             radio-contrat:HIDDEN = YES
                                             fill-contratloc:SCREEN-VAL = "" lib-contratloc:SCREEN-VAL = "" /*$A50403*/
                                             fill-contratloc:HIDDEN = YES
                                             btn-contratloc:HIDDEN = YES
                                             lib-contratloc:HIDDEN = YES.
        ELSE DO:
            /*$A62169...*/
            FIND FIRST savcon WHERE savcon.contrat = entetcli.contrat AND savcon.typ_con = "LOC" NO-LOCK NO-ERROR.
            IF NOT AVAIL savcon THEN FIND FIRST savcon WHERE savcon.contrat = entetcli.contrat AND savcon.typ_con = "" NO-LOCK NO-ERROR.
            IF AVAIL savcon THEN DO :
                ASSIGN toggle-contrat:SCREEN-VAL = "YES"
                       radio-contrat:SCREEN-VAL = (IF AVAIL savcon AND savcon.typ_con = "LOC":U THEN "L" ELSE "M")
                       vTypeContrat = (IF radio-contrat:SCREEN-VAL = "L" THEN "LOC":U ELSE "")
                       fill-contratloc:SCREEN-VAL = entetcli.contrat
                       fill-contratloc
                       fill-contratloc:PRIVATE-DATA = STRING(entetcli.ind_con).
                IF fill-contratloc:SCREEN-VAL <> "" THEN DO: /*Ne pas faire de apply sur fill-contrat sinon on perd ind_contrat*/
                    FIND FIRST savcon WHERE savcon.contrat = fill-contratloc:SCREEN-VAL AND savcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
                    IF AVAIL savcon THEN lib-contratloc:SCREEN-VAL = savcon.lib_con.
                    IF NOT AVAIL savcon AND vTypeContrat = "LOC":U THEN DO : /*$A74851*/
                        FIND FIRST savhcon WHERE savhcon.contrat = fill-contratloc:SCREEN-VAL AND savhcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
                        IF AVAIL savhcon THEN DO :
                            ASSIGN fill-contratloc:SENSITIVE = NO
                                   btn-contratloc:SENSITIVE = NO
                                   lib-contratloc:SCREEN-VAL = savhcon.lib_con.
                        END.
                    END. /*$A74851*/
                END.
            END.
            ELSE ASSIGN toggle-contrat:SCREEN-VAL = "NO"
                radio-contrat:HIDDEN = YES
                fill-contratloc:HIDDEN = YES
                btn-contratloc:HIDDEN = YES
                lib-contratloc:HIDDEN = YES.
            /*...$A62169*/
        END.
    END.
    ELSE ASSIGN toggle-contrat:SCREEN-VAL = "NO"
                radio-contrat:HIDDEN = YES
                fill-contratloc:HIDDEN = YES
                btn-contratloc:HIDDEN = YES
                lib-contratloc:HIDDEN = YES.
    /* ... $1727 */
END.
DO WITH FRAME fc2 :
    IF type-cours = "C" AND (AVAIL pargp AND pargp.prealable AND pargp.par_prea = "C") THEN
    DO:
        IF creation = NO THEN
           ASSIGN fill-lancement:HIDDEN = NO btn-lancement:HIDDEN = NO
                  fill-lancement:SCREEN-VAL = entetcli.NO_lance
                  fill-lancement.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-title wsaicl1
PROCEDURE envoi-title :
/*$A95455...*/
DEF VAR cTitleF AS CHAR NO-UNDO.
IF entetcli.typ_sai = "F" THEN DO:
    IF GetSessionData("copylg_c","avoir":U) <> "" THEN DO:
        IF nopt = 615 THEN cTitleF = Traduction("Facture financi�re",-2,"").
        ELSE
            IF nopt = 556 THEN cTitleF = Traduction("Avoir financier",-2,"").
        ELSE /*nopt = 53*/
            IF GetSessionData("copylg_c","avoir":U) = "YES" THEN cTitleF = Traduction("Avoir client",-2,""). ELSE cTitleF = Traduction("Facture client",-2,"").
    END.
    ELSE DO:
        IF nopt = 615 THEN cTitleF = Traduction("Facture financi�re",-2,""). ELSE cTitleF = Traduction("Facture client",-2,"").
    END.
END.
/*...$A95455*/
{&WINDOW-NAME}:title=
        (if entetcli.typ_sai="C" then Traduction("Commande client",-2,"") + " "
            else if entetcli.typ_sai="D" then Traduction("Devis client",-2,"") + " "
            /*$A95455 else if entetcli.typ_sai="F" then (IF nopt = 615 THEN Traduction("Facture financi�re",-2,"") ELSE Traduction("Facture client",-2,"")) + " " */
            ELSE IF entetcli.typ_sai="F" THEN cTitleF + " " /*$A95455*/
            else if entetcli.typ_sai="A" then Traduction("Abonnement",-2,"") + " "
            else Traduction("Comptoir client",-2,"") + " ") +
        entetcli.adr_cde[1] + " " + Traduction("de",-2,"") + " " + entetcli.villec +
        " " + Traduction("N� T�l�phone",-2,"") + " : " + entetcli.num_tel +
        " " + IF parsoc.FORM_liv=3 THEN Traduction("Date exp�dition",-2,"")
              ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�part",-2,"")
              ELSE IF parsoc.FORM_liv=1 THEN Traduction("Date livraison",-2,"")
              ELSE Traduction("Date livraison",-2,"") + " " + "(" + Traduction("D�part",-2,"") + ")" + " " + STRING(entetcli.dat_liv) +
        (IF entetcli.tournee<>"" THEN " " + Traduction("Tourn�e",-2,"") + ":" + "" + " " + entetcli.tournee ELSE "").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERGO-PROC-WINDOW-RESIZED wsaicl1
PROCEDURE ERGO-PROC-WINDOW-RESIZED :
IF FRAME f1:SENSITIVE AND FRAME F1:HIDDEN = NO AND valid-handle(hbrowse)
        THEN ASSIGN hbrowse:WIDTH  = FRAME f1:WIDTH - 0.29
                    hbrowse:HEIGHT = FRAME f1:HEIGHT - 6.70.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERGO-PROC-WINDOW-RESIZED2 wsaicl1
PROCEDURE ERGO-PROC-WINDOW-RESIZED2 :
IF valid-handle(hbrowse)
        THEN ASSIGN hbrowse:WIDTH  = FRAME f1:WIDTH - 0.29
                    hbrowse:HEIGHT = FRAME f1:HEIGHT - 6.70.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fin-envoi-entete wsaicl1
PROCEDURE fin-envoi-entete :
IF v-rapide = "oui" AND porigine <> "2" /*AND porigine <> "6b" $1750 */ THEN RUN choose-bvalider-p. /*$872 ...*/
ELSE DO:
    RUN Ecran2_Ok.
    if ecr-cours<>2 then do:
        ecr-cours=2.
        run aff-assist.
    end.
END.
/*....$872*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fin-pgm wsaicl1
PROCEDURE fin-pgm :
IF LoadSettings("HBSAICL1", OUTPUT hSettingsApi) THEN DO:
        ASSIGN logbid=SaveSetting("ToutTri",PCL_GET_VALEUR("chqryby")) /* $2240 */
               logbid=SaveSetting("ChampTri", PCL_GET_VALEUR("colord":U)) /* $2240 */
               logbid=SaveSetting("*HBColonnes", qliste-champ)
               logbid=SaveSetting("*HBBloquees", STRING(qcolonne-lock))
               logbid=SaveSetting("*HBLabels", qliste-label)
               logbid=SaveSetting("*HBCondition", qcondition).

        UnloadSettings(hSettingsApi).
    end.
    /*$A36586...*/
    IF LoadSettings("PAREDTBL":U, OUTPUT hSettingsApi) THEN DO:
        logbid=SaveSetting("DocsComp",STRING(CorDocsComp,"OUI/NON":U)).
        logbid=SaveSetting("AvecConf",STRING(CorAvcConf,"OUI/NON":U)).
        logbid=SaveSetting("docmailBL",STRING(docmailBL,"OUI/NON":U)). /*$A49610*/
        UnloadSettings(hSettingsApi).
    END.
    /*...$A36586*/
    /*$A49610...*/
    IF LoadSettings("PAREDTARC":U, OUTPUT hSettingsApi) THEN DO:
        logbid=SaveSetting("DocsComp",STRING(ARCDocsComp,"OUI/NON":U)).
        logbid=SaveSetting("AvecConf",STRING(ARCAvcConf,"OUI/NON":U)).
        logbid=SaveSetting("docmailarc",STRING(docmailarc,"OUI/NON":U)).
        UnloadSettings(hSettingsApi).
    END.
    IF LoadSettings("PAREDTDEV":U, OUTPUT hSettingsApi) THEN DO:
        logbid=SaveSetting("DocsComp",STRING(DEVDocsComp,"OUI/NON":U)).
        logbid=SaveSetting("AvecConf",STRING(DEVAvcConf,"OUI/NON":U)).
        logbid=SaveSetting("docmaildev",STRING(docmaildev,"OUI/NON":U)).
        UnloadSettings(hSettingsApi).
    END.
    IF LoadSettings("PAREDTFAC":U, OUTPUT hSettingsApi) THEN DO:
        logbid=SaveSetting("DocsComp",STRING(FACDocsComp,"OUI/NON":U)).
        logbid=SaveSetting("AvecConf",STRING(FACAvcConf,"OUI/NON":U)).
        logbid=SaveSetting("docmailfac",STRING(docmailfac,"OUI/NON":U)).
        UnloadSettings(hSettingsApi).
    END.
    /*...$A49610*/
    if valid-handle(Hpostit) then apply "close" to hpostit.
    if valid-handle(child-info) then apply "close" to child-info.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GET-VARIABLE wsaicl1
PROCEDURE GET-VARIABLE :
/* $A38744 */
    DEF INPUT  PARAM pvariable AS CHAR  NO-UNDO.
    DEF OUTPUT PARAM pvaleur   AS CHAR  NO-UNDO.

    /* Permet de r�cup�rer une valeur dans un autre programme sans rajouter de param�tre */
    /* dans l'autre programme :
       DEF VAR hparent AS HANDLE     NO-UNDO.
       hparent = SOURCE-PROCEDURE.
       IF CAN-DO(hparent:INTERNAL-ENTRIES,"GET-VARIABLE") THEN RUN GET-VARIABLE IN hparent ("retour", OUTPUT retour).
     */

    CASE pvariable :
        WHEN "type-saisie":U  THEN pvaleur = type-saisie.
        WHEN "child-info":U   THEN pvaleur = STRING(child-info).
        WHEN "porigine":U     THEN pvaleur = STRING(porigine).
        /* $A40589... */
        WHEN "pacces":U       THEN pvaleur = "E".
        WHEN "nopt":U         THEN pvaleur = STRING(nopt).
        WHEN "v-rapide":U     THEN pvaleur = v-rapide.
        WHEN "creation-piece":U THEN pvaleur = STRING(creation).
                /* ...$A40589 */
    END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE INFO-ADRESSE-LIVRAISON wsaicl1
PROCEDURE INFO-ADRESSE-LIVRAISON :
DEF OUTPUT PARAMETER civLiv  AS CHARACTER          NO-UNDO.
    DEF OUTPUT PARAMETER adrLiv  AS CHARACTER EXTENT 3 NO-UNDO.
    DEF OUTPUT PARAMETER adrLiv4 AS CHARACTER          NO-UNDO.
    DEF OUTPUT PARAMETER kpost2l AS CHARACTER          NO-UNDO.
    DEF OUTPUT PARAMETER villel  AS CHARACTER          NO-UNDO.
    DEF OUTPUT PARAMETER paysl   AS CHARACTER          NO-UNDO.
    DEF OUTPUT PARAMETER recup   AS LOGICAL            NO-UNDO.

    /* $A38401 appel� depuis mntaff_af */

    IF NOT creation THEN DO:
        IF NOT AVAIL cptcli THEN FIND FIRST cptcli WHERE cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli = client.cod_cli NO-LOCK NO-ERROR.
         IF (cptcli.civilite <> entetcli.civ_liv OR client.nom_cli <> entetcli.adr_liv[1] OR client.adresse[1] <> entetcli.adr_liv[2] OR
             client.adresse[2] <> entetcli.adr_liv[3] OR client.adresse[3] <> entetcli.adrliv4 OR TRIM(client.k_post2) <> entetcli.k_post2l OR
             client.ville <> entetcli.villel OR client.pays <> entetcli.paysl) THEN DO:
            MESSAGE Traduction("Voulez-vous r�cup�rer l'adresse de livraison de la pi�ce en cours ?",-2,"") VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE recup.
            IF recup THEN
                ASSIGN
                    civLiv    = entetcli.civ_liv
                    adrLiv[1] = entetcli.adr_liv[1]
                    adrLiv[2] = entetcli.adr_liv[2]
                    adrLiv[3] = entetcli.adr_liv[3]
                    adrLiv4   = entetcli.adrliv4
                    kpost2l   = entetcli.k_post2l
                    villel    = entetcli.villel
                    paysl     = entetcli.paysl.
         END.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-colonnes wsaicl1
PROCEDURE init-colonnes :
ASSIGN
        qliste-champ  = "typ_sai,no_cde,no_bl,dat_cde,ref_cde,dat_liv,dat_dep,sta_pce,mt_ht,mt_cde,informat,qui,not_ref,typ_cde,nat_cde,depot,arc_fax,arc_mail"
        qliste-label  = "T" + "|" + Traduction("N� Cde",-2,"") + "|" + Traduction("N� B.L",-2,"") + "|" + Traduction("Date Cde",-2,"") + "|" + Traduction("R�f�rence",-2,"") + "|" +
                        (IF parsoc.FORM_liv=3 THEN Traduction("Date exp.",-2,"") ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�p.",-2,"") ELSE Traduction("Date liv.",-2,"")) + "|" +
                        (IF parsoc.FORM_liv=3 THEN Traduction("Exp. ini.",-2,"") ELSE IF parsoc.FORM_liv=2 THEN Traduction("D�p. ini.",-2,"") ELSE Traduction("Liv. ini.",-2,"")) + "|" +
                        Traduction("Commentaire",-2,"") + "|" + Traduction("HT",-2,"") + " " + g-dftdev + "�t" + "|" +
                        Traduction("TTC",-2,"") + " " + g-dftdev + "�t" + "|" + Traduction("Information",-2,"") + "|" + Traduction("Qui",-2,"") + "|" + Traduction("Notre r�f�rence",-2,"") + "|" +
                        "TC" + "|" + Traduction("Nature",-2,"") + "|" + Traduction("D�p�t",-2,"") + "|" + Traduction("Fax�",-2,"") + "|" + Traduction("Mail�",-2,"")
        qcondition    = ""
        qcolonne-lock = 0.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE lancerCatalogue wsaicl1
PROCEDURE lancerCatalogue :
/*$A38645*/
    DEFINE INPUT  PARAMETER pcod_ce     AS CHARACTER   NO-UNDO.    /* code catalogue */
    DEFINE INPUT  PARAMETER ptitre      AS CHARACTER   NO-UNDO.    /* titre catalogue */
    DEFINE INPUT  PARAMETER pappelant   AS CHARACTER   NO-UNDO.    /* titre Programme progress appelant */

    LOGBID = SESSION:SET-WAIT-STATE("GENERAL").

    bcataloguePressed = YES.
    RUN maj-entete.

    /* RUN lanccat_ce (pcod_ce,ptitre,entetcli.cod_cli, entetcli.typ_sai, entetcli.no_cde, entetcli.depot, wsaicl1).*/
    RUN lanccat_ce (pcod_ce,ptitre,rowid(entetcli), wsaicl1, pappelant).

    LOGBID = SESSION:SET-WAIT-STATE("").
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-al wsaicl1
PROCEDURE maj-al :
/*$A35969...*/

    IF adrliv-modifie THEN ASSIGN
        entetcli.zon_geo = adrliv-zon_geo
        entetcli.transpor = adrliv-transpor
        entetcli.MOD_liv = adrliv-mod_liv
        entetcli.comment = adrliv-comment
        entetcli.com_liv[1] = adrliv-com_liv[1]
        entetcli.com_liv[2] = adrliv-com_liv[2]
        entetcli.com_liv[3] = adrliv-com_liv[3]
        entetcli.com_liv[4] = adrliv-com_liv[4]
        entetcli.civ_liv = adrliv-civ_liv
        entetcli.adr_liv[1] = adrliv-adr_liv[1]
        entetcli.adr_liv[2] = adrliv-adr_liv[2]
        entetcli.adr_liv[3] = adrliv-adr_liv[3]
        entetcli.adrliv4 = adrliv-adrliv4
        entetcli.k_post2l = adrliv-k_post2l
        entetcli.villel = adrliv-villel
        entetcli.paysl = adrliv-paysl
        entetcli.delai_trs = adrliv-delai_trs
        entetcli.port = adrliv-port
        entetcli.typ_veh = adrliv-typ_veh
        entetcli.typ_con = adrliv-typ_con
        entetcli.k_postl = adrliv-no_adr.

    ELSE DO:
    /*...$A35969*/

        /* L'adresse livraison par d�faut a peut-�tre �t� chang�e ? */
        IF entetcli.k_postl<>wno_adr THEN DO:
            entetcli.k_postl=wno_adr.
            find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if entetcli.cl_livre=0 THEN entetcli.cod_cli ELSE entetcli.cl_livre) AND adresse.affaire="" and adresse.cod_adr=wno_adr no-lock no-error.
            IF AVAIL adresse THEN DO :
                ASSIGN
                entetcli.zon_geo =(IF adresse.zon_geo= 0 THEN client.zon_geo  ELSE adresse.zon_geo) /*$1728*/
                entetcli.transpor=(IF adresse.transpor=0 THEN client.transpor ELSE adresse.transpor) /*$1728*/
                entetcli.MOD_liv=(IF adresse.MOD_liv="" THEN client.MOD_liv ELSE adresse.MOD_liv) /*$1728*/
                entetcli.comment=adresse.comment
                entetcli.com_liv[1]=adresse.com_liv[1]
                entetcli.com_liv[2]=adresse.com_liv[2]
                entetcli.com_liv[3]=adresse.com_liv[3]
                entetcli.com_liv[4]=adresse.com_liv[4]
                entetcli.civ_liv=adresse.civilite
                entetcli.adr_liv[1]=adresse.nom_adr
                entetcli.adr_liv[2]=adresse.adresse[1]
                entetcli.adr_liv[3]=adresse.adresse[2]
                entetcli.adrliv4=adresse.adresse4
                entetcli.k_post2l=adresse.k_post2
                entetcli.villel=adresse.ville
                entetcli.paysl=adresse.pays.
                RUN trt-deltra.
                /*entetcli.delai_trs = wdelai_trs.*/
                /*$1728...*/
                find tabgco where tabgco.type_tab="ML" and a_tab=entetcli.MOD_liv no-lock no-error.
                if available tabgco then entetcli.port=tabgco.code_df.
                /*...$1728*/

                IF adresse.transpor=0 AND pref_parcde_transpor THEN DO:
                    FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = entetcli.depot NO-LOCK NO-ERROR.
                    IF AVAIL depcli AND depcli.transpor > 0 THEN entetcli.transpor = depcli.transpor.
                END.
            END.
        END.

        entetcli.delai_trs = wdelai_trs. /* GC : on affecte le d�lai � chaque fois car si on modifie � partir du bouton "autres dates" �a ne marche pas */

    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete wsaicl1
PROCEDURE maj-entete :
DEF VAR bvl AS CHAR no-undo.
DEF VAR wdepot AS INT NO-UNDO.
DEF VAR wstatut AS CHAR NO-UNDO.
DEF VAR aUnit AS CHAR  NO-UNDO.
DEFINE VARIABLE vsetudedef AS LOGICAL    NO-UNDO. /*$A74998*/

DO TRANSACTION:
    if creation and not modif-entete then do:
        create entetcli.
        assign
            entetcli.ndos       = (IF parsoc.mult_soc THEN g-ndos ELSE 0)
            entetcli.depot      = INT(edepot:SCREEN-VAL IN FRAME fc1)
            entetcli.dep_csg    = (IF parcde.gst_zone[33] AND tConsign:CHECKED THEN depotcsg else 0) /*$1130*/
            entetcli.typ_sai    = type-saisie
            entetcli.cod_cli    = int(entetcli.cod_cli:screen-value in frame fonglet)
            entetcli.no_cde     = int(entetcli.no_cde:screen-value in frame fonglet)
            entetcli.no_bl      = int(no_bl:SCREEN-VALUE IN FRAME Fonglet)
            usr_crt             = g-user
            dat_crt             = today
            typ_cde             = typ-cde
            Entetcli.qui        = /*qui$ $913...*/ (IF quiauto <> "" THEN quiauto ELSE if can-do("3,4",porigine) THEN interv ELSE /*$A64667...*/ IF porigine = "6a" THEN vqui ELSE /*...$A64667*/ qui$) /*...$913*/
            qui_liv             = /*qui$ $913...*/ (IF quiauto <> "" THEN quiauto ELSE if can-do("3,4",porigine) THEN interv ELSE /*$A64667...*/ IF porigine = "6a" THEN vqui ELSE /*...$A64667*/ qui$) /*...$913*/
            ENTETCLI.HR_SAI     = int(substr(string(time,"hh:mm:ss"),1,2) + substr(string(time,"hh:mm:ss"),4,2) + substr(string(time,"hh:mm:ss"),7,2))
            entetcli.retr_cess  = (IF client.c_retr THEN "R" ELSE IF client.or_ces THEN "C" ELSE "").

        IF client.or_ces THEN entetcli.motif_av = "##$".

        IF nopt = 556 THEN entetcli.af_fin = "A". /* Avoir financier */
        ELSE IF nopt = 615 THEN entetcli.af_fin = "F". /* Facture financi�re */
        if can-do("3,4",porigine) AND type-plan="T" THEN entetcli.cod_tlv=interv.

        assign frame fc1 entetcli.famille entetcli.s2_famille entetcli.s3_famille entetcli.s4_famille /*$A41444...*/ entetcli.langue /*...$A41444*/ . /*$333*/
        {entetcl3.i entetcli} /* Textes BL,factures */
        IF type-saisie = "D" AND wori_ate THEN /* $754 si atelier on cree ateent */
        DO:
            entetcli.ori_ate = YES. /* $a36019 */
            RUN maj-entete-suite1. /* pnv_op1 */
        END.

        /* Si saisie comptoir alors on recherche si l'utilisateur est li� � une caisse $2129 */
        IF type-saisie = "R" AND parsoc.ges_cai /* $A127383 ... */ AND Pcaisse > 0 /* ... $A127383 */ THEN entetcli.NO_caisse = Pcaisse.

        SetLock ("entetcli", STRING(ROWID(entetcli))). /* $A36207 */

    end.

    /* MAJ tout le temps */

    entetcli.motif_av = (IF parcde.gst_zone[33] AND tConsign:CHECKED THEN "##C" ELSE IF entetcli.motif_av="##C" THEN "" ELSE entetcli.motif_av). /*$1130*/
    /* texte */
    if note-modifiee then do:
        find txtentcl of entetcli no-lock no-error.
        if available txtentcl and txtentcl.note<>"" then entetcli.informat=substr(txtentcl.note,1,40).
        else entetcli.informat="".
    end.
    /* suppression de tous les plans d'�ch�ances �ventuellement cr��s */
    if (tmultiech:hidden in frame fc1=yes) OR (tmultiech:hidden in frame fc1=NO AND TMultiEch:screen-value in frame fc1 ="no") then do:
        for each EchCli of entetcli:
            delete EchCLi.
        end.
        entetcli.nb_ech=0.
    end.
    else do:
        X=0.
        for each EchCLi of entetcli NO-LOCK:
            x = x + 1.
        end.
        entetcli.nb_ech = x.
    end.
    RUN maj-entete-suite2. /* $pnv_op1 */

    /* GPAO */
    IF type-cours = "c" AND (AVAIL pargp AND pargp.prealable AND pargp.par_prea = "C" AND pargp.cde_prea) THEN
    DO:
        x = 0.
        DEF VAR ch AS CHAR no-undo.
        IF creation THEN /* si n'existe pas, le cr�er */
        DO:
            X = NEXT-VALUE (compteur_lancement).
            ch = STRING (" ","x(3)") + trim(STRING (X, ">>>999999")).
            IF X <> 0 THEN
            DO:
                CREATE lancement.
                ASSIGN
                    lancement.dat_crt = TODAY
                    lancement.dat_lan = TODAY
                    lancement.depot = INT(edepot:SCREEN-VAL IN FRAME fc1)
                    lancement.par_prea = "C"
                    lancement.prealable = YES
                    lancement.usr_crt = G-USER
                    lancement.NO_lance = STRING (" ","x(3)") + trim(STRING (X, ">>>999999")).
               lancement.descriptif = (IF AVAIL pargp AND pargp.nom_lanc <> "" THEN pargp.nom_lanc ELSE Traduction("Lancement",-2,"")) + " " +
               Traduction("pr�alable",-2,"") + " " + "-" + " " + Traduction("Commande",-2,"") + " " + ":" + "" + " " + (IF AVAIL entetcli THEN STRING (entetcli.NO_cde) ELSE "").

               FIND FIRST prefinfu WHERE  prefinfu.contexte = "LANCPREA" AND prefinfu.cod_user = g-user AND prefinfu.actif NO-LOCK NO-ERROR.
               IF NOT AVAIL prefinfu THEN
                   FIND FIRST prefinfu WHERE  prefinfu.contexte = "LANCPREA" AND prefinfu.cod_user = "" AND prefinfu.actif NO-LOCK NO-ERROR.
               IF AVAIL prefinfu THEN
               DO:
                   /* On a un param�trage pour le lancement pr�alable */
                   RUN exeinfo ("LANCPREA","entetcli",STRING (BUFFER entetcli:HANDLE),NO,?,YES,"NON",OUTPUT bvl).
                   IF bvl <> "" THEN
                       bvl = REPLACE (bvl,"*p","").
                   IF bvl <> "" THEN lancement.descriptif = SUBSTR (bvl,1,80).
               END.
            END.
            fill-lancement:SCREEN-VAL IN FRAME fc2 = lancement.NO_lance.
        END.
        ELSE /* en modif */
        DO:
            logbid=NO.
            IF (fill-lancement <> fill-lancement:SCREEN-VAL IN FRAME fc2) THEN
            DO:
                /* on a change de lancement pour la commande */
                FOR EACH lignecli OF entetcli WHERE lignecli.prod_gp=YES AND lignecli.statut = "" :
                    IF fabsurdepot (lignecli.cod_pro, lignecli.depot) THEN
                    DO:
                        lignecli.NO_lance = fill-lancement:SCREEN-VAL IN FRAME fc2.
                        IF lignecli.NO_of > 0 THEN
                        DO:
                            FOR EACH prefab WHERE prefab.cod_cli = lignecli.cod_cli AND prefab.NO_cde = lignecli.NO_cde AND
                                                    prefab.NO_ligne = lignecli.NO_ligne:
                                FOR EACH precons WHERE precons.no_of = prefab.NO_of:
                                    precons.NO_lance = lignecli.NO_lance.
                                END.
                                FOR EACH pregam WHERE pregam.no_of = prefab.NO_of:
                                    pregam.NO_lance = lignecli.NO_lance.
                                END.
                                prefab.NO_lance = lignecli.NO_lance.
                            END.
                        END.
                    END.
                END.

                FOR FIRST lancement FIELDS (depot statut) WHERE lancement.NO_lance = fill-lancement NO-LOCK :
                    ASSIGN  wdepot = lancement.depot wstatut = lancement.statut.
                END.
                FIND FIRST lignecli WHERE lignecli.prod_gp=YES AND lignecli.statut = wstatut AND lignecli.typ_sai = entetcli.typ_sai AND lignecli.depot = wdepot AND lignecli.NO_lance = fill-lancement NO-LOCK NO-ERROR.
                IF AVAILABLE lignecli THEN logbid = TRUE.
                ELSE DO:
                    FIND FIRST entetcli2 WHERE entetcli2.NO_lance = fill-lancement AND entetcli2.no_cde <> entetcli.no_cde NO-LOCK NO-ERROR.
                    IF AVAILABLE entetcli2 THEN logbid = TRUE.
                END.
                IF logbid = NO THEN
                DO:
                    FIND FIRST lancprea WHERE lancprea.NO_lance = fill-lancement EXCLUSIVE-LOCK NO-ERROR.
                    IF AVAILABLE lancprea THEN DELETE lancprea.
                    FIND FIRST lancement WHERE lancement.NO_lance = fill-lancement EXCLUSIVE-LOCK NO-ERROR.
                    IF AVAILABLE lancement THEN DO:
                        lancement.anc_statut = "saicl1_c.w" + CHR(1) + "PGM".
                        DELETE lancement.
                    END.
                END.
            END.
        END.
        entetcli.NO_lance = fill-lancement:SCREEN-VAL IN FRAME fc2.
    END.
    /*Textile*/
    IF lgTextile THEN
        ASSIGN MAJPREST = (entetcli.grp_ps <> entetcli.grp_ps:SCREEN-VALUE IN FRAME fc2)
               entetcli.saison = entetcli.saison:SCREEN-VALUE IN FRAME fc2
               entetcli.grp_ps = entetcli.grp_ps:SCREEN-VALUE
               entetcli.gencod_det = (entetcli.gencod_det:SCREEN-VAL = "yes").
    /* Onglet 'Divers' */
    if parcde.gst_zone[1] then do:
        RUN maj-entete-suite3. /* $pnv_op1 */
    end.
    if creation then do:
        RUN maj-entete-suite4. /* $pnv_op1 */
    end.

    RUN maj-al.

    /*$1728...
    RUN trt-deltra.
    entetcli.delai_trs = wdelai_trs. $1728*/

    RUN maj-entete-suite5. /* $pnv_op1 */

    /* Client payeur, factur� */
    IF entetcli.CL_fact <> 0 AND entetcli.CL_fact<>entetcli.cod_cli THEN DO:
        if not parcde.gst_zone[1] then DO:
            FIND clientr WHERE clientr.COD_CLI = entetcli.CL_FACT NO-LOCK NO-ERROR.
            IF AVAIL clientr THEN DISP entetcli.liasse=clientr.liasse.
        END.
        FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = entetcli.CL_fact NO-LOCK NO-ERROR.
        IF AVAIL cptclir THEN DO:
            ASSIGN
                entetcli.edi_rlv = (cptclir.edi_rlv="O")
                entetcli.iden_ce = cptclir.idcee.
            if not parcde.gst_zone[1] then ASSIGN
                entetcli.num_tel = cptclir.num_tel
                entetcli.num_fax = cptclir.num_fax
                entetcli.CPT_BQ = cptclir.cpt_bq.
        END.
    END.
    IF entetcli.CL_PAYE <> 0 AND entetcli.CL_PAYE<>entetcli.COD_CLI AND entetcli.CL_PAYE<>entetcli.CL_FACT THEN DO:
        FIND cptclir WHERE cptclir.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptclir.COD_CLI = entetcli.CL_PAYE NO-LOCK NO-ERROR.
        IF AVAIL cptclir THEN DO:
            entetcli.edi_rlv = (cptclir.edi_rlv="O").
            if not parcde.gst_zone[1] then entetcli.CPT_BQ=cptclir.cpt_bq.
        END.
    END.
    /* Valeurs libres */
    modif-val-libre=no.
    do x=1 to 5:
        if entetcli.znu[x]<>wn[x] then modif-val-libre=yes.
        ASSIGN entetcli.znu[x]=wn[x] entetcli.zal[x]=wa[x] entetcli.zta[x]=wt[x] entetcli.zda[x]=wd[x] entetcli.zlo[x]=wl[x].
    end.
    if modif-val-libre then do:
        find first lignefor of entetcli no-lock no-error.
        if available lignefor then do :
            RUN majfor_c (ROWID (client), ROWID (entetcli)).
            if can-find(first lignecli where lignecli.cod_cli=entetcli.cod_cli and lignecli.typ_sai=entetcli.typ_sai
                                         and lignecli.no_cde=entetcli.no_cde and lignecli.no_bl=entetcli.no_bl
                                         and lignecli.sous_type="PH"
                                         and (lignecli.niveau1<>0 or lignecli.niveau2<>0 or lignecli.niveau3<>0 or lignecli.niveau4<>0 or lignecli.niveau5<>0)
                                         and (if porigine='5' then lignecli.num_ave<>0 else yes)) then
                run maj_phase (entetcli.cod_cli,entetcli.NO_cde,entetcli.NO_bl,entetcli.typ_sai).
        end.
    end.
    /* Divers factur�s */
    if creation and not modif-entete then do:
        find parcde2 where parcde2.typ_fich="P" no-lock no-error.
        DO X=1 TO 3:
            ASSIGN
                entetcli.div_fac[x] = client.div_fac[x]
                entetcli.val_fac[x] = client.val_fac[x].
        END.
        {entetcl2.i entetcli} /* Divers factur�s client */
    end.
    if (not creation or modif-entete) and (entetcli.rem_glo[1]<>0 or entetcli.rem_glo[2]<>0) then do:
      if entetcli.cal_marg="V" or can-find(first lignecli of entetcli where lignecli.sous_type="PH" and lignecli.cal_marg="V" no-lock)
        then assign entetcli.rem_glo[1]=0 entetcli.rem_glo[2]=0.
    end.
    if not creation and not modif-entete and entetcli.fac_edi=yes and entetcli.no_fact<>0 THEN RUN test-regroupe.
    assign entetcli.fac_edi=no modif-entete=yes.
    RUN maj_statut_pce.

    release lignecli.
    release ligneclt.
END.
/* $TX */
IF lgtextile AND MAJPREST AND CAN-FIND (FIRST lignecli OF entetcli NO-LOCK) THEN DO:
    MESSAGE Traduction("Le groupe de prestations a �t� modifi�, voulez-vous r�g�n�rer toutes les prestations pour toutes les lignes de cette commande?",-2,"") UPDATE MAJPREST VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO.
    IF MAJPREST THEN DO:
        RUN maj-prest_tx (ROWID(entetcli)).
        MESSAGE Traduction("Voulez-vous lancer le recalcul des prix conso. ?",-2,"") UPDATE MAJPREST VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO.
        IF MAJPREST THEN RUN majpvc_tx (ROWID(entetcli)).
    END.
END.
IF lgtextile THEN DO:
    IF LoadSettings("saicl2_tx", OUTPUT hSettingsApi) THEN DO:
        ASSIGN logbid=SaveSetting("tremise1" ,"no")
               logbid=SaveSetting("tremise2" ,"no")
               logbid=SaveSetting("tremise3" ,"no")
               logbid=SaveSetting("tremise4" ,"no")
               logbid=SaveSetting("Val Rem1" ,"0")
               logbid=SaveSetting("Val Rem2" ,"0")
               logbid=SaveSetting("Val Rem3" ,"0")
               logbid=SaveSetting("Val Rem4" ,"0") /*$1798...*/
               logbid=SaveSettingLog("remEnVal1" ,no)
               logbid=SaveSettingLog("remEnVal2" ,no)
               logbid=SaveSettingLog("remEnVal3" ,no)
               logbid=SaveSettingLog("remEnVal4" ,no). /*...$1798*/
        UnloadSettings(hSettingsApi).
    end.
END.
/* ...$TX*/

/*$A38645...*/
/* ne pas faire appel au sp� si on valide � partir du catalogue */
IF vchar="" AND bcataloguePressed THEN DO :
    bcataloguePressed = NO.
    return.
END.
/*...$A38645*/

IF v-rapide = "oui" AND porigine <> "2" AND porigine <> "6b" THEN RUN LockWindowUpdate IN G-HPROWIN (0). /*$872*/
/* APPEL SPECIFIQUE */
vchar="".
PgmSpec = RechPgmSpeGco("s-saicl1.r").
IF pgmSpec <> ? THEN
    RUN VALUE(pgmSpec) (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,"1",output vchar).
if vchar<>"" then /* Abandon */ do transaction:
    /* Les 2 lignes suivantes sont indispensables si lancement d'une window d�finie en suppress window */
    CURRENT-WINDOW = wsaicl1.
    APPLY "entry" TO wsaicl1.
    /* Affichage entete */
    find entetcli where rowid(entetcli)=svrowid exclusive-lock no-wait no-error.
    run envoi-chapitre.
end.
/* FIN APPEL SPECIFIQUE */
else do:
    RUN envoi-title.

    /* Saisie code barre */
    if creation and codebarre and porigine<>"2" then do:
        logbid=no.
        run codqte_o (entetcli.depot,IF AVAIL client THEN ROWID(client) ELSE ?,rowid(entetcli),input-output logbid).
        if logbid then run crtlig_o (rowid(entetcli)). /* Cr�ation lignes */
    end.
    RUN Ecran2_NonOk.
    vchar="".
    /* Les 2 lignes suivantes sont indispensables si lancement d'une window d�finie en suppress window */
    CURRENT-WINDOW = wsaicl1.
    APPLY "entry" TO wsaicl1.

    /* R�f�rencement,dynamique,cadencier */
    if creation and porigine<>"2" AND ((parsoc.ges_cad and vapp3="yes")
                                       OR (parsoc.ges_refc and vapp1="yes")
                                       OR vapp2="yes") then do:
        RUN derlig_c (no,OUTPUT derlig,ROWID (entetcli)).
        run VALUE(IF vapp1="yes" THEN "sairef_c"
                  ELSE IF vapp3="yes" THEN "saicad_c"
                  ELSE "saidyn_c") (rowid(entetcli),1,input-output derlig,output svrowid-lig,output vchar).

        /* $1493 ... */
        RUN Mt_cdecl (ROWID(entetcli), OUTPUT TOTAL-POID, OUTPUT TOTAL-VOLUME, OUTPUT TOTAL-COL, OUTPUT TOTAL-PAL,
                      OUTPUT TOTAL-HEURE, OUTPUT TOTAL-HT, OUTPUT TOTAL-TTC, OUTPUT TOTAL-ML /*$A57952*/, OUTPUT TOTAL-HA, OUTPUT encdb).
        IF type-saisie = "C" THEN DO:
            ASSIGN buf-ty_mini = "VA":U buf-bl_mini = NO buf-mt_mini = 0.
            FOR FIRST client WHERE client.cod_cli = entetcli.cod_cli NO-LOCK:
                IF client.mt_mini <> 0 THEN ASSIGN buf-ty_mini = client.ty_mini buf-bl_mini = client.bl_mini buf-mt_mini = client.mt_mini.
                ELSE FOR FIRST typelem WHERE typelem.typ_fich = "C" AND typelem.typ_elem = entetcli.typ_elem NO-LOCK:
                    ASSIGN buf-ty_mini = typelem.ty_mini buf-bl_mini = typelem.bl_mini buf-mt_mini = typelem.mt_mini.
                END.
            END.
        END.
        ELSE ASSIGN buf-ty_mini = "VA":U buf-bl_mini = NO buf-mt_mini = 0.
        buf-pmini = NO.
        IF buf-mt_mini <> 0 THEN DO:
            CASE buf-ty_mini:
                WHEN "VA":U THEN IF TOTAL-HT < buf-mt_mini   THEN buf-pmini = YES. ELSE buf-pmini = NO.
                WHEN "CO" THEN IF TOTAL-COL < buf-mt_mini  THEN buf-pmini = YES. ELSE buf-pmini = NO.
                WHEN "PA":U THEN IF TOTAL-PAL < buf-mt_mini  THEN buf-pmini = YES. ELSE buf-pmini = NO.
                WHEN "PO":U THEN IF TOTAL-POID < buf-mt_mini THEN buf-pmini = YES. ELSE buf-pmini = NO.
            END CASE.
        END.
        logbid = YES.
        IF porigine<>'5' AND buf-pmini THEN DO:
            IF buf-bl_mini THEN DO:
                MESSAGE Traduction("Validation impossible",-2,"") + "." + Traduction("Le minimum de commande n'est pas atteint !",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                logbid = NO.
            END.
            ELSE MESSAGE Traduction("Le minimum de commande n'est pas atteint !",-2,"") SKIP Traduction("Voulez-vous valider ?",-2,"") VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
        END.
        IF NOT logbid THEN vchar="".
        /* ... $1493 */

        if vchar="1" then vchar="".
        if vchar="2" then do: /* Fin de commande dans R�f�rencement,dynamique,cadencier */
            CURRENT-WINDOW = wsaicl1.
            IF v-rapide = "oui" AND porigine <> "2" AND porigine <> "6b" THEN DO:
                run maj-fin1.
                run maj-fin2.
            END.
            ELSE DO:
                run saicl3_c (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,child-info,porigine,"",output retour).
                if retour then do:
                    vchar="".
                    DO-RESIZEWINDOW().
                END.
            END.
        end.
    end.

    /* $1105 ... */
    IF CONNECTED ("WORKFLOW") AND SEARCH(PropWF + "\execut_wf.r") <> ? THEN DO:
        IF NOT CREATION AND AVAIL wecli THEN DO:
            wlist-init = DYNAMIC-FUNCTION("BufferCompare" IN g-hprowin, BUFFER entetcli:HANDLE, BUFFER wecli:HANDLE, "").
            pretwf = "".
            DO X=1 TO NUM-ENTRIES(wlist-init):
                pretwf = pretwf + (IF pretwf = "" THEN "" ELSE ",") + "GCO.entetcli." + ENTRY(X,wlist-init).
            END.
        END.
        ELSE pretwf = "".
        RUN VALUE (PropWF + "\execut_wf") (NO, 0, "PROGIWIN", "*SAISIE VENTE", YES, "GCO.entetcli" + CHR(1) + STRING(ROWID(entetcli)) + CHR(1) + STRING(TEMP-TABLE wecli:DEFAULT-BUFFER-HANDLE), IF creation THEN "C" ELSE pretwf, "", OUTPUT pretwf).
    END.
    /* ... $1105 */
    /* Appel lignes commandes */
    IF vchar<>"" AND porigine="4" THEN DO:
        RUN cr-plan.
        IF retour THEN vchar="".
    END.
    if vchar="" then do:
        IF VALID-HANDLE(hpostit) then RUN PostitHide IN hpostit (1).
        IF VALID-HANDLE(hpostit) then RUN PostitHide IN hpostit (2).
        /* Les 2 lignes suivantes sont indispensables si lancement d'une window d�finie en suppress window */
        CURRENT-WINDOW = wsaicl1.
        APPLY "entry" TO wsaicl1.
        IF v-rapide = "oui" AND porigine <> "2" /*AND porigine <> "6b" $1750 */ THEN DO:
            IF porigine = "6b" THEN DO:
                RUN LockWindowUpdate IN G-HPROWIN (0).
                SESSION:SET-WAIT-STATE("").
                view {&WINDOW-NAME}.
            END.
            /* $A40589... */
            IF etudrapid AND CAN-DO("D,C",entetcli.typ_sai) THEN RUN saietud_c(entetcli.cod_cli, entetcli.typ_sai, entetcli.no_cde, entetcli.no_bl, ?,OUTPUT logbid).
            ELSE /* ...$A40589 */ RUN saicli_c (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai + (IF creation THEN "O" ELSE "N"),nopt,porigine,OUTPUT retour).
        END. /* $1750 */
        ELSE IF porigine <> "LOC":U THEN DO: /* $GC */ /*$LOC*/
            passe = NO. /* $A38744 */
            IF loadsettings("SAIETUD", output hsettingsapi) then do:
                FIND FIRST ateent OF entetcli  NO-LOCK NO-ERROR.
                /*$A74998... IF CAN-DO("D,C",entetcli.typ_sai) /* $A40589 */ AND NOT AVAIL ateent AND LOGICAL(GetSetting("SETUDEDEF","NO")) THEN DO: /* $A38744 */*/
                vsetudedef = LOGICAL(GetSetting("SETUDEDEF","NO")).
                IF (entetcli.typ_sai = "D" AND NOT AVAIL ateent AND vsetudedef)
                OR (entetcli.typ_sai = "C" AND NOT AVAIL ateent AND LOGICAL(GetSetting("SETUDCDEF",STRING(vsetudedef,"YES/NO")))) THEN DO:
                /*...$A74998*/
                    RUN saietud_c(entetcli.cod_cli, entetcli.typ_sai, entetcli.no_cde, entetcli.no_bl,?,OUTPUT logbid).
                    assign cm1 = entetcli.coef_mar[1] cm2 = entetcli.coef_mar[2] cm3 = entetcli.coef_mar[3]
                           cm4 = entetcli.coef_mar[4] cm5 = entetcli.coef_mar[5] cc1 = entetcli.cal_marg.
                    passe = YES.
                END.
                UnloadSettings(hSettingsApi).
            END.
            IF NOT passe THEN DO:
                /* $A38744 */
                /*run saicl3_c (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,child-info,porigine,"",output retour).
 *             ELSE $A38744 */

                /*$A59696...*/
                IF creation AND entetcli.location AND entetcli.typ_sai = "D" THEN DO TRANSACTION:
                    FIND FIRST lignecli WHERE lignecli.typ_sai = entetcli.typ_sai and
                                              lignecli.cod_cli = entetcli.cod_cli and
                                              lignecli.no_cde = entetcli.no_cde and
                                              lignecli.sous_type = "PH" and
                                              lignecli.location = yes exclusive-lock no-error.
                    IF NOT AVAIL(lignecli) THEN DO:
                        create lignecli.
                        assign lignecli.no_ligne=(if parcde2.gst_zone[4] then 1 else 25)
                               lignecli.NO_unique = NEXT-VALUE(compteur_lignecli) /*$2093*/
                               lignecli.typ_sai=entetcli.typ_sai
                               lignecli.cod_cli=entetcli.cod_cli
                               lignecli.no_cde=entetcli.no_cde
                               lignecli.no_bl=entetcli.no_bl
                               lignecli.location = yes
                               lignecli.typ_cdef=1
                               lignecli.typ_elem="PH"
                               lignecli.sous_type="PH"
                               lignecli.div_fac=0
                               lignecli.ndos = entetcli.ndos
                               lignecli.niveau1=1
                               lignecli.total=YES
                               lignecli.tot_txt=NO
                               lignecli.heure=NO
                               lignecli.zal[1]="1"
                               lignecli.zal[2]="3".
                    END.
                    IF NOT AVAIL(tarif) OR tarif.no_tarif <> entetcli.no_tarif THEN FIND FIRST tarif WHERE tarif.NO_tarif = entetcli.no_tarif NO-LOCK NO-ERROR.
                    aUnit = "MOIS":U.
                    IF AVAIL(tarif) THEN CASE tarif.uni_tps:
                        WHEN "J" THEN ASSIGN aUnit = "JOU":U.
                        WHEN "S" THEN ASSIGN aUnit = "SEM":U.
                        WHEN "M" THEN ASSIGN aUnit = "MOIS":U.
                    END.
                    ASSIGN lignecli.commerc[1]=entetcli.commerc[1]
                           lignecli.commerc[2]=entetcli.commerc[2]
                           lignecli.app_aff=entetcli.app_aff
                           lignecli.depot=entetcli.depot
                           lignecli.nom_pro=subst(Traduction("LOCATION EN NOMBRE DE &1",-2,""),entry(lookup(aUnit,"JOU,SEM,MOIS":U),Traduction("JOURS",-2,"") + "," + Traduction("SEMAINES",-2,"") + "," + Traduction("MOIS",-2,"")))
    /*                       lignecli.typ_pha=ph-typ */
                           lignecli.lib_univ=aUnit
                           /*lignecli.qte=max(INTERVAL(datetime(entetcli.fin_loc),datetime(entetcli.deb_loc),entry(lookup(aUnit,"JOU,SEM,MOIS":U),"days,weeks,months":U)),1).*/
                           lignecli.qte = IF aUnit = "JOU" THEN MAX(INTERVAL(datetime(entetcli.fin_loc),datetime(entetcli.deb_loc), "days":U),1) ELSE IF aUnit = "SEM" THEN MAX(INTERVAL(datetime(entetcli.fin_loc),datetime(entetcli.deb_loc), "weeks":U),1)  ELSE MAX((INTERVAL(ADD-INTERVAL(datetime(entetcli.fin_loc), 1, "DAY") ,datetime(entetcli.deb_loc),"months":U)),1). /*$A124140*/
                    VALIDATE lignecli.
                    FIND CURRENT lignecli NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
                END. /********** Fin cr�ation ************/
                /* ...$A59696 */

                run saicl2_c (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,child-info,porigine,"E",output retour).
            END.
        END.
        ELSE run saicl3_c (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,child-info,porigine,"",output retour). /*$LOC*/

        DO-RESIZEWINDOW().
        ASSIGN CURRENT-WINDOW = wsaicl1
               {&WINDOW-NAME}:title= ltype-saisie.
        IF NOT retour AND porigine="4" THEN RUN cr-plan.
        if retour then do transaction:
            /* Affichage entete */
            find entetcli where rowid(entetcli)=svrowid exclusive-lock no-wait no-error.
            RUN Ecran2_Ok.
            Logbid = CBEnableItem (Hcombars-fentete, 2, {&brecalcul}, YES).
            Logbid = CBEnableItem (hComBars-fentete, 2, {&Btablibre}, YES). /*$A67777*/
            if NOT CAN-DO("C,D",entetcli.typ_sai) OR (AVAIL pargp AND pargp.prealable) then do:
                find first lignecli of entetcli where lignecli.sous_type="AR" no-lock no-error.
                if available lignecli /*$A50782...*/ AND entetcli.af_fin = "" /*...$A50782*/ THEN disable edepot WITH FRAME fc1.
            end.
            PgmSpec = RechPgmSpeGco("s-saicl1.r").
            IF pgmSpec <> ? THEN
                RUN specif-entete.
            run envoi-chapitre.
        end.
        else if can-do("2,4,6a,6b,LOC":U,porigine) then apply "close" to this-procedure.
        else run active-f1.
    end.
    else if porigine="4" OR porigine = "6a" then apply "close" to this-procedure.
    else run active-f1.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete-suite1 wsaicl1
PROCEDURE maj-entete-suite1 :
CREATE ateent.
ASSIGN
    ateent.cod_cli = entetcli.cod_cli
    ateent.typ_sai = entetcli.typ_sai
    ateent.NO_cde = entetcli.NO_cde
    ateent.NO_bl = entetcli.NO_bl
    ateent.materiel = ori_mat
    ateent.typ_fich = ori_parc
    ateent.qui = entetcli.qui.
FIND FIRST materiel WHERE materiel.typ_fich = ori_parc AND materiel.materiel = ori_mat NO-LOCK NO-ERROR.
IF AVAIL materiel THEN ASSIGN
    ateent.kilom = materiel.der_km
    ateent.dat_ent = materiel.der_ent
    ateent.dat_sor = materiel.der_sor
    ateent.dat_vp = materiel.dat_vp
    ateent.dat_pm = materiel.dat_pm
    /* $A42093 ... */
    ateent.zal = materiel.zal
    ateent.zda = materiel.zda
    ateent.zlo = materiel.zlo
    ateent.znu = materiel.znu
    ateent.zta = materiel.zta
    /* ... $A42093 */.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete-suite2 wsaicl1
PROCEDURE maj-entete-suite2 :
DO WITH FRAME fc1:
    entetcli.dat_dep = (IF ((creation and modif-entete) or not creation) AND entetcli.typ_sai="C" AND entetcli.dat_dep=? AND INPUT entetcli.dat_liv > entetcli.dat_liv THEN entetcli.dat_liv
                        ELSE IF ((creation and modif-entete) or not creation) AND entetcli.typ_sai="C" AND entetcli.dat_dep<>? AND INPUT entetcli.dat_liv < entetcli.dat_dep THEN ?
                        ELSE entetcli.dat_dep).

    ASSIGN frame fc1
        entetcli.dat_liv entetcli.semaine entetcli.dat_cde entetcli.dat_px entetcli.ref_cde entetcli.not_ref
        entetcli.civ_fac adr_fac[1] adr_fac[2] adr_fac[3] adrfac4 k_post2f
        villef paysf entetcli.devise entetcli.langue type_fac entetcli.code_reg entetcli.code_ech fac_dvs ori_cde entetcli.cal_marg
        entetcli.rem_glo[1] entetcli.rem_glo[2] entetcli.groupe
        entetcli.famille entetcli.s2_famille entetcli.s3_famille entetcli.s4_famille entetcli.region commerc[1] app_aff entetcli.regime nat_cde entetcli.canal entetcli.affaire
        /*$A35969...*/ entetcli.prix_franco /*...$A35969*/ entetcli.location /*$A77503*/ entetcli.deb_loc entetcli.fin_loc entetcli.dur_loc.

    ASSIGN
        entetcli.typ_fich   = vTypFich
        entetcli.materiel   = vMateriel
        entetcli.no_ordre_m = vNoOrdre. /*$A41246*/ /* $2215 */

    /* $2086 ... */
    IF AVAIL parsoc AND parsoc.affaire AND entetcli.affaire <> "" THEN DO:
        FIND FIRST affpar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
        IF AVAIL affpar AND affpar.type_fac = "S" THEN DO:
            IF SUBSTR (entetcli.type_fac,1,1) = "R" THEN DO:
                DEF VAR tmpch AS CHAR NO-UNDO.
                tmpch = "S" + SUBSTR (entetcli.type_fac,2,1).
                entetcli.type_fac = tmpch.
            END.
        END.
    END.
    /* ... $2086 */

    /* M�j dates */
    ASSIGN
        entetcli.dat_livd = wdat_livd
        entetcli.dat_acc = wdat_acc
        entetcli.dat_rll = wdat_rll
        entetcli.dat_mad = wdat_mad
        entetcli.dat_rec = wdat_rec
        entetcli.dat_vald = wdat_valid
        entetcli.hr_rec = whr_rec.
    IF entetcli.typ_sai = "D" THEN DO:
        IF AVAIL parcde AND parcde.typ_fich = "C" AND parcde.gst_num[8] <> 0 AND entetcli.dat_dev = ? THEN entetcli.dat_dev = TODAY + parcde.gst_num[8]. /* $2069 */ /*$A69045*/
        FIND FIRST ateent OF entetcli SHARE-LOCK NO-ERROR.
        IF AVAIL ateent THEN ASSIGN
            ateent.dat_cde = entetcli.dat_cde
            ateent.usr_mod = entetcli.usr_mod
            ateent.ref_cde = entetcli.ref_cde
            ateent.NOT_ref = entetcli.ref_cde.
        FIND CURRENT ateent no-lock no-error. /* �XREF_NOWHERE� */
    END.
    /*$724*/
    IF fac_dvs=NO THEN entetcli.devise=g-dftdev.

    ASSIGN
        entetcli.no_info = local_no_info
        entetcli.achev = no
        entetcli.dat_mod = today
        tx_ech_d = (if fac_dvs then dec(tx_ech_d:screen-value) else 0)
        entetcli.coef_mar[1] = cm1
        entetcli.coef_mar[2] = cm2
        entetcli.coef_mar[3] = cm3
        entetcli.coef_mar[4] = cm4
        entetcli.coef_mar[5] = cm5
/*$975*/ entetcli.COL_vis = (IF pcol=? THEN 0 ELSE pcol)
        no_tarif = if not parsoc.tar_col then 1 else int(no_tarif:screen-value)
        entetcli.usr_mod = IF entetcli.typ_sai<>"A" THEN g-user ELSE Traduction("Abonnement",-2,"")
        entetcli.depot=INT(edepot:SCREEN-VAL IN FRAME fc1).

    IF NOT creation AND parcde.gst_zone[33] AND tConsign:CHECKED THEN
        entetcli.dep_csg= depotcsg. /*$1130*/

    /* $2059 ... */
    ASSIGN entetcli.det_frais = STRING(frais1-prc1) + CHR(2)
                            + STRING(frais1-prc2) + CHR(2)
                            + STRING(frais1-prc3) + CHR(2)
                            + STRING(frais1-prc4)
                            + CHR(1)
                            + STRING(frais2-prc1) + CHR(2)
                            + STRING(frais2-prc2) + CHR(2)
                            + STRING(frais2-prc3) + CHR(2)
                            + STRING(frais2-prc4)
                            + CHR(1)
                            + STRING(frais3-prc1) + CHR(2)
                            + STRING(frais3-prc2) + CHR(2)
                            + STRING(frais3-prc3) + CHR(2)
                            + STRING(frais3-prc4)
                            + CHR(1)
                            + STRING(frais4-prc1) + CHR(2)
                            + STRING(frais4-prc2) + CHR(2)
                            + STRING(frais4-prc3) + CHR(2)
                            + STRING(frais4-prc4)
                            + CHR(1)
                            + STRING(frais5-prc1) + CHR(2)
                            + STRING(frais5-prc2) + CHR(2)
                            + STRING(frais5-prc3) + CHR(2)
                            + STRING(frais5-prc4).
    /* ... $2059 */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete-suite3 wsaicl1
PROCEDURE maj-entete-suite3 :
DO WITH FRAME fc2:
    ASSIGN frame fc2
        entetcli.num_tel entetcli.num_fax entetcli.edt_arc entetcli.liv_cpl entetcli.cde_cpl entetcli.prep_isol entetcli.proforma entetcli.chif_bl
        entetcli.fac_ttc entetcli.fac_pxa entetcli.maj_ach entetcli.fra_app entetcli.taux_esc entetcli.cat_tar
        entetcli.liasse entetcli.dat_ech entetcli.commerc[2] entetcli.cpt_bq entetcli.plus_bpbl entetcli.bl_regr
        entetcli.tar_fil[1] entetcli.tar_fil[2] entetcli.tar_fil[3] entetcli.tar_fil[4] entetcli.tar_fil[5] entetcli.tar_fil[6]
        entetcli.tar_cum[1] entetcli.tar_cum[2] entetcli.tar_cum[3] entetcli.tar_cum[4] entetcli.tar_cum[5] entetcli.tar_cum[6]
        entetcli.cat_cum /* $A59971 ... */ entetcli.lias_bl /* ... $A59971 */
        entetcli.tva_date.
    ASSIGN
        entetcli.borne=if not parsoc.rem_vte then 0 else int(entetcli.borne:screen-value)
        entetcli.cl_stat=integer(vcl_stat:screen-value)
        entetcli.cl_plreg=integer(vcl_plr:screen-value)
        entetcli.cl_plnat=integer(vcl_pln:screen-value)
        entetcli.cl_grp=integer(vcl_grp:screen-value)
        entetcli.cl_livre=integer(vcl_livre:screen-value)
        entetcli.cl_fact=integer(vcl_fact:screen-value)
        entetcli.cl_paye=integer(vcl_paye:screen-value)
        entetcli.c_factor=integer(vc_factor:screen-value)
        entetcli.contrat = (IF NOT toggle-contrat:HIDDEN AND toggle-contrat:CHECKED
                            THEN fill-contratloc:SCREEN-VAL
                            ELSE IF NOT toggle-contrat:HIDDEN AND NOT toggle-contrat:CHECKED THEN "" /*$A50403*/
                            ELSE entetcli.contrat) /* $1727 */
        entetcli.ind_con = (IF NOT toggle-contrat:HIDDEN AND toggle-contrat:CHECKED
                            THEN INT(fill-contratloc:PRIVATE-DATA)
                            ELSE IF NOT toggle-contrat:HIDDEN AND NOT toggle-contrat:CHECKED THEN 0 /*$A50403*/
                            ELSE entetcli.ind_con) /* $1727 */
        entetcli.location = entetcli.location. /*(IF NOT toggle-contrat:HIDDEN AND toggle-contrat:CHECKED AND vTypeContrat = "LOC":U THEN YES ELSE (parsoc.ges_loc /*AND type-saisie = "D" $A50403*/ AND entetcli.location:CHECKED IN FRAME fc1)) /* $A33727 */.        */ /*$A62169*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete-suite4 wsaicl1
PROCEDURE maj-entete-suite4 :
DO WITH FRAME fc2:
    IF not modif-entete THEN DO:
        ASSIGN
            entetcli.zon_lib  = client.zon_lib
            entetcli.iden_ce  = cptcli.idcee
            entetcli.edi_rlv  = (cptcli.edi_rlv<>"N")
            entetcli.transpor = client.transpor
            entetcli.mod_liv  = client.mod_liv
            entetcli.zon_geo  = client.zon_geo
            entetcli.typ_elem = client.typ_elem
            entetcli.zone_exp = client.zone_exp
            entetcli.typ_veh  = client.typ_veh /*$A35969*/
            entetcli.typ_con  = client.typ_con
            entetcli.edt_bp   = if type-saisie<>"R" then client.edt_bp else ""
            svrowid           = rowid(entetcli)
            entetcli.mnt_rlk  = client.mnt_rlk[typ-cde]
            entetcli.inc_deb  = client.inc_deb
            entetcli.trs_deb  = client.trs_deb.
/*$1728...D�sormais g�r� dans entetcl4.i
         find tabgco where tabgco.type_tab="ML" and a_tab=client.mod_liv no-lock no-error.
        if available tabgco then entetcli.port=tabgco.code_df.
...$1728*/

        /* Si onglet 'divers' non g�r� --> MAJ selon fichier client */
        if not parcde.gst_zone[1] then assign
            entetcli.edt_arc=client.edt_arc
            entetcli.liv_cpl=client.liv_cpl
            entetcli.cde_cpl=client.cde_cpl
            entetcli.prep_isol=client.prep_isol
            entetcli.bl_regr=client.bl_regr
            entetcli.plus_bpbl=client.plus_bpbl
            entetcli.num_tel=notel
            entetcli.num_fax=nofax
            entetcli.proforma=client.proforma
            entetcli.chif_bl=client.chif_bl
            entetcli.fac_ttc=client.fac_ttc
            entetcli.fac_pxa=client.fac_pxa
            entetcli.maj_ach=client.maj_ach
            entetcli.fra_app=client.fra_app
            entetcli.taux_esc=cptcli.taux_esc
            entetcli.cat_tar=client.cat_tar
            entetcli.liasse=client.liasse
            entetcli.lias_bl=client.lias_bl /* $A59971 */
            entetcli.borne=client.borne
            entetcli.no_tarif=client.no_tarif
            entetcli.cl_stat=client.cl_stat
            entetcli.cl_plreg=client.cl_plreg
            entetcli.cl_plnat=client.cl_plnat
            entetcli.cl_grp=client.cl_grp
            entetcli.cl_livre=client.cl_livre
            entetcli.cl_fact=client.cl_fact
            entetcli.cl_paye=client.cl_paye
            entetcli.c_factor=client.c_factor
            entetcli.tar_fil[1]=client.tar_fil[1]
            entetcli.tar_fil[2]=client.tar_fil[2]
            entetcli.tar_fil[3]=client.tar_fil[3]
            entetcli.tar_fil[4]=client.tar_fil[4]
            entetcli.tar_fil[5]=client.tar_fil[5]
            entetcli.tar_fil[6]=client.tar_fil[6]
            entetcli.tar_cum[1]=client.tar_cum[1]
            entetcli.tar_cum[2]=client.tar_cum[2]
            entetcli.tar_cum[3]=client.tar_cum[3]
            entetcli.tar_cum[4]=client.tar_cum[4]
            entetcli.tar_cum[5]=client.tar_cum[5]
            entetcli.tar_cum[6]=client.tar_cum[6]
            entetcli.cat_cum=client.cat_cum.

        /*$A60003...
        {entetcl9.i entetcli} /* Adresse commande */
        */
        IF NOT adr_fac2cde THEN DO:
            {entetcl9.i entetcli} /* Adresse commande */
        END.
        ELSE ASSIGN
            entetcli.civ_cde = entetcli.civ_fac
            entetcli.adr_cde[1] = entetcli.adr_fac[1]
            entetcli.adr_cde[2] = entetcli.adr_fac[2]
            entetcli.adr_cde[3] = entetcli.adr_fac[3]
            entetcli.adr_cde[4] = entetcli.adrfac4
            entetcli.k_post2c = entetcli.k_post2f
            entetcli.villec = entetcli.villef
            entetcli.paysc = entetcli.paysf.
        /*...$A60003*/

        {entetcl4.i entetcli 'O'} /* Adresse livraison */

        IF pref_parcde_transpor THEN DO:
            FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = entetcli.depot NO-LOCK NO-ERROR.
            IF AVAIL depcli AND depcli.transpor > 0 THEN entetcli.transpor = depcli.transpor.
        END.
        IF parsoc.ges_tc AND parcde.gst_zone[74] AND type-saisie = "C" /*CAN-DO("C,D",type-saisie)*/ THEN RUN creation-tournee.

    END.
    IF AVAIL client AND depass > 0 AND CAN-DO("C,D",type-cours) THEN entetcli.Blocage = client.BLOK_enc.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-entete-suite5 wsaicl1
PROCEDURE maj-entete-suite5 :
/*$1728...*/
IF creation THEN DO :

    /*$A35969
    DEF VAR typcat AS LOG NO-UNDO. /*$ED*/
    IF parsoc.ges_car THEN DO: /* $1483 */
        FIND FIRST carpar NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
        IF AVAIL carpar AND entetcli.nat_cde=carpar.nature THEN ASSIGN typcat = YES.
    END.*/

    logbid = YES.
    IF entetcli.transpor<>0 THEN
        /* recherhe Transporteur + Client */
        FOR FIRST transpor FIELDS(transpor.coeff_fac)
                           WHERE transpor.achat = NO
                             AND transpor.cod_fou = entetcli.transpor
                             AND transpor.cod_cli = entetcli.cod_cli
                             AND transpor.typ_con = entetcli.typ_con
                             AND transpor.depot = 0
                             AND transpor.typ_cat = entetcli.typ_veh /*$A35969 typcat*/ NO-LOCK :
            IF transpor.coeff_fac<>0 THEN ASSIGN logbid = NO entetcli.marge_p = transpor.coeff_fac.
        END.

    /* recherche Tous transporteurs + Client */
    IF logbid THEN
        FOR FIRST transpor FIELDS(transpor.coeff_fac)
                           WHERE transpor.achat = NO
                             AND transpor.cod_fou = 0
                             AND transpor.cod_cli = entetcli.cod_cli
                             AND transpor.typ_con = entetcli.typ_con
                             AND transpor.depot = 0
                             AND transpor.typ_cat = entetcli.typ_veh /*$A35969 typcat*/ NO-LOCK :
            IF transpor.coeff_fac<>0 THEN ASSIGN logbid = NO entetcli.marge_p = transpor.coeff_fac.
        END.

    IF logbid AND client.marge_p<>0 THEN ASSIGN logbid = NO entetcli.marge_p = client.marge_p.

    /* recherche Transporteur + tous clients */
    IF logbid THEN
        FOR FIRST transpor FIELDS(transpor.coeff_fac)
                           WHERE transpor.achat = NO
                             AND transpor.cod_fou = entetcli.transpor
                             AND transpor.cod_cli = 0
                             AND transpor.typ_con = entetcli.typ_con
                             AND transpor.depot = 0
                             AND transpor.typ_cat = entetcli.typ_veh /*$A35969 typcat*/ NO-LOCK :
            IF transpor.coeff_fac<>0 THEN ASSIGN logbid = NO entetcli.marge_p = transpor.coeff_fac.
        END.

    /* recherche Tous transporteurs + Tous clients */
    IF logbid THEN
        FOR FIRST transpor FIELDS(transpor.coeff_fac)
                           WHERE transpor.achat = NO
                             AND transpor.cod_fou = 0
                             AND transpor.cod_cli = 0
                             AND transpor.typ_con = entetcli.typ_con
                             AND transpor.depot = 0
                             AND transpor.typ_cat = entetcli.typ_veh /*$A35969 typcat*/ NO-LOCK :
            entetcli.marge_p = transpor.coeff_fac.
        END.
END.
/*...$1728*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-fin1 wsaicl1
PROCEDURE maj-fin1 :
RUN mt_cdecl (ROWID(entetcli), OUTPUT TOTAL-POID, OUTPUT TOTAL-VOLUME, OUTPUT TOTAL-COL, OUTPUT TOTAL-PAL,
                  OUTPUT TOTAL-HEURE, OUTPUT TOTAL-HT, OUTPUT TOTAL-TTC, OUTPUT TOTAL-ML /*$A57952*/, OUTPUT TOTAL-HA, OUTPUT encdb).
    ASSIGN
        entetcli.dat_mod=today
        entetcli.usr_mod=g-user
        entetcli.achev=YES
        entetcli.poids   = total-poid
        entetcli.volume  = total-volume
        entetcli.colis   = total-col
        entetcli.palette = total-pal
        entetcli.metre_lin   = total-ml /*$A57952*/
        entetcli.mt_cde  = total-ttc
        entetcli.mt_ht   = total-ht
        entetcli.temps   = total-heure
        entetcli.mt_ini  = total-ht
        entetcli.poi_ini = total-poid.

    RUN maj_statut_pce.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-fin2 wsaicl1
PROCEDURE maj-fin2 :
/* M�j du montant HT de la commande dans planning appel/visite avant traitement livraison */
    IF entetcli.typ_sai="C" AND entetcli.nb_bp=0 THEN DO:
        FIND planav WHERE planav.cod_cli=entetcli.cod_cli AND planav.NO_cde=entetcli.NO_cde EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
        IF AVAIL planav THEN planav.mt_ht=entetcli.mt_ht.
    END.

    /* Test blocage blok_enc */
    IF CAN-DO("C,D",entetcli.typ_sai) AND AVAIL client AND client.BLOK_enc <> "" THEN DO:
        find cptcli where cptcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ cptcli.cod_cli=entetcli.cod_cli no-lock no-error.
        FIND CURRENT cptcli NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
        run enccli_c (cptcli.cod_cli,NO,NO,entetcli.typ_sai,OUTPUT depass,output logbid).
        IF depass > 0 THEN entetcli.Blocage = client.BLOK_enc.
    END.

    /*$B2 - si au moins 2 d�p�ts --> �clatement */
    IF entetcli.typ_sai = "C" AND
       (CAN-FIND(FIRST lignecli OF entetcli WHERE lignecli.dat_liv <> entetcli.dat_liv NO-LOCK) OR
        CAN-FIND(FIRST lignecli OF entetcli WHERE lignecli.depot <> entetcli.depot NO-LOCK)) THEN RUN eclcde_c (wsaicl1:TITLE, ROWID(entetcli), OUTPUT vchar).

    /* M�j cadencier */
    IF parcde.gst_zone[75] AND parsoc.ges_cad AND
        ((entetcli.typ_sai="F" AND entetcli.NO_bl<>0 AND porigine<>"2")
         OR (entetcli.typ_sai="C" AND porigine<>"7"))
        THEN RUN majcad_c (entetcli.cod_cli).

    /* Synchro multi-soci�t� */
    /* Gestion Magasin-Entrepot : la commande client cr��e la commande fournisseur
    sauf si c'est une commande fournisseur qui a cr��e cette commande client */
    IF entetcli.typ_sai="C" AND SUBSTR(entetcli.caution,1,2)<>"$$" THEN DO:
        FIND tabgco WHERE tabgco.TYPE_tab="DS" AND tabgco.cod_cli=entetcli.cod_cli NO-LOCK NO-ERROR.
        IF AVAIL tabgco THEN DO:
            FIND tabgco WHERE tabgco.TYPE_tab="DS" AND tabgco.n_tab=entetcli.depot NO-LOCK NO-ERROR.
            IF AVAIL tabgco AND tabgco.cod_fou<>0 THEN DO:
                FIND fournis where fournis.cod_fou=tabgco.cod_fou no-lock no-error.
                IF AVAIL fournis THEN run synchro_a (rowid(entetcli)).
            END.
        END.
    END.
    /* Gestion Magasin-Entrepot : la livraison client g�n�re la r�ception fournisseur */
    ELSE IF entetcli.typ_sai="F" /*AND SUBSTR(entetcli.caution,1,3)="$$I"*/ THEN DO:
        FIND entetfou WHERE entetfou.regr=entetcli.NO_cde AND entetfou.typ_sai="C" AND entetfou.retr_cess<>"" NO-LOCK NO-ERROR.
        IF AVAIL entetfou THEN DO:
            FIND tabgco WHERE tabgco.TYPE_tab="DS" AND tabgco.n_tab=entetfou.depot NO-LOCK NO-ERROR.
            IF AVAIL tabgco THEN DO:
                IF NOT entetcli.st_interne THEN DO:
                    X=1.
                    IF LoadSettings("SAICL", OUTPUT hSettingsApi) THEN DO:
                        X = INT(GetSetting("RSynchro","1")).
                        UnloadSettings(hSettingsApi).
                    END.
                    IF X <> 0 THEN
                        run synchro_r (rowid(entetcli),X).
                END.
                ELSE run synchro_r (rowid(entetcli),3).
            END.
        END.
    END.

    /* Ev�nements */
    IF CAN-FIND(FIRST evenmsg WHERE evenmsg.actif no-lock) THEN RUN msgsai_o ("C",ROWID(entetcli),entetcli.mt_cde).

    /* $1105 ... */
    IF CONNECTED ("WORKFLOW") AND SEARCH(PropWF + "\execut_wf.r") <> ? THEN DO:
        IF AVAIL wecli THEN DO:
            ASSIGN pretwf = "" wlist-init = "".
            wlist-init = DYNAMIC-FUNCTION("BufferCompare" IN g-hprowin, BUFFER entetcli:HANDLE, BUFFER wecli:HANDLE, "").
            DO X=1 TO NUM-ENTRIES(wlist-init):
                pretwf = pretwf + (IF pretwf = "" THEN "" ELSE ",") + "GCO.entetcli." + ENTRY(X,wlist-init).
            END.
            RUN VALUE (PropWF + "\execut_wf") (NO, 0, "PROGIWIN", "*SAISIE VENTE", YES,"GCO.entetcli" + CHR(1) + STRING(ROWID(entetcli)) + CHR(1) + STRING(TEMP-TABLE wecli:DEFAULT-BUFFER-HANDLE), pretwf, "", OUTPUT pretwf).
        END.
    END.
    /* ... $1105 */
    SESSION:SET-WAIT("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-tournee wsaicl1
PROCEDURE maj-tournee :
DEFINE VARIABLE svcodtou AS CHAR NO-UNDO.
    DEFINE VARIABLE svord AS INTEGER    NO-UNDO.
    DEFINE VARIABLE wno_adr AS INTEGER    NO-UNDO.
    DEFINE VARIABLE svhr AS INTEGER    NO-UNDO.
    DEFINE VARIABLE svdatliv AS DATE NO-UNDO.
    DEFINE VARIABLE logbid AS LOGICAL    NO-UNDO.

    DEFINE BUFFER badresse FOR adresse .

    run saitou_c (entetcli.cod_cli,ROWID(entetcli),"","",entetcli.depot /* $1110 */ ,entetcli.dat_cde, output logbid).
    if logbid THEN DO:
        ASSIGN
            entetcli.ref_cde =getsessiondataex("Tournee":U, "Ref":U,"")
            entetcli.NOt_ref=getsessiondataex("Tournee":U, "Not","")
            entetcli.civ_liv=getsessiondataex("Tournee", "Civ","")
            entetcli.adr_liv[1]=getsessiondata("Tournee", "A1")
            entetcli.adr_liv[2]=getsessiondataex("Tournee", "A2","")
            entetcli.adr_liv[3]=getsessiondataex("Tournee", "A3","")
            entetcli.adrliv4=getsessiondataex("Tournee", "A4","")
            entetcli.k_post2l=getsessiondataex("Tournee", "Cp","")
            entetcli.villel=getsessiondata("Tournee", "Vil")
            entetcli.paysl=getsessiondata("Tournee", "Pay")
            svcodtou=getsessiondata("Tournee":U, "Tou")
            svord=getsessionInt("Tournee":U, "Ord",0)
            wno_adr=getsessionInt("Tournee", "Adr",0)
            svhr=getsessionInt("Tournee":U, "Hr",0)
            svdatliv=getsessiondate("Tournee":U, "Dat", TODAY)
            /*$A35969...*/
            entetcli.zon_geo = getsessionInt("Tournee":U, "zon_geo",0)
            entetcli.typ_veh = getsessionLog("Tournee":U, "typ_veh",NO)
            entetcli.typ_con = getsessiondataex("Tournee":U, "typ_con","").
            /*...$A35969*/

        /*$1728...*/
        FOR FIRST badresse where badresse.typ_fich = "C"  and badresse.cod_tiers =(if entetcli.cl_livre=0 then entetcli.cod_cli else entetcli.cl_livre)
                             AND badresse.affaire="" and badresse.cod_adr = wno_adr NO-LOCK:
             IF badresse.mod_liv > "" THEN entetcli.MOD_liv = badresse.MOD_liv .
             IF badresse.transpor > 0 THEN entetcli.transpor = badresse.transpor.
/*             RUN aff-assisd.*/
        end.
        /*...$1728adresse*/
        IF svcodtou="CO" AND entetcli.depot<10 THEN DO:
            svcodtou=svcodtou + STRING(entetcli.depot,"9").
            FIND tabgco WHERE tabgco.type_tab="TO" AND tabgco.a_tab="CO" + STRING(entetcli.depot,"9") NO-LOCK NO-ERROR.
            if not available tabgco then do:
                CREATE tabgco.
                ASSIGN
                    tabgco.type_tab="TO"
                    tabgco.a_tab="CO" + STRING(entetcli.depot,"9")
                    tabgco.inti_tab=Traduction("Comptoir",-2,"") + " (" + Traduction("Pris sur place",-2,"") + ")".
            end.
        END.
        IF svcodtou=? THEN ASSIGN
            svcodtou=""
            svord=0.

        ASSIGN
            entetcli.k_postl  = wno_adr
            entetcli.hr_dep   = svhr
            entetcli.tournee  = svcodtou
            entetcli.NO_ordre = svord
            entetcli.dat_dep = (IF ((creation and modif-entete) or not creation) AND entetcli.typ_sai="C" AND entetcli.dat_dep=? AND svdatliv > entetcli.dat_liv THEN entetcli.dat_liv
                                ELSE IF ((creation and modif-entete) or not creation) AND entetcli.typ_sai="C" AND entetcli.dat_dep<>? AND svdatliv < entetcli.dat_dep THEN ?
                                ELSE entetcli.dat_dep)
            entetcli.dat_liv  = svdatliv.
    END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj_statut_pce wsaicl1
PROCEDURE maj_statut_pce :
{Statut_Pce_C.i entetcli}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modif-aff wsaicl1
PROCEDURE modif-aff :
DO with frame fc1:
  session:set-wait-state("GENERAL").
  parnum = int(input entetcli.affaire) no-error.
  if not error-status:error then numaff = string(int(input entetcli.affaire),"ZZZZZZZZZZZ").
                            else numaff = fill(" ",11 - length(input entetcli.affaire)) + input entetcli.affaire.
  find affaire where affaire.affaire = numaff no-lock no-error.
  if available affaire then do:
      frame Fc1:sensitive = no. 
      wsaicl1:SENSITIVE = NO. /*$A122103*/

      /* $930 ... */
      ASSIGN
          svrowid-lig = ROWID(affaire)
          numaff = "".
/*      RUN mntaff_af (2, INPUT-O svrowid-lig, int(entetcli.cod_cli:screen-value in frame Fonglet), date(entetcli.dat_cde:screen-value in frame fc1), INPUT-O numaff, OUTPUT lib-aff, OUTPUT retour, ?, 0, "").*/
      RUN mntaff_af (2, INPUT-O svrowid-lig, int(entetcli.cod_cli:screen-value in frame Fonglet), date(entetcli.dat_cde:screen-value in frame fc1), INPUT-O numaff, OUTPUT lib-aff, OUTPUT retour, ?, 0, "",? /*$1782*/).
      /* ... $930 */
      if retour then do:
        entetcli.affaire:screen-value = numaff.
        display lib-aff.
      end.
      frame Fc1:sensitive = yes.
      wsaicl1:SENSITIVE = YES. /*$A122103*/
  end.
  else message Traduction("Impossible de modifier une affaire qui n'a pas encore �t� cr��e.",-2,"") view-as alert-box error.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ObtenirNomCalPx wsaicl1
PROCEDURE ObtenirNomCalPx :
DEF INPUT-OUTPUT PARAMETER atelier_sur_cde AS LOG no-undo.
    DEF INPUT-OUTPUT PARAMETER chemin_calpx_t AS CHAR no-undo.

    ASSIGN atelier_sur_cde = NO chemin_calpx_t = "calpx_t".
    IF (entetcli.typ_sai = "F" OR entetcli.typ_sai = "D") AND parsoc.ges_ate THEN
    DO:
      FIND FIRST ateent OF entetcli NO-LOCK NO-ERROR.
      IF AVAIL ateent THEN
      DO:
          atelier_sur_cde = TRUE.
          IF can-find(first atecc no-lock) THEN chemin_calpx_t = "calpx_at". /*�XREF_NOWHERE�*/
      END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openquery wsaicl1
PROCEDURE openquery :
    /*$A106349...*/
    DEFINE BUFFER bparcde FOR parcde.
    find bparcde where bparcde.typ_fich="C" no-lock no-error.
    /*...$A106349*/

    /* $2240 */
    DO WITH FRAME F1:
        PCL_SET_TYPE_SAISIE("SAICL1":U, type-saisie).
        PCL_SET_TOUTES_SAISIES("SAICL1":U, toutes-saisies).
        PCL_SET_COD_CLI("SAICL1":U, IF AVAIL bparcde AND NOT bparcde.gst_zone[3] AND INT(cli$) = 0 THEN (-1) ELSE INT(cli$)). /*$A106349*/
        PCL_SET_DEPOT("SAICL1":U,INT(cdepot:screen-val)).
        PCL_SET_LISTEDEPOTS("SAICL1":U,ListeDepots).
        PCL_SET_NATURE("SAICL1":U,nat$:screen-value).
        PCL_SET_DEVTERM("SAICL1":U,tbdevis:CHECKED).
        PCL_SET_QCONDITION("SAICL1":U,qcondition).
        PCL_OPENQUERY ("SAICL1":U).
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openquery-pce wsaicl1
PROCEDURE openquery-pce :
RUN openquery.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pgm-libre wsaicl1
PROCEDURE pgm-libre :
/* Programme initialisation  */
find pgm where pgm.no_pgm=parnum no-lock no-error.
if available pgm then do:
    vchar = TrouveRepertoire(pgm.no_pgm). /*$A61779*/
    if search(vchar)<>? then run value(vchar) (cle-cours,input-output val-ini).
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE posicurs1 wsaicl1
PROCEDURE posicurs1 :
do with frame fc1:
    CASE parcde.curseur1:
        when "dat_cde" then first-fc1=entetcli.dat_cde:handle.
        when "dat_px" then first-fc1=entetcli.dat_px:handle.
        when "ref_cde" then first-fc1=entetcli.ref_cde:handle.
        when "not_ref" then first-fc1=not_ref:handle.
        when "dat_liv" then first-fc1=dat_liv:handle.
        when "depot" then first-fc1=edepot:handle.
        WHEN "nat_cde" THEN first-fc1=nat_cde:handle.
        when "civ_fac" then first-fc1=civ_fac:handle.
        when "adr_fac" then first-fc1=adr_fac[1]:handle.
        when "k_postf" then first-fc1=k_post2f:handle.
        when "villef" then first-fc1=villef:handle.
        when "paysf" then first-fc1=paysf:handle.
        when "devise" then first-fc1=devise:handle.
    /*$585*/    when "no_tarif" then first-fc1=no_tarif:handle.
        when "tx_ech_d" then first-fc1=tx_ech_d:handle.
        when "code_reg" then first-fc1=code_reg:handle.
        when "code_ech" then first-fc1=code_ech:handle.
        when "langue" then first-fc1=langue:handle.
        when "regime" then first-fc1=regime:handle.
        when "type_fac" then first-fc1=type_fac:handle.
        when "fac_dvs" then first-fc1=fac_dvs:handle.
        when "region" then first-fc1=region:handle.
        when "famille" then first-fc1=famille:handle.
        when "commerc" then first-fc1=commerc[1]:handle.
        when "app_aff" then first-fc1=app_aff:handle.
        when "ori_cde" then first-fc1=ori_cde:handle.
        when "cal_marg" then first-fc1=cal_marg:handle.
        when "groupe":U then first-fc1=groupe:handle.
    END CASE.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init wsaicl1
PROCEDURE proc-init :
	RUN aTrad$VariableInitial. /* $tradVar */
find first parsoc no-lock no-error. /* �XREF_NOWHERE� */
FIND FIRST atepar NO-LOCK NO-ERROR.  /*�XREF_NOWHERE�*/

ASSIGN
    g-user-droit = (IF ADesDroits("Droit", g-user) THEN g-user ELSE GetGroupeUser("Droit", g-user))
    g-user-perso = (IF ADesDroits("Personnalisation", g-user) THEN g-user ELSE GetGroupeUser("Personnalisation", g-user))
    pas-droit-crt = can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CRTCLI")
    droit-marge = NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="MARGE")
        AND (parsoc.valo_px<>"A" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPA"))
        AND (parsoc.valo_px<>"M" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPM"))
        AND (parsoc.valo_px<>"R" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPR"))
    pas-droit-vpr=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPR":U) /*$2629*/
    ProLink = GetSessionData("Config!", "ProLink")
    PropWF  = GetSessionData("Config!", "Workflow") /* $1105 */
    LogBid = {&WINDOW-NAME}:LOAD-ICON(GetSessionData("Config!", "Icone"))
    LogBid = bt$adresse-aff:Load-Image(GcoMedia + "\copier-af.bmp") IN FRAME Fc1 /* $A33761 */
    LogBid = Bcrtcli:Load-Image(StdMedia + "\creer.bmp") IN FRAME F1
    LogBid = Bfidelite:Load-Image(GCOMedia + "\fidelite.bmp")
    LogBid = BR-mg:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-nc$:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brc-cli:Load-Image(StdMedia + "\combo.bmp")
    LogBid = bappdev:Load-Image(GCOMedia + "\d40.bmp") IN FRAME Fc1
    LogBid = bcreer-aff:Load-Image(StdMedia + "\creer.bmp")
    LogBid = bmodif-aff:Load-Image(StdMedia + "\modifier.bmp")
    br-aa:PRIVATE-D = "AA"
    br-ci:PRIVATE-D = "CI"
    BR-DE:PRIVATE-D = "DE"
    br-fc:PRIVATE-D = "FC"
    br-gc:PRIVATE-D = "GC":U
    BR-LG:PRIVATE-D = "LG"
    br-nc:PRIVATE-D = "NC"
    br-kv:PRIVATE-D = "KV"
    br-oc:PRIVATE-D = "OC"
    BR-PA:PRIVATE-D = "PA"
    br-rg:PRIVATE-D = "RG"
    br-rp1:PRIVATE-D = "RP"
    br-rv:PRIVATE-D = "RV"
    br-tf:PRIVATE-D = "TF"
    LogBid = br-aa:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-aff:Load-Image(StdMedia + "\combo.bmp")
    LogBid = btn-materiel:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-ci:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-DE:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-Ech:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-gc:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-fc:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-sc:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-sd:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-se:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-lg:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-nc:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-kv:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-oc:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-PA:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-KP:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-reg:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-rg:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-rp1:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-rv:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-tar:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-tf:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-xt1:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-xt2:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-xt3:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-xt4:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-xt5:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-ct:Load-Image(StdMedia + "\combo.bmp") IN FRAME Fc2
    br-ct:PRIVATE-D = "CT"
    br-rp2:PRIVATE-D = "RP"
    br-ss:PRIVATE-D = "SS"
    br-gpres:PRIVATE-D = "GP"
    LogBid = br-ss:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-gpres:Load-Image(StdMedia + "\combo.bmp")
    LogBid = br-rp2:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcchq:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcfact:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcfactor:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcgrp:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brclivre:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcpaye:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcpln:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcplr:Load-Image(StdMedia + "\combo.bmp")
    LogBid = brcstat:Load-Image(StdMedia + "\combo.bmp")
    LogBid = btn-lancement:Load-Image(StdMedia + "\combo.bmp")
    logbid = btn-contratloc:LOAD-IMAGE(StdMedia + "\combo.bmp") /* $1727 */
    LogBid = BR-zt1:Load-Image(StdMedia + "\combo.bmp") IN FRAME Fval
    LogBid = BR-zt2:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-zt3:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-zt4:Load-Image(StdMedia + "\combo.bmp")
    LogBid = BR-zt5:Load-Image(StdMedia + "\combo.bmp").

    /*$A54437...*/
    IF type-saisie = "C" AND CAN-FIND (FIRST prefinfu WHERE prefinfu.contexte = "ENTETCLICOM":U NO-LOCK) THEN afficherinfo = YES.
    ELSE IF type-saisie = "D" AND CAN-FIND (FIRST prefinfu WHERE prefinfu.contexte = "ENTETCLIDEV":U NO-LOCK) THEN afficherinfo = YES.
    ELSE IF type-saisie = "F" AND CAN-FIND (FIRST prefinfu WHERE prefinfu.contexte = "ENTETCLIFAC":U NO-LOCK) THEN afficherinfo = YES.
    ELSE afficherinfo = NO.
    /*...$A54437*/
        /* $A56248... */
    crtHorsSaicl1 = (getSessionData("saicl1_c","crtHorsSaicl1":U) = "yes").
    setSessionData("saicl1_c","crtHorsSaicl1":U, ?).
    /* ...$A56248 */

    pvalaffaire = GetSessionData("SAICL1_C":U, "VAL-AFFAIRE":U). /*$A85716*/

FormatColBrowse (INPUT BUFFER entetcli:HANDLE,"","").
FormatColBrowse (INPUT BUFFER ateent:HANDLE,"","").
FormatColBrowse (INPUT BUFFER affaire:HANDLE,"",""). /* $1932 */

/* Droits pour modifier pi�ces */
/* $A40219 ... */
IF NOT MNU_DROIT_NOOPTM (g-user-perso, 52) /*(can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="" and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="Gestion avant vente":U and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.no_opt=52 no-lock)) and not
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="A" and z-menuser.no_opt=52 no-lock)*/
    THEN droit-modifier[1] = NO.
IF NOT MNU_DROIT_NOOPTM (g-user-perso, 46) /*(can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="" and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="Gestion commandes":U and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.no_opt=46 no-lock)) and not
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="A" and z-menuser.no_opt=46 no-lock)*/
    THEN droit-modifier[2] = NO.
IF NOT MNU_DROIT_NOOPTM (g-user-perso, 53) /*(can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="" and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="Gestion facturation":U and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.no_opt=53 no-lock)) and not
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="A" and z-menuser.no_opt=53 no-lock)*/
    THEN droit-modifier[3] = NO.
IF NOT MNU_DROIT_NOOPTM (g-user-perso, 54) /*(can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="" and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.menu="Facturation":U and z-menuser.s_menu="Gestion facturation":U and z-menuser.no_opt=0 no-lock) or
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="S" and z-menuser.no_opt=54 no-lock)) and not
 *     can-find(first z-menuser where z-menuser.ndos=0 and z-menuser.util=g-user-perso and z-menuser.action="A" and z-menuser.no_opt=54 no-lock)*/
    THEN droit-modifier[4] = NO.
/* ... $A40219 */

IF LENGTH (type-saisie) > 2 AND SUBSTR (type-saisie,1,2) = "da" THEN
    ASSIGN wori_ate = YES
           wori_saibp1 = YES  /* $a36019 */
           ori_depot = INT (SUBSTR (type-saisie,31,4)) /* $a36019 */
           ori_parc = SUBSTR (type-saisie,3,1)
           ori_mat = SUBSTR (type-saisie,4,17)
           vqui = SUBSTR (type-saisie,21,10)
           type-saisie = "D". /* $754 on vient de l'atelier */

/* $A69165 */
find FIRST tva where Tva.ngr1 = g-tva-ngr1 AND Tva.ngr2 = g-tva-ngr2 AND Tva.ndos = g-tva-ndos AND tva.dft_poste[7] = YES no-lock no-error.
TvaParDefaut = (if avail tva then tva.CODE_tva ELSE 1).

find parcde where parcde.typ_fich="B" no-lock no-error.
if available parcde then case type-saisie:
    when "D" then codebarre=(gst_zone[1]=yes).
    when "C" then codebarre=(gst_zone[2]=yes).
    when "F" then codebarre=(gst_zone[3]=yes).
    when "R" then codebarre=(gst_zone[4]=yes).
end.

find parcde where parcde.typ_fich="C" no-lock no-error.
ASSIGN
    gere_ate = AVAIL parsoc AND parsoc.ges_ate /* $879 */
    gere_aff = AVAIL parsoc AND parsoc.affaire /* $1932 */
    lgTextile = (AVAILABLE parsoc) AND (parsoc.ges_tex).
IF gere_ate THEN DO:
    FIND FIRST atepar NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
    IF AVAIL atepar THEN tx_atelier = atepar.tx_pce.
END.

/* GPAO */
IF AVAILABLE parsoc AND parsoc.gpao = TRUE THEN FIND FIRST pargp NO-LOCK NO-ERROR.

droits-modif-aff = AFF_DROITS_MODIFS(INPUT g-user). /*$A32485*/

/* Association des variables onglets � des tableaux d'handle */
do with frame fonglet:
    /*RUN control_load.*/

    RUN creer-imglist.
    RUN creer-tabstrip.

    /* Lecture des chapitres � traiter */
    assign
        myonglet   = chCtrlFrame:TabStrip
        myonglet:tabs(1):caption = Traduction("En-t�te",-2,"")
        chCtrlFrame:TabStrip:ImageList = chCtrlFrame-2:ImageList
        Logbid=entetcli.cod_cli:MOVE-TO-TOP()
        Logbid=entetcli.NO_cde:MOVE-TO-TOP()
        Logbid=entetcli.NO_bl:MOVE-TO-TOP()
        Logbid=fill-libmat:MOVE-TO-TOP()
        Logbid=entetcli.cod_cli:SIDE-LABEL-HANDLE:MOVE-TO-TOP()
        Logbid=entetcli.no_cde:SIDE-LABEL-HANDLE:MOVE-TO-TOP()
        Logbid=entetcli.no_BL:SIDE-LABEL-HANDLE:MOVE-TO-TOP()
        chap-cours=1.

    if parcde.gst_zone[1] then DO X=2 TO (if parcde.gst_zone[2] then 3 ELSE 2):
        IF X = 2 THEN myonglet:tabs(2):caption = Traduction("En-t�te Divers",-2,"").
        ELSE myonglet:tabs(3):caption = Traduction("Valeurs libres",-2,"").
        nb-chap = X.
    end.
    ELSE DO:
        myonglet:tabs:remove(3).
        myonglet:tabs:remove(2).
    END.
    IF parcde.gst_zone[1] AND parcde.gst_zone[2] = NO THEN myonglet:tabs:remove(3).

    HIDE
        entetcli.cod_cli
        entetcli.no_cde
        entetcli.no_bl IN FRAME fonglet.
    VIEW FRAME Fonglet IN WINDOW Wsaicl1.
end.

/* Initialisation valeurs libres si utilis� */
do with frame fc1:
    hide
        xa1 xa2 xa3 xa4 xa5 xd1 xd2 xd3 xd4 xd5 xn1 xn2 xn3 xn4 xn5 xt1 xt2 xt3 xt4 xt5
        br-xt1 br-xt2 br-xt3 br-xt4 br-xt5 lib-xt1 lib-xt2 lib-xt3 lib-xt4 lib-xt5 xl1 xl2 xl3 xl4 xl5.
    ASSIGN
        zah[1]=za1:handle zah[2]=za2:handle zah[3]=za3:handle zah[4]=za4:handle zah[5]=za5:HANDLE
        znh[1]=zn1:handle znh[2]=zn2:handle znh[3]=zn3:handle znh[4]=zn4:handle znh[5]=zn5:HANDLE
        zlh[1]=zl1:handle zlh[2]=zl2:handle zlh[3]=zl3:handle zlh[4]=zl4:handle zlh[5]=zl5:HANDLE
        zdh[1]=zd1:handle zdh[2]=zd2:handle zdh[3]=zd3:handle zdh[4]=zd4:handle zdh[5]=zd5:HANDLE
        zth[1]=zt1:handle zth[2]=zt2:handle zth[3]=zt3:handle zth[4]=zt4:handle zth[5]=zt5:HANDLE
        xah[1]=xa1:handle xah[2]=xa2:handle xah[3]=xa3:handle xah[4]=xa4:handle xah[5]=xa5:HANDLE
        xnh[1]=xn1:handle xnh[2]=xn2:handle xnh[3]=xn3:handle xnh[4]=xn4:handle xnh[5]=xn5:HANDLE
        xlh[1]=xl1:handle xlh[2]=xl2:handle xlh[3]=xl3:handle xlh[4]=xl4:handle xlh[5]=xl5:HANDLE
        xdh[1]=xd1:handle xdh[2]=xd2:handle xdh[3]=xd3:handle xdh[4]=xd4:handle xdh[5]=xd5:HANDLE
        xth[1]=xt1:handle xth[2]=xt2:handle xth[3]=xt3:handle xth[4]=xt4:handle xth[5]=xt5:HANDLE.
end.

if parcde.gst_zone[2] then RUN INIT-LIBRE ("C","C").

/* D�p�t �cran F1 et FC1 */

IF loadsettings("SAICL", output hsettingsapi) then do:
    ASSIGN
        radio-adresse:SCREEN-VAL = GetSetting("Radio-adresse","no")
        vchar = GetSetting("Depot",?)
        vcli  = GetSetting("Client",?)
        tvoir = GetSetting("Infos",?) = "yes"
        toutes-saisies = GetSetting("Toutes",?) = "yes".
    IF porigine="3" OR porigine="4" THEN vqui=interv.
    ELSE /*$A64667...*/ IF pOrigine <> "6a" THEN /*...$A64667*/ vqui = GetSetting("Qui",?).
    UnloadSettings(hSettingsApi).
END.

FIND tabgco where (IF parsoc.mult_soc THEN tabgco.ndos = g-ndos ELSE TRUE) AND tabgco.type_tab="DS" NO-LOCK NO-ERROR.
ASSIGN
    logbid=cdepot:DELETE(1) IN FRAME f1
    logbid=edepot:DELETE(1) IN FRAME fc1
    aucun-depot=NO
    ListeDepots="".
IF AMBIGUOUS tabgco THEN DO:
    find droigco where droigco.util = g-user and droigco.fonc = "DEPOT" no-lock no-error.
    IF NOT AVAIL droigco THEN find droigco where droigco.util = g-user-droit and droigco.fonc = "DEPOT" no-lock no-error.

    find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type="DS" no-lock no-error.
    X=0.
    IF AVAIL droigco AND droigco.liste_alpha THEN FOR EACH tabgco WHERE (IF parsoc.mult_soc THEN tabgco.ndos = g-ndos ELSE TRUE) AND tabgco.TYPE_tab = "DS" NO-LOCK  BY tabgco.inti_tab:
        IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
        X=X + 1.
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("DS", STRING(tabgco.n_tab), g-langue).
        IF NOT CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) AND NOT CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user AND droitdep.depot=tabgco.n_tab NO-LOCK) /*$A66728*/ THEN ASSIGN
            logbid=cdepot:ADD-LAST ( (IF intiTab <> "" THEN intiTab ELSE tabgco.inti_tab) + "(" + string(tabgco.n_tab) + ")", STRING (tabgco.n_tab))
            logbid=edepot:ADD-LAST ( (IF intiTab <> "" THEN intiTab ELSE tabgco.inti_tab) + "(" + string(tabgco.n_tab) + ")", STRING (tabgco.n_tab))
            ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
    END.
    ELSE FOR EACH tabgco WHERE (IF parsoc.mult_soc THEN tabgco.ndos = g-ndos ELSE TRUE) AND tabgco.TYPE_tab = "DS" NO-LOCK  BY tabgco.n_tab:
        IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
        X=X + 1.
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("DS", STRING(tabgco.n_tab), g-langue).
        IF NOT CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) AND NOT CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user AND droitdep.depot=tabgco.n_tab NO-LOCK) /*$A66728*/ THEN ASSIGN
            logbid=cdepot:ADD-LAST ( (IF intiTab <> "" THEN intiTab ELSE tabgco.inti_tab) + "(" + string(tabgco.n_tab) + ")", STRING (tabgco.n_tab))
            logbid=edepot:ADD-LAST ( (IF intiTab <> "" THEN intiTab ELSE tabgco.inti_tab) + "(" + string(tabgco.n_tab) + ")", STRING (tabgco.n_tab))
            ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
    END.
    IF cdepot:NUM-ITEMS>1 THEN DO:
        IF cdepot:NUM-ITEMS<>X THEN ListeDepots=TRIM(ListeDepots,",").
        ELSE ListeDepots="".
        cdepot:ADD-FIRST ((IF parsoc.mult_soc THEN ("<" + TRIM(g-nom) + ">") ELSE ("<" + Traduction("Tous d�p�ts autoris�s",-2,"") + ">")), "0").
    END.
END.
ELSE DO:
    IF AVAIL tabgco THEN ASSIGN
        logbid=cdepot:ADD-LAST ("<" + Traduction("Tous d�p�ts",-2,"") + ">", STRING(tabgco.n_tab))
        logbid=edepot:ADD-LAST ("<" + Traduction("Tous d�p�ts",-2,"") + ">", STRING(tabgco.n_tab)).
    ELSE ASSIGN
        logbid=cdepot:ADD-LAST ("<" + Traduction("Tous d�p�ts",-2,"") + ">", "1")
        logbid=edepot:ADD-LAST ("<" + Traduction("Tous d�p�ts",-2,"") + ">", "1")
        aucun-depot=YES.
END.
assign
    depot-f1=cdepot:ENTRY(1)
    depot-fc1=edepot:ENTRY(1).
IF cdepot:NUM-ITEMS>1 THEN DO:
    IF AVAIL droigco AND droigco.depot>0 AND LOOKUP(STRING(droigco.depot),cdepot:LIST-ITEM-PAIRS)>0 THEN depot-f1=STRING(droigco.depot).
    ELSE IF LOOKUP(vchar,cdepot:LIST-ITEM-PAIRS)>0 THEN depot-f1=vchar.
END.
IF edepot:NUM-ITEMS>1 THEN DO:
    IF AVAIL droigco AND droigco.depot>0 AND LOOKUP(STRING(droigco.depot),edepot:LIST-ITEM-PAIRS)>0 THEN depot-fc1=STRING(droigco.depot).
    ELSE IF AVAIL enttabdft AND enttabdft.dft_poste<>"" AND LOOKUP(trim(enttabdft.dft_poste),edepot:LIST-ITEM-PAIRS)>0 THEN depot-fc1=trim(enttabdft.dft_poste).
    ELSE ASSIGN
        logbid=edepot:ADD-FIRST ("<" + Traduction("Choisir un d�p�t",-2,"") + ">", "0")
        depot-fc1="0".
END.

IF loadsettings("PARFAX", output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("Corresp",?)
           CorFax = (Vchar = "OUI").
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("REFERE", output hsettingsapi) then do:
    vapp1 = GetSetting("Appel",?).
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("SAIDYNC", output hsettingsapi) then do:
    vapp2 = GetSetting("Appel",?).
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("CADENCIER", output hsettingsapi) then do:
    vapp3 = GetSetting("Appel",?).
    UnloadSettings(hSettingsApi).
END.

/* $A40589... */
IF loadsettings("SAIETUD", output hsettingsapi) then do:
    etudrapid = LOGICAL(GetSetting("RAPIDE","NO")).
    UnloadSettings(hSettingsApi).
END.
/* ...$A40589 */
IF loadsettings("RAPIDE", output hsettingsapi) then do:
    ASSIGN
        v-rapide = GetSetting("Rapide",?)
        vrai-v-rapide = v-rapide. /* $A40589 */
    UnloadSettings(hSettingsApi).
END.

/* $A40589... */
IF etudrapid AND CAN-DO("D,C":U,type-saisie) THEN v-rapide = "oui":U.
ELSE v-rapide = vrai-v-rapide.
/* ...$A40589 */

IF loadsettings("HBSAICL1", output hsettingsapi) then do:
    ASSIGN chqryby = GetSetting("ToutTri",?)
           colord  = GetSetting("ChampTri",?)
           qliste-champ = GetSetting("*HBColonnes",?)
           qcolonne-lock = int(GetSetting("*HBBloquees","0"))
           qliste-label  = GetSetting("*HBLabels",?)
           qcondition    = GetSetting("*HBCondition",?).
    UnloadSettings(hSettingsApi).
END.

IF qliste-champ = "" OR qliste-champ = ? THEN RUN init-colonnes.

coldesc=(chqryby<>? AND SUBSTR(chqryby,LENGTH(chqryby) - 3,4)="DESC").
IF loadsettings("PARFAX", output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("Corresp",?)
           CorFax = (Vchar = "OUI").
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("PARMAIL", output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("Corresp",?)
           CorMail = (Vchar = "OUI").
    UnloadSettings(hSettingsApi).
END.
/*$A36586...*/
IF loadsettings("PAREDTBL":U, output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("DocsComp",?)
           CorDocsComp = (Vchar = "OUI":U)
           vchar = GetSetting("AvecConf",?)
           CorAvcConf = (Vchar = "OUI":U)
           vchar = GetSetting("docmailBL",?)
           DocmailBL = (Vchar = "OUI":U).
    UnloadSettings(hSettingsApi).
END.
/*...$A36586*/
/*$A49610...*/
IF loadsettings("PAREDTARC":U, output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("DocsComp",?)
           ARCDocsComp = (Vchar = "OUI":U)
           vchar = GetSetting("AvecConf",?)
           ARCAvcConf = (Vchar = "OUI":U)
           vchar = GetSetting("docmailarc",?)
           Docmailarc = (Vchar = "OUI":U).
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("PAREDTDEV":U, output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("DocsComp",?)
           DEVDocsComp = (Vchar = "OUI":U)
           vchar = GetSetting("AvecConf",?)
           DEVAvcConf = (Vchar = "OUI":U)
           vchar = GetSetting("docmaildev",?)
           Docmaildev = (Vchar = "OUI":U).
    UnloadSettings(hSettingsApi).
END.
IF loadsettings("PAREDTFAC":U, output hsettingsapi) then do:
    ASSIGN vchar = GetSetting("DocsComp",?)
           FACDocsComp = (Vchar = "OUI":U)
           vchar = GetSetting("AvecConf",?)
           FACAvcConf = (Vchar = "OUI":U)
           vchar = GetSetting("docmailFAC",?)
           DocmailFAc = (Vchar = "OUI":U).
    UnloadSettings(hSettingsApi).
END.
/*...$A49610*/

/* date et semaine livraison initiale */
FIND FIRST depsoc WHERE depsoc.depot = INT (depot-f1) NO-LOCK NO-ERROR.  /* $1110 */
dat-liv-ini=TODAY.
IF CAN-DO("C,D",type-saisie) THEN DO:
    IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN /*$979*/
        run trt-calendar (0,year(dat-liv-ini),(IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv),input-output dat-liv-ini).
    else dat-liv-ini=dat-liv-ini + (IF AVAIL depsoc AND depsoc.del_liv <> 0 THEN depsoc.del_liv ELSE parsoc.DEL_liv).
END.
IF parsoc.sem_liv THEN sem-liv-ini=CalculSemaine(dat-liv-ini).

IF loadsettings("colsai", output hsettingsapi) then do:
    case type-saisie:
      when "A" then ASSIGN rccola  = int(GetSetting("r8",ColBlanc))
                           rccolb  = int(GetSetting("r8b",ColBlanc)).
      when "D" then ASSIGN rccola  = int(GetSetting("r3",ColBlanc))
                           rccolb  = int(GetSetting("r3b",ColBlanc)).
      when "C" then ASSIGN rccola  = int(GetSetting("r4",ColBlanc))
                           rccolb  = int(GetSetting("r4b",ColBlanc)).
      when "F" then ASSIGN rccola  = int(GetSetting("r5",ColBlanc))
                           rccolb  = int(GetSetting("r5b",ColBlanc)).
      otherwise ASSIGN rccola  = int(GetSetting("r6",ColBlanc))
                       rccolb  = int(GetSetting("r6b",ColBlanc)).
    end case.
    UnloadSettings(hSettingsApi).
END.

/* Type de saisie */
case type-saisie:
    when "C" then ltype-saisie= Traduction("Gestion des commandes",-2,"").
    when "F" then DO:
        IF nopt = 556 THEN ltype-saisie=Traduction("Gestion des avoirs",-2,""). /* Avoir financier */
            ELSE IF nopt = 615 THEN ltype-saisie=Traduction("Factures financi�res",-2,""). /* facture financiere */
                ELSE ltype-saisie=Traduction("Gestion des factures",-2,"").
    END.
    when "D" then ltype-saisie=Traduction("Gestion des devis",-2,"").
    when "R" then ltype-saisie=Traduction("Gestion des saisies comptoir",-2,"").
end case.

RUN ComBarsCreation.

RUN api-piece-cl PERSISTENT SET hapi-piece-cl (HndGcoErg). /* $2178 */

ASSIGN
    Logbid = CBCheckItem(hComBars-f1, 2, {&Bcorres-m}, Cormail)
    Logbid = CBCheckItem(hComBars-f1, 2, {&Bcorres-f}, CorFax)
    Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-ctrvt}, (AVAIL parsoc AND parsoc.gpao AND NOT pas-droit-vpr)) /*$2629*/.
/* $2240... */
hbrowse = PCL_CREATION_BROWSE_PIECE ("SAICL1":U, "", 1, 160, 790, FRAME F1:HEIGHT-P - 165, FRAME F1:HANDLE, "", qliste-champ, qliste-label, qcondition, qcolonne-lock
                                     , colord, coldesc, chQryBy, NO ,NO,0,0,100,100).

ON F11 OF HBrowse PERSISTENT RUN Ctrlf8 IN THIS-PROCEDURE.
/* ...$2240 */

/*$A49249...*/
IF parsoc.ngrp <> 0 THEN DO:
    buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.LOGIN = ":U + QUOTER(g-user)).
    IF buf-id <> ? THEN quiauto = CAPS(TPS-GET-PERS-VALUE(buf-id, "COD_PERS", 0)).
END.
/*...$A49249*/

/*Doit �tre fait aussi depuis suivi saisie client*/
Pcaisse = 0.
IF can-do("1,3,6A":U,porigine) AND type-saisie = "R" AND parsoc.ges_cai then DO:
    FOR EACH tabgco WHERE tabgco.TYPE_tab = "CM" NO-LOCK:
        IF tabgco.jour_t[4] = YES AND g-user = tabgco.NO_port THEN DO:
            Pcaisse = tabgco.n_tab.
            LEAVE.
        END.
    END.
END.

/* Ecran 1 */
if can-do("1,3",porigine) then DO WITH FRAME F1:
    assign
        cli$="" lib-cli="" qui$="" lib-mg="" typ-cde$:list-items="".

    /* $1753 ... */
    IF vChargerClient THEN
    DO:
        FIND FIRST client WHERE client.cod_cli = pnocl NO-LOCK NO-ERROR.
        IF AVAIL client THEN ASSIGN lib-cli = nom_cli
                                    cli$ = STRING(client.cod_cli).
    END.
    /* ... $1753 */

    /*$2129 on recherche si l'utilisateur est li� � une caisse */
    IF type-saisie = "R" AND parsoc.ges_cai THEN DO:
/*Doit �tre fait aussi depuis suivi saisie client
        FOR EACH tabgco WHERE tabgco.TYPE_tab = "CM" NO-LOCK:
            IF tabgco.jour_t[4] = YES AND g-user = tabgco.NO_port THEN DO:
                Pcaisse = tabgco.n_tab.
                LEAVE.
            END.
        END.
*/
        /* si oui alors on initialise le client et le d�p�t de la caisse */
        IF Pcaisse > 0 AND AVAIL tabgco /*AND tabgco.cli_cai > 0*/ THEN DO:
            IF tabgco.cli_cai > 0 THEN DO:
                FIND FIRST client WHERE client.cod_cli = tabgco.cli_cai NO-LOCK NO-ERROR.
                IF AVAIL client THEN ASSIGN lib-cli = client.nom_cli
                                            cli$ = STRING(client.cod_cli) vcli = ?.
            END.

            IF tabgco.depot > 0 THEN depot-f1 = string(tabgco.depot).

            ASSIGN Logbid = CBShowItem (Hcombars-f1, 2, {&Breaffect}, YES)
                   Logbid = CBShowItem (Hcombars-f1, 2, {&Bacompte}, YES). /*$2624*/
        END.
    END.

    /* Client */
    if porigine="3" and pnocl<>0 then do:
      find client where client.cod_cli=pnocl no-lock no-error.
      if available client then assign lib-cli=nom_cli cli$=string(client.cod_cli).
    end.
    else if gst_zone[3] and vcli<>? then do:
      find client where client.cod_cli=int(vcli) no-lock no-error.
      if available client then assign lib-cli=nom_cli cli$=string(client.cod_cli).
    end.
    /* Intervenant */
    if vqui<>? AND NOT (type-saisie = "R" AND AVAIL parsoc AND parsoc.rab_mgv) /* $914 */ then do:
      /*$2104...find tabgco where tabgco.type_tab="MG" and a_tab=vqui AND tabgco.typ_int[5]=YES no-lock no-error.*/
      buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(vqui) + " AND TPPERSONNE.vente = YES ").
      IF  buf-id <> ? THEN ASSIGN lib-mg:SCREEN-VAL = TPS-GET-PERS-VALUE(buf-id, "LIBELLE_GCO", 0) qui$:SCREEN-VAL = TPS-GET-PERS-VALUE(buf-id, "COD_PERS", 0) qui$.  /*...$2104*/
    end.
    /* Types de commande */

    for each tabgco where tabgco.type_tab="YC" no-lock:
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("YC", STRING(tabgco.n_tab), g-langue).
        typ-cde$:add-last(string(tabgco.n_tab,"9") + " " + (IF g-langue <> "" AND intiTab <> "" THEN string(intiTab,"x(25)") ELSE string(tabgco.inti_tab,"x(25)"))).
    end.
    if typ-cde$:num-items<2 then plusieurs-types=no.
    else do:
      plusieurs-types=yes.
      find enttabdft where enttabdft.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /* $A64095 */ enttabdft.ent_type="YC" no-lock no-error.
      if available enttabdft and int(enttabdft.dft_poste)>0 and int(enttabdft.dft_poste)<= typ-cde$:num-items then
        typ-cde$:screen-value=typ-cde$:entry(int(enttabdft.dft_poste)).
      else typ-cde$:screen-value=typ-cde$:entry(1).
    end.
    /* Fin */

    DISP Ft$selection-1.
    ASSIGN
        dat-liv$:LABEL=IF parsoc.FORM_liv=3 THEN Traduction("Date exp�dition",-2,"")
                       ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�part",-2,"")
                       ELSE Traduction("Date livraison",-2,"")
        dat-liv$:TOOLTIP=Traduction("Date livraison",-2,"") + " " + "(" + Traduction("D�part",-2,"") + ")".

    ENABLE
        cli$ brc-cli qui$ br-mg dat-cde$ typ-cde$ dat-liv$ sem$ ref-cde$ not-ref$
        tvoir nat$ br-nc$ cdepot Tbdevis bcrtcli bfidelite.
    IF pas-droit-crt THEN HIDE bcrtcli.
    /*IF Pcaisse = 0 THEN $JL en attendant */ HIDE Bfidelite.
    display tvoir cli$ lib-cli qui$ today @ dat-cde$ dat-liv-ini @ dat-liv$ sem-liv-ini @ sem$.
    assign
        cdepot:SCREEN-VAL=depot-f1
        nat$:screen-value=""
        Tbdevis:HIDDEN = (type-saisie <> "D").
    IF cdepot:NUM-ITEMS=1 THEN cdepot:HIDDEN=YES.
    if not parsoc.ges_nat then hide nat$ br-nc$.
    if not parsoc.ges_qui then hide qui$ br-mg lib-mg.
    /*$A49249....
    /*$913...*/
    IF parsoc.ngrp <> 0 THEN DO:
        /*$2104... FOR FIRST tabgco WHERE tabgco.TYPE_tab = "MG" AND tabgco.login = g-user no-lock:*/
        buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.LOGIN = ":U + QUOTER(g-user)).
        IF  buf-id <> ? THEN ASSIGN quiauto = TPS-GET-PERS-VALUE(buf-id, "COD_PERS", 0).
    END.
    /*...$913*/
    ...$A49249*/
    IF NOT parsoc.sem_liv THEN HIDE sem$.
    if not parsoc.ges_ref then hide ref-cde$.
    if not parsoc.ges_not then hide not-ref$.
    if not plusieurs-types then hide typ-cde$.
    /*$1105... */
    IF CONNECTED ("WORKFLOW") AND SEARCH(PropWF + "\suivi_wf.r") <> ? AND SEARCH(PropWF + "\reccont_wf.r") <> ? THEN DO:
        RUN VALUE (PropWF + "\reccont_wf") ("PROGIWIN","*SAISIE VENTE", OUTPUT pok-wkf).
        IF pok-wkf THEN ASSIGN Logbid = CBEnableItem (Hcombars-f1, 2, {&bwrkf}, YES) gest-wkf = YES.
        ELSE ASSIGN Logbid = CBEnableItem (Hcombars-f1, 2, {&bwrkf}, NO) gest-wkf = NO.
    END.
    ELSE ASSIGN Logbid = CBEnableItem (Hcombars-f1, 2, {&bwrkf}, NO) gest-wkf = NO. /* ...$1105*/
    RUN RESIZEWINDOW.
    assign frame f1:hidden=no.
    if cli$<>"" then RUN CLI-YES.
    ELSE RUN CLI-NO.
    ecr-cours=1.
    run aff-assist.
END.

entetcli.region:LABEL IN FRAME FC1 = Traduction("R�gion",-1,"").

{&WINDOW-NAME}:title=ltype-saisie.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE put-ini wsaicl1
PROCEDURE put-ini :
DO WITH FRAME F1:
    IF CAN-DO("1,3",porigine) /* $A41934 */ AND LoadSettings("SAICL", OUTPUT hSettingsApi) THEN DO:
        ASSIGN
            logbid=SaveSetting("Radio-Adresse",radio-adresse:SCREEN-VALUE IN FRAME Fc1)
            logbid=SaveSetting("Depot",cdepot:SCREEN-VAL IN FRAME f1)
            logbid=SaveSetting("Client",cli$:screen-value)
            logbid=SaveSetting("Infos" ,tvoir:screen-value).
        IF qui$:screen-val<>"" THEN logbid=SaveSetting("Qui",(IF quiauto <> "" THEN quiauto ELSE qui$:SCREEN-VAL)).
        UnloadSettings(hSettingsApi).
    end.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rch-tx-devise-vente wsaicl1
PROCEDURE rch-tx-devise-vente :
do with frame fc1:
    DEF VAR p%Val AS DEC DECIMALS 8 no-undo.
    p%Val = CalculTauxChange(YES,entetcli.devise:INPUT-VALUE,entetcli.dat_px:INPUT-VALUE,0).
    display p%Val @ entetcli.tx_ech_d.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recalcul-global wsaicl1
PROCEDURE recalcul-global :
run recalcul-prix (NO).
/* Sous-type DF */
for each lignecli of entetcli where lignecli.sous_type="DF" and lignecli.calcul<=2 exclusive-lock:
  if lignecli.px_refa <> 0 then do:
    if can-do("C,P",entetcli.cal_marg) then do:
        run artphase (lignecli.typ_elem,output cum-marge).
        if entetcli.cal_marg="P" then lignecli.px_vte=round(lignecli.px_refa / (1 - (cum-marge / 100)),g-ndv).
        else lignecli.px_vte=ROUND((lignecli.px_refa * cum-marge) / 100,g-ndv).
    end.
    else if entetcli.cal_marg="V" then lignecli.px_vte=lignecli.px_refa.
    else if entetcli.fac_pxa and entetcli.maj_ach<>0 then lignecli.px_vte=round(lignecli.px_refa + (lignecli.px_refa * entetcli.maj_ach / 100),g-ndv).
  end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recalcul-prix wsaicl1
PROCEDURE recalcul-prix :
def input param precach as log no-undo. /*$603*/
DEF VAR LigneGratuit AS LOG NO-UNDO. /*$A119346*/
DEF VAR TypeGratuit AS CHAR NO-UNDO. /*$A119346*/

DEFINE VARIABLE vint AS INTEGER             NO-UNDO. /*$A120503*/

{calpx-v.i}
logbid=YES.

/*$A35969...*/
IF precach AND recalculer-prix-franco THEN
DO:
    /*$A120503...*/
    RUN recalchx_c(Traduction("Vous avez modifi� un param�tre influant sur le calcul des prix franco.",-2,""), "", OUTPUT vint).
    /*MESSAGE Traduction("Vous avez modifi� un param�tre influant sur le calcul des prix franco.",-2,"") + " " + Traduction("Voulez-vous recalculer les prix ?",-2,"")
        VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.*/
    /*...$A120503*/
    recalculer-prix-franco = NO.
END.
ELSE
/*...$A35969*/
    IF precach THEN DO:
        /*$A120503...*/
        RUN recalchx_c(SUBSTITUTE(Traduction("Vous pouvez recalculer les prix � la date du &1.",-2,""),entetcli.dat_px:screen-value in frame fc1), "", OUTPUT vint).
        /*MESSAGE SUBSTITUTE(Traduction("Voulez-vous recalculer les prix � la date du &1 ?",-2,""),entetcli.dat_px:screen-value in frame fc1)
            view-as alert-box question buttons yes-no update logbid.*/
        /*...$A120503*/
    END.

IF vint > 1 THEN DO: /*$A120503*/
    session:SET-WAIT("general").
    /* $1396 cf calpx-v.i */
    ASSIGN
        wls_tarif="".
        wls_cumc="".

    IF entetcli.tar_fil[1]:CHECKED IN FRAME fc2 THEN wls_tarif = vcl_grp:SCREEN-VAL IN FRAME fc2.
    IF entetcli.tar_fil[2]:CHECKED THEN wls_tarif = wls_tarif + "," + vcl_livre:SCREEN-VAL.
    IF entetcli.tar_fil[3]:CHECKED THEN wls_tarif = wls_tarif + "," + vcl_fact:SCREEN-VAL.
    IF entetcli.tar_fil[4]:CHECKED THEN wls_tarif = wls_tarif + "," + vcl_paye:SCREEN-VAL.
    IF entetcli.tar_fil[5]:CHECKED THEN wls_tarif = wls_tarif + "," + vcl_pln:SCREEN-VAL.
    IF entetcli.tar_fil[6]:CHECKED THEN wls_tarif = wls_tarif + "," + vcl_plr:SCREEN-VAL.
    IF entetcli.tar_cum[1]:CHECKED THEN wls_cumc = vcl_grp:SCREEN-VAL.
    IF entetcli.tar_cum[2]:CHECKED THEN wls_cumc = wls_cumc + "," + vcl_livre:SCREEN-VAL.
    IF entetcli.tar_cum[3]:CHECKED THEN wls_cumc = wls_cumc + "," + vcl_fact:SCREEN-VAL.
    IF entetcli.tar_cum[4]:CHECKED THEN wls_cumc = wls_cumc + "," + vcl_paye:SCREEN-VAL.
    IF entetcli.tar_cum[5]:CHECKED THEN wls_cumc = wls_cumc + "," + vcl_pln:SCREEN-VAL.
    IF entetcli.tar_cum[6]:CHECKED THEN wls_cumc = wls_cumc + "," + vcl_plr:SCREEN-VAL.
    ASSIGN
        wls_tarif=TRIM(wls_tarif,",").
        wls_cumc=TRIM(wls_cumc,",").

    RUN ObtenirNomCalPx (INPUT-OUTPUT atelier_sur_cde, INPUT-OUTPUT chemin_calpx_t).

    /*$A35969...*/
    IF parsoc.prix_franco THEN
    DO:
        SetSessionData("CALPX-VENTE":U,"CodeAffaire":U,entetcli.affaire:SCREEN-VALUE).
        SetSessionData("CALPX-VENTE":U,"NoAdresse":U,(IF adrliv-modifie THEN string(adrliv-no_adr) ELSE string(wno_adr))).
        SetSessionData("CALPX-VENTE":U,"TypeCond":U,(IF adrliv-modifie THEN adrliv-typ_con ELSE entetcli.typ_con)).
        SetSessionData("CALPX-VENTE":U,"PrixFranco":U,(IF entetcli.prix_franco:CHECKED THEN "yes" ELSE "no")).
        IF adrliv-modifie THEN SetSessionData("CALPX-VENTE":U,"ZoneGeo":U,string(adrliv-zon_geo)).
        ELSE DO:
            IF entetcli.k_postl<>wno_adr THEN DO:
                find first adresse where adresse.typ_fich="C" and adresse.cod_tiers=(if entetcli.cl_livre=0 THEN entetcli.cod_cli ELSE entetcli.cl_livre) AND adresse.affaire="" and adresse.cod_adr=wno_adr no-lock no-error.
                IF AVAIL adresse THEN SetSessionData("CALPX-VENTE":U,"ZoneGeo":U,(IF adresse.zon_geo= 0 THEN string(client.zon_geo)  ELSE string(adresse.zon_geo))).
            END.
        END.
    END.
    /*...$A35969*/

    EMPTY TEMP-TABLE TT-opt-ligne. /* $A41467 */

    /* Recalcul des prix */
    for each lignecli of entetcli:
        IF lignecli.SOUS_TYPE="HS" OR lignecli.SOUS_TYPE="AR" OR lignecli.sous_type="PS" THEN DO:

            IF vint = 2 AND (lignecli.forcer OR lignecli.gratuit) THEN NEXT. /*$A120503*/

            IF lignecli.cod_ori<>0 THEN FIND produit WHERE produit.cod_pro=lignecli.cod_ori NO-LOCK NO-ERROR.
            ELSE FIND produit WHERE produit.cod_pro=lignecli.cod_pro NO-LOCK NO-ERROR.

            /* Calcul prix vente,achat */
            wno_tarif=int(entetcli.no_tarif:SCREEN-VAL IN FRAME fc1).
            IF lignecli.dev_vte<>g-dftdev THEN FIND FIRST tabcomp
                WHERE tabcomp.TYPE_tab="DE" AND tabcomp.a_tab=lignecli.dev_vte NO-LOCK NO-ERROR.
            IF CAN-DO("C,P",entetcli.cal_marg:SCREEN-VAL) THEN RUN artphase (lignecli.typ_elem,OUTPUT cum-marge).

            if precach then do:
                IF lignecli.sous_type="HS" THEN ASSIGN
                    wpx_refa=lignecli.px_refa
                    wpx_refv=lignecli.px_refv.
                /* Remise quantit� : qt� U,C,S produit */
                wqv=lignecli.qte.
                IF lignecli.sous_type="AR" AND AVAIL produit THEN
                  wqv = UL_NbUE (lignecli.qte, lignecli.uni_vte, lignecli.nb_uv1, lignecli.nb_uv2, lignecli.nb_uv, lignecli.nb_cv3, lignecli.nb_cv) /
                        UL_NbUE (1, produit.uni_vte, produit.nb_uv1, produit.nb_uv2, produit.nb_uv, produit.nb_cv3, produit.nb_cv).

                run VALUE(chemin_calpx_t) (entetcli.cod_cli,no,entetcli.typ_cde,date(entetcli.dat_px:screen-value in frame fc1),input-output wno_tarif,
                                           entetcli.cat_tar:screen-value in frame fc2 + "|" + entetcli.canal:SCREEN-VAL IN FRAME fc1,entetcli.cat_cum:CHECKED,wls_tarif,wls_cumc,
                                           if entetcli.fac_pxa then "P" else entetcli.cal_marg,if can-do("C,P",entetcli.cal_marg:SCREEN-VAL) then cum-marge else entetcli.maj_ach,entetcli.borne,entetcli.fra_app,
                                           IF lignecli.cod_ori<>0 THEN lignecli.cod_ori ELSE lignecli.cod_pro,lignecli.cod_dec1,lignecli.cod_dec2,lignecli.cod_dec3,lignecli.cod_dec4,lignecli.cod_dec5,lignecli.fam_tar,lignecli.sf_tar,lignecli.s3_tar,lignecli.s4_tar,
                                           lignecli.depot,wqv,lignecli.cod_fou,lignecli.devise,lignecli.typ_cdef,
                                           input-output wpx_refv,output wpx_vte,output wdev_vte,input-output wpx_refa,output wpx_ach,output wpx_ach_d,output wrem1,output wrem2,output wrem3,output wrem4,
                                           output wremval1, output wremval2, output wremval3, output wremval4, output wpromo,output wremapp,
                                           /* $A60838... lignecli.k_var */ (IF lignecli.k_opt <> "" AND lignecli.k_var <> "" THEN SUBSTR (lignecli.k_opt,1,LENGTH (lignecli.k_opt) - 1) + lignecli.k_var ELSE lignecli.k_var + lignecli.k_opt) /* ...$A60838 */,ROWID(entetcli)).

               IF chemin_calpx_t = "calpx_t" AND atelier_sur_cde AND wrem1>0 THEN DO:
                   wrem1 = wrem1 - atepar.tx_pce.
                   IF wrem1 < 0 THEN wrem1 = 0.
               END.

               ASSIGN
                   lignecli.px_refv=wpx_refv
                   lignecli.px_vte=(IF lignecli.num_colis = "" THEN wpx_vte ELSE (wpx_refv * lignecli.volume) / lignecli.qte) /*$BOIS*/
                   lignecli.dev_vte=wdev_vte
                   lignecli.remise1=wrem1
                   lignecli.remise2=wrem2
                   lignecli.rem_app=wremapp
                   lignecli.remise3=wrem3
                   lignecli.remise4=wrem4 /*$1798...*/
                   lignecli.rem_val[1] = wremval1
                   lignecli.rem_val[2] = wremval2
                   lignecli.rem_val[3] = wremval3
                   lignecli.rem_val[4] = wremval4 /*...$1798*/
                   lignecli.px_refa=wpx_refa
                   lignecli.px_ach=wpx_ach
                   lignecli.px_ach_d=wpx_ach_d.
               IF lignecli.sous_type="AR" THEN
                   lignecli.pp_uv = produit.pp_uv.
               IF lignecli.sous_type="PS" THEN ASSIGN
                   lignecli.px_rvt=wpx_ach
                   lignecli.pmp=wpx_ach.

               /*A119346*/
               IF lignecli.nmc_lie="L" THEN ASSIGN
                   TypeGratuit = lignecli.typ_gra
                   LigneGratuit = lignecli.gratuit.
               {lignecl7.i lignecli} /* Extraction Wpromo */
               /*A119346*/
               IF lignecli.nmc_lie="L" AND LigneGratuit AND NOT lignecli.gratuit THEN ASSIGN
                   lignecli.gratuit = YES
                   lignecli.typ_gra = TypeGratuit.

               /*$A35969...*/
               IF parsoc.prix_franco AND AVAIL entetcli AND entetcli.prix_franco THEN
               DO:
                    ASSIGN lignecli.px_trs = (IF GetSessionData("calpx_t","Franco_trsp") = ? THEN 0 ELSE dec(GetSessionData("calpx_t","Franco_trsp")) /*GetSessionData("calpx_t","Franco_trsp")$mba*/ ).
                    SetSessionData("calpx_t","Franco_march","0").
                    SetSessionData("calpx_t","Franco_trsp","0").
                    recalculer-prix-franco = NO.
               END.
               /*...$A35969*/
            end.
            else IF entetcli.cal_marg:SCREEN-VAL <> "" THEN do:
                valopx=parsoc.valo_px.
                FOR FIRST cleval WHERE cleval.chapitre="PROGIWIN" AND cleval.domaine="MARGE_VTE" AND cleval.cod_user="PROGIWIN" AND cleval.section=STRING(entetcli.NO_cde) AND cleval.cle="valopx" NO-LOCK:
                    valopx=cleval.valeur.
                END.
                px-int = (IF valopx="M" THEN lignecli.pmp ELSE IF valopx="R" THEN lignecli.px_rvt ELSE lignecli.px_ach).
                if entetcli.cal_marg:SCREEN-VAL="P" then
                    lignecli.px_vte=round(px-int / (1 - (cum-marge / 100)),IF lignecli.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
                else if entetcli.cal_marg:SCREEN-VAL="C" then
                    lignecli.px_vte=ROUND((px-int * cum-marge) / 100,IF lignecli.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
                else if entetcli.cal_marg:SCREEN-VAL="V" then
                    lignecli.px_vte=px-int.
            end.

            /* $TX */
            IF lignecli.sous_type = "AR" AND lignecli.nb_uv > 1 THEN
                FOR FIRST tcprosai WHERE tcprosai.tccodpro = lignecli.cod_pro NO-LOCK, FIRST carton WHERE carton.cod_niv1 = lignecli.cod_dec3 NO-LOCK:
                ASSIGN
                    lignecli.px_refa  = (IF lignecli.nb_uv > 1 THEN (wpx_refa / lignecli.nb_uv) ELSE wpx_refa)
                    lignecli.px_ach   = (IF lignecli.nb_uv > 1 THEN (wpx_ach / lignecli.nb_uv) ELSE wpx_ach)
                    lignecli.px_ach_d = (IF lignecli.nb_uv > 1 THEN (wpx_ach_d / lignecli.nb_uv) ELSE wpx_ach_d).
            END.

            /* Facturation poids (convertir px poids en px pi�ce) */
            if parsoc.poi_var and not lignecli.fac_poi AND lignecli.sous_type = "AR" AND produit.px_poi THEN
                lignecli.px_vte=round(lignecli.px_vte * UL_PoiDBrutUE (produit.uni_vte, lignecli.depot,lignecli.cod_dec1,lignecli.cod_dec2,lignecli.cod_dec3,lignecli.cod_dec4,lignecli.cod_dec5,lignecli.k_var,
                                                                       ROWID(produit),ROWID(client),?),
                                      if lignecli.dev_vte<>g-dftdev and avail tabcomp then tabcomp.deb_ce else g-ndv).

            /* Facturation TTC */
            if entetcli.fac_ttc AND ((NOT parsoc.px_ttc and entetcli.regime<>9 AND lignecli.CODE_tva<>0)
                                     OR (parsoc.px_ttc AND lignecli.cod_pro>0 AND entetcli.regime<>1)) THEN DO:
                /* Recalcul du HT */
                IF parsoc.px_ttc AND AVAIL produit AND produit.CODE_tva<>0 THEN DO:
                        FIND LAST Tva WHERE Tva.ngr1 = g-tva-ngr1 AND Tva.ngr2 = g-tva-ngr2 AND Tva.ndos = g-tva-ndos
                            AND Tva.code_tva = produit.CODE_TVA AND (Tva.tva_date<=(IF entetcli.tva_date<>? THEN entetcli.tva_date ELSE entetcli.dat_liv) OR Tva.tva_date=?) NO-LOCK NO-ERROR.
                        if available tva AND tva.non_percue="" then lignecli.px_vte=round(lignecli.px_vte / ((tva.tx_tva / 100) + 1),IF lignecli.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
                END.

                /* Recalcul TTC */
                if parsoc.px_ttc AND (entetcli.regime=9 OR (AVAIL produit AND produit.code_tva=0)) then x=0.
                ELSE x=lignecli.code_tva.
                if x<>0 then do:
                    FIND LAST Tva WHERE Tva.ngr1 = g-tva-ngr1 AND Tva.ngr2 = g-tva-ngr2 AND Tva.ndos = g-tva-ndos
                        AND Tva.code_tva = X AND (Tva.tva_date<=(IF entetcli.tva_date<>? THEN entetcli.tva_date ELSE entetcli.dat_liv) OR Tva.tva_date=?) NO-LOCK NO-ERROR.
                    if avail tva AND tva.non_percue="" then lignecli.px_vte=round(lignecli.px_vte + ((lignecli.px_vte * tva.tx_tva) / 100),IF lignecli.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
                end.
            END.

            lignecli.forcer=NO.                                         

            {lignecl5.i lignecli entetcli} /* Total HT */
        END.
    end.

    /* $A41467... */
    /* on recalcul tous les produits � nomenclture commerciale avec pxvte, pxach  */
    IF precach THEN DO:
        FOR EACH lignecli OF entetcli WHERE lignecli.div_fac = 0 AND lignecli.nmc_lie = "" AND lignecli.lien_nmc <> 0 EXCLUSIVE-LOCK TRANSACTION:
            FIND FIRST produit WHERE produit.cod_pro = lignecli.cod_pro NO-LOCK NO-ERROR.
            IF AVAIL produit THEN RUN TRAITEMENT-COMPOSANTS. /* $A59286 */
        END.
    END.
    /* ...$A41467 */

END.
session:set-wait-state("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-btn wsaicl1
PROCEDURE refresh-btn :
ASSIGN Logbid = CBEnableItem (hComBars-f1, 2, {&bmodifier}, YES)
           Logbid = CBEnableItem (hComBars-f1, 2, {&bsupprimer}, YES)
           Logbid = CBShowItem (hComBars-f1, 2, {&bfax}, YES)
           Logbid = CBShowItem (hComBars-f1, 2, {&b-mail}, YES)
           Logbid = CBShowItem (hComBars-f1, 2, {&bsms}, YES) /* $A80432 */
           Logbid = CBShowItem (hComBars-f1, 2, {&bediter-interne}, droit-marge).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refresh-hbrowse wsaicl1
PROCEDURE refresh-hbrowse :
hbrowse:refresh().
    apply "value-changed" to hbrowse.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reposition-rowid wsaicl1
PROCEDURE reposition-rowid :
DEFINE INPUT  PARAMETER psvrowid AS ROWID      NO-UNDO.
   /* $2178... */
    ASSIGN
        svrowid = psvrowid
        toutes-saisies = YES
        qcondition = ?.
    /*RUN openquery.
 *     IF AVAIL entetcli THEN DO:
 *         IF hbrowse:NUM-ITERATIONS > 0 AND hbrowse:NUM-ITERATIONS<>? THEN hbrowse:SET-REPOSITIONED-ROW(hbrowse:NUM-ITERATIONS,"always").
 *         HQuery:REPOSITION-TO-ROWID(svrowid) NO-ERROR.
 *         APPLY "value-changed" TO hbrowse.
 *     END. $2240 */
    RUN reposition-rowid IN hapi-piece-cl (psvrowid). /* $2240 */
    /* ...$2178 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RESIZEWINDOW wsaicl1
PROCEDURE RESIZEWINDOW :
ADD-RESIZEWIDGET(FRAME f1:HANDLE, "WH").
ADD-RESIZEWIDGET(FRAME fc1:HANDLE, "WH").
ADD-RESIZEWIDGET(FRAME fc2:HANDLE, "WH").
ADD-RESIZEWIDGET(FRAME fval:HANDLE, "WH").
ADD-RESIZEWIDGET(FRAME fentete:HANDLE, "W").
ADD-RESIZEWIDGET(FRAME fonglet:HANDLE, "WH").
ADD-RESIZEWIDGET-P(FRAME FRAME-adrliv:HANDLE, 'XY',50,50). /* $A38935 */
ADD-RESIZEWIDGET(FrmComBars-f1, "W").
ADD-RESIZEWIDGET(RECT-25:HANDLE, "W").
ADD-RESIZEWIDGET(RECT-29:HANDLE, "W").
ADD-RESIZEWIDGET(Ctrlframe:HANDLE, "WH").
ADD-RESIZEWIDGET(FrmComBars-fentete, "W").
DO-RESIZELOAD().
RUN ERGO-PROC-WINDOW-RESIZED2.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ret-vcl wsaicl1
PROCEDURE ret-vcl :
DO WITH FRAME fc2:
    parcod=self:screen-value.
    IF Parcod = "" OR parcod = ? OR Parcod = "0" THEN CASE self:name:
        WHEN "vcl_grp"   then LIB-grp:screen-value   = Traduction("Inexistant",-2,"").
        WHEN "vcl_fact"  then LIB-fact:screen-value  = Traduction("Inexistant",-2,"").
        WHEN "vcl_livre" then LIB-livre:screen-value = Traduction("Inexistant",-2,"").
        WHEN "vcl_pln"   then LIB-pln:screen-value   = Traduction("Inexistant",-2,"").
        WHEN "vcl_plr"   then LIB-plr:screen-value   = Traduction("Inexistant",-2,"").
        WHEN "vcl_stat"  then LIB-stat:screen-value  = Traduction("Inexistant",-2,"").
        WHEN "vcl_paye"  then LIB-paye:screen-value  = Traduction("Inexistant",-2,"").
        OTHERWISE LIB-factor:screen-value = Traduction("Inexistant",-2,"").
    END CASE.
    ELSE run rechcli ("C","3,9",input-output parcod, output parlib).
     /* A48827 */
    CASE self:name:
        WHEN "vcl_grp" then DO:
            SELF:screen-value = parcod.
            IF int(parcod)<>0 THEN LIB-grp:screen-value = parlib.
            apply "tab" to brcgrp.
        END.
        WHEN "vcl_fact" then DO:
            IF int(vcl_fact)<>int(parcod) THEN RUN chg-fact.
            ASSIGN SELF:screen-value = parcod vcl_fact.
            IF int(parcod)<>0 THEN LIB-fact:screen-value = parlib.
            apply "tab" to brcfact.
        END.
        WHEN "vcl_livre" then DO:
            IF int(vcl_livre)<>int(parcod) THEN DO:
                IF NOT creation THEN ChangementClientLivre = YES. /*$A65359*/
                RUN chg-livre.
            END.
            ASSIGN SELF:screen-value = parcod vcl_livre.
            IF int(parcod)<>0 THEN LIB-livre:screen-value = parlib.
            apply "tab" to brclivre.
            IF ChangementClientLivre THEN RUN Choose-badrliv. /*$A65359*/
        END.
        WHEN "vcl_pln" then DO:
            SELF:screen-value = parcod.
            IF int(parcod)<>0 THEN LIB-pln:screen-value = parlib.
            apply "tab" to brcpln.
        END.
        WHEN "vcl_plr" then DO:
            SELF:screen-value = parcod.
            IF int(parcod)<>0 THEN LIB-plr:screen-value = parlib.
            apply "tab" to brcplr.
        END.
        WHEN "vcl_stat" then DO:
            SELF:screen-value = parcod.
            IF int(parcod)<>0 THEN LIB-stat:screen-value = parlib.
            apply "tab" to brcstat.
        END.
        WHEN "vcl_paye" then DO:
            IF int(vcl_paye)<>int(parcod) THEN RUN chg-paye.
            ASSIGN SELF:screen-value = parcod vcl_paye.
            IF int(parcod)<>0 THEN LIB-paye:screen-value = parlib.
            apply "tab" to brcpaye.
        END.
        OTHERWISE DO:
            SELF:screen-value = parcod.
            IF int(parcod)<>0 THEN LIB-factor:screen-value = parlib.
            apply "tab" to brcfactor.
        END.
    END CASE.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE return-affaire wsaicl1
PROCEDURE return-affaire :
DEF VAR majof AS LOG INIT NO no-undo.
DO with frame fc1:
    /*$1027*/
    IF NOT creation AND INPUT entetcli.affaire <> entetcli.affaire THEN
    DO:
        FIND FIRST prefab WHERE prefab.cod_cli = entetcli.cod_cli AND prefab.NO_cde = entetcli.NO_cde NO-LOCK NO-ERROR.
        IF AVAIL prefab THEN
            majof = YES.
    END.
    /*fin $1027*/

    if input entetcli.affaire <> "" then do:
        /*$166*/
        if not creation and entetcli.affaire<>"" then do:
            find first ligaffai where ligaffai.affaire=entetcli.affaire and ligaffai.no_cde=entetcli.no_cde no-lock no-error.
            if available ligaffai then do:
                message Traduction("Vous ne pouvez plus changer d'affaire",-2,"") skip Traduction("Des lignes ont d�j� �t� renseign�es",-2,"") view-as alert-box error.
                entetcli.affaire:screen-value=entetcli.affaire.
                apply "entry" to entetcli.affaire.
                return no-apply.
            end.
        end.
        /*fin $166*/
        parcod = input entetcli.affaire.
        RUN AFF_GET_COD_LIB IN G-hAff(INPUT-OUTPUT parcod, OUTPUT parlib, YES, "", OUTPUT logbid).
        IF logbid THEN FIND FIRST affaire WHERE affaire.affaire = parcod NO-LOCK NO-ERROR.
        IF logbid AND AVAIL affaire THEN DO:
            /*IF affaire.dat_fer <> ? AND affaire.dat_fer < TODAY THEN MESSAGE Traduction("L'affaire est ferm�e !",-2,"") VIEW-AS ALERT-BOX WARNING. $A62274 */
            assign entetcli.affaire:screen-value = affaire.affaire
                   lib-aff:screen-value = affaire.int_aff
                   bcreer-aff:sensitive = no.
        END.
        else assign lib-aff:screen-value = Traduction("Inexistante",-2,"") bcreer-aff:sensitive = yes.

        /*$166 - Verifier client */
        if available affaire and affaire.cod_cli <> int(entetcli.cod_cli:screen-val in frame Fonglet) then do:
            message Traduction("Le client de commande et celui de l'affaire sont diff�rents. Voulez-vous quand m�me conserver ce n� ?",-2,"")
            view-as alert-box question buttons yes-no update logbid.
            if not logbid then do:
                /* $A54465 ...
                find affaire where affaire.affaire = input entetcli.affaire no-lock no-error.
                assign entetcli.affaire:screen-value=entetcli.affaire
                       lib-aff:screen-value=(if available affaire then affaire.int_aff else Traduction("Inexistante",-2,"")).
                */
                IF creation THEN ASSIGN entetcli.affaire:SCREEN-VALUE = "" lib-aff:SCREEN-VALUE = "".
                ELSE DO:
                    find affaire where affaire.affaire = entetcli.affaire no-lock no-error.
                    IF AVAIL affaire THEN ASSIGN entetcli.affaire:SCREEN-VALUE = affaire.affaire lib-aff:SCREEN-VALUE = affaire.int_aff.
                    ELSE ASSIGN entetcli.affaire:SCREEN-VALUE = "" lib-aff:SCREEN-VALUE = "".
                END.
                /* ... $A54465 */
            end.
        end.
        /*fin $166*/
    end.
    else do:
        /*$166*/
        if not creation and entetcli.affaire <> "" then do:
            find first ligaffai where ligaffai.affaire=entetcli.affaire and ligaffai.no_cde=entetcli.no_cde no-lock no-error.
            if available ligaffai then do:
                message Traduction("Vous ne pouvez plus dissocier l'affaire",-2,"") skip Traduction("Des lignes ont d�j� �t� renseign�es",-2,"") view-as alert-box error.
                entetcli.affaire:screen-value=entetcli.affaire.
                apply "entry" to entetcli.affaire.
                return no-apply.
            end.
        end.
        /*fin $166*/
        assign lib-aff:screen-value = ""
               bcreer-aff:sensitive = yes.
    end.
    IF NOT creation AND INPUT entetcli.affaire <> entetcli.affaire AND majof THEN
    DO:
        FOR EACH prefab WHERE prefab.cod_cli = entetcli.cod_cli AND prefab.NO_cde = entetcli.NO_cde SHARE-LOCK TRANSACTION :
            prefab.affaire = INPUT entetcli.affaire.
            FOR EACH histolig where histolig.NO_of = prefab.NO_of SHARE-LOCK : /*$bob17*/
                IF histolig.typ_mvt = "P" OR histolig.typ_mvt = "V" THEN
                    histolig.affaire = INPUT entetcli.affaire.
            END.
        END.
    END.
    if last-event:function="return" then do:
        apply "tab" to br-aff.
        return no-apply.
    end.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SAISON-NATURE wsaicl1
PROCEDURE SAISON-NATURE :
FIND FIRST tabgco WHERE tabgco.type_tab = "ss" AND tabgco.NO_fax[1] = entetcli.nat_cde:SCREEN-VALUE IN FRAME fc1 NO-LOCK NO-ERROR.
    IF NOT AVAILABLE tabgco THEN FIND FIRST tabgco WHERE tabgco.type_tab = "ss" AND tabgco.NO_fax[2] = entetcli.nat_cde:SCREEN-VALUE IN FRAME fc1 NO-LOCK NO-ERROR.
    IF AVAILABLE tabgco THEN do:
        /*A35891*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("SS", tabgco.a_tab, g-langue).
        IF intiTab <> "" THEN lib-ss:SCREEN-VALUE IN FRAME fc2 = intiTab.
        ELSE lib-ss:SCREEN-VALUE IN FRAME fc2 = tabgco.inti_tab.

        ASSIGN entetcli.saison:SCREEN-VALUE IN FRAME fc2 = tabgco.a_tab.
               /*lib-ss:SCREEN-VALUE IN FRAME fc2 = tabgco.inti_tab.*/
        RUN DATPX-SAISON.
    END.
    ELSE ASSIGN entetcli.saison:SCREEN-VALUE IN FRAME fc2 = ""
                lib-ss:SCREEN-VALUE IN FRAME fc2 = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SET-VARIABLE wsaicl1
PROCEDURE SET-VARIABLE :
/* $A38744 */
    DEF INPUT  PARAM pvariable AS CHAR  NO-UNDO.
    DEF INPUT  PARAM pvaleur   AS CHAR  NO-UNDO.

    /* Permet de modifier une valeur depuis un autre programme sans rajouter de param�tre */
    /* dans l'autre programme :
       DEF VAR hparent AS HANDLE     NO-UNDO.
       hparent = SOURCE-PROCEDURE.
       IF CAN-DO(hparent:INTERNAL-ENTRIES,"SET-VARIABLE") THEN RUN SET-VARIABLE IN hparent ("retour","YES").
     */

    CASE pvariable :
        WHEN "retour":U THEN retour = LOGICAL(pvaleur).
        WHEN "qui":U THEN ASSIGN qui$ = pvaleur vqui = pvaleur /*$A64842*/.
        WHEN "depot":U  THEN DO:
            IF pvaleur <> "" AND pvaleur <> ? AND LOOKUP(pvaleur,edepot:LIST-ITEM-PAIRS IN FRAME FC1) > 0 THEN edepot:SCREEN-VAL IN FRAME Fc1 = pvaleur. /* $A40401 */
            if edepot:NUM-ITEMS > 1 AND AVAIL client AND client.depot > 0 AND LOOKUP(STRING(client.depot),edepot:LIST-ITEM-PAIRS) > 0 THEN edepot:SCREEN-VAL = STRING(client.depot).
        END.
    END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE specif-entete wsaicl1
PROCEDURE specif-entete :
DO WITH FRAME FC1:
    display entetcli.dat_cde entetcli.dat_liv entetcli.dat_px.
    ASSIGN
        edepot:screen-val=string(entetcli.depot)
        entetcli.nat_cde:screen-val=entetcli.nat_cde
        entetcli.canal:screen-val=entetcli.canal
        entetcli.devise:screen-val=entetcli.devise
        entetcli.semaine:SCREEN-VAL=STRING(entetcli.semaine)
        entetcli.regime:screen-val=string(entetcli.regime)
        entetcli.type_fac:screen-val=entetcli.type_fac
        entetcli.code_reg:screen-val=string(entetcli.code_reg)
        entetcli.code_ech:screen-val=string(entetcli.code_ech)
        entetcli.ref_cde:screen-val=entetcli.ref_cde
        entetcli.langue:screen-val=entetcli.langue
        entetcli.paysf:screen-val=entetcli.paysf
        entetcli.civ_fac:screen-val=entetcli.civ_fac
        entetcli.adr_fac[1]:screen-val=entetcli.adr_fac[1]
        entetcli.k_post2f:screen-val=entetcli.k_post2f
        entetcli.adr_fac[2]:screen-val=entetcli.adr_fac[2]
        entetcli.adr_fac[3]:screen-val=entetcli.adr_fac[3]
        entetcli.adrfac4:SCREEN-VAL=entetcli.adrfac4
        entetcli.villef:screen-val=entetcli.villef
        entetcli.not_ref:screen-val=entetcli.not_ref
        entetcli.fac_dvs:screen-val=string(entetcli.fac_dvs,Traduction("oui/non",-2,""))
        entetcli.tx_ech_d:screen-val=string(entetcli.tx_ech_d)
        entetcli.groupe:screen-val= entetcli.groupe
        entetcli.famille:screen-val= entetcli.famille
        entetcli.s2_famille:screen-val= entetcli.s2_famille
        entetcli.s3_famille:screen-val= entetcli.s3_famille
        entetcli.s4_famille:screen-val= entetcli.s4_famille
        entetcli.region:screen-val=string(entetcli.region)
        entetcli.commerc[1]:screen-val=entetcli.commerc[1]
        entetcli.app_aff:screen-val=entetcli.app_aff
        entetcli.ori_cde:screen-val=entetcli.ori_cde
        entetcli.rem_glo[1]:screen-val=string(entetcli.rem_glo[1])
        entetcli.rem_glo[2]:screen-val=string(entetcli.rem_glo[2])
        entetcli.cal_marg:screen-val=entetcli.cal_marg
        entetcli.affaire:screen-val=entetcli.affaire
        entetcli.no_tarif:screen-val=string(entetcli.no_tarif)
        entetcli.location:SCREEN-VAL=STRING(entetcli.location)
        entetcli.deb_loc:SCREEN-VAL=STRING(entetcli.deb_loc)
        entetcli.fin_loc:SCREEN-VAL=STRING(entetcli.fin_loc)
        entetcli.dur_loc:SCREEN-VAL=STRING(entetcli.dur_loc).
END.
DO WITH FRAME FC2:
    assign
        entetcli.num_tel:screen-val=entetcli.num_tel
        entetcli.num_fax:screen-val= entetcli.num_fax
        entetcli.taux_esc:screen-val=string(entetcli.taux_esc)
        entetcli.liasse:screen-val=string(entetcli.liasse)
        entetcli.lias_bl:screen-val=string(entetcli.lias_bl) /* $A59971 */
        entetcli.cat_tar:screen-val=entetcli.cat_tar
        entetcli.borne:screen-val=string(entetcli.borne)
        entetcli.fac_ttc:screen-val=string(entetcli.fac_ttc,"oui/non")
        entetcli.fac_pxa:screen-val=string(entetcli.fac_pxa,"oui/non")
        entetcli.commerc[2]:screen-val=entetcli.commerc[2]
        entetcli.cpt_bq:SCREEN-VAL=STRING(entetcli.cpt_bq)
        entetcli.proforma:screen-val=string(entetcli.proforma,"oui/non")
        entetcli.fra_app:screen-val=string(entetcli.fra_app,"oui/non")
        entetcli.chif_bl:screen-val=string(entetcli.chif_bl,"oui/non")
        entetcli.edt_arc:screen-val=string(entetcli.edt_arc,"oui/non")
        entetcli.liv_cpl:screen-val=string(entetcli.liv_cpl,"yes/no")
        entetcli.cde_cpl:screen-val=string(entetcli.cde_cpl,"yes/no")
        entetcli.prep_isol:screen-val=string(entetcli.prep_isol,"yes/no")
        entetcli.plus_bpbl:screen-val=string(entetcli.plus_bpbl,"yes/no")
        entetcli.bl_regr:screen-val=string(entetcli.bl_regr,"yes/no")
        vcl_stat:screen-val=string(entetcli.cl_stat)
        entetcli.dat_ech:screen-val=string(entetcli.dat_ech)
        entetcli.tva_date:screen-val=string(entetcli.tva_date)
        entetcli.maj_ach:screen-val=string(entetcli.maj_ach)
        vcl_livre:screen-val=string(entetcli.cl_livre)
        vcl_grp:screen-val=string(entetcli.cl_grp)
        vcl_pln:screen-val=string(entetcli.cl_plnat)
        vcl_plr:screen-val=string(entetcli.cl_plreg)
        vcl_paye:screen-val=string(entetcli.cl_paye)
        vcl_fact:screen-val=string(entetcli.cl_fact)
        vc_factor:screen-val=string(entetcli.c_factor)
        entetcli.tar_fil[1]:screen-value=string(entetcli.tar_fil[1],"yes/no")
        entetcli.tar_fil[2]:screen-value=string(entetcli.tar_fil[2],"yes/no")
        entetcli.tar_fil[3]:screen-value=string(entetcli.tar_fil[3],"yes/no")
        entetcli.tar_fil[4]:screen-value=string(entetcli.tar_fil[4],"yes/no")
        entetcli.tar_fil[5]:screen-value=string(entetcli.tar_fil[5],"yes/no")
        entetcli.tar_fil[6]:screen-value=string(entetcli.tar_fil[6],"yes/no")
        entetcli.tar_cum[1]:screen-value=string(entetcli.tar_cum[1],"yes/no")
        entetcli.tar_cum[2]:screen-value=string(entetcli.tar_cum[2],"yes/no")
        entetcli.tar_cum[3]:screen-value=string(entetcli.tar_cum[3],"yes/no")
        entetcli.tar_cum[4]:screen-value=string(entetcli.tar_cum[4],"yes/no")
        entetcli.tar_cum[5]:screen-value=string(entetcli.tar_cum[5],"yes/no")
        entetcli.tar_cum[6]:screen-value=string(entetcli.tar_cum[6],"yes/no")
        entetcli.cat_cum:screen-value=string(entetcli.cat_cum,"yes/no").
END.
/* Valeurs libres --> Chapitre 2 */
DO with frame fval:
    /* Valeurs libres */
    do x=1 to 5:
       ASSIGN
           wn[x]=entetcli.znu[x]
           wa[x]=entetcli.zal[x]
           wt[x]=entetcli.zta[x]
           wd[x]=entetcli.zda[x]
           wl[x]=entetcli.zlo[x]
           zah[X]:SCREEN-VAL=wa[x]
           znh[X]:SCREEN-VAL=string(wn[x])
           zdh[X]:SCREEN-VAL=string(wd[x])
           zlh[X]:SCREEN-VAL=string(wl[x])
           zth[X]:SCREEN-VAL=wt[x].
    END.
    run val-lib-dep.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE suppression-piece wsaicl1
PROCEDURE suppression-piece :
DEF VAR vCCReliq AS LOG    NO-UNDO.
    DEFINE BUFFER b$entetcli    FOR entetcli.
    DEFINE BUFFER b$histoent    FOR histoent.

    /* Multi-soci�t� ? */
    IF substr(entetcli.caution,1,3)="$$$" THEN DO:
        FIND FIRST parms WHERE parms.cod_cl2=entetcli.cod_cli AND parms.cod_fo2>0 NO-LOCK NO-ERROR.
        IF AVAIL parms THEN MESSAGE SUBSTITUTE(Traduction("Voulez-vous �galement supprimer la commande fournisseur associ�e sur la soci�t� &1 ?",-2,""),caps(parms.nom_dos))
            view-as alert-box question buttons yes-no update retour.
    END.
    ELSE IF substr(entetcli.caution,1,3)="$$I" OR (entetcli.retr_cess<>"" AND entetcli.no_devis>0) THEN DO:
        vCCReliq = (   entetcli.typ_sai = "C" AND entetcli.no_bl = 0
                    AND (   CAN-FIND (FIRST b$entetcli WHERE b$entetcli.cod_cli = entetcli.cod_cli AND b$entetcli.typ_sai = "F" AND b$entetcli.NO_cde = entetcli.NO_cde NO-LOCK)
                         OR CAN-FIND (FIRST b$histoent WHERE b$histoent.typ_mvt = "C" AND b$histoent.cod_cf = entetcli.cod_cli AND b$histoent.NO_cde = entetcli.NO_cde NO-LOCK))).
        FIND entetfou WHERE entetfou.regr=entetcli.NO_cde AND entetfou.typ_sai="C" AND entetfou.retr_cess<>"" NO-LOCK NO-ERROR.
        IF AVAIL entetfou THEN DO:
            IF vCCReliq THEN vCCReliq = NOT CAN-FIND(FIRST b$histoent WHERE b$histoent.typ_mvt = "F" AND b$histoent.cod_cf = entetfou.NO_cde AND b$histoent.retr_cess <> "" NO-LOCK).
            IF vCCReliq
            THEN MESSAGE Traduction("Voulez-vous effacer les reliquats sur la commande fournisseur associ�e ?",-2,"")
                view-as alert-box question buttons yes-no update retour.
            ELSE MESSAGE Traduction("Voulez-vous �galement supprimer la commande fournisseur associ�e ?",-2,"")
                view-as alert-box question buttons yes-no update retour.
        END.
    END.

    SESSION:SET-WAIT-STATE("general").
    PgmSpec = RechPgmSpeGco("s-supcl.r").
    IF pgmSpec <> ? THEN
        RUN VALUE(pgmSpec) (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,output vchar).
    /* $A99522 ... */
    IF CONNECTED ("WORKFLOW") AND SEARCH("workflow\execut_wf.r") <> ? THEN
        RUN VALUE (PropWF + "\execut_wf") (NO, 0, "PROGIWIN", "*SAISIE VENTE", YES, "GCO.entetcli" + CHR(1) + STRING(ROWID(entetcli)),"S","",OUTPUT pretwf).
    if CAN-DO("F,R",entetcli.typ_sai) AND entetcli.fac_edi=yes and entetcli.no_fact<>0 THEN RUN test-regroupe.
    IF retour AND substr(entetcli.caution,1,3)="$$$" THEN
        run conncl_ms (ROWID(entetcli),ROWID(parms),2).
    ELSE IF retour AND (substr(entetcli.caution,1,3)="$$I" OR (entetcli.retr_cess<>"" AND entetcli.no_devis>0)) THEN DO:
        DO TRANSACTION:
            FIND CURRENT entetfou EXCLUSIVE-LOCK NO-ERROR. /* �XREF_NOWHERE� */
            IF AVAIL entetfou THEN DO:
                IF vCCReliq THEN DO:
                    entetfou.mnt_rlk = NO.
                    RELEASE entetfou.
                END.
                ELSE DO:
                    /* M�morisation infos avant suppression pour trigger delef_db */
                    entetfou.com_deb = STRING("SAICL1_C","X(12)") + "INT$��".
                    DELETE entetfou.
                END.
            END.
        END.
    END.
    DO TRANSACTION:
        /* M�morisation infos avant suppression pour trigger delec_db */
        entetcli.contrat=STRING("SAICL1_C","X(12)") + "INT$��".
        DELETE entetcli.
    END.
    /* ... $A99522 */
    SESSION:SET-WAIT-STATE ("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-regroupe wsaicl1
PROCEDURE test-regroupe :
/* Modif de tous les BL si facture regroup�e et �dit�e */
for each entetcli2 where entetcli2.ndos=entetcli.ndos AND entetcli2.fac_maj=no and entetcli2.no_fact=entetcli.no_fact
  and (entetcli2.no_cde<>entetcli.no_cde or entetcli2.no_bl<>entetcli.no_bl) exclusive-lock:
    entetcli2.fac_edi=no.
    {Statut_Pce_C.i entetcli2}
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-sup wsaicl1
PROCEDURE test-sup :
DEFINE VARIABLE numerr AS INTEGER    NO-UNDO.
if (entetcli.typ_sai="F" or entetcli.typ_sai="R") and entetcli.no_fact<>0 and can-find(first droigco where droigco.util=g-user-droit and droigco.fonc="SUPPFAC") then
        message SUBSTITUTE(Traduction("Suppression commande &1 impossible.",-2,""),string(entetcli.no_cde)) skip
            Traduction("Vous n'�tes pas autoris� � supprimer une facture.",-2,"") view-as alert-box info.
    else if (entetcli.typ_sai="F" or entetcli.typ_sai="R") and can-find(first droigco where droigco.util=g-user-droit and droigco.fonc="SUPPBL") then
        message SUBSTITUTE(Traduction("Suppression commande &1 impossible.",-2,""),string(entetcli.no_cde)) skip
            Traduction("Vous n'�tes pas autoris� � supprimer un B.L",-2,"") view-as alert-box info.
    else if entetcli.typ_sai="C" and can-find(first droigco where droigco.util=g-user-droit and droigco.fonc="SUPPCC") then
        message SUBSTITUTE(Traduction("Suppression commande &1 impossible.",-2,""),string(entetcli.no_cde)) skip
            Traduction("Vous n'�tes pas autoris� � supprimer une commande",-2,"") view-as alert-box info.
    /*$BOIS...*/
    ELSE IF parsoc.bois_ar THEN DO:
        IF Verif-suppr-entetcli-bois(ROWID(entetcli),OUTPUT numerr) THEN RETURN "11".
        ELSE CASE Numerr:
            WHEN 1 THEN MESSAGE Traduction("Une partie ou la totalit� d'une ligne d'avoir a d�j� �t� refactur�e ou transform�e, vous ne pouvez pas supprimer cet avoir.",-2,"") VIEW-AS ALERT-BOX ERROR.
        END CASE.
    END.
    /*...$BOIS*/
    ELSE RETURN "11".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TRAITEMENT-COMPOSANTS wsaicl1
PROCEDURE TRAITEMENT-COMPOSANTS :
/* $A41467 */

    DEF BUFFER lignecli2 FOR lignecli.
    DEFINE VARIABLE passe    AS LOGICAL    NO-UNDO.
    DEFINE VARIABLE nmpx_vte AS DECIMAL    NO-UNDO.
        DEFINE VARIABLE wqv      AS DECIMAL    NO-UNDO.
        /* $A59286... */
    DEFINE VARIABLE nmpx_ach AS DECIMAL    NO-UNDO.
    DEFINE VARIABLE nmpx_rvt AS DECIMAL    NO-UNDO.
    DEFINE VARIABLE nmpx_pmp AS DECIMAL    NO-UNDO.
    DEFINE VARIABLE MoLigne  AS LOGICAL    NO-UNDO.

    IF CAN-FIND(FIRST prnomenc WHERE prnomenc.type_nmc = "C" AND prnomenc.cod_pro = lignecli.cod_pro AND prnomenc.mo_ensai /*$A61522*/ NO-LOCK) AND
        NOT CAN-FIND(FIRST prnomenc WHERE prnomenc.type_nmc = "C" AND prnomenc.cod_pro = lignecli.cod_pro AND prnomenc.mo_ensai /*$A61522*/ = NO NO-LOCK) THEN MoLigne = YES.
    /* ...$A59286 */
    IF NOT CAN-FIND(FIRST TT-opt-ligne NO-LOCK) THEN FOR EACH lignecli2 FIELDS(heure lien_nmc nmc_lie) of entetcli where lignecli2.nmc_lie="S" NO-LOCK :
        CREATE TT-opt-ligne.
        ASSIGN
            TT-opt-ligne.lien_nmc  = lignecli2.lien_nmc
            TT-opt-ligne.lig_rowid = ROWID(lignecli2)
            TT-opt-ligne.nmc_lie   = lignecli2.nmc_lie.
        VALIDATE TT-opt-ligne.
    END.

    FOR EACH tt-opt-ligne WHERE tt-opt-ligne.nmc_lie = "S" AND tt-opt-ligne.lien_nmc = lignecli.lien_nmc, FIRST lignecli2 WHERE ROWID(lignecli2) = tt-opt-ligne.lig_rowid NO-LOCK :
                /* $A59286... */
        ASSIGN
            nmpx_ach = nmpx_ach + ROUND((IF NOT lignecli2.gratuit THEN lignecli2.qte_dejl * lignecli2.px_ach ELSE 0),g-nda)
            nmpx_rvt = nmpx_rvt + ROUND((IF NOT lignecli2.gratuit THEN lignecli2.qte_dejl * lignecli2.px_rvt ELSE 0),g-nda)
            nmpx_pmp = nmpx_pmp + ROUND((IF NOT lignecli2.gratuit THEN lignecli2.qte_dejl * lignecli2.pmp ELSE 0),g-nda)
            nmpx_vte = nmpx_vte + ROUND((IF NOT lignecli2.gratuit THEN lignecli2.qte_dejl * lignecli2.px_vte ELSE 0),g-ndv)
            wqv = IF lignecli2.qte_dejl <> 0 THEN lignecli2.qte_dejl ELSE lignecli2.qte
            passe = YES.
                /* ...$A59286 */
    END.

    IF passe THEN DO:
                /* $A59286... */
        IF CAN-DO("1,4",STRING(produit.ordre,"9")) THEN
            ASSIGN
                lignecli.px_ach = (IF MoLigne THEN lignecli.px_ach ELSE 0) + nmpx_ach
                lignecli.px_rvt = (IF MoLigne THEN lignecli.px_rvt ELSE 0) + nmpx_rvt
                lignecli.pmp    = (IF MoLigne THEN lignecli.pmp ELSE 0) + nmpx_pmp.
        IF CAN-DO("1,3",STRING(produit.ordre,"9")) THEN lignecli.px_vte = (IF MoLigne THEN lignecli.px_vte ELSE 0) + nmpx_vte. /* ...$A59286 */
        {lignecl5.i lignecli entetcli} /* Total HT */
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE trt-adresse wsaicl1
PROCEDURE trt-adresse :
X = (IF INT(vcl_fact:SCREEN-VAL IN FRAME fc2)<>0 THEN INT(vcl_fact:SCREEN-VAL IN FRAME fc2) ELSE cle-cours).
run recad_b ("C","F",X,entetcli.adr_fac[1]:screen-val in frame fc1,yes,output parlib, output parnum,output pcores).
if parnum <> 0 then do:
    find adresse where adresse.typ_fich="C" and cod_tiers=X AND adresse.affaire="" and cod_adr=parnum no-lock no-error.
    find TABCOMP where TABCOMP.TYPE_TAB = "CI" and TABCOMP.A_TAB = adresse.civilite no-lock no-error.
    if available tabcomp THEN DO:
        /*A35891... LIB-CI = TABCOMP.inti_tab. else LIB-CI = Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("CI", TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN LIB-CI = intiTab.
        ELSE LIB-CI = TABCOMP.inti_tab.
    END.
    else LIB-CI = Traduction("Inexistant",-2,"").
    find TABCOMP where TABCOMP.TYPE_TAB = "PA" and TABCOMP.A_TAB = adresse.pays no-lock no-error.
    if available tabcomp THEN DO:
        /*A35891... LIB-pa = TABCOMP.inti_tab. else LIB-pa = Traduction("Inexistant",-2,"").*/
        IF g-langue <> "" THEN intiTab = GetIntituleTableLangue ("PA":U, TABCOMP.a_tab, g-langue).
        IF intiTab <> "" THEN LIB-pa = intiTab.
        ELSE LIB-pa = TABCOMP.inti_tab.
    END.
    else LIB-pa = Traduction("Inexistant",-2,"").

    ASSIGN
        entetcli.num_tel:SCREEN-VAL IN FRAME fc2=adresse.num_tel
        entetcli.num_fax:SCREEN-VAL IN FRAME fc2=adresse.num_fax.
    display
        adresse.civilite @ entetcli.civ_fac lib-ci adresse.nom_adr @ entetcli.adr_fac[1]
        adresse.adresse[1] @ entetcli.adr_fac[2] adresse.adresse[2] @ entetcli.adr_fac[3] adresse.adresse4 @ entetcli.adrfac4
        adresse.k_post2 @ entetcli.k_post2f adresse.ville @ entetcli.villef
        adresse.pays @ entetcli.paysf lib-pa with frame fc1.
    CodePostal = entetcli.k_post2f:SCREEN-VAL.

    Adr_fac2cde = (adresse.adr_fac AND (NOT adresse.adr_fdef) AND adresse.adr_exp). /*$A60003 on force l'adresse de commande si on a s�lectionn� un adresse de facturation qui n'est pas la principale et que c'est aussi une adresse de commande*/
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Trt-deltra wsaicl1
PROCEDURE Trt-deltra :
DEFINE VARIABLE Wtranspor AS INTEGER    NO-UNDO.

    Wtranspor = IF creation THEN (IF adresse.transpor=0 THEN client.transpor ELSE adresse.transpor) /*$1728 client.transpor*/ ELSE entetcli.transpor.
    IF creation AND pref_parcde_transpor THEN DO:
        FIND FIRST depcli WHERE depcli.cod_cli = client.cod_cli AND depcli.depot = INT(edepot:SCREEN-VAL IN FRAME FC1) NO-LOCK NO-ERROR.
        IF AVAIL depcli AND depcli.transpor > 0 THEN Wtranspor = depcli.transpor.
    END.

    /* D�lai de transport par d�p�t/transporteur */
    FIND FIRST deltra where deltra.vente   = YES
                        and deltra.depot   = INT(edepot:SCREEN-VAL IN FRAME FC1)
                        and deltra.cod_cli = adresse.cod_tiers
                        and deltra.cod_adr = adresse.cod_adr
                        and deltra.cod_fou = Wtranspor NO-LOCK NO-ERROR.
    IF NOT AVAIL deltra
        THEN FIND FIRST deltra where deltra.vente   = YES
                                 and deltra.depot   = INT(edepot:SCREEN-VAL IN FRAME FC1)
                                 and deltra.cod_cli = adresse.cod_tiers
                                 and deltra.cod_adr = adresse.cod_adr
                                 and deltra.cod_fou = 0 NO-LOCK NO-ERROR.
    IF NOT AVAIL deltra
        THEN FIND FIRST deltra where deltra.vente   = YES
                                 and deltra.depot   = 0
                                 and deltra.cod_cli = adresse.cod_tiers
                                 and deltra.cod_adr = adresse.cod_adr
                                 and deltra.cod_fou = Wtranspor NO-LOCK NO-ERROR.
    IF AVAIL deltra THEN wdelai_trs = deltra.delai_trs.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE trt-supprimer wsaicl1
PROCEDURE trt-supprimer :
DO WITH FRAME f1:
if (entetcli.typ_sai="F" or entetcli.typ_sai="R") and entetcli.no_fact<>0 and entetcli.fac_maj=yes then
    message SUBSTITUTE(Traduction("Suppression commande &1 impossible.",-2,""),string(entetcli.no_cde)) skip
    SUBSTITUTE(Traduction("La facture &1 est valid�e.",-2,""),string(entetcli.no_fact)) view-as alert-box info.
ELSE IF (entetcli.typ_sai="F" or entetcli.typ_sai="R") AND ((parsoc.vfm_deb <> ? AND entetcli.dat_liv < parsoc.vfm_deb) OR (parsoc.vfm_fin <> ? AND entetcli.dat_liv > parsoc.vfm_fin)) THEN DO: /*$A26501*/
    message SUBSTITUTE(Traduction("Suppression commande &1 impossible.",-2,""),string(entetcli.no_cde)) skip
        Traduction("date livraison",-2,"") + " " + Traduction("en dehors de la p�riode de validit� mouvements",-2,"") view-as alert-box info.
END.
ELSE DO:
    RUN test-sup.
    IF RETURN-VALUE="11" THEN
    DO:
        IF AVAIL parsoc AND parsoc.gpao AND entetcli.typ_sai = "C" THEN
        DO:
            RUN ControleProduction (OUTPUT logbid).
            IF logbid = NO THEN RETURN.
        END.
        assign
            logbid=yes
            retour=NO.
        message SUBSTITUTE(Traduction("Voulez-vous supprimer la pi�ce &1 ?",-2,""),entetcli.no_cde:screen-value in frame Fonglet)
          view-as alert-box question buttons yes-no update logbid.
        if logbid then DO TRANSACTION:
          RUN suppression-piece.
          if porigine<>"4" AND porigine <> "6a" then do:
            hbrowse:delete-current-row().
            if hbrowse:num-iterations>0 then RUN refresh-hbrowse IN hapi-piece-cl. /* $2240 */
            else DO:
                RUN cacher-champs.
            END.
            RUN Ecran1_Ok.
            if creation then apply "entry" to cli$.
            else apply "entry" to hbrowse.
          end.
          else apply "close" to this-procedure.
        end.
    END.
END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE trt-tarif wsaicl1
PROCEDURE trt-tarif :
DO WITH FRAME fc1:
    find tarif WHERE tarif.location = (parsoc.ges_loc /*AND type-saisie = "D" $A50403*/ AND entetcli.location:checked) AND tarif.no_tarif=int(entetcli.no_tarif:screen-val) no-lock no-error.
    IF AVAIL tarif THEN DO:
        IF tarif.devise<>"" and tarif.devise<>g-dftdev THEN DO:
            ASSIGN entetcli.fac_dvs:SCREEN-VAL="oui" entetcli.devise:SCREEN-VAL=tarif.devise.
            APPLY "value-changed" TO fac_dvs.
        END.
        ELSE ASSIGN entetcli.fac_dvs:SCREEN-VAL="non" entetcli.devise:SCREEN-VAL=g-dftdev
            entetcli.tx_ech_d:HIDDEN=YES entetcli.devise:HIDDEN=YES br-de:HIDDEN=YES bappdev:HIDDEN=YES.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE val-fc1 wsaicl1
PROCEDURE val-fc1 :
/*$A40063*/
do with frame fc1:
    CASE int(SUBSTRING(SELF:NAME,3,3)):
        WHEN 1 THEN DO:
            find tabgco where tabgco.type_tab=br-xt1:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-xt1:screen-value = intiTab.
                ELSE lib-xt1:screen-value = tabgco.inti_tab.
            END.
            else lib-xt1:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 2 THEN DO:
            find tabgco where tabgco.type_tab=br-xt2:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-xt2:screen-value = intiTab.
                ELSE lib-xt2:screen-value = tabgco.inti_tab.
            END.
            else lib-xt2:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 3 THEN DO:
            find tabgco where tabgco.type_tab=br-xt3:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
             if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-xt3:screen-value = intiTab.
                ELSE lib-xt3:screen-value = tabgco.inti_tab.
            END.
            else lib-xt3:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 4 THEN DO:
            find tabgco where tabgco.type_tab=br-xt4:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-xt4:screen-value = intiTab.
                ELSE lib-xt4:screen-value = tabgco.inti_tab.
            END.
            else lib-xt4:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 5 THEN DO:
            find tabgco where tabgco.type_tab=br-xt5:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-xt5:screen-value = intiTab.
                ELSE lib-xt5:screen-value = tabgco.inti_tab.
            END.
            else lib-xt5:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
    END CASE.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE val-fval wsaicl1
PROCEDURE val-fval :
/*$A40063*/
do with frame fval:
    CASE int(SUBSTRING(SELF:NAME,3,3)):
        WHEN 1 THEN DO:
            find tabgco where tabgco.type_tab=br-zt1:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-zt1:screen-value = intiTab.
                ELSE lib-zt1:screen-value = tabgco.inti_tab.
            END.
            else lib-zt1:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 2 THEN DO:
            find tabgco where tabgco.type_tab=br-zt2:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-zt2:screen-value = intiTab.
                ELSE lib-zt2:screen-value = tabgco.inti_tab.
            END.
            else lib-zt2:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 3 THEN DO:
            find tabgco where tabgco.type_tab=br-zt3:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
             if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-zt3:screen-value = intiTab.
                ELSE lib-zt3:screen-value = tabgco.inti_tab.
            END.
            else lib-zt3:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 4 THEN DO:
            find tabgco where tabgco.type_tab=br-zt4:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-zt4:screen-value = intiTab.
                ELSE lib-zt4:screen-value = tabgco.inti_tab.
            END.
            else lib-zt4:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
        WHEN 5 THEN DO:
            find tabgco where tabgco.type_tab=br-zt5:private-data and tabgco.a_tab=SELF:SCREEN-VALUE no-lock no-error.
            if available tabgco THEN DO:
                IF g-langue <> "" THEN intiTab = GetIntituleTableLangue (tabgco.type_tab, tabgco.a_tab, g-langue).
                IF intiTab <> "" THEN lib-zt5:screen-value = intiTab.
                ELSE lib-zt5:screen-value = tabgco.inti_tab.
            END.
            else lib-zt5:SCREEN-VALUE=Traduction("Inexistant",-2,"").
        END.
    END CASE.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE val-lib-dep wsaicl1
PROCEDURE val-lib-dep :
/* Valeurs libres d�plac�es --> Chapitre 1 */
DO WITH FRAME FC1:
    DO X=1 TO 5:
        IF xah[X]:SENSITIVE THEN xah[X]:SCREEN-VAL=wa[x].
        IF xnh[X]:SENSITIVE THEN xnh[X]:SCREEN-VAL=string(wn[x]).
        IF xdh[X]:SENSITIVE THEN xdh[X]:SCREEN-VAL=string(wd[x]).
        IF xlh[X]:SENSITIVE THEN xlh[X]:SCREEN-VAL=string(wl[x]).
        IF xth[X]:SENSITIVE THEN xth[X]:SCREEN-VAL=wt[x].
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valider-chapitre wsaicl1
PROCEDURE valider-chapitre :
CASE chap-cours:
    when 1 then Do:
        RUN ValiderChapitre1a.
        IF RETURN-VALUE <> "error" THEN DO:
            RUN ValiderChapitre1b.
            /* $A83361... */
            IF RETURN-VALUE = "error" then return "ERROR".
        END.
        /* ...$A83361 */
        val-chap1 = YES.
    END.
    when 2 then do:
        RUN ValiderChapitre2.
        val-chap2 = YES.
    END.
    /* Valeurs libres */
    when 3 then do with frame fval:
        DO X=1 TO 5:
            if zah[x]:visible then run ctr-lib ("A",zah[x]:handle,?).
            if zdh[x]:visible then run ctr-lib ("D",zdh[x]:handle,?).
            if znh[x]:visible then run ctr-lib ("N",znh[x]:handle,?).
            if zlh[x]:visible then run ctr-lib ("L",zlh[x]:handle,?).
        END.
        if zt1:visible then run ctr-lib ("T",zt1:handle,br-zt1:handle).
        if zt2:visible then run ctr-lib ("T",zt2:handle,br-zt2:handle).
        if zt3:visible then run ctr-lib ("T",zt3:handle,br-zt3:handle).
        if zt4:visible then run ctr-lib ("T",zt4:handle,br-zt4:handle).
        if zt5:visible then run ctr-lib ("T",zt5:handle,br-zt5:handle).

        /* Envoi message erreur $872 ... */
        if txtmsg<>"" then do:
            message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
            txtmsg="".
            apply "entry" to wid.
            return "error".
        end.
        val-traite=yes.
        DO X=1 TO 5:
            if zah[x]:visible then wa[X]=zah[x]:screen-value.
            if zdh[x]:visible then wd[x]=zdh[x]:INPUT-VALUE.
            if znh[x]:visible then wn[x]=decimal(znh[x]:screen-value).
            if zth[x]:visible then wt[x]=zth[x]:screen-value.
            if zlh[x]:visible then wl[x]=(zlh[x]:screen-value="yes").
        END.
        hide frame fval.
    end.
END CASE.

IF lgTextile THEN DO:
    find FIRST tabgco where tabgco.TYPE_TAB="SS" and tabgco.A_TAB=entetcli.saison:screen-value IN FRAME fc2 no-lock no-error.
    if not available tabgco then
     assign wid=(if txtmsg="" then entetcli.saison:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Saison inexistante",-2,"") + " " + "~n" + ""
            entetcli.saison:bgcolor=31 lib-ss:screen-value = Traduction("Inexistante",-2,"").
    IF entetcli.grp_ps:screen-value <> "" THEN DO:
        FIND FIRST tabgco where tabgco.TYPE_TAB="GP" and tabgco.A_TAB=entetcli.grp_ps:screen-value no-lock no-error.
        if not available tabgco then
         assign wid=(if txtmsg="" then entetcli.grp_ps:handle else wid)
                txtmsg=txtmsg + "" + "-" + " " + Traduction("Groupe de prestations inexistant",-2,"") + " " + "~n" + ""
                entetcli.grp_ps:bgcolor=31 lib-gpres:screen-value = Traduction("Inexistant",-2,"").
    END.
    /* Envoi message erreur $872 ... */
    if txtmsg<>"" then do:
        message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
        assign
            txtmsg=""
            chap-cours=2.
        RUN ENVOI-CHAPITRE.
        apply "entry" to wid.
        return "error".
    end.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValiderChapitre1a wsaicl1
PROCEDURE ValiderChapitre1a :
do with frame fc1:
    ASSIGN
        txtmsg = "" /* $A106276 */
        entetcli.ref_cde:bgcolor=16 dat_liv:bgcolor=16 entetcli.ori_cde:bgcolor=16
        entetcli.semaine:bgcolor=16
        entetcli.commerc[1]:bgcolor=16 entetcli.app_aff:bgcolor=16 entetcli.nat_cde:bgcolor=16
        entetcli.devise:bgcolor=16 entetcli.tx_ech_d:bgcolor=16 entetcli.code_reg:bgcolor=16
        entetcli.code_ech:bgcolor=16 entetcli.regime:bgcolor=16 entetcli.type_fac:bgcolor=16
        entetcli.civ_fac:bgcolor=16 entetcli.paysf:bgcolor=16 entetcli.villef:bgcolor=16
        adr_fac[1]:bgcolor=16 entetcli.langue:bgcolor=16
        entetcli.famille:bgcolor=16 entetcli.s2_famille:bgcolor=16 entetcli.s3_famille:bgcolor=16 entetcli.s4_famille:bgcolor=16
        entetcli.region:bgcolor=16 entetcli.groupe:bgcolor=16
        entetcli.affaire:bgcolor=16 /*$585*/ entetcli.no_tarif:bgcolor=16 edepot:bgcolor=16
        entetcli.canal:bgcolor=16.

    /* $A106276 ... */
    /*$14*/
    if entetcli.affaire:screen-value<>"" then do:
        parnum = int(input entetcli.affaire) no-error.
        if not error-status:error then find affaire where affaire.affaire = string(int(input entetcli.affaire),"ZZZZZZZZZZ9") no-lock no-error.
                                  else find affaire where affaire.affaire = fill(" ",11 - length(input entetcli.affaire)) + input entetcli.affaire no-lock no-error.
        if not available affaire then do:
            message Traduction("Affaire inexistante",-2,"") + "." + " " + Traduction("Voulez-vous la cr�er ?",-2,"") view-as alert-box question buttons yes-no update logbid.
            if logbid then DO:
                /* APPLY "choose" TO bcreer-aff. */
                RUN creer-aff.
                FIND FIRST affaire WHERE affaire.affaire = entetcli.affaire:SCREEN-VAL NO-LOCK NO-ERROR.
                IF NOT AVAIL affaire THEN RETURN "error". /*$A106276*/
                    /* $A106276
                    ASSIGN wid=(IF txtmsg="" THEN entetcli.affaire:HANDLE ELSE wid)
                           txtmsg=txtmsg + "" + "-" + " " + Traduction("Affaire inexistante",-2,"") + " " + "~n" + ""
                           entetcli.affaire:BGCOLOR = 31 lib-aff:SCREEN-VALUE = Traduction("Inexistante",-2,"").*/
            END.
            ELSE RETURN "error". /*$A106276*/
            /* $A106276
            else assign wid=(if txtmsg="" then entetcli.affaire:handle else wid)
                        txtmsg=txtmsg + "" + "-" + " " + Traduction("Affaire inexistante",-2,"") + " " + "~n" + ""
                        entetcli.affaire:bgcolor=31 lib-aff:screen-value = Traduction("Inexistante",-2,"").
                        */
        end.
    end.
    /*fin $14*/
    /* ... $A106276 */

    if int(edepot:SCREEN-VAL)=0 then
      assign wid=(if txtmsg="" then edepot:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("D�p�t incorrect",-2,"") + " " + "~n" + ""
       edepot:bgcolor=31.
    if input dat_cde>input dat_liv then
      assign wid=(if txtmsg="" then dat_liv:handle else wid) txtmsg=txtmsg + "" + "- " + Traduction("Date de commande > date de livraison",-2,"") + " " + "~n" + ""
       dat_liv:bgcolor=31.
    if can-do("F,R",type-cours) and parcde.gst_zone[98] AND input dat_liv>today then
      assign wid=(if txtmsg="" then dat_liv:handle else wid) txtmsg=txtmsg + "" + "- " + Traduction("Date livraison > aujourd'hui",-2,"") + " " + "~n" + ""
       dat_liv:bgcolor=31.
    ELSE IF can-do("F,R",type-cours) AND entetcli.dat_liv:SENSITIVE AND ((parsoc.vfm_deb <> ? AND INPUT dat_liv < parsoc.vfm_deb) OR (parsoc.vfm_fin <> ? AND INPUT dat_liv > parsoc.vfm_fin )) THEN /*$A26501*/
      assign wid=(if txtmsg="" then dat_liv:handle else wid) txtmsg=txtmsg + "" + "- " + Traduction("Date livraison",-2,"") + " " + Traduction("en dehors de la p�riode de validit� mouvements",-2,"") + "~n" + ""
       dat_liv:bgcolor=31.
    if INPUT entetcli.semaine > 53 then assign
        wid=(if txtmsg="" then entetcli.semaine:handle else wid)
        txtmsg=txtmsg + "- " + Traduction("Semaine incorrecte !",-2,"") + " " + "~n" + ""
        entetcli.semaine:bgcolor=31.
    /* R�f�rence commande */
    if parsoc.ges_ref and client.ref_cde and type-saisie<>"D" and entetcli.ref_cde:screen-value="" THEN assign
        wid=(if txtmsg="" then entetcli.ref_cde:handle else wid)
        txtmsg=txtmsg + "" + "-" + " " + Traduction("R�f�rence obligatoire",-2,"") + " " + "~n" + ""
        entetcli.ref_cde:bgcolor=31.

    find tabgco where tabgco.type_tab="OC" and tabgco.a_tab=input entetcli.ori_cde no-lock no-error.
    if not available tabgco then
       assign wid=(if txtmsg="" then entetcli.ori_cde:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Origine inexistante",-2,"") + " " + "~n" + ""
        entetcli.ori_cde:bgcolor=31 lib-oc:screen-value = Traduction("Inexistant",-2,"").

    if parsoc.ges_nat then do:
        find tabgco where tabgco.TYPE_TAB="NC" and tabgco.A_TAB=entetcli.nat_cde:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=(if txtmsg="" then entetcli.nat_cde:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Nature inexistante",-2,"") + " " + "~n" + ""
            entetcli.nat_cde:bgcolor=31
            lib-nc:screen-value = Traduction("Inexistant",-2,"").
    end.
    if parsoc.ges_can then do:
        find tabgco where tabgco.TYPE_TAB="KV" and tabgco.A_TAB=entetcli.canal:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=(if txtmsg="" then entetcli.canal:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Canal de vente inexistant",-2,"") + " " + "~n" + ""
            entetcli.canal:bgcolor=31
            lib-kv:screen-value = Traduction("Inexistant",-2,"").
        ELSE IF parsoc.par_can<>"" AND client.canal<>"" AND client.canal<>entetcli.canal:SCREEN-VALUE THEN assign
            wid=(if txtmsg="" then entetcli.canal:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Canal de vente non conforme � celui du client",-2,"") + " " + "~n" + ""
            entetcli.canal:bgcolor=31
            lib-kv:screen-value = Traduction("Incompatible",-2,"").
    end.

    /*$2104...find tabgco where tabgco.TYPE_TAB="MG" AND tabgco.fct_com=YES and tabgco.A_TAB=entetcli.commerc[1]:screen-value no-lock no-error.*/
    buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[1]:screen-value) + " AND TPPERSONNE.FCT_COM = YES ").
    if buf-id = ? /*...$2104*/  then
     assign wid=(if txtmsg="" then entetcli.commerc[1]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Commercial n� 1 inexistant",-2,"") + " " + "~n" + ""
      entetcli.commerc[1]:bgcolor=31 lib-rp1:screen-value = Traduction("Inexistant",-2,"").
    if entetcli.app_aff:screen-value<>"" then do:
        /*$2104...find tabgco where tabgco.TYPE_TAB="MG" AND tabgco.fct_aa=YES and tabgco.A_TAB=entetcli.app_aff:screen-value no-lock no-error.*/
        buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.app_aff:screen-value) + " AND TPPERSONNE.FCT_AA = YES ").
        if buf-id = ? /*...$2104*/  then
         assign wid=(if txtmsg="" then entetcli.app_aff:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Apporteur d'affaires inexistant",-2,"") + " " + "~n" + ""
          entetcli.app_aff:bgcolor=31 lib-aa:screen-value = Traduction("Inexistant",-2,"").
    end.

    /*$A33800... */
    IF parsoc.affaire THEN DO :
        logbid = TRUE.
        pmsg = "".
        CASE type-saisie :
            WHEN "C" THEN logbid = AFF_TEST_VALIDITE (9,qui$,entetcli.affaire:SCREEN-VAL,date(entetcli.dat_cde:SCREEN-VALUE),"",OUTPUT pmsg).
            WHEN "F" THEN logbid = AFF_TEST_VALIDITE (10,qui$,entetcli.affaire:SCREEN-VAL,date(entetcli.dat_cde:SCREEN-VALUE),"",OUTPUT pmsg).
            WHEN "D" THEN DO:
                IF parcde.gst_zone[29] THEN logbid = AFF_TEST_VALIDITE (8,qui$,entetcli.affaire:SCREEN-VAL,date(entetcli.dat_cde:SCREEN-VALUE),"",OUTPUT pmsg).
                ELSE logbid = TRUE.
            END.
        END CASE.
        IF NOT logbid THEN DO:
            ASSIGN wid=(IF txtmsg = "" THEN entetcli.affaire:HANDLE ELSE wid)
                   txtmsg=txtmsg + "" + "-" + " " + REPLACE(pmsg,"~n", "~n- ") + " " + "~n" + "" /* $A62274 */
                   entetcli.affaire:BGC = 31.
        END.
        ELSE IF pmsg <> "" THEN MESSAGE pmsg VIEW-AS ALERT-BOX WARNING BUTTONS OK.
    END.
    /*...$A33800*/

    /* $2215 ... */
    IF fill-materiel:SCREEN-VAL <> "" THEN
    DO:
        FIND FIRST materiel WHERE materiel.typ_fich = vTypFich AND materiel.materiel = vMateriel AND materiel.no_ordre = vNoOrdre NO-LOCK NO-ERROR.
        IF NOT AVAIL materiel THEN ASSIGN wid = (IF txtmsg = "" THEN fill-materiel:HANDLE ELSE wid)
                                          txtmsg = txtmsg + "" + "-" + " " + Traduction("Mat�riel inexistant", -2, "") + " " + "~n" + ""
                                          fill-materiel:BGCOLOR = 31
                                          lib-materiel:SCREEN-VALUE = Traduction("Inexistant", -2, "").
    END.
    /* ... $2215 */

    if entetcli.fac_dvs:screen-value="oui" then do:
        if type-cours="R" and entetcli.type_fac:screen-value="CO" then assign
            wid=(if txtmsg="" then entetcli.type_fac:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Facture comptoir impossible en devises",-2,"") + " " + "~n" + "" entetcli.type_fac:bgcolor=31.
        find TABCOMP where TABCOMP.TYPE_TAB="DE" and TABCOMP.A_TAB=entetcli.devise:screen-value no-lock no-error.
        if not available TABCOMP or tabcomp.a_tab=g-dftdev then assign
            wid=(if txtmsg="" then entetcli.devise:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Devise inexistante",-2,"") + " " + "~n" + ""
            entetcli.devise:bgcolor=31.
        if AVAIL tabcomp AND tabcomp.a_tab<>g-dftdev and (tabcomp.dev_in=? or input entetcli.dat_px<tabcomp.dev_in) and dec(entetcli.tx_ech_d:screen-value)=0 THEN assign
            wid=(if txtmsg="" then entetcli.tx_ech_d:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Taux �change obligatoire si devise HORS ZONE EURO",-2,"") + " " + "~n" + ""
            entetcli.tx_ech_d:bgcolor=31.
    end.
    ELSE IF entetcli.devise:screen-value<>"" THEN DO:
        find TABCOMP where TABCOMP.TYPE_TAB="DE" and TABCOMP.A_TAB=entetcli.devise:screen-value no-lock no-error.
        if not available TABCOMP then assign
            wid=(if txtmsg="" then entetcli.devise:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Devise inexistante",-2,"") + " " + "~n" + ""
            entetcli.devise:bgcolor=31.
    END.

    IF parsoc.ges_loc /*AND type-saisie = "D" $A50403*/ AND entetcli.location:CHECKED THEN DO:
        IF date(entetcli.deb_loc:SCREEN-VAL) = ? THEN ASSIGN
            wid=(if txtmsg="" then entetcli.deb_loc:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Date de d�but de location",-2,"") + " " + Traduction("obligatoire",-2,"") + " " + "~n" + ""
            entetcli.deb_loc:bgcolor=31.

        IF date(entetcli.fin_loc:SCREEN-VAL) = ? THEN ASSIGN
            wid=(if txtmsg="" then entetcli.fin_loc:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Date de fin de location",-2,"") + " " + Traduction("obligatoire",-2,"") + " " + "~n" + ""
            entetcli.fin_loc:bgcolor=31.

        IF INT(entetcli.dur_loc:SCREEN-VAL) = 0 THEN ASSIGN
            wid=(if txtmsg="" then entetcli.dur_loc:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("Dur�e de location",-2,"") + " " + Traduction("obligatoire",-2,"") + " " + "~n" + ""
            entetcli.dur_loc:bgcolor=31.

    END.

    find modereg where modereg.ngr1=g-modereg-ngr1 AND modereg.ngr2=g-modereg-ngr2 AND modereg.ndos=g-modereg-ndos AND codet_reg and modereg.code_reg=int(entetcli.code_reg:screen-value) no-lock no-error.
    if not available modereg then
     assign wid=(if txtmsg="" then entetcli.code_reg:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Mode de r�glement inexistant",-2,"") + " " + "~n" + ""
      entetcli.code_reg:bgcolor=31 LIB-reg:screen-value = Traduction("Inexistant",-2,"").
    find condech where condech.ngr1=g-condech-ngr1 AND condech.ngr2=g-condech-ngr2 AND condech.ndos=g-condech-ndos AND condech.code_ech=int(entetcli.code_ech:screen-value) no-lock no-error.
    if not available condech then
     assign wid=(if txtmsg="" then entetcli.code_ech:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Condition d'�ch�ance inexistante",-2,"") + " " + "~n" + ""
      entetcli.code_ech:bgcolor=31 LIB-ech:screen-value = Traduction("Inexistant",-2,"").
    find tabgco where tabgco.TYPE_TAB="RV" and tabgco.N_TAB=int(entetcli.regime:screen-value) no-lock no-error.
    if not available tabgco then assign wid=entetcli.regime:handle txtmsg="" + "-" + " " + Traduction("R�gime TVA inexistant",-2,"") + " " + "~n" + ""
      entetcli.regime:bgcolor=31 lib-rv:screen-value = Traduction("Inexistant",-2,"").
    find tabgco where tabgco.TYPE_TAB="TF" and tabgco.A_TAB=entetcli.type_fac:screen-value no-lock no-error.
    if not available tabgco then assign wid=(if txtmsg="" then entetcli.type_fac:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Type facturation inexistant",-2,"") + " " + "~n" + ""
      entetcli.type_fac:bgcolor=31 lib-tf:screen-value = Traduction("Inexistant",-2,"").
    if input entetcli.civ_fac<>"" then do:
        find TABCOMP where TABCOMP.TYPE_TAB="CI" and TABCOMP.A_TAB=entetcli.civ_fac:screen-value no-lock no-error.
        if not available TABCOMP then
         assign wid=entetcli.civ_fac:handle txtmsg="" + "-" + " " + Traduction("Civilit� inexistante",-2,"") + " " + "~n" + "" entetcli.civ_fac:bgcolor = 31
          lib-ci:screen-value = Traduction("Inexistant",-2,"").
    end.
    if entetcli.adr_fac[1]:screen-value="" then assign wid=(if txtmsg="" then entetcli.adr_fac[1]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Nom obligatoire",-2,"") + " " + "~n" + ""
      entetcli.adr_fac[1]:bgcolor = 31.
    if entetcli.villef:screen-value="" then assign wid=(if txtmsg="" then entetcli.villef:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Ville obligatoire",-2,"") + " " + "~n" + ""
      entetcli.villef:bgcolor = 31.
    find TABCOMP where TABCOMP.TYPE_TAB="PA" and TABCOMP.A_TAB=entetcli.paysf:screen-value no-lock no-error.
    if not available TABCOMP then
     assign wid=(if txtmsg="" then entetcli.paysf:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Pays inexistant",-2,"") + " " + "~n" + ""
      entetcli.paysf:bgcolor=31 lib-pa:screen-value = Traduction("Inexistant",-2,"").
    IF INPUT entetcli.paysf = "FRA" THEN DO:
        X = INT(trim(entetcli.k_post2f:SCREEN-VAL)) NO-ERROR.
        IF ERROR-STATUS:ERROR OR length(trim(entetcli.k_post2f:SCREEN-VAL)) > 5 THEN assign wid=(if txtmsg="" then entetcli.k_post2f:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Code postal inexistant",-2,"") + " " + "~n" + ""
        entetcli.k_post2f:bgcolor = 31.
    END.

    IF INPUT entetcli.groupe<>"" THEN DO:
        find tabgco where tabgco.TYPE_TAB="GC":U and tabgco.A_TAB=entetcli.groupe:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=entetcli.groupe:handle
            txtmsg="" + "-" + " " + Traduction("Groupe inexistant",-2,"") + " " + "~n" + ""
            entetcli.groupe:bgcolor=31
            lib-gc:screen-value = Traduction("Inexistant",-2,"").
    END.

    find tabgco where tabgco.TYPE_TAB="FC" and tabgco.A_TAB=entetcli.famille:screen-value no-lock no-error.
    if not available tabgco then assign
        wid=entetcli.famille:handle
        txtmsg="" + "-" + " " + Traduction("Famille inexistante",-2,"") + " " + "~n" + ""
        entetcli.famille:bgcolor=31
        lib-fc:screen-value = Traduction("Inexistant",-2,"").
    IF INPUT entetcli.s2_famille<>"" THEN DO:
        find tabgco where tabgco.TYPE_TAB="SC" and tabgco.A_TAB=STRING(entetcli.famille:screen-value,"X(3)") + entetcli.s2_famille:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=entetcli.s2_famille:handle
            txtmsg="" + "-" + " " + Traduction("Sous-Famille inexistante",-2,"") + " " + "~n" + ""
            entetcli.s2_famille:bgcolor=31
            lib-sc:screen-value = Traduction("Inexistant",-2,"").
    END.
    IF INPUT entetcli.s3_famille<>"" THEN DO:
        find tabgco where tabgco.TYPE_TAB="SD" and tabgco.A_TAB=STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:SCREEN-VALUE,"X(3)") + entetcli.s3_famille:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=entetcli.s3_famille:handle
            txtmsg="" + "-" + " " + Traduction("S/Sous-Famille inexistante",-2,"") + " " + "~n" + ""
            entetcli.s3_famille:bgcolor=31
            lib-sd:screen-value = Traduction("Inexistant",-2,"").
    END.
    IF INPUT entetcli.s4_famille<>"" THEN DO:
        find tabgco where tabgco.TYPE_TAB="SE" and tabgco.A_TAB=STRING(entetcli.famille:screen-value,"X(3)") + STRING(entetcli.s2_famille:SCREEN-VALUE,"X(3)") + STRING(entetcli.s3_famille:SCREEN-VALUE,"X(3)") + entetcli.s4_famille:screen-value no-lock no-error.
        if not available tabgco then assign
            wid=entetcli.s4_famille:handle
            txtmsg="" + "-" + " " + Traduction("S/S/Sous-Famille inexistante",-2,"") + " " + "~n" + ""
            entetcli.s4_famille:bgcolor=31
            lib-se:screen-value = Traduction("Inexistant",-2,"").
    END.

    find tabgco where tabgco.TYPE_TAB="RG" and tabgco.N_TAB=int(entetcli.region:screen-value) no-lock no-error.
    if not available tabgco then
     assign wid=(if txtmsg="" then entetcli.region:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("R�gion inexistante",-2,"") + " " + "~n" + ""
      entetcli.region:bgcolor=31 lib-rg:screen-value = Traduction("Inexistant",-2,"").
    if entetcli.langue:screen-value <> "" then do:
      find TABCOMP where TABCOMP.TYPE_TAB="LG" and TABCOMP.A_TAB=entetcli.langue:screen-value no-lock no-error.
      if not available TABCOMP then
       assign wid=(if txtmsg="" then entetcli.langue:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Langue inexistante",-2,"") + " " + "~n" + ""
        entetcli.langue:bgcolor=31 lib-lg:screen-value = Traduction("Inexistant",-2,"").
    end.

    /* N� Tarif client */
    if parsoc.tar_col AND (input entetcli.no_tarif<>0 OR (NOT parcde.gst_zone[41] AND input entetcli.no_tarif=0)) then do:
        find tarif where tarif.location = (parsoc.ges_loc AND entetcli.location:checked) AND tarif.no_tarif=input entetcli.no_tarif no-lock no-error.
        if not available tarif then assign wid=(if txtmsg="" then entetcli.no_tarif:handle else wid)
            txtmsg=txtmsg + "" + "-" + " " + Traduction("N� Tarif client inexistant",-2,"") + " " + "~n" + "" entetcli.no_tarif:bgcolor=31.
        else if tarif.devise<>"" and tarif.devise<>g-dftdev and (entetcli.fac_dvs:screen-value in frame fc1="non" or entetcli.devise:screen-value in frame fc1<>tarif.devise)
           then assign wid=(if txtmsg="" then entetcli.no_tarif:handle else wid)
                       txtmsg=txtmsg + "" + "-" + " " + Traduction("N� Tarif en devise",-2,"") + " ==> " + Traduction("Facturation dans cette m�me devise",-2,"") + " " + "~n" + "" entetcli.no_tarif:bgcolor=31.
    end.
    if entetcli.cal_marg:screen-value="V" and cm1<>0 then do:
        find produit where produit.cod_pro=parsoc.cptv[1] no-lock no-error.
        if not available produit then assign wid=(if txtmsg="" then entetcli.cal_marg:handle else wid)
          txtmsg=txtmsg + "" + "-" + " " + Traduction("Si marge en valeur, vous devez d�finir un divers factur� pour stocker le montant de la marge (Param�trage dossier -> Param�trage client -> Param�trage devis)",-2,"") + " " + "~n" + "".
    end.
    if txtmsg<>"" and wid:hidden=yes then
     message Traduction("Un champ non visible est en erreur !",-2,"") skip
       Traduction("Il s'agit de",-2,"") + " " + ":" + " " skip entry(1,txtmsg,"~n") skip
       Traduction("Vous devez abandonner et, soit modifier sa valeur dans la fiche client, soit rendre modifiable ce champ dans le param�trage de gestion facturation client !",-2,"")
        view-as alert-box info.

    /* Envoi message erreur $872 ...*/
    if txtmsg<>"" then do:
        message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
        txtmsg="".
        apply "entry" to wid.
        return "error".
    end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValiderChapitre1b wsaicl1
PROCEDURE ValiderChapitre1b :
do with frame fc1:
    DEF VAR rpxrvt AS DEC NO-UNDO. /*$A48240*/
    DEF VAR rpxpmp AS DEC NO-UNDO. /*$A48240*/
    DEF VAR rpxach AS DEC NO-UNDO. /*$A48240*/
    DEF VAR modif_dateliv AS LOG    NO-UNDO INITIAL NO.
        /* $A52966... */
    DEFINE VARIABLE wdat AS DATE        NO-UNDO.
    DEFINE VARIABLE lgar AS INTEGER     NO-UNDO.
        /* ...$A52966 */

    DEFINE VARIABLE supStk  AS LOGICAL    NO-UNDO. /* $A54157 */
    DEF VAR ecartdatemad AS INT NO-UNDO. /*$A79284*/

    /* $A83361... */
    DEFINE VARIABLE lstProduits     AS CHARACTER    NO-UNDO.
    DEFINE VARIABLE vMsg            AS CHARACTER    NO-UNDO.
    DEFINE BUFFER bproduit          FOR produit.
    /* ...$A83361 */

    DEF BUFFER parcde2 for parcde. /*$A76691*/

    if can-do("F,R",type-cours) and parsoc.vf_deb<>? and input entetcli.dat_liv<parsoc.vf_deb then
        MESSAGE Traduction("Attention !",-2,"") SKIP Traduction("La date livraison est ant�rieure � la p�riode de validit� de facturation",-2,"") + " " VIEW-AS ALERT-BOX WARNING.

    if input entetcli.regime<>9
        and entetcli.fac_dvs:screen-value="oui":U
        and input entetcli.devise<>g-dftdev
        and (input entetcli.regime=1 or can-find(first regtva where regtva.code_tva=TvaParDefaut and regtva.regime=input entetcli.regime and regtva.tva_exc>0 no-lock))
      then do:
       logbid=no.
       message Traduction("Si le client est factur� en devises alors que son r�gime TVA est non �xon�r� (<>9), le calcul de la TVA sera effectif sur la facture !",-2,"") skip
         Traduction("Voulez-vous continuer ?",-2,"") view-as alert-box question buttons yes-no update logbid.
       if not logbid then do:
            apply "entry" to entetcli.regime.
            return "error". /*$872*/
       end.
    end.

    /* R�f�rence commande $1823 */
    FIND parcde2 WHERE parcde2.typ_fich = "C1":U NO-LOCK NO-ERROR. /*$A76691*/
    if parsoc.ges_ref and client.ref_cde AND AVAIL parcde2 AND parcde2.gst_zone[1] /*$A76691*/ and entetcli.ref_cde:screen-value<>"" AND type-saisie <> "D" /* type-saisie = "C" AND creation = YES $A50690*/ THEN DO:
        logbid=YES.
        FOR EACH entetcli2 WHERE entetcli2.cod_cli = client.cod_cli AND entetcli2.typ_sai <> "D" AND entetcli2.typ_sai <> "W" /*$A103205*/ /*entetcli2.typ_sai = "C" $A50690*/ NO-LOCK:
            IF entetcli2.ref_cde = entetcli.ref_cde:SCREEN-VALUE AND entetcli2.no_cde <> int(entetcli.no_cde:SCREEN-VALUE IN FRAME fonglet) THEN logbid=NO.
            IF NOT logbid THEN LEAVE.
        END.
        /*$A50690 Recherche �galement dans l'historique... */
        IF logbid THEN DO :
            FIND FIRST histoent WHERE histoent.cod_cf = client.cod_cli AND histoent.typ_mvt = "C" AND histoent.ref_cde = entetcli.ref_cde:SCREEN-VALUE NO-LOCK NO-ERROR.
            IF AVAIL histoent THEN logbid = NO.
        END.
        /*...Fin recherche $A50690*/
        IF NOT logbid THEN MESSAGE Traduction("Une autre commande avec cette r�f�rence commande existe d�j� pour ce client",-2,"") skip
              Traduction("Voulez-vous continuer ?",-2,"") view-as alert-box question buttons yes-no update logbid.
        if not logbid then do:
             apply "entry" to entetcli.ref_cde.
             return "error".
        end.
    END.

    /* Contr�les valeurs libres d�plac�es */
    if parcde.gst_zone[2] and au-moins-une then do:
        DO X=1 TO 5:
            if xah[x]:visible then run ctr-lib ("A",xah[x]:handle,?).
            if xdh[x]:visible then run ctr-lib ("D",xdh[x]:handle,?).
            if xnh[x]:visible then run ctr-lib ("N",xnh[x]:handle,?).
            if xlh[x]:visible then run ctr-lib ("L",xlh[x]:handle,?).
        END.

        if xt1:visible then run ctr-lib ("T",xt1:handle,br-xt1:handle).
        if xt2:visible then run ctr-lib ("T",xt2:handle,br-xt2:handle).
        if xt3:visible then run ctr-lib ("T",xt3:handle,br-xt3:handle).
        if xt4:visible then run ctr-lib ("T",xt4:handle,br-xt4:handle).
        if xt5:visible then run ctr-lib ("T",xt5:handle,br-xt5:handle).
        /* Envoi message erreur $872 ... */
        if txtmsg<>"" then do:
            message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
            txtmsg="".
            apply "entry" to wid.
            return "error".
        end.
        /* Svg valeurs */
        DO X=1 TO 5:
            if xah[x]:visible then wa[X]=xah[x]:screen-value.
            if xdh[x]:visible then wd[x]=xdh[x]:INPUT-VALUE.
            if xnh[x]:visible then wn[x]=decimal(xnh[x]:screen-value).
            if xth[x]:visible then wt[x]=xth[x]:screen-value.
            if xlh[x]:visible then wl[x]=(xlh[x]:screen-value="yes").
        END.
    end.

    /* Modif date livraison */
    if ((creation and modif-entete) or not creation) and (input entetcli.dat_liv<>entetcli.dat_liv
                                                          OR wdat_livd<>entetcli.dat_livd /*$A64016*/
                                                          OR wdat_acc<>entetcli.dat_acc /*$A64016*/
                                                          OR wdat_rll<>entetcli.dat_rll /*$A64016*/
                                                          OR wdat_mad<>entetcli.dat_mad
                                                          or input entetcli.commerc[1]<>entetcli.commerc[1] or input entetcli.app_aff<>entetcli.app_aff
                                                          or input entetcli.regime<>entetcli.regime or input entetcli.affaire<>entetcli.affaire
                                                          OR (edepot:VISIBLE AND INT(edepot:screen-val)<>entetcli.depot AND CAN-DO("C,D",entetcli.typ_sai))) then do:
        retour=yes.

        /*$A64016...*/
        ASSIGN
            txtmes = Traduction("Vous avez modifi� l'une des donn�es suivantes :",-2,"") +  " : ~n"
            txtmes = txtmes + "   - " + Traduction("le d�p�t",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("la date de livraison",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("la date de livraison demand�e",-2,"") + " " + "(" + Traduction("arriv�e",-2,"") + ")"+ "~n"
            txtmes = txtmes + "   - " + Traduction("la date de livraison accord�e",-2,"") + " " + "(" + Traduction("arriv�e",-2,"") + ")" + "~n"
            txtmes = txtmes + "   - " + Traduction("la date de livraison r�elle",-2,"") + " " + "(" + Traduction("arriv�e",-2,"") + ")" + "~n"
            txtmes = txtmes + "   - " + Traduction("la date de mise � disposition",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("le commercial",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("l'apporteur d'affaire",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("le r�gime TVA",-2,"") + "~n"
            txtmes = txtmes + "   - " + Traduction("l'affaire",-2,"") + "~n"
            txtmes = txtmes + Traduction("Voulez-vous r�percuter ces modifications sur toutes les lignes ?",-2,"").
        MESSAGE txtmes
            view-as alert-box question buttons yes-no update retour.
        /*...$A64016*/
        session:set-wait-state("general").
        if retour then do:

            /* $A83361... */
            /* Si modification du d�p�t, on v�rifie le statut produit / d�p�t sur toutes les lignes.
               S'il est bloquant, on emp�che l'utilisateur de modifier le d�p�t */

    	    /*$A128155...*/
            EMPTY TEMP-TABLE wlcli NO-ERROR.
            FOR EACH lignecli OF entetcli WHERE lignecli.sous_type <> "PH" NO-LOCK:
                CREATE wlcli.
                BUFFER-COPY lignecli TO wlcli.
                wlcli.rid = ROWID(lignecli).
            END.
	        /*...$A128155*/

            IF edepot:VISIBLE AND INT(edepot:screen-val) <> entetcli.depot THEN DO:

                ASSIGN lstProduits  = ""
                       vMsg         = "".

                FOR EACH lignecli OF entetcli WHERE lignecli.sous_type <> "PH" NO-LOCK:

                    IF NOT StatutArticleDepotOk("2,8,9", lignecli.cod_pro, INTEGER (edepot:screen-val)) THEN
                        lstProduits = lstProduits + (IF lstProduits <> "" THEN ", " ELSE "") + STRING (lignecli.cod_pro).

                END.

                IF lstProduits <> "" THEN DO:

                    IF NUM-ENTRIES (lstProduits, ",") > 1 THEN
                        vMsg = SUBSTITUTE(Traduction (Traduction("Les produits &1 ont un statut particulier pour le d�p�t &2 qui les rend inaccessibles.",-2,""),-2,""), trim (lstProduits), edepot:screen-val).
                    ELSE DO:
                        FIND FIRST bproduit WHERE bproduit.cod_pro = INTEGER (trim (lstProduits)) NO-LOCK NO-ERROR.
                        vMsg = SUBSTITUTE(Traduction("Le produit &1 a un statut particulier pour le d�p�t &2 qui le rend inaccessible.",-2,""),
                                          (IF AVAILABLE bproduit THEN bproduit.nom_pro ELSE trim (lstProduits)), edepot:screen-val).
                    END.

                    MESSAGE Traduction("Modification du d�p�t impossible sur les lignes.",-2,"") SKIP vMsg
                        VIEW-AS ALERT-BOX ERROR.

                    edepot:SCREEN-VALUE = STRING (entetcli.depot).

                    RETURN "ERROR".
                END.
            END.
            /* ...$A83361 */


          if input entetcli.affaire="" then maj-sto=yes.
          else do:
              find affaire where affaire.affaire = input entetcli.affaire no-lock no-error.
              if available affaire /* $1261 ... and affaire.maj_sto */ AND affaire.maj_stov /* ... $1261 */ then maj-sto = no.
              else maj-sto=yes.
          end.
          /*$A61956...*/
          if input entetcli.affaire<>entetcli.affaire THEN DO:
              for each lignecli of entetcli where lignecli.sous_type="PH" exclusive-lock:
                  lignecli.affaire="".
              END.
          END.
          /*...$A61956*/
          /*$A99173...*/
          IF edepot:VISIBLE AND INT(edepot:SCREEN-VALUE) <> entetcli.depot AND CAN-DO("C,D",entetcli.typ_sai) THEN DO:
              FOR EACH lignecli OF entetcli WHERE lignecli.sous_type = "PH" EXCLUSIVE-LOCK:
                  IF lignecli.depot <> INT(edepot:SCREEN-VALUE) THEN lignecli.depot = INT(edepot:SCREEN-VALUE).
              END.
          END.
          /*...$A99173*/
          for each lignecli of entetcli where lignecli.sous_type<>"PH" exclusive-lock:
            find FIRST produit where produit.cod_pro=lignecli.cod_pro no-lock no-error. /* $1091 */
            if input entetcli.affaire<>entetcli.affaire then lignecli.maj_stk=maj-sto
                                                                              AND (NOT AVAIL produit OR produit.ges_stk) /*$A60709*/ .
            if input entetcli.affaire<>entetcli.affaire then lignecli.affaire="". /*$A61956*/

            IF CAN-DO("C,D",entetcli.typ_sai) AND edepot:VISIBLE AND INT(edepot:screen-val)<>entetcli.depot AND lignecli.no_of = 0 AND lignecli.no_lance = "" /*$1091 */ THEN
            do:
                /*$1091 */
                IF AVAIL produit AND lignecli.prod_gp <> (produit.prod_gp AND FabSurDepot (lignecli.cod_pro,INT(edepot:screen-val))) THEN
                DO:
                    IF produit.prod_gp AND FabSurDepot (lignecli.cod_pro,INT(edepot:screen-val)) THEN
                        ASSIGN lignecli.prod_gp = YES lignecli.mode_gp = produit.mode_gp.
                    ELSE ASSIGN lignecli.prod_gp = NO lignecli.mode_gp = "".
                END.
                /*fin $1091 */

                /*$A48240*/
                IF lignecli.depot<>INT(edepot:screen-val) THEN
                DO:
                    RUN calcul_prix (lignecli.cod_pro,lignecli.cod_ori,lignecli.cod_dec1,lignecli.cod_dec2,lignecli.cod_dec3,lignecli.cod_dec4,lignecli.cod_dec5,lignecli.md5,lignecli.k_var,lignecli.k_opt /*$A40573*/,
                                     INT(edepot:screen-val),lignecli.qte,(IF AVAIL entetcli THEN ROWID(entetcli) ELSE ?),
                                     OUTPUT rpxrvt,OUTPUT rpxpmp,OUTPUT rpxach ,NO,DATE (entetcli.dat_px:SCREEN-VAL),0,"","").
                    ASSIGN lignecli.px_rvt = rpxrvt lignecli.pmp = rpxpmp.
                END.
                /* fin $A48240*/

                lignecli.depot=INT(edepot:screen-val).
                /* $1090 */
                IF AVAIL produit THEN
                DO:
                    FIND FIRST deppro WHERE deppro.cod_pro = (IF lignecli.cod_ori <> 0 THEN lignecli.cod_ori ELSE lignecli.cod_pro)
                        AND deppro.depot = INT(edepot:screen-val) NO-LOCK NO-ERROR.
                    IF pref_parcde_famcpt AND AVAIL deppro AND deppro.fam_cpt <> 0 THEN lignecli.fam_cpt = deppro.fam_cpt.
                    ELSE
                    DO:
                        IF lignecli.cod_ori <> 0 THEN
                            FIND FIRST produit3 WHERE produit3.cod_pro = lignecli.cod_ori NO-LOCK NO-ERROR.
                        lignecli.fam_cpt = IF lignecli.cod_ori <> 0 AND AVAIL produit3 THEN produit3.fam_cpt ELSE produit.fam_cpt.
                    END.
                END.
                /*fin $1090 */
            END.
            if input entetcli.dat_liv<>entetcli.dat_liv then do:
                modif_dateliv = YES.

                if wdat_mad<>entetcli.dat_mad AND wforcerdat_mad = NO THEN  /*$A79284*/
                do:
                    IF lignecli.dat_mad = lignecli.dat_liv OR lignecli.dat_mad = ? THEN
                        ecartdatemad = 0.
                    ELSE
                    DO:
                        IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN /*$979*/
                            RUN delai-trt-calendar (lignecli.dat_mad, lignecli.dat_liv, OUTPUT ecartdatemad).
                        ELSE
                            ecartdatemad = lignecli.dat_liv - lignecli.dat_mad.
                    END.
                END.

                lignecli.dat_liv=input entetcli.dat_liv.

                /* $A118183... */
                IF (lignecli.lien_los<>0 OR lignecli.lien_ims<>0 OR lignecli.lien_emp<>0) AND (lignecli.typ_sai="R" OR lignecli.typ_sai="F" OR lignecli.typ_sai="C") THEN
                    logbid = STK_MAJDATMVT_LOTIMMEMP (lignecli.lien_los, lignecli.lien_ims, lignecli.lien_emp, lignecli.dat_liv). /*$STKJR*/
                /* ...$A118183 */

                /* $A52966... */
                IF lignecli.lien_gar > 0 THEN DO:

                    /*   => Calcul fin garantie de base */
                    wdat = lignecli.dat_liv.

                    FIND FIRST lignegar WHERE lignegar.lien_gar = lignecli.lien_gar EXCLUSIVE-LOCK.
                    IF AVAIL lignegar AND lignegar.lien_ori = 0 THEN DO:

                        RUN calcul-gar(INPUT-OUTPUT wdat).

                        lgar = lignegar.lien_gar.

                        /*   => Calcul fin extensions de garantie */
                        FOR EACH lignegar WHERE lignegar.lien_ori = lgar EXCLUSIVE-LOCK:
                            lignegar.dat_deb = wdat.
                            RUN calcul-gar(INPUT-OUTPUT wdat).
                        END.

                        lignecli.fin_gar = wdat.

                    END.

                END.
                                /* ...$A52966 */

                /* $A54157... */
                IF AVAIL produit THEN DO:
                    supStk = NO.
                    IF (entetcli.typ_sai = "F" OR entetcli.typ_sai = "R") AND lignecli.maj_stk THEN DO:
                        IF lignecli.cod_dec1 <> "" THEN DO:
                            FIND FIRST stockdec WHERE stockdec.typ_elem = produit.typ_elem AND stockdec.cod_pro = produit.cod_pro
                                                  AND stockdec.cod_dec1 = lignecli.cod_dec1 AND stockdec.cod_dec2 = lignecli.cod_dec2
                                                  AND stockdec.cod_dec3 = lignecli.cod_dec3 AND stockdec.cod_dec4 = lignecli.cod_dec4
                                                  AND stockdec.cod_dec5 = lignecli.cod_dec5 AND stockdec.depot = lignecli.depot EXCLUSIVE-LOCK NO-ERROR.
                            IF AVAIL stockdec AND NOT(stockdec.arr_sit <= lignecli.dat_liv OR stockdec.arr_sit = ?) THEN DO:
                                stockdec.stock = stockdec.stock + UL_NbUE (lignecli.qte, lignecli.uni_vte, lignecli.nb_uv1, lignecli.nb_uv2, lignecli.nb_uv, lignecli.nb_cv3, lignecli.nb_cv).
                                supStk = YES.
                                RELEASE stockdec.
                            END.
                        END.
                        ELSE IF lignecli.k_var <> "" THEN DO:
                            FIND FIRST stockvar WHERE stockvar.cod_pro = produit.cod_pro AND stockvar.md5 = lignecli.md5 AND stockvar.depot = lignecli.depot EXCLUSIVE-LOCK NO-ERROR.
                            IF AVAIL stockvar AND NOT(stockvar.arr_sit <= lignecli.dat_liv OR stockvar.arr_sit = ?) THEN DO:
                                stockvar.stock = stockvar.stock + UL_NbUE (lignecli.qte, lignecli.uni_vte, lignecli.nb_uv1, lignecli.nb_uv2, lignecli.nb_uv, lignecli.nb_cv3, lignecli.nb_cv).
                                supStk = YES.
                                RELEASE stockvar.
                            END.
                        END.
                        ELSE DO:
                            FIND FIRST stock WHERE stock.cod_pro = produit.cod_pro AND stock.depot = lignecli.depot EXCLUSIVE-LOCK NO-ERROR.
                            IF AVAIL stock AND NOT(stock.arr_sit <= lignecli.dat_liv OR stock.arr_sit = ?) THEN DO:
                                stock.stock = stock.stock + UL_NbUE (lignecli.qte, lignecli.uni_vte, lignecli.nb_uv1, lignecli.nb_uv2, lignecli.nb_uv, lignecli.nb_cv3, lignecli.nb_cv).
                                supStk = YES.
                                RELEASE stock.
                            END.
                        END.
                    END.

                    IF supStk THEN DO: /* si la date de livraison est inf�rieure � la date d'inventaire on supprime les liens + maj_stk � NO */
                        lignecli.maj_stk = NO.
                        STK_SUPPRESSION-LIENS-IMMLOTCON-LIG(BUFFER lignecli).
                        STK_SUPPRESSION-EMPL-SOR-HIS (lignecli.lien_emp, lignecli.qte). /* $A68016 */
                    END. /* sinon on change juste la date des histolot, histole, histoemp */
                    ELSE if (lignecli.lien_los<>0 or lignecli.lien_ims<>0 OR lignecli.lien_emp<>0) and (lignecli.typ_sai="R" or lignecli.typ_sai="F" OR lignecli.typ_sai="C" /* $A63720 */) then do:
                        logbid = STK_MAJDATMVT_LOTIMMEMP (lignecli.lien_los, lignecli.lien_ims, lignecli.lien_emp, lignecli.dat_liv). /*$STKJR*/
                    end.
                END.
                /* ...$A54157 */

            end.

            if wdat_mad<>entetcli.dat_mad THEN
            do:
                IF wforcerdat_mad THEN /*$A79284*/
                    ASSIGN lignecli.dat_mad=wdat_mad .
                ELSE
                DO:
                    IF ecartdatemad = 0 THEN /* date ori1 correspond � date ori2 */
                        lignecli.dat_mad= lignecli.dat_liv.
                    ELSE
                    DO:
                        lignecli.dat_mad= lignecli.dat_liv.
                        IF CAN-FIND(FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" NO-LOCK) THEN /*$979*/
                            RUN trt-calendar (0, YEAR(lignecli.dat_mad),-1 * ecartdatemad, INPUT-OUTPUT lignecli.dat_mad).
                        ELSE
                            lignecli.dat_mad = lignecli.dat_mad - ecartdatemad.
                    END.
                END.
            END.
            if wdat_livd<>entetcli.dat_livd then lignecli.dat_livd=wdat_livd.
            if wdat_acc<>entetcli.dat_acc then lignecli.dat_acc=wdat_acc.
            if wdat_rll<>entetcli.dat_rll then lignecli.dat_rll=wdat_rll.
            if input entetcli.commerc[1]<>entetcli.commerc[1] then lignecli.commerc[1]=input entetcli.commerc[1].
            if input entetcli.app_aff<>entetcli.app_aff then lignecli.app_aff=input entetcli.app_aff.
            if input entetcli.regime<>entetcli.regime then do:
              if lignecli.sous_type="HS" then do:
                  if input entetcli.regime=1 or input entetcli.regime=9 then lignecli.code_tva=TvaParDefaut.
                  else do:
                    find regtva where regtva.code_tva=TvaParDefaut and regtva.regime=input entetcli.regime no-lock no-error.
                    if avail regtva then lignecli.code_tva=regtva.tva_exc. else lignecli.code_tva=0.
                  end.
              end.
              else do:
                  /*find produit where produit.cod_pro=lignecli.cod_pro no-lock no-error.*/
                  if input entetcli.regime=1 or input entetcli.regime=9 then lignecli.code_tva=produit.code_tva.
                  else do:
                    find regtva where regtva.code_tva=produit.code_tva and regtva.regime=input entetcli.regime no-lock no-error.
                    if avail regtva then lignecli.code_tva=regtva.tva_exc. else lignecli.code_tva=0.
                  end.
              end.
            end.
          end.
          IF parsoc.bois_ar AND lookup(entetcli.typ_sai,"F,R") > 0 THEN Maj-date-sortie-bois("ENTETCLI",ROWID(entetcli)).
          /*$1027*/
          IF input entetcli.affaire<>entetcli.affaire THEN
          DO:
              FOR EACH prefab WHERE prefab.cod_cli = entetcli.cod_cli AND prefab.NO_cde = entetcli.NO_cde AND prefab.statut <= "G" SHARE-LOCK:
                  prefab.affaire = INPUT entetcli.affaire.
              END.
              FIND FIRST prefab NO-LOCK NO-ERROR. /*�XREF_NOWHERE�*/
          END.
          /*fin $1027*/
            
	        /*$A128155...*/
            IF CONNECTED ("WORKFLOW") AND SEARCH(WF_CHEMINPGM("execut_wf")) <> ? THEN DO:
                FOR EACH lignecli OF entetcli WHERE lignecli.sous_type <> "PH" NO-LOCK,
                    FIRST wlcli WHERE wlcli.rid = ROWID(lignecli) NO-LOCK:
                    wlist-init = DYNAMIC-FUNCTION("BufferCompare" IN g-hprowin, BUFFER lignecli:HANDLE, BUFFER wlcli:HANDLE, "").
                    pretwf = "".
                    DO X=1 TO NUM-ENTRIES(wlist-init):
                        pretwf = pretwf + (IF pretwf = "" THEN "" ELSE ",") + "GCO.lignecli." + ENTRY(X,wlist-init).
                    END.
                    IF pretwf <> "" THEN
                        RUN VALUE(WF_CHEMINPGM("execut_wf")) (NO, 0, "PROGIWIN", "*SAISIE LIGNES VENTE", YES, "GCO.lignecli" + CHR(1) + STRING(ROWID(lignecli)) + CHR(1) + STRING(TEMP-TABLE wlcli:DEFAULT-BUFFER-HANDLE), pretwf, "", OUTPUT pretwf).
                END.
            END.
	        /*...$A128155*/
        end.
        wforcerdat_mad = NO. /*$A79284*/
    end.


    /* $A40389 ... */
    IF AVAIL entetcli AND entetcli.typ_sai = "F" AND modif_dateliv AND entetcli.NO_bl > 0 THEN DO:
        FIND entetcli2 WHERE entetcli2.cod_cli = entetcli.cod_cli AND entetcli2.typ_sai = entetcli.typ_sai AND entetcli2.no_cde > 0 AND entetcli2.no_bl = entetcli.NO_bl NO-LOCK NO-ERROR.
        IF AMBIGUOUS entetcli2 THEN DO:
            MESSAGE Traduction("Vous avez modifi� la date de livraison, voulez-vous r�percuter cette modification sur toutes les commandes et lignes du B.L ?",-2,"")
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
            IF logbid THEN DO:
                FOR EACH entetcli2 WHERE entetcli2.cod_cli = entetcli.cod_cli AND entetcli2.typ_sai = entetcli.typ_sai AND entetcli2.no_cde > 0 AND entetcli2.NO_bl = entetcli.NO_bl SHARE-LOCK:
                    ASSIGN  entetcli2.dat_liv   = INPUT entetcli.dat_liv
                            entetcli2.dat_mad   = wdat_mad
                            entetcli2.dat_acc   = wdat_acc.

                    FOR EACH lignecli2 OF entetcli2 WHERE lignecli2.sous_type <> "PH" SHARE-LOCK:
                        ASSIGN  lignecli2.dat_liv   = INPUT entetcli.dat_liv
                                lignecli2.dat_mad   = wdat_mad
                                lignecli2.dat_acc   = wdat_acc.
                    END.
                END.
            END.
        END.
    END.
    /* ... $A40389 */


    if ((creation and modif-entete) or not creation) AND (input entetcli.dat_px<>entetcli.dat_px
                                                          OR (parcde.gst_zone[41] AND edepot:VISIBLE AND INT(edepot:screen-val)<>entetcli.depot AND CAN-DO("C,D",entetcli.typ_sai))
                                                          OR recalculer-prix-franco /*...$A35969*/
                                                          ) THEN DO:
        RUN recalcul-prix(YES).
                /* $A60738... */
        SESSION:SET-WAIT-STATE("general").
        IF entetcli.cal_marg <> "" /*OR (entetcli.fac_pxa AND entetcli.maj_ach<>0)*/ THEN RUN calcmarg (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,"P").
        RUN maj_phase (entetcli.cod_cli,entetcli.NO_cde,entetcli.NO_bl,entetcli.typ_sai).
        SESSION:SET-WAIT-STATE("").
                /* ...$A60738 */
    END.
    hide frame fc1.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValiderChapitre2 wsaicl1
PROCEDURE ValiderChapitre2 :
do with frame fc2:
    assign
        entetcli.cat_tar:bgcolor=16 vcl_stat:bgcolor=16 entetcli.commerc[2]:bgcolor=16
        vcl_grp:bgcolor=16 vcl_fact:bgcolor=16 vcl_paye:bgcolor=16 vcl_livre:bgcolor=16
        vcl_pln:bgcolor=16 vcl_plr:bgcolor=16 vc_factor:bgcolor=16
        entetcli.saison:bgcolor=16 entetcli.grp_ps:bgcolor=16 entetcli.cpt_bq:BGCOLOR=16.
    if input entetcli.cat_tar<>"" then do:
        find tabgco where tabgco.TYPE_TAB="CT" and tabgco.A_TAB=entetcli.cat_tar:screen-value no-lock no-error.
        if not available tabgco then
         assign wid=entetcli.cat_tar:handle txtmsg="" + "-" + " " + Traduction("Cat�gorie tarifaire inexistante",-2,"") + " " + "~n" + ""
          entetcli.cat_tar:bgcolor=31 lib-ct:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.cat_cum:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.cat_cum:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions de la cat�gorie tarifaire impossible si aucune cat�gorie",-2,"") + " " + "~n" + "".
    if entetcli.commerc[2]:screen-value<>"" then do:
        /*$2104...find tabgco where tabgco.TYPE_TAB="MG" AND tabgco.fct_com=YES and tabgco.A_TAB=entetcli.commerc[2]:screen-value no-lock no-error.*/
        buf-id = TPS-FIND-TPPERSONNE("TPPERSONNE.COD_PERS = " + QUOTER(entetcli.commerc[2]:screen-value) + " AND TPPERSONNE.FCT_COM = YES ").
        if buf-id = ? /*...$2104*/ then
         assign wid=(if txtmsg="" then entetcli.commerc[2]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Commercial n� 2 inexistant",-2,"") + " " + "~n" + ""
          entetcli.commerc[2]:bgcolor=31 lib-rp2:screen-value = Traduction("Inexistant",-2,"").
    end.
    IF input entetcli.cpt_bq <> 0 THEN DO:
        FIND PLANCPT where plancpt.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ PLANCPT.cpt = input entetcli.cpt_bq no-lock no-error.
        if not available plancpt then
         assign wid=(if txtmsg="" then entetcli.cpt_bq:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Compte banque inexistant",-2,"") + " " + "~n" + ""
          entetcli.cpt_bq:bgcolor=31 LIB-cchq:SCREEN-VAL = Traduction("Inexistant",-2,"").
    END.
    /* $1727 ... */
    IF toggle-contrat:CHECKED THEN
    DO:
        FIND FIRST savcon WHERE savcon.contrat = fill-contratloc:SCREEN-VAL AND savcon.indice = INT(fill-contratloc:PRIVATE-DATA)
                                                                            AND savcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
        IF NOT AVAIL savcon THEN DO : /*$A74851*/
            FIND FIRST savhcon WHERE savhcon.contrat = fill-contratloc:SCREEN-VAL AND savhcon.indice = INT(fill-contratloc:PRIVATE-DATA)
                                                                                  AND savhcon.typ_con = vTypeContrat NO-LOCK NO-ERROR.
            IF NOT AVAIL savhcon THEN ASSIGN wid = (IF txtmsg = "" THEN fill-contratloc:HANDLE ELSE wid) /*$A74851*/
                                                       txtMsg = txtMsg + "- " + (IF vTypeContrat = "LOC":U THEN Traduction("Contrat de location inexistant", -2, "")
                                                                                 ELSE Traduction("Contrat de maintenance inexistant", -2, "")) + " ~n"
                                                       fill-contratloc:BGCOLOR = 31.
        END.
    END.
    /* ... $1727 */
    if vcl_fact:screen-value=entetcli.cod_cli:screen-value in frame Fonglet or
     vcl_livre:screen-value=entetcli.cod_cli:screen-value in frame Fonglet or
     vcl_paye:screen-value=entetcli.cod_cli:screen-value in frame Fonglet then
      assign wid=(if txtmsg="" then vcl_grp:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Le client en cours ne peut pas �tre affect� dans l'un des clients � saisir",-2,"") + " " + "~n" + "".
    if input vc_factor=entetcli.cod_cli:screen-value in frame Fonglet then
      assign wid=(if txtmsg="" then vc_factor:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Le client en cours ne peut pas �tre affect� au client d'affacturage",-2,"") + " " + "~n" + "".
    if int(vcl_grp:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=int(vcl_grp:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_grp:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client groupement inexistant",-2,"") + " " + "~n" + ""
        vcl_grp:bgcolor=31 LIB-grp:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[1]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[1]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[1]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[1]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if int(vcl_livre:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_livre:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_livre:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client livr� inexistant",-2,"") + " " + "~n" + ""
        vcl_livre:bgcolor=31 LIB-livre:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[2]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[2]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[2]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[2]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if int(vcl_fact:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_fact:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_fact:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client factur� inexistant",-2,"") + " " + "~n" + ""
        vcl_fact:bgcolor=31 LIB-fact:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[3]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[3]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[3]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[3]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if int(vcl_paye:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_paye:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_paye:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client payeur inexistant",-2,"") + " " + "~n" + ""
        vcl_paye:bgcolor=31 LIB-paye:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[4]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[4]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[4]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[4]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if int(vcl_pln:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_pln:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_pln:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client plateforme nationale inexistant",-2,"") + " " + "~n" + ""
        vcl_pln:bgcolor=31 LIB-pln:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[5]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[5]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[5]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[5]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if int(vcl_plr:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_plr:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_plr:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client plateforme r�gionale inexistant",-2,"") + " " + "~n" + ""
        vcl_plr:bgcolor=31 LIB-plr:screen-value = Traduction("Inexistant",-2,"").
    end.
    else if entetcli.tar_fil[6]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_fil[6]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client Tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    else if entetcli.tar_cum[6]:screen-value="yes" then assign wid=(if txtmsg="" then entetcli.tar_cum[6]:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Cumuler les conditions du client tarif impossible si aucun client",-2,"") + " " + "~n" + "".
    if (entetcli.tar_fil[1]:screen-value="yes" AND entetcli.tar_cum[1]:screen-value="yes")
        OR (entetcli.tar_fil[2]:screen-value="yes" AND entetcli.tar_cum[2]:screen-value="yes")
        OR (entetcli.tar_fil[3]:screen-value="yes" AND entetcli.tar_cum[3]:screen-value="yes")
        OR (entetcli.tar_fil[4]:screen-value="yes" AND entetcli.tar_cum[4]:screen-value="yes")
        OR (entetcli.tar_fil[5]:screen-value="yes" AND entetcli.tar_cum[5]:screen-value="yes")
        OR (entetcli.tar_fil[6]:screen-value="yes" AND entetcli.tar_cum[6]:screen-value="yes") then assign
        wid=(if txtmsg="" then entetcli.tar_fil[6]:handle else wid)
        txtmsg=txtmsg + "" + "-" + " " + Traduction("Soit client tarif pour calcul d'un prix, soit cumul des conditions",-2,"") + " " + "~n" + "".
    if int(vcl_stat:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vcl_stat:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vcl_stat:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client stat inexistant",-2,"") + " " + "~n" + ""
        vcl_stat:bgcolor=31 LIB-stat:screen-value = Traduction("Inexistant",-2,"").
    end.
    IF fill-lancement:HIDDEN = NO AND btn-lancement:SENSITIVE THEN
    DO:
        FIND FIRST lancement WHERE lancement.NO_lance = fill-lancement:SCREEN-VAL NO-LOCK NO-ERROR.
        IF NOT AVAILABLE lancement THEN
            assign wid=(if txtmsg="" then btn-lancement:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Lancement inexistant",-2,"") + " " + "~n" + "".
    END.
    if int(vc_factor:screen-value)<>0 then do:
      find clientr where clientr.cod_cli=integer(vc_factor:screen-value) no-lock no-error.
      if not available clientr then
       assign wid=(if txtmsg="" then vc_factor:handle else wid) txtmsg=txtmsg + "" + "-" + " " + Traduction("Client affacturage inexistant",-2,"") + " " + "~n" + ""
        vc_factor:bgcolor=31 LIB-factor:screen-value = Traduction("Inexistant",-2,"").
    end.
    if txtmsg<>"" and wid:hidden=yes then
     message Traduction("Un champ non visible est en erreur !",-2,"") skip
       Traduction("Il s'agit de",-2,"") + " " + ":" + " " skip entry(1,txtmsg,"~n") skip
       Traduction("Vous devez abandonner et, soit modifier sa valeur dans la fiche client, soit rendre modifiable ce champ dans le param�trage de gestion facturation client !",-2,"")
        view-as alert-box info.
    /* Envoi message erreur $872 ... */
    if txtmsg<>"" then do:
        message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
        txtmsg="".
        apply "entry" to wid.
        return "error".
    end.
    hide frame fc2.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vc-facdvs wsaicl1
PROCEDURE vc-facdvs :
DO WITH FRAME fc1:
entetcli.tx_ech_d:hidden=yes.
if input entetcli.fac_dvs="oui" then do:
    assign entetcli.devise:hidden=no br-de:hidden=no bappdev:HIDDEN=NO.
    find TABCOMP where TABCOMP.type_tab = "DE" and TABCOMP.a_tab = input entetcli.devise no-lock no-error.
    if available TABCOMP then do:
        if tabcomp.a_tab<>g-dftdev and (tabcomp.dev_in=? or input entetcli.dat_px<tabcomp.dev_in) then do:
            run rch-tx-devise-vente.
            entetcli.tx_ech_d:hidden=no.
        end.
    end.
end.
else assign entetcli.devise:hidden=yes br-de:hidden=yes bappdev:HIDDEN=YES.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vc-hbrowse wsaicl1
PROCEDURE vc-hbrowse :
DO with frame f1:
        /* $2178... */

    rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
    FIND FIRST entetcli WHERE ROWID(entetcli) = rentetcli NO-LOCK NO-ERROR. /* $2240 */
    IF entetcli.typ_sai="C" AND entetcli.edt_arc THEN CBSetItemCaption (Hcombars-f1, 2, {&bediter-arc}, "&" + Traduction("A.R.C",-2,"")).
    ELSE IF entetcli.typ_sai = "A" THEN CBSetItemCaption (Hcombars-f1, 2, {&bediter-arc}, "&" + Traduction("Abonnement",-2,"")).

    ASSIGN
        logbid = CBShowItem (Hcombars-f1, 2, {&bavan},            PCL_AFF_AVANCEMENT(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bcd},              PCL_AFF_TRF_CMD_DEV(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bcbl},             PCL_AFF_TRF_CMD_BL(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bediter-bp},       PCL_AFF_EDT_BP(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bsaisie-liv},      PCL_AFF_SAISIE_LIVRAISON(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bannuler-bp},      PCL_AFF_ANNULATION_BP(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bediter-arc},      PCL_AFF_EDT_ARC(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bediter-proforma}, PCL_AFF_EDT_PROFORMA(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bversdev},         PCL_AFF_VERSDEV(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bdc},              PCL_AFF_TRF_DEV_CMD(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bediter-devis},    PCL_AFF_EDT_DEV(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&brelance},         PCL_AFF_RELANCE(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bediter-facture},  PCL_AFF_EDT_FAC(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&breediter-bl},     PCL_AFF_EDT_BL(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bvalider-bl},      PCL_AFF_VALIDATION_BL(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bcomptoir},        PCL_AFF_TRF_FAC_COMPTOIR(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&bverifpx},         PCL_AFF_VERIFPX(ROWID(entetcli)))
        logbid = CBShowItem (Hcombars-f1, 2, {&Bdetailtemps},     parsoc.ges_ate AND entetcli.ori_ate) /*$A39538*/
        Logbid = CBShowItem (Hcombars-f1, 2, {&bproforma-f},      PCL_AFF_EDT_PROFORMA(ROWID(entetcli)))  /*$A20038*/
        Logbid = CBShowItem (Hcombars-f1, 2, {&bproforma-m},      PCL_AFF_EDT_PROFORMA(ROWID(entetcli))) /*$A20038*/
        Logbid = CBShowItem(hComBars-f1, 2, {&Docmaildev},        PCL_AFF_EDT_DEV(ROWID(entetcli))) /*$A49610*/
        Logbid = CBShowItem(hComBars-f1, 2, {&DocmailBL},         PCL_AFF_EDT_BL(ROWID(entetcli))AND NOT entetcli.NO_fact <> 0) /*$A49610*/
        Logbid = CBShowItem(hComBars-f1, 2, {&DocmailARC},        PCL_AFF_EDT_ARC(ROWID(entetcli))) /*$A49610*/
        Logbid = CBShowItem(hComBars-f1, 2, {&DocmailFAC},        PCL_AFF_EDT_FAC(ROWID(entetcli)) AND entetcli.NO_fact <> 0) /*$A49610*/
        .
/* ...$2178 */

    /*$A36586...  */
    /*$A49610 IF PCL_AFF_EDT_BL(ROWID(entetcli)) THEN DO:
        Logbid = CBShowItem (Hcombars-f1, 2, {&bedtDocComp}, NO ).
        Logbid = CBShowItem (Hcombars-f1, 2, {&bedtAvcConf}, NO ).
        vchar="".
        FOR EACH INFOENT WHERE INFOENT.typ_fich = "C" and INFOENT.cod_tiers = entetcli.cod_cli NO-LOCK:
             IF CAN-FIND(FIRST infolig WHERE infolig.typ_fich = INFOENT.typ_fich AND infolig.NO_info = infoent.NO_info AND infolig.edt_of NO-LOCK) THEN DO:
                 Logbid = CBShowItem (Hcombars-f1, 2, {&bedtDocComp}, YES).
                 Logbid = CBShowItem (Hcombars-f1, 2, {&bedtAvcConf}, YES).
                 vchar="X".
                 LEAVE.
             END.
        END.
        IF vchar="" AND entetcli.NO_info > 0 THEN FOR EACH INFOENT WHERE infoent.typ_fich = "E" and INFOENT.no_info = entetcli.NO_info NO-LOCK:
             IF CAN-FIND(FIRST infolig WHERE infolig.typ_fich = INFOENT.typ_fich AND infolig.NO_info = infoent.NO_info AND infolig.edt_of NO-LOCK) THEN DO:
                 Logbid = CBShowItem (Hcombars-f1, 2, {&bedtDocComp}, YES).
                 Logbid = CBShowItem (Hcombars-f1, 2, {&bedtAvcConf}, YES).
                 LEAVE.
             END.
        END.
    END. $A49610*/
    /*...$A36586*/

    /* Note interne */
    find txtentcl of entetcli no-lock no-error.
    if available txtentcl and txtentcl.note <> "" then do:
        IF NOT VALID-HANDLE(hpostit) THEN RUN postit PERSISTENT SET hpostit.
        IF VALID-HANDLE(hpostit) then RUN PostitAffichage IN hpostit (FRAME f1:HANDLE, 3, "NI", SUBSTITUTE(Traduction("Note interne (Commande &1)",-2,""),STRING(txtentcl.no_cde)), txtentcl.note).
    end.
    else IF VALID-HANDLE(hpostit) then RUN PostitHide IN hpostit (3).
    /*$1105... */
    IF gest-wkf THEN DO:
        DEF VAR petat AS INT  NO-UNDO.
        rentetcli = PCL_GET_ROWID("SAICL1":U).  /* $2240 */
        IF rentetcli <> ? AND HBrowse:NUM-SELECTED-ROWS = 1 AND CONNECTED ("WORKFLOW") THEN DO:
            RUN VALUE (PropWF + "\recmod_wf") ("PROGIWIN","*SAISIE VENTE",rentetcli,OUTPUT petat).
            IF petat = 2 THEN CBSetItemIcon(hComBars-f1, 2, {&Bwrkf}, {&Bwrkf2}).
            ELSE DO:
                IF petat = 3 THEN CBSetItemIcon(hComBars-f1, 2, {&Bwrkf}, {&Bwrkf3}).
                ELSE CBSetItemIcon(hComBars-f1, 2, {&Bwrkf}, {&Bwrkf}).
            END.
        END.
        ELSE CBSetItemIcon(hComBars-f1, 2, {&Bwrkf}, {&Bwrkf}).
    END. /* ...$1105*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vc-location wsaicl1
PROCEDURE vc-location :
DEF VAR canal_ok AS LOG    NO-UNDO.
DO WITH FRAME fc1:
        ASSIGN entetcli.deb_loc:HIDDEN = NOT entetcli.location:CHECKED
               entetcli.fin_loc:HIDDEN = NOT entetcli.location:CHECKED
               entetcli.dur_loc:HIDDEN = NOT entetcli.location:CHECKED.
        IF entetcli.location:CHECKED THEN DO:
            FIND FIRST tarif WHERE tarif.location = YES AND tarif.tarif_ref = YES NO-LOCK no-error.
            IF NOT avail(tarif) THEN FIND FIRST tarif WHERE tarif.location = YES NO-LOCK no-error.
            IF avail(tarif) THEN DO:
                entetcli.NO_tarif:SCREEN-VALUE = STRING(tarif.no_tarif).
                APPLY "LEAVE" TO entetcli.NO_tarif.
            END.
            FIND FIRST tabgco WHERE tabgco.TYPE_tab = "NC" AND tabgco.location = YES NO-LOCK NO-ERROR.
            IF AVAIL(tabgco) THEN DO:
                entetcli.nat_cde:SCREEN-VAL = tabgco.a_tab.
                IF parsoc.ges_can AND (parsoc.par_can="" OR client.canal="") AND tabgco.canal <> "" THEN ASSIGN
                    canal_ok = YES
                    entetcli.canal:SCREEN-VAL = tabgco.canal.
                APPLY "LEAVE" TO entetcli.nat_cde.
            END.
            IF parsoc.ges_can AND (parsoc.par_can="" OR client.canal="") THEN DO:
                IF NOT canal_ok THEN DO:
                    FIND FIRST parloc NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
                    IF parloc.canal <> "" THEN ASSIGN
                        canal_ok = yes
                        entetcli.canal:SCREEN-VAL = parloc.canal.
                END.
                IF canal_ok THEN APPLY "LEAVE" TO entetcli.canal.
            END.
        END.
        ELSE DO:
            ASSIGN entetcli.no_tarif:SCREEN-VALUE = string(client.NO_tarif)
                   toggle-contrat:CHECKED IN FRAME Fc2 = NO.
            APPLY "VALUE-CHANGED" TO toggle-contrat.

        END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vc-marg wsaicl1
PROCEDURE vc-marg :
DO with frame fc1:
bpardev:hidden=(self:screen-value="").

/* $A36788... */
valopx=parsoc.valo_px.
if NOT creation THEN FOR FIRST cleval WHERE cleval.chapitre="PROGIWIN" AND cleval.domaine="MARGE_VTE" AND cleval.cod_user="PROGIWIN" AND cleval.section=STRING(entetcli.NO_cde) AND cleval.cle="valopx" NO-LOCK:
    valopx=cleval.valeur.
END.
IF valopx = "M" AND entetcli.cal_marg:SCREEN-VAL <> "" THEN
    MESSAGE Traduction("Attention !",-2,"") SKIP Traduction("La pr�f�rence de la valorisation est au P.M.P",-2,"") SKIP
                    Traduction("Lors de la sortie de stock des produits (passage de devis � facture, livraisons...) les P.M.P seront recalcul�s ainsi que les prix de vente.",-2,"") SKIP
    VIEW-AS ALERT-BOX WARNING BUTTONS OK.
/* ...$A36788 */


/*$498*/
if not creation and self:screen-val<>"" then do:
  logbid=yes.
  message Traduction("Vous modifiez le mode de calcul des prix.",-2,"") skip
          Traduction("Voulez-vous recalculer le prix des lignes et le montant de la pi�ce ?",-2,"")
          view-as alert-box question buttons yes-no update logbid.
  if logbid then do transaction:
    assign entetcli.cal_marg.
    run recalcul-global.
    /* $952 */
    SESSION:SET-WAIT-STATE("general").
    IF entetcli.cal_marg <> "" /*OR (entetcli.fac_pxa AND entetcli.maj_ach<>0)*/ THEN RUN calcmarg (entetcli.cod_cli,entetcli.no_cde,entetcli.no_bl,entetcli.typ_sai,"P").
    RUN maj_phase (entetcli.cod_cli,entetcli.NO_cde,entetcli.NO_bl,entetcli.typ_sai).
    SESSION:SET-WAIT-STATE("").

    bappdev:HIDDEN = (NOT parcde.gst_zone[14] OR entetcli.fac_dvs:SCREEN-VALUE = "non":U OR entetcli.cal_marg:SCREEN-VALUE <> "").

  end.
end.
/*fin $498*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE voir-infos wsaicl1
PROCEDURE voir-infos :
DO with frame f1:
  if self:screen-value="yes" then do:
      x=int(cli$:screen-value) no-error.
      if not error-status:error then do:
        find client where client.cod_cli=x no-lock no-error.
        if available client then do:
          if not valid-handle(child-info) then run afincl_f persistent set child-info.
          run refresh-editeur in child-info (x).
          current-window=wsaicl1.
        end.
      end.
  end.
  else if valid-handle(child-info) then apply "close" to child-info.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MENUCLICK wsaicl1
PROCEDURE MENUCLICK : /* $A104178 */
    x=integer(cli$:screen-value in frame f1) no-error.
    if not error-status:error and x<>0 then /* contextuel */ DO:
        if valid-handle(child-info) then RUN ChildBottom IN child-info.
        run popup_o (string(x),0,"","","","","","C",0,"saicl1_c","").
    END.
    ELSE IF AVAIL entetcli THEN run popup_o (string(entetcli.cod_cli),0,"","","","","","C",entetcli.depot,"saicl1_c","").
    /* $A104178 ... */
    ELSE DO:
        X=INTEGER(entetcli.cod_cli:SCREEN-VAL IN FRAME fonglet) NO-ERROR.
        if not error-status:error and x<>0 then /* contextuel */ DO:
            if valid-handle(child-info) then RUN ChildBottom IN child-info.
            run popup_o (string(x),0,"","","","","","C",0,"saicl1_c","").
        END.
    END.
    /* ... $A104178 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
