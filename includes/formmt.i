/* Auteur : HL
Fin du format du montant en fonction des d�cimales g�r�es 
N�cessite l'include {decim.i}
*/
FUNCTION FormMt RETURNS CHAR (INPUT devise AS CHAR):
    if Devise=g-dftdev or Devise="" then RETURN fmtm.
    else do:
        find first tabcomp where type_tab="DE" and a_tab=Devise no-lock no-error.     
        IF AVAIL tabcomp THEN RETURN (IF deci_dev = 0 THEN "9" ELSE "9." + FILL("9",deci_dev)).
        ELSE RETURN fmtm.
    end.
END FUNCTION.
