&IF DEFINED(G-hProwin) = 0 &THEN
{G-HPROWIN.i}
&ENDIF

&IF DEFINED(G-hProwinBase) = 0 &THEN
&GLOBAL-DEFINE G-hProwinBase G-hProwinBase
DEF VAR G-hProwinBase AS HANDLE NO-UNDO.
G-hProwinBase = GetHApi("ProwinBase").
FUNCTION QueryAccessRights RETURNS LOG (INPUT CHAR, INPUT CHAR, INPUT CHAR) IN G-hProwinBase.
&ENDIF
&IF "{1}" = "" OR "{1}" = "Agents" &THEN
FUNCTION GetCommanders RETURNS CHAR IN G-hProwinBase.
FUNCTION GetAgents     RETURNS CHAR (INPUT CHAR) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Inf" &THEN
FUNCTION PutInfProwin   RETURNS LOG  (INPUT CHAR, INPUT CHAR, INPUT CHAR) IN G-hProwinBase.
FUNCTION GetInfProwin   RETURNS CHAR (INPUT CHAR, INPUT CHAR) IN G-hProwinBase.
FUNCTION PutInfProwinEx RETURNS LOG  (INPUT CHAR, INPUT CHAR, INPUT INT, INPUT CHAR) IN G-hProwinBase.
FUNCTION GetInfProwinEx RETURNS CHAR (INPUT CHAR, INPUT CHAR, INPUT INT) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Users" &THEN
FUNCTION GetUser       RETURN LOG  (INPUT CHAR,  INPUT CHAR, INPUT INT, OUTPUT CHAR, OUTPUT INT) IN G-hProwinBase.
FUNCTION GetFirstUser  RETURN CHAR (INPUT CHAR,  INPUT INT, OUTPUT CHAR, OUTPUT INT) IN G-hProwinBase.
FUNCTION GetNextUser   RETURN CHAR (OUTPUT CHAR, OUTPUT INT) IN G-hProwinBase.
FUNCTION GrantUser     RETURN LOG (INPUT pUser AS CHAR, INPUT pNDos AS INT, INPUT pNiv AS INT) IN G-hProwinBase.
FUNCTION RevokeUser    RETURN LOG (INPUT pUser AS CHAR, INPUT pNDos AS INT) IN G-hProwinBase.
FUNCTION SetUserProwin RETURN CHAR (INPUT pUser AS CHAR, INPUT pMdp AS CHAR, INPUT pNDos AS INT) IN G-hProwinBase.
FUNCTION GetUserName   RETURN CHAR (INPUT pLogin AS CHAR) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Msg" &THEN
FUNCTION PublishMsg   RETURNS LOG (INPUT LOG, INPUT CHAR, INPUT CHAR, INPUT CHAR) IN G-hProwinBase.
FUNCTION PublishMsgEx RETURNS LOG (INPUT LOG, INPUT CHAR, INPUT CHAR, INPUT CHAR, INPUT CHAR) IN G-hProwinBase.
FUNCTION NewMessage   RETURNS LOG IN G-hProwinBase.
FUNCTION NewMessageEx RETURNS LOG (INPUT CHAR) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Lock" &THEN
FUNCTION SetLock     RETURNS LOG (INPUT pType AS CHAR, INPUT pLock AS CHAR) IN G-hProwinBase.
FUNCTION SetLockDesc RETURNS LOG (INPUT pType AS CHAR, INPUT pLock AS CHAR, INPUT pComment AS CHAR) IN G-hProwinBase.
FUNCTION FreeLock    RETURNS LOG (INPUT pType AS CHAR, INPUT pLock AS CHAR) IN G-hProwinBase.
FUNCTION IsLocked    RETURNS LOG (INPUT pType AS CHAR, INPUT pLock AS CHAR) IN G-hProwinBase.
FUNCTION IsLockedEx  RETURNS LOG (INPUT pType AS CHAR, INPUT pLock AS CHAR, OUTPUT pQuiLck AS CHAR, OUTPUT pQuiWLck AS CHAR, OUTPUT pPCLck AS CHAR, OUTPUT pDateLck AS DATE, OUTPUT pHeureLck AS CHAR, OUTPUT pComment AS CHAR) IN G-hProwinBase.
&ENDIF

&IF ("{1}" = "" OR "{1}" = "Pref") AND DEFINED(hSettingsApi) = 0 &THEN
&GLOBAL-DEFINE hSettingsApi hSettingsApi
DEF VAR hSettingsApi AS HANDLE NO-UNDO.
FUNCTION LoadSettingsEx RETURNS LOG (INPUT pAppli AS CHAR, INPUT pDos AS INT, INPUT pUser AS CHAR, INPUT pChap AS CHAR, OUTPUT pPrefId AS HANDLE) IN G-hProwinBase.
FUNCTION LoadSettings   RETURNS LOG (INPUT pChap AS CHAR, OUTPUT pPrefId AS HANDLE) IN G-hProwinBase.
FUNCTION UnloadSettings RETURNS LOG (INPUT pPrefId AS HANDLE) IN G-hProwinBase.

FUNCTION GetSettingList  RETURNS CHAR IN hSettingsApi.
FUNCTION DeleteSettings  RETURNS LOG  IN hSettingsApi.
FUNCTION DeleteSetting   RETURNS LOG  (INPUT pCle AS CHAR) IN hSettingsApi.
FUNCTION GetSetting      RETURNS CHAR (INPUT pCle AS CHAR, INPUT pDft AS CHAR) IN hSettingsApi.
FUNCTION GetSettingInt   RETURNS INT  (INPUT pCle AS CHAR, INPUT pDft AS INT)  IN hSettingsApi.
FUNCTION GetSettingDec   RETURNS DEC  (INPUT pCle AS CHAR, INPUT pDft AS DEC)  IN hSettingsApi.
FUNCTION GetSettingDate  RETURNS DATE (INPUT pCle AS CHAR, INPUT pDft AS DATE) IN hSettingsApi.
FUNCTION GetSettingLog   RETURNS LOG  (INPUT pCle AS CHAR, INPUT pDft AS LOG)  IN hSettingsApi.
FUNCTION SaveSetting     RETURNS LOG  (INPUT pCle AS CHAR, INPUT pVal AS CHAR) IN hSettingsApi.
FUNCTION SaveSettingInt  RETURNS LOG  (INPUT pCle AS CHAR, INPUT pVal AS INT)  IN hSettingsApi.
FUNCTION SaveSettingDec  RETURNS LOG  (INPUT pCle AS CHAR, INPUT pVal AS DEC)  IN hSettingsApi.
FUNCTION SaveSettingDate RETURNS LOG  (INPUT pCle AS CHAR, INPUT pVal AS DATE) IN hSettingsApi.
FUNCTION SaveSettingLog  RETURNS LOG  (INPUT pCle AS CHAR, INPUT pVal AS LOG)  IN hSettingsApi.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Bases" &THEN
FUNCTION GetNumBase RETURNS INT (INPUT pServName AS CHAR, INPUT pCode AS CHAR, INPUT pNameDb AS CHAR) IN G-hProwinBase.
FUNCTION GetSeqBase RETURNS INT (INPUT pBase AS CHAR) IN G-hProwinBase.
&ENDIF


&IF "{1}" = "" OR "{1}" = "Licences" &THEN
FUNCTION IsGstLicence  	 RETURNS LOG  () IN G-hProwinBase.
FUNCTION IsValidLicence  RETURNS LOG  () IN G-hProwinBase.
FUNCTION IsValidApplic   RETURNS LOG  (pAppli AS CHAR, pVers AS DEC) IN G-hProwinBase.
FUNCTION IsValidLicApp   RETURNS LOG  (pApplic AS CHAR, pVersion AS DEC) IN G-hProwinBase.
FUNCTION GetLicValue     RETURNS CHAR (pApplic AS CHAR, pVersion AS DEC, pCodinf AS CHAR) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "Imprimante" &THEN
FUNCTION GetMachineDrv        RETURNS CHAR () IN G-hProwinBase.
FUNCTION IsGstImprimante      RETURNS LOG () IN G-hProwinBase.
FUNCTION GetSysPrinters       RETURNS CHAR (hFeedBack AS HANDLE, pLoad AS LOG) IN G-hProwinBase.
FUNCTION GetProwinPrinters    RETURNS CHAR (INPUT pBatch AS CHAR) IN G-hProwinBase.
FUNCTION LoadPrinter          RETURNS LOG (pPrinter AS CHAR, hFeedBack AS HANDLE) IN G-hProwinBase.
FUNCTION PrinterForMachine    RETURN LOG (pMachine AS CHAR) IN G-hProwinBase.
FUNCTION PrinterIsConnected   RETURNS LOG (pDeviceID AS CHAR) IN G-hProwinBase.
FUNCTION IsPrinterRights      RETURNS LOG (pRowid AS ROWID) IN G-hProwinBase.
FUNCTION IsPrinterRightsEx    RETURNS LOG (pRowid AS ROWID, pTestCnx AS LOG) IN G-hProwinBase.
FUNCTION GetDriverPrinter     RETURNS ROWID (pDeviceID AS CHAR) IN G-hProwinBase.
FUNCTION AddPrinterConnection RETURNS LOG (pDeviceID AS CHAR, pDefault AS LOG, pWait AS LOG) IN G-hProwinBase.
FUNCTION DelPrinterConnection RETURNS LOG (pDeviceID AS CHAR) IN G-hProwinBase.
FUNCTION SetDefaultPrinter    RETURNS LOG (pDeviceID AS CHAR) IN G-hProwinBase.
FUNCTION IsDefaultPrinter     RETURNS LOG (pDeviceID AS CHAR) IN G-hProwinBase.
FUNCTION GetParmImpKey        RETURNS CHAR (hBufDrv AS HANDLE) IN G-hProwinBase.
FUNCTION FindParmImp          RETURNS LOG (hBufDrv AS HANDLE, pKey AS CHAR) IN G-hProwinBase.
FUNCTION FindImpDevice        RETURNS CHAR (INPUT pImp AS CHAR) IN G-hProwinBase.
FUNCTION RegistryParmImp      RETURNS LOG (pSection AS CHAR, pRowid AS ROWID) IN G-hProwinBase.
&ENDIF

&IF "{1}" = "" OR "{1}" = "API" &THEN
FUNCTION GetMdpRes  RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pDos AS INT)                     IN G-hProwinBase.
FUNCTION GetMdpCpt  RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pDos AS INT)                     IN G-hProwinBase.
FUNCTION SetMdpRes  RETURNS LOG  (INPUT pApplic AS CHAR, INPUT pDos AS INT, INPUT pMdp AS CHAR) IN G-hProwinBase.
FUNCTION SetMdpCpt  RETURNS LOG  (INPUT pApplic AS CHAR, INPUT pDos AS INT, INPUT pMdp AS CHAR) IN G-hProwinBase.
FUNCTION IsEtatExt  RETURNS LOG  (INPUT pApplic AS CHAR, INPUT pNomProg AS CHAR)                IN G-hProwinBase.
FUNCTION SetEtatExt RETURNS LOG  (INPUT pApplic AS CHAR, INPUT pNomProg AS CHAR, INPUT pIsEtatExt AS LOG) IN G-hProwinBase.
FUNCTION GetNbOperationsPocwin  RETURNS CHAR () IN G-hProwinBase.
FUNCTION SetNivVersDossPocwin   RETURNS CHAR (INPUT pNdos AS INT, INPUT pNvers1 AS INT, INPUT pNvers2 AS INT, INPUT pNvers3 AS INT, INPUT pNvers4 AS INT) IN G-hProwinBase.
FUNCTION SetNivVersDoss         RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNvers1 AS INT, INPUT pNvers2 AS INT, INPUT pNvers3 AS INT, INPUT pNvers4 AS INT) IN G-hProwinBase.
FUNCTION GetNivVersDossPocwin   RETURNS CHAR (INPUT pNdos AS INT) IN G-hProwinBase.
FUNCTION GetNivVersDoss         RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pNdos AS INT) IN G-hProwinBase.
&ENDIF

