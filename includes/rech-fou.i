/* Author: HL       
$1081 JL 20/09/05 ACHFOU : Acheteur : Que ses fournisseurs (user-ach) 
$1155 JL 08/03/06 Interdire un type en recherche 
SC 20/03/09 Procedure FOU-RECHINIT
*/

/* procedure de recherche d'un fournisseur */

DEFINE VARIABLE lgFouRechInit   AS LOGICAL    NO-UNDO INIT NO.
DEFINE VARIABLE zzphoneFouRech  AS CHARACTER NO-UNDO.
DEFINE VARIABLE zztypeFouRech   as CHARACTER NO-UNDO.

PROCEDURE FOU-RECHINIT:
    IF LoadSettings("RECFOU", OUTPUT hsettingsapi) THEN DO:
        ASSIGN zztypeFouRech = GetSetting("Type", ?)
               zzphoneFouRech = GetSetting("Phonetique", ?).
        UnloadSettings(hSettingsApi).
    END.
    lgFouRechInit = YES.
END PROCEDURE.


PROCEDURE rech-fou:
    DEFINE INPUT   PARAM zzcde    AS CHARACTER NO-UNDO.  /* Commande "","C" Transporteur "T" */
    DEFINE INPUT   PARAM zzstatut AS CHARACTER NO-UNDO.  /* Liste Statuts � omettre */
    DEFINE INPUT-O PARAM zzcpt    AS CHARACTER NO-UNDO.  /* N� ou d�but Nom          */
    DEFINE OUTPUT  PARAM zzinti   AS CHARACTER NO-UNDO.  /* Intitul� en retour       */

    
    def var zzvnum as INTEGER NO-UNDO.
    def var zzpcpt as CHARACTER NO-UNDO.
    def var zzpinti as CHARACTER NO-UNDO.
    def var zzok as LOGICAL NO-UNDO.
    
    DEF VAR zzvchar AS CHARACTER NO-UNDO.
    DEF VAR zzvchar2 AS CHARACTER NO-UNDO.
    DEF VAR pas-droit-type AS LOG NO-UNDO.

    IF NOT lgFouRechInit THEN RUN FOU-RECHINIT.

    assign 
        zzok=no 
        zzvnum=integer(zzcpt) no-error.
    if error-status:error OR SUBSTR(zzcpt,1,1)=" " then do:
        /* Init champs recherche : " " = contains zzvchar, "&" = contains zzvchar2 */
        IF SUBSTR(zzcpt,1,1)=" " THEN DO:
            IF INDEX(zzcpt,'&')<>0 THEN ASSIGN 
                zzvchar=SUBSTR(zzcpt,2,INDEX(zzcpt,'&') - 2)
                zzvchar2=SUBSTR(zzcpt,INDEX(zzcpt,'&') + 1).
            ELSE ASSIGN 
                zzvchar=SUBSTR(zzcpt,2) 
                zzvchar2="".
        END.
        IF zzphoneFouRech="yes" THEN DO:
            zzvchar=IF SUBSTR(zzcpt,1,1)=" " THEN DoSoundex(zzvchar) ELSE DoSoundex(zzcpt).
            IF zzvchar2<>"" THEN zzvchar2=DoSoundex(zzvchar2).
        END.

        IF zztypeFouRech=? THEN DO:
            IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                find FOURNISR where fournisr.nom_fou > "" AND FOURNISR.nom_fou MATCHES "*" + zzvchar + "*" no-lock no-error.
            ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                find FOURNISR where fournisr.nom_fou > "" AND FOURNISR.nom_fou MATCHES "*" + zzvchar + "*" AND FOURNISR.nom_fou MATCHES "*" + zzvchar2 + "*" no-lock no-error.
            ELSE 
                find FOURNISR where FOURNISR.mot_cle=zzcpt no-lock no-error.
            if not available fournisr then 
                find FOURNISR where FOURNISR.nom_fou begins zzcpt no-lock no-error.
            if zzphoneFouRech="yes" AND not available fournisr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find fournisr where fournisr.phone > "" and fournisr.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find fournisr where fournisr.phone > "" and fournisr.phone MATCHES "*" + zzvchar + "*" AND fournisr.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                ELSE 
                    find fournisr where fournisr.phone begins zzvchar no-lock no-error.
            END.
        END.
        ELSE DO:
            IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                find FOURNISR where FOurnisR.typ_elem=zztypeFouRech AND fournisr.nom_fou > "" AND FOURNISR.nom_fou MATCHES "*" + zzvchar + "*" no-lock no-error.
            ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                find FOURNISR where FOurnisR.typ_elem=zztypeFouRech AND fournisr.nom_fou > "" AND FOURNISR.nom_fou MATCHES "*" + zzvchar + "*" AND FOURNISR.nom_fou MATCHES "*" + zzvchar2 + "*" no-lock no-error.
            ELSE 
                find FOURNISR where FOurnisR.typ_elem=zztypeFouRech AND FOURNISR.mot_cle=zzcpt no-lock no-error.
            if not available fournisr then 
                find FOURNISR where FOurnisR.typ_elem=zztypeFouRech AND FOURNISR.nom_fou begins zzcpt no-lock no-error.
            if zzphoneFouRech="yes" AND not available fournisr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find fournisr where FOurnisR.typ_elem=zztypeFouRech AND fournisr.phone > "" and fournisr.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find fournisr where FOurnisR.typ_elem=zztypeFouRech AND fournisr.phone > "" and fournisr.phone MATCHES "*" + zzvchar + "*" AND fournisr.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                ELSE 
                    find fournisr where FOurnisR.typ_elem=zztypeFouRech AND fournisr.phone begins zzvchar no-lock no-error.
            END.
        END.
    end.
    else if substr(zzcpt,1,1)="0" or length(trim(zzcpt))>6 then do: 
        IF zztypeFouRech=? THEN find FOURNISR where FOURNISR.notel begins zzcpt no-lock no-error.
        ELSE find FOURNISR where FOurnisR.typ_elem=zztypeFouRech AND FOURNISR.notel begins zzcpt no-lock no-error.
    END.
    else find FOURNISR where FOURNISR.cod_fou = zzvnum no-lock no-error.

    if available FOURNISR THEN RUN droittyp ("RE","F",FOURNISR.typ_elem,0,OUTPUT pas-droit-type).

    /* trouv�  */
    if available FOURNISR and lookup(string(fournisr.statut,"9"),zzstatut)=0 AND pas-droit-type=NO then do:
        if zzcde="C" then do:
            find typelem where typelem.typ_fich="F" and typelem.typ_elem=fournisr.typ_elem no-lock no-error.
            if available typelem and typelem.commande then zzok=yes.
        end.
        else if zzcde="T" and fournisr.typ_elem<>"TRS" then zzok=no.
        else zzok=yes.

        IF pas-droit-ach AND fournisr.acheteur<>user-ach THEN zzok=NO.
    end.
    else if available fournisr THEN DO:
          IF pas-droit-type THEN 
                MESSAGE "Fournisseur " trim(fournisr.nom_fou) 
                skip "a un statut : interdit de recherche pour votre profil" view-as alert-box info.
          ELSE message "Fournisseur " trim(fournisr.nom_fou) skip "a un statut : "
           (if fournisr.statut=1 then "interdit commande"
            else "� supprimer") 
           view-as alert-box info.
    END.

    if zzok then assign zzinti = fournisR.nom_fou zzcpt = string(fournisR.cod_fou).
    /* non trouv� ou non valide */
    else do:
        zzpcpt=zzcpt.
        /* RECHERCHE FOURNISSEUR */
        run recfou_b (zzcde,zzstatut,input-output zzpcpt, output zzpinti).
        if zzpcpt <> "" then assign zzcpt = zzpcpt zzinti = zzpinti.
        else zzcpt="".
    end.
END PROCEDURE.
