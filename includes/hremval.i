/* INCLUDE:hremval.i... */
/*
Auteur    : AP
Date      : 21/04/09

$A56990 HL 10/04/14 Gestion d'une grille de remises illimit�es en vente (grille_rem, remise 2 fig�e)
*/
{g-hprowin.def}

&IF DEFINED(HndRemVal) = 0 &THEN
&GLOBAL-DEFINE HndRemVal HndRemVal
DEF VAR HndRemVal AS HANDLE NO-UNDO.
DEF VAR HndRemVal### AS C NO-UNDO.
HndRemVal### = GetSessionData ("HndRemVal","CLE").
HndRemVal = WIDGET-HANDLE(HndRemVal###) NO-ERROR.

{g-hprowinbase.i}
IF NOT VALID-HANDLE (HndRemVal) THEN DO:
    RUN remval PERSISTENT SET HndRemVal.
    
    SetSessionData ("HndRemVal","CLE",STRING(HndRemVal)).
END.
&ENDIF

function calpx-remval           return dec (input p$typ_rem as log, input p$px as dec, input p$ndarr as int,
                                        input p$remise1 as dec,input p$remise2 as dec,input p$remise3 as dec,input p$remise4 as dec,
                                        input p$typ_rem1 as log,input p$typ_rem2 as log,input p$typ_rem3 as log,input p$typ_rem4 as log) in HndRemVal.
function calpx-undo-remval      return dec (input p$typrem as log, input p$px as dec, input p$ndarr as int,
                                        input p$remise1 as dec,input p$remise2 as dec,input p$remise3 as dec,input p$remise4 as dec,
                                        input p$typ_rem1 as log,input p$typ_rem2 as log,input p$typ_rem3 as log,input p$typ_rem4 as log) in HndRemVal.
function remises-valide-h       return log (input p$typrem as log, input p$tabrem as widget-handle extent, input p$tab-remval as widget-handle extent) in HndRemVal.
function concat-rem             return char (input p$remise as dec, input p$val as log, input p$devise as char, input p$typrem as log, input p$remlig as char) in HndRemVal.
function calcul-remise-grille   return dec (input p$grillerem as CHAR, /* grille remise */ INPUT p$typrem AS LOG, /* grille en remise en valeur */ input p$dat as DATE /* date de calcul */)  in HndRemVal.

/* ...INCLUDE:hremval.i */
