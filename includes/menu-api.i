
/* 
    API d'acc�s aux tables du menu
    CSA le 2/05/2011
    
   FM le 31/01/2012 : Pour les chemins de recherche, il faut mettre des Slash plut�t que des anti-Slash pour les batchs Unix et les services Web Linux 
   $A59048 CSA 11/02/13 Probl�me de test de droit pour les composants GCO dans Pocwin+ � cause de l'application
   $CSA    CSA 13/01/15 Nouvelles fonctions pour l'administration PROWIN
*/

&IF DEFINED(hmenu-api) = 0 &THEN
&GLOBAL-DEFINE hmenu-api hmenu-api
DEF VAR hmenu-api AS HANDLE NO-UNDO.
DEF VAR hmenu-api### AS C NO-UNDO.
{g-hprowinbase.i}
hmenu-api### = GetSessionData ("MENU-API":U,"CLE":U).
hmenu-api = WIDGET-HANDLE(hmenu-api###) NO-ERROR.

FUNCTION CheckField RETURNS LOG (INPUT pBase AS CHAR, INPUT pTable AS CHAR, INPUT pFld AS CHAR) IN g-hprowin.
                                        
  
/* Fonction qui permet de savoir si un utilisateur a le droit � un menu / sous-menu / sous-sous-menu / option � partir de son num�ro */
FUNCTION MNU_DROIT_NOOPTM       RETURNS LOG (INPUT pUser AS CHAR, INPUT pNoOptM AS INT)     IN hmenu-api.

FUNCTION MNU_DROIT_NOOPTM_APPLIC       RETURNS LOG (INPUT pUser AS CHAR, INPUT pNoOptm AS INT, INPUT pApplic AS CHAR) IN hmenu-api. /* $A59048 */

/* Fonction qui permet de savoir si un utilisateur a des droits personnalis�s */
FUNCTION MNU_A_MNUPERSO        RETURNS LOG (INPUT pUser AS CHAR)                           IN hmenu-api.

/* R�cup�ration du num�ro de mod�le utilis�e par un utilisateur */
FUNCTION MNU_GET_MODELE_MENU   RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pGroupe AS CHAR) IN hmenu-api.

/* R�cup�ration du mod�le standard sur lequel l'utilisateur pointe */
FUNCTION MNU_GET_MENU_STD_REF RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pGroupe AS CHAR) IN hmenu-api.

/* Savoir si l'utilisateur a un menu personnalis� � lui ou non */
FUNCTION MNU_GET_MENU_PERSO_UTIL RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* R�cup�ration du mod�le de bureau utilis� par un utilisateur */
FUNCTION MNU_GET_MODELE_BUREAU RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Application d'un mod�le de bureau � un utilisateur */
FUNCTION MNU_SET_MODELE_BUREAU RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModele AS INT) IN hmenu-api.

/* R�initialisation des param�tres personnels de bureau de l'utilisateur */
FUNCTION MNU_REINIT_PARAMS RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Fonction permettant de basculer le menu d'un utilisateur dans un autre mod�le standard (ancien menu, nouveau menu, ...) */
FUNCTION MNU_BASCULER_MENU RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModeleBascule AS INT) IN hmenu-api.
FUNCTION MNU_BASCULER_MENU2 RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModeleBascule AS INT) IN hmenu-api.

/* Modificication du mode de menu utilis� */
FUNCTION MNU_MODIFIER_PERSOMNU RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pModeMenu AS INT) IN hmenu-api.

/* Basculer l'apparence du menu */
FUNCTION MNU_BASCULER_APPARENCE RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModeleBascule AS INT) IN hmenu-api.
                                                                                                                           
/* Retourne yes si l'utilisateur a un menu personnalis� */
FUNCTION MNU_IS_PERSO RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Changement de la valeur non g�r� pour les options consultation ( champ consult utilis� dans POCWIN) */
FUNCTION MNU_SET_OPT_CONSULT RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModele AS INT, INPUT pNonGere AS CHAR) IN hmenu-api.

/* Retourne vrai si l'utilisateur a droit � l'option */
FUNCTION MNU_IS_OPT_GERE RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoOpt AS INT) IN hmenu-api.

/* Cr�ation d'un menu */
FUNCTION MNU_CRT_MNU RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT, INPUT POrd AS INT, INPUT PNiveau AS INT, INPUT PNoOptM AS INT, INPUT PNoOptMPere AS INT,
                                  INPUT pSav AS LOG, INPUT PNom AS CHAR, INPUT PNEx AS INT, INPUT PIcone AS CHAR, INPUT pNomIcone AS CHAR, INPUT PMot AS CHAR, INPUT pConsult AS LOG, 
                                  INPUT pResp AS LOG, INPUT pListeFct AS CHAR) IN hmenu-api.

/* Cr�ation d'une option */
FUNCTION MNU_CRT_OPT RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT, INPUT POrd AS INT, INPUT PNoOptM AS INT, INPUT PNoOptMPere AS INT,
                                  INPUT pSav AS LOG, INPUT pVers AS CHAR, INPUT pTra AS LOG, INPUT pIco AS CHAR, INPUT pNomIcone AS CHAR, INPUT pPgm AS CHAR, 
                                  INPUT PNom AS CHAR, INPUT PMot AS CHAR, INPUT pConsult AS LOG, INPUT pResp AS LOG, INPUT pListeFct AS CHAR) IN hmenu-api.

/* Suppression de tous les menus et options d'un mod�le standard */
FUNCTION MNU_EFFACER_MENU_STD RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT) IN hmenu-api.

/* Test d'existence d'option d'une version donn�e */
FUNCTION MNU_GET_OPT_VERS RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pVersion AS CHAR) IN hmenu-api.

/* Test d'existence d'option standard */
FUNCTION MNU_GET_MODELES RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT) IN hmenu-api.

/* Cr�ation des enregistrements dans MODELES des mod�les de menus standards */ 
FUNCTION MNU_CRT_MODELES_STD RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT) IN hmenu-api.

/* Changer la valeur de non_gere , le libell�, et les mots-cl�s, � partir d'un num�ro d'option ou de menu 
   Pour les options d�sactiv�es par d�faut (D), on le fait uniquement pour les mod�les standards.
   ossibilit� de sp�cifier un num�ro de mod�le. */
FUNCTION MNU_SET_NG_LIB_NOOPTM RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT, INPUT pNoOptm AS INT, 
                                         INPUT pLibelle AS CHAR, INPUT pMotsCles AS CHAR, INPUT pNonGere AS CHAR) IN hmenu-api.

/* Changer la valeur de non_gere, le libell�, et les mots-cl�s, � partir d'un num�ro de menu p�re
   Pour les options d�sactiv�es par d�faut (D), on le fait uniquement pour les mod�les standards.
   Possibilit� de sp�cifier un num�ro de mod�le. */
FUNCTION MNU_SET_NG_LIB_NOPERE RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT, INPUT pNoPere AS INT, 
                                            INPUT pLibelle AS CHAR, INPUT pMotsCles AS CHAR, INPUT pNonGere AS CHAR) IN hmenu-api.

/* Retourne le type du mod�le de menu : STD ou PERS */
FUNCTION MNU_GET_TYPE_MODELEMENU RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pNoModele AS INT) IN hmenu-api.

/* Retourne le num�ro du mod�le de menu utilis� par un utilisateur */
FUNCTION MNU_GET_NOMODELE_MENU_UTILISE RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Retourne vrai si les nouvelles options de la release ont �t� install�es */
FUNCTION MNU_GET_NV_OPT_RELEASE RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pRelease AS CHAR) IN hmenu-api.

/* Retourne vrai si la librairie de composants est install�e */
FUNCTION MNU_GET_COMPOS RETURNS LOG (INPUT pApplic AS CHAR) IN hmenu-api.

/* Cr�ation d'une option dans la barre d'outils standard */
FUNCTION MNU_CREER_BO_STD RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNoOptm AS INT, INPUT pNoOrd AS INT) IN hmenu-api.

/* Retourne vrai si au moins une option est pr�sente dans la barre d'outils standard */
FUNCTION MNU_GET_BO_STD RETURNS LOG (INPUT pApplic AS CHAR) IN hmenu-api.

/* Retourne le groupe auquel appartient l'utilisateur */
FUNCTION MNU_GET_GROUP_USER RETURNS CHAR (INPUT pApplic AS CHAR, INPUT pUser AS CHAR) IN hmenu-api.

FUNCTION MNU_SET_OPT_NG RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModele AS INT, INPUT pNoOptm AS INT, INPUT pNonGere AS CHAR) IN hmenu-api.

FUNCTION MNU_EFFACER_MENU_USER RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

FUNCTION MNU_CREATE_MODUSER_MENU RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR, INPUT pNoModele AS INT, INPUT pTypeMo AS CHAR) IN hmenu-api.

FUNCTION MNU_CREER_MENU_PERSO RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Permet de r�initialiser le menu d'un utilisateur sur toutes les soci�t�s 
    => Utilis� lorsqu'on d�place un utilisateur de groupe */
FUNCTION MNU_REINIT_MENU_UTIL RETURNS LOG (INPUT pApplic AS CHAR, INPUT pUser AS CHAR) IN hmenu-api.

/* Permet de r�initialiser le menu d'un utilisateur sur un dossier particulier 
    => Utilis� lorsqu'on d�place un utilisateur de groupe */
FUNCTION MNU_REINIT_MENU_UTIL_NDOS RETURNS LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUser AS CHAR) IN hmenu-api.

/* Fonction de suppression des personnalsiations dans le cas o� l'on supprime un utilisateur */
FUNCTION MNU_SUPPRESSION_USER RETURNS LOG (INPUT pApplic AS CHAR, INPUT pUser AS CHAR) IN hmenu-api.
                                                                                                     
/* Fonction appel�e lorsqu'on supprime un dossier pour nettoyer les tables du menu */
FUNCTION MNU_SUPPRESSION_DOSSIER RETURN LOG (INPUT pApplic AS CHAR, INPUT pNdos AS INT) IN hmenu-api.

/* $CSA... */
/* Fonction de copie d'un menu utilisateur vers un autre utilisateur */
FUNCTION MNU_COPIE_MENU_UTIL RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUserOri AS CHAR, INPUT pUserDest AS CHAR) IN hmenu-api.

/* Copie d'un menu utilisateur vers un autre sur tous les dossiers et applications */
FUNCTION MNU_COPIE_MENU_UTIL_TOUS_DOSSIERS RETURN LOG (INPUT pUserOri AS CHAR, INPUT pUserDest AS CHAR) IN hmenu-api.

/* Fonction de copie des param�trages de portail d'un utilisateur vers un autre utilisateur */
FUNCTION MNU_COPIE_PORTAIL_UTIL RETURNS INT (INPUT pApplic AS CHAR, INPUT pNdos AS INT, INPUT pUserOri AS CHAR, INPUT pUserDest AS CHAR) IN hmenu-api.

/* Copie des param�trages de portail d'un utilisateur vers un autre sur tous les dossiers et applications */
FUNCTION MNU_COPIE_PORTAIL_UTIL_TOUS_DOSSIERS RETURN LOG (INPUT pUserOri AS CHAR, INPUT pUserDest AS CHAR) IN hmenu-api.
/* ...$CSA */



IF hmenu-api### <> "NO" AND NOT VALID-HANDLE (hmenu-api) AND CheckField("PROWIN", "menugrp", "applic") THEN DO:
    DEFINE VARIABLE chemin-portail AS CHARACTER  NO-UNDO.
    chemin-portail = SEARCH (getsessiondata("Config!", "rep_spe") + "/Portail/menu-api.r":U). /* FM */
    IF chemin-portail = ? THEN chemin-portail = GetSessionData("Config!", "PORTAIL":U) + "/menu-api.r":U.
    RUN VALUE(chemin-portail) PERSISTENT SET hmenu-api.
    SetSessionData ("MENU-API":U,"CLE":U,STRING(hmenu-api)).
END.
&ENDIF
