

DEF VAR buf-hbuffer AS HANDLE NO-UNDO.    
    
FUNCTION REC-ZONE RETURNS CHARACTER (ptyp AS CHAR, pnom AS CHAR):
    DEF VAR pval AS CHAR no-undo.

    buf-hbuffer = ?.

    CASE ptyp :
        WHEN "TABGCO"   THEN IF AVAIL TABGCO  THEN buf-hbuffer = BUFFER TABGCO:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "CLIENT":U   THEN IF AVAIL CLIENT  THEN buf-hbuffer = BUFFER CLIENT:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "AFFAIRE":U  THEN IF AVAIL AFFAIRE THEN buf-hbuffer = BUFFER AFFAIRE:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "FOURNIS"  THEN IF AVAIL FOURNIS THEN buf-hbuffer = BUFFER FOURNIS:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "RCEVE"    THEN IF AVAIL RCEVE   THEN buf-hbuffer = BUFFER RCEVE:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "RCACT"    THEN IF AVAIL RCACT   THEN buf-hbuffer = BUFFER RCACT:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "CPTFOU"   THEN IF AVAIL CPTFOU  THEN buf-hbuffer = BUFFER CPTFOU:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "CPTCLI"   THEN IF AVAIL cptcli  THEN buf-hbuffer = BUFFER cptcli:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "PRODUIT":U  THEN IF AVAIL produit THEN buf-hbuffer = BUFFER produit:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "MATERIEL":U THEN IF AVAIL materiel THEN buf-hbuffer = BUFFER materiel:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "CORRESP":U THEN IF AVAIL CORRESP THEN buf-hbuffer = BUFFER CORRESP:BUFFER-FIELD(ENTRY(1,pnom,"[")). 
        WHEN "FRET":U THEN IF AVAIL fret THEN buf-hbuffer = BUFFER fret:BUFFER-FIELD(ENTRY(1,pnom,"[")).
        WHEN "HISTOENT" THEN IF AVAIL histoent THEN buf-hbuffer = BUFFER histoent:BUFFER-FIELD(ENTRY(1,pnom,"[")).

    END CASE.

    IF buf-hbuffer<>? THEN DO:
        IF INDEX(pnom,"[")<>0 THEN ASSIGN pval = buf-hbuffer:BUFFER-VALUE(INT(SUBSTR(ENTRY(2,pnom,"["),1,LENGTH(ENTRY(2,pnom,"[")) - 1))).
        ELSE pval = buf-hbuffer:BUFFER-VALUE.
    END.
    IF pval="?":U THEN pval = "".
    
    RETURN pval.

END FUNCTION.
