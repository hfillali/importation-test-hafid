/* Author: HL       */
/* Procedure de recherche d'un produit 
$67 HL 06/04/99 
$584 VR/DDG 29/05/00 Ajout du sous-type "PS" dans la liste
$725 JL 09/07/01 Ajout recherche gencod et refint declinaisons 
$1155 JL 08/03/06 Interdire un type en recherche 
SC 20/03/09 Procedure PRO-RECHINIT
$2132 APR 03/02/10 Recherche produit sur ref_ext/int idem nom_pro (contient)
$2133 APR 03/02/10 Recherche 2� designation  - 16/08/10 + le commence par
$2173 JL  26/03/10 Recherche des ref. externe aussi dans Prmultfo
$A58929 JL 06/02/13 Optimisation de la recherche par contient : Passage par la table rechac � la place de produit
$OPT1   JL 04/10/13 Ajout de 'produit-rech.cod_pro = rechac.cod_tiers and' dans tous les for each rechac
*/

DEFINE VARIABLE lgProRechInit AS LOGICAL    NO-UNDO INIT NO.
DEFINE VARIABLE chProRechMod AS CHARACTER  NO-UNDO.
DEFINE VARIABLE lgProRechDeclAR AS LOGICAL    NO-UNDO.
DEFINE VARIABLE zzphoneProRech AS CHARACTER NO-UNDO.
DEFINE VARIABLE lgRechNom2 AS LOGICAL    NO-UNDO.

/* $A58929... */
DEF VAR rechac-actif-pro AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-pro-des AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-pro-marque AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-pro-refint AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-pro-refext AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR ind-i-pro AS INT NO-UNDO.
DEF VAR vcod_rech_pro AS INT    NO-UNDO.
/* ...$A58929 */


/*SC 20/03/09 ...*/
PROCEDURE PRO-RECHINIT:
    DEF BUFFER parcder FOR parcde.

    lgProRechInit = YES.
    IF LoadSettings("RECPRO", OUTPUT hsettingsapi) THEN DO:
        zzphoneProRech = GetSetting("Phonetique", ?).
        UnloadSettings(hSettingsApi).
    END.

    FIND FIRST parsoc NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */ 
    IF AVAIL parsoc THEN ASSIGN chProRechMod = "X" lgProRechDeclAR = parsoc.decl_ar.

    FIND FIRST parcder WHERE parcder.typ_fich = "P" NO-LOCK NO-ERROR.
    IF AVAIL parcder AND parcder.gst_zone[16] THEN lgRechNom2 = YES.
END PROCEDURE.
/*...SC 20/03/09*/

PROCEDURE rech-pro:
    DEFINE INPUT   PARAM zzstatut    AS CHARACTER NO-UNDO.  /* Liste Statuts � omettre */
    DEFINE INPUT   PARAM zzdepot     AS INTEGER NO-UNDO.   /* D�p�t pour les statuts � omettre */
    DEFINE INPUT   PARAM zzclause    AS CHARACTER NO-UNDO.  /* Clause */
    DEFINE INPUT   PARAM zztype      AS CHARACTER NO-UNDO.  /* Type d'�l�ment */
    DEFINE INPUT   PARAM zzsous-type AS CHARACTER NO-UNDO.  /* Sous-Type d'�l�ment AR,PS,DF */
    DEFINE INPUT   PARAM zzexc       AS CHARACTER NO-UNDO.  /* "X":Exclusif "":non exclusif "ARPS" pour Article et Prestation*/
    DEFINE INPUT   PARAM zzrech      AS CHARACTER NO-UNDO.  /* Pr�f�rence recherche */
    DEFINE INPUT-O PARAM zzcpt       AS CHARACTER NO-UNDO.  /* N� ou d�but Nom          */
    DEFINE OUTPUT  PARAM zzinti      AS CHARACTER NO-UNDO.  /* Intitul� en retour       */
    DEFINE OUTPUT  PARAM zzmult      AS LOGICAL NO-UNDO.   /* Multi-fournisseur       */

    def var zzvnum as INTEGER NO-UNDO.
    def var zzpcpt as CHARACTER NO-UNDO.
    def var zzpinti as CHARACTER NO-UNDO.
    def var zzpmult as LOGICAL NO-UNDO.
    DEF VAR zzpcli AS INTEGER NO-UNDO.
    
    def var champ-alpha as LOGICAL NO-UNDO. /*$67*/
    DEF VAR zzvchar AS CHARACTER NO-UNDO.
    DEF VAR zzvchar2 AS CHARACTER NO-UNDO.
    DEF VAR pas-droit-type AS LOG NO-UNDO.

    DEFINE VARIABLE ok-pref AS LOGICAL NO-UNDO INIT NO. /*$1315*/
    DEFINE VARIABLE pcodpro AS INTEGER NO-UNDO. /*$1315*/

    DEF BUFFER prmultfoR FOR prmultfo. /*$2173*/

    DEF BUFFER Produit-rech FOR Produit. /*$A58929*/

    IF NOT lgProRechInit THEN RUN PRO-RECHINIT.

    IF substr(zzrech,2)<>"" THEN ASSIGN 
        zzpcli=INT(SUBSTR(zzrech,2)) 
        zzrech=substr(zzrech,1,1).

    zzvnum=integer(zzcpt) no-error.
    champ-alpha=error-status:error.

    /* $A58929... Si les rechac etaient en creation alors il faut regarder si ils sont maintenant actifs*/
    ASSIGN rechac-actif-pro = -1 rechac-actif-pro-des = -1 rechac-actif-pro-marque = -1 rechac-actif-pro-refint = -1 rechac-actif-pro-refext = -1 
           vcod_rech_pro = ?.
    IF SUBSTR(zzcpt,1,1) = " " THEN DO:
        CASE zzrech:
            WHEN "D" THEN DO:
                IF rechac-actif-pro-des = -1 THEN DO:
                  FIND FIRST rechap WHERE rechap.typ_fich = "P" AND rechap.typ_cha = "D|A":U NO-LOCK NO-ERROR.
                  IF AVAIL rechap THEN
                      rechac-actif-pro-des = rechap.decompo.      
                END.
                rechac-actif-pro = rechac-actif-pro-des.
            END.
            WHEN "M" THEN DO:
                IF rechac-actif-pro-marque = -1 THEN DO:
                    FIND FIRST rechap WHERE rechap.typ_fich = "P" AND rechap.typ_cha = "M|A":U NO-LOCK NO-ERROR.
                    IF AVAIL rechap THEN
                        rechac-actif-pro-marque = rechap.decompo.
                END.
                rechac-actif-pro = rechac-actif-pro-marque.
            END.
            WHEN "I" THEN DO:
                IF rechac-actif-pro-refint = -1 THEN DO:
                    FIND FIRST rechap WHERE rechap.typ_fich = "P" AND rechap.typ_cha = "I|A":U NO-LOCK NO-ERROR.
                    IF AVAIL rechap THEN
                        rechac-actif-pro-refint = rechap.decompo.
                END.
                rechac-actif-pro = rechac-actif-pro-refint.
            END.
            WHEN "E" THEN DO:
                IF rechac-actif-pro-refext = -1 THEN DO:
                    FIND FIRST rechap WHERE rechap.typ_fich = "P" AND rechap.typ_cha = "E|A":U NO-LOCK NO-ERROR.
                    IF AVAIL rechap THEN
                        rechac-actif-pro-refext = rechap.decompo.
                END.
                rechac-actif-pro = rechac-actif-pro-refext.
            END.
        END CASE.
    END.
    /* ...$A58929 */

    /*$1315...*/
    IF chProRechMod = "X" THEN RUN rech-param(zzcpt, zzpcli, zztype, zzsous-type, zzexc, OUTPUT ok-pref, OUTPUT pcodpro).
    /* ...$1315*/
    
    IF ok-pref THEN FIND produitr WHERE produitr.cod_pro = pcodpro NO-LOCK NO-ERROR.
    ELSE DO:
        if champ-alpha OR SUBSTR(zzcpt,1,1)=" " then do:
            /* Init champs recherche : " " = contains zzvchar, "&" = contains zzvchar2 */
            IF SUBSTR(zzcpt,1,1)=" " THEN DO:
                IF INDEX(zzcpt,'&')<>0 THEN ASSIGN 
                    zzvchar=SUBSTR(zzcpt,2,INDEX(zzcpt,'&') - 2)
                    zzvchar2=SUBSTR(zzcpt,INDEX(zzcpt,'&') + 1).
                ELSE ASSIGN 
                    zzvchar=SUBSTR(zzcpt,2) 
                    zzvchar2="".
            END.
            IF zzphoneProRech="yes" THEN DO:
                zzvchar=IF SUBSTR(zzcpt,1,1)=" " THEN DoSoundex(zzvchar) ELSE DoSoundex(zzcpt).
                IF zzvchar2<>"" THEN zzvchar2=DoSoundex(zzvchar2).
            END.
        END.

        if zzrech="r" THEN 
            find specifs where specifs.typ_fich="C" AND specifs.cod_tiers=zzpcli AND specifs.ref_cli begins zzcpt no-lock no-error.
        ELSE if zzrech="n" THEN 
            find specifs where specifs.typ_fich="C" AND specifs.cod_tiers=zzpcli AND specifs.nom_cli begins zzcpt no-lock no-error.
        ELSE if zztype<>"" then do:
            if zzrech="g" THEN 
                find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.gencod-v begins zzcpt no-lock no-error.
/*$2132...
            ELSE if zzrech="x" then 
                find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.refext begins zzcpt no-lock no-error.
            else if zzrech="i" then 
                find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.refint begins zzcpt no-lock no-error.*/
            else if zzrech="x" THEN do:

                if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/ 
                    END.
                    ELSE DO :
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.typ_elem=zztype and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. 
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/ 
                    END. /* $A58929 */
                END.
                else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" and Produit-rech.refext MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2 :
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                    END.
                    ELSE DO :
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.typ_elem=zztype and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" and PRODUITR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. 
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/ 
                    END. /* $A58929 */
                END.
                else /*if champ-alpha then */ DO:
                    find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.refext begins zzcpt no-lock no-error. 
                    IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and PrmultfoR.refext BEGINS zzcpt no-lock no-error. /*$2173*/
                END.
            END.
            else if zzrech="i" THEN do:
                if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                    END.
                    ELSE
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.typ_elem=zztype and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" no-lock no-error. 
                END.
                else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                              rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" and Produit-rech.refint MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                    END.
                    ELSE 
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.typ_elem=zztype and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" and PRODUITR.refint MATCHES "*" + zzvchar2 + "*" no-lock no-error. 
                END.
                else /*if champ-alpha then */
                    find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.refint begins zzcpt no-lock no-error.
            END.
/*...2132*/
            ELSE IF SUBSTR(zzcpt,1,1)=" " THEN
                /*$2133...*/
                IF lgRechNom2 THEN 
                    if zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.typ_elem=zztype and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") no-lock no-error.
                    END.
                    else DO :
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  (rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) OR rechac.decompo = SUBSTRING(zzvchar2,1,rechac-actif-pro)) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                         (Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar2 + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.typ_elem=zztype and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                         (PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar2 + "*") no-lock no-error.
                    END.
                /*$...$2133*/
                ELSE

                    if zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.typ_elem=zztype and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" no-lock no-error.
                    END.
                    else DO :
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" and Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.typ_elem=zztype and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" and PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                    END.
            else if champ-alpha then 
                /*$2133...*/
                IF lgRechNom2 THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              (rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) OR rechac.decompo = SUBSTRING(zzvchar2,1,rechac-actif-pro)) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.typ_elem=zztype and (Produit-rech.nom_pro begins zzcpt OR Produit-rech.nom_pr2 begins zzcpt) NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                    END.
                    ELSE 
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.typ_elem=zztype and (PRODUITR.nom_pro begins zzcpt OR PRODUITR.nom_pr2 begins zzcpt) no-lock no-error.
                END.
                ELSE
                    find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.nom_pro begins zzcpt no-lock no-error.
                /*...$2133*/
            ELSE 
                find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.cod_pro=zzvnum no-lock no-error.

            if champ-alpha AND zzphoneProRech="yes" AND not available produitr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find PRODUITR where PRODUITR.typ_elem=zztype and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find PRODUITR where PRODUITR.typ_elem=zztype and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" and PRODUITR.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                ELSE 
                    find PRODUITR where PRODUITR.typ_elem=zztype and PRODUITR.phone begins zzvchar no-lock no-error.
            END.
        end.
        else if zzsous-type<>"" then do:
            if zzrech="g" then 
                find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.gencod-v begins zzcpt no-lock no-error.
/*$2132...
            ELSE if zzrech="x" then 
                find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.refext begins zzcpt no-lock no-error.
            else if zzrech="i" THEN 
                find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.refint begins zzcpt no-lock no-error.*/                
            ELSE IF zzrech="x" THEN DO:
                if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        IF NOT AVAIL produitr AND zzsous-type = "AR" THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/
                    END.
                    ELSE DO :
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" no-lock no-error.
                        IF NOT AVAIL produitr AND zzsous-type = "AR" THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/
                    END. /* $A58929 */
                END.
                else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" and Produit-rech.refext MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        IF NOT AVAIL produitr AND zzsous-type = "AR" THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/
                    END.
                    ELSE DO :
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" and PRODUITR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                        IF NOT AVAIL produitr AND zzsous-type = "AR" THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/
                    END. /* $A58929 */
                END.
                else /*if champ-alpha then*/ DO:
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.refext begins zzcpt no-lock no-error.
                    IF NOT AVAIL produitr AND zzsous-type = "AR" THEN find PrmultfoR where PrmultfoR.refext BEGINS zzcpt no-lock no-error. /*$2173*/
                END.
            END.
            ELSE IF zzrech="i" THEN DO:
                if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                    END.
                    ELSE 
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" no-lock no-error.
                END.
                else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                    /* $A58929... */
                    IF rechac-actif-pro > 0 THEN DO :
                        FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                              rechac.typ_cha = zzrech AND 
                                              rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                              rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                              rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                        FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" and Produit-rech.refint MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                            IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                            ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                        END.
                        IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                    END.
                    ELSE 
                        /* ...$A58929 */
                        find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" and PRODUITR.refint MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                END.
                else /*if champ-alpha then */
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.refint begins zzcpt no-lock no-error.
            END.
/*...$2132*/
            ELSE IF SUBSTR(zzcpt,1,1)=" " THEN
                /*$2133...*/
                IF lgRechNom2 THEN 
                    if zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.sous_type=zzsous-type and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") no-lock no-error.
                    END.
                    else DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                                   (Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar2 + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.sous_type=zzsous-type and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                                   (PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar2 + "*") no-lock no-error.
                    END.
                /*$...$2133*/
                ELSE
                    if zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' 
                                              AND rechac.typ_cha = zzrech
                                              AND rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro)
                                              AND rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" no-lock no-error.
                    END.
                    else DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type=zzsous-type and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" and Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE
                            /* ...$A58929 */
                            find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" and PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                    END.

            else if champ-alpha then 
                /*$2133...*/
                IF lgRechNom2 THEN
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and (PRODUITR.nom_pro begins zzcpt OR PRODUITR.nom_pr2 begins zzcpt) no-lock no-error.
                ELSE
                    find PRODUITR WHERE PRODUITR.sous_type=zzsous-type and PRODUITR.nom_pro begins zzcpt no-lock no-error.
                /*...$2133*/
            else 
                find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.cod_pro=zzvnum no-lock no-error.
            if champ-alpha AND zzphoneProRech="yes" AND not available produitr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" and PRODUITR.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                ELSE 
                    find PRODUITR where PRODUITR.sous_type=zzsous-type and PRODUITR.phone begins zzvchar no-lock no-error.
            END.
        end.
        else do:
            if zzexc = "ARPS":U then /* pour article ET prestation de service */ do:
                if zzrech="g" then 
                    find PRODUITR where produitr.sous_type = "AR" and PRODUITR.gencod-v begins zzcpt no-lock no-error.
                ELSE if zzrech="x" then do :
/*$2132...
                    find PRODUITR where produitr.sous_type = "AR" and PRODUITR.refext begins zzcpt no-lock no-error.*/
                    IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type = "AR" and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error.
                        END.
                        ELSE DO :
                            /* ...$A58929 */
                            find PRODUITR where produitr.sous_type = "AR" and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" NO-LOCK NO-ERROR.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" NO-LOCK NO-ERROR. /*$2173*/
                        END. /* $A58929 */
                    END.
                    ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type = "AR" and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" and Produit-rech.refext MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock NO-ERROR.
                        END.
                        ELSE DO :
                            /* ...$A58929 */
                            find PRODUITR where produitr.sous_type = "AR" and produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" and PRODUITR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" and PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/
                        END. /* $A58929 */
                    END.
                    ELSE DO:
                        find PRODUITR where produitr.sous_type = "AR" and PRODUITR.refext begins zzcpt no-lock no-error.
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.refext BEGINS zzcpt no-lock no-error. /*$2173*/
                    END.
/*...$2132*/
                    if not available produitr then 
                        find PRODUITR where produitr.sous_type = "PS" and PRODUITR.refext begins zzcpt no-lock no-error.
                end.
                else if zzrech="i" then do :
/*$2132...
                    find PRODUITR where produitr.sous_type = "AR" and PRODUITR.refint begins zzcpt no-lock no-error.*/
                    IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type = "AR" and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where produitr.sous_type = "AR" and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" no-lock no-error.
                    END.
                    ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type = "AR" and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" and Produit-rech.refint MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where produitr.sous_type = "AR" and produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" and PRODUITR.refint MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                    END.
                    ELSE 
                        find PRODUITR where produitr.sous_type = "AR" and PRODUITR.refint begins zzcpt no-lock no-error.
/*...$2132*/                    
                    if not available produitr THEN 
                        find PRODUITR where produitr.sous_type = "PS" and PRODUITR.refint begins zzcpt no-lock no-error.
                end.
                else if champ-alpha OR SUBSTR(zzcpt,1,1)=" " then do :
                    IF SUBSTR(zzcpt,1,1)=" " THEN
                        /*$2133...*/
                        IF lgRechNom2 THEN 
                            if zzvchar2="" THEN DO:
                                /* $A58929... */
                                IF rechac-actif-pro > 0 THEN DO :
                                    FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                          rechac.typ_cha = zzrech AND 
                                                          rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                          rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                    FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type="AR" and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                        IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                        ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                    END.
                                    IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                                END.
                                ELSE 
                                    /* ...$A58929 */
                                    find PRODUITR where PRODUITR.sous_type="AR" and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") no-lock no-error.
                            END.
                            else DO:
                                /* $A58929... */
                                IF rechac-actif-pro > 0 THEN DO :
                                    FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                          rechac.typ_cha = zzrech AND 
                                                          rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                          rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                          rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                                    FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type="AR" and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                                           (Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar2 + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                        IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                        ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                    END.
                                    IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                                END.
                                ELSE 
                                    /* ...$A58929 */
                                    find PRODUITR where PRODUITR.sous_type="AR" and (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") and 
                                                                                           (PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar2 + "*") no-lock no-error.
                            END.
                        /*$...$2133*/
                        ELSE
                            if zzvchar2="" THEN DO:
                                /* $A58929... */
                                IF rechac-actif-pro > 0 THEN DO :
                                    FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                          rechac.typ_cha = zzrech AND 
                                                          rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                          rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                    FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type="AR" and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                        IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                        ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                    END.
                                    IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                                END.
                                ELSE 
                                    /* ...$A58929 */
                                    find PRODUITR where PRODUITR.sous_type="AR" and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" no-lock no-error.
                            END.
                            else DO:
                                /* $A58929... */
                                IF rechac-actif-pro > 0 THEN DO :
                                    FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                          rechac.typ_cha = zzrech AND 
                                                          rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                          rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                          rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                                    FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.sous_type="AR" and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" and Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" NO-LOCK ind-i-pro = 1 TO 2:
                                        IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                        ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                    END.
                                    IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                                    IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.typ_elem=zztype and prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/ 
                                END.
                                ELSE
                                    /* ...$A58929 */
                                    find PRODUITR where PRODUITR.sous_type="AR" and produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" and PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                            END.
                    ELSE 
                        /*$2133...*/
                        IF lgRechNom2 THEN 
                            find PRODUITR where produitr.sous_type = "AR" and (PRODUITR.nom_pro begins zzcpt OR PRODUITR.nom_pr2 BEGINS zzcpt) no-lock no-error.
                        ELSE
                            find PRODUITR where produitr.sous_type = "AR" and PRODUITR.nom_pro begins zzcpt no-lock no-error.
                        /*$...$2133*/
                    if not available produitr THEN 
                        find PRODUITR where produitr.sous_type = "PS" and PRODUITR.nom_pro begins zzcpt no-lock no-error.
                    if zzphoneProRech="yes" AND not available produitr then DO:
                        find PRODUITR where PRODUITR.sous_type="AR" and PRODUITR.phone begins zzvchar no-lock no-error.
                        IF NOT AVAIL produitr AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                            find PRODUITR where PRODUITR.sous_type="PS" and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                        ELSE IF NOT AVAIL produitr AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                            find PRODUITR where PRODUITR.sous_type="PS" and produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" and PRODUITR.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                        ELSE IF NOT AVAIL produitr THEN 
                            find PRODUITR where PRODUITR.sous_type="PS" and PRODUITR.phone begins zzvchar no-lock no-error.
                    END.
                end.
                else do :
                    find PRODUITR where produitr.sous_type = "AR" and PRODUITR.cod_pro=zzvnum no-lock no-error.
                    if not available produitr THEN 
                        find PRODUITR where produitr.sous_type = "PS" and PRODUITR.cod_pro=zzvnum no-lock no-error.
                end.
            end.
            else do:
                if zzrech="g" then 
                    find PRODUITR where PRODUITR.gencod-v begins zzcpt AND produitr.sous_type <> 'DV':U no-lock no-error.
/*$2132...
                ELSE if zzrech="x" then 
                    find PRODUITR where PRODUITR.refext begins zzcpt AND produitr.sous_type <> 'DV' no-lock no-error.
                else if zzrech="i" then 
                    find PRODUITR where PRODUITR.refint begins zzcpt AND produitr.sous_type <> 'DV' no-lock no-error.*/                    
                ELSE if zzrech="x" then DO:
                    if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" AND Produit-rech.sous_type <> 'DV':U NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error.
                        END.
                        ELSE DO :
                            /* ...$A58929 */
                            find PRODUITR where produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" no-lock no-error. /*$2173*/
                        END. /* $A58929 */
                    END.
                    else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.refext > "" AND Produit-rech.refext MATCHES "*" + zzvchar + "*" AND Produit-rech.refext MATCHES "*" + zzvchar2 + "*" AND Produit-rech.sous_type <> 'DV':U no-lock ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" AND PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/
                        END.
                        ELSE DO :
                            /* ...$A58929 */
                            find PRODUITR where produitr.refext > "" AND PRODUITR.refext MATCHES "*" + zzvchar + "*" AND PRODUITR.refext MATCHES "*" + zzvchar2 + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                            IF NOT AVAIL produitr THEN find PrmultfoR where prmultfor.refext > "" AND PrmultfoR.refext MATCHES "*" + zzvchar + "*" AND PrmultfoR.refext MATCHES "*" + zzvchar2 + "*" no-lock no-error. /*$2173*/
                        END. /* $A58929 */
                    END.
                    else DO:
                        find PRODUITR where PRODUITR.refext begins zzcpt AND produitr.sous_type <> 'DV':U no-lock no-error.
                        IF NOT AVAIL produitr THEN find PrmultfoR where PrmultfoR.refext BEGINS zzcpt no-lock no-error. /*$2173*/
                    END.
                END.
                ELSE if zzrech="i" then DO:
                    if SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                            FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" AND Produit-rech.sous_type <> 'DV':U NO-LOCK ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE
                            /* ...$A58929 */
                            find PRODUITR where produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                    END.
                    else if SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN DO:
                        /* $A58929... */
                        IF rechac-actif-pro > 0 THEN DO :
                            FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                  rechac.typ_cha = zzrech AND 
                                                  rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                  rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                  rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.refint > "" AND Produit-rech.refint MATCHES "*" + zzvchar + "*" AND Produit-rech.refint MATCHES "*" + zzvchar2 + "*" AND Produit-rech.sous_type <> 'DV':U no-lock ind-i-pro = 1 TO 2:
                                IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                            END.
                            IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                        END.
                        ELSE 
                            /* ...$A58929 */
                            find PRODUITR where produitr.refint > "" AND PRODUITR.refint MATCHES "*" + zzvchar + "*" AND PRODUITR.refint MATCHES "*" + zzvchar2 + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                    END.
                    else 
                        find PRODUITR where PRODUITR.refint begins zzcpt AND produitr.sous_type <> 'DV':U no-lock no-error.
                END.
/*...$2132*/
                ELSE IF SUBSTR(zzcpt,1,1)=" " THEN /* �XREF_WHOLE:produit� */
                    /*$2133...*/
                    IF lgRechNom2 THEN 
                        if zzvchar2="" THEN DO:
                            /* $A58929... */
                            IF rechac-actif-pro > 0 THEN DO :
                                FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                      rechac.typ_cha = zzrech AND 
                                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                      rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                    IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                    ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                END.
                                IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            END.
                            ELSE 
                                /* ...$A58929 */
                                find PRODUITR where (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") no-lock no-error. 
                        END.
                        ELSE DO: /* �XREF_WHOLE:produit� */
                            /* $A58929... */
                            IF rechac-actif-pro > 0 THEN DO :
                                FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                      rechac.typ_cha = zzrech AND 
                                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                      rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                      rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and (Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar + "*") and  
                                                                             (Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" OR Produit-rech.nom_pr2 MATCHES "*" + zzvchar2 + "*") NO-LOCK ind-i-pro = 1 TO 2:
                                    IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                    ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                END.
                                IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            END.
                            ELSE                                 
                                /* ...$A58929 */
                                find PRODUITR where (PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar + "*") and  
                                                                             (PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" OR PRODUITR.nom_pr2 MATCHES "*" + zzvchar2 + "*") no-lock no-error.
                        END.
                    /*$...$2133*/
                    ELSE
                        if zzvchar2="" THEN DO:
                            /* $A58929... */
                            IF rechac-actif-pro > 0 THEN DO :
                                FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                      rechac.typ_cha = zzrech AND 
                                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                      rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" no-lock ind-i-pro = 1 TO 2:
                                    IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                    ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                END.
                                IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            END.
                            ELSE
                                /* ...$A58929 */
                                find PRODUITR where produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" no-lock no-error. 
                        END.
                        else DO:
                            /* $A58929... */
                            IF rechac-actif-pro > 0 THEN DO :
                                FOR EACH rechac WHERE rechac.typ_fich = 'P' AND 
                                                      rechac.typ_cha = zzrech AND 
                                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-pro) AND 
                                                      rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                                      rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK,
                                FIRST Produit-rech where produit-rech.cod_pro = rechac.cod_tiers and Produit-rech.nom_pro > "" AND Produit-rech.nom_pro MATCHES "*" + zzvchar + "*" and Produit-rech.nom_pro MATCHES "*" + zzvchar2 + "*" no-lock ind-i-pro = 1 TO 2:
                                    IF vcod_rech_pro <> ? THEN ASSIGN vcod_rech_pro = 0.
                                    ELSE ASSIGN vcod_rech_pro = Produit-rech.cod_pro.
                                END.
                                IF vcod_rech_pro <> 0 AND vcod_rech_pro <> ? THEN FIND FIRST produitr WHERE produitr.cod_pro = vcod_rech_pro NO-LOCK NO-ERROR.
                            END.
                            ELSE 
                                /* ...$A58929 */
                                find PRODUITR where produitr.nom_pro > "" AND PRODUITR.nom_pro MATCHES "*" + zzvchar + "*" and PRODUITR.nom_pro MATCHES "*" + zzvchar2 + "*" no-lock no-error. 
                        END.

                else if champ-alpha then /* �XREF_WHOLE:produit� */
                    /*$2133...*/
                    IF lgRechNom2 THEN 
                        find PRODUITR where (PRODUITR.nom_pro begins zzcpt OR PRODUITR.nom_pr2 begins zzcpt) AND produitr.sous_type <> 'DV':U no-lock no-error. 
                    ELSE 
                        find PRODUITR where PRODUITR.nom_pro begins zzcpt AND produitr.sous_type <> 'DV':U no-lock no-error.
                    /*$...$2133*/
                else 
                    find PRODUITR where PRODUITR.cod_pro=zzvnum AND produitr.sous_type <> 'DV':U no-lock no-error.
                if champ-alpha AND zzphoneProRech="yes" AND not available produitr then DO:
                    IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                        find PRODUITR where produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                    ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                        find PRODUITR where produitr.phone > "" and PRODUITR.phone MATCHES "*" + zzvchar + "*" AND PRODUITR.phone MATCHES "*" + zzvchar2 + "*" AND produitr.sous_type <> 'DV':U no-lock no-error.
                    ELSE 
                        find PRODUITR where PRODUITR.phone begins zzvchar AND produitr.sous_type <> 'DV':U no-lock no-error.
                END.
            end.
        end.
    
        if (zzrech="r" OR zzrech="n") THEN DO:
            IF AVAIL specifs THEN 
                FIND produitr WHERE produitr.cod_pro = specifs.cod_pro NO-LOCK NO-ERROR.
            ELSE RELEASE produitr.
        END.
        IF zzrech="x" THEN DO:
            IF NOT AVAIL produitr AND AVAIL prmultfor THEN FIND produitr WHERE produitr.cod_pro = prmultfoR.cod_pro NO-LOCK NO-ERROR. /*$2173*/
        END.
        IF zzrech="g" /*AND AVAIL parsoc*/ AND NOT AVAIL produitr /*AND parsoc.decl_ar*/ AND NOT AMBIGUOUS produitr THEN 
            FIND gendecv WHERE gendecv.gencod-v BEGINS zzcpt NO-LOCK NO-ERROR.
        ELSE IF zzrech="i" /*AND AVAIL parsoc*/ AND NOT AVAIL produitr AND lgProRechDeclAR AND NOT AMBIGUOUS produitr THEN 
            FIND gendecv WHERE gendecv.refint BEGINS zzcpt NO-LOCK NO-ERROR.
    END.

    if available PRODUITR THEN RUN droittyp ("RE","P",PRODUITR.typ_elem,0,OUTPUT pas-droit-type).

    /* trouv�  */
    if available PRODUITR and lookup(string(produitr.statut,"9"),zzstatut)=0 AND StatutArticleDepotOk(zzstatut,produitr.cod_pro,zzdepot) AND pas-droit-type=NO THEN assign 
        zzinti = PRODUITR.nom_pro 
        zzcpt = string(PRODUITR.cod_pro) 
        zzmult=produitr.mult-fou.
    ELSE IF AVAIL gendecv AND pas-droit-type=NO THEN do:
        FIND produitr WHERE produitr.cod_pro = gendecv.cod_pro NO-LOCK NO-ERROR.
        assign 
            zzinti = PRODUITR.nom_pro 
            zzcpt = string(PRODUITR.cod_pro) 
            zzmult=produitr.mult-fou.
    END.
    /* non trouv� ou non valide */
    else do:
        if AVAIL produitr THEN message 
            Traduction("Produit",-2,"") + " " trim(produitr.nom_pro) skip Traduction("a un statut",-2,"") + " "
            (if produitr.statut=1 then "--> " + Traduction("Interdit Achat",-2,"")
             else if produitr.statut=2 then "--> " + Traduction("Interdit Vente",-2,"")
             else if produitr.statut=8 then "--> " + Traduction("Interdit Achat et Vente",-2,"")
             else if produitr.statut=9 THEN "--> " + Traduction("A Supprimer",-2,"")
             ELSE IF pas-droit-type THEN Traduction("interdit de recherche pour votre profil",-2,"")
             ELSE Traduction("particulier pour le d�p�t qui le rend inaccessible",-2,"")) 
            view-as alert-box INFO.
        zzpcpt=zzcpt.
        /* RECHERCHE PRODUIT */
        if "{1}" = "" then 
            RUN recpro_b (zzstatut,zzdepot,zzclause,zztype,zzsous-type, zzexc, zzrech, input-output zzpcpt, output zzpinti, output zzpmult).
        else IF zzpcli=0 THEN 
            run recprm_b (zzstatut,zzdepot,zzclause,zztype,zzsous-type, zzexc, zzrech, input-output zzpcpt, output zzpinti).
        ELSE 
            run recprc_b (zzpcli,zzstatut,zzdepot,zzclause,zztype, zzsous-type, zzexc, zzrech, input-output zzpcpt, output zzpinti).
        if zzpcpt <> "" then assign 
            zzcpt = zzpcpt 
            zzinti = zzpinti 
            zzmult=zzpmult.
        else zzcpt="".
    end.
END PROCEDURE.
