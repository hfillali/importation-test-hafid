DEFINE VARIABLE hapi-mobsr AS HANDLE     NO-UNDO.
DEFINE VARIABLE phgcomobsr AS HANDLE     NO-UNDO.

FUNCTION MOB-IS-SR-VALIDE       RETURNS LOG (INPUT pTyp_mvt AS CHAR,INPUT pCod_cf AS INT,INPUT pNo_cde AS INT,INPUT pNo_bl_a AS CHAR)   IN hapi-mobsr.
FUNCTION MOB-SR-VALIDATION-PIED RETURNS LOG (INPUT pTyp_mvt AS CHAR,INPUT pCod_cf AS INT,INPUT pNo_cde AS INT,INPUT pNo_bl AS INT)      IN hapi-mobsr.
FUNCTION MOB-IS-SR-VALIDABLE    RETURNS LOG (INPUT pTyp_mvt AS CHAR,INPUT pCod_cf AS INT,INPUT pNo_cde AS INT,INPUT pNo_bl_a AS CHAR)   IN hapi-mobsr.

