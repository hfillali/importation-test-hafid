/* Author: HL       
   Calcul d'un ecart de deux dates en fonction du calendrier soci�t� 

$979 DDG 23/09/04 Int�gration du module gestion des ressources (calendrier, agenda, planification r�union,...) 
$OPT PPE 01/03/10 Optimisation des acc�s � grhor
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - focondi
    !   !        - clcondi
   !  !  !
  !   !   !
 !    !    !
!!!!!!!!!!!!  SI UTILISER A PARTIR EDTCOUV2.P
*********************************************************************************/
   
PROCEDURE delai-trt-calendar :
def input parameter pdate1  as date no-undo.
def input parameter pdate2  as date no-undo.
def OUTPUT parameter pnbj   as i no-undo.

def var problem as log init yes no-undo.
def var j as i no-undo.
DEF VAR pannee AS INT no-undo.
DEF VAR pas AS INT no-undo.
DEF VAR inv AS LOG INIT FALSE no-undo.
DEF VAR tmpd AS DATE no-undo.
DEF VAR pmois AS INT no-undo. /*$979*/

DEF BUFFER optgrhor FOR grhor. /*$OPT*/
DEF VAR voptgrhor AS INT NO-UNDO. /*$OPT*/

pnbj = 0.

pas = 1.
IF pdate2 > pdate1 THEN .
ELSE IF pdate2 < pdate1 THEN ASSIGN inv = YES tmpd = pdate2 pdate2 = pdate1 pdate1 = tmpd.
ELSE 
DO:
    pnbj = 0.
    RETURN.
END.
pannee = YEAR (pdate1).

/*$979...*/
/*$OPT...*/
RELEASE grhor.
FIND FIRST optgrhor where optgrhor.typ_par = "" AND optgrhor.typ_fich = "" AND optgrhor.cod_par = "" NO-LOCK NO-ERROR.
DO WHILE AVAIL optgrhor AND NOT AVAIL grhor :
    find FIRST grhor where grhor.typ_par = "" 
                       AND grhor.typ_fich = "" 
                       AND grhor.cod_par = "" 
                       AND grhor.no_ordre = optgrhor.no_ordre 
                       AND grhor.annee = pannee 
                       AND grhor.mois = MONTH(pdate1) 
                      no-lock no-error.
   IF NOT AVAIL grhor THEN DO :
       voptgrhor = optgrhor.no_ordre .
       find FIRST optgrhor where optgrhor.typ_par = "" 
                          AND optgrhor.typ_fich = "" 
                          AND optgrhor.cod_par = "" 
                          AND optgrhor.no_ordre > voptgrhor 
                         no-lock no-error.
   END.
END.
/*find FIRST grhor where grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" AND grhor.annee = pannee AND grhor.mois = MONTH(pdate1) no-lock no-error.
...$OPT*/
if AVAIL grhor then 
do while problem :
    ASSIGN 
        pmois = MONTH(pdate1)
        pannee = YEAR(pdate1).

    IF NOT grhor.mat_trav[DAY(pdate1)] AND NOT grhor.ap_trav[DAY(pdate1)] THEN 
    DO:
        ASSIGN pdate1 = pdate1 + pas .
    END.
    ELSE
    do:
        IF (pas > 0 AND pdate1 >= pdate2) OR (pas < 0 AND pdate1 <= pdate2) THEN
            problem = NO.
        ELSE
            ASSIGN 
                pnbj = pnbj + pas
                pdate1 = pdate1 + pas .
    END.
    IF problem = YES THEN
    DO:
        IF (pannee<>year(pdate1) OR pmois <> MONTH(pdate1)) 
            /*$OPT...*/ AND NOT ( AVAIL grhor AND grhor.annee = YEAR(pdate1) AND grhor.mois = MONTH(pdate1) ) /*...$OPT*/
            then 
        DO:
            /*$OPT...*/
            RELEASE grhor.
            FIND FIRST optgrhor where optgrhor.typ_par = "" AND optgrhor.typ_fich = "" AND optgrhor.cod_par = "" NO-LOCK NO-ERROR.
            DO WHILE AVAIL optgrhor AND NOT AVAIL grhor :
                find FIRST grhor where grhor.typ_par = "" 
                                   AND grhor.typ_fich = "" 
                                   AND grhor.cod_par = "" 
                                   AND grhor.no_ordre = optgrhor.no_ordre 
                                   AND grhor.annee = pannee 
                                   AND grhor.mois = MONTH(pdate1) 
                                  no-lock no-error.
               IF NOT AVAIL grhor THEN DO :
                   voptgrhor = optgrhor.no_ordre .
                   find FIRST optgrhor where optgrhor.typ_par = "" 
                                      AND optgrhor.typ_fich = "" 
                                      AND optgrhor.cod_par = "" 
                                      AND optgrhor.no_ordre > voptgrhor 
                                     no-lock no-error.
               END.
            END.
            /*
            FIND FIRST grhor WHERE grhor.typ_par = "" AND grhor.typ_fich = "" AND grhor.cod_par = "" AND grhor.annee = YEAR(pdate1) AND grhor.mois = MONTH(pdate1) NO-LOCK NO-ERROR.
            ...$OPT*/
            IF NOT AVAIL grhor THEN
                problem = NO.
        END.
    END.
END.
/*...$979*/
IF inv THEN pnbj = pnbj * -1.
END PROCEDURE.
