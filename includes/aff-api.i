/* JR 16/02/2006 API pour les Affaires  
$2184 YO 13/04/10 Refonte du lien entre la Gestion de projets et les saisies des temps affaire 
$A33800 IRR 04/04/11 Possibilit� de rendre obligatoire l'affaire
$A32485 IRR 31/05/11 Droits Cr�ation/Modification affaire selon droits utilisateurs
$A41656 CGU 19/08/11 Fonctions de cr�ation / modification de temps pour la pointeuse
*/


&IF DEFINED(G-hAff) = 0 &THEN
&GLOBAL-DEFINE G-hAff G-hAff
DEF VAR G-hAff AS HANDLE NO-UNDO.
DEF VAR G-hAff### AS C NO-UNDO.
G-hAff### = GetSessionData ("AFFAIRE","CLE").
G-hAff = WIDGET-HANDLE(G-hAff###) NO-ERROR.
FUNCTION AFF_COUT_HOR_INT RETURNS DECIMAL (INPUT codint AS CHAR, INPUT nature AS INT) IN G-hAff.
FUNCTION AFF_COUT_HOR_MAT RETURNS DECIMAL (INPUT ptyp_fich AS CHAR, INPUT pmateriel AS CHAR, INPUT pno_ordre AS INT) IN G-hAff.
FUNCTION AFF_MONTANT_POINTAGE RETURNS DECIMAL (INPUT pno_unique AS INT, INPUT pindice AS INT) IN G-hAff.

/* Cr�ation ligaffai*/
FUNCTION AFF_CREER_TEMPS_MATERIEL RETURNS LOG (INPUT codaff AS CHAR, INPUT Typsai AS INT, INPUT codint AS CHAR,
                                               INPUT datsai AS DATE, INPUT hdeb AS INT, INPUT hfin AS INT,
                                               INPUT codtrav AS INT, INPUT nomtrav AS CHAR, INPUT objtrav AS CHAR, INPUT nbheure AS DEC,
                                               /* Optionnels ... */ INPUT codcli AS INT, INPUT lieu AS CHAR,
                                               INPUT tx_horaire AS DEC, INPUT typfich AS CHAR, INPUT mat AS CHAR, INPUT nordre AS INT, /*...optionnels*/ OUTPUT Numord AS INT
                                               /* $2184 ... */ , INPUT pTache AS CHAR, INPUT pProjet AS CHAR, INPUT pTauxavc AS DEC, INPUT pTraiterGPJ AS INT, INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /*...$2184*/ , INPUT pmat_am AS CHAR) IN G-hAff.


/* $2241 ... */
/* Modifier temps intervenant 
Adapt� uniquement pour les saisies des temps intervenants !!! 
*/
FUNCTION AFF_MODIFIER_TEMPS_TRAVAIL RETURNS LOG (INPUT pnounique AS INT,    /* Identifiant de la ligne � modifier */
                                                 INPUT paffaire  AS CHAR,   /* Nouvelle affaire */
                                                 INPUT pcodint   AS CHAR,   /* Nouvel intervenant */
                                                 INPUT pdatsai   AS DATE,   /* Nouvelle date saisie*/
                                                 INPUT phdeb     AS INT,    /* Heure de d�but */
                                                 INPUT phfin     AS INT,    /* Heure de fin */
                                                 INPUT pcodtrav  AS INT,    /* Code travail */
                                                 INPUT pobjtrav   AS CHAR,   /* Objet */
                                                 INPUT pnbheure   AS DEC,    /* Qt� heure */
                                                 OUTPUT pNumord   AS INT,    /* Nouveau n� ordre*/
                                                 INPUT pTache    AS CHAR, INPUT pProjet AS CHAR, INPUT pTauxavc AS DEC, INPUT pTraiterGPJ AS INT,  /* informations gestion de projet */
                                                 INPUT pnompgm AS CHAR, 
                                                 INPUT ppointeuse AS LOG, /*$A41656*/
                                                 OUTPUT pmsg AS CHAR) IN G-hAff. /* message d'erreur en retour */

/* Modification ligaffai */
FUNCTION AFF_MODIFIER_TPS_MATERIEL    RETURNS LOG (INPUT pno_unique AS INT, INPUT ptyp_fich AS CHAR, INPUT pmateriel AS CHAR, INPUT pno_ordre AS INT /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_COD_INT     RETURNS LOG (INPUT pno_unique AS INT, INPUT pcod_int AS CHAR /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_DAT_SAI     RETURNS LOG (INPUT pno_unique AS INT, INPUT pdat_sai AS DATE /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_AFFAIRE     RETURNS LOG (INPUT pno_unique AS INT, INPUT paffaire AS CHAR /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_CLIENT      RETURNS LOG (INPUT pno_unique AS INT, INPUT pclient AS INT /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_QTE         RETURNS LOG (INPUT pno_unique AS INT, INPUT pqte AS DEC /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_HEURE       RETURNS LOG (INPUT pno_unique AS INT, INPUT ph_deb AS INT, INPUT ph_fin AS INT /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_TRAVAIL     RETURNS LOG (INPUT pno_unique AS INT, INPUT ptravail AS INT /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_TACHE       RETURNS LOG (INPUT pno_unique AS INT, INPUT ptache AS CHAR /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_OBJET       RETURNS LOG (INPUT pno_unique AS INT, INPUT pobjet AS CHAR /* $2184 ... */ , INPUT pnompgm AS CHAR, OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_POINTAGE    RETURNS LOG (INPUT pno_unique AS INT, INPUT pindice AS INT, INPUT ppointage AS DEC /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_POINTAGE_EX RETURNS LOG (INPUT pno_unique AS INT, INPUT pindice AS INT,INPUT ppointage AS DEC,INPUT pval_unit AS DEC,INPUT pval_unit_dev AS DEC,INPUT pdevise AS CHAR,INPUT ptaux_change AS DEC,INPUT pcomment AS CHAR /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_TEXTE       RETURNS LOG (INPUT pno_unique AS INT, INPUT ptexte AS CHAR /* $2184 ... */ , INPUT pnompgm AS CHAR , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_MODIFIER_TPS_ZONE        RETURNS LOG (INPUT pno_unique AS INT, INPUT pzone AS CHAR, INPUT pindice AS INT, INPUT pvaleur AS CHAR /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.

/* Suppression ligaffai, ligafrais */
FUNCTION AFF_SUPPRIMER_TPS_TRAVAIL  RETURNS LOG (/* $2184 INPUT pconfirm AS LOG,*/ INPUT pno_unique AS INT /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.
FUNCTION AFF_SUPPRIMER_TPS_POINTAGE RETURNS LOG (INPUT pno_unique AS INT, INPUT pindice AS INT /* $2184 ... */ , OUTPUT pmsg AS CHAR /* ... $2184 */) IN G-hAff.

FUNCTION AFF_OBTENIR_PARAMETRAGE    RETURNS CHAR (INPUT pcode AS CHAR) IN G-hAff.
FUNCTION AFF_TEST_VALIDITE          RETURNS LOG (INPUT ppgm AS INT,INPUT pcod_pers AS CHAR, INPUT paff AS CHAR, INPUT pdat AS DATE, INPUT pvalibre AS CHAR,OUTPUT ptexte AS CHAR) IN G-hAff. /*$A33800*/
                                                    /*ppgm  =>  1 : Saisie des temps
                                                                2: Commande fournisseur (ent�te)
                                                                3: Commande fournisseur (ligne)
                                                                4: Entr�e en stock hors commande
                                                                5: Sortie de stock autre que factur�e
                                                                6: Demande de prix
                                                                7: Demande d'achat
                                                                8: Devis client
                                                                9: Commande client
                                                                10: Facture client
                                                                11: Ev�nement GRC
                                                                12: Intervention GRC
                                                                13: Contr�le facture fournisseur*/

FUNCTION AFF_DROITS_MODIFS RETURNS LOG(INPUT pcod_pers AS CHARACTER)  IN G-hAff.  /*$A32485*/
FUNCTION AFF_DROITS RETURNS LOG(INPUT pcod_pers AS CHARACTER,INPUT pdroits AS CHARACTER)  IN G-hAff.  /*$A32485*/

IF NOT VALID-HANDLE (G-hAff) THEN DO:
    RUN aff-api PERSISTENT SET G-hAff.
    SetSessionData ("AFFAIRE","CLE",STRING(G-hAff)).
END.
&ENDIF
