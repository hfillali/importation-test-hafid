/* Auteur : HL
Fin du format du prix en fonction des d�cimales g�r�es � la vente 
N�cessite l'include {deciv.i}
*/
FUNCTION FormVte RETURNS CHAR (INPUT devise AS CHAR):
    if Devise=g-dftdev or Devise="" then RETURN fmtv.
    else do:
        find first tabcomp where type_tab="DE" and a_tab=Devise no-lock no-error.     
        IF AVAIL tabcomp THEN RETURN (IF deb_ce = 0 THEN "9" ELSE "9." + FILL("9",deb_ce)).
        ELSE RETURN fmtv.
    end.
END FUNCTION.
