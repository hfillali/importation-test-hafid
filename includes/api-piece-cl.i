/* cr�� par GC le 01/04/10 

A32998 HL 02/07/10 ListeDepots : contient la liste des d�p�ts autoris�s, ce qui permet de g�rer quand m�me la ligne <Tous d�p�ts> 
    chose qu'on ne pouvait pas faire auparavant quand l'un des d�p�ts n'�tait pas autoris�
$A38917 GC 17/02/11 clause concernant les droits d'affichage du client    
A41366 HL 21/07/11 Acc�s � la saisie d'une ligne commande m�me si B.P �dit� (lignecli.typ_rem 1 & 2)
*/

DEF VAR hapi-piece-cl AS HANDLE NO-UNDO.

/* ************************ D�but partie cr�ation browse type saicl1 ************************************** */

FUNCTION PCL_CREATION_BROWSE_PIECE RETURNS HANDLE (INPUT pidObjPCE AS CHAR, INPUT pTitre AS CHAR, INPUT px AS INT, INPUT py AS INT, INPUT pw AS INT, INPUT ph AS INT,
                                                          INPUT phframe AS HANDLE, INPUT ptint AS CHAR, INPUT pqliste-champ AS CHAR, INPUT pqliste-label AS CHAR,
                                                          INPUT pqcondition AS CHAR, INPUT pqcolonne-lock AS INT, INPUT pcolord AS CHAR, INPUT pcoldesc AS LOG,
                                                          INPUT pchQryBy AS CHAR, INPUT pRect AS LOG, INPUT pCombar AS LOG,
                                                          INPUT prX AS INT, INPUT prY AS INT, INPUT prW AS INT, INPUT prH AS INT) IN hapi-piece-cl.

/* permet de recr�er le tableau (uniquement le tableau, pas les buffers, query...) apr�s une r�organisation des colonnes par exemple  */
FUNCTION PCL_CREATION_TABLEAU RETURNS HANDLE (INPUT pidObjPCE AS CHAR) IN hapi-piece-cl.
/* Query prepare */
FUNCTION PCL_PREPARE_QUERY RETURNS LOGICAL (INPUT pidObjPCE AS CHAR) IN hapi-piece-cl.
/* openquery */
FUNCTION PCL_OPENQUERY RETURNS LOGICAL (INPUT pidObjPCE AS CHAR) IN hapi-piece-cl.

FUNCTION PCL_SET_QLISTE_CHAMP  RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqliste-champ AS CHAR)     IN hapi-piece-cl.
FUNCTION PCL_SET_QLISTE_LABEL  RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqliste-label AS CHAR)     IN hapi-piece-cl.
FUNCTION PCL_SET_QCONDITION    RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqcondition AS CHAR)       IN hapi-piece-cl.
FUNCTION PCL_SET_QCONDITIONP   RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqconditionp AS CHAR)      IN hapi-piece-cl.
FUNCTION PCL_SET_QCOLONNE_LOCK RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqcolonne-lock AS INT)     IN hapi-piece-cl.
FUNCTION PCL_SET_COLORD        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcolord AS CHAR)           IN hapi-piece-cl.
FUNCTION PCL_SET_COLDESC       RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcoldesc AS LOG)           IN hapi-piece-cl.
FUNCTION PCL_SET_CHQRYBY       RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pchQryBy AS CHAR)          IN hapi-piece-cl.
FUNCTION PCL_ACTU_OBJETS_FRAME RETURNS LOGICAL (INPUT phframe AS HANDLE)                                  IN hapi-piece-cl.

FUNCTION PCL_SET_TINT RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptint AS CHAR)                      IN hapi-piece-cl.
FUNCTION PCL_SET_DEVTERM RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdevterm AS LOG)                 IN hapi-piece-cl.
FUNCTION PCL_SET_NATURE RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pnature AS CHAR)                  IN hapi-piece-cl.
FUNCTION PCL_SET_DEPOT RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdepot AS INT)                     IN hapi-piece-cl.
FUNCTION PCL_SET_LISTEDEPOTS    RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT plistedepots AS CHAR)     IN hapi-piece-cl.
FUNCTION PCL_SET_TYPE_SAISIE RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptype-saisie AS CHAR)        IN hapi-piece-cl.
FUNCTION PCL_SET_TOUTES_SAISIES RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptoutes-saisies AS LOG)   IN hapi-piece-cl.
FUNCTION PCL_SET_COD_CLI RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcod_cli AS INT)                 IN hapi-piece-cl.
FUNCTION PCL_SET_VOIR_INFOS RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pvoirinfos AS LOG)            IN hapi-piece-cl.

FUNCTION PCL_SET_QUI RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqui AS CHAR)                        IN hapi-piece-cl.
FUNCTION PCL_SET_DATECDE RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdatecde AS DATE)                IN hapi-piece-cl.
FUNCTION PCL_SET_DATELIV RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdateliv AS DATE)                IN hapi-piece-cl.
FUNCTION PCL_SET_SEM RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT psem AS DATE)                        IN hapi-piece-cl.
FUNCTION PCL_SET_TYP_CDE RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptypcde AS INT)                  IN hapi-piece-cl.
FUNCTION PCL_SET_NOTREF RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pnotref AS CHAR)                  IN hapi-piece-cl.
FUNCTION PCL_SET_REFCDE RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT prefcde AS CHAR)                  IN hapi-piece-cl.
FUNCTION PCL_GET_VALEUR RETURNS CHAR (INPUT pnomzon AS CHAR)                                              IN hapi-piece-cl.
FUNCTION PCL_GET_ROWID RETURNS ROWID (INPUT pidObjPCE AS CHAR)                                            IN hapi-piece-cl.


/* ************************ Fin partie cr�ation browse type saicl1 **************************************** */

/* --Contr�le pour afficher les boutons d'acc�s aux fonctionnalit�s-- */
FUNCTION PCL_AFF_EDT_DEV                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_BI                  RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_BP                  RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_ARC                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_FAC                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_BL                  RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_SPE                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_PROFORMA            RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_EDT_BON_COM             RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.

FUNCTION PCL_AFF_TRF_DEV_CMD             RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_TRF_CMD_DEV             RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_TRF_CMD_BL              RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_TRF_FAC_COMPTOIR        RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.

FUNCTION PCL_AFF_VERIFPX                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_VERSDEV                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_RELANCE                 RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_AVANCEMENT              RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_SAISIE_LIVRAISON        RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_VALIDATION_BL           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_ANNULATION_BP           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_PROP_CMD_FOU            RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_GEN_MASSE               RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_MODIFICATION            RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_AFF_ANNULATION_BL           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.

/* --Contr�le pour acc�der aux fonctionnalit�s-- */
/* Editions */
FUNCTION PCL_CTRL_ACCES_EDT_DEV          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_BI           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_BP           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_ARC          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_FAC          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_BL           RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_EDT_SPE          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_EDT_PROFORMA     RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_BON_COM          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_GEN_MASSE        RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_PIECE            RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.

/* Transferts */
FUNCTION PCL_CTRL_ACCES_TRF_DEV_CMD      RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_TRF_CMD_DEV      RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_TRF_CMD_BL       RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_TRF_FAC_COMPTOIR RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 

/* Autres */
FUNCTION PCL_CTRL_ACCES_VERIFPX          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_VERSDEV          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_RELANCE          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_SOCIETE          RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_AVANCEMENT       RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_CREATION         RETURNS LOGICAL (INPUT ptyp_sai AS CHAR, INPUT pclient AS INT, INPUT pqui AS CHAR, INPUT pdatcde AS DATE, INPUT pdatliv AS DATE, INPUT psemliv AS INT, OUTPUT perr-objet AS CHAR, OUTPUT perr-txtmsg AS CHAR) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_MODIFICATION     RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_SUPPRESSION      RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_SAISIE_LIVRAISON RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_VALIDATION_BL    RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.  
FUNCTION PCL_CTRL_ACCES_ANNULATION_BP    RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_ANNULATION_BL    RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl.
FUNCTION PCL_CTRL_ACCES_PROP_CMD_FOU     RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 
FUNCTION PCL_CTRL_ACCES_ANNULATION_BL    RETURNS LOGICAL (INPUT prentetcli AS ROWID) IN hapi-piece-cl. 

/* renvoi un bout de requ�te client si il y a des droit par rapport � mes clients en tant que ou par rapport au type de client */
FUNCTION PCL_DROIT_CLIENT                RETURNS CHAR () IN hapi-piece-cl.  /* $A38917 */
FUNCTION PCL_DROIT_UN_CLIENT             RETURNS LOGICAL (INPUT pcodCli AS INT) IN hapi-piece-cl.

FUNCTION PCL_REDIMENSIONNER              RETURNS LOG     (INPUT pId AS CHAR, INPUT pFrame AS HANDLE)                                    IN hapi-piece-cl.
FUNCTION PCL_MASQUERCADRE                RETURNS LOG     (INPUT pidobjPCE AS CHAR, INPUT pMasquer AS LOG)                               IN hapi-piece-cl.
FUNCTION PCL_NBENREG                     RETURNS INT     (INPUT pidObjPCE AS CHAR)                                                      IN hapi-piece-cl.
FUNCTION PCL_SUPPRIMERTT                 RETURNS LOG     (INPUT pidObjPCE AS CHAR)                                                      IN hapi-piece-cl. 
FUNCTION PCL_INITONGLETRECAP             RETURNS LOG     (INPUT pIdObjPCE AS CHAR)                                                      IN hapi-piece-cl.
FUNCTION PCL_COMPTERNBENREG              RETURNS INT     (INPUT pIdObjPCE AS CHAR)                                                      IN hapi-piece-cl.
FUNCTION PCL_REINITDIM                   RETURNS LOG     (INPUT pId AS CHAR, INPUT pWidth AS DEC, INPUT pHeight AS DEC)                 IN hapi-piece-cl.
FUNCTION PCL_RESIZEWINDOW                RETURNS LOG     (INPUT pHndGedErgo AS HANDLE, INPUT pId AS CHAR, INPUT pResize AS CHAR)        IN hapi-piece-cl.
FUNCTION PCL_DELRESIZEWINDOW             RETURNS LOG     (INPUT pHndGedErgo AS HANDLE, INPUT pId AS CHAR)                               IN hapi-piece-cl.


/*
--> Edition devis
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_EDITION_DEV                 IN hapi-piece-cl (prentetcli).                     

--> Edition Bon Interne
--- D�claration de la proc�dure "CacherChildInfo" possible
RUN PCL_EDITION_BON_INTERNE         IN hapi-piece-cl (prentetcli).                     

--> Edition B.P
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_EDITION_BP                  IN hapi-piece-cl (prentetcli).                     

--> Edition A.R.C
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_EDITION_ARC                 IN hapi-piece-cl (prentetcli).                     

--> Edition sp�cification bois
RUN PCL_EDITION_SPECIFICATION_BOIS  IN hapi-piece-cl (prentetcli).

--> Edition Facture
RUN PCL_EDITION_FACTURE             IN hapi-piece-cl (prentetcli).                     

--> Edition B.L
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_EDITION_BL                  IN hapi-piece-cl (prentetcli).                     

--> Edition PROFORMA
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_EDITION_PROFORMA            IN hapi-piece-cl (prentetcli).                     

--> Edition Bon de commission
RUN PCL_EDITION_BON_COM             IN hapi-piece-cl (prentetcli).

--> Evoi d'un B.L par mail ou fax
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_ENVOI_BL_MAIL_FAX           IN hapi-piece-cl (prentetcli, pfax).               

--> Envoi d'un devis par mail ou par fax
--- D�claration de la proc�dure "CacherChildInfo" et "refresh-hbrowse" possible
RUN PCL_ENVOI_DEV_MAIL_FAX          IN hapi-piece-cl (prentetcli, pfax, pedt, pniv).   

--> Envoi d'une facture par mail ou par fax
RUN PCL_ENVOI_FACTURE_MAIL_FAX      IN hapi-piece-cl (prentetcli, pfax).               

--> Envoi d'un A.R.C par mail ou par fax
RUN PCL_ENVOI_ARC_MAIL_FAX          IN hapi-piece-cl (prentetcli, pfax).               

--> Annuler un B.P
--- D�claration de la proc�dure "refresh-hbrowse" possible
RUN PCL_ANNULATION_BP               IN hapi-piece-cl (prentetcli, OUTPUT logbid).                     

--> Annuler un B.L
--- D�claration de la proc�dure "openquery-pce" possible
RUN PCL_ANNULATION_BL               IN hapi-piece-cl (prentetcli, OUTPUT logbid).                     

--> Transformer le devis en commande
--- D�claration de la proc�dure "reposition-rowid" possible
RUN PCL_TRANSFERT_DEVIS_CMD         IN hapi-piece-cl (prentetcli).                     

--> Transformer la commande en B.L
--- D�claration de la proc�dure "reposition-rowid" possible
RUN PCL_TRANSFERT_CMD_BL            IN hapi-piece-cl (prentetcli).                     

--> Transformer la commande en devis
--- D�claration de la proc�dure "reposition-rowid" possible
RUN PCL_TRANSFERT_CMD_DEV           IN hapi-piece-cl (prentetcli).                     

--> Transformer la facture en saisie comptoir
--- D�claration de la proc�dure "reposition-rowid" possible
RUN PCL_TRANSFERT_FACTURE_COMPTOIR  IN hapi-piece-cl (prentetcli).                     

--> Contr�le de l'encours
PROCEDURE PCL_CTRL_ENCOURS_CLIENT   IN hapi-piece-cl (pclient, ptyp_sai, OUTPUT pretour).

--> Supprimer la pi�ce
--- D�claration de la proc�dure "actuSupp" possible
RUN PCL_SUPPRIMER_PIECE             IN hapi-piece-cl (prentetcli).

--> Valider un B.L
--- D�claration de la proc�dure "openquery-pce" possible
RUN PCL_VALIDATION_BL               IN hapi-piece-cl (prentetcli).

--> V�rifier les prix
--- D�claration de la proc�dure "refresh-hbrowse" possible
RUN PCL_VERIFPX                     IN hapi-piece-cl (prentetcli).

--> Avancement
--- D�claration de la proc�dure "cacherchildinfo" possible
RUN PCL_AVANCEMENT                  IN hapi-piece-cl (prentetcli).

--> Versions d'un devis
RUN PCL_VERSIONS_DEVIS              IN hapi-piece-cl (prentetcli).

--> Relance d'un devis
--- D�claration de la proc�dure "refresh-hbrowse" possible
RUN PCL_RELANCE_DEVIS               IN hapi-piece-cl (prentetcli).

--> Visualisation d'une pi�ce
--- D�claration de la proc�dure "cacherchildinfo" possible
RUN PCL_VISU_PIECE                  IN hapi-piece-cl (prentetcli).

--> Proposition de commande fournisseur                
RUN PCL_PROP_CMD_FOU                IN hapi-piece-cl (prentetcli).

--> G�n�ration en masse                
RUN PCL_GENERATION_MASSE            IN hapi-piece-cl (prentetcli).
*/

/* 
������������������������������������������������������������
Contr�ler l'existence des proc�dures suivantes dans le programme appelant (non bloquant) :
- refresh-hbrowse
- CacherChildInfo
- reposition-rowid
������������������������������������������������������������
*/
