/* Cr�� par GC le 06/07/10

A32998 HL 02/07/10 ListeDepots : contient la liste des d�p�ts autoris�s, ce qui permet de g�rer quand m�me la ligne <Tous d�p�ts> 
    chose qu'on ne pouvait pas faire auparavant quand l'un des d�p�ts n'�tait pas autoris�
$A39552 GC 28/03/11 Infos flash - Tenir compte des droits utilisateur + pouvoir cr�er une proposition de commande par clique droit
 */

DEF VAR hapi-piece-fo AS HANDLE NO-UNDO.

/* Contr�le */
FUNCTION PFO_CTRL_ACCES_PIECE        RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_CREATION     RETURNS LOGICAL (INPUT pcodfou AS INT, INPUT pqui AS CHAR, INPUT pdate1 AS DATE, INPUT pdate2 AS DATE, 
                                                      INPUT psem AS INT, OUTPUT perr-objet AS CHAR, OUTPUT perr-txtmsg AS CHAR) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_MODIFICATION RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_SUPPRESSION  RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_EDT_SPE      RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_EDT_PROP     RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_EDT_CDE      RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_SAISIE_REC   RETURNS LOGICAL (INPUT prentetfou AS ROWID, OUTPUT paction AS CHAR) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_VAL_PROP     RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_ECLATER_PROP RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_BON_DECHGT   RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_CTRL_ACCES_CMD_TRSP     RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.

/* affichage */
FUNCTION PFO_ENABLE_MAIL             RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_ENABLE_FAX              RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_ENABLE_CHANGER          RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_ENABLE_AFFCDE           RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_ENABLE_ECLATER          RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_ENABLE_ECLATER_PAR_DATE RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_AFF_SAISIE_REC          RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_AFF_VALIDER_PROP        RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_AFF_EDITER_CDE          RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.
FUNCTION PFO_AFF_EDITER_PROP         RETURNS LOGICAL (INPUT prentetfou AS ROWID) IN hapi-piece-fo.

/* Cr�ation Browse */
FUNCTION PFO_CREATION_BROWSE_PIECE RETURNS HANDLE   (INPUT pidObjPCE AS CHAR, INPUT pTitre AS CHAR, INPUT px AS INT, INPUT py AS INT, INPUT pw AS INT, INPUT ph AS INT,
                                                     INPUT phframe AS HANDLE, INPUT porigine AS INT, INPUT pqliste-champ AS CHAR, INPUT pqliste-label AS CHAR,
                                                     INPUT pqcondition AS CHAR, INPUT pqcolonne-lock AS INT, INPUT pcolord AS CHAR, INPUT pcoldesc AS LOG,
                                                     INPUT pchQryBy AS CHAR, INPUT pRect AS LOG, INPUT pCombar AS LOG,
                                                     INPUT prX AS INTEGER, INPUT prY AS INTEGER, INPUT prW AS INTEGER, INPUT prH AS INTEGER) IN hapi-piece-fo.
FUNCTION PFO_CREATION_TABLEAU RETURNS HANDLE (INPUT pidObjPCE AS CHAR) IN hapi-piece-fo. 
FUNCTION PFO_PREPARE_QUERY RETURNS LOGICAL (INPUT pidObjPCE AS CHAR)   IN hapi-piece-fo. 
FUNCTION PFO_OPENQUERY RETURNS LOGICAL (INPUT pidObjPCE AS CHAR)       IN hapi-piece-fo.
FUNCTION PFO_GET_ROWID RETURNS ROWID (INPUT pidObjPCE AS CHAR)         IN hapi-piece-fo.

FUNCTION PFO_SET_QLISTE_CHAMP   RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqliste-champ AS CHAR)  IN hapi-piece-fo.
FUNCTION PFO_SET_QLISTE_LABEL   RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqliste-label AS CHAR)  IN hapi-piece-fo.
FUNCTION PFO_SET_QCONDITION     RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqcondition AS CHAR)    IN hapi-piece-fo.
FUNCTION PFO_SET_QCOLONNE_LOCK  RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqcolonne-lock AS INT)  IN hapi-piece-fo.
FUNCTION PFO_SET_COLORD         RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcolord AS CHAR)        IN hapi-piece-fo.
FUNCTION PFO_SET_COLDESC        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcoldesc AS LOG)        IN hapi-piece-fo.
FUNCTION PFO_SET_CHQRYBY        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pchQryBy AS CHAR)       IN hapi-piece-fo.
FUNCTION PFO_SET_QCONDITIONP    RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqconditionp AS CHAR)   IN hapi-piece-fo.
FUNCTION PFO_SET_COD_FOU        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pcod_fou AS INT)        IN hapi-piece-fo.
FUNCTION PFO_SET_DEPOT          RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdepot AS INT)          IN hapi-piece-fo.
FUNCTION PFO_SET_LISTEDEPOTS    RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT plistedepots AS CHAR)   IN hapi-piece-fo.
FUNCTION PFO_SET_NATURE         RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pnature AS CHAR)        IN hapi-piece-fo.
FUNCTION PFO_SET_TYPE_SAISIE    RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptype-saisie AS CHAR)   IN hapi-piece-fo.
FUNCTION PFO_SET_TOUTES_SAISIES RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT ptoutes-saisies AS LOG) IN hapi-piece-fo.
FUNCTION PFO_SET_SEMAINE        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT psem AS INT)            IN hapi-piece-fo.
FUNCTION PFO_SET_DAT_CDE        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdat-cde AS DATE)       IN hapi-piece-fo.
FUNCTION PFO_SET_DAT_LIV        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pdat-liv AS DATE)       IN hapi-piece-fo.
FUNCTION PFO_SET_REF_CDE        RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pref-cde AS DATE)       IN hapi-piece-fo.
FUNCTION PFO_SET_QUI            RETURNS LOGICAL (INPUT pidObjPCE AS CHAR, INPUT pqui AS CHAR)           IN hapi-piece-fo.
FUNCTION PFO_GET_VALEUR         RETURNS CHAR (INPUT pnomzon AS CHAR)                                    IN hapi-piece-fo.
FUNCTION PFO_ACTU_OBJETS_FRAME  RETURNS LOGICAL (INPUT phframe AS HANDLE)                               IN hapi-piece-fo.

FUNCTION PFO_REDIMENSIONNER     RETURNS LOG     (INPUT pId AS CHAR, INPUT pFrame AS HANDLE)             IN hapi-piece-fo.
FUNCTION PFO_MASQUERCADRE       RETURNS LOG     (INPUT pIdObjPCE AS CHAR, INPUT pMasquer AS LOG)        IN hapi-piece-fo.
FUNCTION PFO_NBENREG            RETURNS INT     (INPUT pidObjPCE AS CHAR)                               IN hapi-piece-fo.
FUNCTION PFO_SUPPRIMERTT        RETURNS LOG     (INPUT pId AS CHAR)                                     IN hApi-piece-fo.
FUNCTION PFO_INITONGLETRECAP    RETURNS LOG     (INPUT pIdObjPCE AS CHAR)                               IN hApi-piece-fo.
FUNCTION PFO_COMPTERNBENREG     RETURNS INT     (INPUT pIdObjPCE AS CHAR)                               IN hApi-piece-fo.
FUNCTION PFO_REINITDIM          RETURNS LOG     (INPUT pIdObjPCE AS CHAR, INPUT pWidth AS DEC, INPUT pHeight AS DEC)                        IN hApi-piece-fo.
FUNCTION PFO_RESIZEWINDOW       RETURNS LOG     (INPUT pHndGedErgo AS HANDLE, INPUT pIdObjPCE AS CHAR, INPUT pResize AS CHAR)               IN hApi-piece-fo.
FUNCTION PFO_DELRESIZEWINDOW    RETURNS LOG     (INPUT pHndGedErgo AS HANDLE, INPUT pIdObjPCE AS CHAR)                                      IN hApi-piece-fo.

FUNCTION PFO_DROIT_FOURNISSEUR RETURNS CHAR () IN hApi-piece-fo. /* $A39552 */
FUNCTION PFO_DROIT_UN_FOURNISSEUR RETURNS LOGICAL (INPUT pcodFou AS INT) IN hApi-piece-fo.
