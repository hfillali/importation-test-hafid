/* 
Auteur : HL
Renvoie le statut de la pi�ce client
$TRAD AOR 20/11/14 ajout traduction
*/
{1}.sta_pce=STRING(
  (IF {1}.typ_sai="W" THEN Traduction("Web",-2,"") ELSE 
   IF {1}.typ_sai="D" AND {1}.k_postf=1 THEN Traduction("Devis �dit�",-2,"") ELSE 
   IF {1}.typ_sai="C" AND {1}.der_bp<>0 THEN (IF {1}.nb_bp<2 THEN Traduction("B.P",-2,"") + " " 
   				                                                          ELSE Traduction("Rel B.P",-2,"") + " ") +  TRIM(STRING({1}.der_bp,">>>>>>>>>")) ELSE 
   IF {1}.typ_sai="C" AND {1}.der_bp=0 AND {1}.bl_sai=0 AND {1}.nb_bp>= 1 THEN Traduction("Reliquat",-2,"") ELSE 
   IF {1}.typ_sai="C" AND {1}.der_bp=0 AND {1}.bl_sai<>0 THEN (IF {1}.nb_bp=0 THEN "(" + Traduction("B.P ann",-2,"") + " " 
                                                                              ELSE "(" + Traduction("Rel B.P ann",-2,"") + " ") 
                                                            + TRIM(STRING({1}.bl_sai,">>>>>>>>>")) + "-" + TRIM(STRING({1}.ind_bp,">>>>>>>>>")) + ")" ELSE 
   IF {1}.no_fact<>0 AND {1}.fac_maj THEN SUBSTITUTE(Traduction("Fac &1 val",-2,""),STRING({1}.no_fact,">>>>>>>>>")) ELSE 
   IF {1}.no_fact<>0 AND {1}.fac_edi THEN SUBSTITUTE(Traduction("Fac &1 �dt",-2,""),STRING({1}.no_fact,">>>>>>>>>")) ELSE 
   IF {1}.no_fact<>0 THEN Traduction("Fac",-2,"") + " " +  STRING({1}.no_fact,">>>>>>>>>") 
   ELSE "")  +
  (IF {1}.achev=no THEN "/" + trim({1}.usr_mod) ELSE "") +
  (IF {1}.typ_sai<>"D" AND {1}.typ_sai<>"W" AND {1}.retr_cess="" AND {1}.NO_devis<>0 THEN "/" + "<" + "" + "-" + Traduction("D",-2,"") ELSE "") + 
  (IF {1}.typ_sai<>"D" AND {1}.typ_sai<>"W" AND ({1}.k_postf=1 OR {1}.k_postf=3) THEN "" + "/" + Traduction("A.R.C",-2,"") ELSE "") + 
  (IF {1}.typ_sai<>"D" AND {1}.typ_sai<>"W" AND {1}.k_postf>1 THEN "" + "/" + Traduction("B.L",-2,"") ELSE "")
  , "X(36)").
