/* 
$2222 CGU 31/05/09 Ajout de fonctions pour recherche par crit�re et consulter les �ditions archiv�es
$AP AP 22/10/12 modification pour compatibilit� web
$A74379 LLH 10/03/14 Ajout des m�thode de parcours des crit�res par emplacement
$A74433 LLH 12/03/14 Ajout de m�thodes de cr�ation de chapitre par mois/ann�e
$A75592 IRR 16/05/14 Gestion du multi crit�re
*/

&IF DEFINED(G-hGED) = 0 &THEN

&GLOBAL-DEFINE G-hGED G-hGED
DEF VAR G-hGED AS HANDLE NO-UNDO.

&IF opsys <> "unix" &THEN /*$AP : initialisation de g-hged par une fonction*/
DEF VAR G-hGED### AS C NO-UNDO.
G-hGED### = GetSessionData ("GED","CLE":U).
G-hGED = WIDGET-HANDLE(G-hGED###) NO-ERROR.
&ENDIF

{g-hprowin.i}

/* Cryptage - D�cryptage */
FUNCTION CryptEmpl RETURNS CHAR (INPUT chem AS char) IN G-HGED.
FUNCTION DeCryptEmpl RETURNS CHAR (INPUT chem AS char) IN G-HGED.

/* Param�trages GED */
function Raffraichir_Gedpar RETURNS LOG () IN G-HGED.
FUNCTION Obtenir_Emplacement_Corbeille RETURN CHAR () IN G-HGED.
FUNCTION Obtenir_Emplacement_Corbeille_New RETURN CHAR (INPUT pcle-class AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Classeur RETURN CHAR () IN G-HGED.
FUNCTION Obtenir_Emplacement_Classeur_New RETURN CHAR (INPUT pcle-class AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Moteur RETURN CHAR () IN G-HGED.
FUNCTION Obtenir_Emplacement_Moteur_New RETURN CHAR (INPUT pcle-class AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Pieces RETURN CHAR () IN G-HGED.
FUNCTION Obtenir_Emplacement_Pieces_New RETURN CHAR (INPUT pcle-class AS CHAR) IN G-HGED.
FUNCTION Obtenir_Gestion_Doublon RETURN LOG(cleclasseur AS CHAR) IN G-HGED.
FUNCTION Obtenir_Gestion_Log RETURN LOG(cleclasseur AS CHAR) IN G-HGED.
FUNCTION Obtenir_Gestion_Version RETURN LOG(cleclasseur AS CHAR) IN G-HGED.
FUNCTION Obtenir_Droit_General RETURN LOG() IN G-HGED.
FUNCTION Obtenir_Type_Droit RETURN LOG() IN G-HGED.
FUNCTION Obtenir_gestion_corbeille RETURN LOG(INPUT cleclasseur AS CHAR) IN G-HGED.
FUNCTION Obtenir_gestion_suppression RETURN INT() IN G-HGED.
FUNCTION Obtenir_gestion_niveau RETURN LOG() IN G-HGED.
FUNCTION Obtenir_Arbo RETURN CHAR (INPUT codemp AS CHAR,
                                   OUTPUT pcleclass AS CHAR, OUTPUT pclechap AS CHAR, OUTPUT pclesschap AS CHAR, OUTPUT pcleniv1 AS CHAR, OUTPUT pcleniv2 AS CHAR, OUTPUT pcleniv3 AS CHAR,
                                   OUTPUT plibclass AS CHAR, OUTPUT plibchap AS CHAR, OUTPUT plibsschap AS CHAR, OUTPUT plibniv1 AS CHAR, OUTPUT plibniv2 AS CHAR, OUTPUT plibniv3 AS CHAR) IN G-HGED.
FUNCTION Obtenir_gestion_pleintexte RETURN LOG(INPUT cle_class AS CHAR) IN G-HGED.

/* Crit�res */
FUNCTION Obtenir_Valeur_Critere_Tri RETURN LOG (INPUT pcle_class AS CHAR, INPUT pnomcrit AS CHAR, INPUT pvalcrit AS CHAR, OUTPUT valeur AS CHAR) IN G-HGED.
FUNCTION Obtenir_Numero_Critere RETURN INT (INPUT pcle_class AS CHAR, INPUT pnomcrit AS CHAR) IN G-HGED.
FUNCTION Obtenir_dernier_numcrit RETURN INT (INPUT pcleclass AS CHAR,INPUT pinfo AS LOG) IN G-HGED.
FUNCTION Existe_Critere RETURN LOG (INPUT cleclass AS CHAR,INPUT pcritere AS CHAR) IN G-HGED.
FUNCTION Existe_Nom_Critere RETURN LOG (INPUT cleclass AS CHAR,INPUT nom_critere AS CHAR) IN G-HGED.
FUNCTION Obtenir_Critere RETURN LOG (INPUT cleclass AS CHAR,INPUT pcritere AS CHAR) IN G-HGED.
FUNCTION Obtenir_NbCritere RETURN INT (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Obtenir_ListeCritere RETURN CHAR (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Obtenir_PremierCritere RETURN LOG  (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Obtenir_PremierCritere_Emplacement RETURN LOG  (INPUT cleclass AS CHAR,INPUT cleemp AS CHAR, INPUT pinformatif AS LOG) IN G-HGED. /*$A74379*/
FUNCTION Obtenir_DernierCritere RETURN LOG  (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Obtenir_CritereSuivant RETURN LOG (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Obtenir_CritereSuivant_Emplacement RETURN LOG (INPUT cleclass AS CHAR,INPUT cleemp AS CHAR, INPUT pinformatif AS LOG) IN G-HGED. /*$A74379*/
FUNCTION Obtenir_Critere_MinMax RETURN LOG (INPUT cleclass AS CHAR,INPUT clecritere AS CHAR,
                                            OUTPUT gesmin AS LOG, OUTPUT valmin AS CHAR,
                                            OUTPUT gesmax AS LOG, OUTPUT valmax AS CHAR,
                                            OUTPUT gesdef AS LOG, OUTPUT valdef AS CHAR) IN G-HGED.
FUNCTION Obtenir_Critere_Std RETURN LOG (INPUT cleclass AS CHAR,INPUT clecritere AS CHAR,
                                         OUTPUT cle AS CHAR, OUTPUT ordre AS INT,OUTPUT lib AS CHAR)  IN G-HGED.
FUNCTION Obtenir_Critere_Data RETURN LOG (INPUT cleclass AS CHAR,INPUT clecritere AS CHAR,
                                          OUTPUT ptype AS CHAR, OUTPUT pformat AS DEC,OUTPUT poblig AS log) IN G-HGED.
FUNCTION Obtenir_Critere_ValFormat RETURN CHAR (INPUT cleclass AS CHAR,INPUT clecritere AS CHAR,
                                                INPUT valeur AS char, INPUT formatval AS LOG, INPUT formatlib AS LOG) IN G-HGED.
FUNCTION Obtenir_Critere_Intitule RETURN CHAR (INPUT cleclass AS CHAR,INPUT clecritere AS CHAR,
                                          INPUT valeur AS char) IN G-HGED.

FUNCTION Est_Critere_Multi_Valeurs RETURN LOG (INPUT pcle_class AS CHAR, INPUT pnomcrit AS CHAR)  IN G-HGED. /*$A75592*/

/* Crit�res liste */
FUNCTION Obtenir_NbCritereListe RETURN INT (INPUT pclass AS CHAR,INPUT pcrit AS CHAR) IN G-HGED.
FUNCTION Obtenir_PremierCritereListe RETURN LOG (INPUT pclass AS CHAR,INPUT pcrit AS CHAR, OUTPUT val_critere AS CHAR) IN G-HGED.
FUNCTION Obtenir_DernierCritereListe RETURN LOG (INPUT pclass AS CHAR,INPUT pcrit AS CHAR, OUTPUT val_critere AS CHAR) IN G-HGED.
FUNCTION Obtenir_CritereSuivantListe RETURN LOG (INPUT pclass AS CHAR,INPUT pcrit AS CHAR, OUTPUT val_critere AS CHAR) IN G-HGED.
FUNCTION Ajouter_Critere_Liste RETURNS LOG (INPUT pclas AS CHAR, INPUT pcrit AS char, INPUT pvaleur AS CHAR,OUTPUT motif AS CHAR) IN G-HGED.

/* cr�er un crit�re */
FUNCTION Creer_Critere RETURN LOG (INPUT cleclass AS CHAR,INPUT critinfo AS LOG,INPUT clecritere AS CHAR,INPUT pval_def as CHARACTER,
                                   INPUT pobligatoire as LOGICAL, INPUT ptyp_donnee as CHARACTER, INPUT pval_min as CHARACTER,
                                   INPUT pval_max as CHARACTER, INPUT pnom_crit as CHARACTER, INPUT pcrit_format as DECIMAL,
                                   INPUT pcarac_int as CHARACTER, INPUT pges_min as LOGICAL, INPUT prequete as CHARACTER,
                                   INPUT preq_BD as CHARACTER, INPUT preq_table as CHARACTER, INPUT preq_chpcod as CHARACTER,
                                   INPUT preq_chplib as CHARACTER, INPUT preq_Prog as CHARACTER, INPUT pges_max as LOGICAL,
                                   INPUT pges_valdef as LOGICAL, INPUT preq_Spe as CHARACTER, INPUT pdes_crit as CHARACTER,
                                   INPUT preq_Prog2 as CHARACTER, INPUT paut_ajl as LOGICAL, INPUT preq_prog3 as CHARACTER,
                                   INPUT pzon_lib as CHARACTER, INPUT pvaleurs AS CHARACTER) IN G-HGED.

/* Classeur */
FUNCTION Obtenir_NbClasseur RETURN INT () IN G-HGED.
FUNCTION estPereDe RETURN LOG (INPUT clepere AS CHAR,INPUT clefils AS CHAR) IN G-HGED.
function Obtenir_PremierClasseur RETURN LOG () IN G-HGED.
function Obtenir_DernierClasseur RETURN LOG () IN G-HGED.
FUNCTION Obtenir_ClasseurSuivant RETURN LOG () IN G-HGED.
FUNCTION Obtenir_Classeur RETURN LOG  (INPUT cleclass AS CHAR)  IN G-HGED.
FUNCTION Obtenir_Classeur_Cle RETURN CHAR (INPUT id AS ROWID)  IN G-HGED.
FUNCTION Obtenir_Classeur_Row RETURN ROWID (INPUT cleclass AS CHAR) IN G-HGED.
FUNCTION Obtenir_Classeur_Std RETURN LOG (INPUT codclass AS CHAR, OUTPUT cleclass AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
FUNCTION Obtenir_Classeur_Logiciel RETURN LOG (INPUT cleclass AS CHAR,OUTPUT droitlog AS LOG, OUTPUT log1 AS LOG,
                                               OUTPUT log2 AS LOG,OUTPUT log3 AS LOG, OUTPUT log4 AS LOG,OUTPUT log5 AS LOG) IN G-HGED.
FUNCTION Obtenir_Classeur_Type RETURN LOG (INPUT codclass AS CHAR, OUTPUT ltype AS INT,OUTPUT proprio AS CHAR, OUTPUT domaine AS CHAR,OUTPUT logiciel AS CHAR) IN G-HGED.
FUNCTION Creer_classeur RETURN ROWID (INPUT Desi AS CHAR, INPUT commentaire AS CHAR,INPUT logiciel1 AS LOG,INPUT logiciel2 AS LOG,INPUT logiciel3 AS log,INPUT logiciel4 AS LOG,INPUT logiciel5 AS LOG) IN G-HGED.
FUNCTION Creer_Classeur_Systeme RETURN ROWID (INPUT desi AS CHAR,INPUT proprio AS CHAR, INPUT ldomaine AS CHAR,INPUT llogiciel AS CHAR) IN G-HGED.
FUNCTION Creer_Classeur_Prive RETURN ROWID (INPUT desi AS CHAR,INPUT proprio AS CHAR, INPUT ldomaine AS CHAR,INPUT llogiciel AS CHAR) IN G-HGED.
FUNCTION Modifier_Classeur RETURN ROWID (cle AS CHAR, Desi AS CHAR, commentaire AS CHAR,logiciel1 AS LOG,logiciel2 AS LOG,logiciel3 AS log,logiciel4 AS LOG,logiciel5 AS LOG) IN G-HGED.
FUNCTION Rechercher_Classeur_Prive RETURN CHAR (INPUT proprio AS CHAR, INPUT ldomaine AS CHAR,INPUT llogiciel AS CHAR) IN G-HGED.
FUNCTION Rechercher_Classeur_Systeme RETURN CHAR (INPUT proprio AS CHAR, INPUT ldomaine AS CHAR,INPUT llogiciel AS CHAR) IN G-HGED.
FUNCTION Deplacer_Classeur RETURN LOG (INPUT pcle-class AS CHAR, INPUT pold-chemin AS CHAR,INPUT pnew-chemin AS CHAR) IN G-HGED.

/* Gestion des Documents */
FUNCTION Obtenir_Emplacement_Document RETURN CHAR (cle AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Stock RETURN CHAR (cle AS CHAR) IN G-HGED.
FUNCTION Obtenir_NbDocument RETURN INT (INPUT ttniveau AS log, INPUT cle AS CHAR, INPUT niv AS INT) IN G-HGED.
FUNCTION Obtenir_NbDocumentReq RETURN INT (INPUT ttniveau AS log, INPUT cle AS CHAR, INPUT niv AS INT, INPUT preq AS CHAR) IN G-HGED.
FUNCTION Obtenir_PremierDocument RETURN LOG (INPUT ttniveau AS log, INPUT pere AS CHAR, INPUT nivpere AS INT) IN G-HGED.
FUNCTION Obtenir_DernierDocument RETURN LOG (INPUT ttniveau AS log, INPUT pere AS CHAR, INPUT nivpere AS INT) IN G-HGED.
FUNCTION Obtenir_DocumentSuivant RETURN LOG (INPUT ttniveau AS log, INPUT pere AS CHAR, INPUT nivpere AS INT) IN G-HGED.
FUNCTION Obtenir_Document RETURN LOG  (INPUT pcle AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Std RETURN LOG (INPUT coddcoc AS CHAR, OUTPUT cledoc AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
FUNCTION Obtenir_Chapitre_Par_Libelle RETURN LOG (INPUT pcle_pere AS CHAR, INPUT plibelle AS CHAR, OUTPUT pcle AS CHAR, OUTPUT ptyp AS CHAR) IN G-HGED.
FUNCTION archivage_edition RETURN LOG (INPUT pedition AS CHAR, INPUT pmessage AS LOG, OUTPUT classeur AS CHAR) IN G-HGED. /* $2222 */
FUNCTION archivage_edition_schap RETURN LOG (INPUT pedition AS CHAR, INPUT pmessage AS LOG, OUTPUT pclasseur AS CHAR, OUTPUT pchapitre AS CHAR, OUTPUT psouschapitre AS CHAR) IN G-HGED. /*$AP*/
FUNCTION Est_Dans_La_Ged RETURN LOG (INPUT pchemin AS CHAR, OUTPUT pged AS CHAR) IN G-HGED.

/* Niveau */
FUNCTION Creer_Niveau RETURN ROWID (INPUT pniv AS char, INPUT pcle_pere AS CHAR, INPUT libelle AS CHAR, INPUT texte AS CHAR) IN G-HGED.
FUNCTION modifier_niveau RETURN ROWID (INPUT cle AS CHAR, INPUT lib AS CHAR,INPUT texte AS CHAR) IN G-HGED.
FUNCTION Supprimer_Niveau RETURN LOG (cle AS CHAR) IN G-HGED.
FUNCTION Obtenir_Nb_Document_Niveau RETURN LOG (INPUT cle_niv AS CHAR, INPUT-O i AS INT) IN G-HGED.
FUNCTION Obtenir_Classement_Niveau RETURN LOG (INPUT pcle_niv AS CHAR, OUTPUT pclass AS CHAR, OUTPUT pchap AS CHAR, OUTPUT psschap AS CHAR) IN G-HGED.
FUNCTION Obtenir_Niveau_Row RETURN ROWID (INPUT cle_niv AS CHAR) IN G-HGED.
FUNCTION Obtenir_Libelle_Niveau RETURN CHAR (INPUT cle_niv AS CHAR)  IN G-HGED.
FUNCTION Obtenir_Classement_General RETURN LOG (INPUT cod AS CHAR, OUTPUT cod_class AS CHAR, OUTPUT cod_chap AS CHAR, OUTPUT cod_sschap AS CHAR,OUTPUT cod_niv AS CHAR) IN g-hged.

/* Informations Documents */
FUNCTION Existe_Document RETURN LOG (INPUT pnom AS CHAR, INPUT extension AS CHAR, INPUT cle_class AS CHAR, 
                                     INPUT cle_chap AS CHAR, INPUT cle_sschap AS CHAR, INPUT Cle_niv AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Classement RETURN LOG (INPUT cledoc AS CHAR, OUTPUT cod_class AS CHAR,
                                                 OUTPUT cod_chap AS CHAR, OUTPUT cod_sschap AS CHAR,
                                                 OUTPUT cod_niv1 AS CHAR, OUTPUT cod_niv2 AS CHAR, OUTPUT cod_niv3 AS CHAR ) IN G-HGED.
FUNCTION Obtenir_Document_Chemin RETURN LOG (INPUT cledoc AS CHAR, OUTPUT Chem_ori AS CHAR, 
                                             OUTPUT Chem_ged AS CHAR, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Identite RETURN LOG (INPUT cledoc AS CHAR, OUTPUT pintidoc AS CHAR, OUTPUT pnom AS CHAR, OUTPUT pext AS CHAR,
                                               OUTPUT presume AS CHAR, OUTPUT pdocint AS LOG, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Info_Systeme RETURN LOG (INPUT cledoc AS CHAR, OUTPUT ptaille AS DEC, OUTPUT pdatcrt AS DATE, OUTPUT phcrt AS CHAR,
                                                   OUTPUT pdatmod AS DATE, OUTPUT phmod AS CHAR, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Archivage RETURN LOG (INPUT cledoc AS CHAR, OUTPUT userarch AS CHAR, OUTPUT pdatarch AS DATE,  OUTPUT pharch AS CHAR,
                                                OUTPUT userarchmod AS CHAR, OUTPUT pdatmodarch AS DATE, OUTPUT phmodarch AS CHAR, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Rap RETURN LOG (INPUT cledoc AS CHAR, OUTPUT rap AS LOG, OUTPUT usrrap AS CHAR,
                                          OUTPUT datrap AS DATE, OUTPUT hrrap AS CHAR, OUTPUT chemrap AS char,OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_CleDoc RETURN LOG (INPUT pcleclass AS CHAR, INPUT pclechap AS CHAR, INPUT pclesschap AS CHAR, INPUT pintidoc AS CHAR, 
                                             OUTPUT cledoc AS CHAR) IN G-HGED.
FUNCTION Definir_Document_Un_Critere RETURN LOG (INPUT cledoc AS CHAR, INPUT lcrit AS CHAR, INPUT newval AS CHAR) IN G-HGED.
FUNCTION Definir_Document_CritereS RETURN LOG (INPUT cledoc AS CHAR, INPUT lcritere AS CHAR, INPUT lcritereinfo AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Codcritere RETURN LOG (INPUT cledoc AS CHAR,OUTPUT pcod_pro AS INT, OUTPUT pcod_cli AS INT, OUTPUT pcod_fou AS INT,
                                                 OUTPUT pcod_aut AS CHAR, OUTPUT ptyp_aut AS CHAR, OUTPUT pcod_aff AS CHAR, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Evenement RETURN LOG (INPUT cledoc AS CHAR, OUTPUT num_evenement AS INT, OUTPUT pdata AS CHAR) IN G-HGED.
FUNCTION Obtenir_Valeur_Critere RETURN CHAR (INPUT cledoc AS CHAR, INPUT cod_crit AS CHAR) IN G-HGED.
FUNCTION Obtenir_Valeur_Critere_info RETURN CHAR (INPUT cledoc AS CHAR, INPUT cod_crit AS CHAR) IN G-HGED.
FUNCTION Obtenir_Document_Critere RETURN LOG (INPUT cledoc AS CHAR, OUTPUT lcrit AS CHAR) IN G-HGED.
FUNCTION recherche_document_par_critere RETURN LOG (INPUT pedition AS CHAR, INPUT pnom-crit AS CHAR, INPUT pval-crit AS CHAR, OUTPUT pcle_doc AS CHAR) IN G-HGED. /* $2222 */
FUNCTION recherche_document_par_critere_schap RETURN LOG (INPUT pedition AS CHAR, INPUT pnom-crit AS CHAR, INPUT pval-crit AS CHAR, OUTPUT pcle_doc AS CHAR) IN G-HGED. /*$AP*/
FUNCTION recherche_x_documents_par_criteres_schap RETURN LOG (input pclasseur as char, input pchapitre as char, input psschapitre as char, INPUT pnom-crit AS CHAR, INPUT pval-crit AS CHAR, input pproc as char, input ppere as handle) IN G-HGED. /*$AP*/

/* Op�ration sur les documents */
FUNCTION Creer_documentAPartirDe RETURN LOG (INPUT pmod_doc AS char, INPUT pinti_doc AS CHAR, INPUT emplacement_ori AS CHAR,INPUT b_sup AS LOG, OUTPUT cle_doc AS CHAR, OUTPUT msgerreur AS CHAR) IN G-HGED.
/*FUNCTION Creer_document RETURN LOG(INPUT pinti_doc AS CHAR, INPUT cle_class AS CHAR, INPUT cle_chap AS CHAR,INPUT cle_sschap AS CHAR,INPUT pcle_niv AS CHAR,
 *                                    INPUT Emplacement_ori AS CHAR, INPUT Resume AS CHAR, INPUT lcritere AS CHAR,
 *                                    INPUT lcritereinfo AS CHAR , INPUT Doc_interne AS LOG , INPUT ptrrtf_interne AS COM-HANDLE ,
 *                                    INPUT phraseindex AS CHAR, INPUT b_sup AS LOG, OUTPUT cle_doc AS CHAR, OUTPUT msgerreur AS CHAR) IN G-HGED.*/
/*FUNCTION Modifier_document RETURN LOG(INPUT cle_doc AS CHAR, INPUT pinti_doc AS CHAR, INPUT Resume AS CHAR, INPUT lcritere AS CHAR, 
 *                                       INPUT lcritereinfo AS CHAR,INPUT Doc_interne AS LOG, INPUT ptrrtf_interne AS COM-HANDLE,INPUT phraseindex AS CHAR, INPUT phisto AS LOG, INPUT presumehisto AS CHAR) IN G-HGED.*/

/*FUNCTION Rapatrier_Document RETURN LOG (INPUT cle AS CHAR, INPUT dest AS CHAR) IN G-HGED.*/
/*FUNCTION Remettre_document RETURN LOG (INPUT localisation AS CHAR, INPUT cle AS CHAR, INPUT phisto AS LOG, INPUT presumehisto AS CHAR) IN G-HGED.*/
/*FUNCTION Deplacer_document RETURN LOG (INPUT coddoc AS CHAR, INPUT clas_dest AS CHAR, INPUT chap_dest AS CHAR,
 *                                        INPUT schap_dest AS CHAR, INPUT pcle_niv AS CHAR,
 *                                        OUTPUT msg_erreur AS CHAR, OUTPUT retour AS LOG) IN G-HGED.
 * */
/* Versionning */
FUNCTION Obtenir_Emplacement_Histo RETURN CHAR () IN G-HGED.
FUNCTION Obtenir_Emplacement_Histo_New RETURN CHAR (INPUT pcle-class AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Version RETURN CHAR (INPUT coddoc AS CHAR, INPUT nuvers AS INT) IN G-HGED.
FUNCTION Obtenir_NbVersion_Histo RETURN INT (INPUT cleclasseur AS CHAR) IN G-HGED.
FUNCTION Obtenir_Type_Histo RETURN CHAR (INPUT cleclasseur AS CHAR) IN G-HGED.
FUNCTION Purger_version RETURN LOG (INPUT coddoc AS CHAR) IN G-HGED.
FUNCTION Documents_identiques RETURN LOG (INPUT pdoc1 AS CHAR, INPUT pdoc2 AS CHAR) IN G-HGED.
/*FUNCTION Remettre_Version RETURN LOG (INPUT coddoc AS CHAR, INPUT nuvers AS INT) IN G-HGED.*/

/* Documents associ�s */
FUNCTION Existe_docassoc RETURN LOGICAL (INPUT cle AS CHAR) IN G-HGED.
FUNCTION Ajouter_docassoc RETURN LOGICAL (INPUT cle AS CHAR, INPUT cledocassoc AS CHAR) IN G-HGED.
FUNCTION Supprimer_docassoc RETURN LOGICAL (INPUT cle AS CHAR, INPUT cledocassoc AS CHAR) IN G-HGED.

/* Informations Suppl�mentaires */
FUNCTION Existe_InfoSup RETURN LOGICAL (INPUT cle AS CHAR) IN G-HGED.

/* Pi�ces jointes */
FUNCTION Existe_Piecejointe RETURN LOGICAL (INPUT cle AS CHAR) IN G-HGED.
FUNCTION Obtenir_Emplacement_Piecejointe RETURN CHAR (coddoc AS CHAR, codpj AS CHAR) IN G-HGED.
/*FUNCTION Ajouter_Piecejointe RETURN LOGICAL (INPUT cledoc AS CHAR,INPUT emplacement_ori AS CHAR, INPUT nom_piece AS CHAR, INPUT res_piece AS CHAR, OUTPUT msg_erreur AS CHAR ) IN G-HGED.*/
FUNCTION Supprimer_PieceJointe RETURN LOGICAL (INPUT cle AS CHAR, INPUT codpj AS CHAR) IN G-HGED.
FUNCTION Modifier_pieceJointe RETURN LOGICAL (INPUT cle AS CHAR, INPUT codpj AS CHAR, INPUT nom_piece AS CHAR, INPUT res_piece AS CHAR,INPUT web_principale AS CHAR) IN G-HGED.

/* Gestion de la Corbeille */
FUNCTION Restaurer_Corbeille RETURN LOG (INPUT cle AS CHAR) IN G-HGED.
FUNCTION Supprimer_Corbeille RETURN LOG (INPUT cle AS CHAR) IN G-HGED.
FUNCTION Vider_Corbeille RETURN LOG () IN G-HGED.
FUNCTION Supprimer_Tout_Corbeille_Utilisateur RETURN LOG (INPUT luser AS CHAR) IN G-HGED.
FUNCTION Restaurer_Tout_Corbeille RETURN LOG () IN G-HGED.
FUNCTION Restaurer_Tout_Corbeille_Utilisateur RETURN LOG (INPUT luser AS CHAR) IN G-HGED.
FUNCTION Purger_Corbeille RETURN INT () IN G-HGED.

/* Chapitre */
FUNCTION Obtenir_NbChapitre RETURN INT (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_PremierChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_DernierChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_ChapitreSuivant RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_Chapitre RETURN LOG (INPUT clepere AS CHAR,INPUT cleelt AS CHAR) IN G-HGED.
FUNCTION Obtenir_Chapitre_Row RETURN ROWID (INPUT cleelt AS CHAR) IN G-HGED.
FUNCTION Obtenir_Chapitre_Cle RETURN CHAR (INPUT id AS ROWID) IN G-HGED.
FUNCTION Obtenir_Chapitre_Std RETURN LOG (INPUT clepere AS CHAR,INPUT codchap AS CHAR, OUTPUT clechap AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
FUNCTION Creer_Chapitre RETURN ROWID(INPUT pcle_pere AS CHAR, INPUT libelle AS CHAR,INPUT texte AS CHAR) IN G-HGED.
FUNCTION modifier_chapitre RETURN ROWID (INPUT cle AS CHAR, INPUT lib AS CHAR,INPUT texte AS CHAR) IN G-HGED.
FUNCTION Obtenir_classeur_chapitre RETURN CHAR (INPUT pcle AS CHAR) IN G-HGED.
/*$A74433...*/
FUNCTION Creer_Chapitre_Par_Annee RETURN LOG (INPUT pDate AS DATE,INPUT-O pClePere AS CHAR, INPUT-O pcleclass AS CHAR, INPUT-O pclechap AS CHAR, INPUT-O pclesschap AS CHAR,
                                              INPUT-O pcleniv AS CHAR) IN G-HGED.
FUNCTION Creer_Chapitre_Par_Mois RETURN LOG (INPUT pDate AS DATE,INPUT-O pClePere AS CHAR, INPUT-O pcleclass AS CHAR, INPUT-O pclechap AS CHAR, INPUT-O pclesschap AS CHAR,
                                              INPUT-O pcleniv AS CHAR) IN G-HGED.
/*...$A74433*/

/* Sous-Chapitre */
FUNCTION Obtenir_NbSSChapitre RETURN INT (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_PremierSSChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_DernierSSChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_SSChapitreSuivant RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Obtenir_SSChapitre RETURN LOG  (INPUT clepere AS CHAR,INPUT cleelt AS CHAR) IN G-HGED.
FUNCTION Obtenir_SSChapitre_Std RETURN LOG (INPUT clepere AS CHAR, INPUT codsschap AS CHAR, OUTPUT clesschap AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
FUNCTION Creer_SSChapitre RETURN ROWID(INPUT pcle_pere AS CHAR, INPUT libelle AS CHAR, INPUT texte AS CHAR) IN G-HGED.
FUNCTION obtenir_CodeClasseur_Document RETURN CHAR (INPUT cledoc AS CHAR) IN G-HGED. /*$A75592*/
FUNCTION Modifier_SSchapitre RETURN ROWID (INPUT cle AS CHAR, INPUT lib AS CHAR,INPUT texte AS CHAR) IN G-HGED.

/* Suppression */
/*FUNCTION Supprimer_Tous_Documents RETURN LOG () IN G-HGED.
 * FUNCTION supprimer_sschapitre RETURN LOG (cle AS CHAR) IN G-HGED.
 * FUNCTION supprimer_chapitre RETURN LOG (cle AS CHAR) IN G-HGED.
 * function Supprimer_Classeur RETURN LOG ( cle AS CHAR ) IN G-HGED.*/

/* Gestion Administration */
FUNCTION Obtenir_Droit_Admin RETURN LOGICAL (INPUT util AS CHAR) IN G-HGED.  
FUNCTION Obtenir_Droit_Admin2 RETURN LOGICAL (INPUT util AS CHAR) IN G-HGED.

/* Gestion des droits */
FUNCTION Existe_droit_document RETURN LOG (INPUT cledoc AS CHAR) IN G-HGED.  
FUNCTION Obtenir_droit_document RETURN INT (INPUT util AS CHAR, INPUT cleclas AS CHAR, INPUT clechap AS CHAR,
                                            INPUT clesschap AS CHAR, INPUT cledoc AS CHAR, OUTPUT herite AS LOGICAL) IN G-HGED.
FUNCTION Obtenir_droit_stockage RETURN INT (INPUT util AS CHAR, INPUT cle AS CHAR, INPUT niv AS INT, OUTPUT herite AS LOGICAL) IN G-HGED. /* 1 -> classeur, 2 -> chapitre, 3 -> ss-chapitre */
FUNCTION Obtenir_droit_parent RETURN INT (INPUT cle AS CHAR, INPUT util AS CHAR, INPUT niv AS INT) IN G-HGED.
FUNCTION Obtenir_libelle_droit RETURN CHAR (INPUT cod_droit AS INTEGER) IN G-HGED.
FUNCTION Creer_droit RETURN ROWID (INPUT cle AS CHAR, INPUT util AS CHAR, INPUT cod_droit AS INT, INPUT niv AS INT) IN G-HGED.
FUNCTION Copier_Coller_Droit RETURN LOG (INPUT util_src AS CHAR, INPUT util_dest AS CHAR) IN G-HGED.
FUNCTION Supprimer_Droit_util RETURN LOG (INPUT util AS CHAR) IN G-HGED.
FUNCTION Supprimer_Droit_Doc RETURN LOG (INPUT util AS CHAR, INPUT cod_doc AS CHAR) IN G-HGED.
FUNCTION Supprimer_Droit RETURN LOG (INPUT util AS CHAR, INPUT cle AS CHAR, INPUT niv AS INT) IN G-HGED.

FUNCTION Autoriser_Affichage RETURNS LOG (INPUT toutparcourir AS log, INPUT niv AS INT /* 1 -> classeur, 2 -> chapitre, 3 -> ss-chapitre, 4 -> fichier */
                                          ,INPUT lcle AS CHAR, INPUT codusr AS CHAR) IN G-HGED.

/* Parent */
FUNCTION Obtenir_parent RETURN LOGICAL (INPUT cle AS CHAR, INPUT niv AS INT, OUTPUT cle_pere AS CHAR, OUTPUT niv_pere AS INT) IN G-HGED.

/* Index */
FUNCTION Obtenir_PremierIndex RETURNS LOG  () IN G-HGED.
FUNCTION Obtenir_IndexSuivant RETURNS LOG  () IN G-HGED.
FUNCTION Obtenir_Index_Std RETURNS LOG  (OUTPUT cle AS CHAR, OUTPUT val AS CHAR) IN G-HGED.

/* Gestion des logs */
FUNCTION Referencer_Log RETURNS LOG (INPUT lcle AS CHAR, INPUT ltyp AS CHAR, INPUT lact AS char, INPUT lmotif AS CHAR) IN G-HGED.
/* Codes utilis�s pour ltyp : CL, CH, SC, DO, CO, AU
   Codes utilis�s pour lact : Supprimer, Creer, Rapatrier, Remettre, Restaurer, Vider, Purger, Modifier
 */ 

/* Divers */
FUNCTION Valeur_octet RETURN CHAR (INPUT nb AS DEC) IN G-HGED.
FUNCTION verifier_valeur_format RETURN LOG (typdata AS char, val AS CHAR, ff AS DEC,OUTPUT pnewval AS CHAR)  IN G-HGED.
Function Decouper_NomFichier RETURNS LOG (INPUT pparam AS CHAR,OUTPUT prep AS CHAR,OUTPUT pnom AS CHAR,
                              OUTPUT pext AS CHAR, OUTPUT pnomext AS CHAR) IN G-HGED.

/* Verrou */
function Verrouiller_Enregistrement RETURNS LOG (INPUT cledoc AS CHAR,INPUT motif AS CHAR,OUTPUT txtverr AS CHAR) IN G-HGED.
function DeVerrouiller_Enregistrement RETURNS LOG (INPUT cledoc AS CHAR) IN G-HGED.
function Verrouiller_Enregistrement_Workflow RETURNS LOG (INPUT cledoc AS CHAR,INPUT motif AS CHAR,OUTPUT txtverr AS CHAR) IN G-HGED.
function DeVerrouiller_Enregistrement_Workflow RETURNS LOG (INPUT cledoc AS CHAR) IN G-HGED.

/* Favoris : Classeur, Chapitre et sous-chapitre */
FUNCTION Existe_favori_User RETURN LOG (INPUT cledoc AS CHAR, INPUT puser AS CHAR) IN G-HGED.
FUNCTION Existe_Rep_favori RETURN LOG (INPUT luser AS CHAR) IN G-HGED.
FUNCTION Ajouter_Rep_Favori RETURN LOG (INPUT cle AS CHAR, INPUT niv AS INT, INPUT luser AS CHAR, INPUT recursif AS LOG ) IN G-HGED.
FUNCTION Supprimer_Rep_Favori RETURN LOG (INPUT cle AS CHAR, INPUT niv AS INT, INPUT luser AS CHAR) IN G-HGED.
FUNCTION Obtenir_Rep_Favoris RETURN LOG (INPUT cle AS CHAR, INPUT luser AS CHAR) IN G-HGED.
FUNCTION Obtenir_Rep_favoris_fils RETURN LOG (INPUT cle AS CHAR, INPUT luser AS CHAR) IN G-HGED.
FUNCTION Obtenir_Utilisateur_Droit RETURNS CHAR IN G-HGED.
FUNCTION Obtenir_Utilisateur_Vrai RETURNS CHAR IN G-HGED.

FUNCTION Dupliquer_Classeur RETURN LOG (INPUT cleclass_ori AS CHAR, INPUT cleclass_dest AS CHAR, INPUT PStruct AS LOG,
                                        INPUT PCrit AS LOG, INPUT PDroit AS LOG ) IN G-HGED.

/* ******************************* WEB ****************************** */

/* Classeur Web */
FUNCTION Web_Obtenir_PremierClasseur   RETURN LOG (INPUT portail AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_ClasseurSuivant   RETURN LOG (INPUT portail AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_DernierClasseur   RETURN LOG (INPUT portail AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_Info_Classeur     RETURN LOG (INPUT codclass AS CHAR, OUTPUT cleclass AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
/* Chapitre Web */
FUNCTION Web_Obtenir_PremierChapitre   RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_ChapitreSuivant   RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_DernierChapitre   RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_Chapitre          RETURN LOG (INPUT clepere AS CHAR,INPUT cleelt AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_Info_Chapitre     RETURN LOG (INPUT clepere AS CHAR, INPUT codchap AS CHAR, OUTPUT clechap AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
/* Sous-Chapitre Web */
FUNCTION Web_Obtenir_PremierSSChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_DernierSSChapitre RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_SSChapitreSuivant RETURN LOG (INPUT clepere AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_SSChapitre        RETURN LOG (INPUT clepere AS CHAR,INPUT cleelt AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_Info_SSChapitre   RETURN LOG (INPUT clepere AS CHAR, INPUT codsschap AS CHAR, OUTPUT clesschap AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
/* Niveaux Web */
FUNCTION Web_Obtenir_PremierNiveau     RETURN LOG (INPUT clepere AS CHAR, INPUT pniveau AS INT) IN G-HGED.
FUNCTION Web_Obtenir_NiveauSuivant     RETURN LOG (INPUT clepere AS CHAR, INPUT pniveau AS INT) IN G-HGED.
FUNCTION Web_Obtenir_DernierNiveau     RETURN LOG (INPUT clepere AS CHAR, INPUT pniveau AS INT) IN G-HGED.
FUNCTION Web_Obtenir_Niveau            RETURN LOG  (INPUT clepere AS CHAR,INPUT cleelt AS CHAR, INPUT pniveau AS INT) IN G-HGED.
FUNCTION Web_Obtenir_Info_Niveau       RETURN LOG (INPUT clepere AS CHAR, INPUT codssnivo AS CHAR, INPUT pniveau AS INT, OUTPUT clenivo AS CHAR, OUTPUT lib AS CHAR) IN G-HGED.
/* Crit�res */
FUNCTION Web_Obtenir_PremierCritere    RETURN LOG  (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Web_Obtenir_CritereSuivant    RETURN LOG (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
FUNCTION Web_Obtenir_DernierCritere    RETURN LOG  (INPUT cleclass AS CHAR,INPUT pinformatif AS LOG) IN G-HGED.
/* Autres */
FUNCTION Web_EstCheminValide RETURN CHAR (INPUT pchemin AS CHAR, INPUT pportail AS CHAR) IN G-HGED.
FUNCTION Web_Obtenir_Arbo RETURN CHAR (INPUT codemp AS CHAR, INPUT pvallib AS CHAR,
                                   OUTPUT pcleclass AS CHAR, OUTPUT pclechap AS CHAR, OUTPUT pclesschap AS CHAR, OUTPUT pcleniv1 AS CHAR, OUTPUT pcleniv2 AS CHAR, OUTPUT pcleniv3 AS CHAR,
                                   OUTPUT plibclass AS CHAR, OUTPUT plibchap AS CHAR, OUTPUT plibsschap AS CHAR, OUTPUT plibniv1 AS CHAR, OUTPUT plibniv2 AS CHAR, OUTPUT plibniv3 AS CHAR) IN G-HGED.

/* V�rifie si le document fait partie d'un classeur chapitre ou sous-chapitre qui est � publier sur le web */
FUNCTION Web_EstPublie RETURN LOG (INPUT cle_doc AS CHAR) IN G-HGED.
FUNCTION Web_EstPublie_Creation RETURN LOG (INPUT cle_class AS CHAR,INPUT cle_chap AS CHAR,INPUT cle_sschap AS CHAR) IN G-HGED.

/* V�rifie si le document appartient a une cat�gorie pour un site donn�*/
FUNCTION Web_AppartientCatego RETURN LOG (INPUT cle_doc AS CHAR, INPUT cod_site AS INT) IN G-HGED.

FUNCTION LienDocGedWeb RETURN CHAR (INPUT pcodsite AS INT, INPUT pcledoc AS CHAR) IN G-HGED.

/* *************************** FIN WEB ****************************** */


/* A VOIR */
/*
FUNCTION existe_fichier RETURN CHAR (pnom AS CHAR, adrbase AS CHAR) IN G-HGED.
FUNCTION get_decimal RETURN INT (chiffre AS DEC) IN G-HGED.
FUNCTION FormaterDonnee RETURNS CHAR (typ AS CHAR,longueur AS DEC) IN G-HGED.
*/

/**************************************************************************/
/**************************************************************************/
/**************************************************************************/
/**************************************************************************/
DEF VAR ged-user-droit AS CHAR no-undo.
DEF VAR g-user-vrai AS CHAR no-undo.

&IF opsys = "unix" &THEN /*$AP*/
function load-apiged returns log():
/**/
    if CONNECTED("GED") then run value("ged~/api_gd.r") persistent set g-hged.

    IF VALID-HANDLE (g-hged) THEN DO:
        ged-user-droit = Obtenir_Utilisateur_Droit ().
        g-user-vrai = Obtenir_Utilisateur_Vrai ().
    END.
end function.
function unload-apiged returns log():
/**/
    IF VALID-HANDLE (g-hged) THEN apply "close" to g-hged.
end function.
&ENDIF

&IF opsys <> "unix" &THEN /*$AP*/
IF NOT VALID-HANDLE (g-hged) AND CONNECTED ("GED") THEN DO:
    
    DEFINE VARIABLE PgmAPI AS CHARACTER  NO-UNDO.
    PgmAPI = SEARCH(getsessiondata("config!","rep_spe") + "/GED/api_gd.r").
    IF PgmAPI = ? OR PgmAPI = "" THEN PgmAPI = SEARCH(GetSessionData("Config!", "GED") + "/api_gd.r").

    /*ged\funcapi_gd.i*/
    &IF "{1}" = "PORTAIL" &THEN
         IF FileExists(PgmAPI) THEN tracesession("Lancement de la GED : " + PgmAPI).
         ELSE tracesession("Probl�me au lancement de la GED : Impossible d'acc�der aux objets de la GED (api_gd.r)"). 
    &ENDIF

    RUN VALUE(PgmAPI) PERSISTENT SET g-hged.
    SetSessionData ("GED","CLE":U,STRING(g-hged)).
END.
IF VALID-HANDLE (g-hged) THEN DO:
    ged-user-droit = Obtenir_Utilisateur_Droit ().
    g-user-vrai = Obtenir_Utilisateur_Vrai ().
END.
&ENDIF

&ENDIF
