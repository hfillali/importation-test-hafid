/*
$A35347 VR 17/08/10 fonction sur les entr�es de lot/immat
$A41281 OP 30/08/11 Gestion de la Liste � servir
$A41786 OP 07/11/11 Si r�ception Atelier, proposer de r�-�diter l'OR li� (cr�ation des fonctions getOptionMvtStk & setOptionMvtStk)
$A57303 CSA 28/12/12 Probl�me de modification d'emplacement d'un SSCC si d'autres SSCC contiennent le m�me lot
$A59721 GC  28/02/13 Transformation de stock
$A62496 CSA 31/05/13 Pouvoir visualiser et supprimer des transferts d'emplacement
A55537 HL 4.5 24/04/13 Passage de 3 � 6 U.L, Explications dans {6ul.i}, Api {Api-UL.i}
$A59689 OP  11/09/13 Ajout fonction Creat-All-tt-sscclot
$A64426 ABA 04/10/13 Ajout fonction Creat-All-TempEmp
*/


DEFINE VARIABLE HndGco-MvtStk AS HANDLE NO-UNDO.

FUNCTION ENT-INITIALISATION RETURN LOGICAL (INPUT p%rowidTT AS ROWID) IN HndGco-MvtStk.
FUNCTION SOR-INITIALISATION RETURN LOGICAL (INPUT p%rowidTT AS ROWID) IN HndGco-MvtStk.
FUNCTION TRS-INITIALISATION RETURN LOGICAL in HndGco-MvtStk.
FUNCTION ENT-INFOSFONCTION RETURN LOGICAL (INPUT nomptable AS char, INPUT padr AS ROWID, INPUT plibre AS CHAR) in HndGco-MvtStk. /*$A35347*/
FUNCTION MAJ-PRIX-LOTIMM RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienimm AS INT,
                                         INPUT p%pxach AS DEC, INPUT p%pmp AS DEC,  INPUT p%pxunit AS DEC, INPUT p%pxrvt AS DEC) IN HndGco-MvtStk.

FUNCTION SOR-LOT-PRECONS-GP RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%cod_pro  AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR,
                                            INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR,
                                            INPUT p%depot  AS INT, INPUT p%lienlot AS INT, INPUT p%datmvt AS DATE,
                                            INPUT p%nompro AS CHAR, INPUT p%typmvt AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%nomcli AS CHAR, 
                                            INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC, 
                                            INPUT p%pxach  AS DEC, INPUT p%pmp AS DEC, INPUT p%ppuv AS DEC, INPUT p%pxunit AS DEC, INPUT p%pxrvt AS DEC,
                                            INPUT p%mvtprev AS LOG, OUTPUT p%Tot-valeur-achat AS DEC) IN HndGco-MvtStk.

FUNCTION SOR-IMM-PRECONS-GP RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%cod_pro  AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR,
                                            INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR,
                                            INPUT p%depot  AS INT, INPUT p%lienimm AS INT, INPUT p%datmvt AS DATE,
                                            INPUT p%nompro AS CHAR, INPUT p%typmvt AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%nomcli AS CHAR, 
                                            INPUT p%pxach  AS DEC, INPUT p%pmp AS DEC, INPUT p%ppuv AS DEC, INPUT p%pxunit AS DEC, INPUT p%pxrvt  AS DEC,
                                            INPUT p%mvtprev AS LOG, OUTPUT p%Tot-valeur-achat AS DEC) IN HndGco-MvtStk.

FUNCTION ENT-LOT-PREFAB-GP RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%cod_pro  AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR,
                                           INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR,
                                           INPUT p%depot  AS INT, INPUT p%lienlot AS INT, INPUT p%datmvt AS DATE,
                                           INPUT p%nompro AS CHAR, INPUT p%typmvt AS CHAR, INPUT p%origmvt AS CHAR, 
                                           INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC, 
                                           INPUT p%pxach  AS DEC, INPUT p%pmp AS DEC, INPUT p%ppuv AS DEC, INPUT p%pxunit AS DEC, INPUT p%pxrvt  AS DEC,
                                           INPUT p%mvtprev AS LOG) IN HndGco-MvtStk.

FUNCTION ENT-IMM-PREFAB-GP RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%cod_pro  AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR, INPUT p%cod_dec3 AS CHAR,
                                           INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR, INPUT p%depot  AS INT, 
                                           INPUT p%lienimm AS INT, INPUT p%datmvt AS DATE, INPUT p%nompro AS CHAR, INPUT p%typmvt AS CHAR, INPUT p%origmvt AS CHAR, 
                                           INPUT p%pxach  AS DEC, INPUT p%pmp AS DEC, INPUT p%ppuv AS DEC, INPUT p%pxunit AS DEC, INPUT p%pxrvt AS DEC,
                                           INPUT p%mvtprev AS LOG)IN HndGco-MvtStk.

FUNCTION MAJ-SOR-LOTPREV RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%lienlos-prev AS INT, 
                                         INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC) IN HndGco-MvtStk.

FUNCTION MAJ-SOR-IMMPREV RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%lienims-prev AS INT) IN HndGco-MvtStk.

FUNCTION MAJ-ENT-LOTPREV RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%lienloe-prev AS INT, 
                                         INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC) IN HndGco-MvtStk.

FUNCTION MAJ-ENT-IMMPREV RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%lienime-prev AS INT) IN HndGco-MvtStk.

FUNCTION Lot-saisie RETURN LOGICAL (INPUT p%rowid AS ROWID,INPUT p%premier AS LOG, OUTPUT p%lot AS CHAR, OUTPUT p%qte AS DEC, OUTPUT p%dlc AS DATE,
                                    OUTPUT p%mag AS INT, OUTPUT p%emplac AS CHAR) IN HndGco-MvtStk.

FUNCTION Imm-saisie RETURN LOGICAL (INPUT p%rowid AS ROWID,INPUT p%premier AS LOG, OUTPUT p%imm AS CHAR,
                                    OUTPUT p%mag AS INT, OUTPUT p%emplac AS CHAR) IN HndGco-MvtStk.

FUNCTION Emplac-saisie RETURN LOGICAL (INPUT p%rowid AS ROWID,INPUT p%premier AS LOG, OUTPUT p%qte AS DEC, 
                                       OUTPUT p%mag AS INT, OUTPUT p%emplac AS CHAR) IN HndGco-MvtStk.

FUNCTION Creat-Templotemp RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%esmvt AS CHAR, INPUT P%codpro  AS INT, INPUT P%coddec1 AS CHAR, INPUT P%coddec2 AS CHAR, INPUT P%coddec3 AS CHAR,
                                          INPUT P%coddec4 AS CHAR, INPUT P%coddec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%kvar AS CHAR, INPUT P%depot AS INT,
                                          INPUT p%lot AS CHAR, INPUT p%dlc AS DATE, INPUT P%mag AS INT, INPUT P%emplac AS CHAR, INPUT P%prio AS INT, 
                                          INPUT P%Qte AS DEC, INPUT p%poidb AS DEC, INPUT p%poidn AS DEC, INPUT p%pxa AS DEC,
                                          INPUT p%texte AS CHAR, INPUT p%lotfou AS CHAR, INPUT p%paysori AS CHAR, INPUT p%lotkvar AS CHAR, INPUT p%dlcprev AS DATE) IN HndGco-MvtStk.

FUNCTION Creat-TempEmp RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT P%depot AS INT, INPUT P%mag AS INT, INPUT P%emplac AS CHAR, INPUT P%prio AS INT, INPUT P%Qte AS DEC) IN HndGco-MvtStk.

FUNCTION creat-tempImmemp RETURN LOGICAL (INPUT p%rowidmvt AS ROWID, INPUT p%tie_num AS int,INPUT p%esmvt AS CHAR, INPUT P%codpro  AS INT, INPUT P%coddec1 AS CHAR, INPUT P%coddec2 AS CHAR, INPUT P%coddec3 AS CHAR,
                                          INPUT P%coddec4 AS CHAR, INPUT P%coddec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%kvar AS CHAR, INPUT P%depot AS INT,INPUT p%DepDestination AS INT,
                                          INPUT p%immatr AS CHAR, INPUT p%noImm AS INT, INPUT P%mag AS INT,INPUT P%magDest AS INT, INPUT P%emplac AS CHAR,INPUT P%emplacDest AS CHAR, INPUT P%prio AS INT, INPUT P%prioDest AS INT,
                                          INPUT p%oldimmatr AS CHAR, INPUT p%oldsup AS CHAR,INPUT p%texte AS CHAR, INPUT p%tie_pxu AS DEC)  IN HndGco-MvtStk.

FUNCTION Creat-All-Templotemp RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT p%esmvt AS CHAR, INPUT P%codpro  AS INT, 
                                              INPUT P%coddec1 AS CHAR, INPUT P%coddec2 AS CHAR, INPUT P%coddec3 AS CHAR, INPUT P%coddec4 AS CHAR, INPUT P%coddec5 AS CHAR, 
                                              INPUT p%md5 AS CHAR, INPUT p%kvar AS CHAR, INPUT P%depot AS INT, INPUT p%lot AS CHAR, INPUT p%dlc AS DATE, INPUT P%mag AS INT, 
                                              INPUT P%emp AS CHAR, INPUT P%prio AS INT, INPUT P%Qte AS DEC, INPUT p%poidb AS DEC, INPUT p%poidn AS DEC, INPUT p%pxu AS DEC,
                                              INPUT p%texte AS CHAR, INPUT p%fou AS CHAR, INPUT p%pay AS CHAR, INPUT p%lotk_var AS CHAR, INPUT p%dlcprev AS DATE, 
                                              INPUT p%liensor AS INT, INPUT p%lotmd5 AS CHAR, INPUT p%oldlot AS CHAR, INPUT p%oldpoids_b AS DEC, INPUT p%oldpoids_n AS DEC, INPUT p%oldqte AS DEC) IN HndGco-MvtStk.

FUNCTION Creat-All-tt-sscclot RETURN LOGICAL (INPUT p%rowid_sscclot AS ROWID, INPUT p%rowid_le AS ROWID, INPUT P%Qte AS DEC, INPUT p%rowid_ttle AS ROWID, INPUT P%mag AS INT, INPUT P%emp AS CHAR) IN HndGco-MvtStk. /* $A59689 */

FUNCTION Creat-All-TempEmp RETURN LOGICAL (INPUT p%rowid AS ROWID, INPUT P%depot AS INT, INPUT P%mag AS INT, INPUT P%emplac AS CHAR, INPUT P%prio AS INT, INPUT P%Qte AS DEC) IN HndGco-MvtStk. /*$A64426*/

/* PAS ENCORE VALIDEE... */
/* COMMUN ENTREE / SORTIE */

FUNCTION RECUP-POIDS-VOLUME RETURNS LOGICAL (INPUT p%codPro AS INT, INPUT p%qte AS DEC, INPUT p%depot AS INT,
                                             INPUT p%kvar AS CHAR, INPUT p%d1 AS CHAR, INPUT p%d2 AS CHAR, INPUT p%d3 AS CHAR, INPUT p%d4 AS CHAR, INPUT p%d5 AS CHAR,
                                             INPUT-OUTPUT P%Uni_vte AS CHAR, INPUT-OUTPUT p%poidsBrut AS DEC, OUTPUT p%poidsNet AS DEC, OUTPUT p%volume AS DEC) IN HndGco-MvtStk.

FUNCTION MAJ-TEXTE-REGROUPEMENT RETURNS LOGICAL (INPUT p%typMvt AS CHAR, INPUT p%codTiers AS INT, INPUT p%numreg AS INT, INPUT p%texte AS CHAR) IN HndGco-MvtStk.

/* ENTREE */

FUNCTION ENT-TEST-VALIDE-INVENTAIRE RETURN LOGICAL (INPUT p%depot AS INTEGER, INPUT p%codPro AS INTEGER, INPUT p%d1 AS CHARACTER, INPUT p%d2 AS CHARACTER,
                                                    INPUT p%d3 AS CHARACTER, INPUT p%d4 AS CHARACTER, INPUT p%d5 AS CHARACTER, 
                                                    INPUT p%md5 AS CHARACTER, INPUT p%kvar AS CHARACTER,
                                                    INPUT p%datMvt AS DATE, INPUT p%tt_ligbois AS HANDLE,
                                                    INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codArret AS CHARACTER, OUTPUT p%txtBlocage AS CHARACTER) IN HndGco-MvtStk.

FUNCTION ENT-MAJ-REGROUPEMENT RETURNS INTEGER (INPUT p%numreg AS INT) IN HndGco-MvtStk.

FUNCTION ENT-ARTICLE-VALIDE RETURNS LOGICAL (INPUT p%codPro AS INTEGER, INPUT p%qui AS CHARACTER, INPUT p%dateMvt AS DATE, 
                                             INPUT p%pxMvt AS DECIMAL, INPUT p%natMvt AS CHAR, INPUT p%refMvt AS CHAR,
                                             INPUT p%qte AS DECIMAL, INPUT p%poidsBrut AS DECIMAL, INPUT p%affaire AS CHAR, 
                                             INPUT p%tt_ligbois AS HANDLE, INPUT-OUTPUT p%val_lib AS CHARACTER,
                                             OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION ENT-ARTICLE-VALIDE-2 RETURNS LOGICAL (INPUT p%typEdt AS CHARACTER, INPUT p%codTiers AS INTEGER, INPUT p%location AS LOGICAL,
                                               INPUT p%indContrat AS INTEGER, INPUT p%contrat AS CHARACTER,
                                               INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION ENT-RECEPTION-FOURNISSEUR-VALIDE RETURNS LOGICAL (INPUT p%depot AS INTEGER, INPUT p%dateRec AS DATE, INPUT p%dateChange AS DATE, 
                                                           INPUT p%incDeb AS CHARACTER, INPUT p%pays AS CHARACTER, INPUT p%noBl AS CHARACTER, INPUT p%qui AS CHARACTER, 
                                                           INPUT p%rentetfou AS ROWID, INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

/* SORTIE */

FUNCTION SOR-TEST-VALIDE-INVENTAIRE RETURN LOGICAL (INPUT p%depot AS INTEGER, INPUT p%codPro AS INTEGER, INPUT p%d1 AS CHARACTER, INPUT p%d2 AS CHARACTER,
                                                    INPUT p%d3 AS CHARACTER, INPUT p%d4 AS CHARACTER, INPUT p%d5 AS CHARACTER, 
                                                    INPUT p%md5 AS CHARACTER, INPUT p%kvar AS CHARACTER,
                                                    INPUT p%datMvt AS DATE, INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codArret AS CHARACTER, OUTPUT p%txtBlocage AS CHARACTER) IN HndGco-MvtStk.

FUNCTION SOR-MAJ-REGROUPEMENT RETURNS INTEGER (INPUT p%numreg AS INT, INPUT-OUTPUT p%typEdt AS CHAR, INPUT-OUTPUT p%codTiers AS INT) IN HndGco-MvtStk.

FUNCTION SOR-ARTICLE-VALIDE RETURNS LOGICAL (INPUT p%codPro AS INTEGER, INPUT p%qui AS CHARACTER, INPUT p%dateMvt AS DATE, 
                                             INPUT p%pmp AS DECIMAL, INPUT p%natMvt AS CHAR, INPUT p%refMvt AS CHAR,
                                             INPUT p%qte AS DECIMAL, INPUT p%poidsBrut AS DECIMAL, INPUT p%affaire AS CHAR, 
                                             INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION SOR-ARTICLE-VALIDE-2 RETURNS LOGICAL (INPUT p%typEdt AS CHAR, INPUT p%codTiers AS INT, INPUT-OUTPUT p%val_lib AS CHARACTER,
                                               OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

/* TRANSFERT DE DEPOT A DEPOT */

FUNCTION TRF-TEST-VALIDE-INVENTAIRE RETURN LOGICAL (INPUT p%depotOri AS INTEGER, INPUT p%depotDes AS INTEGER, 
                                                    INPUT p%codPro AS INTEGER, INPUT p%d1 AS CHARACTER, INPUT p%d2 AS CHARACTER,
                                                    INPUT p%d3 AS CHARACTER, INPUT p%d4 AS CHARACTER, INPUT p%d5 AS CHARACTER, 
                                                    INPUT p%md5 AS CHARACTER, INPUT p%kvar AS CHARACTER, INPUT p%datMvt AS DATE, 
                                                    INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codArret AS CHARACTER, OUTPUT p%txtBlocage AS CHARACTER) IN HndGco-MvtStk.

FUNCTION TRF-MAJ-REGROUPEMENT RETURNS INTEGER (INPUT p%numreg AS INT) IN HndGco-MvtStk.

FUNCTION TRF-ARTICLE-VALIDE RETURNS LOGICAL (INPUT p%depotOri AS INT, INPUT p%depotDes AS INT, INPUT p%codPro AS INTEGER, INPUT p%qui AS CHARACTER, INPUT p%dateEnt AS DATE, INPUT p%dateSor AS DATE,
                                                   INPUT p%pxMvt AS DECIMAL, INPUT p%natMvt AS CHAR, INPUT p%refMvt AS CHAR,
                                                   INPUT p%qte AS DECIMAL, INPUT p%poidsBrut AS DECIMAL, INPUT p%affaire AS CHAR, 
                                                   INPUT p%CodeBarre AS LOGICAL, INPUT p%location AS LOGICAL, INPUT p%contrat AS CHARACTER, 
                                                   INPUT p%indContrat AS INTEGER, INPUT-OUTPUT p%val_lib AS CHARACTER,
                                                   OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION TRANSFERT-DIFFERE-POSSIBLE RETURN LOGICAL (INPUT p%numreg AS INT, INPUT valE AS LOGICAL, INPUT valS AS LOGICAL, 
                                                    OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

/* ... PAS ENCORE VALIDEE */

FUNCTION TransfertLotEmplacement RETURNS LOGICAL (INPUT p%codpro AS INT, INPUT p%coddec1 AS CHAR, INPUT p%coddec2 AS CHAR, 
                                                  INPUT p%coddec3 AS CHAR, INPUT p%coddec4 AS CHAR, INPUT p%coddec5 AS CHAR, INPUT p%md5 AS CHAR,
                                                  INPUT p%kvar AS CHAR, 
                                                  INPUT P%Uni_vte AS CHAR, INPUT p%qte AS DEC, 
                                                  INPUT p%depotOri AS INT, INPUT p%magasinOri AS INT, INPUT p%emplacOri AS CHAR, 
                                                  INPUT p%depotDes AS INT, INPUT p%magasinDes AS INT, INPUT p%emplacDes AS CHAR, INPUT p%lot AS CHAR, OUTPUT p%erreur AS CHAR) IN HndGco-MvtStk.


/*****************************************************************************/
/*                                 S.S.C.C                                   */
/*****************************************************************************/

FUNCTION SSCC_Crt_Numero                RETURNS LOG (INPUT p%Dep AS INT, INPUT p%RowidPro AS ROWID, INPUT p%CodDec1 AS CHAR, INPUT p%CodDec2 AS CHAR, INPUT p%CodDec3 AS CHAR, INPUT p%CodDec4 AS CHAR,
                                                     INPUT p%CodDec5 AS CHAR, INPUT p%Kvar AS CHAR, INPUT p%Qte AS DEC, OUTPUT p%SSCC AS CHAR, OUTPUT p%NoUL AS INT) IN HndGco-MvtStk.

FUNCTION SSCC_TransfertEmplacement      RETURNS LOGICAL (INPUT p%sscc AS CHAR, INPUT p%depotDes AS INT, INPUT p%magasinDes AS INT, INPUT p%emplacDes AS CHAR, 
                                                         INPUT P%Uni_vte AS CHAR, OUTPUT p%erreur AS CHAR)  IN HndGco-MvtStk.

FUNCTION SSCC_ControleDepot             RETURNS LOGICAL (INPUT p%Depot AS INT)  IN HndGco-MvtStk.

FUNCTION SSCC_ControleSSCCGere          RETURNS LOGICAL (INPUT p%Depot AS INT, INPUT p%CodPro AS INT) IN HndGco-MvtStk.

FUNCTION SSCC_ControleStatut            RETURNS LOGICAL (INPUT p%SSCC AS CHAR, OUTPUT p%Msg AS CHAR)  IN HndGco-MvtStk.

FUNCTION SSCC_EstAffectee               RETURNS LOGICAL (INPUT p%SSCC AS CHAR, OUTPUT p%Msg AS CHAR)  IN HndGco-MvtStk.

FUNCTION SSCC_ModifierStatut            RETURNS LOGICAL (INPUT p%SSCC AS CHAR, INPUT p%Statut AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_DeplacerColis RETURNS LOGICAL (INPUT p%SSCCMere AS CHAR, INPUT p%SSCCFille AS CHAR, INPUT p%Lot AS CHAR, INPUT p%CodPro AS INT, 
                                             INPUT p%CodDec1 AS CHAR, INPUT p%CodDec2 AS CHAR, INPUT p%CodDec3 AS CHAR, INPUT p%CodDec4 AS CHAR, 
                                             INPUT p%CodDec5 AS CHAR, INPUT p%Md5 AS CHAR, 
                                             INPUT P%Uni_vte AS CHAR, INPUT p%NbColis AS DEC,
                                             INPUT p%Depot AS INT, INPUT p%Magasin AS INT, INPUT p%Emplac AS CHAR, INPUT p%CodCli AS INT,
                                             INPUT p%NoBL AS INT, INPUT p%NoCde AS INT, INPUT p%NoOF AS INT, INPUT p%Statut AS CHAR,
                                             INPUT p%TypeSup AS CHAR, INPUT p%TypePal AS CHAR, INPUT p%TypeSai AS CHAR, INPUT p%Ori AS CHAR, INPUT p%dateMvt AS DATE, 
                                             OUTPUT p%SSCC AS CHAR, OUTPUT p%Msg AS CHAR)    IN HndGco-MvtStk.

FUNCTION SSCC_AffecterSSCC RETURNS LOG (INPUT p%SSCC AS CHAR, INPUT p%Lot AS CHAR, INPUT p%CodPro AS INT, INPUT p%CodDec1 AS CHAR, INPUT p%CodDec2 AS CHAR, INPUT p%CodDec3 AS CHAR, 
                                        INPUT p%CodDec4 AS CHAR, INPUT p%CodDec5 AS CHAR, INPUT p%Md5 AS CHAR, INPUT p%Kvar AS CHAR, INPUT p%Depot AS INT, 
                                        INPUT P%Uni_vte AS CHAR, INPUT p%NbCol AS DEC,
                                        INPUT p%CodCli AS INT, p%CodFou AS INT, INPUT p%NoBL AS INT, INPUT p%NoBLa AS CHAR, INPUT p%NoCde AS INT, INPUT p%NoCdea AS INT, INPUT p%NoOF AS INT, 
                                        INPUT p%NoBP AS INT, INPUT p%TypSai AS CHAR, INPUT p%NoLigne AS INT, INPUT p%MajQtePrep AS LOG, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_AnnulBP RETURNS LOG (INPUT p%CodCli AS INT, INPUT p%NoCde  AS INT, INPUT p%Eclater AS LOG, OUTPUT p%ListeSSCC AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_ModifLigneBP RETURNS LOG (INPUT p%CodCli AS INT, INPUT p%NoCde AS INT, INPUT p%NoLigne AS INT) IN HndGco-MvtStk.

FUNCTION SSCC_BlocageSSCCParLot RETURNS LOG (INPUT p%Depot AS INT, INPUT p%Lot AS CHAR, INPUT p%CodPro AS INT, INPUT p%CodDec1 AS CHAR, INPUT p%CodDec2 AS CHAR, INPUT p%CodDec3 AS CHAR, 
                                         INPUT p%CodDec4 AS CHAR, INPUT p%CodDec5 AS CHAR, INPUT p%Md5 AS CHAR, INPUT p%Blocage AS LOG, INPUT p%NatBlocage AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_BlocageSSCC RETURNS LOG (INPUT p%SSCC AS CHAR, INPUT p%Blocage AS LOG, INPUT p%NatBlocage AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_EstBloquee RETURNS LOG (INPUT p%SSCC AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_ControleBL RETURNS LOG (INPUT p%CodCli AS INT, INPUT p%NoCde AS INT, INPUT p%NoBL AS INT, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_ControleExp RETURNS LOG (INPUT p%CodCli AS INT, INPUT p%NoCde AS INT, INPUT p%NoBL AS INT, INPUT p%ListeSSCC AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_ControleSsccBL RETURNS LOG (INPUT p%CodCli AS INT, INPUT p%NoCde AS INT, INPUT p%NoBL AS INT, INPUT p%SSCC AS CHAR, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC_SortieStock RETURNS LOG (INPUT p%SSCC AS CHAR, INPUT p%Statut AS CHAR, INPUT p%LienSSCC AS INT, INPUT p%Ori AS CHAR, INPUT p%DateMvt AS DATE, OUTPUT p%Msg AS CHAR) IN HndGco-MvtStk.

FUNCTION SSCC-TRF-TEST-VALIDE-INVENTAIRE RETURN LOGICAL (INPUT p%sscc AS CHARACTER, INPUT p%depotDes AS INTEGER, INPUT p%datMvt AS DATE, INPUT-OUTPUT p%val_lib AS CHARACTER, 
                                                         OUTPUT p%codArret AS CHARACTER, OUTPUT p%txtBlocage AS CHARACTER) IN HndGco-MvtStk.

FUNCTION SSCC-TRF-ARTICLE-VALIDE RETURNS LOGICAL (INPUT p%depotOri AS INT, INPUT p%depotDes AS INT, INPUT p%sscc AS CHARACTER, INPUT p%qui AS CHARACTER, INPUT p%dateEnt AS DATE, INPUT p%dateSor AS DATE, 
                                                  INPUT p%natMvt AS CHAR, INPUT p%refMvt AS CHAR, INPUT p%affaire AS CHAR, 
                                                  INPUT p%CodeBarre AS LOGICAL, INPUT p%location AS LOGICAL, INPUT p%contrat AS CHARACTER, 
                                                  INPUT p%indContrat AS INTEGER, INPUT-OUTPUT p%val_lib AS CHARACTER,
                                                  OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION SSCC_InitModifReception RETURNS LOG (INPUT p%Rowid AS ROWID, INPUT p%LienSSCC AS INT, INPUT p%CodPro AS INT) IN HndGco-MvtStk.

FUNCTION Creat-TempSSCC RETURN LOGICAL (INPUT p%SSCC AS CHAR, INPUT p%NoUL AS INT, INPUT p%Rowid AS ROWID, INPUT p%Lot AS CHAR, INPUT P%Qte AS DEC, 
                                        INPUT p%CodPro AS INT, INPUT p%TypePal AS CHAR, /* $A57303 ... */ INPUT p%Magasin AS INT, INPUT p%Emplac AS CHAR /* ... $A57303 */) IN HndGco-MvtStk.

FUNCTION SSCC_SuppressionTrsEmplacement RETURNS LOG (INPUT pLienSSCC AS INT) IN HndGco-MvtStk. /* $A62496 ... */


/* $A41281 ... */
FUNCTION CREAT-HISTOEMP-ENT-PREV RETURN LOGICAL (INPUT p%lienemp AS INT, INPUT p%cod_pro AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR, INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR,
                                                 INPUT p%k_var AS CHAR, INPUT p%depot AS INT, INPUT p%datmvt AS DATE, INPUT p%datvp AS DATE, INPUT p%hrvp AS INT, INPUT p%nompro AS CHAR, INPUT p%mag AS INT, INPUT p%emplac AS CHAR, INPUT p%origmvt AS CHAR,
                                                 INPUT p%qte AS DEC, 
                                                 INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC) IN HndGco-MvtStk.

FUNCTION CREAT-HISTOEMP-SOR-PREV RETURN LOGICAL (INPUT p%lienemp AS INT, INPUT p%cod_pro  AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR, INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR, INPUT p%md5 AS CHAR,
                                                 INPUT p%k_var AS CHAR, INPUT p%depot  AS INT, INPUT p%datmvt AS DATE, INPUT p%datvp AS DATE, INPUT p%hrvp AS INT, INPUT p%nompro AS CHAR, INPUT p%mag AS INT, INPUT p%emplac AS CHAR, INPUT p%origmvt AS CHAR,
                                                 INPUT p%qte AS DEC, 
                                                 INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC) IN HndGco-MvtStk.

FUNCTION CREAT-HISTOLE-ENT-PREV RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienemp AS INT, INPUT p%cod_pro AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR, INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR,
                                                INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR, INPUT p%depot AS INT, INPUT p%datmvt AS DATE, INPUT p%datvp AS DATE, INPUT p%hrvp AS INT, INPUT p%nompro AS CHAR, INPUT p%lot AS CHAR, INPUT p%mag AS INT,
                                                INPUT p%emplac AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%PdsBru AS DEC, INPUT p%PdsNet AS DEC, INPUT p%qte AS DEC, 
                                                INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC, 
                                                INPUT p%px_unit AS DEC, INPUT p%px_ach AS DEC, INPUT p%pmp AS DEC, INPUT p%px_rvt AS DEC, INPUT p%pp_uv AS DEC) IN HndGco-MvtStk.

FUNCTION CREAT-HISTOLE-SOR-PREV RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienemp AS INT, INPUT p%cod_pro AS INT, INPUT p%cod_dec1 AS CHAR, INPUT p%cod_dec2 AS CHAR, INPUT p%cod_dec3 AS CHAR, INPUT p%cod_dec4 AS CHAR, INPUT p%cod_dec5 AS CHAR,
                                                INPUT p%md5 AS CHAR, INPUT p%k_var AS CHAR, INPUT p%depot AS INT, INPUT p%datmvt AS DATE, INPUT p%datvp AS DATE, INPUT p%hrvp AS INT, INPUT p%nompro AS CHAR, INPUT p%lot AS CHAR, INPUT p%mag AS INT,
                                                INPUT p%emplac AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%PdsBru AS DEC, INPUT p%PdsNet AS DEC, INPUT p%qte AS DEC, 
                                                INPUT P%Uni_vte AS CHAR, INPUT P%Nb_uv1 AS DEC, INPUT P%Nb_uv2 AS DEC, INPUT P%Nb_uv AS DEC, INPUT P%Nb_cv3 AS DEC, INPUT P%Nb_cv AS DEC, 
                                                INPUT p%px_unit AS DEC, INPUT p%px_ach AS DEC, INPUT p%pmp AS DEC, INPUT p%px_rvt AS DEC, INPUT p%pp_uv AS DEC) IN HndGco-MvtStk.

FUNCTION SOR-REFMVT RETURN CHARACTER (INPUT p%OrigMvt AS CHARACTER) IN HndGco-MvtStk.
/* ... $A41281 */

FUNCTION getOptionMvtStk RETURN CHARACTER (INPUT p%Cle AS CHAR) IN HndGco-MvtStk. /* $A41786 */
FUNCTION setOptionMvtStk RETURN LOGICAL (INPUT p%Cle AS CHAR, INPUT p%Valeur AS CHAR) IN HndGco-MvtStk. /* $A41786 */

/* $A59721 Transformation de stock... */
FUNCTION TS-CTRL-ENTETE-TRANSFO RETURN LOGICAL (INPUT p%typEntree AS CHAR, INPUT p%codTiers AS INT, INPUT p%qui AS CHAR, INPUT p%datMvt AS DATE,
                                                INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION TS-ARTICLE-VALIDE RETURNS LOGICAL (INPUT p%codPro AS INTEGER, INPUT p%typEntree AS CHAR, INPUT p%codTiers AS INT, INPUT p%qui AS CHAR, INPUT p%dateMvt AS DATE, 
                                            INPUT p%pmp AS DECIMAL, INPUT p%natMvt AS CHAR, INPUT p%refMvt AS CHAR,
                                            INPUT p%qte AS DECIMAL, INPUT p%poidsBrut AS DECIMAL, INPUT p%affaire AS CHAR, 
                                            INPUT-OUTPUT p%val_lib AS CHARACTER, OUTPUT p%codErreur AS CHARACTER, OUTPUT p%txtErreur AS CHARACTER) IN HndGco-MvtStk.

FUNCTION TS-MAJ-REGROUPEMENT RETURNS INTEGER (INPUT p%numreg AS INT) IN HndGco-MvtStk.

/* ...$A59721 */
