DEF VAR hXPEdite AS HANDLE NO-UNDO.
DEF VAR hXPMain  AS HANDLE NO-UNDO.
{g-hprowinbase.i}
FUNCTION QueryAccessRights RETURNS LOG (INPUT pTypObj AS CHAR, INPUT pObjet AS CHAR, INPUT pUser AS CHAR) IN G-hProwinBase.
/* 
hXPEdite :

    PROCEDURE NouvelEnvoi:
        DEF INPUT  PARAM pType AS CHAR NO-UNDO.
        DEF INPUT  PARAM pSect AS CHAR NO-UNDO.
        DEF OUTPUT PARAM pRet  AS INT  NO-UNDO.

hXPMain :

    PROCEDURE RTVAccuse. (INPUT pSms AS LOG)
    PROCEDURE SendFax :
        DEF INPUT  PARAM pNumFax AS INT NO-UNDO.
        DEF OUTPUT PARAM pOkFax  AS LOG NO-UNDO.
    PROCEDURE DoSendFaxes: (INPUT pSms AS LOG)
*/

FUNCTION AddFaxDest     RETURNS LOG  (INPUT pNum AS INT, INPUT pDest AS CHAR, INPUT pAttn AS CHAR) IN hXPMain.
FUNCTION DeleteFax      RETURNS LOG  (INPUT pNumFax AS INT) IN hXPMain.
FUNCTION DeleteFaxFiles RETURNS LOG  (INPUT pNumFax AS INT) IN hXPMain.
FUNCTION DeleteEnvoi    RETURNS LOG  (INPUT pNumFax AS INT, INPUT pNumEnv AS INT) IN hXPMain.
FUNCTION GetValidNumber RETURNS CHAR (INPUT pNoFax AS CHAR) IN hXPMain.
FUNCTION AddFaxToSend   RETURNS LOG  (INPUT pNum AS INT, INPUT pFile AS CHAR, OUTPUT pType AS LOG) IN hXPMain.
FUNCTION GetXPEInfo     RETURNS CHAR (INPUT pType AS CHAR) IN hXPMain.
FUNCTION DeleteRecu     RETURNS LOG  (INPUT pNumRec AS INT) IN hXPMain.

FUNCTION GetXPText RETURNS CHAR (INPUT pNum AS INT, INPUT pNumEnv AS INT, INPUT pNum2 AS INT, INPUT pType AS CHAR) IN hXPMain.
FUNCTION MajXPText RETURNS LOG  (INPUT pNum AS INT, INPUT pNumEnv AS INT, INPUT pNum2 AS INT, INPUT pType AS CHAR, INPUT pBuff AS CHAR) IN hXPMain.
 
DEF VAR CanXPEdite AS LOG NO-UNDO.

DEF VAR DynXPEdite AS LOG NO-UNDO.

DEF VAR FaxByMail AS LOG NO-UNDO.

DEF VAR chTst AS CHAR NO-UNDO.
DEF SHARED VAR G-User AS CHAR.

DEF VAR XConfig AS CHAR NO-UNDO.

ASSIGN FILE-INFO:FILE-NAME = GetSessionData("Config!", "XPEdite")
       CanXPEdite = FILE-INFO:FULL-PATHNAME <> ?
       FaxByMail  = (GetInfProwin("Config!", "FaxByMail") <> ?).

IF FaxByMail THEN CanXPEdite = YES.
ELSE DO:
    RUN VALUE(GetSessionData("Config!", "ProLink") + "\canxpdt.r") (G-User, OUTPUT XConfig).
    CanXpEdite = CanXpEdite AND (XConfig <> "").
    ASSIGN hXPEdite = GethApi(":StrXPdt")
           hXPMain  = GethApi(":XPMain").
    IF VALID-HANDLE(hXPEdite) AND hXPEdite:Get-Signature("DemXPMain") = "" THEN hXPEdite = ?.
    IF VALID-HANDLE(hXPMain)  AND hXPMain:Get-Signature("NewFaxEx")   = "" THEN hXPMain  = ?.
END.

FUNCTION IsXPDTAdmin RETURNS LOG:
    DEF VAR AdmUser   AS LOG NO-UNDO.
    DEF VAR ASP       AS LOG NO-UNDO.
    DEF VAR NewProwin AS LOG NO-UNDO.
    IF FaxByMail THEN RETURN NO.

    ASSIGN ASP      = (GetSessionData("Config!", "ASP") = "Oui")
           AdmUser  = (G-User = "Admin") OR
               (ASP AND ((G-User BEGINS "Adm_" OR G-User BEGINS "Pnv_") OR (CAN-DO("Admin,PnvAdm,CdrData", USERID("Prowin"))))) OR
               (GetSessionData("Config!", "ModeProwin") = "Oui") OR
               (IsMember(G-User, "000887", "@Admin")) OR
               (IsMember(G-User, "Prowin", "@Admin"))
           NewProwin = CAN-DO(G-hProwinBase:INTERNAL-ENTRIES, "QueryAccessRights").
    IF NewProwin AND NOT AdmUser THEN AdmUser = (QueryAccessRights("XPEdite", "Admin", G-User) = YES).
    RETURN AdmUser.
END FUNCTION.

PROCEDURE StartXPEdite:
    TraceSession("StartXPEdite CanXPEdite : " + GetSecureVal(STRING(CanXPEdite), "?")).
    IF NOT CanXPEdite THEN RETURN ERROR.

    TraceSession("StartXPEdite").
    IF FaxByMail THEN RUN VALUE(GetSessionData("Config!", "XPEdite")  + "\FaxByMail.r") PERSISTENT SET hXPEdite.
    ELSE DO:
        hXPEdite = GethApi(":StrXPdt").
        IF VALID-HANDLE(hXPEdite) AND hXPEdite:Get-Signature("DemXPMain") = "" THEN hXPEdite = ?.
        IF NOT VALID-HANDLE(hXPEdite) THEN DO:
            SetSessionData("XPEDite", "Parent", STRING(THIS-PROCEDURE)).
            RUN VALUE(GetSessionData("Config!", "ProLink") + "\StrXPdt.R") PERSISTENT SET hXPEdite.
	    TraceSession("StartXPEdite 2").
            DynXPEdite = YES.
        END.
        IF VALID-HANDLE(hXPEdite) THEN RUN DemXPMain IN hXPEdite.
    END.
    TraceSession("StartXPEdite: hXPEdite " + STRING(VALID-HANDLE(hXPEdite))).

    hXPMain = GethApi(":XPMain").
    IF VALID-HANDLE(hXPMain)  AND hXPMain:Get-Signature("NewFaxEx")  = "" THEN hXPMain  = ?.
END PROCEDURE.
PROCEDURE StartXPEdBatch:
    DEF INPUT PARAM XPHost AS CHAR NO-UNDO.
    DEF INPUT PARAM XPName AS CHAR NO-UNDO.
    DEF INPUT PARAM XPPort AS INT  NO-UNDO.

    IF FaxByMail THEN RETURN.

    SetSessionData("XPEDite", "Mode", "Batch").
    hXPEdite = GethApi(":StrXPdt").
    IF VALID-HANDLE(hXPEdite) AND hXPEdite:Get-Signature("DemXPMain") = "" THEN hXPEdite = ?.
    IF NOT VALID-HANDLE(hXPEdite) THEN DO:
        RUN VALUE(GetSessionData("Config!", "ProLink") + "\StrXPdtB.R") PERSISTENT SET hXPEdite (XPHost, XPName, XPPort, THIS-PROCEDURE).
        DynXPEdite = YES.
    END.
    /*IF VALID-HANDLE(hXPEdite) THEN RUN DemXPMain IN hXPEdite.*/
    hXPMain = GethApi(":XPMain").
    IF VALID-HANDLE(hXPMain)  AND hXPMain:Get-Signature("NewFaxEx")  = "" THEN hXPMain  = ?.
END PROCEDURE.

FUNCTION CloseXPEdite RETURNS LOG:
    IF DynXPEdite AND VALID-HANDLE(hXPEdite) THEN APPLY "CLOSE" TO hXPEdite.
    hXPEdite = ?.
    RETURN YES.
END FUNCTION.
&IF "{1}" EQ "HISTO" &THEN
DEF VAR hHisto AS HANDLE NO-UNDO.
FUNCTION SetModeGestion RETURNS LOG (INPUT CHAR, INPUT CHAR, INPUT CHAR) IN hHisto.
FUNCTION SetSelection   RETURNS LOG (INPUT CHAR, INPUT CHAR, INPUT CHAR) IN hHisto.
FUNCTION GetMinSize     RETURNS LOG (OUTPUT INT, OUTPUT INT) IN hHisto.
FUNCTION ApplyMinSize   RETURNS LOG IN hHisto.
FUNCTION ApplyNewSize   RETURNS LOG IN hHisto.

DEF VAR hHistRec AS HANDLE NO-UNDO.
FUNCTION SetSelection-R RETURNS LOG (INPUT CHAR, INPUT DATE) IN hHistRec.
FUNCTION AddNumero-R    RETURNS LOG (INPUT CHAR) IN hHistRec.
FUNCTION AddTiers RETURNS LOG (INPUT CHAR, INPUT INT) IN hHistRec.
FUNCTION GetMinSize-R   RETURNS LOG (OUTPUT INT, OUTPUT INT) IN hHistRec.
FUNCTION ApplyMinSize-R RETURNS LOG IN hHistRec.
FUNCTION ApplyNewSize-R RETURNS LOG IN hHistRec.
&ENDIF
