/* 
Cr�� par VR
Sp�cificit�s par d�p�t pour cr�ation de lignecli, lignefou, histolig
N�cessaire si cr�ation de l'enregistrement � partir de produit ou
           si cr�ation de l'enregistrement avec un d�p�t diff�rent
*/

/* Prise en compte du produit commercial pour les donn�es se trouvant sur celui-ci */

IF pref_parcde_prod OR pref_parcde_famcpt THEN
DO:
    FIND FIRST deppro WHERE deppro.cod_pro = (
            &if "{2}" = "VENTE" OR "{2}" = "HISTO" OR "{2}" = "ACHAT" &then IF {1}.cod_ori <> 0 THEN {1}.cod_ori ELSE &endif         
            {1}.cod_pro) AND deppro.depot = {1}.depot NO-LOCK NO-ERROR. 
    IF pref_parcde_famcpt AND AVAIL deppro THEN
        IF deppro.fam_cpt <> 0 THEN {1}.fam_cpt = deppro.fam_cpt.
    
    /* Prise en compte du produit normal */
    &if "{2}" = "VENTE" &then
        IF {1}.cod_ori <> 0 THEN
            FIND FIRST deppro WHERE deppro.cod_pro = {1}.cod_pro AND deppro.depot = {1}.depot NO-LOCK NO-ERROR. 
        IF pref_parcde_prod AND AVAIL deppro THEN
        DO:
            IF {1}.no_of = 0 AND {1}.no_lance = '' AND AVAIL deppro AND deppro.prod_gp = NO THEN {1}.prod_gp = NO.
        END.
    &endif
END.


