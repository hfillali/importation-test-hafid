/* Duplication des lignes scenario 
   Cr�� par VR le 20/04/05 */

DEF VAR memosce AS INT no-undo.
DEF BUFFER ligscep2 for ligscep.
DEF BUFFER ligsces2 for ligsces.

IF {1}.ligsce <> 0 THEN
DO:    
    memosce = NEXT-VALUE (compteur_scelig).
    FOR EACH ligscep WHERE ligscep.cpt_saisie = {1}.ligsce NO-LOCK :
        CREATE ligscep2.
        BUFFER ligscep2:BUFFER-COPY (BUFFER ligscep:HANDLE,"cpt_saisie").
        ASSIGN
            ligscep2.cpt_saisie = memosce.
    END.

    FOR EACH ligsces WHERE ligsces.cpt_saisie = {1}.ligsce NO-LOCK :
        CREATE ligsces2.
        BUFFER ligsces2:BUFFER-COPY (BUFFER ligsces:HANDLE,"cpt_saisie").
        ASSIGN
            ligsces2.cpt_saisie = memosce.
    END.    
    {2}.ligsce = memosce.
END. 
