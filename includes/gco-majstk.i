&IF DEFINED(HndGco-Majstk) = 0 &THEN
&GLOBAL-DEFINE HndGco-Majstk HndGco-Majstk
DEFINE VARIABLE HndGco-Majstk    AS HANDLE NO-UNDO.
DEFINE VARIABLE HndGco-Majstk### AS CHARACTER NO-UNDO.

{g-hprowinbase.i}
HndGco-Majstk### = GetSessionData ("GCO-MAJSTK":U,"CLE":U).
HndGco-Majstk = WIDGET-HANDLE(HndGco-Majstk###) NO-ERROR.

/***************************************************************************/
/*                         CREATION STOCK                                  */
/***************************************************************************/
/* Cr�ation du Stock pour un Produit sans d�clinaison ni variante */
FUNCTION STK_Creation-Stock-Produit RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%depot AS INTEGER) IN HndGco-Majstk.

/* Cr�ation du Stock pour une D�clinaison */
FUNCTION STK_Creation-Stockdec-Produit RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                                       INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, INPUT p%depot AS INTEGER) IN HndGco-Majstk.

/* Cr�ation du Stock pour une Variante */
FUNCTION STK_Creation-Stockvar-Produit RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%k_var AS CHARACTER, INPUT p%md5 AS CHARACTER, INPUT p%depot AS INTEGER) IN HndGco-Majstk.
/* Cr�ation du stock */
FUNCTION STK_Creation-Stock RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                                    INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, 
                                                    INPUT p%k_var AS CHARACTER, INPUT p%md5 AS CHARACTER, INPUT p%depot AS INTEGER) IN HndGco-Majstk.

/***************************************************************************/
/*                                ENTREE DE STOCK                          */
/***************************************************************************/
/* MAJ entr�e du Stock seul */
FUNCTION STK_Majent_Stock_Produit RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG) IN HndGco-Majstk.

/* MAJ entr�e du Stockdec seul */
FUNCTION STK_Majent_Stockdec_Produit RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG) IN HndGco-Majstk.

/* MAJ entr�e du Stockvar seul */
FUNCTION STK_Majent_Stockvar_Produit RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG) IN HndGco-Majstk.

/* Appel entr�e Global MAJ du stock */
FUNCTION STK_Majent_stock RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG) IN HndGco-Majstk.



FUNCTION STK_Majent_Stock_Produit_GP RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%typ AS CHAR, 
                                                     INPUT p%OptimCalPmp AS LOG, INPUT p%calpmp_qteunit AS DEC, INPUT p%calpmp_POidtot AS DEC,
                                                     INPUT-OUTPUT p%calpmp_etape AS INT, INPUT-OUTPUT p%calpmp_dernpmp AS DEC) IN HndGco-Majstk.
FUNCTION STK_Majent_Stockdec_Produit_GP RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%typ AS CHAR, 
                                                        INPUT p%OptimCalPmp AS LOG, INPUT p%calpmp_qteunit AS DEC, INPUT p%calpmp_POidtot AS DEC,
                                                        INPUT-OUTPUT p%calpmp_etape AS INT, INPUT-OUTPUT p%calpmp_dernpmp AS DEC) IN HndGco-Majstk.
FUNCTION STK_Majent_stockvar_Produit_GP RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%typ AS CHAR, 
                                                        INPUT p%OptimCalPmp AS LOG, INPUT p%calpmp_qteunit AS DEC, INPUT p%calpmp_POidtot AS DEC,
                                                        INPUT-OUTPUT p%calpmp_etape AS INT, INPUT-OUTPUT p%calpmp_dernpmp AS DEC) IN HndGco-Majstk.
FUNCTION STK_Majent_stock_GP RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%typ AS CHAR, 
                                             INPUT p%OptimCalPmp AS LOG, INPUT p%calpmp_qteunit AS DEC, INPUT p%calpmp_POidtot AS DEC,
                                             INPUT-OUTPUT p%calpmp_etape AS INT, INPUT-OUTPUT p%calpmp_dernpmp AS DEC ) IN HndGco-Majstk.


FUNCTION STK_MAJENT-QTEPICKING RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT p%depot AS INTEGER,
                                               INPUT p%magasin AS INTEGER, INPUT p%emplac  AS CHARACTER) IN HndGco-Majstk.

/* Suppression d'une ent�e */
FUNCTION STK_SUPPRESSION-IMMLOT-ENT-PREV RETURN LOGICAL( INPUT p%lienlot AS INT, INPUT p%lienimm AS INT) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-IMMLOT-ENT-HIS RETURN LOGICAL( INPUT p%lienlot AS INT, INPUT p%lienimm AS INT, INPUT p%lienemp AS INT) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-EMPL-ENT-HIS RETURN LOGICAL(INPUT p%lienemp AS INT) IN HndGco-Majstk.
    


/***************************************************************************/
/*                                SORTIE DE STOCK                          */
/***************************************************************************/
FUNCTION STK_MajSor_Stock_Produit-HIS RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
FUNCTION STK_MajSor_Stockdec_Produit-HIS RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
FUNCTION STK_MajSor_Stockvar_Produit-HIS RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
/* MAJ : Sortie Globale du stock */
FUNCTION STK_Majsor_stock-HIS RETURN LOGICAL (BUFFER p%histolig FOR histolig, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.

FUNCTION STK_MajSor_Stock_Produit-LIG RETURN LOGICAL (BUFFER p%lignecli FOR lignecli, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
FUNCTION STK_MajSor_Stockdec_Produit-LIG RETURN LOGICAL (BUFFER p%lignecli FOR lignecli, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
FUNCTION STK_MajSor_Stock_varProduit-LIG RETURN LOGICAL (BUFFER p%lignecli FOR lignecli, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.
/* MAJ : Sortie Globale du stock */
FUNCTION STK_Majsor_stock-LIG RETURN LOGICAL (BUFFER p%lignecli FOR lignecli, INPUT P%repartition-emp AS LOG, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.

/* MAJ du stock lors d'une suppression d'une sortie */
/* Attention MAJ du lignecli */
FUNCTION STK_SORTIE-SUPPRIME RETURN LOGICAL (BUFFER hlignecli FOR lignecli) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-EMPL-SOR-HIS RETURN LOGICAL(INPUT p%lienemp AS INT, INPUT p%qte AS DEC) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-LIENS-IMMLOTCON-LIG RETURN LOGICAL( BUFFER hlignecli FOR lignecli) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-IMMLOTCON-SOR-PRE RETURN LOGICAL( INPUT p%lienlot AS INT, INPUT p%lienimm AS INT) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-LIENS-IMMLOTCON-HIS RETURN LOGICAL( BUFFER hhistolig FOR histolig) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-LIENS-IMMLOTCON RETURN LOGICAL(INPUT p%lienlot AS INT, INPUT p%lienimm AS INT, INPUT p%liencon AS INT, INPUT p%qte AS DEC ) IN HndGco-Majstk.
FUNCTION STK_SUPPRESSION-LIENS-SSCC RETURNS LOGICAL (BUFFER hlignecli FOR lignecli) IN HndGco-Majstk.

/* Recalcul du stock et P.M.P */
FUNCTION STK_Recalcul_stock_PMP RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                                INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, 
                                                INPUT p%depot AS INTEGER, INPUT p%k_var AS CHARACTER, INPUT p%md5 AS CHARACTER) IN HndGco-Majstk.


FUNCTION STK_REFMVT RETURN CHARACTER (INPUT p%esmvt AS CHARACTER, INPUT p%OrigMvt AS CHARACTER) IN HndGco-Majstk.
FUNCTION MAJ_NOMPRO-COM RETURN CHARACTER (INPUT p%codpro AS INT, INPUT p%nompro AS CHAR, INPUT p%md5 AS CHAR, INPUT p%kvar AS CHAR) IN HndGco-Majstk.
FUNCTION MAJ_DISPO_EMPLAC RETURN LOGICAL (INPUT p%depot AS INT, INPUT p%mag AS INT, INPUT p%emplac AS CHAR) IN HndGco-Majstk.

FUNCTION STK_Changement-EmplPicking RETURN LOGICAL (INPUT p%codpro AS INT, INPUT p%coddec1 AS CHAR, INPUT p%coddec2 AS CHAR,
                                                    INPUT p%coddec3 AS CHAR, INPUT p%coddec4 AS CHAR, INPUT p%coddec5 AS CHAR, INPUT p%md5 AS CHAR, INPUT p%kvar AS CHAR,
                                                    INPUT p%depot AS INT, INPUT p%datmvt AS DATE,
                                                    INPUT p%old-mag AS INT, INPUT p%old-emplac AS CHAR, INPUT p%new-mag AS INT, INPUT p%new-emplac AS CHAR) IN HndGco-Majstk.

/************************************************************************/
/*                        GESTION DES LOTS                              */
/************************************************************************/
FUNCTION STK_CREATION_SORENT_LOT RETURN LOGICAL (INPUT p%lienlot-ent AS INT, INPUT p%lienlot-sor AS INT, INPUT p%datmvt AS DATE,
                                                 INPUT p%codcli AS INT, INPUT p%nomcli AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%typmvt AS CHAR) IN HndGco-Majstk.

/************************************************************************/
/*                        GESTION DES IMMATRICULATIONS                  */
/************************************************************************/
/* Cr�ation d'une sortie � partir d'une entr�e d'immatriculation */
FUNCTION STK_CREATION_SORENT_IMM RETURN LOGICAL (INPUT p%lienimm-ent AS INT, INPUT p%lienimm-sor AS INT, INPUT p%datmvt AS DATE,
                                                 INPUT p%codcli AS INT, INPUT p%nomcli AS CHAR, INPUT p%origmvt AS CHAR, INPUT p%typmvt AS CHAR) IN HndGco-Majstk.


/************************************************************************/
/*                                DIVERS                                */
/************************************************************************/
FUNCTION STK_ENT_PREV_REEL RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienimm AS INT, INPUT p%lienemp AS INT, INPUT p%origmvt AS CHAR) IN HndGco-Majstk.
FUNCTION STK_SOR_PREV_REEL RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienimm AS INT, INPUT p%lienemp AS INT, INPUT p%origmvt AS CHAR) IN HndGco-Majstk.

/* MAJ de la date de mouvement de histolot/histoimm/histoemp/stocklot/stokcimm/stockemp/stockle */
FUNCTION STK_MAJDATMVT_LOTIMMEMP RETURN LOGICAL (INPUT p%lienlot AS INT, INPUT p%lienimm AS INT, INPUT p%lienemp AS INT, INPUT p%datmvt AS DATE) IN HndGco-Majstk.


/*A42270...*//* MAJ Blocage d'un Lot */
FUNCTION STK_MAJBLOC_LOT RETURN LOGICAL (INPUT p%depot AS INTEGER, INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                        INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, 
                                        INPUT p%md5 AS CHARACTER, INPUT p%lot AS CHAR, INPUT p%codBloc AS LOGICAL, INPUT p%cpt AS INTEGER) IN HndGco-Majstk.
/*...A42270*/
/* MAJ DLC d'un Lot */
FUNCTION STK_MAJDLC_LOT RETURN LOGICAL (INPUT p%depot AS INTEGER, INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                        INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, 
                                        INPUT p%k_var AS CHARACTER, INPUT p%md5 AS CHARACTER, INPUT p%lot AS CHAR, INPUT p%nvDLC AS DATE, /*A42270...*/ INPUT p%codBloc AS LOGICAL /*...A42270*/) IN HndGco-Majstk.
/* MAJ ruptures */
FUNCTION MAJ_RUPTURES RETURN LOGICAL (INPUT p%codpro AS INTEGER, INPUT p%coddec1 AS CHARACTER, INPUT p%coddec2 AS CHARACTER,
                                      INPUT p%coddec3 AS CHARACTER, INPUT p%coddec4 AS CHARACTER, INPUT p%coddec5 AS CHARACTER, 
                                      INPUT p%md5 AS CHARACTER, INPUT p%k_var AS CHARACTER, INPUT p%depot AS INTEGER,
                                      INPUT p%datvp AS DATE, INPUT p%hrvp AS INTEGER, p%futurstock AS DECIMAL) IN HndGco-Majstk.

/* Magasin par d�faut */
FUNCTION STK_MAGASIN_DEFAUT RETURN INTEGER () IN HndGco-Majstk.



/* *** */
IF NOT VALID-HANDLE (HndGco-Majstk) THEN DO:
    RUN gco-majstk PERSISTENT SET HndGco-Majstk.
    SetSessionData ("GCO-MAJSTK":U,"CLE":U,STRING(HndGco-Majstk)).
END.

&ENDIF
