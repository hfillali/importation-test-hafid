/* cleval.i
   Cr�� par VR
*/
   
FUNCTION GetCleVal RETURNS CHAR (INPUT pchapitre AS CHAR, INPUT pdomaine AS CHAR, INPUT puser AS CHAR, INPUT psection AS CHAR, INPUT pcle AS CHAR) :
    FIND FIRST cleval where cleval.chapitre = pchapitre and cleval.domaine = pdomaine and cleval.cod_user = puser and cleval.section = psection and cleval.cle = pcle  NO-LOCK NO-ERROR. 
    IF AVAIL cleval THEN
        RETURN cleval.valeur.
    ELSE
        RETURN "".
END function.

FUNCTION SetCleVal RETURNS LOG (INPUT pchapitre AS CHAR, INPUT pdomaine AS CHAR, INPUT puser AS CHAR, INPUT psection AS CHAR, INPUT pcle AS CHAR, INPUT pvaleur AS CHAR) :
    DO TRANSACTION :
        FIND FIRST cleval where cleval.chapitre = pchapitre and cleval.domaine = pdomaine and cleval.cod_user = puser and cleval.section = psection and cleval.cle = pcle  EXCLUSIVE-LOCK NO-ERROR. 
        IF LOCKED(cleval) THEN RETURN NO.
        IF AVAIL cleval THEN
        DO:
            IF pvaleur = ? THEN
                DELETE cleval.
            ELSE
                cleval.valeur = pvaleur.
        END.
        ELSE
        DO:
            IF pvaleur <> ? THEN
            DO:
                CREATE cleval.
                ASSIGN
                    cleval.chapitre = pchapitre 
                    cleval.domaine = pdomaine 
                    cleval.cod_user = puser 
                    cleval.section = psection 
                    cleval.cle = pcle
                    cleval.valeur = pvaleur.
            END.
        END.
    END.
    FIND CURRENT cleval NO-LOCK NO-ERROR. 
    RETURN YES.
END function.
