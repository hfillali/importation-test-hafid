&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bcreer 102

{Hgcoergo.i}
{hfileopen.i}
  
DEFINE TEMP-TABLE tps
FIELD n_produit AS INT
FIELD qte AS INT
FIELD prix AS DEC
FIELD tot AS DEC.
DEFINE VARIABLE som AS INTEGER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME br$1

/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 f$d f$c b$-3 f$a f$p b$2 f$q ~
f$pri b$1 br$1 
&Scoped-Define DISPLAYED-OBJECTS f$d f$c f$a f$p f$q f$pri 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON b$-3 
     LABEL "" 
     SIZE 4 BY 1.13.

DEFINE BUTTON b$1 
     LABEL "ajouter" 
     SIZE 12 BY 1.13.

DEFINE BUTTON b$2 
     LABEL "" 
     SIZE 4 BY 1.13.

DEFINE VARIABLE f$a AS CHARACTER FORMAT "X(256)":U 
     LABEL "Adresse" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE f$c AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Client" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE f$d AS DATE FORMAT "99/99/99":U 
     LABEL "Date facture" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE f$p AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� produit" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE f$pri AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Prix de vente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE f$q AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Qte" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 94 BY 8.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 94 BY 7.5.
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br$1 FOR 
      tps SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br$1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br$1 C-Win _FREEFORM
 QUERY br$1 DISPLAY
      tps.n_produit FORMAT ">>>>>9"     COLUMN-LABEL "N�produit" WIDTH 15
      tps.qte      FORMAT ">>>>>9"      COLUMN-LABEL "Qte"       WIDTH 15
      tps.prix     FORMAT ">>>>>9"      COLUMN-LABEL "Prix"      WIDTH 15
    tps.tot     FORMAT ">>>>>9"      COLUMN-LABEL    "TOTAL produit"      WIDTH 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 94 BY 7.25 ROW-HEIGHT-CHARS 1.17 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     f$d AT ROW 5 COL 19 COLON-ALIGNED WIDGET-ID 6
     f$c AT ROW 7 COL 19 COLON-ALIGNED WIDGET-ID 8
     b$-3 AT ROW 7 COL 47 WIDGET-ID 32
     f$a AT ROW 9 COL 19 COLON-ALIGNED WIDGET-ID 10
     f$p AT ROW 13 COL 19 COLON-ALIGNED WIDGET-ID 14
     b$2 AT ROW 13 COL 47 WIDGET-ID 30
     f$q AT ROW 15 COL 19 COLON-ALIGNED WIDGET-ID 16
     f$pri AT ROW 17 COL 19 COLON-ALIGNED WIDGET-ID 18
     b$1 AT ROW 17 COL 70 WIDGET-ID 22
     br$1 AT ROW 20.25 COL 6 WIDGET-ID 200
     "  Liste des produits" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 19 COL 9 WIDGET-ID 28
          FGCOLOR 1 
     "   Ent�te facture" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 3 COL 13 WIDGET-ID 4
          FGCOLOR 1 
     "  Ligne de produit" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 10.75 COL 13 WIDGET-ID 24
          FGCOLOR 1 
     RECT-1 AT ROW 3.5 COL 6 WIDGET-ID 2
     RECT-2 AT ROW 11.5 COL 6 WIDGET-ID 12
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 101.57 BY 27.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "gestion des facteurs"
         HEIGHT             = 27.29
         WIDTH              = 101.57
         MAX-HEIGHT         = 32.08
         MAX-WIDTH          = 149.43
         VIRTUAL-HEIGHT     = 32.08
         VIRTUAL-WIDTH      = 149.43
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB br$1 b$1 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* gestion des facteurs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* gestion des facteurs */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$-3 C-Win
ON CHOOSE OF b$-3 IN FRAME DEFAULT-FRAME
DO:
  DEFINE VARIABLE cod AS INT  NO-UNDO.
  DEFINE VARIABLE rid AS ROWID      NO-UNDO.
  RUN dynrechex("recherche client", /*Nom fenetre*/
                "GCO.client", /*la table*/
                "", /*Where*/
                "cod_cli�CODE client|nom_cli�nom client", /*colonnnes affich�s*/
                "cod_cli", /*Champs a recuperer*/
                OUTPUT cod, OUTPUT rid,
                "PLEINECRAN", "", ? ).
  f$c:SCREEN-VALUE = STRING (cod).
       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$1 C-Win
ON CHOOSE OF b$1 IN FRAME DEFAULT-FRAME /* ajouter */
DO:
    ASSIGN f$p f$q f$pri.
  CREATE tps.
  
      ASSIGN 
      tps.n_produit = f$p
      tps.qte = f$q
      tps.prix = f$pri
      tps.tot = INT (f$pri) * INT (f$q).
      OPEN QUERY br$1 FOR EACH tps.
           f$p:SCREEN-VALUE = "".
        f$q:SCREEN-VALUE = "".
        f$pri:SCREEN-VALUE = "".
       
        
        

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$2 C-Win
ON CHOOSE OF b$2 IN FRAME DEFAULT-FRAME
DO:
  DEFINE VARIABLE cod AS INT  NO-UNDO.
  DEFINE VARIABLE rid AS ROWID      NO-UNDO.
  RUN dynrechex("recherche produit", /*Nom fenetre*/
                "GCO.produit", /*la table*/
                "", /*Where*/
                "cod_pro�CODE produit|nom_pro�nom produit", /*colonnnes affich�s*/
                "cod_pro", /*Champs a recuperer*/
                OUTPUT cod, OUTPUT rid,
                "PLEINECRAN", "", ? ).
  f$p:SCREEN-VALUE = STRING (cod).
       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br$1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.


ON f6 OF FRAME default-frame ANYWHERE
    RUN choose-bcreer.

ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcreer C-Win 
PROCEDURE choose-bcreer :
DO WITH FRAME DEFAULT-FRAME : 
  /*  poursuivre le trvaille d ici */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 
    CBLoadIcon(hcombars-f1, {&bcreer},StdMedia + "\StdMedia.icl",36).


    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcreer}, Translate ('&Creer' + ' (F6)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bcreer}, {&xtpButtonIconAndCaption}).
 
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bcreer} THEN RUN choose-bcreer.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY f$d f$c f$a f$p f$q f$pri 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 f$d f$c b$-3 f$a f$p b$2 f$q f$pri b$1 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win 
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win 
PROCEDURE proc-init :
DO WITH FRAME default-frame:
     ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
        RUN getini.


        RUN combarscreation.


    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win 
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

