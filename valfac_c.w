&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r11 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME W1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W1 
/*------------------------------------------------------------------------

  File: VALFAC_C
  Description: Validation des factures
  Author: HL
  Created: 16/09/97
$34 HL 19/03/99 pas-droit
$645 Hl 11/10/00 Batchage des factures avec temp-table partag�e
$698 JL 02/03/01 Ajout contr�le afin de ne pas pouvoir �diter une facture qui n'a pas de ligne
$791 FM 25/04/02 V�rification droits utilisateur avant �dition des factures
$1070 HL 18/08/05 Multi-Soci�t� (Rechercher : parvs, n_d, parsoc.mult_soc)
$ERGOJL 17/01/07
$1383 MT 22/10/07 Editer le bon interne suite � l'�dition de facture

DEFINE QUERY brcli FOR 
      entetcli 
            FIELDS(
      entetcli.adr_fac
      entetcli.dat_cde
      entetcli.ref_cde
      entetcli.dat_liv
      entetcli.no_fact
      entetcli.fac_maj
      entetcli.fac_edi
      entetcli.dat_fac
      entetcli.no_cde
      ) SCROLLING.
      
$DOL1 NR 23/05/06 Impossible de valider une facture avec la cat�gorie 18
$DOL2 NR 23/11/06 Ne pas quitter le programme apres validation
===> $V41 ABR : migration de V34 vers V41 du 28/10/2008
$DOL3 non report�
$DOL4 NR 05/05/10 N'autoriser qu'un seul utilisateur � la fois 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def shared var g-user as char.
def shared var g-ndos as i.
{hGco-Api.DEF}

DEF VAR n_d AS INT no-undo. /* N� Soci�t� */

def var droit-marge as log no-undo.
def var pas-droit as log no-undo.
def var x as i no-undo.
def var y as i no-undo.
DEF VAR LOGBID AS LOG no-undo.
def var listfac as char no-undo.
def var regr as log no-undo.
DEF VAR nompgm  AS C INIT "EDTFAC" no-undo.

def buffer entetcli2 for entetcli.
DEF VAR i AS INT no-undo.
{tmp-fac.i}

DEF VAR MODEDIT  AS INT INIT 1 no-undo.
DEF VAR ListeServeur AS CHAR no-undo.
DEF VAR vsoumis AS CHAR no-undo.
DEF VAR parm-edt AS CHAR no-undo.

DEF VAR chaine AS CHAR no-undo.
DEF VAR prolink AS CHAR no-undo.
DEF VAR chPlanif AS CHAR NO-UNDO.
ProLink = GetSessionData("Config!", "ProLink") .

DEF VAR g-user-droit AS CHAR no-undo.

/*$1383...*/
DEF VAR wrecap AS CHAR NO-UNDO.
DEF VAR wdetail AS LOG NO-UNDO.
DEF VAR wdetail-texte AS LOG NO-UNDO.
DEF VAR wdescente AS LOG NO-UNDO.
DEF VAR wnote AS LOG NO-UNDO.
DEF VAR wentete AS LOG NO-UNDO.
DEF VAR wpied AS LOG NO-UNDO.
DEF VAR wvalo AS CHAR NO-UNDO.
DEF VAR wheure AS CHAR NO-UNDO.
DEF VAR wrecmo AS LOG NO-UNDO.
DEF VAR wnivo AS INT NO-UNDO.
DEF VAR wref AS CHAR NO-UNDO.
DEF VAR wvar AS LOG NO-UNDO.
DEF VAR wopt AS LOG NO-UNDO.
/*...$1383*/

{hcombars.i}
{Hgcoergo.i}
{G-HprowinBase.i}

DEF VAR FrmComBars-f1  AS HANDLE NO-UNDO.
DEF VAR hComBars-f1    AS INT NO-UNDO.
&SCOPED-DEFINE Bquitter   101
&SCOPED-DEFINE Bvalider   102
&SCOPED-DEFINE Bediter    103
&SCOPED-DEFINE bcontroler 104
&SCOPED-DEFINE bctrlf12   105
&SCOPED-DEFINE Baffenc    106

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f1
&Scoped-define BROWSE-NAME brcli

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES entetcli

/* Definitions for BROWSE brcli                                         */
&Scoped-define FIELDS-IN-QUERY-brcli entetcli.no_fact entetcli.dat_fac entetcli.no_cde entetcli.ref_cde entetcli.dat_cde entetcli.dat_liv entetcli.adr_fac[1] entetcli.commerc[1]   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brcli   
&Scoped-define SELF-NAME brcli
&Scoped-define OPEN-QUERY-brcli IF parsoc.ges_cai AND tbcaisse:CHECKED = NO THEN DO:     if fact$=0 and ffact$=0 then     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact<>0 and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK.     else if ffact$=0 then     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no and entetcli.no_fact>=fact$ and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK.     else     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>0 and entetcli.no_fact>=fact$ and entetcli.no_fact<=ffact$ and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK. END. ELSE DO:     if fact$=0 and ffact$=0 then     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact<>0 and entetcli.fac_edi=yes NO-LOCK.     else if ffact$=0 then     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>=fact$ and entetcli.fac_edi=YES NO-LOCK.     else     OPEN QUERY {&SELF-NAME} FOR EACH entetcli           WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>0 and entetcli.no_fact>=fact$ and entetcli.no_fact<=ffact$ and entetcli.fac_edi=YES NO-LOCK. END.
&Scoped-define TABLES-IN-QUERY-brcli entetcli
&Scoped-define FIRST-TABLE-IN-QUERY-brcli entetcli


/* Definitions for BROWSE brfac                                         */
&Scoped-define FIELDS-IN-QUERY-brfac entetcli.typ_sai entetcli.no_cde entetcli.no_bl entetcli.dat_cde entetcli.ref_cde entetcli.dat_liv entetcli.sta_pce entetcli.blocage entetcli.adr_fac[1] entetcli.achev entetcli.mt_ht entetcli.mt_cde entetcli.informat entetcli.qui entetcli.depot entetcli.nat_cde entetcli.not_ref entetcli.affaire entetcli.typ_cde "Cr��e le " + string(entetcli.dat_crt) + " par " + entetcli.usr_crt "Modifi� le " + string(entetcli.dat_mod) + " par " + entetcli.usr_mod   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brfac   
&Scoped-define SELF-NAME brfac
&Scoped-define QUERY-STRING-brfac FOR EACH entetcli WHERE     entetcli.ndos=n_d     AND entetcli.fac_maj=NO     AND entetcli.no_fact<>0     and entetcli.fac_edi=no NO-LOCK by entetcli.dat_fac
&Scoped-define OPEN-QUERY-brfac OPEN QUERY {&SELF-NAME} FOR EACH entetcli WHERE     entetcli.ndos=n_d     AND entetcli.fac_maj=NO     AND entetcli.no_fact<>0     and entetcli.fac_edi=no NO-LOCK by entetcli.dat_fac.
&Scoped-define TABLES-IN-QUERY-brfac entetcli
&Scoped-define FIRST-TABLE-IN-QUERY-brfac entetcli


/* Definitions for FRAME f1                                             */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f1 ~
    ~{&OPEN-QUERY-brcli}~
    ~{&OPEN-QUERY-brfac}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bplan RECT-25 RECT-26 RECT-10 brfac brcli ~
fact$ ffact$ soumis serveur tbcaisse Ft$selection-1 pbfact aucune-commande 
&Scoped-Define DISPLAYED-OBJECTS fact$ ffact$ soumis serveur tbcaisse ~
Ft$selection-1 pbfact aucune-commande 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W1 AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bplan 
     IMAGE-UP FILE "planning2.bmp":U NO-FOCUS FLAT-BUTTON
     LABEL "Button 1" 
     SIZE 3.43 BY 1.04 TOOLTIP "Planifier l'�x�cution du travail".

DEFINE VARIABLE serveur AS CHARACTER FORMAT "X(25)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 13.14 BY 1
     BGCOLOR 16 FGCOLOR 23 FONT 49 NO-UNDO.

DEFINE VARIABLE aucune-commande AS CHARACTER FORMAT "X(40)":U INITIAL "Auncun r�sultat" 
      VIEW-AS TEXT 
     SIZE 18.72 BY 1.21
     FGCOLOR 49 FONT 29 NO-UNDO.

DEFINE VARIABLE fact$ AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Du n�" 
     VIEW-AS FILL-IN 
     SIZE 9.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE ffact$ AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "au n�" 
     VIEW-AS FILL-IN 
     SIZE 9.72 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE Ft$selection-1 AS CHARACTER FORMAT "X(256)":U INITIAL "S�lection" 
      VIEW-AS TEXT 
     SIZE 8.72 BY .58
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE pbfact AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 50.43 BY .63
     BGCOLOR 12 FGCOLOR 15  NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.14 BY 1.08.

DEFINE RECTANGLE RECT-25
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 113.14 BY 8.63.

DEFINE RECTANGLE RECT-26
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 113.14 BY 11.04.

DEFINE VARIABLE soumis AS LOGICAL INITIAL no 
     LABEL "Soumettre travail" 
     VIEW-AS TOGGLE-BOX
     SIZE 16.57 BY .63 TOOLTIP "Fait tourner le travail sur un autre poste et donc ne bloque pas votre poste" NO-UNDO.

DEFINE VARIABLE tbcaisse AS LOGICAL INITIAL no 
     LABEL "+ Ventes Caisse" 
     VIEW-AS TOGGLE-BOX
     SIZE 16.14 BY .63 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brcli FOR 
      entetcli SCROLLING.

DEFINE QUERY brfac FOR 
      entetcli SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brcli
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brcli W1 _FREEFORM
  QUERY brcli DISPLAY
      entetcli.no_fact  COLUMN-LABEL "N� Facture"
      entetcli.dat_fac COLUMN-LABEL "Date Fac"
      entetcli.no_cde COLUMN-LABEL "N� Cde"
      entetcli.ref_cde COLUMN-LABEL "R�f�rence"
      entetcli.dat_cde COLUMN-LABEL "Date Cde"
      entetcli.dat_liv COLUMN-LABEL "Date Liv"
      entetcli.adr_fac[1] COLUMN-LABEL "Client"
      entetcli.commerc[1] COLUMN-LABEL "Commercial"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 94.86 BY 10.75
         BGCOLOR 48 FONT 49 ROW-HEIGHT-CHARS .54 FIT-LAST-COLUMN.

DEFINE BROWSE brfac
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brfac W1 _FREEFORM
  QUERY brfac DISPLAY
      entetcli.typ_sai COLUMN-LABEL "T"
      entetcli.no_cde COLUMN-LABEL "N� Cde"
      entetcli.no_bl FORMAT ">>>>>>>>>"
      entetcli.dat_cde COLUMN-LABEL "Date Cde"
      entetcli.ref_cde COLUMN-LABEL "R�f�rence"
      entetcli.dat_liv COLUMN-LABEL "Date Liv."
      entetcli.sta_pce COLUMN-LABEL "Commentaire" FORMAT "x(36)"
      entetcli.blocage COLUMN-LABEL "Bloq(F4)"
      entetcli.adr_fac[1] COLUMN-LABEL "Client"
      entetcli.achev FORMAT "�/":U COLUMN-FONT 47 COLUMN-LABEL "Achev�e"
      entetcli.mt_ht COLUMN-LABEL "HT" FORMAT "->>>>>>>>9.99"
      entetcli.mt_cde COLUMN-LABEL "TTC" FORMAT "->>>>>>>>9.99"
      entetcli.informat
      entetcli.qui COLUMN-LABEL "Qui"
      entetcli.depot COLUMN-LABEL "D�p�t"
      entetcli.nat_cde COLUMN-LABEL "Nat"
      entetcli.not_ref
      entetcli.affaire COLUMN-LABEL "Code Affaire"
      entetcli.typ_cde COLUMN-LABEL "Y"
      "Cr��e le " + string(entetcli.dat_crt) + " par " + entetcli.usr_crt COLUMN-LABEL "Cr�ation" FORMAT "x(34)"
      "Modifi� le " + string(entetcli.dat_mod) + " par " + entetcli.usr_mod COLUMN-LABEL "Modification" FORMAT "x(34)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 111.72 BY 7.17
         BGCOLOR 48 FONT 49 ROW-HEIGHT-CHARS .54 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f1
     bplan AT ROW 18.04 COL 15.14
     brfac AT ROW 3.67 COL 2
     brcli AT ROW 11.25 COL 18.86
     fact$ AT ROW 11.33 COL 6.57 COLON-ALIGNED
     ffact$ AT ROW 12.25 COL 6.57 COLON-ALIGNED
     soumis AT ROW 17.42 COL 2
     serveur AT ROW 18.08 COL 2 NO-LABEL
     tbcaisse AT ROW 19.13 COL 2
     Ft$selection-1 AT ROW 2.21 COL 1.72 NO-LABEL WIDGET-ID 20
     pbfact AT ROW 2.96 COL 2.29 NO-LABEL
     aucune-commande AT ROW 12 COL 47 NO-LABEL
     "factures clients." VIEW-AS TEXT
          SIZE 16.14 BY .63 AT ROW 15.88 COL 2.29
          BGCOLOR 15 FGCOLOR 12 
     "ATTENTION !!!" VIEW-AS TEXT
          SIZE 16.14 BY .63 AT ROW 13.88 COL 2.29
          BGCOLOR 15 FGCOLOR 12 
     "r��dite pas les" VIEW-AS TEXT
          SIZE 16.14 BY .63 AT ROW 15.21 COL 2.29
          BGCOLOR 15 FGCOLOR 12 
     "Ce traitement ne" VIEW-AS TEXT
          SIZE 16.14 BY .63 AT ROW 14.54 COL 2.29
          BGCOLOR 15 FGCOLOR 12 
     RECT-25 AT ROW 2.46 COL 1.14
     RECT-26 AT ROW 11.08 COL 1.14
     RECT-10 AT ROW 1.04 COL 1.14 WIDGET-ID 4
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 113.43 BY 21.17
         FONT 49.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W1 ASSIGN
         HIDDEN             = YES
         TITLE              = "Validation des Factures Clients"
         COLUMN             = 1
         ROW                = 2.63
         HEIGHT             = 21.17
         WIDTH              = 113.43
         MAX-HEIGHT         = 21.17
         MAX-WIDTH          = 113.43
         VIRTUAL-HEIGHT     = 21.17
         VIRTUAL-WIDTH      = 113.43
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         PRIVATE-DATA       = "98"
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         FONT               = 49
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT W1:LOAD-ICON("PROGIWIN":U) THEN
    MESSAGE "Unable to load icon: PROGIWIN"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W1
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f1
   NOT-VISIBLE FRAME-NAME                                               */
/* BROWSE-TAB brfac RECT-10 f1 */
/* BROWSE-TAB brcli brfac f1 */
/* SETTINGS FOR FILL-IN aucune-commande IN FRAME f1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Ft$selection-1 IN FRAME f1
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN pbfact IN FRAME f1
   ALIGN-L                                                              */
/* SETTINGS FOR COMBO-BOX serveur IN FRAME f1
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W1)
THEN W1:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brcli
/* Query rebuild information for BROWSE brcli
     _START_FREEFORM
IF parsoc.ges_cai AND tbcaisse:CHECKED = NO THEN DO:
    if fact$=0 and ffact$=0 then
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact<>0 and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK.
    else if ffact$=0 then
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no and entetcli.no_fact>=fact$ and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK.
    else
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>0 and entetcli.no_fact>=fact$ and entetcli.no_fact<=ffact$ and entetcli.fac_edi=yes AND entetcli.ticket = 0 NO-LOCK.
END.
ELSE DO:
    if fact$=0 and ffact$=0 then
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact<>0 and entetcli.fac_edi=yes NO-LOCK.
    else if ffact$=0 then
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>=fact$ and entetcli.fac_edi=YES NO-LOCK.
    else
    OPEN QUERY {&SELF-NAME} FOR EACH entetcli
          WHERE entetcli.ndos=n_d and entetcli.fac_maj=no AND entetcli.no_fact>0 and entetcli.no_fact>=fact$ and entetcli.no_fact<=ffact$ and entetcli.fac_edi=YES NO-LOCK.
END.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brcli */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brfac
/* Query rebuild information for BROWSE brfac
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH entetcli WHERE
    entetcli.ndos=n_d
    AND entetcli.fac_maj=NO
    AND entetcli.no_fact<>0
    and entetcli.fac_edi=no NO-LOCK by entetcli.dat_fac.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brfac */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f1
/* Query rebuild information for FRAME f1
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME f1 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME brcli
&Scoped-define SELF-NAME brcli
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brcli W1
ON VALUE-CHANGED OF brcli IN FRAME f1
DO:
    if brcli:num-selected-rows=0 and fact$=0 and ffact$=0 then 
        CBSetItemCaption (Hcombars-f1, 2, {&Bvalider}, Translate("&Valider TOUTES les factures") + " (F9)").
    else if brcli:num-selected-rows=0 then 
        CBSetItemCaption (Hcombars-f1, 2, {&Bvalider}, Translate("&Valider la plage de factures") + " (F9)").
    else if brcli:num-selected-rows=1 then 
        CBSetItemCaption (Hcombars-f1, 2, {&Bvalider}, Translate("&Valider facture n� ") + string(entetcli.no_fact) + " (F9)").
    else 
        CBSetItemCaption (Hcombars-f1, 2, {&Bvalider}, Translate("&Valider ") + string(brcli:num-selected-rows,">>>9") + translate(" lignes s�lectionn�es") + " (F9)").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fact$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fact$ W1
ON LEAVE OF fact$ IN FRAME f1 /* Du n� */
OR RETURN OF fact$
DO:
if input fact$<>fact$ or (last-event:function="return" and input fact$=0) then do:
      assign fact$ ffact$ logbid=session:set-wait-state("general").
      RUN openquery.
      logbid=session:set-wait-state("").
      apply "tab" to fact$.
      return no-apply.
end.
else if last-event:function="return" and input fact$<>0 then do:
    apply "tab" to fact$.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ffact$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ffact$ W1
ON LEAVE OF ffact$ IN FRAME f1 /* au n� */
OR RETURN OF ffact$
DO:
if input ffact$<>ffact$ or (last-event:function="return" and input ffact$=0) then do:
      assign fact$ ffact$ logbid=session:set-wait-state("general").
      RUN openquery.
      logbid=session:set-wait-state("").
      apply "tab" to ffact$.
      return no-apply.
end.
else if last-event:function="return" and input ffact$<>0 then do:
    apply "tab" to ffact$.
    return no-apply.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tbcaisse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbcaisse W1
ON VALUE-CHANGED OF tbcaisse IN FRAME f1 /* + Ventes Caisse */
DO:
    RUN openquery.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W1 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
       LOGBID = SESSION:SET-WAIT-STATE("GENERAL").

ON Alt-Cursor-Up OF frame F1       ANYWHERE logbid=CBFocusItem(hcombars-f1       , 2, {&Bquitter}).

{reord_o.i}
{g-hmenu.i}
FUNCTION GetServers RETURNS CHAR (INPUT CHAR) IN g-hmenu.
FUNCTION SetJobChar RETURNS CHAR (INPUT ptype AS CHAR):
    DEF VAR pret## AS CHAR NO-UNDO INIT ?.
    CASE ptype:
        WHEN "PARAM" THEN pret##=parm-edt.
        WHEN "SERVEUR" THEN pret##=serveur:SCREEN-VALUE IN FRAME f1.
    END CASE.
    RETURN pret##.
END FUNCTION.
FUNCTION SetJobBool RETURNS LOG (INPUT ptype AS CHAR):
    DEF VAR pret## AS LOG NO-UNDO INIT ?.
    CASE ptype:
        WHEN "BATCH" THEN pret##=(IF ListeServeur="" OR soumis:SCREEN-VAL IN FRAME f1="no" THEN NO ELSE YES).
        WHEN "NOEDIT" THEN pret##=YES.
    END CASE.
    RETURN pret##.
END FUNCTION.

ON f11 OF FRAME f1 ANYWHERE DO:
    IF brcli:NUM-SELECTED-ROWS = 1 AND AVAIL entetcli THEN RUN affenc_c(ROWID(entetcli)).
END.

ON f1 ANYWHERE SYSTEM-HELP GetSessionData("Config!", "BIN") + "\pgi_ventes.HLP" CONTEXT INT({&WINDOW-NAME}:PRIVATE-DATA).
ON CHOOSE OF bplan IN FRAME F1 /* Button 1 */
DO:
    DEF VAR TDate AS DATE NO-UNDO.
    Chaine = chPlanif.
    RUN VALUE(ProLink + "\PlanifJob") (INPUT-O Chaine, OUTPUT TDate).
    IF TDate <> ? THEN chPlanif = ENTRY(1, Chaine, "|") + (IF Chaine = "" THEN "" ELSE "||" + STRING(TDate, "99/99/9999")).
    IF Chaine = ""
      THEN ASSIGN LogBid = bPlan:LOAD-IMAGE(ProLink + "\Planning2.bmp":U)
                  bPlan:TOOLTIP = "Planifier l'ex�cution du travail ...".
      ELSE ASSIGN LogBid = bPlan:LOAD-IMAGE(ProLink + "\Planning.bmp":U)
                  bPlan:TOOLTIP = "Travail planifi�~n" + (IF Chaine BEGINS "1" THEN "E" ELSE "Prochaine e") + "x�cution : le " + STRING(TDate, "99/99/9999").
    SetSessionData("RunJob", "Chaine de planification", Chaine).
END.
ON VALUE-CHANGED OF soumis IN FRAME f1
    ASSIGN serveur:HIDDEN=(SELF:SCREEN-VAL="no") bplan:HIDDEN=(SELF:SCREEN-VAL="no").

/* Triggers Return --> tabulation */
ON RETURN OF W1 ANYWHERE DO:
if can-do("fill-in,toggle-box,radio-set,combo-box",self:type) then do:
  apply "tab" to self.
  return no-apply.
end.
else if self:type="editor" then logbid=self:insert-string("~n").
else if self:type="button" then apply "choose" to self.
else if can-do("browse,selection-list",self:type) then apply "default-action" to self.
END.

ON F9 OF FRAME F1 ANYWHERE do:
    IF CBGetItemEnableState (Hcombars-f1, 2, {&Bvalider}) THEN RUN CHOOSE-BVALIDER.
END.

/****************** PGM PRINCIPAL **********************/

ASSIGN {&WINDOW-NAME}:hidden = YES frame f1:hidden = YES.

ON WINDOW-CLOSE, END-ERROR, ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
    APPLY "CLOSE":U TO THIS-PROCEDURE.
    RETURN NO-APPLY.
END.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE DO:

    /*$DOL4...*/
    DO TRANSACTION:
        FIND FIRST fctcstgp where fctcstgp.typ_cst = "C" 
                              and fctcstgp.nom_cst = "VALFAC" EXCLUSIVE-LOCK NO-ERROR. 
        IF AVAIL fctcstgp THEN ASSIGN fctcstgp.valeur  = "".
    END.             
    RELEASE fctcstgp.
    /*...$DOL4*/

    IF VALID-HANDLE(HndGcoErg) THEN APPLY "CLOSE" TO HndGcoerg.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE({&WINDOW-NAME}) THEN DELETE WIDGET {&WINDOW-NAME}.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

    /*$DOL4...*/
    DO TRANSACTION:
        FIND FIRST fctcstgp where fctcstgp.typ_cst = "C" 
                              and fctcstgp.nom_cst = "VALFAC" EXCLUSIVE-LOCK NO-ERROR. 
        IF NOT AVAIL fctcstgp THEN DO:
            CREATE fctcstgp.
            ASSIGN fctcstgp.comment = "Verouillage validation facture"
                   fctcstgp.domaine = "POUBELLLE"
                   fctcstgp.nom_cst = "VALFAC"
                   fctcstgp.typ_cst = "C"
                   fctcstgp.typ_data= "CA".
        END.
    END.
    RELEASE fctcstgp.
    /*...$DOL4*/

    RUN proc-init.
    RUN RESIZEWINDOW.
    view {&WINDOW-NAME}.
    LOGBID = SESSION:SET-WAIT-STATE("").
    APPLY "ENTRY" TO fact$.

  /*$DOL4...*/
    FIND FIRST fctcstgp where fctcstgp.typ_cst = "C" 
                              and fctcstgp.nom_cst = "VALFAC" NO-LOCK NO-ERROR. 
  IF AVAIL fctcstgp AND fctcstgp.valeur <> "" THEN DO:
      MESSAGE "Lancement du programme impossible" SKIP
              "D�j� lanc� par" g-user
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      pas-droit = YES.
  END. 
  ELSE /*...$DOL4*/ if pas-droit then message "Vous n'�tes pas autoris� � g�rer les FACTURES !" view-as alert-box info.
  else IF NOT THIS-PROCEDURE:PERSISTENT THEN WAIT-FOR CLOSE OF THIS-PROCEDURE.

  if pas-droit then do:
    IF VALID-HANDLE({&WINDOW-NAME}) THEN DELETE WIDGET {&WINDOW-NAME}.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
  end.
  ELSE DO:
      /*$DOL4...*/
    DO TRANSACTION:
        FIND FIRST fctcstgp where fctcstgp.typ_cst = "C" 
                              and fctcstgp.nom_cst = "VALFAC" EXCLUSIVE-LOCK NO-ERROR. 
        IF AVAIL fctcstgp THEN ASSIGN fctcstgp.valeur  = g-user.
    END.
    RELEASE fctcstgp.
    /*...$DOL4*/
  END.



END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-bcontroler W1 
PROCEDURE CHOOSE-bcontroler :
DO WITH FRAME F1:
    if brcli:num-selected-rows=0 then run intpocw (yes,"").
    else do:
      listfac="".
      DO X = 1 TO BRCLI:NUM-SELECTED-ROWS in frame f1:
        logbid = brcli:fetch-selected-row(x).
        LISTFAC = LISTFAC + STRING(rowid(entetcli)) + ",".
      END.
      listfac=trim(listfac,",").
      run intpocw (yes,listfac).
    end.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bediter W1 
PROCEDURE CHOOSE-Bediter :
DO WITH FRAME F1:
    /* Editer facture */
    if brfac:num-selected-rows > 0 then DO:
        logbid=session:set-wait-state("general").
        logbid=brfac:fetch-selected-row(1).
        /* Contr�le */
        if not entetcli.achev and entetcli.usr_mod<>g-user then 
            message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
              "Accessible par " entetcli.usr_mod "." view-as alert-box info.
        else if parsoc.val_fac and entetcli.fac_maj then
            message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
              "La facture " string(entetcli.no_fact) " a d�j� �t� valid�e." view-as alert-box info.
        else if not parsoc.val_fac and entetcli.no_fact<>0 then
            message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
              "La facture " string(entetcli.no_fact) " a d�j� �t� �dit�e." view-as alert-box info.
        else do:
            if entetcli.blocage<>"" then do:
              find tabgco where tabgco.type_tab="BC" and tabgco.a_tab=entetcli.blocage no-lock no-error.
              if available tabgco and typ_int[3] then do:
                  message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
                    "Cette commande est bloqu�e pour l'�dition de facture." view-as alert-box info.
                  return no-apply.
              end.
            end.
            /* Impossible si facture regroup�e et validation de factures */
            if entetcli.type_fac<>"CO" and parsoc.val_fac and entetcli.no_fact<>0 then do:
                find first entetcli2 where entetcli2.ndos=entetcli.ndos AND entetcli2.fac_maj=no and entetcli2.no_fact=entetcli.no_fact
                  and (entetcli2.no_cde<>entetcli.no_cde or entetcli2.no_bl<>entetcli.no_bl) no-lock no-error.
                if available entetcli2 then do:
                    message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
                      "La facture " string(entetcli.no_fact) " est de type REGROUP�E. Vous devez passer par l'�dition de factures !" view-as alert-box info.
                    return no-apply.
                end.
            end.
            /* Commande compl�te */
            IF ENTETcli.TYP_SAI="F" THEN DO:
                FIND FIRST CLIENT WHERE CLIENT.COD_CLI=ENTETcli.COD_CLI NO-LOCK NO-ERROR.
                IF AVAILABLE CLIENT AND CLIENT.CDE_CPL and 
                  can-find(first lignecli where lignecli.cod_cli=entetcli.cod_cli and lignecli.typ_sai="C" and lignecli.no_cde=entetcli.no_cde no-lock) then do:
                    message "Edition facture pour commande " string(entetcli.no_cde) " impossible." skip
                      "Client en facturation commande compl�te (reste des reliquats) !" view-as alert-box info.
                    return no-apply.
                end.
            END.
            
            IF NOT CAN-FIND(FIRST lignecli OF entetcli NO-LOCK) THEN /* $698 */
                MESSAGE "Cette facture ne contient aucune ligne !~n" /* $698 */
                        "Edition impossible !" VIEW-AS ALERT-BOX ERROR. /* $698 */
            ELSE DO:
                IF NOT CAN-FIND(droigco WHERE droigco.util=g-user-droit AND droigco.fonc="FACTURE") THEN DO:
                    run edt1fa_c (rowid(entetcli),"",output logbid).
                    /*$1383...*/
                     IF logbid THEN
                     DO:
                         FIND FIRST parcde WHERE parcde.typ_fich = "C" NO-LOCK NO-ERROR.
                         IF AVAIL parcde AND parcde.gst_zone[36] AND droit-marge THEN DO:
                             IF loadsettings("EDTBOI", OUTPUT hsettingsapi) THEN DO:
                                ASSIGN  wrecap = GetSetting("Type �dition","D") 
                                        wdetail = (GetSetting("D�tail Produit","") = "O")
                                        wdetail-texte = (GetSetting("Texte Produit","") = "O")
                                        wdescente = (GetSetting("Descente","") = "yes")
                                        wnote = (GetSetting("Note interne","") = "O") 
                                        wentete = (GetSetting("Texte en-t�te","") = "O")
                                        wpied = (GetSetting("Texte pied","") = "O")
                                        wvalo = GetSetting("Valorisation",parsoc.valo_px)
                                        wheure = GetSetting("Heures","C")
                                        wrecmo = (GetSetting("R�cap MO","") = "O")
                                        wnivo = INT(GetSetting("Niveau MO","1"))
                                        wref = GetSetting("Preference","C")
                                        wvar = (GetSetting("Variante","") = "O")
                                        wopt = (GetSetting("Option","") = "O") .
                                UnloadSettings(hSettingsApi).
                            END.
                             ASSIGN PARM-EDT = STRING(entetcli.cod_cli, "999999") +  /*  1 �  6 */
                                               STRING(entetcli.typ_sai, "X") +  /*  7 �  7 */
                                               STRING(entetcli.NO_cde,  "999999999") +  /*  8 � 16 */
                                               STRING(entetcli.NO_bl,   "999999999") +  /* 17 � 25 */
                                               STRING(wrecap,           "X") +  /* 26 � 26 */
                                               STRING(wdetail,          "O/N") +  /* 27 � 27 */
                                               STRING(wdetail-texte,    "O/N") +  /* 28 � 28 */
                                               STRING(wnote,            "O/N") +  /* 29 � 29 */
                                               STRING(wentete,          "O/N") +  /* 30 � 30 */
                                               STRING(wpied,            "O/N") +  /* 31 � 31 */
                                               STRING(wvalo,            "X") +  /* 32 � 32 */
                                               STRING(wheure,           "X") +  /* 33 � 33 */
                                               STRING(wRECMO,           "O/N") +  /* 34 � 34 */
                                               STRING(wNIVO ,           "9") +  /* 35 � 35 */
                                               STRING(wREF,             "X") +
                                               STRING(today,            "99/99/99") +
                                               STRING(wvar,             "O/N") +
                                               STRING(wopt,             "O/N").
                             RUN RunJob ("EDTBOI", PARM-Edt, OUTPUT Modedit).
                             IF wdescente THEN RUN edtboinm_c(entetcli.cod_cli,entetcli.typ_sai,entetcli.NO_cde,entetcli.NO_bl).
                         END.
                     END.
                     /*...$1383*/    
                    if logbid then run openquery.
                END.
                ELSE MESSAGE "Vous n'�tes pas autoris� � �diter les factures." VIEW-AS ALERT-BOX INFO BUTTONS OK.
            END.
        end.
        logbid=session:set-wait-state("").
    end.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bquitter W1 
PROCEDURE CHOOSE-Bquitter :
/* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bvalider W1 
PROCEDURE CHOOSE-Bvalider :
DO WITH FRAME F1:
    /* Validation des factures */
    session:set-wait-state("general").
    if brcli:num-selected-rows = 0 then run traitement-toutes.
    else if brcli:num-selected-rows > 0 then run traitement-certaines.
    
    IF CAN-FIND(FIRST wfac NO-LOCK) THEN DO:
        IF LoadSettings("VALFAC", OUTPUT hSettingsApi) THEN DO:
            Logbid=SaveSetting("Soumis", soumis:SCREEN-VAL).
             
            UnloadSettings(hSettingsApi).
        END.
        PARM-EDT = "000000V000000000000000000" + /*  1 �  25 */
                   STRING(YEAR(TODAY), "9999") + /* 26 � 30 */ "-" +
                   STRING(MONTH(TODAY),  "99") + /* 31 � 33 */ "-" +
                   STRING(DAY(TODAY),    "99"). /* 34 � 35 */
    
        RUN RunJob ("EDTFAC", "CUSTOM:" + STRING(THIS-PROCEDURE), OUTPUT MODEDIT).
        
        APPLY "entry" TO fact$. /*$DOL2*/
        /* $DOL2 
        APPLY "CLOSE":U TO THIS-PROCEDURE.
        RETURN NO-APPLY. */
        /* $V41-DOL...*/
        RUN openquery.
        /* ...$V41-DOL */
        END.
    session:set-wait-state("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation W1 
PROCEDURE CombarsCreation :
RUN combars PERSISTENT SET hComBars.

    /* Barre pour la frame f1 */
    hComBars-F1 = CBOCXAdd(FRAME F1:HANDLE, OUTPUT FrmComBars-F1).
    CBSetSize(hComBars-F1, 1.04, 28.26, 1.15,FRAME F1:WIDTH - 27.66).   /*taille et place de la barre*/
    CBAddBar(hComBars-F1, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-F1, NO).
    CBToolBarAccelTips(hComBars-F1,NO).

    CBAddImage(hComBars-F1, {&bquitter}, StdMedia + "\abandonner.bmp").
    CBAddImage(hComBars-F1, {&Bvalider}, StdMedia + "\valider.bmp").
    CBAddImage(hComBars-F1, {&Bediter}, StdMedia + "\editer.bmp").
    CBAddImage(hComBars-f1, {&bctrlf12}, GCOMedia + "\ctrlf12.bmp").
    CBAddImage(hComBars-f1, {&Baffenc}, GCOMedia + "\d106.bmp").
    
    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bquitter}, Translate("&Abandonner") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&bquitter}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bvalider}, Translate("&Valider TOUTES les factures") + " (F9)", NO).
    CBSetItemStyle(hComBars-F1, 2, {&bvalider}, {&xtpButtonIconAndCaption}).

    CbAddItemPrint(hComBars-f1, 2, ?, {&BEditer}, Translate("&Editer"),YES,Nompgm).

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bcontroler}, Translate("&Contr�ler l'int�grit� comptable"), YES).
    CBSetItemStyle(hComBars-F1, 2, {&bcontroler}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&Baffenc},Translate("&Pi�ce") + " (F11)", yes).
    CBSetItemRightAlign (hComBars-f1, 2, {&baffenc}, yes).

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bctrlf12}, Translate("R�organiser les colonnes") + " (CTRL-F12)", YES).
    CBSetItemRightAlign (hComBars-f1, 2, {&bctrlf12}, yes).

    RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE(FRAME F1:HANDLE, "Validation des factures").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ComBarsExecute W1 
PROCEDURE ComBarsExecute :
DEFINE INPUT PARAMETER pNumComBar AS INT NO-UNDO.
    DEFINE INPUT PARAMETER pButton    AS INT NO-UNDO.

    IF pNumComBar = 1 THEN CASE pButton:
        WHEN {&bQuitter}   THEN RUN CHOOSE-Bquitter.
        WHEN {&bValider}   THEN RUN CHOOSE-Bvalider.
        WHEN {&bediter}    THEN RUN CHOOSE-Bediter.
        WHEN {&bcontroler} THEN RUN CHOOSE-bcontroler.
        WHEN {&bctrlf12}   THEN DO:
            IF BROWSE Brcli:HIDDEN = NO THEN APPLY "ctrl-f12" TO BROWSE Brcli.
        END.
        WHEN {&Baffenc}    THEN RUN Ctrlf8.
    END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Ctrlf8 W1 
PROCEDURE Ctrlf8 :
IF brcli:NUM-SELECTED-ROWS IN FRAME F1 = 1 AND AVAIL entetcli THEN RUN affenc_c(ROWID(entetcli)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openquery W1 
PROCEDURE openquery :
do with frame f1:
    {&OPEN-QUERY-brfac}

    if not available entetcli then assign brfac:hidden=true pbfact:hidden=yes Logbid = CBEnableItem (Hcombars-f1, 2, {&bediter}, NO).
    else assign brfac:hidden=no pbfact:hidden=no Logbid = CBEnableItem (Hcombars-f1, 2, {&bediter}, YES).

    {&OPEN-QUERY-brcli}

    if available entetcli then do:
        RUN initBrowse(Brcli:HANDLE,"VALFAC_C").
        assign aucune-commande:hidden=true brcli:hidden=no Logbid = CBEnableItem (Hcombars-f1, 2, {&bcontroler},YES) Logbid = CBEnableItem (Hcombars-f1, 2, {&bvalider},YES)
        logbid=brcli:select-row(1).
        apply "value-changed" to brcli.
    end.
    else assign brcli:hidden=true aucune-commande:hidden=no Logbid = CBEnableItem (Hcombars-f1, 2, {&bcontroler},NO) Logbid = CBEnableItem (Hcombars-f1, 2, {&bvalider},NO).
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init W1 
PROCEDURE proc-init :
find first parsoc no-lock no-error.
RUN CombarsCreation.
ASSIGN 
    n_d = (IF parsoc.mult_soc THEN g-ndos ELSE 0)
    g-user-droit = (IF ADesDroits("Droit", g-user) THEN g-user ELSE GetGroupeUser("Droit", g-user))
    pas-droit=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="FACTURE")
    droit-marge = NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="MARGE") 
        AND (parsoc.valo_px<>"A" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPA")) 
        AND (parsoc.valo_px<>"M" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPM")) 
        AND (parsoc.valo_px<>"R" OR NOT can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPR")) 
    brcli:PRIVATE-DATA IN FRAME f1 = "N� Facture|Date facture|N� Commande|R�f�rence|Date commande|Date Livraison|Client|Commercial".

DO WITH FRAME F1:
  assign fact$=0 ffact$=0
         pbfact="ATTENTION !!! Les factures suivantes doivent �tre r��dit�es !!!".
  ENABLE brfac fact$ ffact$ brcli soumis serveur bplan tbcaisse.
  display fact$ ffact$ pbfact ft$selection-1.
  IF NOT parsoc.ges_cai THEN HIDE tbcaisse.
  assign aucune-commande:hidden=yes frame f1:hidden=no.
  
  IF loadsettings("VALFAC", output hsettingsapi) then do:
      vsoumis = GetSetting("Soumis","no").
     
      UnloadSettings(hSettingsApi).
  END.
  
  ListeServeur=GetServers("PROGIWIN").
  IF ListeServeur="" THEN HIDE soumis serveur bplan.
  ELSE do:
      serveur:LIST-ITEMS=ListeServeur.
      serveur:SCREEN-VAL=serveur:ENTRY(1).
      soumis:SCREEN-VAL=(IF vsoumis="yes" THEN vsoumis ELSE "no").
      apply "value-changed" to soumis. 
  END.

  RUN openquery.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RESIZEWINDOW W1 
PROCEDURE RESIZEWINDOW :
ADD-RESIZEWIDGET(FRAME f1:HANDLE, "H").
    ADD-RESIZEWIDGET(brcli:HANDLE, "H").
    ADD-RESIZEWIDGET(rect-26:HANDLE, "H").
    SET-RESIZEVALUE("FIXW","yes").
    DO-RESIZELOAD().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traitement-certaines W1 
PROCEDURE traitement-certaines :
listfac="".

/* MAJ stats et historique pour chaque ent�te,lignes facture client */
DO X = 1 TO BRCLI:NUM-SELECTED-ROWS in frame f1:
   logbid = brcli:fetch-selected-row(x).
   if logbid then do:
       y = LOOKUP(STRING(entetcli.no_fact), LISTFAC).
       IF y = 0 THEN LISTFAC = LISTFAC + STRING(entetcli.no_fact) + ",".
   end.
END.

/* Lecture des factures */
LISTFAC = TRIM(LISTFAC, ",").
/*$645*/
EMPTY TEMP-TABLE WFAC NO-ERROR.
I = 0.
IF LISTFAC<>"" THEN DO X = 1 TO NUM-ENTRIES(LISTFAC):
    FOR EACH ENTETCLI2 FIELDS (ndos fac_maj no_fact fac_edi type_fac ticket dat_liv NO_bl NO_cde) 
        WHERE entetcli2.ndos=n_d AND ENTETCLI2.FAC_MAJ=NO AND ENTETCLI2.NO_FACT=INT(ENTRY(X,LISTFAC)) and ENTETCLI2.fac_edi=yes NO-LOCK BY ENTETCLI2.NO_fact BY ENTETCLI2.dat_liv BY ENTETCLI2.NO_bl BY ENTETCLI2.NO_cde:
        IF ENTETCLI2.ticket <> 0 AND ENTETCLI2.ticket <> ? THEN NEXT.
        CREATE WFAC.
        ASSIGN WFAC.WFNRG = ENTETCLI2.NO_FACT
               I = I + 1
               WFAC.WFORB = I
               WFAC.WFREG = (if substr(ENTETCLI2.type_fac,1,1)="R" or substr(ENTETCLI2.type_fac,1,1)="T" then yes else no)
               WFAC.WFRID = RECID(ENTETCLI2).
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traitement-toutes W1 
PROCEDURE traitement-toutes :
/*$645*/
EMPTY TEMP-TABLE WFAC NO-ERROR.
I = 0.
if fact$=0 and ffact$=0 then
FOR EACH ENTETCLI2 fields (ndos fac_maj no_fact type_fac fac_edi dat_liv NO_bl NO_cde ticket zta /*$DOL1*/ ) WHERE entetcli2.ndos=n_d AND FAC_MAJ=NO AND NO_FACT<>0 and fac_edi=yes NO-LOCK BY ENTETCLI2.NO_fact BY ENTETCLI2.dat_liv BY ENTETCLI2.NO_bl BY ENTETCLI2.NO_cde:
    /* on ne valide pas les ventes qui viennent de la caisse */
    IF ENTETCLI2.ticket <> 0 AND ENTETCLI2.ticket <> ? THEN NEXT.

    IF entetcli2.zta[2] = "" AND entetcli2.zta[1] = "18" OR entetcli2.zta[2] = "18" THEN NEXT. /*$DOL1 CB*/

    CREATE WFAC.
    ASSIGN WFAC.WFNRG = ENTETCLI2.NO_FACT
           I = I + 1
           WFAC.WFORB = I
           WFAC.WFREG = (if substr(ENTETCLI2.type_fac,1,1)="R" or substr(ENTETCLI2.type_fac,1,1)="T" then yes else no)
           WFAC.WFRID = RECID(ENTETCLI2).
END.
else if ffact$=0 then
FOR EACH ENTETCLI2 fields (ndos fac_maj no_fact type_fac fac_edi dat_liv NO_bl NO_cde ticket zta /*$DOL1*/ ) WHERE entetcli2.ndos=n_d AND FAC_MAJ=NO AND ENTETCLI2.no_fact>=fact$ and fac_edi=yes NO-LOCK BY ENTETCLI2.NO_fact BY ENTETCLI2.dat_liv BY ENTETCLI2.NO_bl BY ENTETCLI2.NO_cde:
    /* on ne valide pas les ventes qui viennent de la caisse */
    IF ENTETCLI2.ticket <> 0 AND ENTETCLI2.ticket <> ? THEN NEXT.

    IF entetcli2.zta[2] = "" AND entetcli2.zta[1] = "18" OR entetcli2.zta[2] = "18" THEN NEXT. /*$DOL1 CB*/

    CREATE WFAC.
    ASSIGN WFAC.WFNRG = ENTETCLI2.NO_FACT
           I = I + 1
           WFAC.WFORB = I
           WFAC.WFREG = (if substr(ENTETCLI2.type_fac,1,1)="R" or substr(ENTETCLI2.type_fac,1,1)="T" then yes else no)
           WFAC.WFRID = RECID(ENTETCLI2).
END.
else
FOR EACH ENTETCLI2 fields (ndos fac_maj no_fact type_fac fac_edi dat_liv NO_bl NO_cde ticket zta /*$DOL1*/ ) WHERE entetcli2.ndos=n_d AND FAC_MAJ=NO AND ENTETCLI2.no_fact>0 and ENTETCLI2.no_fact>=fact$ and ENTETCLI2.no_fact<=ffact$ and fac_edi=yes NO-LOCK BY ENTETCLI2.NO_fact BY ENTETCLI2.dat_liv BY ENTETCLI2.NO_bl BY ENTETCLI2.NO_cde:
    /* on ne valide pas les ventes qui viennent de la caisse */
    IF ENTETCLI2.ticket <> 0 AND ENTETCLI2.ticket <> ? THEN NEXT.

    IF entetcli2.zta[2] = "" AND entetcli2.zta[1] = "18" OR entetcli2.zta[2] = "18" THEN NEXT. /*$CB*/

    CREATE WFAC.
    ASSIGN WFAC.WFNRG = ENTETCLI2.NO_FACT
           I = I + 1
           WFAC.WFORB = I
           WFAC.WFREG = (if substr(ENTETCLI2.type_fac,1,1)="R" or substr(ENTETCLI2.type_fac,1,1)="T" then yes else no)
           WFAC.WFRID = RECID(ENTETCLI2).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

