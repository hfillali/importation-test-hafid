&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME W1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W1 
/*------------------------------------------------------------------------

  File: Histfb
  Description: Historique commandes,BL,factures
  Author: JL
  Created: 10/10/01

$75 HL 12/04/99 F9
$113 HL 22/04/99 refint x(17)
$127 HL 28/04/99 copylg_a
$128 HL 29/04/99 mt_fact
$140 HL 06/05/99 mt_ht
$158 MF 17/05/99 taffaire + proc MAJ-STOCK-AFFAIRE & MAJ-STOCK-AFFAIRE-SUPPRIME
$182 HL 21/05/99 run edfs1
$217 HL 05/07/99 run edhb1 et traitement-plage bplage
$221 HL 07/07/99 tqrecue enable + suppression ligne si qt�=0
$222 HL 12/07/99 poi_var
$290 HL 08/09/99 bug 2 n� BL identiques � la m�me date
$295 HL 10/09/99 � partir du n�pi�ce
$475 HL 19/01/00 ajout param�tre � lotmod,immmod
$543 JL 16/03/00 Quand s�lection par pi�ce, on ne prend que les 50 premi�res
$547 JL 23/03/00 Ajout menu contextuel (popup)
$556 JL 11/04/00 Ajout choix �dition Code, R�f. Interne ou externe
$607 JL 11/07/00 Qte passe sur 7
$618 MF 03/08/00 Affichage des situations dans la liste des factures uniquement si valid�es (histoent.chif_bl=yes)
$645 Hl 11/10/00 Batchage des factures avec temp-table partag�e
$647 JL 18/10/00 volume
$663 JL 20/11/00 Gestion demande ARC et R�f. ARC (ARC)
$731 JL 10/08/01 Ajout param�tre de valorisation ou non de l'�dition run edrf1 (pval)
$776 FM 27/02/02 Ajout bouton de visualisation des documents d'ent�te
$807 JL 01/07/02 Gestion entfacfo et ligfacfo (facture fournisseur)
$1070 HL 18/08/05 Multi-Soci�t� (Rechercher : parvs, parsoc.mult_soc, n_d, n_dep, n_soc, {soc.i "T")
$1081 JL 20/09/05 ACHFOU : Acheteur : Que ses fournisseurs (user-ach)
$1149 HL 23/02/06 Dossier prix de revient � l'achat (dos_pxr, ges_om)
$1184 JL 03/05/06 Gestion des litiges avec les nouvelles zones de ligfacfo (run litige)
$1130 ED 22/05/06 Couleur diff�rente pour les lignes en stock de consignation

$ERGOJL 11/12/06

$1258 MT 14/02/07 Gestion du droit "MODFACA" pour modification r�ception ou dossier prix de revient quand tout a �t� transf�r� en compta
$1293 JL 10/05/07 Nouvelles gestion des litiges (Mntlit_a)
$BOIS APR 17/08/07 acc�s �cran tra�abilit�
$1359 GC 19/10/07 Edition pdf par XML pour g�rer l'UTF-8
$1479 APR 23/04/08 Edition bon interne
$1489 DDG 09/05/08 Divers am�liorations factures de situations
$1495 vr 20/06/08 Recherche rapide sur les tables de maintenance
$1539 YO 29/07/08 Nouveau param�tre �dition facture situation
$1583 ED 21/11/08 Gestion des �ditions param�trables
$1759 vr 04/03/09 Pouvoir �diter un BL carri�re. Pouvoir visualiser un BL carri�re
$1837 vr 23/04/09 Acc�s aux zones carri�re dans l'historique BL
$1852 JL 06/05/09 Possibilit� de faxer ou d'envoyer par mail une facture en historique
$1728 Ppe 14/10/09 gestion du transport affr�t�
$2122 JL  19/01/09 Ajout possibilit� de r��diter un bon de retour en achat
$2110 PPe 29/01/10 Gestion des consignes � l'achat
$2222 CGU 31/05/10 Consultation des �ditions archiv�es
$A33836 Acc�s � l'historique des produits factur�s d'une commande.
$A34993 HL 13/07/10 - ListeDepots : contient la liste des d�p�ts autoris�s, ce qui permet de g�rer quand m�me la ligne <Tous d�p�ts>
                   chose qu'on ne pouvait pas faire auparavant quand l'un des d�p�ts n'�tait pas autoris�
                   - ListeSocietes : contient la liste des soci�t�s autoris�es, ce qui permet de g�rer quand m�me la ligne <Toutes soci�t�s>
                   chose qu'on ne pouvait pas faire auparavant quand l'une des soci�t�s n'�tait pas autoris�e
$A35790 IRR 09/09/10 Ajout d'un bouton pour g�rer l'automatisme d'affichage
$A37021 GC 17/11/10 Gestion envoi par mail de la facture de situation
$A35969 MT 11/01/11 Gestion des prix franco
$A35891 LLA 26/01/11 Libell�s des d�signations produit en langue �trang�re => ajout colonne dans le browse + gestion de histoent
$A38917 GC 17/02/11 clause concernant les droits d'affichage du client
$A39559 GC 30/03/11 Droits fournisseurs
$2672   APR 04/05/11 Mobilit�
A38196 HL 03/05/11 4.4 usr_ctl dat_ctl hr_ctl dans entfacfo
A37322 HL 05/05/11 4.4 saisie des prix sur 8 entiers au lieu de 7
$A41075 vr 24/06/11 caract�ristiques de r�ception g�n�rale
$A40207 CSA 28/07/11 Gestion S.S.C.C
$A26501 lla 04/10/11 Test p�riode validit� mvt
$A42112 LLA 04/10/11 Meilleure Visualisation litiges non r�solus et r�solus
$A40504 ALF 04/11/11 Modification frm-composants, CTRL F8
$A40206 TEN 23/11/11 Etiquette SSCC
A43751 HL 19/12/11 entfacfo.zone_libre[9] pour stocker le fait que les montants de facture aient �t� chang�s
A39914 HL 29/03/12 Gestion des retards fournisseurs avec impact sur les commandes clients (entetfou.dat_liv et dat_acc)
$A47809 OP 18/04/12 G�rer le cas des ent�tes de type cession (atelier)
$A20014 ALF 30/05/12 Docs � l'achat.
$A35573 MCS 14/08/12 Affichage suivant client command�, factur� ou livr�
$A53253 MCS 03/09/12 En arborescence de pi�ces, ajustement � la fen�tre du browse Hbrowse-lig quand on retail en largeur
$A54375 CSA 04/10/12 Tenir compte des droits pour l'affichage des colonnes prix d'achat, prix de revient, et P.M.P dans le browse des composants pour les produits � nomenclature
$A54459 GC  04/10/12 Plantage sur Tri / actualisation en historique de commande / B.L / Facture
A55569 HL 07/12/12 Affichage information pi�ce r�gl�e / partiellement r�gl�e
$A55185 MT 13/12/12 Correction sur taux de change li�s au frais.
A57448 HL 15/02/13 Fen�tre de l�gende (legende) + A59320
A59782 HL 4.5 01/03/03 Ajout B.L HT vente
$A59823 GC 04/03/13 Acc�s aux devis archiv�s dans la GED
$A60174 TEN 14/03/13 G�n�ration fichier XML
A52484 HL 4.5 20/03/13  parsoc.form_liv : intitul� de la date de livraison d�part dans les labels et colonnes (dat_liv + dat_dep + dat_mvt)
$A60546 IRR 05/04/13 Ajout de la visualisation des documents GED associ�s aux pi�ces
A55537 HL 4.5 24/04/13 Passage de 3 � 6 U.L, Explications dans {6ul.i}, Api {Api-UL.i}
$A62727 APR 11/06/13 coloriser ligne selon litige r�solu / non r�solu + bouton acc�s CR
$A55544 JL 05/07/13 Compta Multi-soci�t�
$A59971 AJA 22/07/2013 Option de prise en compte du nombre de liasses B.L pour la r��dition
$A62908 CGU Gestion des DF regroup�s dans le copylg
$A62570 DDG 18/06/13 Factures de situation : saisie montants r�vision / actualisation
$A66083 CSA 08/10/13 Libell� et boutons mal initialis�s c�t� client si la zone recherche client est vide
$A62337 CSA 13/09/13 Tra�abilit� des modifications de commande
$A66889 IRR 28/10/13 Lien GED -Commande facture fournisseur
$A68043 CSA 22/01/14 S.S.C.C en sortie uniquement
$A71882 CSA 23/01/14 Probl�me de s�lection par d�faut sur le client en tant que
$A72943 CGU 03/03/14 Correction de l'appel � cr_rec
$A68752 DMO 22/04/14 Ajout d'un bouton dans la combar lan�ant un comparatif Cmd / Devis
$A76832 CGU 07/05/14 On peut visualiser toutes les factures de toutes les soci�t�s, m�me si on pas le droit a certaines soci�t�s
$A56990 HL 10/04/14 Gestion d'une grille de remises illimit�es en vente (grille_rem, remise 2 fig�e)
$A41450 ALE 27/06/14 Remplacement libell� 'S�lection du correspondant' par 'S�lection du correspondant / Fax Express
$A62526 CAF 08/10/14 Edition BL en historique (chiffr� ou pas)
$A87991  JL 17/11/14 Ajout de l'affichage des lignes typ_mvt = 'V' pour le contr�le qualit�
$A66700 DDG 08/12/14 Recherche par affaire dans l'historique des factures achat
$A91177 VR 22/01/15 Compl�ment �cran histolig vente
$A93217 ARI 26/01/15 Optimisation recherche tous clients et pas d'affichage automatique si premier lancement
$A88742 DDG 13/03/15 Lien factures clients GED et ERP : pb en cas de multi-bl
$A73116 DDG 13/03/15 Histo fact vente : prob affichage si N fact situation avec m�me N� BL
$A65291 JCA 07/04/15 R��dtion plage factures, �dition des factures de situation avec le bon mod�le

YK002 ISMA 06/2015 Ajout des Boutons D�nomination de Facture et Eclatemen de Facture (en sus)
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER pTypMvt    AS C no-undo.  /* Client,Fournisseur */
DEF INPUT PARAMETER pCodeTiers AS I no-undo.
DEF INPUT PARAMETER pRowID     AS RowID no-undo.

/* Local Variable Definitions ---                                       */
DEF SHARED VAR G-Dftdev AS C.
DEF SHARED VAR g-langue AS CHAR NO-UNDO.  /*A35891*/
DEF SHARED VAR g-multisoc AS LOG. /*$A55544*/

{portail/menu-api.i} /* $A40219 */
{Api-UL.i} /* API sur Unit�s Logistiques A55537 */

DEFINE VARIABLE paffaire AS CHARACTER  NO-UNDO.
IF NUM-ENTRIES(pTypMvt,CHR(1)) > 1 THEN
    ASSIGN
        paffaire = ENTRY(2,pTypMvt,CHR(1))
        pTypMvt  = ENTRY(1,pTypMvt,CHR(1)).

DEF VAR hpostit AS HANDLE NO-UNDO.

DEF VAR PARM-EDT AS C FORMAT "X(2560)" no-undo.
DEF VAR MODEDIT  AS I INIT 1 no-undo.
DEFINE VARIABLE NOMPGM AS CHARACTER NO-UNDO. NOMPGM = "EDTHF".
DEFINE VARIABLE NOMPGM3 AS CHARACTER NO-UNDO. NOMPGM3 = "EDTHF".

/* Liste d�p�ts */
DEF VAR vdepot AS CHAR no-undo.
DEF VAR un-depot AS LOG no-undo.
DEF VAR n_d AS INT no-undo. /* N� Soci�t� en cours */
DEF VAR n_soc AS INT no-undo. /* N� Soci�t� choisie */
DEF VAR n_dep AS INT no-undo. /* N� D�p�t choisi */
DEF VAR LstSoc AS CHAR no-undo.
DEF VAR ListeSocietes AS CHAR no-undo.
DEF VAR ListeDepots AS CHAR no-undo.

DEF VAR TreeSui AS COM-HANDLE no-undo.
DEF VAR Imgtree AS COM-HANDLE no-undo.

DEFINE VARIABLE liste-soc AS CHARACTER  NO-UNDO. /*$A76832*/

DEF VAR svrowid   AS ROWID no-undo.
DEFINE VARIABLE X AS INTEGER    NO-UNDO.

DEFINE VARIABLE Pload AS LOGICAL    NO-UNDO.

/* $A33837... */
DEF VAR HBrowse-c      AS HANDLE     NO-UNDO.
DEF VAR HQuery-c       AS HANDLE     NO-UNDO.
DEF VAR qliste-champ-c AS CHARACTER  NO-UNDO.
DEF VAR qliste-label-c AS CHARACTER  NO-UNDO.
DEF VAR ii             AS INTEGER    NO-UNDO.
DEF VAR hcol-c         AS HANDLE     NO-UNDO.
DEF BUFFER histolig2 FOR histolig.
DEF VAR creerBrwsComp AS LOG    NO-UNDO.
/* ...$A33837 */

DEF VAR HQry-brw AS HANDLE NO-UNDO.
DEF VAR HBuf-brw AS HANDLE NO-UNDO.

/*$A66889...*/
DEF TEMP-TABLE tt-req NO-UNDO
    FIELD ged-requete AS CHAR
    FIELD vCriteresGed AS CHAR
    FIELD vValCriteres AS CHAR.
DEF VAR ged-requete-def AS CHAR NO-UNDO.
DEF VAR vCriteresGed-def AS CHAR NO-UNDO.
DEF VAR vValCriteres-def AS CHAR NO-UNDO.
DEFINE VARIABLE pascritere AS LOGICAL    NO-UNDO.
/*...$A66889*/

DEF VAR AffSVariante AS HANDLE no-undo. /*$1208*/
DEF VAR mybuf1  AS C no-undo.
DEF VAR mybuf2  AS C no-undo.
DEF VAR k       AS I no-undo.
DEF VAR z       AS I no-undo.
DEF VAR parcod  AS C no-undo.
DEF VAR parlib  AS C no-undo.
DEF VAR logbid  AS LOG no-undo.
DEF VAR svcle   AS C no-undo.
DEF VAR svcol   AS DEC no-undo.
DEF VAR svwidth AS DEC no-undo.
DEF VAR svtree  AS DEC no-undo.
DEF VAR svbut   AS DEC no-undo.
DEF VAR chaine  AS C no-undo.
def var vex     as c no-undo.
DEF VAR retour  AS LOG no-undo.

DEF VAR HCol    AS HANDLE NO-UNDO.
DEF VAR qprepare-lldespro AS CHAR no-undo. /*A35891*/
DEF VAR qprepare-histoent AS CHAR no-undo. /*A35891*/
/* En-t�te */
DEF VAR HBrowse-Ent AS HANDLE NO-UNDO.
DEF VAR HQuery-Ent  AS HANDLE NO-UNDO.
DEF VAR qprepare-Ent  AS C no-undo.
DEF VAR qcondition-Ent AS C no-undo.
DEF VAR qliste-champ-ent  AS C no-undo.
DEF VAR qliste-label-ent  AS C no-undo.
DEF VAR qcolonne-lock-ent AS i no-undo.
DEF VAR CHQRYBY-ent  AS C no-undo.
DEF VAR COLORD-ent   AS C no-undo.
DEF VAR COLDESC-ent  AS LOG no-undo.
DEF VAR hcoldisp-ent AS HANDLE EXTENT 11 no-undo.
DEF VAR qchar-ent         AS C no-undo.

/* Ligne */
DEF VAR Qliste-label-lig  AS C no-undo.
DEF VAR qliste-champ-lig  AS C no-undo.
DEF VAR qchar-lig         AS C no-undo.
DEF VAR qcondition-lig    AS C no-undo.
DEF VAR qcolonne-lock-lig AS I no-undo.
DEF VAR HBrowse-lig       AS HANDLE NO-UNDO.
DEF VAR CHQRYBY-lig  AS C no-undo.
DEF VAR COLORD-lig   AS C no-undo.
DEF VAR COLDESC-lig  AS LOG no-undo.
DEF VAR Hquery-lig   AS HANDLE no-undo.
DEF VAR Qprepare-lig AS C no-undo.
DEF VAR hcoldisp-lig AS HANDLE EXTENT 22 /*$A35969 19*/ no-undo.

DEFINE VARIABLE hremglo AS HANDLE     NO-UNDO.
DEFINE VARIABLE hcarte AS HANDLE     NO-UNDO.
/* $A38917... */
{api-piece-cl.i}
DEFINE VARIABLE qprepare-cli AS CHARACTER  NO-UNDO.
DEFINE VARIABLE clause-cli AS CHARACTER  NO-UNDO.
/* ...$A38917 */
/* $1489 ... */
/* $A39559... */
{api-piece-fo.i}
DEFINE VARIABLE qprepare-fou AS CHARACTER  NO-UNDO.
DEFINE VARIABLE clause-fou AS CHARACTER  NO-UNDO.
/* ...$A39559 */
DEF VAR pdup AS LOG NO-UNDO INIT NO.
DEF VAR pexclisoc AS LOG NO-UNDO INIT NO.
DEF VAR pniveau AS INT NO-UNDO INIT 1.
DEF VAR pdescom AS LOG NO-UNDO INIT NO.
DEF VAR ptxtent AS LOG NO-UNDO INIT NO.
DEF VAR pdatfac AS DATE NO-UNDO.
DEF VAR psimul AS LOG NO-UNDO INIT NO.
DEF VAR previs AS DEC NO-UNDO.
DEF VAR pactual AS DEC NO-UNDO.
/* $1539 ... */
DEF VAR plibrev AS CHAR NO-UNDO.
DEF VAR plibact AS CHAR NO-UNDO.
DEF VAR pfacdef AS LOG NO-UNDO. /* ... $1539*/
DEFINE VARIABLE plibre1 AS CHARACTER NO-UNDO. plibre1 = "".
DEFINE VARIABLE plibre2 AS CHARACTER NO-UNDO. plibre2 = "".
DEFINE VARIABLE plibre3 AS CHARACTER NO-UNDO. plibre3 = "".
/* ... $1489 */
/*$2110...*/
DEF NEW SHARED TEMP-TABLE WCDE NO-UNDO
    FIELD WCFOU AS INT  FORMAT '999999'    INITIAL  0   /* N� Fournisseur */
    FIELD WCTYP AS CHAR FORMAT 'X'         INITIAL ""   /* Type saisie    */
    FIELD WCNOC AS INT  FORMAT '999999999' INITIAL  0   /* N� Commande    */
    FIELD WCNOB AS INT  FORMAT '999999999' INITIAL  0   /* N� BL    */
INDEX IWCD IS PRIMARY UNIQUE WCFOU WCTYP WCNOC
INDEX IWCD2 WCNOC.
/*...$2110*/

DEFINE NEW SHARED TEMP-TABLE tt_facture NO-UNDO
    FIELD supprime AS LOGICAL INITIAL NO
    FIELD tt_mntfact AS DECIMAL
    FIELD tt_civ_fac AS CHAR
    FIELD tt_adr_fac AS CHAR EXTENT 3
    FIELD tt_k_post2f AS CHAR
    FIELD tt_villef  AS CHAR
    FIELD tt_paysf AS CHAR.



/* Browse Entfacfo */
DEF VAR CHQRYBY-ent2  AS C no-undo.
DEF VAR COLORD-ent2   AS C no-undo.
DEF VAR COLDESC-ent2  AS LOG no-undo.
DEF VAR qcondition-ent2    AS C no-undo.
DEF VAR qliste-champ-ent2  AS C no-undo.
DEF VAR qcolonne-lock-ent2 AS i no-undo.
DEF VAR qliste-label-ent2  AS C no-undo.
DEF VAR qchar-ent2        AS C no-undo.

/* Browse Ligfacfo */
DEF VAR CHQRYBY-lig2  AS C no-undo.
DEF VAR COLORD-lig2   AS C no-undo.
DEF VAR COLDESC-lig2  AS LOG no-undo.
DEFINE VARIABLE avec-joint AS LOGICAL    NO-UNDO. /*A35891*/
DEF VAR qcondition-lig2    AS C no-undo.
DEF VAR qliste-champ-lig2  AS C no-undo.
DEF VAR qcolonne-lock-lig2 AS i no-undo.
DEF VAR qliste-label-lig2  AS C no-undo.
DEF VAR qchar-lig2        AS C no-undo.

/* Tri sur les colonnes */
DEF VAR HFLD     AS HANDLE no-undo.

DEF BUFFER Clientr   FOR Client.
DEF BUFFER FournisR  FOR Fournis.
DEF BUFFER Histoent2 FOR Histoent.
DEF BUFFER Histoent3 FOR Histoent. /*$A61412*/

{droit_cli1.i}
DEF VAR g-user-droit AS C no-undo.
def var pas-droit-ach AS LOG NO-UNDO.
def var pas-droit-vpr as log no-undo.
def var pas-droit-vpa as log no-undo.
def var pas-droit-vpm as log no-undo.
def var pas-droit-vtrsp as log no-undo. /*$Z35969*/
DEF VAR user-ach AS C NO-UNDO.
DEF VAR g-user-perso AS C no-undo.
def var pas-droit-reajuster as log no-undo.
def var pas-droit-validerSR as log no-undo. /*$2672*/
DEF VAR droit-modif AS LOG NO-UNDO. /*$1258*/

DEFINE VARIABLE valid AS LOGICAL    NO-UNDO.

DEF SHARED VARIABLE g-ndos AS I.
DEF SHARED VARIABLE g-nom AS CHAR.

DEF VAR ges_WMS AS LOG    NO-UNDO. /*$2672*/
DEF VAR mode_validSR AS LOG    NO-UNDO. /*$2672*/

/* $2222 ... */
DEF VAR xitem-edt AS COM-HANDLE NO-UNDO.
DEF VAR cpt-doc AS INT NO-UNDO.
DEF TEMP-TABLE tt-doc NO-UNDO
    FIELD tt-num AS INT
    FIELD tt-cledoc AS CHAR
    INDEX id1 IS PRIMARY tt-num.
/* ... $2222 */

def var vchar       as C no-undo. /*$A35790*/
DEF VAR typclient AS INT    NO-UNDO. /*$A35573*/

FUNCTION REC-ZONE RETURNS CHARACTER (ptyp AS CHAR, pnom AS CHAR) FORWARD. /*$A66889*/

{tmp-fac.i}
{hGco-Api.DEF}
{G-hProwinBase.i}
{hcombars.i}
{Hgcoergo.i}
{hchkqdyn.i}
{cleval.i} /* $1359 */
{HXpedite.i}
{aff-api.i}
{hremval.i} /*$1798*/
{decia.i}
{decip.i}
{deciv.i}
{formach.i}
{formvte.i}
{decim.i}
{formmt.i}
{ged\funcapi_gd.i} /*$2222*/
{wms\api-mobsr.i} /*$2672*/
{wms\prop_mob.i}
{rec_zon.i} /*$A66889*/


DEF VAR buf-couleur AS INT EXTENT 5 NO-UNDO.

DEFINE VARIABLE Faxer AS LOGICAL    NO-UNDO.

DEF VAR FrmComBars-f1  AS HANDLE NO-UNDO.
DEF VAR hComBars-f1    AS INT NO-UNDO.
DEF VAR valbtn_autorefresh AS LOG NO-UNDO. /*$A35790*/
DEF VAR valbtn_blchiffr    AS LOG NO-UNDO. /*$A62526*/

&SCOPED-DEFINE Bquitter              101
&SCOPED-DEFINE Bajouter-colonnes-ent     102
&SCOPED-DEFINE Bajouter-colonnes-ent2    103
&SCOPED-DEFINE bajouter-colonnes-lig  104
&SCOPED-DEFINE bajouter-colonnes-lig2  105
&SCOPED-DEFINE bpopup                106
&SCOPED-DEFINE Baffac                107
&SCOPED-DEFINE Boption               108
&SCOPED-DEFINE Bentete               109
&SCOPED-DEFINE Bpied                 110
&SCOPED-DEFINE Baffcde               111
&SCOPED-DEFINE Bcopier               112
&SCOPED-DEFINE Binfo                 113
&SCOPED-DEFINE Bcolisage             114
&SCOPED-DEFINE Bmarge                115
&SCOPED-DEFINE Bdoc                  116
&SCOPED-DEFINE Betiq                 117
&SCOPED-DEFINE Bediter               118
&SCOPED-DEFINE Bplage                119
&SCOPED-DEFINE Breajuster            120
&SCOPED-DEFINE BdossierPR            121
&SCOPED-DEFINE Blitige               122
&SCOPED-DEFINE Btraca                123 /*$BOIS*/
&SCOPED-DEFINE bediter-spe           124 /*$BOIS*/
&SCOPED-DEFINE Beditfaci             125 /*$BOIS*/
&SCOPED-DEFINE Bfax                  126
&SCOPED-DEFINE Bmail                 127
&SCOPED-DEFINE Bgstenvoi             128
&SCOPED-DEFINE bediter-bonCommission 129   /* $Bois ten */
&SCOPED-DEFINE btrpaffret            130  /*1728*/
&SCOPED-DEFINE Bdetail               131
&SCOPED-DEFINE bimm                  132
&SCOPED-DEFINE blot                  133
&SCOPED-DEFINE bemp                  134
&SCOPED-DEFINE Btn-varctrlf8         135
&SCOPED-DEFINE Baffdetprod           136 /* $A33836 */
&SCOPED-DEFINE Banarec               137 /* $A33836 */
&SCOPED-DEFINE Bafficher           138 /* $A35790 */
&SCOPED-DEFINE Bautorefresh           139 /* $A35790 */
&SCOPED-DEFINE bcarvar               140
&SCOPED-DEFINE bsscc                 141 /* $A40207 */
&SCOPED-DEFINE ctypcli               142 /* $A35573 */
&SCOPED-DEFINE blegende              143
&SCOPED-DEFINE bXML                  144 /*$A60174*/
&SCOPED-DEFINE bcr                   145 /*$A62727*/
&SCOPED-DEFINE btracacde             146 /* $A62337 */
&SCOPED-DEFINE bcompare              147 /*$A68752*/
&SCOPED-DEFINE bgrille_rem           148 /*A56990*/
&SCOPED-DEFINE bblchiffr             149 /*$A62526*/
&SCOPED-DEFINE Badresse              150
/*&SCOPED-DEFINE BEclate               151    YK002 ISMA Bouton Eclatement de Facture */


/* $2222 ATTENTION =>                800 � 850 r�serv� pour acc�s aux documents GED */

DEF VAR hcombars-legende AS INT NO-UNDO.
DEF VAR frcombars-legende AS HANDLE no-undo.

&SCOPED-DEFINE blegende-abandonner    501

DEF VAR FrmComBars-fedtfac  AS HANDLE NO-UNDO.
DEF VAR hComBars-fedtfac    AS INT NO-UNDO.
&SCOPED-DEFINE Bannuler-r            201
&SCOPED-DEFINE Bvalider-r            202

DEF VAR FrmComBars-fplafac  AS HANDLE NO-UNDO.
DEF VAR hComBars-fplafac    AS INT NO-UNDO.
&SCOPED-DEFINE Bannuler-p            301
&SCOPED-DEFINE Bvalider-p            302

DEF VAR FrmComBars-Frchoix  AS HANDLE NO-UNDO.
DEF VAR hComBars-Frchoix    AS INT NO-UNDO.
&SCOPED-DEFINE Bretour               401
&SCOPED-DEFINE Bediterc              402

/*$A59971...*/
DEF VAR FrmComBars-fedtbl  AS HANDLE NO-UNDO.
DEF VAR hComBars-fedtbl    AS INT NO-UNDO.
&SCOPED-DEFINE Bannuler-edtbl            601
&SCOPED-DEFINE Bvalider-edtbl            602
/*...$A59971*/

DEF VAR dynreq AS HANDLE NO-UNDO. /* $1495 */
DEF VAR dynreq_facfo AS HANDLE NO-UNDO. /* $1495 */
DEF VAR dynreq_histolig AS HANDLE NO-UNDO. /* $1495 */

DEFINE VARIABLE ModeleCaract AS CHARACTER NO-UNDO. /*$A41075*/
DEFINE VARIABLE typinfo AS CHARACTER  NO-UNDO. /*A20014*/

/*A55569*/
def shared var g-plateforme as char.
def var gestion-pocwin as LOG NO-UNDO.
DEF VAR wcl_cpt AS INT no-undo.
DEF VAR cpt411 AS INT no-undo.
{radical.i "modulo"}
{radical.i "411"}
cpt411=INT(RIGHT-TRIM(radical-411,'*')) * radical-modulo.

/*$A65291...*/
/*table temp copie de WFAC */
DEF NEW SHARED TEMP-TABLE WFACSIT NO-UNDO
  FIELD WFNRG AS INT  FORMAT '999999999' INITIAL  0   /* N� regroupement */
  FIELD WFORB AS INT  FORMAT '999999999' INITIAL  0   /* N� ordre regroupement */
  FIELD WFREG AS LOG  FORMAT 'O/N'       INITIAL  YES /* B.L regoup�s */
  FIELD WFRID AS INT  FORMAT '999999999' INITIAL  0   /* RecId du B.L */
INDEX IWFG IS PRIMARY UNIQUE WFNRG WFORB.
/*...$A65291*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F1

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES histoent

/* Definitions for FRAME Fadresse                                       */
&Scoped-define FIELDS-IN-QUERY-Fadresse histoent.civ_liv ~
histoent.adr_liv[1] histoent.adr_liv[2] histoent.adr_liv[3] ~
histoent.k_post2l histoent.villel histoent.civ_fac histoent.adr_fac[1] ~
histoent.adr_fac[2] histoent.adr_fac[3] histoent.k_post2f histoent.villef 
&Scoped-define ENABLED-FIELDS-IN-QUERY-Fadresse histoent.civ_liv ~
histoent.adr_liv[1] histoent.adr_liv[2] histoent.adr_liv[3] ~
histoent.k_post2l histoent.villel histoent.civ_fac histoent.adr_fac[1] ~
histoent.adr_fac[2] histoent.adr_fac[3] histoent.k_post2f histoent.villef 
&Scoped-define ENABLED-TABLES-IN-QUERY-Fadresse histoent
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-Fadresse histoent
&Scoped-define QUERY-STRING-Fadresse FOR EACH histoent SHARE-LOCK
&Scoped-define OPEN-QUERY-Fadresse OPEN QUERY Fadresse FOR EACH histoent SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-Fadresse histoent
&Scoped-define FIRST-TABLE-IN-QUERY-Fadresse histoent


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-268 RECT-270 RECT-271 RECT-10 csoc ~
RType rs-selection BR1 vcod_cli FLib1 FDateDeb FDateFin ent$ Falpha FNum ~
Tbarbo Tbapartir Butt-spli Ft$selection-1 ffrais freel FTxtAucun 
&Scoped-Define DISPLAYED-OBJECTS csoc RType rs-selection vcod_cli FLib1 ~
FDateDeb FDateFin ent$ Falpha FNum Tbarbo Tbapartir Ft$selection-1 ffrais ~
freel FTxtAucun 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD comparaisonDispo W1 
FUNCTION comparaisonDispo LOG FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W1 AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.
DEFINE VARIABLE CtrlFrame-2 AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame-2 AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BR1 
     LABEL "":L 
     SIZE 2.86 BY .88.

DEFINE BUTTON Butt-spli 
     LABEL "" 
     SIZE .72 BY 16.5.

DEFINE VARIABLE csoc AS CHARACTER INITIAL "0" 
     LABEL "Soci�t�" 
     VIEW-AS COMBO-BOX INNER-LINES 30
     LIST-ITEM-PAIRS "1234567890123456789012345","1"
     DROP-DOWN AUTO-COMPLETION UNIQUE-MATCH
     SIZE 30 BY 1 TOOLTIP "Soci�t� (CTRL-F4)"
     BGCOLOR 27 FONT 49 NO-UNDO.

DEFINE VARIABLE Falpha AS CHARACTER FORMAT "X(20)":U 
     VIEW-AS FILL-IN 
     SIZE 12 BY .71
     BGCOLOR 16 FONT 49 NO-UNDO.

DEFINE VARIABLE FDateDeb AS DATE FORMAT "99/99/99":U INITIAL 01/01/01 
     LABEL "Du" 
     VIEW-AS FILL-IN 
     SIZE 8.29 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE FDateFin AS DATE FORMAT "99/99/99":U 
     LABEL "au" 
     VIEW-AS FILL-IN 
     SIZE 8.29 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE ffrais AS DECIMAL FORMAT "-ZZZZZZZZ9.99":U INITIAL -123456789.99 
     LABEL "Frais" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .58
     BGCOLOR 19 FONT 49 NO-UNDO.

DEFINE VARIABLE FLib1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 26.86 BY .79
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE FNum AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .71
     BGCOLOR 16 FONT 49 NO-UNDO.

DEFINE VARIABLE freel AS CHARACTER FORMAT "X(255)":U INITIAL "Estim�s" 
      VIEW-AS TEXT 
     SIZE 6.57 BY .58
     BGCOLOR 19  NO-UNDO.

DEFINE VARIABLE Ft$selection-1 AS CHARACTER FORMAT "X(255)":U INITIAL "S�lection pour affichage" 
      VIEW-AS TEXT 
     SIZE 27.29 BY .67
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE FTxtAucun AS CHARACTER FORMAT "X(255)":U INITIAL "Aucun r�sultat" 
      VIEW-AS TEXT 
     SIZE 17.29 BY 1.21
     FGCOLOR 49 FONT 29 NO-UNDO.

DEFINE VARIABLE vcod_cli AS CHARACTER FORMAT "X(30)" 
     LABEL "Fournisseur (*=Tous)" 
     VIEW-AS FILL-IN 
     SIZE 9.86 BY .79
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE ent$ AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "En-t�tes", "E",
"En-t�tes && Lignes", "T"
     SIZE 22.72 BY .63 NO-UNDO.

DEFINE VARIABLE rs-selection AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Fournisseur", "F",
"Affaire", "A"
     SIZE 11.57 BY 1.46 NO-UNDO.

DEFINE VARIABLE RType AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Factures", "F",
"Commandes", "C",
"Bons de Livraison", "BL"
     SIZE 40 BY .63
     FGCOLOR 4  NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.14 BY 1.08.

DEFINE RECTANGLE RECT-268
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 113.14 BY 2.25.

DEFINE RECTANGLE RECT-270
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 90.57 BY .88.

DEFINE RECTANGLE RECT-271
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 22.57 BY .88.

DEFINE VARIABLE Tbapartir AS LOGICAL INITIAL no 
     LABEL "A partir du N� commande" 
     VIEW-AS TOGGLE-BOX
     SIZE 19.86 BY .54
     FONT 49 NO-UNDO.

DEFINE VARIABLE Tbarbo AS LOGICAL INITIAL no 
     LABEL "&Arborescence des pi�ces" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .54
     FGCOLOR 3 FONT 49 NO-UNDO.

DEFINE BUTTON Babandon-adr AUTO-END-KEY 
     LABEL "&Abandonner" 
     SIZE 26 BY 1.

DEFINE BUTTON BR-codpaysf 
     LABEL "":L 
     SIZE 2.86 BY .92.

DEFINE BUTTON BR-codpaysl 
     LABEL "":L 
     SIZE 2.86 BY .92.

DEFINE BUTTON Bvalider-adr AUTO-GO 
     LABEL "&Valider la nouvelle adresse (F9)" 
     SIZE 31 BY 1.

DEFINE VARIABLE wcodpaysf AS CHARACTER FORMAT "X(3)" 
     LABEL "&Pays" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .83
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE wcodpaysl AS CHARACTER FORMAT "X(3)" 
     LABEL "&Pays" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .83
     BGCOLOR 16 FGCOLOR 23 .

DEFINE VARIABLE wlibpaysf AS CHARACTER FORMAT "X(35)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY .83
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE wlibpaysl AS CHARACTER FORMAT "X(35)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY .83
     BGCOLOR 17 FGCOLOR 15  NO-UNDO.

DEFINE RECTANGLE RECT-273
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 13.

DEFINE RECTANGLE RECT-274
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63 BY 1.25.

DEFINE RECTANGLE RECT-272
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.86 BY 2.

DEFINE VARIABLE texbl AS LOGICAL INITIAL no 
     LABEL "Tenir compte des exemplaires soci�t�s et clients" 
     VIEW-AS TOGGLE-BOX
     SIZE 41.86 BY .83 NO-UNDO.

DEFINE VARIABLE fniveau AS CHARACTER FORMAT "X(255)":U INITIAL "Dernier niveau � �diter:" 
      VIEW-AS TEXT 
     SIZE 16.43 BY .67 NO-UNDO.

DEFINE VARIABLE rniveau AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "1", 1,
"2", 2,
"3", 3
     SIZE 13 BY .63 NO-UNDO.

DEFINE VARIABLE rs$global AS LOGICAL 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Edition d�taill�e", no,
"Edition globale", yes
     SIZE 32 BY .63 NO-UNDO.

DEFINE RECTANGLE RECT-252
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43.86 BY 5.25.

DEFINE VARIABLE rediter AS LOGICAL INITIAL no 
     LABEL "Editer description commerciale" 
     VIEW-AS TOGGLE-BOX
     SIZE 28.29 BY .67 NO-UNDO.

DEFINE VARIABLE rtexte AS LOGICAL INITIAL no 
     LABEL "Editer texte en-t�te" 
     VIEW-AS TOGGLE-BOX
     SIZE 18.14 BY .67 NO-UNDO.

DEFINE VARIABLE tdup AS LOGICAL INITIAL no 
     LABEL "Mention 'Duplicata Facture' sur le document" 
     VIEW-AS TOGGLE-BOX
     SIZE 34 BY .63 NO-UNDO.

DEFINE VARIABLE tex AS LOGICAL INITIAL no 
     LABEL "Tenir compte des exemplaires soci�t�s et clients" 
     VIEW-AS TOGGLE-BOX
     SIZE 38.57 BY .63 NO-UNDO.

DEFINE VARIABLE Legende-lib-648 AS CHARACTER FORMAT "X(255)":U INITIAL "N� Facture ou N� B.L Fournisseur (Achat)" 
      VIEW-AS TEXT 
     SIZE 41 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-649 AS CHARACTER FORMAT "X(255)":U INITIAL "Quantit�" 
      VIEW-AS TEXT 
     SIZE 42.72 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-650 AS CHARACTER FORMAT "X(255)":U INITIAL "Lignes" 
      VIEW-AS TEXT 
     SIZE 9 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-652 AS CHARACTER FORMAT "X(255)":U INITIAL "En-t�tes" 
      VIEW-AS TEXT 
     SIZE 11.43 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-653 AS CHARACTER FORMAT "X(255)":U INITIAL "N� B.L Fournisseur (Achat)" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-654 AS CHARACTER FORMAT "X(255)":U INITIAL "litige non r�solu" 
      VIEW-AS TEXT 
     SIZE 33.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-655 AS CHARACTER FORMAT "X(255)":U INITIAL "compos� commercial" 
      VIEW-AS TEXT 
     SIZE 33.29 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-657 AS CHARACTER FORMAT "X(255)":U INITIAL "composant commercial" 
      VIEW-AS TEXT 
     SIZE 33 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-658 AS CHARACTER FORMAT "X(255)":U INITIAL "litige r�solu" 
      VIEW-AS TEXT 
     SIZE 33.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-659 AS CHARACTER FORMAT "X(255)":U INITIAL "n� facture renseign�" 
      VIEW-AS TEXT 
     SIZE 33.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-661 AS CHARACTER FORMAT "X(255)":U INITIAL "N� Facture et Facture H.T (Achat)" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-662 AS CHARACTER FORMAT "X(255)":U INITIAL "Date de livraison" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-663 AS CHARACTER FORMAT "X(255)":U INITIAL "consignation" 
      VIEW-AS TEXT 
     SIZE 33.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-664 AS CHARACTER FORMAT "X(255)":U INITIAL "r�ception li�e � la WMS non valid�e" 
      VIEW-AS TEXT 
     SIZE 32 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-665 AS CHARACTER FORMAT "X(255)":U INITIAL "Livraison en retard" 
      VIEW-AS TEXT 
     SIZE 32 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-666 AS CHARACTER FORMAT "X(255)":U INITIAL "N� Commande" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-667 AS CHARACTER FORMAT "X(255)":U INITIAL "prix ou remises forc�s" 
      VIEW-AS TEXT 
     SIZE 33.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-668 AS CHARACTER FORMAT "X(255)":U INITIAL "R�f. commande" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-669 AS CHARACTER FORMAT "X(255)":U INITIAL "N� Facture (Vente)" 
      VIEW-AS TEXT 
     SIZE 41 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-670 AS CHARACTER FORMAT "X(255)":U INITIAL "non r�gl�" 
      VIEW-AS TEXT 
     SIZE 11.29 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-671 AS CHARACTER FORMAT "X(255)":U INITIAL "en partie r�gl�" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-672 AS CHARACTER FORMAT "X(255)":U INITIAL "r�gl�" 
      VIEW-AS TEXT 
     SIZE 9.86 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-673 AS CHARACTER FORMAT "X(255)":U INITIAL "Montant H.T" 
      VIEW-AS TEXT 
     SIZE 41 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-674 AS CHARACTER FORMAT "X(255)":U INITIAL "promotion" 
      VIEW-AS TEXT 
     SIZE 11.14 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-675 AS CHARACTER FORMAT "X(255)":U INITIAL "gratuit" 
      VIEW-AS TEXT 
     SIZE 10.29 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-676 AS CHARACTER FORMAT "X(255)":U INITIAL "garantie" 
      VIEW-AS TEXT 
     SIZE 10 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-677 AS CHARACTER FORMAT "X(255)":U INITIAL "Prix" 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-678 AS CHARACTER FORMAT "X(255)":U INITIAL "prestation" 
      VIEW-AS TEXT 
     SIZE 9.14 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-679 AS CHARACTER FORMAT "X(255)":U INITIAL "D�signation, R�f�rences, ..." 
      VIEW-AS TEXT 
     SIZE 40.86 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE Legende-lib-680 AS CHARACTER FORMAT "X(255)":U INITIAL "non r�f�renc�" 
      VIEW-AS TEXT 
     SIZE 11.14 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-681 AS CHARACTER FORMAT "X(255)":U INITIAL "divers factur�" 
      VIEW-AS TEXT 
     SIZE 11.29 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-682 AS CHARACTER FORMAT "X(255)":U INITIAL "cession" 
      VIEW-AS TEXT 
     SIZE 10.72 BY .54 NO-UNDO.

DEFINE VARIABLE Legende-lib-683 AS CHARACTER FORMAT "X(255)":U INITIAL "r�tro-cession" 
      VIEW-AS TEXT 
     SIZE 10.72 BY .54 NO-UNDO.

DEFINE RECTANGLE Legende-RECT-368
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 31 .

DEFINE RECTANGLE Legende-RECT-369
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 18 .

DEFINE RECTANGLE Legende-RECT-370
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 52 .

DEFINE RECTANGLE Legende-RECT-371
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 3 .

DEFINE RECTANGLE Legende-RECT-372
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 154 .

DEFINE RECTANGLE Legende-RECT-373
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 57 .

DEFINE RECTANGLE Legende-RECT-374
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 55 .

DEFINE RECTANGLE Legende-RECT-375
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 59 .

DEFINE RECTANGLE Legende-RECT-376
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 57 .

DEFINE RECTANGLE Legende-RECT-377
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 110 .

DEFINE RECTANGLE Legende-RECT-380
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 103 .

DEFINE RECTANGLE Legende-RECT-382
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 25 .

DEFINE RECTANGLE Legende-RECT-388
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 39 .

DEFINE RECTANGLE Legende-RECT-389
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 53 .

DEFINE RECTANGLE Legende-RECT-391
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 131 .

DEFINE RECTANGLE Legende-RECT-392
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 160 .

DEFINE RECTANGLE Legende-RECT-393
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 58 .

DEFINE RECTANGLE Legende-RECT-394
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 116 .

DEFINE RECTANGLE Legende-RECT-395
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 110 .

DEFINE RECTANGLE Legende-RECT-396
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 6.57 BY .63
     BGCOLOR 148 .

DEFINE RECTANGLE RECT-371
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 59.86 BY 10.75.

DEFINE RECTANGLE RECT-372
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 59.86 BY 6.63.

DEFINE VARIABLE fdat-d AS DATE FORMAT "99/99/99":U 
     LABEL "Du" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE fdat-f AS DATE FORMAT "99/99/99":U 
     LABEL "au" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE fn-d AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 990600001 
     LABEL "Du" 
     VIEW-AS FILL-IN 
     SIZE 9.86 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE fn-f AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "au" 
     VIEW-AS FILL-IN 
     SIZE 9.86 BY .79
     BGCOLOR 16 FGCOLOR 23  NO-UNDO.

DEFINE VARIABLE radio-editpdf AS LOGICAL 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Editer les �ditions classiques", no,
"Editer les �ditions �trang�res", yes
     SIZE 45 BY 1.08 NO-UNDO.

DEFINE VARIABLE typ-plage AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Plage de n� de facture", "N",
"Plage de dates de facture", "D"
     SIZE 23.43 BY 3 NO-UNDO.

DEFINE RECTANGLE RECT-259
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50.72 BY 6.29.

DEFINE VARIABLE tcli AS LOGICAL INITIAL no 
     LABEL "Uniquement pour le client s�lectionn� auparavant" 
     VIEW-AS TOGGLE-BOX
     SIZE 42.86 BY .63 NO-UNDO.

DEFINE VARIABLE Ft$selection-2 AS CHARACTER FORMAT "X(255)":U INITIAL "Pr�f�rence produit" 
      VIEW-AS TEXT 
     SIZE 17 BY .67
     FGCOLOR 49 FONT 36 NO-UNDO.

DEFINE VARIABLE Rsref AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Code", "C",
"R�f. interne", "I",
"R�f. externe", "X",
"Les 2 r�f�rences", "R"
     SIZE 51.86 BY .67 NO-UNDO.

DEFINE RECTANGLE RECT-264
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53.29 BY 1.92.

DEFINE RECTANGLE RECT-267
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53.29 BY .83.

DEFINE VARIABLE Tbval AS LOGICAL INITIAL no 
     LABEL "&Edition valoris�e" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .67 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY Fadresse FOR 
      histoent SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F1
     csoc AT ROW 2.63 COL 81.43 COLON-ALIGNED
     RType AT ROW 2.88 COL 27.72 NO-LABEL
     rs-selection AT ROW 3 COL 1.43 NO-LABEL WIDGET-ID 38
     BR1 AT ROW 3.54 COL 38
     vcod_cli AT ROW 3.58 COL 26 COLON-ALIGNED
     FLib1 AT ROW 3.58 COL 38.86 COLON-ALIGNED NO-LABEL
     FDateDeb AT ROW 3.58 COL 91.57 COLON-ALIGNED
     FDateFin AT ROW 3.58 COL 103.14 COLON-ALIGNED
     ent$ AT ROW 3.71 COL 67.72 NO-LABEL WIDGET-ID 42
     Falpha AT ROW 4.79 COL 46.29 COLON-ALIGNED NO-LABEL
     FNum AT ROW 4.79 COL 46.29 COLON-ALIGNED NO-LABEL
     Tbarbo AT ROW 4.88 COL 1.72
     Tbapartir AT ROW 4.88 COL 28
     Butt-spli AT ROW 5.58 COL 23.29
     Ft$selection-1 AT ROW 2.21 COL 1.72 NO-LABEL WIDGET-ID 20
     ffrais AT ROW 4.83 COL 63.14 COLON-ALIGNED
     freel AT ROW 4.83 COL 74 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     FTxtAucun AT ROW 12.5 COL 58 COLON-ALIGNED NO-LABEL
     RECT-268 AT ROW 2.46 COL 1.14
     RECT-270 AT ROW 4.71 COL 23.72
     RECT-271 AT ROW 4.71 COL 1.14
     RECT-10 AT ROW 1.04 COL 1.14 WIDGET-ID 4
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 113.29 BY 21.17
         FONT 49.

DEFINE FRAME frm-composants
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 9.75
         SIZE 113 BY 7.5
         TITLE "Composants" WIDGET-ID 100.

DEFINE FRAME Fedtbl
     texbl AT ROW 2.58 COL 2.14 WIDGET-ID 4
     RECT-272 AT ROW 2.21 COL 1.14 WIDGET-ID 2
    WITH 1 DOWN OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 35 ROW 7.5
         SIZE 45.43 BY 4.88
         FONT 49
         TITLE "Edition B.L" WIDGET-ID 300.

DEFINE FRAME Fedtfac
     tdup AT ROW 2.5 COL 2.14
     tex AT ROW 3.21 COL 2.14
     rs$global AT ROW 3.96 COL 2.14 NO-LABEL WIDGET-ID 2
     rniveau AT ROW 4.96 COL 23.86 NO-LABEL
     rediter AT ROW 5.75 COL 4.72
     rtexte AT ROW 6.46 COL 4.72
     fniveau AT ROW 4.96 COL 7.43 NO-LABEL
     RECT-252 AT ROW 2.21 COL 1.14
    WITH 1 DOWN OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 35 ROW 7.5
         SIZE 45.43 BY 8
         FGCOLOR 23 FONT 49
         TITLE "Edition Facture".

DEFINE FRAME Fplafac
     tcli AT ROW 2.33 COL 1.72
     tdup AT ROW 3 COL 1.72
          LABEL "Mention 'Duplicata Facture' sur le document"
          VIEW-AS TOGGLE-BOX
          SIZE 38.29 BY .63
     tex AT ROW 3.67 COL 1.72
          LABEL "Tenir compte des exemplaires soci�t�s et clients"
          VIEW-AS TOGGLE-BOX
          SIZE 42.86 BY .63
     typ-plage AT ROW 4.33 COL 1.43 NO-LABEL
     fn-d AT ROW 4.58 COL 26.43 COLON-ALIGNED
     fn-f AT ROW 4.58 COL 39.29 COLON-ALIGNED
     fdat-d AT ROW 6.08 COL 26.43 COLON-ALIGNED
     fdat-f AT ROW 6.08 COL 38 COLON-ALIGNED
     radio-editpdf AT ROW 7.25 COL 4.29 NO-LABEL WIDGET-ID 26
     RECT-259 AT ROW 2.21 COL 1.14
    WITH 1 DOWN OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 32.43 ROW 6.96
         SIZE 53.14 BY 9.13
         FGCOLOR 23 FONT 49
         TITLE "R��dition plage factures".

DEFINE FRAME Frchoix
     Rsref AT ROW 3.29 COL 2.14 NO-LABEL
     Tbval AT ROW 4.5 COL 19.72
     Ft$selection-2 AT ROW 2.21 COL 2 NO-LABEL WIDGET-ID 20
     RECT-264 AT ROW 2.46 COL 1.14
     RECT-267 AT ROW 4.42 COL 1.14
    WITH 1 DOWN OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 31.86 ROW 7.75
         SIZE 54.86 BY 5.75
         FGCOLOR 23 FONT 49
         TITLE "Pr�f�rences �dition".

DEFINE FRAME flegende
     Legende-lib-652 AT ROW 2.21 COL 1.14 NO-LABEL WIDGET-ID 210
     Legende-lib-648 AT ROW 2.83 COL 4.14 NO-LABEL WIDGET-ID 4
     Legende-lib-654 AT ROW 3.58 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     Legende-lib-658 AT ROW 4.25 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 110
     Legende-lib-661 AT ROW 4.88 COL 4.14 NO-LABEL WIDGET-ID 118
     Legende-lib-659 AT ROW 5.63 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 114
     Legende-lib-653 AT ROW 6.25 COL 4.14 NO-LABEL WIDGET-ID 78
     Legende-lib-664 AT ROW 7 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 80
     Legende-lib-662 AT ROW 7.63 COL 4.14 NO-LABEL WIDGET-ID 120
     Legende-lib-665 AT ROW 8.38 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 122
     Legende-lib-669 AT ROW 9 COL 4.14 NO-LABEL WIDGET-ID 100
     Legende-lib-670 AT ROW 9.75 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 98
     Legende-lib-671 AT ROW 9.75 COL 29.43 COLON-ALIGNED NO-LABEL WIDGET-ID 96
     Legende-lib-672 AT ROW 9.79 COL 47.57 COLON-ALIGNED NO-LABEL WIDGET-ID 106
     Legende-lib-666 AT ROW 10.38 COL 4.14 NO-LABEL WIDGET-ID 142
     Legende-lib-663 AT ROW 11.13 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 140
     Legende-lib-668 AT ROW 11.75 COL 4.14 NO-LABEL WIDGET-ID 168
     Legende-lib-682 AT ROW 12.5 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 166
     Legende-lib-683 AT ROW 12.5 COL 29.43 COLON-ALIGNED NO-LABEL WIDGET-ID 172
     Legende-lib-650 AT ROW 13.33 COL 1.14 NO-LABEL WIDGET-ID 214
     Legende-lib-677 AT ROW 14 COL 4.14 NO-LABEL WIDGET-ID 148
     Legende-lib-667 AT ROW 14.75 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 146
     Legende-lib-679 AT ROW 15.38 COL 4.14 NO-LABEL WIDGET-ID 154
     Legende-lib-678 AT ROW 16.13 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 152
     Legende-lib-681 AT ROW 16.13 COL 29.43 COLON-ALIGNED NO-LABEL WIDGET-ID 162
     Legende-lib-680 AT ROW 16.13 COL 47.57 COLON-ALIGNED NO-LABEL WIDGET-ID 158
     Legende-lib-649 AT ROW 16.75 COL 4.14 NO-LABEL WIDGET-ID 36
     Legende-lib-655 AT ROW 17.5 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     Legende-lib-657 AT ROW 18.17 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     Legende-lib-673 AT ROW 18.75 COL 4.14 NO-LABEL WIDGET-ID 126
     Legende-lib-674 AT ROW 19.5 COL 11.29 COLON-ALIGNED NO-LABEL WIDGET-ID 128
     Legende-lib-675 AT ROW 19.5 COL 29.43 COLON-ALIGNED NO-LABEL WIDGET-ID 130
     Legende-lib-676 AT ROW 19.5 COL 47.72 COLON-ALIGNED NO-LABEL WIDGET-ID 132
     Legende-RECT-368 AT ROW 3.54 COL 6.57 WIDGET-ID 6
     Legende-RECT-380 AT ROW 17.46 COL 6.57 WIDGET-ID 40
     Legende-RECT-382 AT ROW 18.13 COL 6.57 WIDGET-ID 50
     Legende-RECT-388 AT ROW 6.96 COL 6.57 WIDGET-ID 82
     Legende-RECT-391 AT ROW 9.71 COL 24.72 WIDGET-ID 102
     Legende-RECT-392 AT ROW 9.71 COL 6.57 WIDGET-ID 104
     Legende-RECT-393 AT ROW 9.75 COL 42.86 WIDGET-ID 108
     Legende-RECT-369 AT ROW 4.21 COL 6.57 WIDGET-ID 112
     Legende-RECT-370 AT ROW 5.58 COL 6.57 WIDGET-ID 116
     Legende-RECT-389 AT ROW 8.33 COL 6.57 WIDGET-ID 124
     Legende-RECT-394 AT ROW 19.46 COL 24.72 WIDGET-ID 134
     Legende-RECT-395 AT ROW 19.46 COL 6.57 WIDGET-ID 136
     Legende-RECT-396 AT ROW 19.46 COL 42.86 WIDGET-ID 138
     Legende-RECT-371 AT ROW 11.08 COL 6.57 WIDGET-ID 144
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 24.29 ROW 1.17
         SIZE 63.14 BY 21
         FGCOLOR 23 FONT 49 WIDGET-ID 200.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME flegende
     Legende-RECT-372 AT ROW 14.71 COL 6.57 WIDGET-ID 150
     Legende-RECT-373 AT ROW 16.08 COL 6.57 WIDGET-ID 156
     Legende-RECT-374 AT ROW 16.08 COL 42.86 WIDGET-ID 160
     Legende-RECT-375 AT ROW 16.08 COL 24.72 WIDGET-ID 164
     Legende-RECT-376 AT ROW 12.46 COL 6.57 WIDGET-ID 170
     Legende-RECT-377 AT ROW 12.46 COL 24.72 WIDGET-ID 174
     RECT-371 AT ROW 2.54 COL 1.14 WIDGET-ID 212
     RECT-372 AT ROW 13.67 COL 1.14 WIDGET-ID 216
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 24.29 ROW 1.17
         SIZE 63.14 BY 21
         FGCOLOR 23 FONT 49
         TITLE "L�gende" WIDGET-ID 200.

DEFINE FRAME Fadresse
     histoent.civ_liv AT ROW 2.25 COL 16 COLON-ALIGNED WIDGET-ID 24
          LABEL "Civilit�"
          VIEW-AS FILL-IN 
          SIZE 7 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_liv[1] AT ROW 3.21 COL 16 COLON-ALIGNED WIDGET-ID 8
          LABEL "Nom du client"
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_liv[2] AT ROW 4.08 COL 16 COLON-ALIGNED WIDGET-ID 10
          LABEL "Adresse"
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_liv[3] AT ROW 4.96 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.k_post2l AT ROW 5.92 COL 16 COLON-ALIGNED WIDGET-ID 28
          LABEL "Code postal"
          VIEW-AS FILL-IN 
          SIZE 10 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.villel AT ROW 5.92 COL 32 COLON-ALIGNED WIDGET-ID 38
          LABEL "Ville"
          VIEW-AS FILL-IN 
          SIZE 29 BY .83
          BGCOLOR 16 FGCOLOR 23 
     BR-codpaysl AT ROW 6.79 COL 23 WIDGET-ID 18
     wcodpaysl AT ROW 6.83 COL 16 COLON-ALIGNED WIDGET-ID 42
     wlibpaysl AT ROW 6.83 COL 24 COLON-ALIGNED NO-LABEL WIDGET-ID 46
     histoent.civ_fac AT ROW 8.75 COL 16 COLON-ALIGNED WIDGET-ID 22
          LABEL "Civilit�"
          VIEW-AS FILL-IN 
          SIZE 7 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_fac[1] AT ROW 9.71 COL 16 COLON-ALIGNED WIDGET-ID 2
          LABEL "&Nom du client"
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_fac[2] AT ROW 10.58 COL 16 COLON-ALIGNED WIDGET-ID 4
          LABEL "&Adresse"
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.adr_fac[3] AT ROW 11.46 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 45 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.k_post2f AT ROW 12.42 COL 16 COLON-ALIGNED WIDGET-ID 26
          LABEL "Code postal"
          VIEW-AS FILL-IN 
          SIZE 10 BY .83
          BGCOLOR 16 FGCOLOR 23 
     histoent.villef AT ROW 12.42 COL 32 COLON-ALIGNED WIDGET-ID 36
          LABEL "&Ville"
          VIEW-AS FILL-IN 
          SIZE 29 BY .83
          BGCOLOR 16 FGCOLOR 23 
     BR-codpaysf AT ROW 13.29 COL 23 WIDGET-ID 16
     wcodpaysf AT ROW 13.33 COL 16 COLON-ALIGNED WIDGET-ID 40
     wlibpaysf AT ROW 13.33 COL 24 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     Bvalider-adr AT ROW 14.58 COL 33 WIDGET-ID 20
     Babandon-adr AT ROW 14.63 COL 3 WIDGET-ID 14
     " Adresse de livraison" VIEW-AS TEXT
          SIZE 61 BY .63 AT ROW 1.5 COL 3 WIDGET-ID 48
          FGCOLOR 49 FONT 36
     " Adresse de facturation" VIEW-AS TEXT
          SIZE 61 BY .63 AT ROW 8 COL 3 WIDGET-ID 34
          FGCOLOR 49 FONT 36
     RECT-273 AT ROW 1.33 COL 2 WIDGET-ID 30
     RECT-274 AT ROW 14.42 COL 2 WIDGET-ID 32
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 13.29 ROW 1.25
         SCROLLABLE SIZE 64.29 BY 15.63
         BGCOLOR 24 FGCOLOR 23 FONT 49
         TITLE "Modification adresse de facturation" WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W1 ASSIGN
         HIDDEN             = YES
         TITLE              = "Historique Commandes/B.L/Factures"
         COLUMN             = 26.14
         ROW                = 2.46
         HEIGHT             = 21.17
         WIDTH              = 113.29
         MAX-HEIGHT         = 35.46
         MAX-WIDTH          = 205.72
         VIRTUAL-HEIGHT     = 35.46
         VIRTUAL-WIDTH      = 205.72
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         FONT               = 49
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT W1:LOAD-ICON("progiwin.ico":U) THEN
    MESSAGE "Unable to load icon: progiwin.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W1
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frm-composants:FRAME = FRAME F1:HANDLE.

/* SETTINGS FOR FRAME F1
   FRAME-NAME                                                           */
ASSIGN 
       FRAME F1:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON BR1 IN FRAME F1
   NO-DISPLAY                                                           */
ASSIGN 
       Butt-spli:MANUAL-HIGHLIGHT IN FRAME F1 = TRUE
       Butt-spli:MOVABLE IN FRAME F1          = TRUE.

/* SETTINGS FOR FILL-IN Ft$selection-1 IN FRAME F1
   ALIGN-L                                                              */
/* SETTINGS FOR FRAME Fadresse
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME Fadresse:HEIGHT           = 15.87
       FRAME Fadresse:WIDTH            = 67.19.

/* SETTINGS FOR FILL-IN histoent.adr_fac[1] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.adr_fac[2] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.adr_fac[3] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.adr_liv[1] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.adr_liv[2] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.adr_liv[3] IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR BUTTON BR-codpaysf IN FRAME Fadresse
   NO-DISPLAY                                                           */
/* SETTINGS FOR BUTTON BR-codpaysl IN FRAME Fadresse
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN histoent.civ_fac IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.civ_liv IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.k_post2f IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.k_post2l IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.villef IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN histoent.villel IN FRAME Fadresse
   EXP-LABEL                                                            */
/* SETTINGS FOR FRAME Fedtbl
                                                                        */
/* SETTINGS FOR FRAME Fedtfac
                                                                        */
/* SETTINGS FOR FILL-IN fniveau IN FRAME Fedtfac
   ALIGN-L                                                              */
/* SETTINGS FOR FRAME flegende
                                                                        */
/* SETTINGS FOR FILL-IN Legende-lib-648 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-649 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-650 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-652 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-653 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-661 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-662 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-666 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-668 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-669 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-673 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-677 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN Legende-lib-679 IN FRAME flegende
   ALIGN-L                                                              */
/* SETTINGS FOR FRAME Fplafac
                                                                        */
/* SETTINGS FOR FRAME Frchoix
                                                                        */
ASSIGN 
       FRAME Frchoix:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN Ft$selection-2 IN FRAME Frchoix
   ALIGN-L                                                              */
/* SETTINGS FOR FRAME frm-composants
                                                                        */
ASSIGN 
       FRAME frm-composants:HIDDEN           = TRUE
       FRAME frm-composants:MOVABLE          = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W1)
THEN W1:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME Fadresse
/* Query rebuild information for FRAME Fadresse
     _TblList          = "GCO.histoent"
     _Query            is OPENED
*/  /* FRAME Fadresse */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME F1:HANDLE
       ROW             = 5.63
       COLUMN          = 1.29
       HEIGHT          = 16.5
       WIDTH           = 22.14
       HIDDEN          = no
       SENSITIVE       = yes.

CREATE CONTROL-FRAME CtrlFrame-2 ASSIGN
       FRAME           = FRAME F1:HANDLE
       ROW             = 13
       COLUMN          = 7
       HEIGHT          = 1.58
       WIDTH           = 5.43
       HIDDEN          = yes
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {C74190B6-8589-11D1-B16A-00C0F0283628} type: TreeView */
/* CtrlFrame-2 OCXINFO:CREATE-CONTROL from: {2C247F23-8591-11D1-B16A-00C0F0283628} type: ImageList */
      CtrlFrame:MOVE-AFTER(Butt-spli:HANDLE IN FRAME F1).
      CtrlFrame-2:MOVE-AFTER(FRAME frm-composants:HANDLE).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W1 W1
ON END-ERROR OF W1 /* Historique Commandes/B.L/Factures */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W1 W1
ON WINDOW-CLOSE OF W1 /* Historique Commandes/B.L/Factures */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Fplafac
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Fplafac W1
ON ENTRY OF FRAME Fplafac /* R��dition plage factures */
apply "value-changed" to typ-plage.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fadresse
&Scoped-define SELF-NAME BR-codpaysf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-codpaysf W1
ON CHOOSE OF BR-codpaysf IN FRAME Fadresse
DO:
/*$PRO2...*/
    run rectc_b ("PA", output parlib, output parcod).
    if parcod <> "" then do:
        assign wcodpaysf:screen-value = parcod
               wlibpaysf:screen-value = parlib.
        apply "TAB" TO SELF.
    END. /*...$PRO2*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BR-codpaysl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR-codpaysl W1
ON CHOOSE OF BR-codpaysl IN FRAME Fadresse
DO:
/*$PRO2...*/
    run rectc_b ("PA", output parlib, output parcod).
    if parcod <> "" then do:
        assign wcodpaysl:screen-value = parcod
               wlibpaysl:screen-value = parlib.
        apply "TAB" TO SELF.
    END. /*...$PRO2*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F1
&Scoped-define SELF-NAME BR1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BR1 W1
ON CHOOSE OF BR1 IN FRAME F1
DO:
    session:set-wait-state("general").
    parcod=input vcod_cli.
    if rs-selection:SCREEN-VAL = "F" then
        run recfou_b ("","",input-output parcod, output parlib).
    ELSE if rs-selection:SCREEN-VAL = "C" THEN DO:
        run reccli_b ("","",input-output parcod, output parlib).
        /*IF Vcod_cli <> "*" THEN */ CBComboSetSelEx  (hCombars-f1, 2, {&ctypcli}, 1). /*$A35573*/
    END.
    ELSE RUN recaff_b(0,OUTPUT parlib, OUTPUT parcod).
    if parcod <> "" then do:
        assign vcod_cli:screen-value = string(parcod) FLIB1:screen-value = parlib.
        apply "TAB" TO SELF.
    end.
    IF Vcod_cli:SCREEN-VAL <> "" AND Vcod_cli:SCREEN-VAL <> Vcod_cli THEN DO:
        IF tbarbo:CHECKED THEN DO:
          RUN remp-entete.
          TreeSui:Nodes("R0"):SELECTED() = True.
        END.
        RUN openquery("",YES).
    END.
    session:set-wait-state("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Butt-spli
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Butt-spli W1
ON END-MOVE OF Butt-spli IN FRAME F1
DO:
  run splitterMoved.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CtrlFrame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CtrlFrame W1 OCX.NodeClick
PROCEDURE CtrlFrame.TreeView.NodeClick .
/*------------------------------------------------------------------------------
  Purpose:
  Parameters:  Required for OCX.
    Node
  Notes:
------------------------------------------------------------------------------*/
    DEF INPUT PARAM p-Node AS COM-HANDLE NO-UNDO.
    svcle = p-Node:KEY.
    IF Vcod_cli:SCREEN-VAL IN FRAME F1 <> "" THEN
        RUN openquery(svcle,NO).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ent$
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ent$ W1
ON VALUE-CHANGED OF ent$ IN FRAME F1
RUN openquery ("",NO).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Falpha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Falpha W1
ON RETURN OF Falpha IN FRAME F1
DO:
    if falpha:SCREEN-VAL <> "" then do:
        ASSIGN falpha.
        if {&WINDOW-NAME}:hidden=no then
            run openquery("",NO).
        apply "tab" to self.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FNum
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FNum W1
ON RETURN OF FNum IN FRAME F1
DO:
    if input fnum <> 0 then do:
        ASSIGN fnum.
        if {&WINDOW-NAME}:hidden=no then
            run openquery("",NO).
        apply "tab" to self.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs-selection
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-selection W1
ON VALUE-CHANGED OF rs-selection IN FRAME F1
DO:
    CASE rs-selection:SCREEN-VAL :
        WHEN "C" THEN /*$A35573...*/ DO:
            RUN Choose-ctypcli(OUTPUT typclient). /*$A35573*/
            /*vcod_cli:LABEL = Traduction("Client",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")".*/
            CBComboSetSelEx  (hCombars-f1, 2, {&ctypcli}, 1). /*$A35573*/
        END. /*$A35573*/
        WHEN "F" THEN vcod_cli:LABEL = (/*$1728...*/IF ptypmvt = "H":u THEN Traduction("Transporteur",-2,"") ELSE /*...$1728*/ Traduction("Fournisseur",-1,"")) + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")".
        WHEN "A" THEN vcod_cli:LABEL = Traduction("Affaire",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Toutes",-1,"") + ")".
    END CASE.
    ASSIGN
        vcod_cli:SCREEN-VAL = "*" vcod_cli
        FLIB1:SCREEN-VALUE = Traduction("Tous",-1,"").
    RUN openquery("",NO).


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RType W1
ON VALUE-CHANGED OF RType IN FRAME F1
DO:
DO WITH FRAME F1:
    IF vcod_cli:SCREEN-VAL <> "*"
        THEN ASSIGN fnum:HIDDEN=YES Falpha:HIDDEN=YES tbarbo:HIDDEN = NO.
        ELSE ASSIGN fnum:HIDDEN=(RType:screen-value = "BL" AND (Ptypmvt="F"/*$1728..*/ OR ptypmvt = "H":u /*...$1728*/)) Falpha:HIDDEN=NOT (RType:screen-value = "BL" AND (Ptypmvt="F"/*$1728..*/ OR ptypmvt = "H":u /*...$1728*/)) tbarbo:HIDDEN = YES.
    Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-bonCommission}, (RType:screen-value = "F")). /* $BOIS TEN */
    if RType:screen-value = "F" then Do:
        IF Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
        tbapartir:label=Traduction("A partir du n� facture",-2,"").
    END.
    else if RType:screen-value = "C" THEN tbapartir:label=Traduction("A partir du n� commande",-2,"").
    else if RType:screen-value = "BL" then DO:
        ASSIGN tbapartir:label=Traduction("A partir du n� B.L",-2,"").
        IF Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
    END.
    APPLY "value-changed" TO tbarbo.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tbapartir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tbapartir W1
ON VALUE-CHANGED OF Tbapartir IN FRAME F1 /* A partir du N� commande */
DO:
DO WITH FRAME F1:
    if tbapartir:CHECKED then do:
        assign FNum:hidden=(RType:screen-value = "BL" AND (Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/))
               Falpha:HIDDEN=NOT (RType:screen-value = "BL" AND (Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/))
               vcod_cli:hidden=yes
               br1:hidden=yes
               flib1:sensitive=yes flib1:sensitive=no FLib1:hidden=yes
               FDateDeb:hidden=yes
               FDateFin:hidden=YES.
        IF Fnum:hidden=NO THEN APPLY "entry" TO Fnum.
                          ELSE APPLY "entry" TO Falpha.
    end.
    else do:
        assign FNum:hidden=YES
               Falpha:HIDDEN=YES
               vcod_cli:hidden=no
               br1:hidden=no
               FLib1:hidden=no
               FDateDeb:hidden=no
               FDateFin:hidden=no.
        APPLY "value-changed" TO rtype.
    END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tbarbo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tbarbo W1
ON VALUE-CHANGED OF Tbarbo IN FRAME F1 /* Arborescence des pi�ces */
DO:
    ASSIGN ctrlframe:VISIBLE = tbarbo:CHECKED
           Butt-spli:VISIBLE = tbarbo:CHECKED
           Tbapartir:VISIBLE = NOT tbarbo:CHECKED AND ((Ptypmvt<>"F" /*$1728..*/ AND ptypmvt <> "H":u /*...$1728*/)OR rtype:SCREEN-VAL <> "F")
           Tbapartir:CHECKED = NO.
    APPLY "value-changed" TO Tbapartir.
    IF tbarbo:CHECKED THEN DO:
        ASSIGN ctrlframe:WIDTH-P = svtree
               Butt-spli:X       = svbut.
        IF VALID-HANDLE(HBrowse-Ent) THEN HIDE HBrowse-Ent.
        RUN remp-entete.
        TreeSui:Nodes("R0"):SELECTED() = True.
        RUN openquery("",NO).
    END.
    ELSE DO:
        IF Vcod_cli:SCREEN-VAL <> "" THEN
            RUN openquery("",NO).
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fplafac
&Scoped-define SELF-NAME typ-plage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL typ-plage W1
ON VALUE-CHANGED OF typ-plage IN FRAME Fplafac
DO:
  if self:screen-val="N" then assign fn-d:hidden=no fn-f:hidden=no
    fdat-d:hidden=yes fdat-f:hidden=yes fn-d:sensitive=yes fn-f:sensitive=yes.
  else assign fdat-d:hidden=no fdat-f:hidden=no
    fn-d:hidden=yes fn-f:hidden=yes fdat-d:sensitive=yes fdat-f:sensitive=yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F1
&Scoped-define SELF-NAME vcod_cli
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL vcod_cli W1
ON LEAVE OF vcod_cli IN FRAME F1 /* Fournisseur (*=Tous) */
OR RETURN OF vcod_cli DO:
    if (vcod_cli <> input vcod_cli or {&WINDOW-NAME}:hidden=yes) then do:
        parcod=self:screen-value.
        if parcod="*" then do:
            ASSIGN FLIB1:screen-value = Traduction("Tous",-2,"") tbarbo:CHECKED = NO tbarbo:HIDDEN = YES
                   ctrlframe:VISIBLE = tbarbo:CHECKED
                   Butt-spli:VISIBLE = tbarbo:CHECKED
                   Tbapartir:VISIBLE = NOT tbarbo:CHECKED AND ((Ptypmvt<>"F" /*$1728..*/ AND ptypmvt = "H":u /*...$1728*/) OR rtype:SCREEN-VAL <> "F")
                   Tbapartir:CHECKED = NO.
        END.
        ELSE DO:
            IF rs-selection:SCREEN-VALUE = "C" THEN
            ASSIGN fnum:HIDDEN=YES Falpha:HIDDEN = YES tbarbo:HIDDEN = NO.
            if RType:screen-value = "F" then DO:
                IF Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
                tbapartir:label=Traduction("A partir du n� facture",-2,"").
            END.
            else if RType:screen-value = "C" then tbapartir:label=Traduction("A partir du n� commande",-2,"").
            else if RType:screen-value = "BL" THEN DO:
                tbapartir:label=Traduction("A partir du n� B.L",-2,"").
                IF Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
            END.

            IF rs-selection:SCREEN-VAL = "A" THEN RUN AFF_GET_COD_LIB IN G-hAff (INPUT-OUTPUT parcod, OUTPUT parlib, YES, "", OUTPUT logbid).
            ELSE if pTypMvt = "C" then run rech-cli ("","",input-output parcod, output parlib).
            else run rech-fou ("","",input-output parcod, output parlib).
            assign vcod_cli:screen-value = string(parcod) FLIB1:screen-value = parlib.
        END.
    end.
    IF Vcod_cli:SCREEN-VAL <> "" AND Vcod_cli:SCREEN-VAL <> Vcod_cli THEN DO:
        IF tbarbo:CHECKED THEN DO:
            RUN remp-entete.
            TreeSui:Nodes("R0"):SELECTED() = True.
        END.
        RUN openquery("",NO).
    END.
    if last-event:function="return" then do:
        apply "tab" to br1.
        return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME Fadresse
&Scoped-define SELF-NAME wcodpaysf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wcodpaysf W1
ON LEAVE OF wcodpaysf IN FRAME Fadresse /* Pays */
DO:
   /*$PRO2...*/
   if wcodpaysf:screen-value <> " " then do: 
        find tabcomp where tabcomp.type_tab = "PA"
                     and tabcomp.a_tab = input wcodpaysf
                     no-lock no-error.
        if available tabcomp then do:
            display tabcomp.inti_tab @ wlibpaysf with frame Fadresse.
            APPLY "TAB" TO SELF.
        end.
        else apply "choose" to BR-codpaysf.
   end.
   ELSE apply "choose" to BR-codpaysf. /*...$PRO2*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME wcodpaysl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wcodpaysl W1
ON LEAVE OF wcodpaysl IN FRAME Fadresse /* Pays */
DO:
   /*$PRO2...*/
   if wcodpaysl:screen-value <> " " then do: 
        find tabcomp where tabcomp.type_tab = "PA"
                     and tabcomp.a_tab = input wcodpaysl
                     no-lock no-error.
        if available tabcomp then do:
            display tabcomp.inti_tab @ wlibpaysl with frame Fadresse.
            APPLY "TAB" TO SELF.
        end.
        else apply "choose" to BR-codpaysl.
   end.
   ELSE apply "choose" to BR-codpaysl. /*...$PRO2*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W1 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME}
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
       FRAME F1:SCROLLABLE = NO
       {&WINDOW-NAME}:PRIVATE-D=(IF ptypmvt="F" THEN "124" ELSE /*$1728...*/ IF ptypmvt="H" THEN "706" ELSE /*...$1728*/ "126").


/* $Trad */     TraitementArbre(YES,FRAME F1:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME frm-composants:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME Frchoix:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME Fplafac:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME Fedtfac:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME Fedtbl:HANDLE,'','').
/* $Trad */     TraitementArbre(YES,FRAME flegende:HANDLE,'','').
/* $Trad */     TraitementArbre(NO,W1:HANDLE,'','').

            TraitementArbre(YES,FRAME FLegende:HANDLE,'','').

ON Alt-Cursor-Up OF frame F1       ANYWHERE logbid=CBFocusItem(hcombars-f1       , 2, {&Bquitter}).
ON Alt-Cursor-Up OF frame fedtfac  ANYWHERE logbid=CBFocusItem(hcombars-fedtfac       , 2, {&Bannuler-r}).
ON Alt-Cursor-Up OF frame fedtbl   ANYWHERE logbid=CBFocusItem(hcombars-fedtbl       , 2, {&Bannuler-edtbl}). /*$A59971*/
ON Alt-Cursor-Up OF frame fplafac  ANYWHERE logbid=CBFocusItem(hcombars-fplafac       , 2, {&Bannuler-p}).
ON Alt-Cursor-Up OF frame Frchoix  ANYWHERE logbid=CBFocusItem(hcombars-frchoix       , 2, {&Bretour}).

ON F1 ANYWHERE DO:
    IF ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN AidezMoi("pgi_achats", INT({&WINDOW-NAME}:PRIVATE-D), ?, "").
    ELSE AidezMoi("pgi_ventes", INT({&WINDOW-NAME}:PRIVATE-D), ?, "").
END.

ON WINDOW-CLOSE, END-ERROR, ENDKEY OF FRAME FLegende ANYWHERE DO:
    RUN CHOOSE-blegende-abandonner.
    RETURN NO-APPLY.
END.
ON END-ERROR OF FRAME F1 RUN CHOOSE-bquitter.
ON END-ERROR OF {&WINDOW-NAME} OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
    RUN choose-Bquitter.
    RETURN NO-APPLY.
END.

/*$A35790...*/
ON F5 OF FRAME F1 ANYWHERE DO:
    IF CBGetItemEnableState (Hcombars-f1, 2, {&bafficher}) AND CBGetItemShowState (Hcombars-f1, 2, {&bafficher}) THEN RUN CHOOSE-BAFFICHER.
    RETURN NO-APPLY.
END.
/*...$A35790*/

ON f11 OF FRAME F1 ANYWHERE RUN CHOOSE-Baffac.
ON f10 OF FRAME F1 ANYWHERE RUN CHOOSE-bdetail.

ON START-SEARCH OF FRAME F1 ANYWHERE DO:
    IF SELF:TYPE <> "Browse" THEN RETURN NO-APPLY.
    hFld = SELF:CURRENT-COLUMN.
    IF hFld:NAME = ? OR SUBSTR(hFld:NAME,1,1)="*" THEN RETURN NO-APPLY. /* $A54459 */

    IF SELF:PRIVATE-D="ent":U THEN DO:
        IF (Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/) AND rtype:SCREEN-VAL = "F" THEN ASSIGN
            COLDESC-ent2 = (IF COLORD-ent2 = hFld:NAME THEN NOT COLDESC-ent2 ELSE NO)
            CHQRYBY-ent2 = "BY " + hFld:NAME + (IF COLDESC-ent2 THEN " DESC" ELSE "")
            COLORD-ent2 = hFld:NAME.
        ELSE ASSIGN
            COLDESC-ent = (IF COLORD-ent = hFld:NAME THEN NOT COLDESC-ent ELSE NO)
            CHQRYBY-ent = "BY " + hFld:NAME + (IF COLDESC-ent THEN " DESC" ELSE "")
            COLORD-ent = hFld:NAME.
        RUN OpenQuery("",NO).
        APPLY "Entry" TO HBrowse-Ent.
    END.
    ELSE DO:
        IF (Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/) AND rtype:SCREEN-VAL = "F" THEN DO:  /* $A54459 */
            ASSIGN
                COLDESC-lig2 = (IF COLORD-lig2 = Hfld:NAME THEN NOT COLDESC-lig2 ELSE NO)
                CHQRYBY-lig2 = "BY " + Hfld:NAME + (IF COLDESC-lig2 THEN " DESC" ELSE "")
                COLORD-lig2 = Hfld:NAME.
                        /* $A54459... */
            RUN creation-browse-ligfac.
            if AVAIL ligfacfo THEN Hbrowse-lig:hidden=NO.
            else Hbrowse-lig:hidden=TRUE.
        END. /* ...$A54459 */
        ELSE DO: /* $A54459 */
            ASSIGN
                COLDESC-lig = (IF COLORD-lig = Hfld:NAME THEN NOT COLDESC-lig ELSE NO)
                CHQRYBY-lig = "BY " + Hfld:NAME + (IF COLDESC-lig THEN " DESC" ELSE "")
                COLORD-lig = Hfld:NAME.
                        /* $A54459... */
            RUN creation-browse-ligne.
            if AVAIL histolig THEN Hbrowse-lig:hidden=NO.
            else Hbrowse-lig:hidden=TRUE.
                        /* ...$A54459 */
        END.
        /*RUN openquery ("",NO). $A54459 */
        APPLY "entry" TO hbrowse-lig.
    END.
END.

ON 'END-RESIZE':U OF FRAME f1 ANYWHERE DO:
    IF ((VALID-HANDLE(hbrowse-ent) AND SELF:PARENT = hbrowse-ent:HANDLE)
        OR (VALID-HANDLE(hbrowse-lig) AND SELF:PARENT = hbrowse-lig:HANDLE)) AND SELF:NAME <> ? THEN DO:
        IF loadsettings("HBHI" + (IF VALID-HANDLE(hbrowse-ent) AND SELF:PARENT = hbrowse-ent:HANDLE THEN "S" ELSE "L") + Ptypmvt + (IF Ptypmvt="F" AND rtype:SCREEN-VAL="F" THEN "3" ELSE ""), output hsettingsapi) then do:
            SaveSetting(SELF:NAME, STRING(SELF:WIDTH)).
            UnloadSettings(hSettingsApi).
        END.
    END.
END.

ON 'CTRL-F4':U OF FRAME f1 ANYWHERE APPLY "entry" TO csoc.
ON VALUE-CHANGED OF csoc IN FRAME f1 DO:
    IF self:TEXT-SELECTED THEN DO:
        ASSIGN
            n_dep=(IF SUBSTR(csoc:SCREEN-VAL,1,1) = "D" AND INT(SUBSTR(csoc:SCREEN-VAL,2)) > 0 THEN INT(SUBSTR(csoc:SCREEN-VAL,2)) ELSE 0)
            n_soc=(IF SUBSTR(csoc:SCREEN-VAL,1,1) = "S" AND INT(SUBSTR(csoc:SCREEN-VAL,2)) > 0 THEN INT(SUBSTR(csoc:SCREEN-VAL,2)) ELSE 0).
        IF tbarbo:CHECKED THEN DO:
            RUN remp-entete.
            TreeSui:Nodes("R0"):SELECTED() = True.
        END.
        RUN openquery("",NO).

        /* $A40207 ... */
        FIND FIRST tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab = INT(SUBSTR(csoc:SCREEN-VAL,2)) NO-LOCK NO-ERROR.
        IF AVAIL tabgco AND NOT tabgco.non_sscc THEN Logbid = CBEnableItem (Hcombars-f1, 2, {&Bsscc}, YES).
        ELSE Logbid = CBEnableItem (Hcombars-f1, 2, {&Bsscc}, NO).
        /* ... $A40207 */
    END.
END.
ON LEAVE OF csoc IN FRAME f1 DO:
    IF self:LOOKUP(SELF:SCREEN-VAL) = 0 THEN SELF:SCREEN-VALUE = ?. /*$A77918*/
    IF SELF:SCREEN-VAL=? THEN DO:
        SELF:SCREEN-VAL=SELF:ENTRY(1).
        APPLY "value-changed" TO SELF.
    END.
END.

/* Bouton droit : Menu contextuel */
ON MOUSE-MENU-CLICK OF {&WINDOW-NAME} OR ALT-F1 OF {&WINDOW-NAME} ANYWHERE DO:
    if SELF:PRIVATE-D = "ligfa":U AND AVAIL ligfacfo AND can-do("AR,DF,PS",ligfacfo.sous_type) then
        run popup_o (string(ligfacfo.cod_fou),ligfacfo.cod_pro,ligfacfo.cod_dec1,ligfacfo.cod_dec2,ligfacfo.cod_dec3,ligfacfo.cod_dec4,ligfacfo.cod_dec5,"P" + Ptypmvt + STRING("0","999999999") + STRING(ligfacfo.qte),0,"histfb_c","").
    ELSE if SELF:PRIVATE-D = "lig" AND can-do("AR,DF,PS",histolig.sous_type) then
        run popup_o (string(histolig.cod_cf),histolig.cod_pro,histolig.cod_dec1,histolig.cod_dec2,histolig.cod_dec3,histolig.cod_dec4,histolig.cod_dec5,"P" + Ptypmvt + STRING(histolig.NO_of,"999999999") + STRING(histolig.qte),histolig.depot,"histfb_c",histolig.k_var).
    else IF (ptypmvt="F" /*$1728...*/ OR ptypmvt = "H":u /*...$1728*/ )AND rtype:SCREEN-VAL IN FRAME f1 = "F" AND AVAIL entfacfo THEN
        run popup_o (string(entfacfo.cod_fou),0,"","","","","",PTYPMVT,0,"Histfb_c","").
    ELSE IF AVAIL histoent THEN
        run popup_o (string(histoent.cod_cf),0,"","","","","",PTYPMVT,0,"Histfb_c","").
END.

/* Triggers Return --> tabulation */
ON RETURN OF W1 ANYWHERE DO:
    if can-do("fill-in,toggle-box,radio-set,combo-box",self:type) then do:
      apply "tab" to self.
      return no-apply.
    end.
    else if self:type="editor" then logbid=self:insert-string("~n").
    else if self:type="button" then apply "choose" to self.
    else if can-do("browse,selection-list",self:type) then apply "default-action" to self.
END.

ON F9 OF FRAME FPLAFAC ANYWHERE RUN CHOOSE-bvalider-p.
ON F9 OF FRAME FEDTFAC ANYWHERE RUN CHOOSE-bvalider-r.
ON F9 OF FRAME FEDTBL  ANYWHERE RUN CHOOSE-bvalider-edtbl.
ON F9 OF FRAME FRCHOIX ANYWHERE RUN CHOOSE-bediterc.
ON F9 OF FRAME FADRESSE ANYWHERE RUN CHOOSE-badresse.


ON SHIFT-F8 ANYWHERE DO:
    if CBGetItemEnableState (Hcombars-f1, 2, {&baffcde}) THEN RUN choose-Baffcde.
END.

/*$A40504...*/
ON CTRL-F8 ANYWHERE DO:
    IF SELF:TYPE = "BROWSE" THEN DO:
        IF SELF:HANDLE = HBrowse-c THEN DO :
            IF NOT VALID-HANDLE (AffSVariante) THEN
                RUN afsvar2_gp PERSISTENT SET AffSVariante ("","").
            IF AVAIL histolig2 THEN
                RUN AffichageVariante IN AffSVariante (histolig2.k_var + (IF histolig2.k_opt <> "" THEN CHR (1) + histolig2.k_opt ELSE "" /* $A35329 */) ,STRING(histolig2.cod_pro)).
        END.
        ELSE RUN CHOOSE-Btn-varctrlf8.
    END.
END.
/*...$A40504*/

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE DO:
    /* $2222 ... */
    IF VALID-HANDLE (xitem-edt) THEN DO:
        RELEASE OBJECT xitem-edt NO-ERROR.
        xitem-edt = ?.
    END.
    /* ... $2222 */
    IF VALID-HANDLE(hapi-piece-cl) THEN APPLY "close" TO hapi-piece-cl. /* $A38917 */
    IF VALID-HANDLE(hapi-piece-fo) THEN APPLY "close" TO hapi-piece-fo. /* $A39559 */
    if valid-handle(Hpostit) then apply "close" to hpostit.
    IF VALID-HANDLE(hapi-mobsr) THEN APPLY "close" TO hapi-mobsr.
    IF VALID-HANDLE(HndGcoErg)  THEN APPLY "CLOSE" TO HndGcoerg.
    IF VALID-HANDLE(hComBars)   THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE(hndChkQDyn) THEN APPLY "CLOSE" TO hndChkQDyn.
    IF VALID-HANDLE (dynreq) THEN RUN FermerFrame IN dynreq. /* $1495 */
    IF VALID-HANDLE (dynreq_facfo) THEN RUN FermerFrame IN dynreq_facfo. /* $1495 */
    IF VALID-HANDLE (dynreq_histolig) THEN RUN FermerFrame IN dynreq_histolig. /* $1495 */
    IF VALID-HANDLE(AffSVariante) THEN APPLY "close" TO AffSVariante. /*$1208*/
    RUN disable_UI.
END.


/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

ON RETURN OF Fdatedeb,Fdatefin IN FRAME f1 ANYWHERE DO:
    IF Vcod_cli:SCREEN-VAL <> "" AND self:SCREEN-VAL <> STRING(IF self:NAME = "Fdatedeb" THEN Fdatedeb ELSE Fdatefin) THEN DO:
        IF tbarbo:CHECKED THEN DO:
          RUN remp-entete.
          TreeSui:Nodes("R0"):SELECTED() = True.
        END.
        RUN openquery("",NO).
    END.
END.
ON LEAVE OF Fdatedeb,Fdatefin IN FRAME f1 ANYWHERE Do:
    IF Vcod_cli:SCREEN-VAL <> "" AND self:SCREEN-VAL <> STRING(IF self:NAME = "Fdatedeb" THEN Fdatedeb ELSE Fdatefin) THEN DO:
        IF tbarbo:CHECKED THEN DO:
          RUN remp-entete.
          TreeSui:Nodes("R0"):SELECTED() = True.
        END.
        RUN openquery("",NO).
    END.
END.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  VIEW FRAME f1 IN WINDOW w1.
  RUN CombarsCreation.
  RUN RESIZEWINDOW.
  IF NOT VALID-HANDLE (dynreq) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = Ptypmvt and cleval.section = "histoent" NO-LOCK) /* $1495 */
                                    OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = Ptypmvt + "�" + g-user and cleval.section = "histoent" NO-LOCK)) THEN  /* $1495 */
  DO:
      RUN dynsel_b PERSISTENT SET dynreq (ptypmvt,"histoent",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse","histfb_c" + Ptypmvt) .
      RUN ApparenceFrame IN dynreq (0,Traduction("en-t�te",-2,""),"").
      RUN CreerFrame IN dynreq.
      RUN AfficherFrame IN dynreq.
  END.
  IF pTypMvt = "F" AND NOT VALID-HANDLE (dynreq_facfo) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "" and cleval.section = "entfacfo" NO-LOCK) /* $1495 */
                                    OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "�" + g-user and cleval.section = "entfacfo" NO-LOCK)) THEN  /* $1495 */
  DO:
      RUN dynsel_b PERSISTENT SET dynreq_facfo ("","entfacfo",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse","histfb_c") .
      RUN ApparenceFrame IN dynreq_facfo (83,Traduction("facture",-2,""),"").
      RUN CreerFrame IN dynreq_facfo.
      RUN AfficherFrame IN dynreq_facfo.
  END.
  IF NOT VALID-HANDLE (dynreq_histolig) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "P" and cleval.section = "histolig" NO-LOCK)
                                    OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "P" + "�" + g-user and cleval.section = "histolig" NO-LOCK)
                                     OR CAN-FIND (FIRST reqpersa WHERE reqpersa.nom_pgm = "HIL":U + ptypmvt AND reqpersa.nom_sspgm = "" AND reqpersa.cod_user = g-user NO-LOCK) /*$A91177*/
                                     OR CAN-FIND (FIRST reqpersa WHERE reqpersa.nom_pgm = "HIL":U + ptypmvt AND reqpersa.nom_sspgm = "" AND reqpersa.cod_user = "" NO-LOCK)) THEN  /* $1495 */
  DO:
      RUN apicomp_b PERSISTENT SET dynreq_histolig ("p","histolig",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse":U,"histfb_c" + Ptypmvt,"HIL":U + ptypmvt,"") . /*$A91177*/
      RUN ApparenceFrame IN dynreq_histolig (83,Traduction("ligne",-2,""),"").
      RUN CreerFrame IN dynreq_histolig.
      RUN AfficherFrame IN dynreq_histolig.
  END.

  RUN proc-init.

  IF Vcod_cli:SCREEN-VAL <> "" THEN DO:
      IF tbarbo:CHECKED THEN DO:
          RUN remp-entete.
          TreeSui:Nodes("R0"):SELECTED() = True.
      END.
      RUN openquery("",NO).
  END.
  ELSE
      ASSIGN Logbid = CBShowItem(Hcombars-f1, 2, {&bmail}, NO)
             Logbid = CBShowItem(Hcombars-f1, 2, {&bfax}, NO). /* $A66083 */

  VIEW W1.
  session:set-wait ("").
  APPLY "entry" TO w1.
  APPLY "entry" TO Vcod_cli.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

{rech-cli.i}
{rech-fou.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Actu-Composants W1 
PROCEDURE Actu-Composants :
/* $A33837... */
    IF NOT creerBrwsComp THEN DO:
        /*Correction*/
        DELETE OBJECT HBrowse-c NO-ERROR.
        DELETE OBJECT HQuery-c NO-ERROR.
        /*Correction*/
        ASSIGN
            qliste-champ-c = "cod_pro,nom_pro"  + (IF g-langue <> "" THEN ",lldespro.des_lan" ELSE "") /*A35891*/ + ",qte,":U +
                             /* $A54375 ... */ (IF NOT pas-droit-vpa THEN "px_ach," ELSE "") +
                             (IF NOT pas-droit-vpr THEN "px_rvt," ELSE "") +
                             (IF NOT pas-droit-vpm THEN "pmp,":U ELSE "") + /* $A54375 */
                             "px_vte,gratuit,refint,cod_dec1,cod_dec2,cod_dec3,cod_dec4,cod_dec5,k_var"
            qliste-label-c = Traduction("Code",-2,"") + "|" + Traduction("Intitul�",-2,"")  + (IF g-langue <> "" THEN "|" + Traduction("D�signation Produit",-2,"") + " " + g-langue ELSE "") + "|" + Traduction("Quantit�",-2,"") + "|" +
                             /* $A54375 ... */ (IF NOT pas-droit-vpa THEN Traduction("Px Achat",-2,"") + "|" ELSE "") +
                             (IF NOT pas-droit-vpr THEN Traduction("Px revient",-2,"") + "|" ELSE "") +
                             (IF NOT pas-droit-vpm THEN Traduction("P.M.P",-2,"") + "|" ELSE "") + /* ... $A54375 */  Traduction("Px Vte",-2,"") + "|" + Traduction("Gratuit",-2,"") + "|" + Traduction("R�f. Interne",-2,"") + "|" + Traduction("Dec1",-2,"") + "|" + Traduction("Dec2",-2,"") + "|" + Traduction("Dec3",-2,"") + "|" + Traduction("Dec4",-2,"") + "|" + Traduction("Dec5",-2,"") + "|" + Traduction("Variantes",-2,"").

        CREATE BROWSE HBrowse-c
        ASSIGN FRAME    = FRAME frm-composants:HANDLE
            X        = 10
            Y        = 10
            WIDTH-P  = FRAME frm-composants:WIDTH-P - 20
            HEIGHT-P = FRAME frm-composants:HEIGHT-P - 40
            BGCOLOR = 48
            FONT = 49
            SENSITIVE  = YES
            EXPANDABLE = YES
            SEPARATORS = YES
            ROW-MARKERS = NO
            PRIVATE-D = "hbrowse-c"
            COLUMN-SCROLLING=YES
            COLUMN-RESIZABLE=YES.

        /* Creation du query */
        CREATE QUERY HQuery-c.
        HQuery-c:CACHE=10000.
        HQuery-c:SET-BUFFERS(BUFFER histolig2:HANDLE).

        IF INDEX(qliste-champ-c,"lldespro.":U)<>0 THEN DO:
            qprepare-lldespro=", first lldespro where lldespro.cod_pro = histolig2.cod_pro AND lldespro.langue = '" + g-langue + "' NO-LOCK OUTER-JOIN ".
            HQuery-c:add-BUFFER(BUFFER lldespro:HANDLE).
        END.
        ELSE qprepare-lldespro="".

    END.
    ELSE DO:
        IF INDEX(qliste-champ-c,"lldespro.":U)<>0 THEN qprepare-lldespro=", first lldespro where lldespro.cod_pro = histolig2.cod_pro AND lldespro.langue = '" + g-langue + "' NO-LOCK OUTER-JOIN ".
        ELSE qprepare-lldespro="".
    END.

     /* A35891 Infos d�signation langue */
    logbid = HQuery-c:QUERY-PREPARE("FOR EACH HISTOLIG2 WHERE HISTOLIG2.typ_mvt = '" + histolig.typ_mvt + "' AND HISTOLIG2.cod_cf = " + STRING(histolig.cod_cf) +
        " AND HISTOLIG2.no_cde = " + STRING(histolig.NO_cde) + " AND HISTOLIG2.no_bl = " + STRING(HISTOLIG2.no_bl) + " AND HISTOLIG2.div_fac = " + STRING(HISTOLIG.div_fac) +
        " AND HISTOLIG2.nmc_lie = 'S' AND HISTOLIG2.lien_nmc = " + STRING(HISTOLIG.lien_nmc) + " no-lock " + qprepare-lldespro /*A35891*/ ).

    IF NOT logbid THEN MESSAGE Traduction("Probl�me dans la condition !",-2,"") SKIP STRING("FOR EACH HISTOLIG2 WHERE HISTOLIG2.typ_mvt = '" + histolig.typ_mvt + "' AND HISTOLIG2.cod_cf = " + STRING(histolig.cod_cf) +
        " AND HISTOLIG2.no_cde = " + STRING(histolig.NO_cde) + " AND HISTOLIG2.no_bl = " + STRING(HISTOLIG2.no_bl) + " AND HISTOLIG2.div_fac = " + STRING(HISTOLIG.div_fac) +
        " AND HISTOLIG2.nmc_lie = 'S' AND HISTOLIG2.lien_nmc = " + STRING(HISTOLIG.lien_nmc) + qprepare-lldespro) VIEW-AS ALERT-BOX INFO.
    ELSE DO:

        HBrowse-c:QUERY = HQuery-c.

        IF NOT creerBrwsComp THEN DO ii = 1 TO NUM-ENTRIES(qliste-champ-c) ON ERROR UNDO,LEAVE ON STOP UNDO,LEAVE:

            IF (qprepare-lldespro<>"" AND SUBSTR(ENTRY(ii,qliste-champ-c),1,9)="lldespro.":U) THEN HCol-c=HBrowse-c:ADD-LIKE-COLUMN(ENTRY(ii,qliste-champ-c)). /*A35891*//*Correction*/
            ELSE
            HCol-c = HBrowse-c:ADD-LIKE-COLUMN("histolig." + ENTRY(ii,qliste-champ-c)).
            ASSIGN
                HCol-c:LABEL=ENTRY(1,ENTRY(ii,qliste-label-c,"|"),"�")
                Hcol-c:NAME = ENTRY(ii,qliste-champ-c)
                HCol-c:LABEL-BGCOLOR = ?
                HCol-c:LABEL-FGCOLOR = ?.

            IF hcol-c:DATA-TYPE = "logical" AND hcol-c:BUFFER-FIELD:FORMAT = "{&UTrema}/" THEN hcol-c:COLUMN-FONT = 47.
            IF hcol-c:NAME = "k_var" THEN hcol-c:WIDTH = 100. /*$A40504*/

        END.

        SESSION:SET-WAIT-STATE("").

        HQuery-c:QUERY-OPEN().
        TRACEQUERY(hquery-c:HANDLE).

        ASSIGN
            creerBrwsComp  = YES
            HBrowse-c:HIDDEN = NO
            FRAME frm-composants:HIDDEN = NO.
    END.
/* ...$A33837 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE afficherComparaison W1 
PROCEDURE afficherComparaison :
DEF VAR hBuffer AS HANDLE NO-UNDO.
    IF AVAIL histoent THEN DO:
        hBuffer = BUFFER histoent:HANDLE.
        RUN compdev_c (hBuffer).
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aTrad$VariableInitial W1 
PROCEDURE aTrad$VariableInitial :
ASSIGN
    freel:SCREEN-VAL IN FRAME F1 = Traduction("Estim�s",-1,"") freel /* $tradVar */
    Ft$selection-1:SCREEN-VAL IN FRAME F1 = Traduction("S�lection pour affichage",-1,"") Ft$selection-1 /* $tradVar */
    FTxtAucun:SCREEN-VAL IN FRAME F1 = Traduction("Aucun r�sultat",-1,"") FTxtAucun /* $tradVar */
    fniveau:SCREEN-VAL IN FRAME Fedtfac = Traduction("Dernier niveau � �diter",-1,"") fniveau /* $tradVar */
                Legende-lib-648:SCREEN-VAL IN FRAME flegende = Traduction("N� Facture ou N� B.L Fournisseur",-2,"") + " " + "(" + Traduction("Achat",-2,"") + ")" Legende-lib-648 /* $tradVar */
                Legende-lib-649:SCREEN-VAL IN FRAME flegende = Traduction("Quantit�",-2,"") Legende-lib-649 /* $tradVar */
                Legende-lib-650:SCREEN-VAL IN FRAME flegende = Traduction("Lignes",-2,"") Legende-lib-650 /* $tradVar */
                Legende-lib-652:SCREEN-VAL IN FRAME flegende = Traduction("En-t�tes",-2,"") Legende-lib-652 /* $tradVar */
                Legende-lib-653:SCREEN-VAL IN FRAME flegende = Traduction("N� B.L Fournisseur",-2,"") + " " + "(" + Traduction("Achat",-2,"") + ")" Legende-lib-653 /* $tradVar */
                Legende-lib-654:SCREEN-VAL IN FRAME flegende = Traduction("litige non r�solu",-2,"") Legende-lib-654 /* $tradVar */
                Legende-lib-655:SCREEN-VAL IN FRAME flegende = Traduction("compos� commercial",-2,"") Legende-lib-655 /* $tradVar */
                Legende-lib-657:SCREEN-VAL IN FRAME flegende = Traduction("composant commercial",-2,"") Legende-lib-657 /* $tradVar */
                Legende-lib-658:SCREEN-VAL IN FRAME flegende = Traduction("litige r�solu",-2,"") Legende-lib-658 /* $tradVar */
                Legende-lib-659:SCREEN-VAL IN FRAME flegende = Traduction("n� facture renseign�",-2,"") Legende-lib-659 /* $tradVar */
                Legende-lib-661:SCREEN-VAL IN FRAME flegende = Traduction("N� Facture et Facture H.T",-2,"") + " " + "(" + Traduction("Achat",-2,"") + ")" Legende-lib-661 /* $tradVar */
                Legende-lib-662:SCREEN-VAL IN FRAME flegende = Traduction("Date livraison",-2,"") Legende-lib-662 /* $tradVar */
                Legende-lib-663:SCREEN-VAL IN FRAME flegende = Traduction("consignation",-2,"") Legende-lib-663 /* $tradVar */
                Legende-lib-664:SCREEN-VAL IN FRAME flegende = Traduction("r�ception li�e � la WMS non valid�e",-2,"") Legende-lib-664 /* $tradVar */
                Legende-lib-665:SCREEN-VAL IN FRAME flegende = Traduction("Livraison en retard",-2,"") Legende-lib-665 /* $tradVar */
                Legende-lib-666:SCREEN-VAL IN FRAME flegende = Traduction("N� Commande",-2,"") Legende-lib-666 /* $tradVar */
                .

        ASSIGN
                Legende-lib-667:SCREEN-VAL IN FRAME flegende = Traduction("prix ou remises forc�s",-2,"") Legende-lib-667 /* $tradVar */
                Legende-lib-668:SCREEN-VAL IN FRAME flegende = Traduction("R�f. cde",-2,"") Legende-lib-668 /* $tradVar */
                Legende-lib-669:SCREEN-VAL IN FRAME flegende = Traduction("N� Facture",-2,"") + " " + "(" + Traduction("Vente",-2,"") + ")" Legende-lib-669 /* $tradVar */
                Legende-lib-670:SCREEN-VAL IN FRAME flegende = Traduction("non r�gl�",-2,"") Legende-lib-670 /* $tradVar */
                Legende-lib-671:SCREEN-VAL IN FRAME flegende = Traduction("en partie r�gl�",-2,"") Legende-lib-671 /* $tradVar */
                Legende-lib-672:SCREEN-VAL IN FRAME flegende = Traduction("r�gl�",-2,"") Legende-lib-672 /* $tradVar */
                Legende-lib-673:SCREEN-VAL IN FRAME flegende = Traduction("Montant H.T",-2,"") Legende-lib-673 /* $tradVar */
                Legende-lib-674:SCREEN-VAL IN FRAME flegende = Traduction("promotion",-2,"") Legende-lib-674 /* $tradVar */
                Legende-lib-675:SCREEN-VAL IN FRAME flegende = Traduction("gratuit",-2,"") Legende-lib-675 /* $tradVar */
                Legende-lib-676:SCREEN-VAL IN FRAME flegende = Traduction("garantie",-2,"") Legende-lib-676 /* $tradVar */
                Legende-lib-677:SCREEN-VAL IN FRAME flegende = Traduction("Prix",-2,"") Legende-lib-677 /* $tradVar */
                Legende-lib-678:SCREEN-VAL IN FRAME flegende = Traduction("prestation",-2,"") Legende-lib-678 /* $tradVar */
                Legende-lib-679:SCREEN-VAL IN FRAME flegende = Traduction("Intitul�",-2,"") + "," + " " + Traduction("R�f�rences",-2,"") + "," + " ..." Legende-lib-679 /* $tradVar */
                Legende-lib-680:SCREEN-VAL IN FRAME flegende = Traduction("non r�f�renc�",-2,"") Legende-lib-680 /* $tradVar */
                Legende-lib-681:SCREEN-VAL IN FRAME flegende = Traduction("divers factur�",-2,"") Legende-lib-681 /* $tradVar */
                Legende-lib-682:SCREEN-VAL IN FRAME flegende = Traduction("cession",-2,"") Legende-lib-682 /* $tradVar */
                Legende-lib-683:SCREEN-VAL IN FRAME flegende = Traduction("r�trocession",-2,"") Legende-lib-683 /* $tradVar */
    Ft$selection-2:SCREEN-VAL IN FRAME Frchoix = Traduction("Pr�f�rence produit",-1,"") Ft$selection-2 /* $tradVar */.
                .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-badresse W1 
PROCEDURE CHOOSE-badresse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*$PRO2...*/
 IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
    IF CAN-FIND(droigco WHERE droigco.util = g-user-droit AND droigco.fonc = "ECLFACT") THEN DO:
        MESSAGE "Vous n'avez pas les droits pour d�nommer les factures !" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        /* Contr�le */
        DO TRANSACTION:
            FIND CURRENT histoent EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
            IF NOT AVAILABLE histoent THEN MESSAGE "Commande verrouill�e ! R�it�rez votre demande ult�rieurement !" VIEW-AS ALERT-BOX INFORMATION.
            ELSE DO:
                FIND tabcomp WHERE tabcomp.type_tab = "PA"
                             AND tabcomp.a_tab = histoent.paysf NO-LOCK NO-ERROR.
                IF AVAILABLE tabcomp THEN ASSIGN wcodpaysf = histoent.paysf
                                                 wlibpaysf:SCREEN-VALUE IN FRAME Fadresse = tabcomp.inti_tab.
                ELSE ASSIGN wcodpaysf:SCREEN-VALUE = ""
                            wlibpaysf:SCREEN-VALUE = "".
    
                FIND tabcomp WHERE tabcomp.TYPE_tab = "PA"
                               AND tabcomp.a_tab = histoent.paysl NO-LOCK NO-ERROR. 
                IF AVAIL tabcomp THEN ASSIGN wcodpaysl = histoent.paysl
                                             wlibpaysl:SCREEN-VAL IN FRAME fadresse = tabcomp.inti_tab.
                ELSE ASSIGN wcodpaysl:SCREEN-VAL = ""
                            wlibpaysl:SCREEN-VAL = "".
    
                /*$PRO3...*/
                DEF VAR OLD_civ_liv AS CHAR NO-UNDO.
                DEF VAR OLD_adr_liv AS CHAR EXTENT 3 NO-UNDO.
                DEF VAR OLD_k_post2l AS CHAR NO-UNDO.
                DEF VAR OLD_villel AS CHAR NO-UNDO.
                DEF VAR OLD_civ_fac AS CHAR NO-UNDO.
                DEF VAR OLD_adr_fac AS CHAR EXTENT 3 NO-UNDO.
                DEF VAR OLD_k_post2f AS CHAR NO-UNDO.
                DEF VAR OLD_villef AS CHAR NO-UNDO.
    
                ASSIGN OLD_civ_liv    = histoent.civ_liv
                       OLD_adr_liv[1] = histoent.adr_liv[1]
                       OLD_adr_liv[2] = histoent.adr_liv[2]
                       OLD_adr_liv[3] = histoent.adr_liv[3]
                       OLD_k_post2l   = histoent.k_post2l
                       OLD_villel     = histoent.villel
                       OLD_civ_fac    = histoent.civ_fac
                       OLD_adr_fac[1] = histoent.adr_fac[1]
                       OLD_adr_fac[2] = histoent.adr_fac[2]
                       OLD_adr_fac[3] = histoent.adr_fac[3]
                       OLD_k_post2f   = histoent.k_post2f
                       OLD_villef     = histoent.villef.
                /*...$PRO3*/
    
                UPDATE histoent.civ_liv histoent.adr_liv[1] histoent.adr_liv[2] histoent.adr_liv[3]
                       histoent.k_post2l histoent.villel wcodpaysl br-codpaysl 
                       histoent.civ_fac histoent.adr_fac[1] histoent.adr_fac[2] histoent.adr_fac[3]
                       histoent.k_post2f histoent.villef wcodpaysf br-codpaysf Babandon-adr Bvalider-adr
                       WITH FRAME Fadresse VIEW-AS DIALOG-BOX.
    
                /*$PRO3...*/
                IF OLD_civ_liv <> histoent.civ_liv OR
                    OLD_adr_liv[1] <> histoent.adr_liv[1] OR
                    OLD_adr_liv[2] <> histoent.adr_liv[2] OR
                    OLD_adr_liv[3] <> histoent.adr_liv[3] OR 
                    OLD_k_post2l <> histoent.k_post2l OR
                    OLD_villel <> histoent.villel OR
                    OLD_civ_fac <> histoent.civ_fac OR
                    OLD_adr_fac[1] <> histoent.adr_fac[1] OR
                    OLD_adr_fac[2] <> histoent.adr_fac[2] OR
                    OLD_adr_fac[3] <> histoent.adr_fac[3] OR 
                    OLD_k_post2f <> histoent.k_post2f OR
                    OLD_villef <> histoent.villef THEN ASSIGN histoent.zal[3] = "Y," + STRING(TODAY) + "," + g-user.
                /*...$PRO3*/
    
    
                ASSIGN histoent.paysf = INPUT wcodpaysf
                       histoent.paysl = INPUT wcodpaysl.
            END.
        END.
    END.
  end. 
  HBrowse-Ent:REFRESH().
  /*...$PRO2*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Baffac W1 
PROCEDURE CHOOSE-Baffac :
DO WITH FRAME F1:
    session:set-wait ("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        IF (Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ ) AND rtype:SCREEN-VAL="F" THEN run affacf_a PERSISTENT (rowid(Entfacfo)).
        ELSE
        DO:
            /*$1759*/
            IF Rtype:SCREEN-VALUE = "bl" AND AVAIL parsoc AND parsoc.ges_car AND AVAIL carpar AND histoent.nat_cde = carpar.nature THEN RUN hisvcar_ca (ROWID (histoent)).
            ELSE /* fin $1759*/
            DO:
                IF histoent.no_fact=0 THEN run afffac_c PERSISTENT (rowid(histoent), "BL").
                ELSE run afffac_c PERSISTENT (rowid(histoent), RType:screen-value).
            END.
        END.
    END.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Baffcde W1 
PROCEDURE CHOOSE-Baffcde :
session:set-wait ("general").
    if pTypMvt = "F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN DO:
        IF AVAIL histoent AND (histoent.typ_mvt = "F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/) THEN run affcde_a(histoent.NO_cde).
    END.
    ELSE IF AVAIL histoent THEN run affcde_c(histoent.NO_cde).
    session:set-wait ("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Baffdetprod W1 
PROCEDURE CHOOSE-Baffdetprod :
/* $A33836... */
    IF AVAIL histoent THEN DO:
        ASSIGN
            logbid = SetSessionData("Histpf_c","Cmd":U, STRING(histoent.no_cde))
            logbid = SetSessionData("Histpf_c","BL":U,  STRING(histoent.no_bl)).
        RUN histpf_c(ptypmvt, histoent.cod_cf, ?, "","","","","","",?).
    END.
/* ...$A33836 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BAFFICHER W1 
PROCEDURE CHOOSE-BAFFICHER :
DO WITH FRAME F1:
    RUN openquery ("",YES).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bajouter-colonnes-ent W1 
PROCEDURE CHOOSE-Bajouter-colonnes-ent :
DO WITH FRAME F1:
    IF VALID-HANDLE(Hbrowse-ent) THEN DO:
        SESSION:SET-WAIT("general").
        IF ptypmvt="F" AND rtype:SCREEN-VAL = "F" THEN DO:
            RUN reord_brws("HBHIS" + Ptypmvt + "3", "", "ENTFACFO", INPUT-O qliste-champ-ent2, INPUT-O qliste-label-ent2, INPUT-O qcolonne-lock-ent2, INPUT-O qcondition-ent2, OUTPUT logbid).
            IF logbid = ? THEN qliste-champ-ent2 = ?.
            IF NOT VALID-HANDLE (dynreq_facfo) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "" and cleval.section = "entfacfo" NO-LOCK) /* $1495 */
                                              OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "�" + g-user and cleval.section = "entfacfo" NO-LOCK)) THEN  /* $1495 */
            DO:
                RUN dynsel_b PERSISTENT SET dynreq_facfo ("","entfacfo",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse","histfb_c") .
                RUN ApparenceFrame IN dynreq_facfo (83,Traduction("facture",-2,""),"").
                RUN CreerFrame IN dynreq_facfo.
                RUN AfficherFrame IN dynreq_facfo.
            END.
        END.
        ELSE DO:
            RUN reord_brws("HBHIS" + Ptypmvt, Ptypmvt, "HISTOENT", INPUT-O qliste-champ-ent, INPUT-O qliste-label-ent, INPUT-O qcolonne-lock-ent, INPUT-O qcondition-Ent, OUTPUT logbid).
            IF logbid = ? THEN qliste-champ-ent = ?.
            IF NOT VALID-HANDLE (dynreq) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = Ptypmvt and cleval.section = "histoent" NO-LOCK) /* $1495 */
                                              OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = Ptypmvt + "�" + g-user and cleval.section = "histoent" NO-LOCK)) THEN  /* $1495 */
            DO:
                RUN dynsel_b PERSISTENT SET dynreq (ptypmvt,"histoent",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse","histfb_c" + Ptypmvt) .
                RUN ApparenceFrame IN dynreq (0,Traduction("en-t�te",-2,""),"").
                RUN CreerFrame IN dynreq.
                RUN AfficherFrame IN dynreq.
            END.
        END.
        IF logbid <> NO AND Vcod_cli:SCREEN-VAL <> "" THEN
            RUN openquery(svcle,YES).
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BAJOUTER-COLONNES-LIG W1 
PROCEDURE CHOOSE-BAJOUTER-COLONNES-LIG :
DO WITH FRAME F1:
    IF VALID-HANDLE(Hbrowse-lig) THEN DO:
        session:set-wait ("general").
        RUN reord_brws ("HBHIL" + ptypmvt,"P","HISTOLIG",INPUT-O qliste-champ-lig,INPUT-O qliste-label-lig,INPUT-O qcolonne-lock-lig, INPUT-O qcondition-lig, OUTPUT logbid).
        IF logbid=? THEN qliste-champ-lig=?.
        IF NOT VALID-HANDLE (dynreq_histolig) AND (CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "P" and cleval.section = "histolig" NO-LOCK)
                                          OR CAN-FIND (FIRST cleval where cleval.chapitre = "BD" and cleval.domaine = "CRITRECH1" AND cleval.cod_user = "P" + "�" + g-user and cleval.section = "histolig" NO-LOCK)
                                          OR CAN-FIND (FIRST reqpersa WHERE reqpersa.nom_pgm = "HIL":U + ptypmvt AND reqpersa.nom_sspgm = "" AND reqpersa.cod_user = g-user NO-LOCK) /*$A91177*/
                                          OR CAN-FIND (FIRST reqpersa WHERE reqpersa.nom_pgm = "HIL":U + ptypmvt AND reqpersa.nom_sspgm = "" AND reqpersa.cod_user = "" NO-LOCK)) THEN  /* $1495 */
        DO:
            RUN apicomp_b PERSISTENT SET dynreq_histolig ("p","histolig",THIS-PROCEDURE,FRAME f1:HANDLE,"OuvrirBrowse":U,"histfb_c" + Ptypmvt),"HIL":U + ptypmvt,"") . /*$A91177*/
            RUN ApparenceFrame IN dynreq_histolig (83,Traduction("ligne",-2,""),"").
            RUN CreerFrame IN dynreq_histolig.
            RUN AfficherFrame IN dynreq_histolig.
        END.
        IF logbid<>NO THEN /* Si changement browse entete, on ne relance pas la creation de l'entete mais uniquement celle des lignes d'o� le valuechanged-ent */
            RUN valuechanged-ent.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BAJOUTER-COLONNES-LIG2 W1 
PROCEDURE CHOOSE-BAJOUTER-COLONNES-LIG2 :
DO WITH FRAME F1:
    IF VALID-HANDLE(Hbrowse-lig) THEN DO:
        session:set-wait ("general").
        RUN reord_brws ("HBHIL" + ptypmvt + "3","","LIGfacfo":U,INPUT-O qliste-champ-lig2,INPUT-O qliste-label-lig2,INPUT-O qcolonne-lock-lig2, INPUT-O qcondition-lig2, OUTPUT logbid).
        IF logbid=? THEN qliste-champ-lig2=?.
        IF logbid<>NO THEN /* Si changement browse entete, on ne relance pas la creation de l'entete mais uniquement celle des lignes d'o� le valuechanged-ent */
            RUN valuechanged-ent.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Banarec W1 
PROCEDURE CHOOSE-Banarec :
IF AVAIL histoent THEN
        RUN anarec_a (histoent.cod_cf, histoent.NO_cde, histoent.NO_bl_a).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bannuler-edtbl W1 
PROCEDURE choose-bannuler-edtbl :
valid = NO.
APPLY "GO" TO FRAME Fedtbl. /*$A59971*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bannuler-p W1 
PROCEDURE CHOOSE-Bannuler-p :
valid=NO.
    APPLY "GO" TO FRAME f1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bannuler-r W1 
PROCEDURE CHOOSE-Bannuler-r :
valid = NO.
    APPLY "GO" TO FRAME Fedtfac.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-Bcarvar W1 
PROCEDURE Choose-Bcarvar :
DEFINE VARIABLE pqui AS CHARACTER NO-UNDO.
DEFINE VARIABLE P%dat_ent AS DATE NO-UNDO.
DEFINE VARIABLE pheu AS INT NO-UNDO.
DEFINE VARIABLE ptex AS CHARACTER NO-UNDO.
DEFINE VARIABLE pno AS INT NO-UNDO.
DEFINE VARIABLE kvi AS CHARACTER NO-UNDO.
DEFINE VARIABLE plib AS CHARACTER NO-UNDO.
DEFINE VARIABLE itsok AS LOGICAL NO-UNDO.
DEF VAR kvCaract AS CHAR NO-UNDO.
DEF VAR pbloq AS LOG NO-UNDO.
DEF VAR pech AS CHAR NO-UNDO.

DO WITH FRAME f1:
    IF ModeleCaract <> "" THEN  /*$A41075*/
    DO:
        FIND FIRST fournis WHERE fournis.cod_fou = histoent.cod_cf NO-LOCK NO-ERROR.
        FIND FIRST histocar WHERE histocar.typ_zon = "REC":U AND histocar.cle2 = STRING (histoent.cod_cf) AND histocar.cle1 = STRING (histoent.NO_cde)
            AND histocar.cle3 = STRING (histoent.no_bl_a) NO-LOCK NO-ERROR.
        IF AVAIL histocar THEN
            kvCaract = histocar.k_varcq.
        IF kvCaract <> "" THEN
        DO:
            RUN carvar_gp (Traduction("Saisie des caract�ristiques globales de r�ception",-2,""),"M",2,ModeleCaract,INPUT-OUTPUT kvCaract,OUTPUT kvi,INPUT-OUTPUT plib,
                            OUTPUT itsok,Traduction("R�ception",-2,"") + "|" + "" + histoent.no_bl_a ,
                           INPUT-OUTPUT pqui, INPUT-OUTPUT P%dat_ent, INPUT-OUTPUT pheu,no,INPUT-OUTPUT pech,INPUT-OUTPUT ptex,INPUT-OUTPUT pno,0,?,?,ROWID (fournis),?,
                           ?,NO,"RECEPTION":U,"",OUTPUT pbloq). /* $1796 */
            IF itsok THEN
            DO:
                DO TRANSACTION :
                    FIND CURRENT histocar SHARE-LOCK NO-ERROR. /* �XREF_NOWHERE� */
                    histocar.k_varcq = kvCaract.
                END.
                FIND CURRENT histocar NO-LOCK NO-ERROR.  /* �XREF_NOWHERE� */
            END.
        END.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bcolisage W1 
PROCEDURE CHOOSE-Bcolisage :
DO WITH FRAME F1:
    session:set-wait ("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        IF Ptypmvt="C" AND rtype:SCREEN-VAL="BL" THEN run affcol_c PERSISTENT (rowid(histoent)).
    END.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bcopier W1 
PROCEDURE CHOOSE-Bcopier :
DO WITH FRAME F1:
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        IF histoent.ndos<>n_d THEN
            MESSAGE Traduction("Traitement impossible !",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
            VIEW-AS ALERT-BOX INFO.
        /*$BOIS empeche la copie de pi�ce achat bois...*/
        ELSE IF pTypMvt<>"C" AND parsoc.bois_ar AND CAN-FIND(FIRST boiscol WHERE boiscol.typ_mvt = histoent.typ_mvt AND boiscol.cod_cf = histoent.cod_cf AND boiscol.no_cde = histoent.no_cde no-lock) THEN
            MESSAGE Traduction("Traitement impossible !",-2,"") + " " + Traduction("La pi�ce s�lectionn�e contient du bois !",-2,"") VIEW-AS ALERT-BOX INFO.
        /*...$BOIS*/
        ELSE
        DO:
            IF RType:SCREEN-VALUE = "F" THEN
            DO:
                IF pTypMvt = "C" THEN
                DO:
                    /* $A47809 ... */
                    IF histoent.no_fact = 0 AND (histoent.usr_crt = "ATELIER":U OR histoent.ori_ate) THEN /* pour les ent�tes de type cession (atelier) */
                    DO:
                        SetSessionData("histfb_c", "cessionAtelier":U, "YES").
                        SetSessionData("histfb_c", "dfauto":U, "no"). /*$A62908*/
                        RUN CopyLg_c(pRowID, histoent.cod_cf, "F", histoent.no_bl, histoent.no_cde).
                        SetSessionData("histfb_c", "cessionAtelier":U, "").
                    END.
                    ELSE /* ... $A47809 */ DO:
                        SetSessionData("histfb_c", "dfauto":U, "no"). /*$A62908*/
                        RUN CopyLg_c(pRowID, histoent.cod_cf, "F", histoent.no_fact, 0).
                    END.
                END.
                ELSE RUN CopyLg_a(pRowID, histoent.cod_cf, "F", histoent.no_fact, 0).
            END.
            ELSE IF RType:SCREEN-VALUE = "C" THEN
            DO:
                IF pTypMvt = "C" THEN do:
                    SetSessionData("histfb_c", "dfauto":U, "no"). /*$A62908*/
                    RUN CopyLg_c(pRowID, histoent.cod_cf, "C", histoent.no_cde, 0).
                END.
                ELSE RUN CopyLg_a(pRowID, histoent.cod_cf, "C", histoent.no_cde, 0).
            END.
            ELSE
            DO:
                IF pTypMvt = "C" THEN do:
                    SetSessionData("histfb_c", "dfauto":U, "no"). /*$A62908*/
                    RUN CopyLg_c(pRowID, histoent.cod_cf, "BL":U, histoent.no_bl, histoent.no_cde).
                END.
                ELSE RUN CopyLg_a(pRowID, histoent.cod_cf, "BL":U, histoent.no_bl, histoent.no_cde).
            END.
        END.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-bcr W1 
PROCEDURE Choose-bcr :
/*$A62727*/
DO WITH FRAME Fr1:
    /*$A72943...*/
    find FIRST histoent3 where histoent3.typ_mvt = histolig.typ_mvt
                        and histoent3.cod_cf = histolig.cod_cf
                        and histoent3.no_cde = histolig.no_cde
                        and histoent3.no_bl = histolig.no_bl no-lock no-error.
    IF AVAIL(histolig) THEN RUN cr_rec (histolig.cod_cf,histolig.no_cde,(IF AVAIL histoent3 THEN STRING(histoent3.NO_fact) ELSE ""),histolig.NO_bl_a,histolig.dat_mvt,histolig.no_ligne,"H",Rowid(Histolig),OUTPUT logbid).
    /*...$A72943*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bdetail W1 
PROCEDURE CHOOSE-Bdetail :
IF AVAIL histolig THEN
        run dethil_b (rowid(histolig),YES).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bdoc W1 
PROCEDURE CHOOSE-Bdoc :
/*$A60546...*/
    DEF VAR ptypsai AS CHAR  NO-UNDO.
    DEF VAR pchamp AS CHAR  NO-UNDO.
    DEF VAR plibre AS CHAR  NO-UNDO.
    /*...$A60546*/

FIND CURRENT histoent NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
    IF AVAIL histoent THEN DO:
        /*$A60546...*/
        IF typinfo = "A" THEN ptypsai = "CF":U.
        ELSE DO :
            IF Rtype:screen-value IN FRAME F1 = "F" THEN ptypsai = "U".
            ELSE IF Rtype:screen-value IN FRAME F1 = "BL" THEN ptypsai = "B".
            ELSE ptypsai = "R".
        END.
        pchamp = STRING(histoent.NO_cde).


        IF ptypsai = "CF":U THEN plibre = "entetfou.cod_fou" + "," + string(histoent.cod_cf) + CHR(1) + "entetfou.NO_cde," + string(histoent.no_cde) + CHR(1) + "entetfou.no_of" + ","  + string(histoent.no_bl).
        ELSE plibre = "client.cod_cli" + "," + string(histoent.cod_cf) + CHR(1) + "entetcli.NO_cde," + string(histoent.no_cde) + CHR(1) + "entetcli.no_bl" + ","  + string(histoent.no_bl).

        SetSessionData ("MNTINF_B","TYPE":U,ptypsai).
        SetSessionData ("MNTINF_B","CHAMP":U,pchamp).
        SetSessionData ("MNTINF_B","PLIBRE":U,plibre).
        SetSessionData ("VISU_GED","HISTORIQUE":U,"yes").
        IF histoent.no_info <> 0 AND histoent.no_info <> ? THEN DO:
            FIND FIRST infoent WHERE infoent.typ_fich = /*"E"*/ typinfo AND infoent.no_info = histoent.no_info NO-LOCK NO-ERROR. /*A20014*/
            IF AVAIL infoent THEN do:
                IF typinfo = "A" THEN RUN mntinf_b ("A", histoent.no_info, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "F" + STRING(histoent.cod_cf)). /*A20014*/
                ELSE RUN mntinf_b ("E", histoent.no_info, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "C" + STRING(histoent.cod_cf)).
            END.
            ELSE DO :
                IF typinfo = "A" THEN RUN mntinf_b ("A", 0, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "F" + STRING(histoent.cod_cf)). /*A20014*/
                ELSE RUN mntinf_b ("E", 0, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "C" + STRING(histoent.cod_cf)).
            END.
        END.
        ELSE DO :
            IF typinfo = "A" THEN RUN mntinf_b ("A", 0, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "F" + STRING(histoent.cod_cf)). /*A20014*/
            ELSE RUN mntinf_b ("E", 0, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "C" + STRING(histoent.cod_cf)).
        END.

        SetSessionData ("MNTINF_B","TYPE":U,"").
        SetSessionData ("MNTINF_B","CHAMP":U,"").
        SetSessionData ("MNTINF_B","PLIBRE":U,"").
        SetSessionData ("VISU_GED","HISTORIQUE":U,"").
        /*...$A60546*/
    END.
    ELSE DO :
        MESSAGE Traduction("Pas de documents...",-2,"") VIEW-AS ALERT-BOX INFO BUTTONS OK.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BdossierPR W1 
PROCEDURE CHOOSE-BdossierPR :
IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        IF histoent.DOS_pxr=0 AND histoent.cal_marg="X" THEN DO:
            logbid=YES.
            IF histoent.fret<>0 THEN MESSAGE Traduction("Voulez-vous cr�er le dossier P.R pour les r�ceptions concern�es par le dossier de fret ?",-2,"")
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
            ELSE MESSAGE Traduction("Voulez-vous cr�er le dossier P.R pour les r�ceptions concern�es ?",-2,"")
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
            IF logbid THEN DO:
                run dospxr_a (0,histoent.fret,ROWID(histoent),OUTPUT logbid).
                IF logbid THEN HBrowse-Ent:REFRESH().
            END.
        END.
        ELSE IF histoent.cal_marg="X" THEN DO:
            run dospxr_a (histoent.dos_pxr,histoent.fret,ROWID(histoent),OUTPUT logbid).
            IF logbid THEN HBrowse-Ent:REFRESH().
        END.
        ELSE MESSAGE Traduction("Ce n'est pas un dossier de groupage. Passez par le bon de r�ception pour �diter, ou modifier r�ception pour r�ajuster",-2,"")
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BEclate W1 
PROCEDURE CHOOSE-BEclate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE pasmodif AS LOGICAL    NO-UNDO.

  IF CAN-FIND(droigco WHERE droigco.util = g-user-droit AND droigco.fonc = "ECLFACT") THEN DO:
        MESSAGE "Vous n'avez pas les droits pour �clater les factures !" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        /*YK...*/
        FIND CURRENT histoent NO-LOCK NO-ERROR.
        IF AVAIL histoent AND histoent.no_fact <> 0 AND histoent.typ_mvt = 'C' THEN DO:
            RUN s-eclfac_histo(histoent.cod_cf,histoent.typ_mvt,histoent.no_cde,histoent.no_bl,histoent.no_fact, OUTPUT pasmodif).
            IF pasmodif 
            THEN DO:
                RUN s-eclfact_his(histoent.cod_cf,histoent.typ_mvt,histoent.no_cde,histoent.no_bl,histoent.no_fact, OUTPUT vchar).
                IF vchar <> "" THEN DO:
                    MESSAGE "Liste des BL cr��s : " + REPLACE(vchar,",",", ") VIEW-AS ALERT-BOX INFO BUTTONS OK.
                    RUN openquery (YES).
                END.
            END.
        END.
        ELSE MESSAGE "Seuls les BL factur�s peuvent �tre �clat�s" VIEW-AS ALERT-BOX ERROR BUTTONS OK.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bediter W1 
PROCEDURE CHOOSE-Bediter :
DO WITH FRAME F1:
    session:set-wait-state("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        SetSessionData("MAQ","TIERS",STRING(histoent.cod_cf)).
        SetSessionData("MAQ","RWENT",STRING(ROWID(histoent))).

        IF histoent.ndos<>n_d THEN
            MESSAGE Traduction("Edition impossible",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
        ELSE if Rtype:screen-value = "F" then do on endkey undo,leave:
            tdup=yes.
            if histoent.nofaca[1]>0 then do:
                /* $1489 ... */
                SetSessionData("saifs_c","SAI-REV-ACT":U, "NO"). /*$A62570*/
                RUN edsit_c (ROWID(histoent),?,0,"", NOT histoent.chif_bl, OUTPUT pdup, OUTPUT pexclisoc, OUTPUT pniveau, OUTPUT pdescom, OUTPUT ptxtent, OUTPUT pdatfac, OUTPUT psimul, /* $1539 ... */ OUTPUT plibrev, /* ... $1539 */ OUTPUT previs, /* $1539 ... */ OUTPUT plibact, /* ... $1539 */ OUTPUT pactual, /* $1539 ... */ OUTPUT pfacdef, /* ... $1539 */  OUTPUT plibre1, OUTPUT plibre2, OUTPUT plibre3, OUTPUT logbid).
                SetSessionData("saifs_c","SAI-REV-ACT":U, ""). /*$A62570*/
                IF NOT logbid THEN RETURN.
                /* ... $1489 */
                run edfs1_c ("",histoent.dat_fac,pniveau,pdescom,ptxtent,histoent.cod_cf,histoent.no_cde,histoent.no_bl,/* $1539 ... */ histoent.ind_con , /* ... $1539 */ pdup,pexclisoc,psimul, /* $1539 ... */ pfacdef, /* ... $1539 */ output logbid).
            end.
            else IF histoent.no_fact>0 THEN do:
                tex=(vex="yes").
                hide fniveau in frame fedtfac rniveau rediter rtexte.
                update unless-hidden tdup tex rs$global with frame fedtfac view-as dialog-box.
                IF valid THEN DO:
                    vex=string(tex).
                    IF parsoc.bois_ar THEN setsessiondata("Facture", "Globale",rs$global:SCREEN-VALUE). /*$BOIS*/
                    run edhf1_c (histoent.cod_cf, histoent.no_cde, histoent.no_bl, tdup, tex, output logbid).
                    RUN putini.
                END.
            end.
        end.
        else if pTypMvt="F" then do on endkey undo,leave with frame FHISTPF:
           tbval = YES.
           DISP Ft$selection-2 WITH FRAME Frchoix.
           IF nompgm = "EDTHCFO":U THEN rsref:HIDDEN = YES. /*$2122*/
                                 ELSE rsref:HIDDEN = NO. /*$2122*/
           update UNLESS-HIDDEN /*$2122*/ rsref Tbval with frame frchoix VIEW-AS DIALOG-BOX.
        end.
        else do:
            /*$1583...*/
            DEF VAR ptypedt AS CHAR NO-UNDO.
            DEF VAR pmodele AS CHAR NO-UNDO.

            IF Rtype:SCREEN-VALUE = "bl" AND AVAIL parsoc AND parsoc.ges_car AND AVAIL carpar AND histoent.nat_cde = carpar.nature THEN
                /*$1759*/
            DO:
                ASSIGN PARM-EDT = STRING(histoent.cod_cf,     "999999") +  /*  1 �  6 */
                              STRING("F",          "X") +  /*  7 �  7 */
                              STRING(histoent.NO_cde,  "999999999") +  /*  8 � 16 */
                              STRING(histoent.NO_bl,   "999999999") +  /* 17 � 25 */
                              STRING(" ",   "X(18)") + STRING(IF AVAIL carent THEN carent.zta[1] ELSE " ",   "X(10)").
                RUN RunJob ("edtblca", /*"CUSTOM:" + STRING(THIS-PROCEDURE)*/ parm-edt, OUTPUT Modedit).
            END.
            ELSE /* fin $1759*/
            DO:
                /*$A59971...*/
                UPDATE UNLESS-HIDDEN texbl WITH FRAME fedtbl VIEW-AS DIALOG-BOX.
                IF valid THEN DO:
                    /*...$A59971*/
                    IF CAN-FIND(FIRST parmoded WHERE parmoded.typ_par = "EDTHB" NO-LOCK) THEN DO :
                        RUN edtwrd_c ("HB",OUTPUT ptypedt,OUTPUT pmodele).
                        IF ptypedt="" THEN RETURN.
                    END.
                    /*...$1583*/

                    ASSIGN
                        LOGBID = SESSION:SET-WAIT-STATE("GENERAL")
                        PARM-EDT =
                            STRING(histoent.cod_cf,            "999999") +  /*  1 �  6 */
                            STRING(histoent.no_cde,         "999999999") +  /*  8 � 16 */
                            STRING(histoent.no_bl,          "999999999") +  /* 17 � 25 */
                            STRING(today,                    "99/99/99") +   /* 26 � 35 */
                            STRING(texbl:CHECKED IN FRAME fedtbl, "O/N"). /*$A59971*/
                    SetSessionData("edthb","chif_bl":U, (IF valbtn_blchiffr THEN "yes" ELSE "no")).    /*$A62526*/
                    RUN RunJob ("EDTHB", PARM-Edt, OUTPUT Modedit).
                    SetSessionData("edthb","chif_bl":U, "").    /*$A62526*/
                    RETOUR = (MODEDIT <> 0).
                END. /*$A59971*/
            END.
        end.
    END.
    session:set-wait-state("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BEDITER-BONCOMMISSION W1 
PROCEDURE CHOOSE-BEDITER-BONCOMMISSION :
DEFINE VARIABLE impressionBC AS CHAR NO-UNDO.
DO WITH FRAME F1: /*$BOIS*/
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
       HBrowse-Ent:FETCH-SELECTED-ROW(1).
       IF AVAIL histoent THEN RUN runjob ("edtbc_bo",INPUT STRING(histoent.cod_cf,     "999999") + STRING(histoent.typ_mvt,          "X") + STRING(histoent.no_cde,  "999999999") +
                                              STRING(histoent.no_bl,  "9999999999") +  STRING("H",        "X") ,OUTPUT impressionBC). /* $BOIS TEN */
    END.
/*APPLY "GO" TO FRAME Frchoix.*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BEDITER-SPE W1 
PROCEDURE CHOOSE-BEDITER-SPE :
/*$BOIS*/

DO WITH FRAME F1:
        FIND CURRENT histoent NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
        IF AVAIL histoent THEN run edtspe_bo (IF pTypmvt = "F" THEN "ACHAT" ELSE "VENTE",no,histoent.cod_cf,histoent.typ_mvt,histoent.no_cde,IF ptypmvt = "F" THEN histoent.no_bl_a ELSE string(histoent.no_bl)).
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bediterc W1 
PROCEDURE CHOOSE-Bediterc :
/* R��dition du bon de retour 2122 ...*/
    IF nompgm = "EDTHCFO":U THEN DO:
        ASSIGN PARM-EDT = STRING(histoent.cod_cf, "999999") +
                          STRING(histoent.typ_mvt, "X")     +
                          STRING(histoent.NO_cde, "999999999") +
                          STRING(histoent.no_bl, "999999999") +
                          STRING(TODAY, "99/99/99") +
                          string(tbval:CHECKED IN FRAME Frchoix,"O/N").

        RUN RunJob (NOMPGM, PARM-Edt, OUTPUT Modedit).
        RETOUR = (MODEDIT <> 0).
    END.
    /*... $2122*/
    ELSE run edrf1_a (histoent.cod_cf,histoent.no_cde,histoent.no_bl,rsref:screen-val in frame frchoix,tbval:CHECKED IN FRAME Frchoix,output logbid).
    APPLY "GO" TO FRAME Frchoix.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Beditfaci W1 
PROCEDURE CHOOSE-Beditfaci :
DO WITH FRAME F1:
    session:set-wait-state("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        if Rtype:screen-value = "F" then do on endkey undo,leave:
            IF histoent.ndos<>n_d THEN
                MESSAGE Traduction("Edition impossible",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
            ELSE run edboni_c (histoent.cod_cf,"HISTFAC",histoent.no_cde,histoent.NO_bl).
        END.
    END.
    session:set-wait-state("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bemp W1 
PROCEDURE choose-bemp :
DO WITH FRAME f1 :
    RUN empdet_s (RowId(histolig),histolig.lien_emp).
END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bentete W1 
PROCEDURE CHOOSE-Bentete :
IF AVAIL histoent THEN RUN visuhent_c(histoent.typ_mvt, ROWID(histoent)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Betiq W1 
PROCEDURE CHOOSE-Betiq :
IF VALID-HANDLE(HBrowse-Ent) THEN DO:
        IF HBrowse-Ent:HIDDEN = NO AND HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
            IF ptypmvt = "F" THEN RUN lanetq_et(STRING(ROWID(histoent)),"","","E","D","HISTFB_C",YES,YES,1,1,"","",OUTPUT LOGBID). /*$A40206*/
            ELSE IF ptypmvt = "C" THEN RUN lanetq_et(STRING(ROWID(histoent)),"","","E","D","HISTFB_C",YES,YES,2,1,"","",OUTPUT LOGBID). /*$A40206*/
        END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-BFAX W1 
PROCEDURE CHOOSE-BFAX :
DEF VAR ptypedt AS CHAR NO-UNDO.
    DEF VAR pmodele AS CHAR NO-UNDO.
DO WITH FRAME F1:
        IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
            HBrowse-Ent:FETCH-SELECTED-ROW(1).
            IF histoent.ndos<>n_d THEN
                MESSAGE Traduction("Edition impossible",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
            /*Facture*/
            if RType:screen-value="F" THEN DO:
                                /* $A37021... */
                /* Facture de situation */
                if histoent.nofaca[1]>0 then do:
                    /* $1489 ... */
                    SetSessionData("saifs_c","SAI-REV-ACT":U, "NO"). /*$A62570*/
                    RUN edsit_c (ROWID(histoent),?,0,"", NOT histoent.chif_bl, OUTPUT pdup, OUTPUT pexclisoc, OUTPUT pniveau, OUTPUT pdescom, OUTPUT ptxtent, OUTPUT pdatfac, OUTPUT psimul, /* $1539 ... */ OUTPUT plibrev, /* ... $1539 */ OUTPUT previs, /* $1539 ... */ OUTPUT plibact, /* ... $1539 */ OUTPUT pactual, /* $1539 ... */ OUTPUT pfacdef, /* ... $1539 */  OUTPUT plibre1, OUTPUT plibre2, OUTPUT plibre3, OUTPUT logbid).
                    SetSessionData("saifs_c","SAI-REV-ACT":U, ""). /*$A62570*/
                    IF NOT logbid THEN RETURN.
                    /* ... $1489 */
                    /*IF Faxer THEN DO:
 *                         Retour = NO.*/
                        RUN Prefax("H",ROWID(Histoent),OUTPUT Retour).
                    /*END.
 *                     ELSE retour = YES.*/
                    IF Retour THEN DO:
                        IF Faxer THEN SetsessionData("GcoFax","Faxer","OUI").
                    ELSE SetsessionData("GcoMail","Mailer","OUI").
                        run edfs1_c ("",histoent.dat_fac,pniveau,pdescom,ptxtent,histoent.cod_cf,histoent.no_cde,histoent.no_bl,/* $1539 ... */ histoent.ind_con , /* ... $1539 */ pdup,pexclisoc,psimul, /* $1539 ... */ pfacdef, /* ... $1539 */ output logbid).
                        IF Faxer THEN SetsessionData("GcoFax","Faxer","NON").
                                 ELSE SetsessionData("GcoMail","Mailer","NON").
                    END.
                end.
                ELSE IF histoent.no_fact>0 THEN do:
                                /* ...$A37021 */
                    tex = (vex="yes").
                    hide fniveau in frame fedtfac rniveau rediter rtexte.
                    update unless-hidden tdup tex rs$global with frame fedtfac view-as dialog-box.
                    IF valid THEN DO:
                        vex = string(tex).
                        IF Faxer THEN DO:
                            Retour = NO.
                            RUN Prefax("H",ROWID(Histoent),OUTPUT Retour).
                        END.
                        ELSE retour = YES.

                        IF Retour THEN DO:
                            IF parsoc.bois_ar THEN setsessiondata("Facture", "Globale",rs$global:SCREEN-VALUE). /*$BOIS*/
                            IF Faxer THEN SetsessionData("GcoFax","Faxer","OUI").
                            ELSE SetsessionData("GcoMail","Mailer","OUI").
                            run edhf1_c (histoent.cod_cf, histoent.no_cde, histoent.no_bl, tdup, tex, output logbid).
                            IF Faxer THEN SetsessionData("GcoFax","Faxer","NON").
                            ELSE SetsessionData("GcoMail","Mailer","NON").
                            RUN putini.
                        END.
                    END.
                END.
            END.
            /*BL*/
            ELSE DO:
                /*$1759*/
                IF AVAIL parsoc AND parsoc.ges_car AND AVAIL carpar AND histoent.nat_cde = carpar.nature THEN DO:
                    /*Pas d'envoi de cette �dition par fax/mail, les boutons sont cach�
                    ASSIGN PARM-EDT = STRING(histoent.cod_cf,     "999999") +  /*  1 �  6 */
                                  STRING("F",          "X") +  /*  7 �  7 */
                                  STRING(histoent.NO_cde,  "999999999") +  /*  8 � 16 */
                                  STRING(histoent.NO_bl,   "999999999") +  /* 17 � 25 */
                                  STRING(" ",   "X(18)") + STRING(IF AVAIL carent THEN carent.zta[1] ELSE " ",   "X(10)").
                    RUN RunJob ("edtblca", /*"CUSTOM:" + STRING(THIS-PROCEDURE)*/ parm-edt, OUTPUT Modedit).*/
                END.
                ELSE DO: /* fin $1759*/
                    IF CAN-FIND(FIRST parmoded WHERE parmoded.typ_par = "EDTHB" NO-LOCK) THEN DO :
                        RUN edtwrd_c ("HB",OUTPUT ptypedt,OUTPUT pmodele).
                        IF ptypedt="" THEN RETURN.
                    END.
                    /*...$1583*/


                    IF Faxer THEN DO:
                        Retour = NO.
                        RUN Prefax("H",ROWID(Histoent),OUTPUT Retour).
                    END.
                    ELSE retour = YES.

                    IF retour THEN DO:
                        ASSIGN LOGBID = SESSION:SET-WAIT-STATE("GENERAL")
                               PARM-EDT = STRING(histoent.cod_cf,    "999999") +  /*  1 �  6 */
                                          STRING(histoent.no_cde, "999999999") +  /*  8 � 16 */
                                          STRING(histoent.no_bl,  "999999999") +  /* 17 � 25 */
                                          STRING(today,            "99/99/99") +   /* 26 � 35 */
                                          STRING(YES                  , "O/N"). /*$A59971*/
                        IF NOT faxer THEN RUN RunJob ("EDTHB" + "^MailOnly", PARM-Edt, OUTPUT Modedit).
                        ELSE DO:
                            SetsessionData("GcoFax","Faxer":U,"OUI":U).
                            RUN RunJob ("EDTHB", PARM-Edt, OUTPUT Modedit).
                            SetsessionData("GcoFax","Faxer":U,"NON":U).
                        END.

                        RETOUR = (MODEDIT <> 0).
                    END.
                END.
            END.
        END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bgrille_rem W1 
PROCEDURE choose-bgrille_rem :
/*A56990*/
DO WITH FRAME f1 :
    RUN affgr_t (histolig.grille_rem,histoent.dat_px).
END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bimm W1 
PROCEDURE choose-bimm :
DO WITH FRAME f1 :
    RUN immdet_s (RowId(histolig),"",histolig.lien_imm,histolig.es_mvt).
END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Binfo W1 
PROCEDURE CHOOSE-Binfo :
IF AVAIL histoent THEN run dethie_b (rowid(histoent)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-blegende W1 
PROCEDURE choose-blegende :
FRAME FLegende:HIDDEN = NO.
UPDATE Legende-RECT-368 WITH FRAME flegende VIEW-AS DIALOG-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-blegende-abandonner W1 
PROCEDURE choose-blegende-abandonner :
FRAME FLegende:HIDDEN = YES.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Blitige W1 
PROCEDURE CHOOSE-Blitige :
session:set-wait ("").
    IF AVAIL entfacfo THEN DO:
        RUN Mntlit_a(entfacfo.cod_fou, entfacfo.no_fact).
        run openquery("",yes). /*$A62727*/
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-blot W1 
PROCEDURE choose-blot :
DO WITH FRAME f1 :
    IF (histolig.es_mvt = "E" AND histolig.qte_unit > 0) THEN
        RUN lotdee_s (RowId(histolig),"",histolig.lien_lot).
    ELSE
        RUN lotdes_s (RowId(histolig),"",histolig.lien_lot).
END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bmarge W1 
PROCEDURE CHOOSE-Bmarge :
run marge3_c PERSISTENT (rowid(histoent)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bpied W1 
PROCEDURE CHOOSE-Bpied :
IF AVAIL histoent THEN RUN visuhpied_c(histoent.cod_cf,histoent.NO_cde,histoent.NO_bl,histoent.typ_mvt).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bplage W1 
PROCEDURE CHOOSE-Bplage :
do on endkey undo,leave with frame fplafac:
    if RType:SCREEN-VALUE IN FRAME F1 = "F" then DO:  /*$1479*/
        tcli=no.
        if avail histoent AND histoent.ndos=n_d then do:
            find client where client.cod_cli=histoent.cod_cf no-lock no-error.
            if avail client then assign tcli=yes tcli:label=SUBSTITUTE(Traduction("Uniquement pour &1",-2,""),client.nom_cli).
        end.
        if tcli=no then hide tcli in frame fplafac.
        assign typ-plage="D" fn-d=0 fn-f=0 fdat-d=? fdat-f=? tdup=yes tex=(vex="yes").

        update unless-hidden tcli tdup tex typ-plage fn-d fn-f fdat-d fdat-f with frame fplafac view-as dialog-box.
        IF valid THEN DO:
            vex=string(tex).
            run traitement-plage.
            if return-value="" AND /*$A65291 CAN-FIND(FIRST wfac NO-LOCK) => ...*/ (CAN-FIND(FIRST wfac NO-LOCK) OR CAN-FIND(FIRST WFACSIT NO-LOCK)) /*...$A65291*/ THEN DO:
                IF radio-editpdf:HIDDEN IN FRAME fplafac = NO AND LOGICAL(radio-editpdf:SCREEN-VAL) = YES THEN /* $1359 */
                        MESSAGE Traduction("La fonction 'Aper�u � l'�cran' ne fonctionne pas pour les �ditions �trang�res.",-2,"") VIEW-AS ALERT-BOX WARNING BUTTONS OK.
                /*$A65291...*/
                /*Factures "normales"*/
                IF CAN-FIND(FIRST wfac NO-LOCK) THEN run edhf1_c (0, 0, 0, tdup, tex, output logbid). 
                /*Factures Situation*/
                FOR EACH WFACSIT NO-LOCK:
                    FIND FIRST histoent2 WHERE RECID(histoent2) = WFACSIT.WFRID NO-LOCK NO-ERROR. 
                    SetSessionData("saifs_c","SAI-REV-ACT":U, "NO"). /*$A62570*/
                    RUN edsit_c (ROWID(histoent2),?,0,"", NOT histoent2.chif_bl, OUTPUT pdup, OUTPUT pexclisoc, OUTPUT pniveau, OUTPUT pdescom, OUTPUT ptxtent, OUTPUT pdatfac, OUTPUT psimul, /* $1539 ... */ OUTPUT plibrev, /* ... $1539 */ OUTPUT previs, /* $1539 ... */ OUTPUT plibact, /* ... $1539 */ OUTPUT pactual, /* $1539 ... */ OUTPUT pfacdef, /* ... $1539 */  OUTPUT plibre1, OUTPUT plibre2, OUTPUT plibre3, OUTPUT logbid).
                    SetSessionData("saifs_c","SAI-REV-ACT":U, "").
                    IF NOT logbid THEN RETURN.
                    run edfs1_c ("",histoent2.dat_fac,pniveau,pdescom,ptxtent,histoent2.cod_cf,histoent2.no_cde,histoent2.no_bl,/* $1539 ... */ histoent2.ind_con , /* ... $1539 */ pdup,pexclisoc,psimul, /* $1539 ... */ pfacdef, /* ... $1539 */ output logbid).
                END.
                /*...$A65291*/

                /*$A65291 run edhf1_c (0, 0, 0, tdup, tex, output logbid).*/
            END.
            ELSE DO: /* $1359 */
               IF radio-editpdf:HIDDEN IN FRAME fplafac = NO THEN
               MESSAGE Traduction("Aucune facture � r��diter !",-2,"") + " " + (IF LOGICAL(radio-editpdf:SCREEN-VAL) = NO THEN Traduction("dans les �ditions classiques",-2,"") ELSE Traduction("dans les �ditions �trang�res",-2,"")) VIEW-AS ALERT-BOX INFORMATION.
            ELSE
                message Traduction("Aucune facture � r��diter !",-2,"") view-as alert-box info.
            END.
        END.
        session:set-wait-state("").
    END.
    /*$1479...*/
    if RType:SCREEN-VALUE IN FRAME F1 ="BL" then DO:
        IF histoent.ndos<>n_d THEN
            MESSAGE Traduction("Edition impossible",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos), STRING(n_d))
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
        ELSE run edboni_c (histoent.cod_cf,"HIST",histoent.no_cde,histoent.no_bl).
    END.
    /*...$1479*/
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bquitter W1 
PROCEDURE CHOOSE-Bquitter :
RUN save-ini.
    APPLY "close" TO THIS-PROCEDURE.
    RETURN NO-APPLY.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Breajuster W1 
PROCEDURE CHOOSE-Breajuster :
session:set-wait-state("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        ASSIGN
            logbid = HBrowse-Ent:FETCH-SELECTED-ROW(1)
            svrowid = rowid(histoent).
        IF NOT mode_validSR THEN DO: /*$2672*/
            IF histoent.ndos<>n_d THEN
                MESSAGE Traduction("R�ajustement impossible !",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
            ELSE DO:
                /*$1258...*/
                IF not(droit-modif) AND histoent.NO_fa_a[1] <> "" THEN DO:
                    MESSAGE Traduction("Vous n'�tes pas autoris� � modifier les lignes d'une facture d�j� contr�l�e en partie",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                    RETURN NO-APPLY.
                END.
                /*...$1258*/
                IF (parsoc.vfm_deb <> ? AND histoent.dat_liv < parsoc.vfm_deb) OR (parsoc.vfm_fin <> ? AND histoent.dat_liv > parsoc.vfm_fin) THEN DO: /*$A26501*/
                    MESSAGE Traduction("R�ajustement impossible !",-2,"") + " " + Traduction("Date livraison",-2,"") + " " + Traduction("en dehors de la p�riode de validit� mouvements",-2,"")
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
                    RETURN NO-APPLY.
                END.
                run saifrais_a (svrowid,OUTPUT logbid).
                IF svrowid = ? OR logbid THEN DO:
                    RUN openquery("",NO).
                    IF svrowid <> ? THEN DO:
                        HBrowse-Ent:SET-REPOSITIONED-ROW(HBrowse-Ent:NUM-ITERATIONS, "always").
                        HQuery-Ent:REPOSITION-TO-ROWID(svrowid) NO-ERROR.
                        IF HQuery-Ent:NUM-RESULTS <> 0 THEN HBrowse-Ent:SELECT-FOCUSED-ROW().
                    END.
                END.
                IF logbid AND HQuery-Ent:NUM-RESULTS <> 0 THEN APPLY "value-changed" TO HBrowse-Ent.
            END.
        END.
        /*$2672...*/
        ELSE DO:
            IF histoent.ndos<>n_d THEN
                MESSAGE Traduction("Validation impossible",-2,"") + " " + SUBSTITUTE(Traduction("La pi�ce s�lectionn�e n'est pas de la m�me soci�t� (&1) que celle en cours (&2) !",-2,""),STRING(histoent.ndos),STRING(n_d))
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
            ELSE IF (parsoc.vfm_deb <> ? AND histoent.dat_liv < parsoc.vfm_deb) OR (parsoc.vfm_fin <> ? AND histoent.dat_liv > parsoc.vfm_fin) THEN DO: /*$A26501*/
                MESSAGE Traduction("R�ajustement impossible !",-2,"") + " " + Traduction("Date livraison",-2,"") + " " + Traduction("en dehors de la p�riode de validit� mouvements",-2,"")
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
                RETURN NO-APPLY.
            END.
            ELSE DO:
                logbid = YES.
                MESSAGE Traduction("Voulez-vous vraiment valider la r�ception s�lectionn�e ?",-2,"") VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE logbid.
                IF logbid THEN DO:
                    logbid = MOB-SR-VALIDATION-PIED(histoent.typ_mvt,histoent.cod_cf,histoent.NO_cde,histoent.NO_bl).
                    IF logbid THEN DO:
                        MESSAGE Traduction("Traitement termin�.",-2,"") VIEW-AS ALERT-BOX INFO BUTTONS OK.
                        HBrowse-Ent:REFRESH().
                    END.
                    ELSE MESSAGE Traduction("Erreur lors de la validation",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                END.
                APPLY "VALUE-CHANGED" TO hbrowse-ent.
            END.
        END.
        /*...$2672*/
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bretour W1 
PROCEDURE CHOOSE-Bretour :
APPLY "go" TO FRAME frchoix.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bsscc W1 
PROCEDURE choose-bsscc :
/* $A40207 ... */
    IF AVAIL histoent AND pTypMvt = "F"        THEN RUN trapal_s (INT(SUBSTR(csoc:SCREEN-VAL IN FRAME F1,2)), histoent.NO_cde, ?, ?, ?, ?, ?).
    ELSE IF AVAIL histoent AND pTypMvt = "C"   THEN RUN trapal_s (INT(SUBSTR(csoc:SCREEN-VAL IN FRAME F1,2)), ?, ?, histoent.NO_cde, ?, ?, ?).
    /* .. $A40207 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Btn-varctrlf8 W1 
PROCEDURE CHOOSE-Btn-varctrlf8 :
IF NOT VALID-HANDLE (AffSVariante) THEN
        RUN afsvar2_gp PERSISTENT SET AffSVariante ("","").
    IF AVAIL histolig THEN
        RUN AffichageVariante IN AffSVariante (histolig.k_var + (IF histolig.k_opt <> "" THEN CHR (1) + histolig.k_opt ELSE "" /* $A35329 */) ,STRING(histolig.cod_pro)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Btraca W1 
PROCEDURE CHOOSE-Btraca :
/*$BOIS*/
DO WITH FRAME F1:
    session:set-wait ("general").
    IF HBrowse-Ent:NUM-SELECTED-ROWS > 0 THEN DO:
        HBrowse-Ent:FETCH-SELECTED-ROW(1).
        RUN tracol_bo("F",ROWID(histoent)).
    END.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-btracacde W1 
PROCEDURE choose-btracacde :
/* $A62337 ... */
    IF AVAIL histoent THEN RUN tracde_c ("V", histoent.cod_cf, histoent.NO_cde, histoent.NO_bl).
    /* ... $A62337 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-btrpaffret W1 
PROCEDURE CHOOSE-btrpaffret :
/*$1728 Creation proc�dure*/
    IF HBrowse-Ent:num-selected-rows = 1  then do:
        FIND FIRST enttrans where enttrans.transpor = histoent.cod_cf and enttrans.no_cde_t = histoent.NO_cde and enttrans.no_bl_t = histoent.NO_bl NO-LOCK NO-ERROR.
        IF AVAIL enttrans THEN RUN saitrp_a ( enttrans.NO_bon ) .

    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bvalider-edtbl W1 
PROCEDURE choose-bvalider-edtbl :
/*$A59971...*/
    valid = YES.
    APPLY "GO" TO FRAME Fedtbl.
    /*...$A59971*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bvalider-p W1 
PROCEDURE CHOOSE-Bvalider-p :
do with frame fplafac:
    if typ-plage:screen-val="N" and
     ((input fn-d=0 or input fn-f=0) or (input fn-d>input fn-f)) then do:
      message Traduction("Plage de n� incorrecte",-2,"") view-as alert-box error.
      apply "entry" to fn-d.
      return no-apply.
    end.
    else if typ-plage:screen-val="D" and
     ((input fdat-d=? or input fdat-f=?) or (input fdat-d>input fdat-f)) then do:
      message Traduction("Plage de dates incorrecte",-2,"") view-as alert-box error.
      apply "entry" to fdat-d.
      return no-apply.
    end.
    valid=YES.
    APPLY "GO" TO FRAME f1.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-Bvalider-r W1 
PROCEDURE CHOOSE-Bvalider-r :
valid = YES.
    APPLY "GO" TO FRAME Fedtfac.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CHOOSE-bXML W1 
PROCEDURE CHOOSE-bXML :
DO WITH FRAME F1:
DEF VAR iLigne AS INT    NO-UNDO.
DEF VAR iCpt   AS INT    NO-UNDO.
DEF VAR iQuery AS HANDLE NO-UNDO.
DEF VAR ListeRowId AS CHAR NO-UNDO INIT "".
    IF (ent$:SCREEN-VAL IN FRAME F1 = "E" OR ent$:SCREEN-VAL IN FRAME F1 = "T") AND VALID-HANDLE(Hbrowse-ent) THEN DO:
        iQuery = Hbrowse-ent:QUERY.
        iQuery:GET-LAST(NO-LOCK) NO-ERROR.
        iLigne = iQuery:NUM-RESULTS NO-ERROR.
        IF iLigne = ? THEN iLigne = 0.
        IF iLigne > 0 THEN DO:
           iQuery:GET-FIRST(NO-LOCK) NO-ERROR.
           REPEAT WHILE NOT iQuery:QUERY-OFF-END :
              ListeRowId = ListeRowId + STRING(iQuery:GET-BUFFER-HANDLE(1):ROWID) + "," NO-ERROR.
              iQuery:GET-NEXT(NO-LOCK) NO-ERROR.
           END.
        END.
        IF listeRowid <> "" THEN RUN getXml_GP("Histoent":U,"",RIGHT-TRIM(listeRowid,","),"client":U,(IF rs-selection:SCREEN-VAL IN FRAME F1 <> "C" OR vcod_cli:SCREEN-VAL IN FRAME F1 = "*" THEN 0 ELSE INT(vcod_cli:SCREEN-VAL IN FRAME F1))).
        ELSE MESSAGE traduction("Traitement impossible !",-2,"") VIEW-AS ALERT-BOX ERROR BUTTONS OK TITLE Traduction("Historique Commandes/B.L/Factures",-2,"").
        Hbrowse-ent:SET-REPOSITIONED-ROW(1) NO-ERROR.
        APPLY "entry" TO Hbrowse-ent.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Choose-ctypcli W1 
PROCEDURE Choose-ctypcli :
/*$A35573...*/
DEFINE VARIABLE pqui AS CHARACTER NO-UNDO.
DEFINE OUTPUT PARAMETER typcli AS INTEGER NO-UNDO.

DO WITH FRAME f1:
    CBComboGetSel (hComBars-f1, 2, {&ctypcli}, OUTPUT pqui).
    ASSIGN typcli = IF pqui = Traduction("Client command�",-2,"") OR pqui = "" /* $A71882 */ THEN 1 ELSE IF pqui = Traduction("Client factur�",-2,"") THEN 2 ELSE 3.

    IF rs-selection:SCREEN-VALUE = "C" THEN DO:
           ASSIGN vcod_cli:LABEL = IF typcli      = 1 THEN Traduction("Client command�",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")"
                                   ELSE IF typcli = 2 THEN Traduction("Client factur�",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")"
                                   ELSE                    Traduction("Client livr�",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")".
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ChooseBDoc W1 
PROCEDURE ChooseBDoc :
FIND CURRENT histoent NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
    IF AVAIL histoent THEN DO:
        IF histoent.no_info <> 0 AND histoent.no_info <> ? THEN DO:
            FIND FIRST infoent WHERE infoent.typ_fich = "E" AND infoent.no_info = histoent.no_info NO-LOCK NO-ERROR.
            IF AVAIL infoent THEN RUN mntinf_b ("E", histoent.no_info, Traduction("Documents pour la commande N�",-2,"") + STRING(histoent.no_cde) , "C" + STRING(histoent.cod_cf)).
            ELSE MESSAGE Traduction("Pas de documents...",-2,"") VIEW-AS ALERT-BOX INFO BUTTONS OK.
        END.
        ELSE MESSAGE Traduction("Pas de documents...",-2,"") VIEW-AS ALERT-BOX INFO BUTTONS OK.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation W1 
PROCEDURE CombarsCreation :
DEF VAR xitem          AS COM-HANDLE NO-UNDO.
    DEF VAR xitem2         AS COM-HANDLE NO-UNDO.

    RUN combars PERSISTENT SET hComBars.
    FIND FIRST parsoc NO-LOCK NO-ERROR. /*$BOIS*/ /* �XREF_NOWHERE� */

    /*L�gende*/
    hcombars-legende = cbocxadd (FRAME FLegende:HANDLE, OUTPUT frcombars-legende).
    CBSetSize(hcombars-legende, 1.04,  1.0, 1.15,FRAME FLegende:WIDTH - 1.66).
    CBAddBar(hcombars-legende, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-legende, NO).
    CBToolBarAccelTips (hcombars-legende, NO).
    CBLoadIcon(hcombars-legende, {&blegende-abandonner},    StdMedia + "\StdMedia.icl",1).

    CBAddItem(hcombars-legende, 2, ?, {&xtpControlButton}, {&blegende-abandonner}, Traduction("Abandonner",-2,"") , YES).
    CBSetItemStyle(hcombars-legende, 2, {&blegende-abandonner}, {&xtpButtonIconAndCaption}).

    /* Barre pour la frame f1 */
    hComBars-F1 = CBOCXAdd(FRAME F1:HANDLE, OUTPUT FrmComBars-F1).
    CBSetSize(hComBars-F1, 1.04, 28.26, 1.15,FRAME F1:WIDTH - 27.66).   /*taille et place de la barre*/
    CBAddBar(hComBars-F1, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-F1, NO).
    CBToolBarAccelTips(hComBars-F1,NO).

    CBLoadIcon(hComBars-F1, {&bquitter},            StdMedia + "\StdMedia.icl",94).
    CBLoadIcon(hComBars-f1, {&bafficher},               StdMedia + "\StdMedia.icl",95).
    CBLoadIcon(hComBars-f1, {&Bajouter-colonnes-ent},   StdMedia + "\StdMedia.icl",3).
    CBLoadIcon(hComBars-f1, {&Bajouter-colonnes-ent2},  StdMedia + "\StdMedia.icl",111).
    CBLoadIcon(hComBars-f1, {&Bajouter-colonnes-lig},   StdMedia + "\StdMedia.icl",2).
    CBLoadIcon(hComBars-f1, {&Bajouter-colonnes-lig2},  StdMedia + "\StdMedia.icl",110).
    CBLoadIcon(hComBars-f1, {&bpopup},              StdMedia + "\StdMedia.icl",91).
    CBLoadIcon(hComBars-f1, {&Baffac},              GcoMedia + "\GcoMedia.icl",21).
    CBLoadIcon(hComBars-f1, {&Boption},             StdMedia + "\StdMedia.icl",85).
    CBLoadIcon(hComBars-F1, {&badresse},            StdMedia + "\StdMedia.icl",78).
    /*CBAddImage(hcombars-f1, {&beclate},                  GcoMedia + "\Eclate.bmp").  YK002 ISMA Bouton Eclatement de Facture */
    CBLoadIcon(hComBars-f1, {&Bentete},             GcoMedia + "\GcoMedia.icl",83).
    CBLoadIcon(hComBars-f1, {&Bpied},               GcoMedia + "\GcoMedia.icl",89).
    CBLoadIcon(hComBars-f1, {&Baffcde},             GcoMedia + "\GcoMedia.icl",2).
    CBLoadIcon(hComBars-f1, {&Bcopier},             GcoMedia + "\GcoMedia.icl",12).
    CBLoadIcon(hComBars-f1, {&Binfo},               GcoMedia + "\GcoMedia.icl",70).
    CBLoadIcon(hComBars-f1, {&Bcolisage},           GcoMedia + "\GcoMedia.icl",49).
    CBLoadIcon(hComBars-f1, {&Bmarge},              GcoMedia + "\GcoMedia.icl",53).
    CBLoadIcon(hComBars-f1, {&Bdoc},                StdMedia + "\StdMedia.icl",72).
    CBLoadIcon(hComBars-f1, {&Betiq},               GcoMedia + "\GcoMedia.icl",60).
    CBAddImage(hComBars-f1, {&bXML},                GcoMedia + "\type_xml.ico"). /* �xref_CBADDImage� */ /*$A60174*/
    CBLoadIcon(hComBars-f1, {&bdetail},             GcoMedia + "\GcoMedia.icl",70).
    CBLoadIcon(hComBars-f1, {&bcr},                 GcoMedia + "\gcomedia.icl", 3). /*$A62727*/
    CBLoadIcon(hComBars-f1, {&blot},                GcoMedia + "\GcoMedia.icl",29).
    CBLoadIcon(hComBars-f1, {&bimm},                GcoMedia + "\GcoMedia.icl",28).
    CBLoadIcon(hComBars-f1, {&bemp},                GcoMedia + "\GcoMedia.icl",35).
    CBLoadIcon(hComBars-f1, {&Btn-varctrlf8},       GcoMedia + "\GcoMedia.icl",98).
    CBLoadIcon(hComBars-f1, {&Baffdetprod},         StdMedia + "\StdMedia.icl",56). /* $A33836 */
    CBAddImage(hcombars-f1, {&banarec},             GCOMedia + '\d75.bmp'). /* �xref_CBADDImage� */
    CBLoadIcon(hComBars-f1, {&Bcarvar},     StdMedia + "\StdMedia.icl",66).
    CBAddImage(hcombars-f1, {&blegende},    GCOMedia + '\legende.bmp'). /*�xref_CBADDImage�*/
    CBLoadIcon(hComBars-f1, {&bcompare},                StdMedia + "\stdmedia.icl", 120). /*$A68752*/
    CBAddImage(hComBars-f1, {&bgrille_rem}, GcoMedia + "\d888.bmp"). /*A56990*/ /* �xref_CBADDImage� */

    RUN CBDebutPersonnalisation IN hcombars("histfb_c", "hComBars-f1").

    CBLoadIcon(hComBars-f1, {&Bediter},             StdMedia + "\StdMedia.icl",57).
    IF parsoc.bois_ar AND ptypmvt <> "F" THEN
        CBLoadIcon(hComBars-f1, {&Beditfaci},       StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hComBars-f1, {&Bplage},              StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hComBars-f1, {&bediter-spe},         StdMedia + "\StdMedia.icl",57).
    CBLoadIcon(hComBars-f1, {&bediter-bonCommission},  StdMedia + "\StdMedia.icl",57).

    CBLoadIcon(hComBars-f1, {&Bfax},                StdMedia + "\StdMedia.icl",63).
    CBLoadIcon(hComBars-f1, {&Bmail},              StdMedia + "\StdMedia.icl",77).

    IF parsoc.traca_vte THEN CBLoadIcon(hComBars-f1, {&btracacde}, StdMedia + "\StdMedia.icl",125). /* $A62337 */

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bquitter}, "&" + Traduction("Quitter",-2,"") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&bquitter}, {&xtpButtonIconAndCaption}).

    /*YK002 Bouton D�nomination de Facture */
    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&badresse}, "&" + Traduction("...Adresse",-2,"") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&badresse}, {&xtpButtonIconAndCaption}).

    /*YK002 Bouton Eclatement de Facture 
    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bEclate}, "&" + Traduction("..Eclatement Fact",-2,"") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&bEclate}, {&xtpButtonIconAndCaption}).
    */

    /*$A35790...*/
    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlSplitButtonPopup}, {&bafficher}, Traduction("Afficher",-2,"") + " " + "(F5)" + "" , YES).
    CBSetItemStyle(hComBars-f1, 2, {&bafficher}, {&xtpButtonIconAndCaption}).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bautorefresh}, Traduction("Affichage automatique",-2,"") , NO).
    CBCheckItem(hComBars-f1, 2, {&bautorefresh}, valbtn_autorefresh ).
    /*...$A35790*/

    xitem = CbAddItemPrint(hComBars-f1, 2, ?, {&bediter}, "&" + Traduction("Bon Livraison",-2,"") ,YES,Nompgm).
    /*$A62526...*/
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&bblchiffr}, Traduction("B.L chiffr�",-2,"") , NO).
    CBCheckItem(hComBars-f1, 2, {&bblchiffr}, valbtn_blchiffr ).
    /*...$A62526*/
    IF parsoc.bois_ar AND ptypmvt <> "F" THEN CbAddItemPrint(hComBars-f1, 2, xitem, {&beditfaci}, "&" + Traduction("Facture interne",-2,"") ,YES,"EDTHFAI").
    IF parsoc.bois_ar THEN DO:
        CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-spe}, '&' + Traduction("Edition des sp�cifications",-2,"") , YES, "EDTSPE_BO"). /*$BOIS*/
        CBAddItemPrint(hcombars-f1, 2, xitem, {&bediter-bonCommission}, '&' + Traduction("Edition Bon de commission",-2,"") , YES, "EDTBC_BO"). /* $BOIS TEN */
    END.

    CbAddItemPrint(hComBars-f1, 2, ?, {&bplage}, "&" + Traduction("Plage de Factures",-2,"") ,YES,Nompgm3).

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&Breajuster}, "&" + Traduction("Modifier R�ception",-2,"") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&Breajuster}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&Baffac}, "&" + Traduction("Pi�ce",-2,"") + " (F11)", yes).
    CBSetItemStyle(hComBars-F1, 2, {&Baffac}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&bdetail}, "&" + Traduction("Mouvement",-2,"") + " (F10)", yes).
    CBSetItemStyle(hComBars-F1, 2, {&bdetail}, {&xtpButtonIconAndCaption}).

    /*$A62727...*/
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bcr}, Traduction("Compte-rendu de r�ception � la ligne et litiges associ�s",-2,"") , YES).
    CBSetItemStyle(hComBars-F1, 2, {&bcr}, {&xtpButtonIconAndCaption}).
    /*...$A62727*/

    CBAddItem(hComBars-F1, 2, ?, {&xtpControlButton}, {&Bcopier}, "&" + Traduction("Copier-Coller",-2,"") , yes).
    CBSetItemStyle(hComBars-F1, 2, {&Bcopier}, {&xtpButtonIconAndCaption}).
    CBSetItemTooltip(hComBars-F1, 2, {&Bcopier}, Traduction("Copier la pi�ce tout ou partie et la coller dans une pi�ce en cours",-2,"")).

    xitem = CBAddItem(hComBars-f1, 2, ?, {&xtpControlSplitButtonPopup}, {&bfax},Traduction("Envoyer facture par Fax " + "/" + " " + Traduction("Fax Express",-2,""),-2,""), yes).
    CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Bgstenvoi}, "&" + Traduction("Gestion des envois",-2,""), NO).

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bmail},Traduction("Envoyer facture par courriel",-2,""), NO).

    xitem-edt = CBAddItem(hComBars-f1, 2, ?, {&xtpControlPopup}, {&Boption}, "&" + Traduction("Options",-2,""), YES).
    CBSetItemStyle(hComBars-f1, 2, {&Boption}, {&xtpButtonIconAndCaption}).
    /*$BOIS...*/
    IF parsoc.bois_ar THEN DO:
        IF pTypmvt = "F" THEN CBAddItem(hComBars-f1, 2, xitem, {&xtpControlButton}, {&Btraca},Traduction("Tra�abilit�",-2,""), YES).
    END.
    /*...$BOIS*/
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Baffcde},Traduction("D�tail et �volution d'une commande",-2,"") + " (SHIFT-F8)", yes).

    /* $A62337 ... */
    IF parsoc.traca_vte THEN DO:
        CBAddItem(hcombars-f1, 2, xitem-edt, {&xtpControlButton}, {&btracacde}, Traduction("Tra�abilit� des modifications de commande",-2,""), NO).
        CBSetItemStyle(hcombars-f1, 2, {&btracacde}, {&xtpButtonIconAndCaption}).
    END.
    /* ... $A62337 */

    /*$A68752...*/
    CBAddItem(hcombars-f1, 2, xitem-edt, {&xtpControlButton}, {&bcompare}, '&' + Traduction("Comparaison avec le devis",-2,""), NO).
    CBSetItemStyle(hcombars-f1, 2, {&bcompare}, {&xtpButtonIconAndCaption}).
    /*...$A68752*/

    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bentete},"&" + Traduction("Visualisation infos en-t�te de la pi�ce",-2,""), yes).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bpied},Traduction("Visualisation infos fin de saisie",-2,""), NO).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Binfo},Traduction("D�tail des informations de l'en-t�te",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Banarec},Traduction("Analyse des r�ceptions",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&BdossierPR},"&" + Traduction("Dossier P.R",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Blitige},Traduction("Gestion des litiges",-2,""), YES).
    IF pTypMvt = "F" THEN ModeleCaract = parsoc.mok_rec. /*$A41075*/
    IF ModeleCaract <> "" THEN CBAddItem(hComBars-f1,2, xitem-edt, {&xtpControlButton}, {&bcarvar}, Traduction("Caract�ristiques globales de r�ception",-2,""), NO). /*$A41075*/
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bcolisage},Traduction("Liste de Colisage",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bmarge},Traduction("Visualisation des marges",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bdoc},Traduction("Visualiser les documents li�s � l'en-t�te",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Betiq},(IF ptypmvt = "F" THEN "&" + Traduction("R��diter �tiquettes de r�ception",-2,"") ELSE "&" + Traduction("R��diter �tiquettes de livraison",-2,"")), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&btrpaffret},Traduction("Commande client" + "/" + Traduction("fournisseur",-2,"") + " ",-2,""), yes).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Btn-varctrlf8},Traduction("Affichage des variantes",-2,""), YES).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&bimm}, Traduction("N� S�ries",-2,"") , NO).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&blot}, Traduction("Lots",-2,"") , NO).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&bemp}, Traduction("Emplacements",-2,"") , NO).
    IF ptypmvt = "C" AND parsoc.grille_rem THEN CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&bgrille_rem}, Traduction("Grille de remises illimit�es",-2,"") , NO). /*A56990*/
    IF ptypmvt <> "H" THEN CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Baffdetprod}, Traduction("Afficher le d�tail des produits pour cette pi�ce",-2,"") , YES). /* $A33836 */
    IF Ptypmvt =  "C" THEN CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&bXML}, Traduction("G�n�rer fichier",-2,"") + " XML":U , NO). /*$A60174*/

    /* $A40207 ... */
    IF parsoc.sscc OR parsoc.sscc_sor /* $A68043 */ THEN DO:
        CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, {&Bsscc}, "&" + Traduction("Tra�abilit� S.S.C.C",-2,"") , YES).
    END.
    /* ... $A40207 */

    /*$A35573...*/
    IF ptypmvt = "C" THEN DO:
        CBAddItem(hCombars-f1, 2, xitem-edt, {&xtpControlComboBox}, {&ctypcli}, Traduction("Client en tant que " + ":",-2,""), YES).
        CBSetItemStyle(hCombars-f1, 2, {&ctypcli}, {&xtpButtonCaption}).
        CBComboSetWidth(hCombars-f1, 2, {&ctypcli}, 90).
    END.
    /*...$A35573*/

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&Bajouter-colonnes-ent}, Traduction("Ajouter",-2,"") + "," + Traduction("r�organiser",-2,"") + "," + Traduction("changer la condition des en-t�tes",-2,"") + " " + "(CTRL-F12)" + "" , YES).
    CBSetItemRightAlign (hComBars-f1, 2, {&Bajouter-colonnes-ent}, YES).
    CBAddItem(hComBars-f1, 2, ?, {&xtpControlButton}, {&bajouter-colonnes-lig}, Traduction("Ajouter",-2,"") + "," + Traduction("r�organiser",-2,"") + "," + Traduction("changer la condition des lignes",-2,"") + " (CTRL-F12)", NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&bajouter-colonnes-lig}, YES).

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&blegende}, Traduction("L�gende",-2,"") , NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&Blegende}, YES).

    CBAddItem(hComBars-f1, 2, ?, {&xtpControlLabel}, {&bpopup}, "" , NO).
    CBSetItemRightAlign (hComBars-f1, 2, {&bpopup}, YES).

    RUN CBFinPersonnalisation IN hcombars("histfb_c", "hComBars-f1", hComBars-f1, 2).

    /*$A35573...*/
    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client command�",-2,""), 1).
    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client factur�",-2,""), 2).
    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client livr�",-2,""), 3).

    CBComboSetSelEx(hComBars-f1, 2, {&ctypcli}, 1).
    /*...$A35573*/

    xitem-edt = CBGetItemComHandle(hcombars-f1, 2, {&Boption}).

    /* Barre pour la frame fedtfac */
    hComBars-fedtfac = CBOCXAdd(FRAME fedtfac:HANDLE, OUTPUT FrmComBars-fedtfac).
    CBSetSize(hComBars-fedtfac, 1.04, 1.14, 1.15,FRAME fedtfac:WIDTH - 1.6).   /*taille et place de la barre*/
    CBAddBar(hComBars-fedtfac, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-fedtfac, NO).
    CBToolBarAccelTips(hComBars-fedtfac,NO).

    CBLoadIcon(hComBars-fedtfac, {&bannuler-r}, StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hComBars-fedtfac, {&bvalider-r}, StdMedia + "\StdMedia.icl",116).

    CBAddItem(hComBars-fedtfac, 2, ?, {&xtpControlButton}, {&bannuler-r}, "&" + Traduction("Abandonner",-2,"") , yes).
    CBSetItemStyle(hComBars-fedtfac, 2, {&bannuler-r}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-fedtfac, 2, ?, {&xtpControlButton}, {&bvalider-r}, "&" + Traduction("Valider",-2,"") + " (F9)", NO).
    CBSetItemStyle(hComBars-fedtfac, 2, {&bvalider-r}, {&xtpButtonIconAndCaption}).

    /* Barre pour la frame fplafac */
    hComBars-fplafac = CBOCXAdd(FRAME fplafac:HANDLE, OUTPUT FrmComBars-fplafac).
    CBSetSize(hComBars-fplafac, 1.04, 1.14, 1.15,FRAME fplafac:WIDTH - 1.6).   /*taille et place de la barre*/
    CBAddBar(hComBars-fplafac, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-fplafac, NO).
    CBToolBarAccelTips(hComBars-fplafac,NO).

    CBLoadIcon(hComBars-fplafac, {&bannuler-p}, StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hComBars-fplafac, {&bvalider-p}, StdMedia + "\StdMedia.icl",116).

    CBAddItem(hComBars-fplafac, 2, ?, {&xtpControlButton}, {&bannuler-p}, "&" + Traduction("Abandonner",-2,"") , yes).
    CBSetItemStyle(hComBars-fplafac, 2, {&bannuler-p}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-fplafac, 2, ?, {&xtpControlButton}, {&bvalider-p}, "&" + Traduction("Valider",-2,"") + " (F9)", NO).
    CBSetItemStyle(hComBars-fplafac, 2, {&bvalider-p}, {&xtpButtonIconAndCaption}).

    /* Barre pour la frame Frchoix */
    hComBars-Frchoix = CBOCXAdd(FRAME Frchoix:HANDLE, OUTPUT FrmComBars-Frchoix).
    CBSetSize(hComBars-Frchoix, 1.04, 1.14, 1.15,FRAME Frchoix:WIDTH - 1.6).   /*taille et place de la barre*/
    CBAddBar(hComBars-Frchoix, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-Frchoix, NO).
    CBToolBarAccelTips(hComBars-Frchoix,NO).

    CBLoadIcon(hComBars-Frchoix, {&Bretour},    StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hComBars-Frchoix, {&Bediterc},   StdMedia + "\StdMedia.icl",116).

    CBAddItem(hComBars-Frchoix, 2, ?, {&xtpControlButton}, {&Bretour}, "&" + Traduction("Abandonner",-2,"") , yes).
    CBSetItemStyle(hComBars-Frchoix, 2, {&Bretour}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-Frchoix, 2, ?, {&xtpControlButton}, {&Bediterc}, "&" + Traduction("Valider",-2,"") + " (F9)", NO).
    CBSetItemStyle(hComBars-Frchoix, 2, {&Bediterc}, {&xtpButtonIconAndCaption}).

    /*$A59971...*/
    /* Barre pour la frame fedtbl */
    hComBars-fedtbl = CBOCXAdd(FRAME fedtbl:HANDLE, OUTPUT FrmComBars-fedtbl).
    CBSetSize(hComBars-fedtbl, 1.04, 1.14, 1.15,FRAME fedtbl:WIDTH - 1.6).   /*taille et place de la barre*/
    CBAddBar(hComBars-fedtbl, "Barre d'outils 1", {&xtpBarTop}, YES, NO).
    CBShowMenu(hComBars-fedtbl, NO).
    CBToolBarAccelTips(hComBars-fedtbl,NO).

    CBLoadIcon(hComBars-fedtbl, {&bannuler-edtbl}, StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hComBars-fedtbl, {&bvalider-edtbl}, StdMedia + "\StdMedia.icl",116).

    CBAddItem(hComBars-fedtbl, 2, ?, {&xtpControlButton}, {&bannuler-edtbl}, "&" + Traduction("Abandonner",-2,"") , yes).
    CBSetItemStyle(hComBars-fedtbl, 2, {&bannuler-edtbl}, {&xtpButtonIconAndCaption}).

    CBAddItem(hComBars-fedtbl, 2, ?, {&xtpControlButton}, {&bvalider-edtbl}, "&" + Traduction("Valider",-2,"") + " (F9)", NO).
    CBSetItemStyle(hComBars-fedtbl, 2, {&bvalider-edtbl}, {&xtpButtonIconAndCaption}).
    /*...$A59971*/

    RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE(FRAME F1:HANDLE, Traduction("Historique Commandes/B.L/Factures",-2,"")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ComBarsExecute W1 
PROCEDURE ComBarsExecute :
DEFINE INPUT PARAMETER pNumComBar AS INT NO-UNDO.
    DEFINE INPUT PARAMETER pButton    AS INT NO-UNDO.

    /* $2222 ... Editions archiv�es dans la GED */
    IF pButton >= 800 AND pButton < 850 THEN DO:
        FIND FIRST tt-doc WHERE tt-doc.tt-num = pButton NO-LOCK NO-ERROR.
        IF AVAIL tt-doc THEN RUN Ouvrir_document IN G-hGED (tt-doc.tt-cledoc, OUTPUT logbid).
    END.
    /* ... $2222 */

    IF pNumComBar = hcombars-legende THEN CASE pbutton :
        WHEN {&blegende-abandonner} THEN RUN choose-blegende-abandonner.
    END CASE.
    ELSE     IF pNumComBar = hComBars-f1 THEN CASE pButton:
        WHEN {&blegende} THEN RUN choose-blegende.
        WHEN {&bcarvar}  THEN RUN choose-Bcarvar.
        WHEN {&bQuitter}          THEN RUN CHOOSE-Bquitter.
        WHEN {&badresse}          THEN RUN CHOOSE-Badresse.   /*YK002 ISMA Bouton D�nomination de Facture */
        /*WHEN {&bEclate}          THEN RUN CHOOSE-BEclate.   YK002 ISMA Bouton Eclatement de Facture */
        WHEN {&bafficher}     THEN RUN CHOOSE-BAFFICHER.
        WHEN {&bautorefresh}  THEN DO:
            IF CBGetItemCheckState  (hComBars-f1, 2, {&bautorefresh}) THEN DO:
                CBCheckItem(hComBars-f1, 2, {&bautorefresh}, NO).
                valbtn_autorefresh = NO.
            END.
            ELSE DO:
                CBCheckItem(hComBars-f1, 2, {&bautorefresh}, YES).
                valbtn_autorefresh = YES.
            END.
        END.
        WHEN {&Bajouter-colonnes-ent} THEN RUN CHOOSE-Bajouter-colonnes-ent.
        WHEN {&Bajouter-colonnes-lig} THEN DO:
            IF rtype:SCREEN-VAL IN FRAME f1 = "F" AND Ptypmvt = "F" THEN RUN CHOOSE-BAJOUTER-COLONNES-LIG2.
            ELSE RUN CHOOSE-BAJOUTER-COLONNES-LIG.
        END.
        WHEN {&Baffac}            THEN RUN CHOOSE-Baffac.
        WHEN {&Bentete}           THEN RUN CHOOSE-Bentete.
        WHEN {&Bpied}             THEN RUN CHOOSE-Bpied.
        WHEN {&Baffcde}           THEN RUN CHOOSE-Baffcde.
        WHEN {&Bcopier}           THEN RUN CHOOSE-Bcopier.
        WHEN {&Binfo}             THEN RUN CHOOSE-Binfo.
        WHEN {&Bcolisage}         THEN RUN CHOOSE-Bcolisage.
        WHEN {&Bmarge}            THEN RUN CHOOSE-Bmarge.
        WHEN {&Bdoc}              THEN RUN CHOOSE-Bdoc.
        WHEN {&Betiq}             THEN RUN CHOOSE-Betiq.
        WHEN {&BXML}              THEN RUN CHOOSE-BXML.  /*$A60174*/
        WHEN {&Bediter}           THEN RUN CHOOSE-Bediter.
        WHEN {&Beditfaci}         THEN RUN CHOOSE-Beditfaci.
        WHEN {&Bplage}            THEN RUN CHOOSE-Bplage.
        WHEN {&Banarec}           THEN RUN CHOOSE-Banarec.
        WHEN {&Breajuster}        THEN RUN CHOOSE-Breajuster.
        WHEN {&BdossierPR}        THEN RUN CHOOSE-BdossierPR.
        WHEN {&Blitige}           THEN RUN CHOOSE-Blitige.
        WHEN {&Baffdetprod}       THEN RUN CHOOSE-Baffdetprod. /* $A33836 */
        WHEN {&Btraca}            THEN RUN CHOOSE-Btraca. /*$BOIS*/
        WHEN {&bediter-spe}       THEN RUN CHOOSE-BEDITER-SPE.  /*$BOIS*/
        WHEN {&bediter-bonCommission} THEN RUN CHOOSE-BEDITER-BONCOMMISSION. /* $BOIS TEN */
        WHEN {&Bfax}              THEN DO:
            Faxer = YES.
            RUN CHOOSE-Bfax.
        END.
        WHEN {&Bmail}             THEN DO:
            Faxer = NO.
            RUN CHOOSE-Bfax.
        END.
        WHEN {&Bgstenvoi} THEN DO:
            session:set-wait ("general").
            RUN mntfax("C", IF AVAIL histoent THEN histoent.cod_cf ELSE 0).
            session:set-wait ("").
        END.
        WHEN {&btrpaffret} THEN RUN CHOOSE-btrpaffret .
        WHEN {&Bdetail}  THEN RUN CHOOSE-Bdetail.
        WHEN {&bcr} THEN RUN Choose-Bcr. /*$A62727*/
        WHEN {&bimm} THEN RUN choose-bimm.
        WHEN {&blot} THEN RUN choose-blot.
        WHEN {&bgrille_rem} THEN RUN choose-bgrille_rem. /*A56990*/
        WHEN {&bemp} THEN RUN choose-bemp.
        WHEN {&Btn-varctrlf8} THEN RUN CHOOSE-Btn-varctrlf8.
        WHEN {&bsscc}   THEN RUN Choose-bsscc. /* $A40207 */
        /*$A35573...*/
        WHEN {&ctypcli}   THEN do:
            RUN choose-ctypcli(OUTPUT typclient).
            RUN openquery("",YES).
        END. /*...$A35573*/
        WHEN {&btracacde} THEN RUN choose-btracacde. /* $A62337 */
        WHEN {&bcompare}  THEN RUN afficherComparaison. /*$A68752*/
        /*$A62526...*/
        WHEN {&bblchiffr} THEN DO:
            ASSIGN valbtn_blchiffr = NOT valbtn_blchiffr
                   Logbid = CBCheckItem(hComBars-f1, 2, {&bblchiffr}, valbtn_blchiffr ).
        END.
        /*...$A62526*/
    END CASE.
    ELSE IF pNumComBar = hComBars-fedtfac THEN CASE pButton:
        WHEN {&Bannuler-r}        THEN RUN CHOOSE-Bannuler-r.
        WHEN {&Bvalider-r}        THEN RUN CHOOSE-Bvalider-r.
    END CASE.
    ELSE IF pNumComBar = hComBars-fplafac THEN CASE pButton:
        WHEN {&Bannuler-p}        THEN RUN CHOOSE-Bannuler-p.
        WHEN {&Bvalider-p}        THEN RUN CHOOSE-Bvalider-p.
    END CASE.
    ELSE IF pNumComBar = hComBars-frchoix THEN CASE pButton:
        WHEN {&Bretour}           THEN RUN CHOOSE-Bretour.
        WHEN {&Bediterc}          THEN RUN CHOOSE-Bediterc.
    END CASE.
    /*$A59971...*/
    ELSE IF pNumComBar = hComBars-fedtbl THEN CASE pButton:
        WHEN {&Bannuler-edtbl}        THEN RUN CHOOSE-Bannuler-edtbl.
        WHEN {&Bvalider-edtbl}        THEN RUN CHOOSE-Bvalider-edtbl.
    END CASE.
    /*...$A59971*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load W1  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "histfb_c.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
    chCtrlFrame-2 = CtrlFrame-2:COM-HANDLE
    UIB_S = chCtrlFrame-2:LoadControls( OCXFile, "CtrlFrame-2":U)
    CtrlFrame-2:NAME = "CtrlFrame-2":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "histfb_c.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creation-browse-ligfac W1 
PROCEDURE creation-browse-ligfac :
DEF VAR tmpch AS CHAR no-undo.
DO WITH FRAME F1:
    session:set-wait ("general").
    DEL-RESIZEWIDGET (Hbrowse-lig).
    DELETE OBJECT Hbrowse-lig NO-ERROR.
    DELETE OBJECT Hquery-lig  NO-ERROR.

    IF qliste-champ-lig2 = ? THEN DO:
        ASSIGN
            qliste-champ-lig2  = "no_ligne,typ_elem,nom_pro" + (IF g-langue <> "" THEN ",lldespro.des_lan" ELSE "") /*A35891*/ + ",qte,*ua,":U +
                            (IF NOT pas-droit-vpa THEN "px_ach,devise,mt_ht," ELSE "") + "litige,int_lit,no_cde,no_bl," +
                            (IF NOT pas-droit-vpa THEN "px_ach_old," ELSE "") +
                            "qte_old," +
                            (IF NOT pas-droit-vpa THEN "mt_ht_old," ELSE "") +
                            "ndos,refint,refext,cod_pro,cod_dec1,cod_dec2,cod_dec3,cod_dec4,cod_dec5"
            qliste-label-lig2  = Traduction("N� Ligne",-2,"") + "|" + Traduction("Type",-2,"") + "|" + Traduction("D�signation Produit",-2,"") + (IF g-langue <> "" THEN "|" + Traduction("D�signation Produit",-2,"") + " " + g-langue ELSE "") + "|" + Traduction("Quantit�",-2,"") + "|"
                            + Traduction("U.A",-2,"") + "|" +
                            (IF NOT pas-droit-vpa THEN Traduction("Px Achat",-2,"") + "|" + Traduction("Devise",-2,"") + "|" + Traduction("Total H.T",-2,"") + "�" + "t" + "|" ELSE "") +
                            Traduction("Litige",-2,"") + "|" + Traduction("Intitul� Litige",-2,"") + "|" + Traduction("N� Commande",-2,"") + "|" + Traduction("N� B.L",-2,"") + "|" +
                           (IF NOT pas-droit-vpa THEN Traduction("Px ach B.L",-2,"") + " " + "|" ELSE "") +
                            Traduction("Quantit�",-2,"") + " " + Traduction("B.L",-2,"") + "|" +
                            (IF NOT pas-droit-vpa THEN Traduction("Total H.T",-2,"") + " " + Traduction("B.L",-2,"") + "�" + "t" + "|" ELSE "") +
                            Traduction("Soci�t�",-2,"") + "|" +
                            Traduction("R�f. int",-2,"") + "|" + Traduction("R�f. externe",-2,"") + "|" + Traduction("Produit",-2,"") + "|" + "D1" + "|" + "D2" + "|" + "D3" + "|" + "D4" + "|" + "D5"
            qcondition-lig2    = ""
            qcolonne-lock-lig2 = 0.
    END.

    IF COLORD-lig2=? OR INDEX(qliste-champ-lig2,COLORD-lig2)=0 THEN ASSIGN
        COLORD-lig2="no_ligne"
        CHQRYBY-lig2="BY " + COLORD-lig2
        COLDESC-lig2=NO.

    IF qcondition-lig2 = ? THEN qcondition-lig2 = "".
    IF qcondition-lig2 <> "" THEN CBSetItemIcon (hComBars-f1, 2, {&Bajouter-colonnes-lig}, {&Bajouter-colonnes-lig2}).
    ELSE CBSetItemIcon (hComBars-f1, 2, {&Bajouter-colonnes-lig}, {&Bajouter-colonnes-lig}).

    /* Cr�ation du browse dynamique */
    CREATE BROWSE HBrowse-lig
    ASSIGN FRAME = FRAME f1:HANDLE
        WIDTH = IF TBARBO:CHECKED THEN (113.60 - butt-spli:COL) ELSE 113.14
        COL = IF TBARBO:CHECKED THEN (butt-spli:COL + 0.5) ELSE 1.14
        HEIGHT = 8.25
           ROW   = 13.88
           BGCOLOR  = 48
           FGCOLOR  = ?
           FONT     = 49
           SENSITIVE        = YES
           EXPANDABLE       = YES
           SEPARATORS       = YES
           READ-ONLY        = NO
           ROW-MARKERS      = NO
           MULTIPLE         = NO
           PRIVATE-DATA     = "ligfa":U
           COLUMN-SCROLLING = YES
           COLUMN-RESIZABLE = YES.

    ON ROW-DISPLAY OF HBrowse-lig PERSISTENT RUN rowdisplay-lig2               IN THIS-PROCEDURE.
    ON 'ctrl-f12'  OF HBrowse-lig PERSISTENT RUN CHOOSE-BAJOUTER-COLONNES-lig2 IN THIS-PROCEDURE.

    ADD-RESIZEBWDYN-P (HBrowse-lig, "WHY",100,50).

    CREATE QUERY Hquery-lig.
    HQuery-lig:CACHE = 10000.
    Hquery-lig:SET-BUFFERS(BUFFER ligfacfo:HANDLE).

     /* A35891 Infos d�signation langue */
    IF INDEX(qliste-champ-lig2,"lldespro.":U)<>0 THEN DO:
        qprepare-lldespro=", first lldespro where lldespro.cod_pro = ligfacfo.cod_pro AND lldespro.langue = '" + g-langue + "' NO-LOCK OUTER-JOIN ".
        Hquery-lig:add-BUFFER(BUFFER lldespro:HANDLE).
    END.
    ELSE qprepare-lldespro="".

    hcoldisp-lig = ?.
    Qprepare-lig = "ligfacfo.ndos = ":U + string(entfacfo.ndos) + " AND ligfacfo.cod_fou = " + string(entfacfo.cod_fou) + " AND ligfacfo.NO_fact = '" + entfacfo.NO_fact + "' AND ligfacfo.dat_fac = " + string(entfacfo.dat_fac).

    IF qcondition-lig2 <> "" THEN qprepare-lig = qprepare-lig + " and " + qcondition-lig2.

    Qprepare-lig=REPLACE(Qprepare-lig,"�"," and ").
    logbid = Hquery-lig:QUERY-PREPARE("FOR EACH ligfacfo WHERE " + Qprepare-lig + " NO-LOCK " + qprepare-lldespro + CHQRYBY-lig2) NO-ERROR.

    If not logbid then message Traduction("Probl�me dans la condition !",-2,"") VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        HBrowse-lig:QUERY = Hquery-lig.
        LoadSettings("HBHIL" + ptypmvt + "3", OUTPUT hSettingsApi).
        DO X = 1 TO NUM-ENTRIES(qliste-champ-lig2) ON ERROR UNDO,LEAVE ON STOP UNDO,LEAVE:
            IF pas-droit-vpa AND ENTRY(x,qliste-champ-lig2) BEGINS 'px_ach' THEN NEXT.
            IF ENTRY(x,qliste-champ-lig2)="*ua" THEN HCol = Hbrowse-lig:ADD-CALC-COLUMN("CHARACTER","X(3)","","").
            ELSE IF (qprepare-lldespro<>"" AND SUBSTR(ENTRY(x,qliste-champ-lig2),1,9)="lldespro.":U) THEN HCol=HBrowse-lig:ADD-LIKE-COLUMN(ENTRY(x,qliste-champ-lig2)). /*A35891*/
            ELSE HCol = HBrowse-lig:ADD-LIKE-COLUMN("ligfacfo.":U + ENTRY(x,qliste-champ-lig2)).
            ASSIGN
                Hcol:NAME = ENTRY(x,qliste-champ-lig2)
                HCol:LABEL=ENTRY(1,ENTRY(X,qliste-label-lig2,"|"),"�")
                HCol:LABEL-FGCOLOR=?
                HCol:LABEL-BGCOLOR=?.

            IF hcol:DATA-TYPE = "logical" AND hcol:BUFFER-FIELD:FORMAT = "{&UTrema}/" THEN hcol:COLUMN-FONT = 47.

            qchar-lig2 = GetSetting(ENTRY(x,qliste-champ-lig2), ?).
            IF qchar-lig2 <> ? THEN HCol:WIDTH = DEC(qchar-lig2).

            IF INDEX(ENTRY(X,qliste-label-lig2,"|"),"�t")<>0 THEN hcol:PRIVATE-DATA="tot".

            CASE ENTRY(x,qliste-champ-lig2):
                WHEN '*ua'      THEN hcoldisp-lig[1]=hcol:HANDLE.
                WHEN 'qte':U      THEN hcoldisp-lig[2]=hcol:HANDLE.
                WHEN 'px_ach'   THEN hcoldisp-lig[3]=hcol:HANDLE.
                WHEN 'px_ach_old'   THEN hcoldisp-lig[4]=hcol:HANDLE.
                WHEN 'mt_ht'   THEN hcoldisp-lig[5]=hcol:HANDLE.
                WHEN 'mt_ht_old'   THEN hcoldisp-lig[6]=hcol:HANDLE.
                WHEN 'lldespro.des_lan' THEN hcoldisp-lig[7]=hcol:HANDLE.
                WHEN 'nom_pro' THEN hcoldisp-lig[8]=hcol:HANDLE. /*$A62727*/
            END.

            IF hcol:NAME=COLORD-lig2 THEN HCol:LABEL-FGCOLOR=IF COLDESC-lig2 THEN 4 ELSE 2.
        END.
        UnloadSettings(hSettingsApi).

        IF qcolonne-lock-lig2 <= NUM-ENTRIES(qliste-champ-lig2) THEN HBrowse-lig:NUM-LOCKED-COLUMNS = qcolonne-lock-lig2.
        HQuery-lig:QUERY-OPEN().
        TRACEQUERY(hquery-lig:HANDLE).
    END.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE creation-browse-ligne W1 
PROCEDURE creation-browse-ligne :
DEF VAR tmpch AS CHAR no-undo.
DO WITH FRAME F1:
    session:set-wait ("general").
    DEL-RESIZEWIDGET (Hbrowse-lig).
    DELETE OBJECT Hbrowse-lig NO-ERROR.
    DELETE OBJECT Hquery-lig  NO-ERROR.

    IF qliste-champ-lig = ? THEN DO:
        IF ptypmvt ="c" THEN ASSIGN
            qliste-champ-lig  = "no_ligne,typ_elem,nom_pro" + (IF g-langue <> "" THEN ",lldespro.des_lan" ELSE "") /*A35891*/ + ",qte,*uv,px_vte_i," + /*$A35969...*/ (IF parsoc.prix_franco AND NOT pas-droit-vtrsp THEN "*px_march,px_trs," ELSE "") + /*...$A35969*/ "uni_fac,dev_vte,remise1,rem_val[1]," +
                            (IF parsoc.grille_rem THEN "grille_rem," ELSE "") + /*$A56990*/
                            "remise2,rem_val[2],remise3,rem_val[3],remise4,rem_val[4],mt_ht,poid_tot,px_vte," +
                            (IF NOT pas-droit-vpa THEN "px_ach," ELSE "") +
                            (IF NOT pas-droit-vpm THEN "pmp,":U ELSE "") +
                            (IF NOT pas-droit-vpr THEN "px_rvt," ELSE "") +
                            "dat_mvt,ndos,depot,refint,refext,cod_pro,cod_dec1,cod_dec2,cod_dec3,cod_dec4,cod_dec5,niveau1,niveau2,niveau3,niveau4,niveau5"
            qliste-label-lig  = Traduction("N� Ligne",-2,"") + "|" + Traduction("Type",-2,"") + "|" + Traduction("D�signation Produit",-2,"") + (IF g-langue <> "" THEN "|" + Traduction("D�signation Produit",-2,"") + " " + g-langue ELSE "") + "|" + Traduction("Quantit�",-2,"") + "|"
                            + Traduction("U.V",-2,"") + "|" + Traduction("Px Vente",-2,"") /*$A35969...*/ + (IF parsoc.prix_franco AND NOT pas-droit-vtrsp THEN "|" + Traduction("Dont px march.",-2,"") + "|" + Traduction("Dont px transport",-2,"") ELSE "") /*...$A35969*/ + "|" + Traduction("U.F",-2,"") + "|" + Traduction("Devise",-2,"") + "|"
                            + Traduction("Remise 1",-2,"") + "|" /*$1798*/ + Traduction("Rem V1",-2,"") + "|"
                            + (IF parsoc.grille_rem THEN Traduction("Grille rem",-2,"") + "|" ELSE "")  /*$A56990*/
                            + Traduction("Remise 2",-2,"") + "|" /*$1798*/ + Traduction("Rem V2",-2,"") + "|"
                            + Traduction("Remise 3",-2,"") + "|" /*$1798*/ + Traduction("Rem V3",-2,"") + "|"
                            + Traduction("Remise 4",-2,"") + "|" /*$1798*/ + Traduction("Rem V4",-2,"") + "|"
                            + Traduction("Total H.T",-2,"") + " " + g-dftdev + "�" + "t" + "|" + Traduction("Poids",-2,"") + "�" + "t" + "|" + Traduction("P.V",-2,"") + " " + g-dftdev + "|" + "" +
                            (IF NOT pas-droit-vpa THEN Traduction("P.A",-2,"") + " " + g-dftdev + "|" ELSE "") +
                            (IF NOT pas-droit-vpm THEN Traduction("P.M.P",-2,"") + " " + g-dftdev + "|" ELSE "") +
                            (IF NOT pas-droit-vpr THEN Traduction("P.R",-2,"") + " " + g-dftdev + "|" ELSE "") +
                            (IF parsoc.FORM_liv=3 THEN Traduction("Date exp.",-2,"") ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�p.",-2,"") ELSE Traduction("Date liv.",-2,"")) + "|" +
                            Traduction("Soci�t�",-2,"") + "|" + Traduction("D�p�t",-2,"") + "|"
                            + Traduction("R�f. int",-2,"") + "|" + Traduction("R�f. externe",-2,"") + "|" + Traduction("Produit",-2,"") + "|" + "D1" + "|" + "D2" + "|" + "D3" + "|" + "D4" + "|" + "D5" + "|" + "n1":U + "|" + "n2":U + "|" + "n3":U + "|" + "n4":U + "|" + "n5":U.
        ELSE ASSIGN
            qliste-champ-lig  = "no_ligne,typ_elem,nom_pro" + (IF g-langue <> "" THEN ",lldespro.des_lan" ELSE "") /*A35891*/ + ",qte,*ua,":U +
                            (IF NOT pas-droit-vpa THEN "px_ach,devise,remise1,rem_val[1],remise2,rem_val[2],remise3,rem_val[3],remise4,rem_val[4],mt_ht," ELSE "") + "poid_tot," +
                            (IF NOT pas-droit-vpa THEN "px_unit," ELSE "") +
                            (IF NOT pas-droit-vpm THEN "pmp,":U ELSE "") +
                            (IF NOT pas-droit-vpr THEN "px_rvt," ELSE "") +
                            "dat_mvt,ndos,depot,refint,refext,cod_pro,cod_dec1,cod_dec2,cod_dec3,cod_dec4,cod_dec5"
            qliste-label-lig  = Traduction("N� Ligne",-2,"") + "|" + Traduction("Type",-2,"") + "|" + Traduction("D�signation Produit",-2,"") + (IF g-langue <> "" THEN "|" + Traduction("D�signation Produit",-2,"") + " " + g-langue ELSE "") + "|" + Traduction("Quantit�",-2,"") + "|"
                            + Traduction("U.A",-2,"") + "|" +
                            (IF NOT pas-droit-vpa THEN Traduction("Px Achat",-2,"") + "|" + Traduction("Devise",-2,"") + "|"
                            + Traduction("Remise 1",-2,"") + "|" /*$1798*/ + Traduction("Rem V1",-2,"") + "|"
                            + Traduction("Remise 2",-2,"") + "|" /*$1798*/ + Traduction("Rem V2",-2,"") + "|"
                            + Traduction("Remise 3",-2,"") + "|" /*$1798*/ + Traduction("Rem V3",-2,"") + "|"
                            + Traduction("Remise 4",-2,"") + "|" /*$1798*/ + Traduction("Rem V4",-2,"") + "|"
                            + Traduction("Total H.T",-2,"") + " " + g-dftdev + "�" + "t" + "|" ELSE "")  + Traduction("Poids",-2,"") + "�" + "t" + "|" +
                            (IF NOT pas-droit-vpa THEN Traduction("P.A",-2,"") + " " + g-dftdev + "|" ELSE "") +
                            (IF NOT pas-droit-vpm THEN Traduction("P.M.P",-2,"") + " " + g-dftdev + "|" ELSE "") +
                           (IF NOT pas-droit-vpr THEN Traduction("P.R",-2,"") + " " + g-dftdev + "|" ELSE "") +
                            Traduction("Date Liv.",-2,"") + "|" + Traduction("Soci�t�",-2,"") + "|" + Traduction("D�p�t",-2,"") + "|"
                            + Traduction("R�f. int",-2,"") + "|" + Traduction("R�f. externe",-2,"") + "|" + Traduction("Produit",-2,"") + "|" + "D1" + "|" + "D2" + "|" + "D3" + "|" + "D4" + "|" + "D5".
        ASSIGN
            qcondition-lig    = ""
            qcolonne-lock-lig = 0.
    END.

    run del-colremval in HndRemval(parsoc.typ_rem,"histolig",input-o qliste-champ-lig,input-o qliste-label-lig). /*$1798*/

    IF COLORD-lig=? OR INDEX(qliste-champ-lig,COLORD-lig)=0 THEN ASSIGN
        COLORD-lig="no_ligne"
        CHQRYBY-lig="BY " + COLORD-lig
        COLDESC-lig=NO.

    /*A35891*/
    IF INDEX(qliste-champ-lig,"histoent")<>0 THEN avec-joint=YES.
                                           ELSE avec-joint=NO.

    IF qcondition-lig = ? THEN qcondition-lig = "".
    IF qcondition-lig <> "" THEN CBSetItemIcon (hComBars-f1, 2, {&Bajouter-colonnes-lig}, {&Bajouter-colonnes-lig2}).
    ELSE CBSetItemIcon (hComBars-f1, 2, {&Bajouter-colonnes-lig}, {&Bajouter-colonnes-lig}).

    /* Cr�ation du browse dynamique */
    CREATE BROWSE HBrowse-lig
    ASSIGN FRAME = FRAME f1:HANDLE
        WIDTH = IF TBARBO:CHECKED THEN (113.60 - butt-spli:COL) ELSE 113.14
        COL = IF TBARBO:CHECKED THEN (butt-spli:COL + 0.5) ELSE 1.14
        ROW   = 13.88
        HEIGHT = 8.25
           BGCOLOR  = 48
           FGCOLOR  = ?
           FONT     = 49
           SENSITIVE        = YES
           EXPANDABLE       = YES
           SEPARATORS       = YES
           READ-ONLY        = NO
           ROW-MARKERS      = NO
           MULTIPLE         = NO
           PRIVATE-DATA     = "lig"
           COLUMN-SCROLLING = YES
           COLUMN-RESIZABLE = YES.

    ON DEFAULT-ACTION OF HBrowse-lig PERSISTENT RUN DEFAULTACTION-LIG IN THIS-PROCEDURE.
    ON ROW-DISPLAY    OF HBrowse-lig PERSISTENT RUN rowdisplay-lig               IN THIS-PROCEDURE.
    ON VALUE-CHANGED  OF HBrowse-lig PERSISTENT RUN valuechanged-lig             IN THIS-PROCEDURE.
    ON 'ctrl-f12'     OF HBrowse-lig PERSISTENT RUN CHOOSE-BAJOUTER-COLONNES-LIG IN THIS-PROCEDURE.

    ADD-RESIZEBWDYN-P (HBrowse-lig, "WHY",100,50).


    CREATE QUERY Hquery-lig.
    HQuery-lig:CACHE = 10000.
    IF avec-joint THEN DO:
        Hquery-lig:SET-BUFFERS(BUFFER histolig:HANDLE, BUFFER histoent:HANDLE). /*A35891*/
        qprepare-histoent = ", FIRST HISTOENT WHERE histoent.typ_mvt = histolig.typ_mvt and histoent.cod_cf = histolig.cod_cf and histoent.no_cde=histolig.no_cde and histoent.no_bl=histolig.no_bl NO-LOCK ".
    END.
    ELSE Hquery-lig:SET-BUFFERS(BUFFER histolig:HANDLE).

    /* A35891 Infos d�signation langue */
    IF INDEX(qliste-champ-lig,"lldespro.":U)<>0 THEN DO:
        qprepare-lldespro=", first lldespro where lldespro.cod_pro = histolig.cod_pro AND lldespro.langue = '" + g-langue + "' NO-LOCK OUTER-JOIN ".
        Hquery-lig:add-BUFFER(BUFFER lldespro:HANDLE).
    END.
    ELSE qprepare-lldespro="".

    hcoldisp-lig = ?.
    IF pTypMvt = "F" THEN Qprepare-lig = " AND histolig.cod_cf = " + string(histoent.cod_cf) + " AND histolig.NO_cde = " + string(histoent.NO_cde) + " AND histolig.NO_bl = " + string(histoent.NO_bl) + " AND histolig.nmc_lie <> 'S' ". /*$A87991*/
                     ELSE Qprepare-lig = "histolig.typ_mvt = '" + histoent.typ_mvt + "' AND histolig.cod_cf = " + string(histoent.cod_cf) + " AND histolig.NO_cde = " + string(histoent.NO_cde) + " AND histolig.NO_bl = " + string(histoent.NO_bl) + " AND histolig.nmc_lie <> 'S' "
                     /* $A73116 ... */ + (IF histoent.usr_mod = 'situation':U AND histoent.ind_con > 0 THEN " AND histolig.ind_con = " + STRING(histoent.ind_con) ELSE ""). /* ... $A73116 */

    IF qcondition-lig <> "" THEN qprepare-lig = qprepare-lig + " and " + qcondition-lig.
    IF VALID-HANDLE (dynreq_histolig) THEN /* $1495 */ DO:
        RUN RequeteFrame IN dynreq_histolig (OUTPUT tmpch).
        IF tmpch <> "" THEN Qprepare-lig = Qprepare-lig + " and " + tmpch.
    END.

    Qprepare-lig=REPLACE(Qprepare-lig,"�"," and ").

    IF pTypMvt = "F" THEN Qprepare-lig = "(histolig.typ_mvt = '" + histoent.typ_mvt + "'" + Qprepare-lig + ") Or (histolig.typ_mvt = 'V'" + Qprepare-lig + ")". /*$A87991*/

    logbid = Hquery-lig:QUERY-PREPARE("FOR EACH histolig WHERE " + Qprepare-lig + " NO-LOCK " + (IF avec-joint THEN qprepare-histoent ELSE "") + qprepare-lldespro + CHQRYBY-lig) NO-ERROR. /*�XREF_TRACEQ�*/
    If not logbid then message Traduction("Probl�me dans la condition !",-2,"") VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        HBrowse-lig:QUERY = Hquery-lig.
        IF VALID-HANDLE (dynreq_histolig) THEN RUN ViderChampCalcule IN dynreq_histolig. /*$A91177*/
        LoadSettings("HBHIL" + ptypmvt, OUTPUT hSettingsApi).
        DO X = 1 TO NUM-ENTRIES(qliste-champ-lig) ON ERROR UNDO,LEAVE ON STOP UNDO,LEAVE:
            IF pas-droit-vpr AND ENTRY(x,qliste-champ-lig) = 'px_rvt' THEN NEXT.
            IF pas-droit-vpa AND ENTRY(x,qliste-champ-lig) = 'px_ach' THEN NEXT.
            IF pas-droit-vpm AND ENTRY(x,qliste-champ-lig) = 'pmp':U THEN NEXT.
            IF pas-droit-vtrsp AND ENTRY(x,qliste-champ-lig) = 'px_trs':U THEN NEXT. /*$A35969*/
            IF pas-droit-vtrsp AND ENTRY(x,qliste-champ-lig) = '*px_march':U THEN NEXT. /*$A35969*/

            IF ENTRY(X,qliste-champ-lig) BEGINS "*$" THEN  /*$A91177*/
            DO:
                IF NOT VALID-HANDLE (dynreq_histolig) THEN NEXT.
                RUN AjouterChampCalcule IN dynreq_histolig (ENTRY(X,qliste-champ-lig),HBrowse-lig, OUTPUT hcol).
                IF hcol = ? THEN NEXT.
            END.
            ELSE IF ENTRY(x,qliste-champ-lig)="*uv" THEN HCol = Hbrowse-lig:ADD-CALC-COLUMN("CHARACTER","X(3)","","").
            ELSE IF (qprepare-lldespro<>"" AND SUBSTR(ENTRY(x,qliste-champ-lig),1,9)="lldespro.":U) THEN HCol=Hbrowse-lig:ADD-LIKE-COLUMN(ENTRY(x,qliste-champ-lig)). /*A35891*/
            ELSE IF ENTRY(x,qliste-champ-lig)="*ua" THEN HCol = Hbrowse-lig:ADD-CALC-COLUMN("CHARACTER","X(3)","","").
            ELSE IF ENTRY(x,qliste-champ-lig)="*px_march" THEN HCol = Hbrowse-lig:ADD-CALC-COLUMN("DECIMAL",">>>>>>9.99","",""). /*$A35969*/
            ELSE IF SUBSTR(ENTRY(x,qliste-champ-lig),1,9)="histoent." THEN HCol=HBrowse-lig:ADD-LIKE-COLUMN(ENTRY(x,qliste-champ-lig)). /*A35891*/
            ELSE HCol = HBrowse-lig:ADD-LIKE-COLUMN("histolig." + ENTRY(x,qliste-champ-lig)).
            ASSIGN
                Hcol:NAME = ENTRY(x,qliste-champ-lig)
                HCol:LABEL=ENTRY(1,ENTRY(X,qliste-label-lig,"|"),"�")
                HCol:LABEL-FGCOLOR=?
                HCol:LABEL-BGCOLOR=?.

            IF hcol:DATA-TYPE = "logical" AND hcol:BUFFER-FIELD:FORMAT = "{&UTrema}/" THEN hcol:COLUMN-FONT = 47.

            qchar-lig = GetSetting(ENTRY(x,qliste-champ-lig), ?).
            IF qchar-lig <> ? THEN HCol:WIDTH = DEC(qchar-lig).

            IF INDEX(ENTRY(X,qliste-label-lig,"|"),"�t")<>0 THEN hcol:PRIVATE-DATA="tot".

            CASE ENTRY(x,qliste-champ-lig):
                WHEN '*uv'      THEN hcoldisp-lig[1]=hcol:HANDLE.
                WHEN 'qte':U      THEN hcoldisp-lig[2]=hcol:HANDLE.
                WHEN 'px_vte'   THEN hcoldisp-lig[3]=hcol:HANDLE.
                WHEN 'px_ach'   THEN hcoldisp-lig[4]=hcol:HANDLE.
                WHEN 'pmp':U      THEN hcoldisp-lig[5]=hcol:HANDLE.
                WHEN 'px_rvt'   THEN hcoldisp-lig[6]=hcol:HANDLE.
                WHEN 'cod_pro'  THEN hcoldisp-lig[7]=hcol:HANDLE.
                WHEN 'nom_pro'  THEN hcoldisp-lig[8]=hcol:HANDLE.
                WHEN 'refint'   THEN hcoldisp-lig[9]=hcol:HANDLE.
                WHEN 'typ_elem' THEN hcoldisp-lig[10]=hcol:HANDLE.
                WHEN 'niveau1':U THEN hcoldisp-lig[11]=hcol:HANDLE.
                WHEN 'niveau2':U THEN hcoldisp-lig[12]=hcol:HANDLE.
                WHEN 'niveau3':U THEN hcoldisp-lig[13]=hcol:HANDLE.
                WHEN 'niveau4':U THEN hcoldisp-lig[15]=hcol:HANDLE.
                WHEN 'niveau5':U THEN hcoldisp-lig[16]=hcol:HANDLE.
                WHEN 'px_vte_i' THEN hcoldisp-lig[17]=hcol:HANDLE.
                WHEN '*ua'      THEN hcoldisp-lig[18]=hcol:HANDLE.
                WHEN 'mt_ht'   THEN hcoldisp-lig[19]=hcol:HANDLE.
                WHEN '*px_march' THEN hcoldisp-lig[20]=hcol:HANDLE. /*$A35969*/
                WHEN 'px_trs' THEN hcoldisp-lig[21]=hcol:HANDLE. /*$A35969*/
                WHEN 'lldespro.des_lan' THEN hcoldisp-lig[22]=hcol:HANDLE. /*$A35969*/
            END.

            IF hcol:NAME=COLORD-lig THEN HCol:LABEL-FGCOLOR=IF COLDESC-lig THEN 4 ELSE 2.
        END.
        UnloadSettings(hSettingsApi).

        IF qcolonne-lock-lig <= NUM-ENTRIES(qliste-champ-lig) THEN HBrowse-lig:NUM-LOCKED-COLUMNS = qcolonne-lock-lig.
        HQuery-lig:QUERY-OPEN().
        TRACEQUERY(hquery-lig:HANDLE).
        IF VALID-HANDLE (dynreq_histolig) THEN RUN Afficherrequete IN dynreq_histolig (IF AVAIL histolig AND VALID-HANDLE (hbrowse-lig) AND HBrowse-lig:NUM-SELECTED-ROWS = 1 THEN ROWID (histolig) ELSE ?). /*$A91177*/
    END.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE defaultaction-ent W1 
PROCEDURE defaultaction-ent :
DO WITH FRAME F1:
    RUN CHOOSE-Baffac.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE defaultaction-lig W1 
PROCEDURE defaultaction-lig :
DO WITH FRAME F1:
    RUN CHOOSE-BDETAIL.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W1  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W1)
  THEN DELETE WIDGET W1.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FinPErsonnalisationCB W1 
PROCEDURE FinPErsonnalisationCB :
/* $A35573 */
    DEF INPUT PARAM pnumcombar AS INT NO-UNDO.
    DEF INPUT PARAM pbutton AS INT NO-UNDO.

    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client command�",-2,""), 1).
    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client factur�",-2,""), 2).
    CBComboAddItem(hCombars-f1, 2, {&ctypcli}, Traduction("Client livr�",-2,""), 3).

    CBComboSetSelEx(hComBars-f1, 2, {&ctypcli}, 1).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE load-ini W1 
PROCEDURE load-ini :
DO WITH FRAME F1:
    IF loadsettings("HBHIS" + ptypmvt, output hsettingsapi) then do:
        ASSIGN vchar                 = GetSetting ("AutoRefresh" ,/*$A93217 ?*/ "no") /*$A35790*/
            valbtn_autorefresh    = (vchar<>"no") /*$A35790*/
            vdepot         = GetSetting("CSoc",?)
            ent$:SCREEN-VALUE = GetSetting("Entete","E")
            CHQRYBY-ent        = GetSetting("ToutTri",?)
            COLDESC-ent        = (CHQRYBY-ent<>? AND SUBSTR(CHQRYBY-ent,LENGTH(CHQRYBY-ent) - 3,4)="DESC")
            COLORD-ent         = GetSetting("ChampTri",?)
            qliste-champ-ent   = GetSetting("*HBColonnes",?)
            qchar-ent          = GetSetting("*HBBloquees",?)
            qcolonne-lock-ent  = INT(qchar-ent)
            qliste-label-ent   = GetSetting("*HBLabels",?)
            qcondition-Ent     = GetSetting("*HBCondition",?)
            tbarbo:CHECKED = GetSetting("Arbo",?) = "yes"
            chaine         = GetSetting("Type",?).

        IF valbtn_autorefresh THEN CBCheckItem(hComBars-f1, 2, {&bautorefresh}, YES). /*$A35790*/
        IF chaine <> ? AND chaine <> "" THEN rtype:SCREEN-VAL = chaine.

        IF paffaire <> "" THEN DO:
            ASSIGN
                rs-selection:SCREEN-VAL = "A"
                vcod_cli:LABEL = Traduction("Affaire",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Toutes",-1,"") + ")"
                chaine = paffaire.
        END.
        ELSE if pCodeTiers = ? THEN DO:
            /*IF parsoc.affaire THEN DO: */ /* $A66083 */
                chaine = GetSetting("TypeTiers",?).
                IF chaine <> ? THEN rs-selection:SCREEN-VAL = chaine.
                CASE rs-selection:SCREEN-VAL :
                    WHEN "C" THEN /*$A35573...*/ DO :
                        RUN Choose-ctypcli(OUTPUT typclient). /*...$A35573*/
                        /*vcod_cli:LABEL = Traduction("Client",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")".*/
                        CBComboSetSelEx  (hCombars-f1, 2, {&ctypcli}, 1). /*$A35573*/
                    END.
                    WHEN "F" THEN vcod_cli:LABEL = (/*$1728...*/ IF ptypmvt = "H":u THEN Traduction("Transporteur",-2,"") ELSE /*...$1728*/ Traduction("Fournisseur",-1,"")) + " " + "(" + "" + "*" + "" + "=" + Traduction("Tous",-1,"") + ")".
                    WHEN "A" THEN vcod_cli:LABEL = Traduction("Affaire",-1,"") + " " + "(" + "" + "*" + "" + "=" + Traduction("Toutes",-1,"") + ")".
                END CASE.
            /*END.*/ /* $A66083 */
            chaine = GetSetting("Tiers",?).
        END.
        else chaine = string(pCodeTiers).
        IF chaine <> ? THEN vcod_cli:SCREEN-VAL = chaine.

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("HBHIL" + ptypmvt, OUTPUT hsettingsapi) THEN DO:
        ASSIGN CHQRYBY-lig = GetSetting("ToutTri",?)
               COLORD-lig  = GetSetting("ChampTri",?)
               qliste-champ-lig  = GetSetting("*HBColonnes",?)
               qcolonne-lock-lig = INT(GetSetting("*HBBloquees","0"))
               qliste-label-lig  = GetSetting("*HBLabels",?)
               qcondition-lig    = GetSetting("*HBCondition",?).
        UnloadSettings(hSettingsApi).
    END.
    COLDESC-lig=(CHQRYBY-lig<>? AND SUBSTR(CHQRYBY-lig,LENGTH(CHQRYBY-lig) - 3,4)="DESC").

    /* Browse Entfacfo */
    IF loadsettings("HBHIS" + ptypmvt + "3", output hsettingsapi) then do:
        ASSIGN CHQRYBY-ent2       = GetSetting("ToutTri",?)
               COLDESC-ent2       = (CHQRYBY-ent2<>? AND SUBSTR(CHQRYBY-ent2,LENGTH(CHQRYBY-ent2) - 3,4)="DESC")
               COLORD-ent2        = GetSetting("ChampTri",?)
               qliste-champ-ent2  = GetSetting("*HBColonnes",?)
               qchar-ent2         = GetSetting("*HBBloquees",?)
               qcolonne-lock-ent2 = INT(qchar-ent2)
               qliste-label-ent2  = GetSetting("*HBLabels",?)
               qcondition-ent2    = GetSetting("*HBCondition",?).

        UnloadSettings(hSettingsApi).
    END.
    /* Browse ligfacfo */
    IF loadsettings("HBHIL" + ptypmvt + "3", output hsettingsapi) then do:
        ASSIGN CHQRYBY-lig2       = GetSetting("ToutTri",?)
               COLDESC-lig2       = (CHQRYBY-lig2<>? AND SUBSTR(CHQRYBY-lig2,LENGTH(CHQRYBY-lig2) - 3,4)="DESC")
               COLORD-lig2        = GetSetting("ChampTri",?)
               qliste-champ-lig2  = GetSetting("*HBColonnes",?)
               qchar-lig2         = GetSetting("*HBBloquees",?)
               qcolonne-lock-lig2 = INT(qchar-lig2)
               qliste-label-lig2  = GetSetting("*HBLabels",?)
               qcondition-lig2    = GetSetting("*HBCondition",?).

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("HISTFB":U, output hsettingsapi) then do:
        ASSIGN
            FRAME frm-composants:X = INT(GetSetting("FrmCompoX","287")) /* $A33837 */
            FRAME frm-composants:Y = INT(GetSetting("FrmCompoY","210")). /* $A33837 */
        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("EDTHF", output hsettingsapi) then do:
        vex = GetSetting("Exemplaire",?).

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("SAIREC", output hsettingsapi) then do:
        chaine = GetSetting("Preference",?).

        IF chaine <> ? AND chaine <> "" THEN rsref:SCREEN-VAL IN FRAME Frchoix = chaine.
        ELSE rsref:SCREEN-VAL = "C".
        UnloadSettings(hSettingsApi).
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openquery W1 
PROCEDURE openquery :
DEFINE INPUT PARAMETER Pcle AS CHARACTER no-undo.
DEF INPUT PARAM pmode AS LOG no-undo.                   /*$A35790*/

DEF VAR tmpch AS char no-undo. /* $1495 */
DEF VAR v-char AS CHAR NO-UNDO . /*$1728*/
DEF VAR v-int AS INT NO-UNDO .   /*$1728*/

DO WITH FRAME F1:

    IF valbtn_autorefresh = NO AND pmode = NO THEN RETURN. /*$A35790*/

    IF Vcod_cli:SCREEN-VAL = "" THEN Vcod_cli:SCREEN-VAL = "*". /* $A39559 */
    mode_validSR = NO. /*$2672*/

    RUN choose-ctypcli(OUTPUT typclient). /*$A35573*/
    session:set-wait ("general").

    /* Suppression des objets dynamiques */
    DEL-RESIZEWIDGET (HBrowse-Ent).
    /*DEL-RESIZEWIDGET (Hbrowse-lig). $A54459 */
    DELETE OBJECT HQuery-Ent NO-ERROR.
    DELETE OBJECT HBrowse-Ent NO-ERROR.
    /*DELETE OBJECT Hbrowse-lig NO-ERROR.
 *     DELETE OBJECT Hquery-lig  NO-ERROR. $A54459 */

    RELEASE histoent NO-ERROR.
    RELEASE entfacfo NO-ERROR.
    HIDE FTxtAucun.

    ASSIGN vcod_cli fdatedeb Fdatefin rtype fnum falpha
           Logbid = CBShowItem(Hcombars-f1, 2, {&Bmarge},NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&Bediter}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&Banarec}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&Beditfaci}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&Bplage}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&breajuster}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&bdossierPR}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&blitige}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&bmail}, NO)
           Logbid = CBShowItem(Hcombars-f1, 2, {&bfax}, NO).


    /* Cr�ation du browse dynamique */
    CREATE BROWSE HBrowse-Ent
       ASSIGN FRAME = FRAME F1:HANDLE
              WIDTH = IF TBARBO:CHECKED THEN (113.60 - butt-spli:COL) ELSE 113.14
              COL = IF TBARBO:CHECKED THEN (butt-spli:COL + 0.5) ELSE 1.14
              ROW = 5.6
              HEIGHT = IF ent$:screen-val="E" THEN 16.54 ELSE 8.25
              BGCOLOR = 48
              FONT = 49
              SENSITIVE = YES
              EXPANDABLE = YES
              SEPARATORS = YES
              PRIVATE-DATA  = "ent":U
              ROW-MARKERS=NO
              COLUMN-SCROLLING=YES
              COLUMN-RESIZABLE=YES.

    IF ent$:SCREEN-VAL="E" THEN ADD-RESIZEBWDYN (HBrowse-ent, "WH").
    ELSE ADD-RESIZEBWDYN-P (HBrowse-ent, "WH",100,50).

    /* Triggers persistents car l'objet browse n'existe pas toujours */
    ON DEFAULT-ACTION OF HBrowse-Ent PERSISTENT RUN defaultaction-ent IN THIS-PROCEDURE.
    ON VALUE-CHANGED  OF HBrowse-Ent PERSISTENT RUN valuechanged-ent IN THIS-PROCEDURE.
    ON ROW-DISPLAY    OF HBrowse-Ent PERSISTENT RUN rowdisplay-ent IN THIS-PROCEDURE. /*$1130*/
    ON 'ctrl-f12'     OF HBrowse-ent PERSISTENT RUN CHOOSE-BAJOUTER-COLONNES-ent IN THIS-PROCEDURE.

    /* Creation du query */
    CREATE QUERY HQuery-Ent.
    HQuery-Ent:CACHE=10000.

    /* On lit les Entfacfo */
    hcoldisp-ent = ?.
    IF rtype:SCREEN-VAL = "F" AND Ptypmvt = "F" THEN RUN openquery2.
    ELSE DO:
        /* Initialisation des colonnes et conditions */
        IF qliste-champ-ent=? THEN ASSIGN
            qliste-champ-ent = (IF ptypmvt="F" /*$1728*/ OR ptypmvt = "H":u THEN
                "dat_cde,dat_liv,dat_acc,no_cde,no_bl_a,no_fa_a[1],ref_cde,adr_fac[1]," + (IF NOT pas-droit-vpa THEN "mt_cde,mt_ht_dev,mt_fact," ELSE "") + "devise,not_ref,ndos,depot,rem_glo[1],dos_pxr,cal_marg,fret"
                ELSE
                "dat_cde,dat_liv,dat_dep,dat_fac,no_cde,no_bl,no_fact,ref_cde,not_ref,mt_fact,mt_fach,mt_cde,mt_ht,adr_liv[1],adr_fac[1],ndos,depot,adr_cde[1]")
            qliste-label-ent = (IF ptypmvt="F" /*$1728*/ OR ptypmvt = "H":u THEN
                Traduction("Date Cde",-2,"") + "|" + Traduction("Date Liv.",-2,"") + "|" + Traduction("Liv. Acc.",-2,"") + "|" + Traduction("N� Cde",-2,"") + "|" + Traduction("N� B.L",-2,"") + "|" + Traduction("N� Facture",-2,"") + "|" + Traduction("R�f�rence fourn.",-2,"") + "|" + Traduction("Fournisseur",-2,"") + "|" +
                (IF NOT pas-droit-vpa THEN Traduction("B.L",-2,"") + " " + Traduction("HT",-2,"") + " " + g-dftdev + "�t" + "|" + Traduction("B.L",-2,"") + " " + Traduction("HT",-2,"") + "�t" + "|" + Traduction("Facture",-2,"") + " " + Traduction("HT",-2,"") + "�t" + "|" ELSE "") +
                Traduction("Devise",-2,"") + "|" +
                Traduction("R�f�rence A.R.C",-2,"") + "|" + Traduction("Soci�t�",-2,"") + "|" + Traduction("D�p�t",-2,"") + "|" + Traduction("% frais d'approche",-2,"") + "|" + Traduction("Dossier P.R",-2,"") + "|" + Traduction("Groupage",-2,"") + "|" + Traduction("Fret",-2,"")
                ELSE
                Traduction("Date Cde",-2,"") + "|" +
                (IF parsoc.FORM_liv=3 THEN Traduction("Date exp.",-2,"") ELSE IF parsoc.FORM_liv=2 THEN Traduction("Date d�p.",-2,"") ELSE Traduction("Date liv.",-2,"")) + "|" +
                (IF parsoc.FORM_liv=3 THEN Traduction("Exp. ini.",-2,"") ELSE IF parsoc.FORM_liv=2 THEN Traduction("D�p. ini.",-2,"") ELSE Traduction("Liv. ini.",-2,"")) + "|" +
                Traduction("Date Fac.",-2,"") + "|" +
                Traduction("N� Cde",-2,"") + "|" + Traduction("N� B.L",-2,"") + "|" + Traduction("N� Facture",-2,"") + "|" +
                Traduction("R�f�rence Client",-2,"") + "|" + Traduction("Notre R�f�rence",-2,"") + "|" +
                Traduction("Facture",-2,"") + " " + Traduction("TTC",-2,"") + " " + g-dftdev + "�t" + "|" +
                Traduction("Facture",-2,"") + " " + Traduction("HT",-2,"") + " " + g-dftdev + "�t" + "|" +
                Traduction("B.L",-2,"") + " " + Traduction("TTC",-2,"") + " " + g-dftdev + "�t" + "|" +
                Traduction("B.L",-2,"") + " " + Traduction("HT",-2,"") + "�t" + "|" +  /* A59782 */
                Traduction("Livr�",-2,"") + "|" + Traduction("Factur�",-2,"") + "|" + Traduction("Soci�t�",-2,"") + "|" + Traduction("D�p�t",-2,"") + "|" + Traduction("Command�",-2,""))
            qcondition-Ent = ""
            qcolonne-lock-ent = 0.

        IF COLORD-ent=? OR INDEX(qliste-champ-ent,COLORD-ent)=0 THEN ASSIGN
            COLORD-ent= (IF Ptypmvt = "F" THEN "dat_liv" ELSE "dat_fac")
            CHQRYBY-ent="BY " + COLORD-ent + " DESC"
            COLDESC-ent=YES.

        /*$A93217...*/
        IF NOT CHQRYBY-ent MATCHES ("*typ_mvt*") THEN DO:
            IF CHQRYBY-ent MATCHES("*desc*":U) THEN CHQRYBY-ent  = "by typ_mvt desc " + CHQRYBY-ent.
            ELSE CHQRYBY-ent  = "by typ_mvt " + CHQRYBY-ent.
        END.
        /*...$A93217*/

        IF CHQRYBY-ent=? THEN CHQRYBY-ent="".
        IF qcondition-Ent=? THEN qcondition-Ent="".

        IF qcondition-Ent<>"" THEN CBSetItemIcon(hComBars-f1, 2, {&Bajouter-colonnes-ent}, {&Bajouter-colonnes-ent2}).
        ELSE CBSetItemIcon(hComBars-f1, 2, {&Bajouter-colonnes-ent}, {&Bajouter-colonnes-ent}).

        HQuery-Ent:SET-BUFFERS(BUFFER histoent:HANDLE).
        /* $1837 */
        IF ptypmvt = "C" AND parsoc.ges_ate THEN  /* $879 */
             HQuery-Ent:ADD-BUFFER(BUFFER ateent:HANDLE).
        IF ptypmvt = "C" AND parsoc.ges_car THEN  /* $879 */
             HQuery-Ent:ADD-BUFFER(BUFFER carent:HANDLE).
        /* $A38917... */
        IF clause-cli <> "" THEN DO:
            HQuery-Ent:ADD-BUFFER(BUFFER client:HANDLE).
            qprepare-cli = " ,FIRST client WHERE client.cod_cli = histoent.cod_cf AND " + clause-cli + " NO-LOCK".
        END.
        ELSE qprepare-cli = "".
                /* ...$A38917 */

        /* $A39559... */
        IF clause-fou <> "" THEN DO:
            HQuery-Ent:ADD-BUFFER(BUFFER fournis:HANDLE).
            qprepare-fou = ", FIRST fournis WHERE fournis.cod_fou = histoent.cod_cf AND " + clause-fou.
        END.
        ELSE qprepare-fou = "".
        /* ...$A39559 */

        qprepare-Ent="histoent.typ_mvt= '" + ptypmvt + "'".
        IF vcod_cli:HIDDEN = NO AND vcod_cli:SCREEN-VAL <> "*" THEN DO:
            IF CAN-DO("C,F",rs-selection:SCREEN-VAL) THEN /*$A35573...*/ do:
                IF typclient = 2 AND rs-selection:SCREEN-VALUE = "C" THEN qprepare-Ent=qprepare-Ent + " �cl_fact=" + STRING(Vcod_cli:SCREEN-VAL).
                ELSE IF typclient = 3 AND rs-selection:SCREEN-VALUE = "C" THEN qprepare-Ent=qprepare-Ent + " �cl_livre=" + STRING(Vcod_cli:SCREEN-VAL).
                ELSE /*...$A35573*/ qprepare-Ent=qprepare-Ent + " �cod_cf=" + STRING(Vcod_cli:SCREEN-VAL).
            END. /*$A35573*/
            ELSE qprepare-Ent=qprepare-Ent + " �affaire='":U + Vcod_cli:SCREEN-VAL + "'".
        END.
        IF substr(Pcle,1,1) = "F" THEN qprepare-Ent=qprepare-Ent + " �no_fact=" + SUBSTR(Pcle,2,9).
        ELSE IF substr(Pcle,1,1) = "C" THEN qprepare-Ent=qprepare-Ent + " �no_cde=" + SUBSTR(Pcle,2,9).
        ELSE IF substr(Pcle,1,2) = "BL" THEN qprepare-Ent=qprepare-Ent + " �no_bl=" + SUBSTR(Pcle,3,9).
        IF substr(Pcle,21,2) = "BL" THEN DO:
            IF ptypmvt = "C" THEN qprepare-Ent=qprepare-Ent + " �no_bl=" + SUBSTR(Pcle,23,9).
                             ELSE qprepare-Ent=qprepare-Ent + " �no_bl_a='" + trim(SUBSTR(Pcle,23,20)) + "'".
        END.
        /* A partir de */
        IF tbapartir:CHECKED AND (fnum <> 0 AND fnum:HIDDEN = NO) OR (Falpha <> "" AND Falpha:HIDDEN = NO) THEN DO:
            /*$1728...*/
            IF ptypmvt = "H":u  THEN DO :
                v-int = INTEGER ( trim(Falpha) ) NO-ERROR .
                IF v-int > 0 THEN v-char = "AFFRET ":u + STRING(v-int , "999999999":u).
                ELSE v-char = trim(Falpha) .
            END. /*...$1728*/
            IF rtype="F" THEN qprepare-Ent=qprepare-Ent + " �no_fact >= " + STRING(fnum).
            ELSE IF rtype="C" THEN qprepare-Ent=qprepare-Ent + " �no_cde >= " + STRING(fnum).
            ELSE DO:
                IF ptypmvt = "C" then qprepare-Ent=qprepare-Ent + " �no_bl >= " + STRING(fnum).
                ELSE IF ptypmvt = "H":u THEN qprepare-Ent=qprepare-Ent + " �no_bl_a >= '":u + v-char + "'":u. /*$1728*/
                    ELSE qprepare-Ent=qprepare-Ent + " �no_bl_a >= '" + trim(Falpha) + "'".
            END.
        END.

        IF fdatedeb:HIDDEN = NO THEN DO:
            IF rtype="F" THEN qprepare-Ent=qprepare-Ent + " �dat_fac >= " + STRING(fdatedeb) + " �dat_fac <= " + STRING(fdatefin).
              ELSE IF rtype="C" THEN qprepare-Ent=qprepare-Ent + " �dat_cde >= " + STRING(fdatedeb) + " �dat_cde <= " + STRING(fdatefin).
                  ELSE qprepare-Ent=qprepare-Ent + " �dat_liv >= " + STRING(fdatedeb) + " �dat_liv <= " + STRING(fdatefin).
        END.

        IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" THEN
            Qprepare-ent = Qprepare-ent + " �CAN-DO('":U + ListeDepots + "',STRING(depot)) ":U.
        ELSE IF n_soc<>0 AND CAN-DO(LstSoc,STRING(n_soc)) THEN
            Qprepare-ent = Qprepare-ent + " �ndos = " + STRING(n_soc) + " �CAN-DO('":U + ListeDepots + "',STRING(depot)) ":U.
        ELSE IF n_soc<>0 THEN
            qprepare-Ent = qprepare-Ent + " �ndos = " + STRING(n_soc).
        ELSE IF n_dep<>0 THEN
            qprepare-Ent = qprepare-Ent + " �depot = " + STRING(n_dep).

        IF qcondition-Ent<>"" THEN qprepare-Ent=qprepare-Ent + "�" + qcondition-Ent.
        IF VALID-HANDLE (dynreq) THEN /* $1495 */ DO:
            RUN RequeteFrame IN dynreq (OUTPUT tmpch).
            IF tmpch <> "" THEN qprepare-Ent = qprepare-Ent + "�" + tmpch.
        END.

        IF ptypmvt = "H":u THEN qprepare-Ent = qprepare-Ent + "�no_bl_a begins 'affret'":u . /*$1728*/

        qprepare-Ent=LEFT-TRIM(qprepare-Ent,"�").
        qprepare-Ent=REPLACE(qprepare-Ent,"�"," and ").
        /* $1837 */
        logbid=HQuery-Ent:QUERY-PREPARE("FOR EACH histoent WHERE " + qprepare-Ent + " NO-LOCK " /*�XREF_TRACEQ�*/
             + (IF ptypmvt = 'C' AND parsoc.ges_ate THEN " ,first ateent where ateent.typ_sai = (if histoent.ticket <> 0 then 'R' else 'F') and ateent.cod_cli = histoent.cod_cf and ateent.no_cde = histoent.no_cde and ateent.no_bl = histoent.no_bl no-lock outer-join " ELSE " ")
             + (IF ptypmvt = 'C' AND parsoc.ges_car THEN " ,first carent where carent.typ_mvt = 'C' and carent.no_cde = histoent.no_cde and carent.no_bl = histoent.no_bl no-lock outer-join " ELSE " ")
             + " " + qprepare-cli + qprepare-fou + " " + CHQRYBY-ent). /* $A38917 */ /* $A39559 */

        If not logbid then message Traduction("Probl�me dans la condition !",-2,"") VIEW-AS ALERT-BOX INFO.
        ELSE DO:
          IF CHQRYBY-ent <> "" AND CHK-INIT-WIDX(HQuery-Ent) AND NOT(CHK-SORTBY(HQuery-Ent)) THEN DO:
              MESSAGE CHK-GETVALUE("msgErr") VIEW-AS ALERT-BOX WARNING BUTTONS OK.
              ASSIGN CHQRYBY-ent = "" COLORD-ent = "".
              RUN OpenQuery(Pcle,NO).
              RETURN.
          END.

          HBrowse-Ent:QUERY = HQuery-Ent.

          ASSIGN
              /*$1130*/ hremglo = ? hcarte = ?.

          Pload = loadsettings("HBHIS" + ptypmvt, output hsettingsapi).
          DO z=1 TO NUM-ENTRIES(qliste-champ-ent) ON ERROR UNDO,LEAVE ON STOP UNDO,LEAVE:

              IF SUBSTR (ENTRY(z,qliste-champ-ent ),1,7) = "ATEENT." THEN
                HCol=HBrowse-Ent:ADD-LIKE-COLUMN(ENTRY(z,qliste-champ-ent)).
              ELSE
                  IF SUBSTR (ENTRY(z,qliste-champ-ent ),1,7) = "CARENT." THEN /* $1837 */
                    HCol=HBrowse-Ent:ADD-LIKE-COLUMN(ENTRY(z,qliste-champ-ent)).
                  ELSE
                    HCol=HBrowse-Ent:ADD-LIKE-COLUMN("histoent." + ENTRY(z,qliste-champ-ent)).

              ASSIGN
                  HCol:LABEL=ENTRY(1,ENTRY(z,qliste-label-ent,"|"),"�")
                  Hcol:NAME = ENTRY(z,qliste-champ-ent)
                  HCol:LABEL-FGCOLOR=?
                  HCol:LABEL-BGCOLOR=?.
              IF hcol:DATA-TYPE = "logical" AND hcol:BUFFER-FIELD:FORMAT = "{&UTrema}/" THEN hcol:COLUMN-FONT = 47.
              IF hcol:NAME=COLORD-ent THEN HCol:LABEL-FGCOLOR=IF COLDESC-ent THEN 4 ELSE 2.

              CASE hcol:NAME:
                  WHEN "rem_glo[1]" THEN hremglo = hCol:HANDLE.
                  WHEN "no_carte" THEN hcarte = hCol:HANDLE.

                  WHEN "no_fa_a[1]" THEN hcoldisp-ent[1] = hCol:HANDLE.
                  WHEN "no_cde"     THEN hcoldisp-ent[2] = hCol:HANDLE.
                  WHEN "mt_fact"    THEN hcoldisp-ent[3] = hCol:HANDLE.
                  WHEN "no_bl_a"    THEN hcoldisp-ent[4] = hCol:HANDLE.
                  WHEN 'mt_fact'    THEN hcoldisp-ent[5] = hcol:HANDLE.
                  WHEN 'mt_fach'    THEN hcoldisp-ent[6] = hcol:HANDLE.
                  WHEN 'mt_cde'     THEN hcoldisp-ent[7] = hcol:HANDLE.
                  WHEN 'mt_ht_dev'  THEN hcoldisp-ent[8] = hcol:HANDLE.
                  WHEN 'dat_liv'    THEN hcoldisp-ent[9] = hcol:HANDLE.
                  WHEN 'no_fact'    THEN hcoldisp-ent[10] = hcol:HANDLE. /*A55569*/
                  WHEN 'ref_cde'    THEN hcoldisp-ent[11] = hcol:HANDLE.
              END CASE.

              IF Pload THEN
                  qchar-ent = GetSetting(ENTRY(z,qliste-champ-ent) ,?).
              IF qchar-ent <> ? THEN HCol:WIDTH=DEC(qchar-ent).
              IF INDEX(ENTRY(z,qliste-label-ent,"|"),"�t")<>0 THEN hcol:PRIVATE-DATA="tot".
          END.
          IF Pload THEN UnloadSettings(hSettingsApi).

          SESSION:SET-WAIT-STATE("").
          IF qcolonne-lock-ent<=NUM-ENTRIES(qliste-champ-ent) THEN HBrowse-Ent:NUM-LOCKED-COLUMNS = qcolonne-lock-ent.
          HQuery-Ent:QUERY-OPEN().
          TRACEQUERY(hquery-ent:HANDLE).

          ASSIGN
              Logbid = CBShowItem(Hcombars-f1, 2, {&Bdoc},YES)
              Logbid = CBShowItem (Hcombars-f1, 2, {&Binfo},YES)
              Logbid = CBShowItem (Hcombars-f1, 2, {&Betiq},/*$1728... YES*/ ptypmvt <> "H":u /*....$1728*/)
              logbid = CBShowItem (Hcombars-f1, 2, {&btrpaffret}, ptypmvt = "H":u ) /*$1728*/ .
          if not avail histoent then do:
              assign
                  HBrowse-Ent:hidden=TRUE
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Bdoc},NO)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Baffac},NO)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Baffcde},NO)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Bcopier},NO)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&bmail}, NO)  /* $A66083 */
                  Logbid = CBShowItem(Hcombars-f1, 2, {&bfax}, NO). /* $A66083 */
              display FTxtAucun.
          end.
          ELSE DO:
              ASSIGN
                  HBrowse-Ent:hidden=NO
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Bdoc},YES)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Bmarge},NOT (ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ OR
                                                                      can-find(droigco where droigco.util=g-user-droit and droigco.fonc="MARGE")))
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Baffac},YES)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Baffcde},/*$1728... YES*/ ptypmvt <> "H":u /*....$1728*/)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Bcopier},/*$1728... YES*/ ptypmvt <> "H":u /*....$1728*/)
                  Logbid = CBShowItem (Hcombars-f1, 2, {&Beditfaci},RType:screen-value="F").
              if RType:screen-value="F" /*$1728...*/ AND ptypmvt <> "H":u /*...$1728*/ then assign
                  Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bediter}, "&" + Traduction("Facture",-2,""))
                  Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bediter}, Traduction("Facture",-2,""))
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bediter}, YES)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bblchiffr}, NO) /*$A62526*/
                  NOMPGM = "EDTHF"
                  Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bediter}, NOMPGM)
                  /*$1479...*/
                  Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bplage}, "&" + Traduction("Plage de Factures",-2,""))
                  Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bplage}, "&" + Traduction("Plage de Factures",-2,""))
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bplage}, YES)
                  NOMPGM3 = "EDTHF"
                  Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bplage}, NOMPGM3)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bmail}, pTypMvt = "C")
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bfax}, CanXpedite AND pTypMvt = "C").
                  /*...$1479*/
              else if RType:screen-value="BL" and ptypmvt="F" then assign
                  Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bediter}, "&" + (IF histoent.mt_cde<0 THEN Traduction("Bon de Retour",-2,"") ELSE Traduction("Bon R�ception",-2,"")))  /*$2122*/
                  Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bediter}, IF histoent.mt_cde<0 THEN Traduction("Bon de Retour",-2,"") ELSE Traduction("Bon R�ception",-2,""))  /*$2122*/
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bediter}, YES)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&breajuster}, NOT pas-droit-reajuster)
                  NOMPGM = IF histoent.mt_cde<0 THEN "EDTHCFO":U ELSE "EDTRF"  /*$2122*/
                  Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bediter}, NOMPGM)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&bdossierPR}, NOT pas-droit-reajuster).
              else if RType:screen-value="BL" /*$1728...*/ AND ptypmvt <> "H":u /*...$1728*/ then assign
                  Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bediter}, "&" + Traduction("Bon Livraison",-2,""))
                  Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bediter}, Traduction("Bon Livraison",-2,""))
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bediter}, YES)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bblchiffr}, YES) /*$A62526*/
                  NOMPGM = "EDTHB"
                  Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bediter}, NOMPGM)
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bmail}, pTypMvt = "C")
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bfax}, CanXpedite AND pTypMvt = "C")
                  /*$1479...*/
                  Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bplage}, Traduction("Bon Interne",-2,""))
                  Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bplage}, Traduction("Bon Interne",-2,""))
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Bplage}, YES)
                  NOMPGM = "EDTHBOI"
                  Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bplage}, NOMPGM3).
              IF ptypmvt="F"
                  AND RType:screen-value<>"F"
                  AND /* $A40219 ... */ MNU_DROIT_NOOPTM (g-user-perso, 769) THEN
                      /*((NOT CAN-FIND(first menuser where menuser.ndos=0 and menuser.util=g-user-perso and menuser.action="S" and menuser.menu="G.R.F":U and menuser.s_menu="" and menuser.no_opt=0 no-lock)
 *                       AND NOT CAN-FIND(first menuser where menuser.ndos=0 and menuser.util=g-user-perso and menuser.action="S" and menuser.menu="G.R.F":U and menuser.s_menu="Analyse":U and menuser.no_opt=0 no-lock)
 *                       AND NOT CAN-FIND(first menuser where menuser.ndos=0 and menuser.util=g-user-perso and menuser.action="S" and menuser.no_opt=769 no-lock))
 *                       OR can-find(first menuser where menuser.ndos=0 and menuser.util=g-user-perso and menuser.action="A" and menuser.no_opt=769 no-lock)) THEN*/
                      /* ... $A40219 */
                  Logbid = CBShowItem(Hcombars-f1, 2, {&Banarec}, YES).

                  /*...$1479*/
              /*HBrowse-Ent:SELECT-ROW(1). $A54459 */
              HBrowse-Ent:SELECT-ROW(1).
              apply "entry" to HBrowse-Ent.
              apply "value-changed" to HBrowse-Ent.
          END.
        END.
    END.

    FIND CURRENT histoent NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
    /*Logbid = CBShowItem (Hcombars-f1, 2, {&Bdoc},CAN-FIND(FIRST infoent WHERE infoent.typ_fich = /*"E"*/ typinfo AND infoent.NO_info = histoent.NO_info NO-LOCK)). /*A20014*/*/ /*$A60546*/

    Logbid = (IF rs-selection:SCREEN-VALUE = "C"  /*AND Vcod_cli <> "*"*/ THEN CBShowItem(Hcombars-f1, 2, {&ctypcli}, YES) ELSE  CBShowItem(Hcombars-f1, 2, {&ctypcli}, NO)). /*$A35573*/

    IF VALID-HANDLE (dynreq) THEN RUN AfficherFrame IN dynreq. /* $1495 */
    IF VALID-HANDLE (dynreq_facfo) THEN RUN AfficherFrame IN dynreq_facfo. /* $1495 */
    IF VALID-HANDLE (dynreq_histolig) THEN RUN AfficherFrame IN dynreq_histolig. /* $1495 */
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openquery2 W1 
PROCEDURE openquery2 :
DEF VAR tmpch AS char no-undo. /* $1495 */
DEF VAR qprepare-aff AS CHAR  NO-UNDO. /*$A66700*/
DO WITH FRAME F1:
    assign
        Logbid = CBEnableItem (Hcombars-f1, 2, {&Bdetail}, NO)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&Bcr}, NO) /*$A62727*/
        logbid = CBEnableItem (hcombars-f1, 2, {&bimm}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&blot}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&bgrille_rem}, NO) /*A56990*/
        logbid = CBEnableItem (hcombars-f1, 2, {&bemp}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&Btn-varctrlf8}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Bdetail}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Bcr}, NO) /*$A62727*/
        logbid = CBShowItem (hcombars-f1, 2, {&bimm}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&blot}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&bgrille_rem}, NO) /*A56990*/
        logbid = CBShowItem (hcombars-f1, 2, {&bemp}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&Btn-varctrlf8}, NO).

    IF qliste-champ-ent2=? THEN ASSIGN
        qliste-champ-ent2 = "dat_fac,dat_ech,no_fact,no_pce,lib_ecr,nom_fou," + (IF NOT pas-droit-vpa THEN "mt_fach,mt_fact," ELSE "") + "devise,tx_ech_d,regime,usr_ctl,dat_ctl,zone_libre[9],zone_libre[10]"
        qliste-label-ent2 = Traduction("Date Fac.",-2,"") + "|" + Traduction("Date Ech.",-2,"") + "|" + Traduction("N� Facture",-2,"") + "|" +
                            Traduction("N� Pi�ce",-2,"") + "|" + Traduction("Libell� �criture",-2,"") + "|" + Traduction("Fournisseur",-2,"") + "|" +
                            (IF NOT pas-droit-vpa THEN Traduction("Facture H.T",-2,"") + "|" + Traduction("Facture T.T.C",-2,"") + "|" ELSE "") +
                            Traduction("Devise",-2,"") + "|" + Traduction("Taux change",-2,"") + "|" +
                            Traduction("T.V.A",-2,"") + "|" + Traduction("Qui a contr�l�",-2,"") + "|" + Traduction("Date contr�le",-2,"") + "|" +
                            Traduction("Modif Mt fourn.",-2,"") + "|" + Traduction("Modif Mt vent.",-2,"")
        qcondition-ent2 = ""
        qcolonne-lock-ent2 = 0.

    IF COLORD-ent2=? OR INDEX(qliste-champ-ent2,COLORD-ent2)=0 THEN ASSIGN
        COLORD-ent2="dat_fac"
        CHQRYBY-ent2="BY " + COLORD-ent2 + " DESC"
        COLDESC-ent2=YES.

    IF CHQRYBY-ent2=? THEN CHQRYBY-ent2="".
    IF qcondition-ent2=? THEN qcondition-ent2="".
    IF qcondition-ent2<>"" THEN CBSetItemIcon(hComBars-f1, 2, {&Bajouter-colonnes-ent}, {&Bajouter-colonnes-ent2}).
    ELSE CBSetItemIcon(hComBars-f1, 2, {&Bajouter-colonnes-ent}, {&Bajouter-colonnes-ent}).

    HQuery-Ent:SET-BUFFERS(BUFFER Entfacfo:HANDLE).

    qprepare-Ent = "".
    /*$A66700...*/
    IF rs-selection:SCREEN-VALUE = "A" AND vcod_cli:HIDDEN = NO AND vcod_cli:SCREEN-VAL <> "*" THEN DO :
        HQuery-Ent:ADD-BUFFER(BUFFER ligfacfo:HANDLE).
        qprepare-aff = ", FIRST ligfacfo WHERE ligfacfo.ndos = entfacfo.ndos AND ligfacfo.cod_fou = entfacfo.cod_fou AND ligfacfo.no_fact = entfacfo.no_fact AND ligfacfo.dat_fac = entfacfo.dat_fac AND ligfacfo.affaire = '" + vcod_cli:SCREEN-VAL + "' NO-LOCK".
    END.
    /*...$A66700*/
    IF rs-selection:SCREEN-VALUE = "F" AND /*$A66700*/  vcod_cli:HIDDEN = NO AND vcod_cli:SCREEN-VAL <> "*" THEN qprepare-Ent=qprepare-Ent + " �cod_fou=" + STRING(Vcod_cli:SCREEN-VAL).
    IF fdatedeb:HIDDEN = NO THEN qprepare-Ent=qprepare-Ent + " �dat_fac >= " + STRING(fdatedeb) + " �dat_fac <= " + STRING(fdatefin).

    /*$debug IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" THEN
        Qprepare-ent = Qprepare-ent + " �CAN-DO('":U + ListeDepots + "',STRING(depot)) ":U.*/

    /*$A76832... Il n'y a pas de champ entfacfo.depot,
    j'ai donc d�duit les soci�t�s autoris�es � partir des d�p�ts autoris�s pour utiliser le champ entfacfo.ndos*/
    IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" THEN
        Qprepare-ent = Qprepare-ent + " �CAN-DO('":U + liste-soc + "',STRING(ndos)) ":U.
    /*...$A76832*/
    IF n_soc<>0 THEN
        qprepare-Ent = qprepare-Ent + " �ndos = " + STRING(n_soc).
    ELSE IF n_dep <> 0 THEN FOR FIRST tabgco where tabgco.type_tab = "DS" and tabgco.n_tab = n_dep NO-LOCK:
        IF tabgco.ndos > 0 THEN qprepare-Ent = qprepare-Ent + " �ndos = " + STRING(tabgco.ndos).
    END.

    IF qcondition-ent2<>"" THEN qprepare-Ent=qprepare-Ent + "�" + qcondition-ent2.
    IF VALID-HANDLE (dynreq_facfo) THEN /* $1495 */ DO:
        RUN RequeteFrame IN dynreq_facfo (OUTPUT tmpch).
        IF tmpch <> "" THEN qprepare-Ent = qprepare-Ent + "�" + tmpch.
    END.
    qprepare-Ent=LEFT-TRIM(qprepare-Ent," �").
    qprepare-Ent=REPLACE(qprepare-Ent,"�"," and ").

    /* $A39559... */
    IF clause-fou <> "" THEN DO:
        HQuery-Ent:ADD-BUFFER(BUFFER fournis:HANDLE).
        qprepare-fou = ", FIRST fournis WHERE fournis.cod_fou = entfacfo.cod_fou AND " + clause-fou.
    END.
    ELSE qprepare-fou = "".
    /* ...$A39559 */

    logbid=HQuery-Ent:QUERY-PREPARE("FOR EACH Entfacfo WHERE " + qprepare-Ent + " NO-LOCK" + qprepare-aff /*$A66700*/ + qprepare-fou + " " + CHQRYBY-ent2) NO-ERROR. /* $A39559 */

    If not logbid then message Traduction("Probl�me dans la condition !",-2,"") VIEW-AS ALERT-BOX INFO.
    ELSE DO:
        IF CHQRYBY-ent <> "" AND CHK-INIT-WIDX(HQuery-Ent) AND NOT(CHK-SORTBY(HQuery-Ent)) THEN DO:
            MESSAGE CHK-GETVALUE("msgErr") VIEW-AS ALERT-BOX WARNING BUTTONS OK.
            ASSIGN CHQRYBY-ent = "" COLORD-ent = "".
            RUN OpenQuery2.
            RETURN.
        END.

        HBrowse-Ent:QUERY = HQuery-Ent.

        Pload = loadsettings("HBHIS" + ptypmvt + "3", output hsettingsapi).
        DO z=1 TO NUM-ENTRIES(qliste-champ-ent2) ON ERROR UNDO,LEAVE ON STOP UNDO,LEAVE:
            HCol=HBrowse-Ent:ADD-LIKE-COLUMN("Entfacfo." + ENTRY(z,qliste-champ-ent2)).

            ASSIGN
                HCol:LABEL=ENTRY(1,ENTRY(z,qliste-label-ent2,"|"),"�")
                Hcol:NAME = ENTRY(z,qliste-champ-ent2)
                HCol:LABEL-FGCOLOR=?
                HCol:LABEL-BGCOLOR=?.
            IF hcol:DATA-TYPE = "logical" AND hcol:BUFFER-FIELD:FORMAT = "{&UTrema}/" THEN hcol:COLUMN-FONT = 47.
            IF hcol:NAME=COLORD-ent2 THEN HCol:LABEL-FGCOLOR=IF COLDESC-ent2 THEN 4 ELSE 2.
            IF Pload THEN
                qchar-ent2 = GetSetting(ENTRY(z,qliste-champ-ent2) ,?).
            IF qchar-ent2 <> ? THEN HCol:WIDTH=DEC(qchar-ent2).

            CASE hCol:NAME:
                WHEN "no_fact" THEN hcoldisp-ent[1] = hCol:HANDLE.
                WHEN 'mt_fact' THEN hcoldisp-ent[5] = hcol:HANDLE.
                WHEN 'mt_fach' THEN hcoldisp-ent[6] = hcol:HANDLE.
            END CASE.
        END.
        IF Pload THEN UnloadSettings(hSettingsApi).

        SESSION:SET-WAIT-STATE("").
        IF qcolonne-lock-ent2<=NUM-ENTRIES(qliste-champ-ent2) THEN HBrowse-Ent:NUM-LOCKED-COLUMNS = qcolonne-lock-ent2.
        HQuery-Ent:QUERY-OPEN().
        TRACEQUERY(hquery-ent:HANDLE).
        ASSIGN Logbid = CBShowItem (Hcombars-f1, 2, {&Bcopier},NO)
               Logbid = CBShowItem (Hcombars-f1, 2, {&Binfo},NO)
               Logbid = CBShowItem (Hcombars-f1, 2, {&Bmarge},NO)
               Logbid = CBShowItem (Hcombars-f1, 2, {&Bdoc},NO)
               Logbid = CBShowItem (Hcombars-f1, 2, {&Betiq},NO).

        if not avail Entfacfo then do:
            assign HBrowse-Ent:hidden=TRUE
                   Logbid = CBShowItem (Hcombars-f1, 2, {&Baffac},NO)
                   Logbid = CBShowItem (Hcombars-f1, 2, {&Baffcde},NO).
            display FTxtAucun.
        end.
        ELSE DO:
            ASSIGN HBrowse-Ent:hidden=NO
                   Logbid = CBShowItem (Hcombars-f1, 2, {&Baffac},YES)
                   Logbid = CBShowItem (Hcombars-f1, 2, {&Baffcde},YES).

            HBrowse-Ent:SELECT-ROW(1).
            apply "entry" to HBrowse-Ent.
            apply "value-changed" to HBrowse-Ent.
        END.
    END.
    IF VALID-HANDLE (dynreq) THEN RUN AfficherFrame IN dynreq. /* $1495 */
    IF VALID-HANDLE (dynreq_facfo) THEN RUN AfficherFrame IN dynreq_facfo. /* $1495 */
    IF VALID-HANDLE (dynreq_histolig) THEN RUN AfficherFrame IN dynreq_histolig. /* $1495 */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OuvrirBrowse W1 
PROCEDURE OuvrirBrowse :
IF Vcod_cli:SCREEN-VAL IN frame f1 <> "" THEN
    RUN openquery(svcle,NO).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-doc-ged W1 
PROCEDURE proc-doc-ged :
DEF INPUT PARAM p%typ_data AS CHAR NO-UNDO.
    DEF INPUT PARAM p%proc-traiter AS CHAR NO-UNDO.

    vCriteresGed-def = "".
    vValCriteres-def = "".
    pascritere = NO.
    FIND FIRST gedlien WHERE gedlien.ndos = n_d AND gedlien.typ_data = p%typ_data NO-LOCK NO-ERROR.
    IF AVAIL gedlien THEN DO :
        EMPTY TEMP-TABLE tt-req NO-ERROR.

        FOR EACH gedjoint WHERE gedjoint.ndos = n_d AND gedjoint.typ_data = p%typ_data  NO-LOCK :
            IF gedjoint.zon_lib [1] = 'yes' THEN NEXT.
            vCriteresGed-def = vCriteresGed-def + (IF vCriteresGed-def = "" THEN "" ELSE "�") + gedjoint.critere_ged.
            vCriteresGed-def = TRIM(vCriteresGed-def, "�").
            vValCriteres-def = vValCriteres-def + (IF vValCriteres-def = "" THEN "" ELSE "�") + rec-zone(gedjoint.tbl_data,gedjoint.cle_data).
            vValCriteres-def = TRIM(vValCriteres-def, "�").
            IF rec-zone(gedjoint.tbl_data,gedjoint.cle_data) = "" THEN pascritere = YES.
        END.

        FOR EACH gedjoint WHERE gedjoint.ndos = n_d AND gedjoint.typ_data = p%typ_data AND gedjoint.zon_lib [1] = 'yes' NO-LOCK:
            CREATE tt-req.
            ASSIGN tt-req.vCriteresGed = vCriteresGed-def + (IF vCriteresGed-def = "" THEN "" ELSE "�") + gedjoint.critere_ged
                   tt-req.vValCriteres = vValCriteres-def + (IF vValCriteres-def = "" THEN "" ELSE "�") + rec-zone(gedjoint.tbl_data,gedjoint.cle_data).
        END.

        IF NOT CAN-FIND (FIRST tt-req NO-LOCK) AND vCriteresGed-def <> "" AND vValCriteres-def <> "" THEN DO:
            CREATE tt-req.
            ASSIGN tt-req.vCriteresGed = vCriteresGed-def
                   tt-req.vValCriteres = vValCriteres-def.
        END.

        IF NOT pascritere THEN
            FOR EACH tt-req NO-LOCK:
                RUN recherche_x_documents_par_critere IN g-hged (gedlien.class_ged , tt-req.vCriteresGed, tt-req.vValCriteres, p%proc-traiter, THIS-PROCEDURE).
            END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init W1 
PROCEDURE proc-init :
RUN aTrad$VariableInitial. /* $tradVar */

/*A55569*/
run rechunit (g-plateforme,"POCWIN",output vchar).
gestion-pocwin=(vchar<>?).

DEFINE VARIABLE temp AS CHARACTER  NO-UNDO.
RUN chkqDyn PERSISTENT SET hndChkQDyn.

ges_WMS = CONNECTED("WMS":U). /*$2672*/
IF ges_WMS THEN RUN VALUE (MOB_CHEMINPGM("api-mobsr":U)) PERSISTENT SET hapi-mobsr (?,?,?,?,OUTPUT phgcomobsr).

DO WITH FRAME F1:

    ent$:RADIO-BUTTONS = Traduction("En-t�tes",-2,"") + ",E,":U + Traduction("En-t�tes",-2,"") + " && ":U + Traduction("Lignes",-2,"") + ",T":U.

    IF AVAIL parsoc AND parsoc.ges_car THEN /*$1759*/
        FIND FIRST carpar NO-LOCK NO-ERROR.  /* �XREF_NOWHERE� */
    vcod_cli:LABEL IN FRAME F1 = Traduction("Fournisseur",-2,"") + " (*=" + Traduction("Tous",-2,"") + ")".

    ASSIGN
        g-user-droit = (IF ADesDroits("Droit", g-user) THEN g-user ELSE GetGroupeUser("Droit", g-user))
        g-user-perso = (IF ADesDroits("Personnalisation":U, g-user) THEN g-user ELSE GetGroupeUser("Personnalisation":U, g-user))
        pas-droit-reajuster = can-find(droigco where droigco.util=g-user-droit and droigco.fonc="REAJBL")
        pas-droit-validerSR = can-find(droigco where droigco.util=g-user-droit and droigco.fonc="MOBVALIDSR":U) /*$2672*/
        pas-droit-vpr=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPR":U)
        pas-droit-vpa=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPA":U)
        pas-droit-vpm=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VPM":U)
        pas-droit-vtrsp=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="VTRSP":U) /*$A35969*/
        droit-modif = CAN-FIND(droigco WHERE droigco.util = g-user-droit AND droigco.fonc = "MODFACA") /*$1258*/
        Logbid = BR1:LOAD-IMAGE(StdMedia + "\combo.bmp").

    buf-couleur[1] = INT (AFF_OBTENIR_PARAMETRAGE ("COULEUR-PHASE-1":U)).
    buf-couleur[2] = INT (AFF_OBTENIR_PARAMETRAGE ("COULEUR-PHASE-2":U)).
    buf-couleur[3] = INT (AFF_OBTENIR_PARAMETRAGE ("COULEUR-PHASE-3":U)).
    buf-couleur[4] = INT (AFF_OBTENIR_PARAMETRAGE ("COULEUR-PHASE-4":U)).
    buf-couleur[5] = INT (AFF_OBTENIR_PARAMETRAGE ("COULEUR-PHASE-5":U)).

    /* $A38917... */
    IF ptypmvt <> "F" AND ptypmvt <> "H" THEN DO:
        RUN api-piece-cl PERSISTENT SET hapi-piece-cl (?).
        IF CAN-DO(hapi-piece-cl:INTERNAL-ENTRIES,"PCL_DROIT_CLIENT") THEN DO:  /* cette ligne pourra �tre enlev�e en 4.4 */
            clause-cli = PCL_DROIT_CLIENT().
        END.
        /* ...$A38917 */
    END.
        /* $A39559... */
    ELSE DO:
        RUN api-piece-fo PERSISTENT SET hapi-piece-fo (?,0).
        IF CAN-DO(hapi-piece-fo:INTERNAL-ENTRIES,"PFO_DROIT_FOURNISSEUR") THEN DO:  /* cette ligne pourra �tre enlev�e en 4.4 */
            clause-fou = PFO_DROIT_FOURNISSEUR().
        END.
    END.
        /* ...$A39559 */

    FIND FIRST parcde WHERE parcde.typ_fich="C" NO-LOCK NO-ERROR.
    n_d=(IF parsoc.mult_soc THEN g-ndos ELSE 0).

    FormatColBrowse (INPUT BUFFER histoent:HANDLE,"","").
    IF Ptypmvt = "F" THEN FormatColBrowse (INPUT BUFFER entfacfo:HANDLE,"","").
    ELSE
    do:
        IF parsoc.ges_ate THEN FormatColBrowse (INPUT BUFFER ateent:HANDLE,"","").
        IF parsoc.ges_car THEN FormatColBrowse (INPUT BUFFER carent:HANDLE,"",""). /* $1837 */
    END.

    {droit_cli2.i}
    {droit_fou.i}

    RUN control_load.
    ASSIGN TreeSui = chCtrlframe:TreeView
           Imgtree = chCtrlFrame-2:ImageList
           TreeSui:ImageList = ImgTree
           FDateDeb  = today - (IF parcde.gst_num[4]=0 THEN 365 ELSE parcde.gst_num[4])
           FDateFin  = TODAY.
    /*$1728...*/
    IF ptypmvt = "H":u THEN DO:
        {&WINDOW-NAME}:title = Traduction("Historique Commandes/B.L/Factures",-2,"").
        ASSIGN Vcod_cli:LABEL = Traduction("Transporteur",-2,"") + " (*=":u + Traduction("Tous",-2,"") + ")"
               RType:radio-buttons=Traduction("Commandes",-2,"") + "," + "C" + "," + Traduction("Bons de Livraison",-2,"") + "," + "BL" .

    END.
    ELSE /*...$1728*/
    if pTypMvt = "F" then do:
        {&WINDOW-NAME}:title = Traduction("Historique Commandes/B.L/Factures",-2,"").
        ASSIGN Vcod_cli:LABEL = Traduction("Fournisseur",-2,"") + " (*=" + Traduction("Tous",-2,"") + ")"
               RType:radio-buttons=Traduction("Commandes",-2,"") + "," + "C" + "," + Traduction("Bons de Livraison",-2,"") + "," + "BL" + "," + Traduction("Factures",-2,"") + "," + "F".
    end.
    else /*$A35573...*/
        RUN Choose-ctypcli(OUTPUT typclient).
    ASSIGN /*Vcod_cli:LABEL = Traduction("Client",-2,"") + " (*=" + Traduction("Tous",-2,"") + ")" $A35573*/
        Ffrais:HIDDEN = YES
        freel:hidden=YES.

    rs-selection:RADIO-BUTTONS = (/*$1728...*/ IF ptypmvt = "H":u THEN Traduction("Transporteur",-2,"") + "," + "F":u ELSE /*$1728*/ if pTypMvt = "F" THEN Traduction("Fournisseur",-1,"") + "," + "F" ELSE Traduction("Client",-1,"") + "," + "C") + "" + "," + Traduction("Affaire",-1,"") + "," + "A".

    RUN load-ini.
    ASSIGN vex=(if vex=? then "no" else "yes").
    DISP Fdatedeb Fdatefin Ft$selection-1 WITH FRAME F1.

    ENABLE
        Rtype rs-selection Vcod_cli br1 csoc ent$ Fdatedeb Fdatefin Tbapartir Fnum Falpha Butt-spli Tbarbo WITH FRAME F1.

    rs-selection:HIDDEN = (parsoc.affaire = NO).

    IF parsoc.bois_ar THEN DO:
        IF LoadSettings(nompgm, OUTPUT hSettingsApi) THEN DO :
            rs$global:SCREEN-VALUE IN FRAME fedtfac = Getsetting("Global","NO").
            UnloadSettings(hSettingsApi).
        END.
    END.
    ELSE HIDE rs$global.

    /* Soci�t�s, D�p�ts */
    {soc.i "T" ''}

    /*$A76832... D�tection des soci�t�s autoris�es en fonction des d�p�ts*/
    DO ii = 1 TO NUM-ENTRIES(ListeDepots):
        FIND tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab = INT(ENTRY(ii,ListeDepots)) NO-LOCK NO-ERROR. /*$A92439*/
        IF AVAIL tabgco AND NOT CAN-DO(liste-soc, STRING(tabgco.ndos)) THEN liste-soc = liste-soc + STRING(tabgco.ndos) + ",".
    END.
    /*...$A76832*/

    ASSIGN
        n_dep=(IF SUBSTR(csoc:SCREEN-VAL,1,1) = "D" AND INT(SUBSTR(csoc:SCREEN-VAL,2)) > 0 THEN INT(SUBSTR(csoc:SCREEN-VAL,2)) ELSE 0)
        n_soc=(IF SUBSTR(csoc:SCREEN-VAL,1,1) = "S" AND INT(SUBSTR(csoc:SCREEN-VAL,2)) > 0 THEN INT(SUBSTR(csoc:SCREEN-VAL,2)) ELSE 0).
    IF NOT parsoc.mult_soc THEN ASSIGN
        csoc:BGC=48
        csoc:TOOLTIP=Traduction("D�p�t",-2,"") + " " + "(CTRL-F4)" + "".

    ASSIGN
        Logbid = CBShowItem (Hcombars-f1, 2, {&Bcopier},NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Baffac},NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Baffcde},NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Bediter}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Banarec}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Beditfaci}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Bplage}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Breajuster}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&BdossierPR}, NO)
        Logbid = CBShowItem(Hcombars-f1, 2, {&Blitige}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Bdetail}, NO)
        Logbid = CBShowItem (Hcombars-f1, 2, {&Bcr}, NO) /*$A62727*/
        logbid = CBShowItem (hcombars-f1, 2, {&bimm}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&blot}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&bgrille_rem}, NO) /*A56990*/
        logbid = CBShowItem (hcombars-f1, 2, {&bemp}, NO)
        logbid = CBShowItem (hcombars-f1, 2, {&Btn-varctrlf8}, NO).

    ASSIGN fnum:HIDDEN  = YES
           Falpha:HIDDEN = YES
           LogBid = butt-spli:load-mouse-pointer("SIZE-w":U)
           svtree = ctrlframe:WIDTH-P
           svbut  = butt-spli:X
           Ctrlframe:HIDDEN = (NOT tbarbo:CHECKED) Butt-spli:HIDDEN = (NOT tbarbo:CHECKED)
           Tbapartir:VISIBLE = NOT tbarbo:CHECKED AND (Ptypmvt<>"F" OR rtype:SCREEN-VAL <> "F").

    IF Vcod_cli:SCREEN-VAL <> "" THEN do:
        IF Vcod_cli:SCREEN-VAL = "*" THEN Flib1:SCREEN-VAL = Traduction("Tous",-2,"").
        ELSE DO:
            if rs-selection:SCREEN-VAL = "F" THEN DO:
                FIND FIRST fournis WHERE fournis.cod_fou = INT(vcod_cli:SCREEN-VAL) no-lock no-error.
                IF AVAIL fournis THEN Flib1:SCREEN-VAL = fournis.nom_fou.
                                 ELSE Flib1:SCREEN-VAL = Traduction("Inexistant",-2,"").
            END.
            ELSE if rs-selection:SCREEN-VAL = "C" THEN DO:
                FIND FIRST client WHERE client.cod_cli = INT(vcod_cli:SCREEN-VAL) no-lock no-error.
                IF AVAIL client THEN Flib1:SCREEN-VAL = client.nom_cli.
                                ELSE Flib1:SCREEN-VAL = Traduction("Inexistant",-2,"").
                Logbid = CBShowItem (Hcombars-f1, 2, {&ctypcli}, YES). /*$A35573*/
            END.
            ELSE DO:
                FIND FIRST affaire WHERE affaire.affaire = vcod_cli:SCREEN-VAL NO-LOCK NO-ERROR.
                IF AVAIL affaire THEN Flib1:SCREEN-VAL = affaire.int_aff.
                                 ELSE Flib1:SCREEN-VAL = Traduction("Inexistant",-2,"").
            END.
        END.
    END.

    if RType:screen-value = "F" then  Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-bonCommission}, YES). /* $BOIS TEN */
    ELSE Logbid = CBShowItem (Hcombars-f1, 2, {&bediter-bonCommission}, NO). /* $BOIS TEN */

    if RType:screen-value = "F" then DO:
        IF Ptypmvt="F" THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
        tbapartir:label=Traduction("A partir du n� facture",-2,"").
    END.
    else if RType:screen-value = "C" then tbapartir:label=Traduction("A partir du n� commande",-2,"").
    else if RType:screen-value = "BL" THEN DO:
        tbapartir:label=Traduction("A partir du n� B.L",-2,"").
        IF Ptypmvt="F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/ THEN ASSIGN tbarbo:CHECKED = NO tbarbo:HIDDEN = YES.
    END.
    IF Ptypmvt = "F" /*$1728..*/ OR ptypmvt = "H":u /*...$1728*/
    THEN ASSIGN Logbid = CBShowItem (Hcombars-f1, 2, {&Bentete},NO)
                Logbid = CBShowItem (Hcombars-f1, 2, {&Bpied},NO).
    logbid = CBShowItem (Hcombars-f1, 2, {&btrpaffret}, ptypmvt = "H" ) /*$1728*/ .
    /* $1359 */
    temp = getcleval("PROGIWIN", "BD", "", "mntfop_b", "activer").
    IF temp = "" OR temp = ? THEN temp = "NO".
    ASSIGN
        radio-editpdf:HIDDEN IN FRAME Fplafac = NOT(LOGICAL(temp))
        radio-editpdf:SENSITIVE = LOGICAL(temp).

    typinfo = (IF ptypmvt = "F" THEN "A" ELSE "E").

    IF rs-selection:SCREEN-VALUE = "C" /*AND Vcod_Cli:SCREEN-VALUE <> "*"*/ THEN CBComboSetSelEx  (hCombars-f1, 2, {&ctypcli}, 1). /*$A35573*/

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini W1 
PROCEDURE putini :
DO WITH FRAME fedtfac:
        IF parsoc.bois_ar AND LoadSettings(nompgm, OUTPUT hSettingsApi) THEN DO :
            SaveSetting("Global",rs$global:SCREEN-VALUE).
            UnloadSettings(hSettingsApi).
        END.
    END.
END procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE remp-entete W1 
PROCEDURE remp-entete :
DO WITH FRAME F1:
    DEF VAR codmaitre  AS C no-undo.
    DEF VAR codmaitre2 AS C no-undo.
    DEF VAR sav        AS I no-undo.
    DEF VAR CPT        AS I no-undo.

    ASSIGN ctrlframe:HIDDEN = YES
           Vcod_cli Fdatedeb Fdatefin Rtype.
    IF VALID-HANDLE(HBrowse-Ent) THEN DO:
        DELETE OBJECT HQuery-Ent NO-ERROR.
        DELETE OBJECT HBrowse-Ent NO-ERROR.
    END.
    session:set-wait ("general").
    TreeSui:Nodes:CLEAR().
    codmaitre = "R0".
    IF ptypmvt = "C" THEN TreeSui:Nodes:ADD(, 4, codmaitre, IF rtype="F" THEN Traduction("Toutes les Factures",-2,"") ELSE IF rtype="C" THEN Traduction("Toutes les Commandes",-2,"") ELSE Traduction("Tous les B.L",-2,""),IF rtype="F" THEN 1 ELSE IF rtype="C" THEN 2 ELSE 3,IF rtype="F" THEN 1 ELSE IF rtype="C" THEN 2 ELSE 3).
                     ELSE TreeSui:Nodes:ADD(, 4, codmaitre, IF rtype="C" THEN Traduction("Toutes les Commandes",-2,"") ELSE Traduction("Tous les B.L",-2,""),IF rtype="C" THEN 4 ELSE 5,IF rtype="C" THEN 4 ELSE 5).
    if RType = "F" then
      for each histoent fields (ndos dat_liv typ_mvt cod_cf no_fact dat_fac adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a depot)
         where histoent.typ_mvt = pTypMvt
            and histoent.cod_cf = int(vcod_cli)
            and histoent.dat_fac <= FDateFin
            and histoent.dat_fac >= fdatedeb NO-LOCK BY histoent.NO_fact DESC:
          if ptypmvt="C" AND histoent.nofaca[1]<>0 and histoent.chif_bl=no then next.

          IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" AND NOT CAN-DO(ListeDepots,STRING(histoent.depot)) THEN NEXT.
          ELSE IF n_soc<>0 AND CAN-DO(LstSoc,STRING(n_soc)) AND (histoent.ndos<>n_soc OR NOT CAN-DO(ListeDepots,STRING(histoent.depot))) THEN NEXT.
          ELSE IF n_soc<>0 AND histoent.ndos<>n_soc THEN NEXT.
          ELSE IF n_dep<>0 AND histoent.depot<>n_dep THEN NEXT.

          IF histoent.no_fact <> sav THEN DO:
              codmaitre2 = "F" + STRING(histoent.NO_fact,"999999999") + STRING(rowid(histoent),"X(10)").
              TreeSui:Nodes:ADD(codmaitre, 4, codmaitre2, Traduction("Fact.",-2,"") + " " + Traduction("N�",-2,"") + " " + string(histoent.NO_fact),1,1).
              /* Recherche les BL de la Facture */
              for each histoent2 fields (ndos dat_liv typ_mvt cod_cf no_fact dat_fac adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a)
                 where histoent2.typ_mvt = pTypMvt
                  and histoent2.ndos=histoent.ndos
                  and histoent2.no_fact = histoent.no_fact NO-LOCK:
                  cpt = cpt + 1.
                  TreeSui:Nodes:ADD(codmaitre2, 4, codmaitre2 + "BL" + STRING(histoent2.NO_bl,"999999999") + STRING(cpt), "BL" + " " + Traduction("N�",-2,"") + " " + string(histoent2.NO_bl) + " - " + Traduction("N� Cde",-2,"") + " " + STRING(histoent2.NO_cde),3,3).
              END.
              Cpt = 0.
          END.
          sav = histoent.no_fact.
    end.
    else if RType = "C" then
        for each histoent fields (ndos dat_liv typ_mvt cod_cf no_cde no_fact no_fa_a[1] dat_cde adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde dev_frais nom_prof dat_px frais dos_pxr rem_glo no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a depot)
          where histoent.typ_mvt = pTypMvt
            and histoent.cod_cf = int(vcod_cli)
            and histoent.dat_cde <= FDateFin
            and histoent.dat_cde >= fdatedeb NO-LOCK BY histoent.NO_cde DESC:
            if ptypmvt="C" AND histoent.nofaca[1]<>0 and histoent.chif_bl=no then next.

            IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" AND NOT CAN-DO(ListeDepots,STRING(histoent.depot)) THEN NEXT.
            ELSE IF n_soc<>0 AND CAN-DO(LstSoc,STRING(n_soc)) AND (histoent.ndos<>n_soc OR NOT CAN-DO(ListeDepots,STRING(histoent.depot))) THEN NEXT.
            ELSE IF n_soc<>0 AND histoent.ndos<>n_soc THEN NEXT.
            ELSE IF n_dep<>0 AND histoent.depot<>n_dep THEN NEXT.

            IF histoent.no_cde <> sav THEN Do:
                codmaitre2 = "C" + STRING(histoent.NO_cde,"999999999") + STRING(rowid(histoent),"X(10)").
                TreeSui:Nodes:ADD(codmaitre, 4, codmaitre2, Traduction("Cde",-2,"") + " " + Traduction("N�",-2,"") + " " + string(histoent.NO_cde),IF ptypmvt = "C" THEN 2 ELSE 4,IF ptypmvt = "C" THEN 2 ELSE 4).
                /* Recherche les BL de la Facture */
                for each histoent2 fields (dat_liv typ_mvt cod_cf no_fact dat_fac adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a)
                   where histoent2.typ_mvt = pTypMvt and histoent2.cod_cf  = int(vcod_cli) AND histoent2.no_cde = histoent.no_cde NO-LOCK:
                    cpt = cpt + 1.
                    IF ptypmvt = "C" THEN TreeSui:Nodes:ADD(codmaitre2, 4, codmaitre2 + "BL" + STRING(histoent2.NO_bl,"999999999") + STRING(Cpt), "BL" + " " + Traduction("N�",-2,"") + " " + string(histoent2.NO_bl) + " - " + Traduction("N� Fact.",-2,"") + " " + STRING(histoent2.NO_fact),3,3).
                    ELSE TreeSui:Nodes:ADD(codmaitre2, 4, codmaitre2 + "BL" + STRING(histoent2.NO_bl_a,"X(20)") + STRING(Cpt), "BL" + " " + Traduction("N�",-2,"") + " " + string(histoent2.NO_bl_a),5,5).
                END.
                Cpt = 0.
            END.
            sav = histoent.no_cde.
    end.
    ELSE if RType = "BL" AND ptypmvt="C" then
        for each histoent fields (ndos dat_liv typ_mvt cod_cf no_cde no_fact no_fa_a[1] dat_cde adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde dev_frais dos_pxr rem_glo nom_prof dat_px frais no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a depot)
          where histoent.typ_mvt = pTypMvt
            and histoent.cod_cf = int(vcod_cli)
            and histoent.dat_liv <= FDateFin
            and histoent.dat_liv >= fdatedeb NO-LOCK BY histoent.NO_bl DESC:
            if histoent.nofaca[1]<>0 and histoent.chif_bl=no then next.

            IF n_soc=0 AND n_dep=0 AND ListeDepots<>"" AND NOT CAN-DO(ListeDepots,STRING(histoent.depot)) THEN NEXT.
            ELSE IF n_soc<>0 AND CAN-DO(LstSoc,STRING(n_soc)) AND (histoent.ndos<>n_soc OR NOT CAN-DO(ListeDepots,STRING(histoent.depot))) THEN NEXT.
            ELSE IF n_soc<>0 AND histoent.ndos<>n_soc THEN NEXT.
            ELSE IF n_dep<>0 AND histoent.depot<>n_dep THEN NEXT.

            IF histoent.no_bl <> sav THEN Do:
                codmaitre2 = "BL" + STRING(histoent.NO_bl,"999999999") + STRING(rowid(histoent),"X(10)").
                TreeSui:Nodes:ADD(codmaitre, 4, codmaitre2, "BL" + " " + Traduction("N�",-2,"") + " " + string(histoent.NO_bl),IF ptypmvt = "C" THEN 2 ELSE 4,IF ptypmvt = "C" THEN 2 ELSE 4).
                /* Recherche les Cdes de la Facture */
                for each histoent2 fields (dat_liv typ_mvt cod_cf no_fact dat_fac adr_liv[1] adr_fac[1] cl_fact cl_paye ref_cde not_ref mt_cde no_cde no_bl mt_fact mt_fach /*$618*/ nofaca[1] chif_bl NO_bl_a)
                   where histoent2.typ_mvt = pTypMvt  and histoent2.cod_cf  = int(vcod_cli) AND histoent2.no_bl  = histoent.no_bl NO-LOCK:
                    cpt = cpt + 1.
                    TreeSui:Nodes:ADD(codmaitre2, 4, codmaitre2 + Traduction("Cde",-2,"") + " " + Traduction("N�",-2,"") + " " + STRING(histoent2.NO_cde,"999999999") + STRING(Cpt), Traduction("Cde",-2,"") + " " + Traduction("N�",-2,"") + " " + string(histoent2.NO_cde) + " - " + Traduction("N� Fact.",-2,"") + " " + STRING(histoent2.NO_fact),3,3).
                END.
                Cpt = 0.
            END.
            sav = histoent.no_bl.
    end.
    ctrlframe:HIDDEN = NO.
    session:set-wait ("").
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RESIZEWINDOW W1 
PROCEDURE RESIZEWINDOW :
ADD-RESIZEWIDGET(FRAME f1:HANDLE, "WH").

    ADD-RESIZEWIDGET(FrmComBars-f1, "W").
    ADD-RESIZEWIDGET(Ctrlframe, "H").
    ADD-RESIZEWIDGET(butt-spli:HANDLE, "H").
    ADD-RESIZEWIDGET(rect-268:HANDLE, "W").
    ADD-RESIZEWIDGET(rect-270:HANDLE, "W").
    {centrer.i "FRAME fadresse"}
    {centrer.i "FRAME flegende"}
    DO-RESIZELOAD().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rowdisplay-ent W1 
PROCEDURE rowdisplay-ent :
DEF VAR WMtPce AS DEC DECIMALS 2 no-undo.
DEF VAR WMtSld AS DEC DECIMALS 2 no-undo.
/*$1130*/
IF rtype:SCREEN-VAL IN FRAME f1 = "F" AND Ptypmvt = "F" AND AVAIL entfacfo AND can-find(FIRST ligfacfo WHERE ligfacfo.ndos = entfacfo.ndos
                               AND ligfacfo.cod_fou = entfacfo.cod_fou
                               AND ligfacfo.NO_fact = entfacfo.NO_fact
                               AND ligfacfo.dat_fac = entfacfo.dat_fac
                               AND ligfacfo.litige = YES AND ligfacfo.resolu = NO NO-LOCK) THEN DO:
    IF VALID-HANDLE(hcoldisp-ent[1]) THEN hcoldisp-ent[1]:BGCOLOR = 31.
    if VALID-HANDLE(hcoldisp-ent[5]) THEN hcoldisp-ent[5]:FORMAT = "->>>>>>>>>" + formmt(entfacfo.devise).
    if VALID-HANDLE(hcoldisp-ent[6]) THEN hcoldisp-ent[6]:FORMAT = "->>>>>>>>>" + formmt(entfacfo.devise).
END.
ELSE IF rtype:SCREEN-VAL = "F" AND Ptypmvt = "F" AND AVAIL entfacfo AND can-find(FIRST ligfacfo WHERE ligfacfo.ndos = entfacfo.ndos /*$A42112*/
                               AND ligfacfo.cod_fou = entfacfo.cod_fou
                               AND ligfacfo.NO_fact = entfacfo.NO_fact
                               AND ligfacfo.dat_fac = entfacfo.dat_fac
                               AND ligfacfo.litige = YES AND ligfacfo.resolu = YES NO-LOCK) THEN DO:
    IF VALID-HANDLE(hcoldisp-ent[1]) THEN hcoldisp-ent[1]:BGCOLOR = 18.
    if VALID-HANDLE(hcoldisp-ent[5]) THEN hcoldisp-ent[5]:FORMAT = "->>>>>>>>>" + formmt(entfacfo.devise).
    if VALID-HANDLE(hcoldisp-ent[6]) THEN hcoldisp-ent[6]:FORMAT = "->>>>>>>>>" + formmt(entfacfo.devise).
END.

ELSE IF AVAIL histoent THEN DO:
    IF histoent.typ_mvt = "F" THEN DO:
        IF ges_WMS AND VALID-HANDLE(hcoldisp-ent[4]) AND NOT MOB-IS-SR-VALIDE(histoent.typ_mvt,histoent.cod_cf,histoent.NO_cde,histoent.NO_bl_a) THEN hcoldisp-ent[4]:BGCOLOR = 39. /*$2672*/
        ELSE IF can-find(FIRST histocr where histocr.cod_fou = histoent.cod_cf
                    and histocr.no_cde      = histoent.NO_cde
                    and histocr.no_bl_a     = histoent.NO_bl_a
                    AND histocr.litige      = yes
                    AND histocr.resolu      = NO
                    and histocr.sur_facture = NO NO-LOCK)
            AND VALID-HANDLE(hcoldisp-ent[4]) THEN DO:
            hcoldisp-ent[4]:BGCOLOR = 31.
        END.
        ELSE IF can-find(FIRST histocr where histocr.cod_fou = histoent.cod_cf
                    and histocr.no_cde      = histoent.NO_cde
                    and histocr.no_bl_a     = histoent.NO_bl_a
                    AND histocr.litige      = yes
                    AND histocr.resolu      = YES
                    and histocr.sur_facture = NO NO-LOCK)
            AND VALID-HANDLE(hcoldisp-ent[4]) THEN DO:
            hcoldisp-ent[4]:BGCOLOR = 18. /*$A42112*/
        END.

        IF histoent.no_fa_a[1]<>"" AND VALID-HANDLE(hcoldisp-ent[1]) THEN hcoldisp-ent[1]:BGCOLOR = 52.
        IF histoent.no_fa_a[1]<>"" AND VALID-HANDLE(hcoldisp-ent[3]) THEN hcoldisp-ent[3]:BGCOLOR = 52.
        if VALID-HANDLE(hcoldisp-ent[5]) THEN hcoldisp-ent[5]:FORMAT = "->>>>>>>>>" + formmt(histoent.devise).
        if VALID-HANDLE(hcoldisp-ent[7]) THEN hcoldisp-ent[7]:FORMAT = "->>>>>>>>>" + fmtm.
        if VALID-HANDLE(hcoldisp-ent[8]) THEN hcoldisp-ent[8]:FORMAT = "->>>>>>>>>" + formmt(histoent.devise).
        IF VALID-HANDLE(hcoldisp-ent[9]) AND histoent.dat_acc <> ? AND histoent.dat_acc < histoent.dat_liv THEN hcoldisp-ent[9]:BGCOLOR = 53.
    END.
    ELSE DO:
        /* Pi�ces r�gl�es A55569*/
        IF gestion-pocwin AND histoent.typ_mvt = "C" AND histoent.no_fact<>0 AND rtype:SCREEN-VAL IN FRAME f1 = "F" AND VALID-HANDLE(hcoldisp-ent[10]) THEN DO:
            ASSIGN
                WCL_CPT=if histoent.cl_paye<>0 then histoent.cl_paye
                    else if histoent.cl_fact<>0 then histoent.cl_fact
                    else histoent.cod_cf
                WMtPce = 0
                WMtSld = 0.

            FOR EACH faccli WHERE faccli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ faccli.CPT=cpt411 + wcl_cpt AND faccli.date_ecr=histoent.dat_fac AND faccli.pce = string(histoent.no_fact,"9999999999") NO-LOCK:
                ASSIGN
                    WMtPce = WMtPce + faccli.mt_pce
                    WMtSld = WMtSld + faccli.mt_solde.
            END.
            IF WMtPce<>0 THEN DO:
                IF WMtPce - WMtSld = 0 THEN hcoldisp-ent[10]:BGC = 160.
                ELSE IF WMtSld = 0 THEN hcoldisp-ent[10]:BGC = 58.
                ELSE hcoldisp-ent[10]:BGC = 131.
            END.
        END.

        if VALID-HANDLE(hcoldisp-ent[5]) THEN hcoldisp-ent[5]:FORMAT = "->>>>>>>>>" + fmtm.
        if VALID-HANDLE(hcoldisp-ent[6]) THEN hcoldisp-ent[6]:FORMAT = "->>>>>>>>>" + fmtm.
        if VALID-HANDLE(hcoldisp-ent[7]) THEN hcoldisp-ent[7]:FORMAT = "->>>>>>>>>" + fmtm.
        IF VALID-HANDLE(hcoldisp-ent[9]) AND histoent.dat_dep <> ? AND histoent.dat_dep < histoent.dat_liv THEN hcoldisp-ent[9]:BGCOLOR = 53.
    END.

    IF parcde.gst_zone[33] AND histoent.dep_csg <> 0 AND VALID-HANDLE(hcoldisp-ent[2]) THEN DO:
        hcoldisp-ent[2]:BGCOLOR=3.
    END.

    IF VALID-HANDLE(hcoldisp-ent[11]) THEN DO:
        IF histoent.retr_cess="C" THEN hcoldisp-ent[11]:BGCOLOR=57.
        ELSE IF histoent.retr_cess="R" THEN hcoldisp-ent[11]:BGCOLOR=110.
    END.

    IF VALID-HANDLE(hremglo) THEN hremglo:FORMAT = "->>9.99".
    IF VALID-HANDLE(hcarte) THEN hcarte:FORMAT = ">>>>>>>>9".
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rowdisplay-lig W1 
PROCEDURE rowdisplay-lig :
if VALID-HANDLE(hcoldisp-lig[1]) THEN
    hcoldisp-lig[1]:SCREEN-VAL = UL_CodeUL (histolig.uni_vte, histolig.lib_univ, histolig.lib_uv1, histolig.lib_uv2, histolig.lib_conv, histolig.lib_uv3, histolig.lib_surv).
IF VALID-HANDLE(hcoldisp-lig[2]) THEN hcoldisp-lig[2]:FORMAT = "->>>>>>" + fmtq.
IF VALID-HANDLE(hcoldisp-lig[3])  THEN hcoldisp-lig[3]:FORMAT = "->>>>>>>" + fmtv.
IF ptypmvt="C" THEN DO:
    if VALID-HANDLE(hcoldisp-lig[17]) AND histolig.forcer THEN hcoldisp-lig[17]:BGCOLOR=154.
    IF VALID-HANDLE(hcoldisp-lig[4]) THEN hcoldisp-lig[4]:FORMAT="->>>>>>>" + fmta.
END.
ELSE DO:
    IF VALID-HANDLE(hcoldisp-lig[4]) THEN hcoldisp-lig[4]:FORMAT="->>>>>>>" + formach(histolig.devise).
    if VALID-HANDLE(hcoldisp-lig[4]) AND histolig.forcer THEN hcoldisp-lig[4]:BGCOLOR=154.
END.

if VALID-HANDLE(hcoldisp-lig[5]) THEN hcoldisp-lig[5]:FORMAT = "->>>>>>>" + fmta.
if VALID-HANDLE(hcoldisp-lig[6]) THEN hcoldisp-lig[6]:FORMAT = "->>>>>>>" + fmta.
if VALID-HANDLE(hcoldisp-lig[19]) THEN hcoldisp-lig[19]:FORMAT = "->>>>>>>>" + fmtm.

if VALID-HANDLE(hcoldisp-lig[7]) THEN
    if histolig.sous_type="AR" then do:
        if VALID-HANDLE(hcoldisp-lig[7]) THEN hcoldisp-lig[7]:bgcolor =48.
        if VALID-HANDLE(hcoldisp-lig[8]) THEN DO:
            hcoldisp-lig[8]:bgcolor=48.
            /*$A62727...*/
            IF can-find(FIRST histocr where histocr.cod_fou = histolig.cod_cf
                        and histocr.no_cde      = histolig.NO_cde
                        and histocr.no_bl_a     = histolig.NO_bl_a
                        and histocr.no_ligne    = histolig.NO_ligne
                        AND histocr.litige      = yes
                        AND histocr.resolu      = NO
                        and histocr.sur_facture = NO NO-LOCK) THEN DO:
                hcoldisp-lig[8]:BGCOLOR = 31.
            END.
            ELSE IF can-find(FIRST histocr where histocr.cod_fou = histolig.cod_cf
                        and histocr.no_cde      = histolig.NO_cde
                        and histocr.no_bl_a     = histolig.NO_bl_a
                        and histocr.no_ligne    = histolig.NO_ligne
                        AND histocr.litige      = yes
                        AND histocr.resolu      = YES
                        and histocr.sur_facture = NO NO-LOCK)
                AND VALID-HANDLE(hcoldisp-ent[4]) THEN DO:
                hcoldisp-lig[8]:BGCOLOR = 18.
            END.
            /*...$A62727*/
        END.
        if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=48.
        if VALID-HANDLE(hcoldisp-lig[9]) THEN hcoldisp-lig[9]:bgcolor=48.
    END.
    else if histolig.sous_type="PS" then do:
        if VALID-HANDLE(hcoldisp-lig[7]) THEN hcoldisp-lig[7]:bgcolor=57.
        if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=57.
        if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=57.
        if VALID-HANDLE(hcoldisp-lig[9]) THEN hcoldisp-lig[9]:bgcolor=57.
    END.
    else if histolig.sous_type="DF" then do:
        if VALID-HANDLE(hcoldisp-lig[7]) THEN hcoldisp-lig[7]:bgcolor=59.
        if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=59.
        if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=59.
        if VALID-HANDLE(hcoldisp-lig[9]) THEN hcoldisp-lig[9]:bgcolor=59.
    END.
    else if histolig.sous_type="HS" then do:
        if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=55.
        if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=55.
        if VALID-HANDLE(hcoldisp-lig[9]) THEN hcoldisp-lig[9]:bgcolor=55.
    END.
    else do:
      if histolig.sous_type="PH" and (histolig.niveau1<>0 or histolig.niveau2<>0 or histolig.niveau3<>0 or histolig.niveau4<>0 or histolig.niveau5<>0) then do:
          if niveau5<>0 then do:
              if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=buf-couleur[5].
              if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=buf-couleur[5].
              if VALID-HANDLE(hcoldisp-lig[16]) THEN hcoldisp-lig[16]:bgcolor=buf-couleur[5].
          END.
          else if niveau4<>0 then do:
              if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=buf-couleur[4].
              if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=buf-couleur[4].
              if VALID-HANDLE(hcoldisp-lig[15]) THEN hcoldisp-lig[15]:bgcolor=buf-couleur[4].
          END.
          else if niveau3<>0 then do:
              if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=buf-couleur[3].
              if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=buf-couleur[3].
              if VALID-HANDLE(hcoldisp-lig[13]) THEN hcoldisp-lig[13]:bgcolor=buf-couleur[3].
          END.
          else if niveau2<>0 then do:
              if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=buf-couleur[2].
              if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=buf-couleur[2].
              if VALID-HANDLE(hcoldisp-lig[12]) THEN hcoldisp-lig[12]:bgcolor=buf-couleur[2].
          END.
          else if niveau1<>0 then do:
              if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=buf-couleur[1].
              if VALID-HANDLE(hcoldisp-lig[10]) THEN hcoldisp-lig[10]:bgcolor=buf-couleur[1].
              if VALID-HANDLE(hcoldisp-lig[11]) THEN hcoldisp-lig[11]:bgcolor=buf-couleur[1].
          END.
      end.
      else if VALID-HANDLE(hcoldisp-lig[8]) THEN hcoldisp-lig[8]:bgcolor=145.
    end.

    IF VALID-HANDLE(hcoldisp-lig[2]) THEN DO:
        IF histolig.typ_mvt = "V" THEN hcoldisp-lig[2]:bgcolor=133. /*$A87991*/
        ELSE IF histolig.lien_nmc<>0 AND histolig.nmc_lie="" THEN hcoldisp-lig[2]:bgcolor=103.
        ELSE IF histolig.nmc_lie="S" then hcoldisp-lig[2]:bgcolor=25.
    END.

    if histolig.gratuit AND VALID-HANDLE(hcoldisp-lig[19]) then hcoldisp-lig[19]:bgcolor=116.
    ELSE if histolig.garantie AND VALID-HANDLE(hcoldisp-lig[19]) then hcoldisp-lig[19]:bgcolor=148.
    ELSE if histolig.c_promo<>"" AND histolig.c_promo<>? AND VALID-HANDLE(hcoldisp-lig[19]) then hcoldisp-lig[19]:bgcolor=110.

    if VALID-HANDLE(hcoldisp-lig[17])  THEN hcoldisp-lig[17]:FORMAT = "->>>>>>>" + formvte(histolig.dev_vte).
    IF VALID-HANDLE(hcoldisp-lig[18]) THEN
        hcoldisp-lig[18]:SCREEN-VAL = UL_CodeUL (histolig.uni_ach, histolig.lib_unia, histolig.lib_ua1, histolig.lib_ua2, histolig.lib_cona, histolig.lib_ua3, histolig.lib_sura).

    /*$A35969...*/
    IF parsoc.prix_franco AND histoent.prix_franco /*AND histolig.px_trs > 0*/ AND VALID-HANDLE(hcoldisp-lig[20]) THEN
        ASSIGN hcoldisp-lig[20]:SCREEN-VAL = string(histolig.px_vte - histolig.px_trs)
               hcoldisp-lig[20]:FORMAT = "->>>>>>>" + fmtv.
    IF parsoc.prix_franco AND histoent.prix_franco AND VALID-HANDLE(hcoldisp-lig[21]) THEN hcoldisp-lig[21]:FORMAT = "->>>>>>>" + fmtv.
    /*...$A35969*/
    IF VALID-HANDLE(hcoldisp-lig[22]) THEN IF lldespro.des_lan = ? THEN hcoldisp-lig[22]:SCREEN-VALUE = "".
    IF VALID-HANDLE (dynreq_histolig) THEN RUN CalculerChampCalcule IN dynreq_histolig (IF AVAIL histolig THEN ROWID (histolig) ELSE ?). /*$A91177*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rowdisplay-lig2 W1 
PROCEDURE rowdisplay-lig2 :
IF VALID-HANDLE(hcoldisp-lig[1]) THEN
    hcoldisp-lig[1]:SCREEN-VAL = UL_CodeUL (ligfacfo.uni_ach, ligfacfo.lib_unia, ligfacfo.lib_ua1, ligfacfo.lib_ua2, ligfacfo.lib_cona, ligfacfo.lib_ua3, ligfacfo.lib_sura).
IF VALID-HANDLE(hcoldisp-lig[2]) THEN hcoldisp-lig[2]:FORMAT = "->>>>>>" + fmtq.
IF VALID-HANDLE(hcoldisp-lig[3]) THEN hcoldisp-lig[3]:FORMAT="->>>>>>>" + formach(ligfacfo.devise).
if VALID-HANDLE(hcoldisp-lig[4]) THEN hcoldisp-lig[4]:FORMAT="->>>>>>>" + formach(ligfacfo.devise).
if VALID-HANDLE(hcoldisp-lig[5]) THEN hcoldisp-lig[5]:FORMAT = "->>>>>>>>>" + formmt(ligfacfo.devise).
if VALID-HANDLE(hcoldisp-lig[6]) THEN hcoldisp-lig[6]:FORMAT = "->>>>>>>>>" + formmt(ligfacfo.devise).
IF VALID-HANDLE(hcoldisp-lig[7]) THEN IF lldespro.des_lan = ? THEN hcoldisp-lig[7]:SCREEN-VALUE = "".
IF VALID-HANDLE(hcoldisp-lig[8]) AND ligfacfo.litige THEN hcoldisp-lig[8]:BGC = (IF ligfacfo.resolu THEN 18 ELSE 31). /*$A62727*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE save-ini W1 
PROCEDURE save-ini :
DO WITH FRAME F1:
    IF loadsettings("HBHIS" + ptypmvt , output hsettingsapi) then do:
        ASSIGN logbid=SaveSetting("AutoRefresh", STRING(valbtn_autorefresh)) /*$A35790*/
            logbid = SaveSetting("CSoc" ,csoc:SCREEN-VAL)
            logbid=SaveSetting("Entete", ent$:SCREEN-VALUE)
            logbid = SaveSetting("ToutTri" , CHQRYBY-ent)
            logbid = SaveSetting("ChampTri" , COLORD-ent)
            logbid = SaveSetting("*HBColonnes" , qliste-champ-ent)
            logbid = SaveSetting("*HBBloquees" , STRING(qcolonne-lock-ent))
            logbid = SaveSetting("*HBLabels" , qliste-label-ent)
            logbid = SaveSetting("*HBCondition" , qcondition-Ent)
            logbid = SaveSetting("Arbo" , string(tbarbo:CHECKED))
            logbid = SaveSetting("TypeTiers" , rs-selection:SCREEN-VAL)
            logbid = SaveSetting("Tiers" , Vcod_cli:SCREEN-VAL)
            logbid = SaveSetting("Type" , Rtype:SCREEN-VAL).
        UnloadSettings(hSettingsApi).
    END.

    IF LoadSettings("HBHIL" + ptypmvt, OUTPUT hSettingsApi) THEN DO:
        ASSIGN logbid=SaveSetting("ToutTri",CHQRYBY-lig)
               logbid=SaveSetting("ChampTri", COLORD-lig)
               logbid=SaveSetting("*HBColonnes", qliste-champ-lig)
               logbid=SaveSetting("*HBBloquees", STRING(qcolonne-lock-lig))
               logbid=SaveSetting("*HBLabels", qliste-label-lig)
               logbid=SaveSetting("*HBCondition", qcondition-lig).
        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("HBHIS" + ptypmvt + "3", output hsettingsapi) then do:
        ASSIGN logbid = SaveSetting("ToutTri" ,CHQRYBY-ent2)
               logbid = SaveSetting("ChampTri" , COLORD-ent2)
               logbid = SaveSetting("*HBColonnes" , qliste-champ-ent2)
               logbid = SaveSetting("*HBBloquees" , STRING(qcolonne-lock-ent2))
               logbid = SaveSetting("*HBLabels" , qliste-label-ent2)
               logbid = SaveSetting("*HBCondition" , qcondition-ent2).

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("HISTFB":U, output hsettingsapi) then do:
        ASSIGN
            logbid = SaveSetting("FrmCompoX",string(FRAME frm-composants:X))
            logbid = SaveSetting("FrmCompoY",string(FRAME frm-composants:Y)).
        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("HBHIL" + ptypmvt + "3", output hsettingsapi) then do:
        ASSIGN logbid = SaveSetting("ToutTri" ,CHQRYBY-lig2)
               logbid = SaveSetting("ChampTri" , COLORD-lig2)
               logbid = SaveSetting("*HBColonnes" , qliste-champ-lig2)
               logbid = SaveSetting("*HBBloquees" , STRING(qcolonne-lock-lig2))
               logbid = SaveSetting("*HBLabels" , qliste-label-lig2)
               logbid = SaveSetting("*HBCondition" , qcondition-lig2).

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("EDTHF", output hsettingsapi) then do:
        logbid = SaveSetting("Exemplaire",vex).

        UnloadSettings(hSettingsApi).
    END.

    IF loadsettings("SAIREC", output hsettingsapi) then do:
        logbid = SaveSetting("Preference",rsref:SCREEN-VAL IN FRAME Frchoix).

        UnloadSettings(hSettingsApi).
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE splitterMoved W1 
PROCEDURE splitterMoved :
do with frame F1:
   assign CtrlFrame:WIDTH-P = butt-spli:X - butt-spli:WIDTH-P
          butt-spli:Y = CtrlFrame:Y
          butt-spli:X = CtrlFrame:X + CtrlFrame:WIDTH-P NO-ERROR.
   IF VALID-HANDLE(HBrowse-Ent) THEN DO:
       assign HBrowse-Ent:WIDTH-P = HBrowse-Ent:WIDTH-P + ((HBrowse-Ent:X - Butt-Spli:X) - 4) NO-ERROR.
       HBrowse-Ent:X = Butt-Spli:X + 4 NO-ERROR.
       ASSIGN svcol   = HBrowse-Ent:COL
              svwidth = HBrowse-Ent:WIDTH.
       /*$A53253...*/
       IF VALID-HANDLE(HBrowse-lig) THEN DO:
            assign HBrowse-lig:WIDTH-P = HBrowse-lig:WIDTH-P + ((HBrowse-lig:X - Butt-Spli:X) - 4) NO-ERROR.
            HBrowse-lig:X = Butt-Spli:X + 4 NO-ERROR.
       END.
       /*...$A53253*/
   END.
   ELSE ASSIGN svcol   = Butt-Spli:COL + Butt-Spli:WIDTH
               Svwidth = FRAME F1:WIDTH - Butt-Spli:COL.

end.  /* do with... */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traitement-plage W1 
PROCEDURE traitement-plage :
if (typ-plage="N" and not can-find(first histoent2 where histoent2.typ_mvt="C" and histoent2.ndos=n_d AND histoent2.no_fact>=fn-d and histoent2.no_fact<=fn-f no-lock))
        or (typ-plage="D" and not can-find(first histoent2 where histoent2.typ_mvt="C" and histoent2.ndos=n_d AND histoent2.dat_fac>=fdat-d and histoent2.dat_fac<=fdat-f no-lock))
        then return "9".

    /*$645*/
    EMPTY TEMP-TABLE WFAC NO-ERROR.
    EMPTY TEMP-TABLE WFACSIT NO-ERROR. /*$A65291*/
    K = 0.
    DO TRANSACTION:
        if tcli=no then do:
          if typ-plage="N" then for each histoent2 where histoent2.typ_mvt="C" and histoent2.ndos=n_d AND histoent2.no_fact>=fn-d and histoent2.no_fact<=fn-f no-lock by histoent2.no_fact by histoent2.dat_liv by histoent2.no_bl by histoent2.no_cde:
              FIND FIRST tabcomp WHERE tabcomp.type_tab = "LG" AND tabcomp.a_tab = histoent2.langue NO-LOCK NO-ERROR.
              IF AVAIL tabcomp THEN DO:
                IF radio-editpdf:HIDDEN  IN FRAME fplafac = NO AND LOGICAL(radio-editpdf:SCREEN-VAL) <> tabcomp.dev_d_m THEN NEXT.
              END.
              CREATE WFAC.
              ASSIGN WFAC.WFNRG = HISTOENT2.NO_FACT
                     K = K + 1
                     WFAC.WFORB = K
                     WFAC.WFRID = RECID(HISTOENT2).
          end.
          else if typ-plage="D" then for each histoent2 where histoent2.typ_mvt="C" and histoent2.ndos=n_d AND histoent2.dat_fac>=fdat-d and histoent2.dat_fac<=fdat-f no-lock by histoent2.no_fact by histoent2.dat_liv by histoent2.no_bl by histoent2.no_cde:
              FIND FIRST tabcomp WHERE tabcomp.type_tab = "LG" AND tabcomp.a_tab = histoent2.langue NO-LOCK NO-ERROR.
              IF AVAIL tabcomp THEN DO:
                IF radio-editpdf:HIDDEN = NO AND LOGICAL(radio-editpdf:SCREEN-VAL IN FRAME fplafac) <> tabcomp.dev_d_m THEN NEXT.
              END.
              CREATE WFAC.
              ASSIGN WFAC.WFNRG = HISTOENT2.NO_FACT
                     K = K + 1
                     WFAC.WFORB = K
                     WFAC.WFRID = RECID(HISTOENT2).
          end.
        end.
        else do:
          if typ-plage="N" then for each histoent2 where histoent2.typ_mvt="C" and histoent2.cod_cf=histoent.cod_cf and histoent2.ndos=n_d AND histoent2.no_fact>=fn-d and histoent2.no_fact<=fn-f no-lock by histoent2.no_fact by histoent2.dat_liv by histoent2.no_bl by histoent2.no_cde:
              FIND FIRST tabcomp WHERE tabcomp.type_tab = "LG" AND tabcomp.a_tab = histoent2.langue NO-LOCK NO-ERROR.
              IF AVAIL tabcomp THEN DO:
                IF radio-editpdf:HIDDEN = NO AND LOGICAL(radio-editpdf:SCREEN-VAL IN FRAME fplafac) <> tabcomp.dev_d_m THEN NEXT.
              END.
              CREATE WFAC.
              ASSIGN WFAC.WFNRG = HISTOENT2.NO_FACT
                     K = K + 1
                     WFAC.WFORB = K
                     WFAC.WFRID = RECID(HISTOENT2).
          end.
          else if typ-plage="D" then for each histoent2 where histoent2.typ_mvt="C" and histoent2.cod_cf=histoent.cod_cf and histoent2.ndos=n_d AND histoent2.dat_fac>=fdat-d and histoent2.dat_fac<=fdat-f no-lock by histoent2.no_fact by histoent2.dat_liv by histoent2.no_bl by histoent2.no_cde:
              FIND FIRST tabcomp WHERE tabcomp.type_tab = "LG" AND tabcomp.a_tab = histoent2.langue NO-LOCK NO-ERROR.
              IF AVAIL tabcomp THEN DO:
                IF radio-editpdf:HIDDEN = NO AND LOGICAL(radio-editpdf:SCREEN-VAL IN FRAME fplafac) <> tabcomp.dev_d_m THEN NEXT.
              END.
              CREATE WFAC.
              ASSIGN WFAC.WFNRG = HISTOENT2.NO_FACT
                     K = K + 1
                     WFAC.WFORB = K
                     WFAC.WFRID = RECID(HISTOENT2).
          end.
        end.

        /*$A65291...*/
        FOR EACH WFAC NO-LOCK:
            FIND FIRST histoent2 WHERE RECID(histoent2) = WFAC.WFRID AND histoent2.usr_mod = "Situation":U NO-LOCK NO-ERROR. 
            IF AVAILABLE histoent2 THEN DO:
                CREATE WFACSIT.
                ASSIGN WFACSIT.WFNRG = WFAC.WFNRG
                WFACSIT.WFORB = WFAC.WFORB
                WFACSIT.WFRID = WFAC.WFRID.
                DELETE WFAC.
            END.
        END.
        /*...$A65291*/

    END.
    return "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traiterDocGed W1 
PROCEDURE traiterDocGed :
/*$A66889...*/
    DEF INPUT PARAM pcle AS CHAR NO-UNDO.
    DEF INPUT PARAM pchemin AS CHAR NO-UNDO.

    CREATE tt-doc.
    ASSIGN cpt-doc = cpt-doc + 1
           tt-doc.tt-num = cpt-doc
           tt-doc.tt-cledoc = pcle.
    CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("Commande",-2,"") + " " + "Progiwin" + " N� ":U + string(histoent.NO_cde), (cpt-doc = 801)).
    /*...$A66889*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traiterDocGedARC W1 
PROCEDURE traiterDocGedARC :
/*$A66889...*/
    DEF INPUT PARAM pcle AS CHAR NO-UNDO.
    DEF INPUT PARAM pchemin AS CHAR NO-UNDO.

    CREATE tt-doc.
    ASSIGN cpt-doc = cpt-doc + 1
           tt-doc.tt-num = cpt-doc
           tt-doc.tt-cledoc = pcle.
    CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("A.R.C",-2,"") + " " + Traduction("fournisseur",-2,""), (cpt-doc = 801)).
    /*...$A66889*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traiterDocGedBL W1 
PROCEDURE traiterDocGedBL :
/*$A66889...*/
    DEF INPUT PARAM pcle AS CHAR NO-UNDO.
    DEF INPUT PARAM pchemin AS CHAR NO-UNDO.

    CREATE tt-doc.
    ASSIGN cpt-doc = cpt-doc + 1
           tt-doc.tt-num = cpt-doc
           tt-doc.tt-cledoc = pcle.
    CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("B.L Fournisseur",-2,"") + " N� ":U + string(histoent.NO_bl_a), (cpt-doc = 801)).
    /*...$A66889*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traiterDocGedFac W1 
PROCEDURE traiterDocGedFac :
/*$A66889...*/
    DEF INPUT PARAM pcle AS CHAR NO-UNDO.
    DEF INPUT PARAM pchemin AS CHAR NO-UNDO.

    CREATE tt-doc.
    ASSIGN cpt-doc = cpt-doc + 1
           tt-doc.tt-num = cpt-doc
           tt-doc.tt-cledoc = pcle.
    CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("Facture fournisseur",-2,"") + " " + string(histoent.NO_fa_a[1]), (cpt-doc = 801)).
    /*...$A66889*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE traiterDocGedREC W1 
PROCEDURE traiterDocGedREC :
/*$A66889...*/
    DEF INPUT PARAM pcle AS CHAR NO-UNDO.
    DEF INPUT PARAM pchemin AS CHAR NO-UNDO.

    CREATE tt-doc.
    ASSIGN cpt-doc = cpt-doc + 1
           tt-doc.tt-num = cpt-doc
           tt-doc.tt-cledoc = pcle.
    CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
    CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("Bon de r�ception",-2,"") + " " + "Progiwin", (cpt-doc = 801)).
    /*...$A66889*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valuechanged-ent W1 
PROCEDURE valuechanged-ent :
DO WITH FRAME F1:
    DEF VAR montant AS DEC no-undo.
    DEF VAR vdatpx AS DATE NO-UNDO. /*$A55185*/
    DEF BUFFER histoentF FOR histoent. /*$A55185*/

    /* $2222 ... */
    IF CONNECTED ("GED") AND pTypMvt = "C" THEN DO:
        DEFINE VARIABLE doc-exist AS LOGICAL NO-UNDO.
        DEFINE VARIABLE buf-cledoc AS CHAR NO-UNDO.

        FOR EACH tt-doc NO-LOCK:
            CBRemoveItem(hComBars-f1, 2, tt-doc.tt-num).
        END.
        EMPTY TEMP-TABLE tt-doc.

        cpt-doc = 800.

        /* recherche ARC */
        doc-exist = recherche_document_par_critere ("edtarc", "no_cde�no_bl�no_fact":U, STRING(histoent.no_cde) + "��", OUTPUT buf-cledoc).
        IF doc-exist THEN DO:
            CREATE tt-doc.
            ASSIGN cpt-doc = cpt-doc + 1
                   tt-doc.tt-num = cpt-doc
                   tt-doc.tt-cledoc = buf-cledoc.
            CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
            CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("A.R.C.":U,-2,"") + " N� ":U + string(histoent.no_cde), YES).
        END.
        /* $A59823... */
        IF NOT doc-exist OR histoent.no_devis <> 0 THEN DO:
            /* recherche Devis */
            doc-exist = recherche_document_par_critere ("edtdev", "no_cde�no_bl�no_fact":U, (IF histoent.no_devis <> 0 THEN STRING(histoent.no_devis) ELSE string(histoent.no_cde)) + "�0�0", OUTPUT buf-cledoc).
            IF doc-exist THEN DO:
                CREATE tt-doc.
                ASSIGN cpt-doc = cpt-doc + 1
                       tt-doc.tt-num = cpt-doc
                       tt-doc.tt-cledoc = buf-cledoc.
                CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
                CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("Devis":U,-2,"") + " N� ":U + (IF histoent.no_devis <> 0 THEN STRING(histoent.no_devis) ELSE string(histoent.no_cde)), YES).
            END.
        END.
        IF histoent.no_bl <> 0 THEN DO: /* ...$A59823 */
            /* recherche BL */
            doc-exist = recherche_document_par_critere("edtbl", "no_cde�no_bl":U, string(histoent.no_cde) + "�" + string(histoent.no_bl) + "�", OUTPUT buf-cledoc).
            IF doc-exist THEN DO:
                CREATE tt-doc.
                ASSIGN cpt-doc = cpt-doc + 1
                       tt-doc.tt-num = cpt-doc
                       tt-doc.tt-cledoc = buf-cledoc.
                CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
                CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("B.L.":U,-2,"") + " N� ":U + string(histoent.no_bl), (cpt-doc = 801)).
            END.
        END. /* $A59823 */
        IF histoent.no_fact <> 0 THEN DO: /* $A59823 */
            /* recherche FACTURE */
            /* $A88742 ...
            doc-exist = recherche_document_par_critere("edtfac", "no_cde�no_bl�no_fact":U, string(histoent.no_cde) + "�" + string(histoent.no_bl) + "�" + STRING(histoent.no_fact), OUTPUT buf-cledoc). */
            doc-exist = recherche_document_par_critere("edtfac", "no_fact":U, STRING(histoent.no_fact), OUTPUT buf-cledoc).
            /* ... $A88742 */
            IF doc-exist THEN DO:
                CREATE tt-doc.
                ASSIGN cpt-doc = cpt-doc + 1
                       tt-doc.tt-num = cpt-doc
                       tt-doc.tt-cledoc = buf-cledoc.
                CBLoadIcon(hComBars-f1, cpt-doc, StdMedia + "\StdMedia.icl",150).
                CBAddItem(hComBars-f1, 2, xitem-edt, {&xtpControlButton}, cpt-doc, Traduction("Facture",-2,"") + " N� ":U + string(histoent.no_fact), (cpt-doc = 801)).
            END.
        END. /* $A59823 */

    END.
    /* ... $2222 */

    /*$A66889...*/
    IF CONNECTED("GED") AND Ptypmvt = "F" THEN DO :

        FOR EACH tt-doc NO-LOCK:
            CBRemoveItem(hComBars-f1, 2, tt-doc.tt-num).
        END.
        EMPTY TEMP-TABLE tt-doc.

        cpt-doc = 800.

        IF AVAIL histoent AND histoent.NO_cde <> 0 THEN DO :

            RUN proc-doc-ged ("FCO":U, "traiterDocGed":U).
            RUN proc-doc-ged ("FBL":U, "traiterDocGedBL":U).
            RUN proc-doc-ged ("FFA":U, "traiterDocGedFac":U).
            RUN proc-doc-ged ("FBR":U, "traiterDocGedREC":U).
            RUN proc-doc-ged ("FAR":U, "traiterDocGedARC":U).

        END.


    END.
    /*...$A66889*/

    /*$2672...*/
    IF ges_WMS AND rtype:SCREEN-VAL = "BL":U AND Ptypmvt = "F" THEN DO:
        mode_validSR = NO.
        IF NOT AVAIL(histoent) THEN CBShowItem(Hcombars-f1, 2, {&breajuster}, NO).
        ELSE IF MOB-IS-SR-VALIDE(histoent.typ_mvt,histoent.cod_cf,histoent.NO_cde,histoent.NO_bl_a) THEN DO:
            CBSetItemCaption(Hcombars-f1, 2, {&Breajuster}, "&" + Traduction("Modifier R�ception",-2,"")).  /*$2122*/
            Logbid = CBShowItem(Hcombars-f1, 2, {&breajuster}, NOT pas-droit-reajuster).
        END.
        ELSE DO:
            CBSetItemCaption(Hcombars-f1, 2, {&Breajuster}, "&" + Traduction("Valider r�ception",-2,"")).  /*$2122*/
            CBSetItemTooltip(Hcombars-f1, 2, {&Breajuster}, "&" + Traduction("Valider r�ception",-2,"")).  /*$2122*/
            Logbid = CBShowItem(Hcombars-f1, 2, {&breajuster}, NOT pas-droit-validerSR).
            Logbid = CBEnableItem(Hcombars-f1, 2, {&breajuster}, MOB-IS-SR-VALIDABLE (histoent.typ_mvt,histoent.cod_cf,histoent.NO_cde,histoent.NO_bl_a)). /*$2122*/
            mode_validSR = YES.
        END.

    END.
    /*...$2672*/
    IF rtype:SCREEN-VAL = "F" AND Ptypmvt = "F" THEN DO:
        Logbid = CBShowItem (Hcombars-f1, 2, {&Blitige},can-find(FIRST Ligfacfo WHERE ligfacfo.ndos = entfacfo.ndos
                                                                 AND LIGFACFO.Cod_fou = ENTFACFO.Cod_fou
                                                                 and LIGFACFO.no_fact = ENTFACFO.no_fact
                                                                 and LIGFACFO.dat_fac = ENTFACFO.dat_fac
                                                                 AND ligfacfo.litige NO-LOCK)).
        IF ent$:SCREEN-VAL="T" THEN DO:
            RUN creation-browse-ligfac.
            if AVAIL ligfacfo THEN Hbrowse-lig:hidden=NO.
            else Hbrowse-lig:hidden=TRUE.
        END.
    END.
    ELSE DO:
        assign
            Logbid = CBEnableItem (Hcombars-f1, 2, {&Bdetail}, NO)
            Logbid = CBEnableItem (Hcombars-f1, 2, {&Bcr}, NO) /*$A62727*/
            logbid = CBEnableItem (hcombars-f1, 2, {&bimm}, NO)
            logbid = CBEnableItem (hcombars-f1, 2, {&blot}, NO)
            logbid = CBEnableItem (hcombars-f1, 2, {&bgrille_rem}, NO) /*A56990*/
            logbid = CBEnableItem (hcombars-f1, 2, {&bemp}, NO)
            logbid = CBEnableItem (hcombars-f1, 2, {&Btn-varctrlf8}, NO).
        IF ptypmvt = "F" AND AVAIL histoent THEN DO:
            ffrais = 0.
            IF histoent.maj_ach>0 OR histoent.dev_reg="O" THEN ASSIGN
                ffrais:bgc=131 freel:bgc=131 freel=Traduction("Estim�s",-2,"")
                ffrais = histoent.mt_cde * (histoent.rem_glo[1] / 100).
            ELSE DO:
                ASSIGN
                    ffrais:bgc=19 freel:bgc=19 freel=Traduction("R�els",-2,"").
                IF histoent.dos_pxr=0 AND histoent.cal_marg="" THEN do z = 1 to 6:
                    if histoent.frais[z] <> 0 then do:
                        montant = histoent.frais[z].
                        if histoent.dev_frais[z] <> g-dftdev then
                          run conv-dev ("M","D","A",histoent.dev_frais[z],histoent.dat_px,0,montant,output montant).
                        ffrais = ffrais + montant.
                    end.
                end.
                ELSE IF histoent.dos_pxr<>0 THEN
                DO:
                    /*$A55185...*/
                    vdatpx = ?.
                    FOR EACH histoentF WHERE histoentF.DOS_pxr = histoent.DOS_pxr AND histoentF.typ_mvt = "F" AND histoentF.devise <> g-dftdev AND histoentF.dat_px <> ? NO-LOCK:
                        IF vdatpx = ? THEN vdatpx = histoentF.dat_px.
                        ELSE vdatpx = MIN(vdatpx,histoentF.dat_px).
                    END.
                    /*...$A55185*/
                    FOR EACH histoent2 FIELDS (DOS_pxr typ_mvt cod_cf tx_ech_d NO_cde NO_bl dat_px) where histoent2.DOS_pxr=histoent.DOS_pxr AND histoent2.typ_mvt="H" NO-LOCK:
                        FOR EACH histolig FIELDS (typ_mvt cod_cf NO_cde NO_bl NO_serie mt_ht devise) where histolig.typ_mvt="H" and histolig.cod_cf=histoent2.cod_cf AND histolig.no_cde=histoent2.no_cde AND histolig.no_bl=histoent2.no_bl AND histolig.regr = histoent2.dos_pxr USE-INDEX hlig1 NO-LOCK:
                            IF histolig.NO_serie="AS":U
                                OR histolig.NO_serie="TR":U
                                OR histolig.NO_serie="DR":U THEN NEXT.
                            montant=histolig.mt_ht.
                            IF HISTOlig.devise <> G-DFTDEV THEN
                                RUN conv-dev ("M","D","A",HISTOlig.devise,(IF HISTOENT2.DAT_PX <> ? THEN histoent2.dat_px ELSE vdatpx) /*$A55185 HISTOENT2.DAT_PX*/,histoent2.tx_ech_d,MONTANT,OUTPUT MONTANT).
                            ffrais = ffrais + MONTANT.
                        END.
                    END.
                END.
            END.
            DISP ffrais freel WITH FRAME F1.
        END.

        FIND CURRENT histoent NO-LOCK NO-ERROR. /* �XREF_NOWHERE� */
        /*Logbid = CBShowItem (Hcombars-f1, 2, {&Bdoc}, CAN-FIND(FIRST infoent WHERE infoent.typ_fich = /*"E"*/ typinfo AND infoent.NO_info = histoent.NO_info NO-LOCK)). /*A20014*/*/ /*$A60546*/

        /* Note interne */
        /*$1728...*/
        IF ptypmvt="H":u THEN find txtentcl where txtentcl.cod_cli=histoent.cod_cf and txtentcl.typ_sai="H":u and txtentcl.no_cde=histoent.no_cde no-lock no-error.
        ELSE /*...$1728*/
        IF ptypmvt="F" THEN find txtentcl where txtentcl.cod_cli=histoent.cod_cf and txtentcl.typ_sai="F" and txtentcl.no_cde=histoent.no_cde no-lock no-error.
        ELSE find txtentcl where txtentcl.cod_cli=histoent.cod_cf and txtentcl.typ_sai="C" and txtentcl.no_cde=histoent.no_cde and txtentcl.no_bl=histoent.no_bl no-lock no-error.
        if available txtentcl and txtentcl.note <> "" then do:
            IF NOT VALID-HANDLE(hpostit) THEN RUN postit PERSISTENT SET hpostit.
            IF VALID-HANDLE(hpostit) then RUN PostitAffichage IN hpostit (FRAME f1:HANDLE, 1, "NI", Traduction("Note interne",-2,"") + " (" + Traduction("Commande",-2,"") + " " + STRING(txtentcl.no_cde) + ")", txtentcl.note).
        end.
        else IF VALID-HANDLE(hpostit) then RUN PostitHide IN hpostit (1).

        IF rtype:SCREEN-VAL="BL" THEN ASSIGN
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bcolisage},CAN-FIND(FIRST colisage where colisage.cod_cli = histoent.cod_cf and colisage.typ_sai = "F" and colisage.no_bl = histoent.NO_bl and colisage.no_cde = histoent.no_cde NO-LOCK))
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bfax},CanXpedite AND NOT (AVAIL parsoc AND parsoc.ges_car AND AVAIL carpar AND histoent.nat_cde = carpar.nature) AND pTypMvt = "C")
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bmail},NOT (AVAIL parsoc AND parsoc.ges_car AND AVAIL carpar AND histoent.nat_cde = carpar.nature) AND pTypMvt = "C")
            valbtn_blchiffr = histoent.chif_bl                              /*$A62526*/
            Logbid = CBCheckItem(hComBars-f1, 2, {&bblchiffr}, valbtn_blchiffr ).    /*$A62526*/
        ELSE Logbid = CBShowItem (Hcombars-f1, 2, {&Bcolisage},NO).
        /*$1728...*/
        IF ptypmvt = "H":u  THEN ASSIGN
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bcolisage},NO)
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bfax},NO)
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bmail},NO)
            logbid = CBShowItem (Hcombars-f1, 2, {&btrpaffret}, YES ).
        ELSE logbid = CBShowItem (Hcombars-f1, 2, {&btrpaffret}, NO ) /*$1728*/ .
        /*...$1728*/
        if RType:screen-value="BL" and ptypmvt="F" then assign
            Logbid = CBSetItemCaption(Hcombars-f1, 2, {&Bediter}, "&" + (IF histoent.mt_cde<0 THEN Traduction("Bon de Retour",-2,"") ELSE Traduction("Bon R�ception",-2,"")))  /*$2122*/
            Logbid = CBSetItemTooltip(Hcombars-f1, 2, {&Bediter}, IF histoent.mt_cde<0 THEN Traduction("Bon de Retour",-2,"") ELSE Traduction("Bon R�ception",-2,"")) /*$2122*/
            NOMPGM = IF histoent.mt_cde < 0 OR
                       ( histoent.mt_cde = 0 AND NOT CAN-FIND (FIRST histolig where histolig.typ_mvt = histoent.typ_mvt and histolig.cod_cf = histoent.cod_cf and histolig.no_cde = histoent.NO_cde and histolig.no_bl = histoent.NO_bl and histolig.nmc_lie <> 'R' NO-LOCK)
                        )
                     THEN "EDTHCFO":U ELSE "EDTRF"  /*$2122*/
            Logbid = CBSetItemEdition (Hcombars-f1, 2, {&Bediter}, NOMPGM).

        IF ent$:SCREEN-VAL="T" THEN DO:
            RUN creation-browse-ligne.
            if AVAIL histolig THEN Hbrowse-lig:hidden=NO.
            else Hbrowse-lig:hidden=TRUE.
        END.
    END.

    IF ModeleCaract <> "" THEN /* $A50695 */
        CBShowItem (Hcombars-f1, 2, {&Bcarvar},rtype:SCREEN-VAL <> "F" AND AVAIL histoent AND CAN-FIND (FIRST histocar WHERE histocar.typ_zon = "REC":U AND histocar.cle2 = STRING (histoent.cod_cf) AND histocar.cle1 = STRING (histoent.NO_cde) AND histocar.cle3 = STRING (histoent.no_bl_a) NO-LOCK)).

    CBEnableItem (hComBars-f1, 2, {&btracacde}, CAN-FIND (FIRST histoed WHERE histoed.vente AND histoed.cod_cf = histoent.cod_cf AND histoed.NO_cde = histoent.NO_cde AND histoed.NO_bl = histoent.NO_bl NO-LOCK)). /* $A62337 */

    CBEnableItem (hComBars-f1, 2, {&bcompare}, comparaisonDispo()). /*A68752*/

    FRAME frm-composants:HIDDEN = YES.
    IF VALID-HANDLE (dynreq) THEN RUN AfficherFrame IN dynreq. /* $1495 */
    IF VALID-HANDLE (dynreq_facfo) THEN RUN AfficherFrame IN dynreq_facfo. /* $1495 */
    IF VALID-HANDLE (dynreq_histolig) THEN RUN AfficherFrame IN dynreq_histolig. /* $1495 */
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valuechanged-lig W1 
PROCEDURE valuechanged-lig :
DO WITH FRAME F1:
    assign
        Logbid = CBEnableItem (Hcombars-f1, 2, {&Bdetail}, NO)
        Logbid = CBEnableItem (Hcombars-f1, 2, {&Bcr}, NO) /*$A62727*/
        logbid = CBEnableItem (hcombars-f1, 2, {&bimm}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&blot}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&bgrille_rem}, NO) /*A56990*/
        logbid = CBEnableItem (hcombars-f1, 2, {&bemp}, NO)
        logbid = CBEnableItem (hcombars-f1, 2, {&Btn-varctrlf8}, NO).

    if hbrowse-lig:num-selected-rows=1 AND can-do("AR,DF,PS",histolig.sous_type) then DO:
        assign
            Logbid = CBEnableItem (Hcombars-f1, 2, {&Bdetail}, YES)
            logbid = CBEnableItem (hcombars-f1, 2, {&bimm},(histolig.lien_imm <> 0))
            logbid = CBEnableItem (hcombars-f1, 2, {&blot},(histolig.lien_lot <> 0))
            logbid = CBEnableItem (hcombars-f1, 2, {&bgrille_rem},(ptypmvt = "C" AND parsoc.grille_rem AND histolig.grille_rem <> "")) /*A56990*/
            logbid = CBEnableItem (hcombars-f1, 2, {&bemp},(histolig.lien_emp <> 0))
            logbid = CBEnableItem (hcombars-f1, 2, {&Btn-varctrlf8},(histolig.k_var<>""))
            Logbid = CBShowItem (Hcombars-f1, 2, {&Bdetail}, YES)
            logbid = CBShowItem (hcombars-f1, 2, {&bimm},(histolig.lien_imm <> 0))
            logbid = CBShowItem (hcombars-f1, 2, {&blot},(histolig.lien_lot <> 0))
            logbid = CBShowItem (hcombars-f1, 2, {&bgrille_rem},(ptypmvt = "C" AND parsoc.grille_rem AND histolig.grille_rem <> "")) /*A56990*/
            logbid = CBShowItem (hcombars-f1, 2, {&bemp},(histolig.lien_emp <> 0))
            logbid = CBShowItem (hcombars-f1, 2, {&Btn-varctrlf8},(histolig.k_var<>"")).

        /*$A62727*/
        IF Ptypmvt = "F" THEN ASSIGN Logbid = CBEnableItem (Hcombars-f1, 2, {&Bcr}, YES)
                                     Logbid = CBShowItem (Hcombars-f1, 2, {&Bcr}, YES).

        /* Variantes */
        IF histolig.k_var<>"" THEN DO:
            RUN calkvar_gp (?, histolig.k_var, (histolig.cod_pro), 2, 110, OUTPUT mybuf2, OUTPUT mybuf1).
            hbrowse-lig:TOOLTIP=replace(mybuf1,"|","~n").
        END.
        ELSE IF histolig.cod_dec1<>"" THEN DO:
            RUN caldec (histolig.typ_elem,trim(histolig.cod_dec1) + "," + trim(histolig.cod_dec2) + "," + trim(histolig.cod_dec3) + "," + trim(histolig.cod_dec4) + "," + trim(histolig.cod_dec5),OUTPUT mybuf1).
            hbrowse-lig:TOOLTIP=mybuf1.
        END.
        ELSE hbrowse-lig:TOOLTIP="".

        /*$1208*/
        IF VALID-HANDLE (AffSVariante) AND AVAIL histolig THEN
            RUN AffichageVariante IN AffSVariante (histolig.k_var + (IF histolig.k_opt <> "" THEN CHR (1) + histolig.k_opt ELSE "" /* $A35329 */) ,STRING(histolig.cod_pro)).

        /* $A33837... */

        IF AVAIL HISTOLIG AND HISTOLIG.lien_nmc <> 0 AND HISTOLIG.nmc_lie = "" THEN DO:
            FIND FIRST histolig2 WHERE histolig2.typ_mvt = histolig.typ_mvt AND histolig2.cod_cf = histolig.cod_cf AND histolig2.no_cde = histolig.no_cde
                                   AND histolig2.no_bl = histolig.no_bl AND histolig.div_fac = histolig2.div_fac AND histolig2.nmc_lie = 'S'
                                   AND histolig2.lien_nmc = histolig.lien_nmc NO-LOCK NO-ERROR.
            IF AVAIL histolig2 THEN RUN Actu-Composants.
            ELSE FRAME frm-Composants:HIDDEN = YES.
        END.
        ELSE FRAME frm-Composants:HIDDEN = YES.
                /* ...$A33837 */

    END.
    ELSE FRAME frm-Composants:HIDDEN = YES.

    IF VALID-HANDLE (dynreq_histolig) THEN RUN Afficherrequete IN dynreq_histolig (IF AVAIL histolig AND VALID-HANDLE (hbrowse-lig) AND HBrowse-lig:NUM-SELECTED-ROWS = 1 THEN ROWID (histolig) ELSE ?). /*$A91177*/
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION comparaisonDispo W1 
FUNCTION comparaisonDispo LOG: /*$A68752*/
    DEF BUFFER bDevis FOR entetcli.

    IF AVAIL histoent THEN
        FIND FIRST bDevis WHERE bDevis.typ_sai = "D" AND bDevis.no_cde = histoent.no_devis NO-LOCK USE-INDEX nocde NO-ERROR.

    RETURN AVAIL bDevis.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

