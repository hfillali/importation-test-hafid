&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                          
DEFINE INPUT  PARAMETER c-file  AS INTEGER    NO-UNDO.*/
DEFINE INPUT  PARAMETER ptype   AS CHARACTER    NO-UNDO. 
/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101

{Hgcoergo.i}
{hfileopen.i}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES produit

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 produit.cod_pro produit.cod_nom ~
produit.refint produit.refext 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define QUERY-STRING-BROWSE-2 FOR EACH produit NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH produit NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 produit
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 produit


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BROWSE-2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      produit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 C-Win _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      produit.cod_pro FORMAT ">>>>>>9":U WIDTH 11.43
      produit.cod_nom FORMAT "X(10)":U WIDTH 27.57
      produit.refint FORMAT "X(17)":U WIDTH 27.14
      produit.refext FORMAT "X(17)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 110 BY 15 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     BROWSE-2 AT ROW 5.25 COL 5 WIDGET-ID 200
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 116 BY 19.92 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "<insert window title>"
         HEIGHT             = 19.92
         WIDTH              = 116
         MAX-HEIGHT         = 40.08
         MAX-WIDTH          = 182.86
         VIRTUAL-HEIGHT     = 40.08
         VIRTUAL-WIDTH      = 182.86
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-2 1 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "GCO.produit"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   > GCO.produit.cod_pro
"produit.cod_pro" ? ? "integer" ? ? ? ? ? ? no ? no no "11.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[2]   > GCO.produit.cod_nom
"produit.cod_nom" ? ? "character" ? ? ? ? ? ? no ? no no "27.57" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[3]   > GCO.produit.refint
"produit.refint" ? ? "character" ? ? ? ? ? ? no ? no no "27.14" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[4]   = GCO.produit.refext
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* <insert window title> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* <insert window title> */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME DEFAULT-FRAME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL DEFAULT-FRAME C-Win
ON GO OF FRAME DEFAULT-FRAME
DO:
   OPEN QUERY BROWSE-2 FOR EACH produit WHERE produit.cod_pro > ptype NO-LOCK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.

ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).

        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
        END CASE.
    END.

END PROCEDURE.
&ANALYZE-RESUME
/* _UIB-CODE-BLOCK-END */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win  
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win  
PROCEDURE proc-init :
DO WITH FRAME DEFAULT-FRAME:
     ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-Win .
        RUN combarscreation.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win  
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI DEFAULT-FRAME  _DEFAULT-DISABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
   ENABLE BROWSE-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



